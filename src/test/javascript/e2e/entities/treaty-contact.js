'use strict';

describe('TreatyContact e2e test', function () {

  var searchParameter=element(by.model('vm.searchQuery'));

    beforeAll(function () {
        browser.get('#/');

    });

    it('should load Deals', function () {
      element(by.css('[ui-sref="deal"]')).click().then(function() {
        expect(element.all(by.css('h3')).first().getText()).toMatch(/Deals/);
      });
    });

    it('Search a Deal', function () {

      searchParameter.clear();
      searchParameter.sendKeys("Test");

      element(by.css('[ng-click="vm.search(vm.searchQuery)"]')).click();
      expect(element(by.exactRepeater('deal in vm.deals')).isPresent()).toBe(true);

      var dealName = element.all(by.repeater('deal in vm.deals').column('deal.name}'));

      element(by.exactRepeater('deal in vm.deals')).isPresent().then(function(result) {
        if (result ) {
          element(by.repeater('deal in vm.deals').row(0)).click();
        }else{
          searchParameter.clear();
          element(by.css('[ng-click="vm.search(vm.searchQuery)"]')).click();
          element(by.repeater('deal in vm.deals').row(0)).click();
        }
      });
    });
    it('should load treaties and Tracking home', function () {
      element(by.css('[ui-sref="treaty-deal({dealId:vm.dealId, page:vm.page})"]')).click();
      expect(element(by.exactRepeater('treaty in vm.treaties')).isPresent()).toBe(true);
      element(by.repeater('treaty in vm.treaties').row(0)).click();

    });


    it('should be on Treaty contact/Companies', function () {
      browser.driver.sleep(1000);
      element(by.css('[ui-sref="contacts-company-detail({id:vm.treatyId,dealId:vm.dealId})"]')).click();
      expect(element(by.id('searchCompanyInput')).isPresent()).toBe(true);;


    });

    it('should be on Associated Companies', function () {
      browser.driver.sleep(1000);
      element(by.css('[ui-sref="contacts-company-detail({id:vm.treatyId,dealId:vm.dealId})"]')).click();

      element(by.id('searchCompanyInput')).sendKeys("tes");
      element(by.id('searchCompanyInput')).sendKeys(protractor.Key.BACK_SPACE);
      browser.driver.sleep(1000);
      element(by.id('searchCompanyInput')).sendKeys(protractor.Key.TAB);
      element(by.model('vm.companyTypeOption')).all(by.tagName('option')).get(1).click();
      element(by.css('[ng-click="addCompany()"]')).click();
      browser.driver.sleep(1000);
      expect(element(by.options('xrefcompany in vm.xrefTreatyCompanies')).isPresent()).toBe(true);

    });

    it('should be on Associated contacts', function () {

      element(by.id('searchExistingContactInput')).sendKeys("tes");
      element(by.id('searchExistingContactInput')).sendKeys(protractor.Key.BACK_SPACE);
      browser.driver.sleep(1000);
      element(by.id('searchExistingContactInput')).sendKeys(protractor.Key.TAB);
      element(by.css('[ng-click="addContact()"]')).click();
      browser.driver.sleep(1000);
      expect(element(by.options('xrefContact in vm.xrefTreatyContacts')).isPresent()).toBe(true);
      //browser.pause();

    });

    afterAll(function () {

    });
});
