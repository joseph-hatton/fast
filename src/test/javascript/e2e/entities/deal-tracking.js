'use strict';

describe('DealTracking e2e test', function () {

    var username = element(by.id('username'));
    var password = element(by.id('password'));
    var entityMenu = element(by.id('entity-menu'));
    var accountMenu = element(by.id('account-menu'));
    var login = element(by.id('login'));
    var logout = element(by.id('logout'));

    beforeAll(function () {
        browser.get('#/deal/24422/1');
        browser.driver.wait(protractor.until.elementIsVisible(element(by.css('h3'))));
    });

    it('should load DealTrackings', function () {
        entityMenu.click();
        element(by.css('[ui-sref="deal-tracking"]')).click().then(function() {
            expect(element.all(by.css('h2')).first().getText()).toMatch(/DealTrackings/);
        });
    });

    it('should load create DealTracking dialog', function () {
        element(by.css('[ui-sref="deal-tracking.new"]')).click().then(function() {
            expect(element(by.css('h4.modal-title')).getText()).toMatch(/Create or edit a DealTracking/);
            element(by.css('button.close')).click();
        });
    });

    afterAll(function () {
        accountMenu.click();
        logout.click();
    });
});
