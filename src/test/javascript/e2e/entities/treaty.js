'use strict';

describe('Treaty e2e test', function () {

    var searchParameter=element(by.model('vm.searchQuery'));
    var treatyName=element(by.model('vm.treaty.treatyName'));

    beforeAll(function () {
      browser.get('#/');
      browser.driver.sleep(1000);
    });

    it('should load Deals', function () {
      element(by.css('[ui-sref="deal"]')).click().then(function() {
        expect(element.all(by.css('h3')).first().getText()).toMatch(/Deals/);
      });
    });

    it('Search a Deal', function () {

      searchParameter.clear();
      searchParameter.sendKeys("Test");

      element(by.css('[ng-click="vm.search(vm.searchQuery)"]')).click();
      expect(element(by.exactRepeater('deal in vm.deals')).isPresent()).toBe(true);

      var dealName = element.all(by.repeater('deal in vm.deals').column('deal.name}'));

      element(by.exactRepeater('deal in vm.deals')).isPresent().then(function(result) {
        if (result ) {
          element(by.repeater('deal in vm.deals').row(0)).click();
        }else{
          searchParameter.clear();
          element(by.css('[ng-click="vm.search(vm.searchQuery)"]')).click();
          element(by.repeater('deal in vm.deals').row(0)).click();
        }
      });
    });

    it('should load treaty-deal dialog', function () {
      element(by.css('[ui-sref="treaty-deal({dealId:vm.dealId, page:vm.page})"]')).click();
      element(by.css('[ui-sref="treaty-deal.new({dealId:vm.dealId})"]')).click().then(function() {
        expect(element(by.css('h4')).getText()).toMatch(/Create or Edit Treaty Basic Information/);
      });
    });

    it('should enter the treaty basic information', function () {
      var today = new Date();
      var day = today.getDate();
      var month = today.getMonth()+1; //By default January count as 0
      var year = today.getFullYear();
      if(day<10) {
        day='0'+day
      }
      if(month<10) {
        month='0'+month
      }
      browser.driver.sleep(1000);
      var CuurentDate= year+''+month+''+day;
      var num=Math.floor((Math.random() * 100) + 1);
      treatyName.sendKeys("Test Treaty"+''+num);
      element(by.css('option[value="4"]')).click();
      element(by.css('option[value="Annuity Co Funds Withheld"]')).click();
      element(by.model('vm.treaty.effectiveDate')).clear();
      element(by.model('vm.treaty.effectiveDate')).sendKeys(CuurentDate);
      element(by.css('option[value="United States"]')).click();
      element(by.css('button[type=submit]')).click();

    });

    it('should be on Treaties and Tracking', function () {
      expect(element(by.exactRepeater('treaty in vm.treaties')).isPresent()).toBe(true)
      element(by.repeater('treaty in vm.treaties').row(0)).click();


    });
    it('should be on Reporting Due', function () {
      element(by.css('div[name=reportingDuePanel] a.accordion-toggle')).click();
      expect(element(by.id('btnTreatyDetailEditReportingDue')).isPresent()).toBe(true);
      element(by.id('btnTreatyDetailEditReportingDue')).click();
      browser.driver.sleep(1000);
      var reportingFrequency = element.all(by.options('periodOptions as periodOptions.name for periodOptions in vm.periodOptions track by periodOptions.id'));
      var selectReportingFrequency = reportingFrequency.get(1);
      selectReportingFrequency.click();
      browser.driver.sleep(1000);

      element(by.model('vm.reportingDue.numberOfDays')).isEnabled().then(function(result) {
        if (result ) {
          console.log("RESULT"+result);
         // element(by.model('vm.reportingDue.numberOfDays')).clear();
          element(by.model('vm.reportingDue.numberOfDays')).sendKeys('10');
        }
      });


      var dayTypeOptions = element.all(by.options('DayTypeOptions as DayTypeOptions for DayTypeOptions in vm.DayTypeOptions track by DayTypeOptions'));
      dayTypeOptions.get(1).click();
      expect(element(by.css('button[type=submit]')).isPresent()).toBe(true);
      element(by.css('button[type=submit]')).click();
      browser.driver.sleep(1000);

    });

    it('should be on Settlement Due -Assuming', function () {
      browser.driver.sleep(1000);
      element(by.css('div[name=settlementDueAssumPanel] a.accordion-toggle')).click();
      expect(element(by.id('btnTreatyDetailEditSettDueAssuming')).isPresent()).toBe(true);
      element(by.id('btnTreatyDetailEditSettDueAssuming')).click();
      browser.driver.sleep(1000);

      //Settlement Due
      element(by.model('vm.treaty.settlementFrequency')).all(by.tagName('option')).get(1).click();

      //element(by.model('vm.settlementDue.numberOfDays')).clear();
      element(by.model('vm.settlementDue.numberOfDays')).sendKeys('10');
      element(by.model('vm.settlementDue.dayType')).all(by.tagName('option')).get(1).click();

      //Grace Period Expiration
      element(by.model('vm.settlementPast.numberOfDays')).clear();
      element(by.model('vm.settlementPast.numberOfDays')).sendKeys('10');
      element(by.model('vm.settlementPast.dayType')).all(by.tagName('option')).get(1).click();
      expect(element(by.css('button[type=submit]')).isPresent()).toBe(true);
      element(by.css('button[type=submit]')).click();

    });

    it('should be on Treaty currencies', function () {
      browser.driver.sleep(1000);
      element(by.css('div[name=treatyCurrenciesPanel] a.accordion-toggle')).click();
      expect(element(by.id('btnTreatyDetailEditCurrencies')).isPresent()).toBe(true);
      element(by.id('btnTreatyDetailEditCurrencies')).click();
      browser.driver.sleep(1000);
      element(by.model('vm.currency')).all(by.tagName('option')).get(2).click();
      element(by.css('[ng-click="vm.currencyAdd()"]')).click();
      expect(element(by.css('button[type=submit]')).isPresent()).toBe(true);
      element(by.css('button[type=submit]')).click();
    });




    afterAll(function () {

    });
});
