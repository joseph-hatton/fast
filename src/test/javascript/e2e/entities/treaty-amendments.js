'use strict';

describe('TreatyAmendments e2e test', function () {

  var searchParameter=element(by.model('vm.searchQuery'));

    beforeAll(function () {
        browser.get('#/');
        browser.driver.wait(protractor.until.elementIsVisible(element(by.css('h1'))));


    });

      it('should load Deals', function () {
        element(by.css('[ui-sref="deal"]')).click().then(function() {
          expect(element.all(by.css('h3')).first().getText()).toMatch(/Deals/);
        });
      });

      it('Search a Deal', function () {

        searchParameter.clear();
        searchParameter.sendKeys("Test");

        element(by.css('[ng-click="vm.search(vm.searchQuery)"]')).click();
        expect(element(by.exactRepeater('deal in vm.deals')).isPresent()).toBe(true);

        var dealName = element.all(by.repeater('deal in vm.deals').column('deal.name}'));

        element(by.exactRepeater('deal in vm.deals')).isPresent().then(function(result) {
          if (result ) {
            element(by.repeater('deal in vm.deals').row(0)).click();
          }else{
            searchParameter.clear();
            element(by.css('[ng-click="vm.search(vm.searchQuery)"]')).click();
            element(by.repeater('deal in vm.deals').row(0)).click();
          }
        });
      });

      it('should load treaties and Tracking home', function () {
        element(by.css('[ui-sref="treaty-deal({dealId:vm.dealId, page:vm.page})"]')).click();
        expect(element(by.exactRepeater('treaty in vm.treaties')).isPresent()).toBe(true);
        element(by.repeater('treaty in vm.treaties').row(0)).click();

      });

      it('should load TreatyAmendments', function () {
          element(by.css('[ui-sref="treaty-amendments-all({treatyId:vm.treatyId, dealId:vm.dealId})"]')).click().then(function() {
              expect(element.all(by.css('h3')).first().getText()).toMatch(/Amendments/);
          });
      });

      it('should load the popup for  a New TreatyAmendments', function () {
        element(by.css('[ui-sref="treaty-amendments-all.new({dealId:vm.dealId,treatyId:vm.treatyId})"]')).click().then(function() {
          expect(element.all(by.css('h4')).first().getText()).toMatch(/Create or Edit Amendment/);
        });
      });

      it('should pass the value  New Amendments', function () {
        element(by.model('vm.treatyAmendments.amendmentName')).sendKeys("12345");

        var today = new Date();
        var day = today.getDate();
        var month = today.getMonth()+1; //By default January count as 0
        var year = today.getFullYear();
        if(day<10) {
          day='0'+day
        }
        if(month<10) {
          month='0'+month
        }
        browser.driver.sleep(1000);
        var curentDate= year+''+month+''+day;
        element(by.model('vm.treatyAmendments.effectiveDate')).sendKeys(curentDate);
        element(by.model('vm.treatyAmendments.comments')).sendKeys("Amendment for testing");
        element(by.css('button[type=submit]')).click();

      });

      it('should load the Home page of TreatyAmendments', function () {
        //expect(element(by.exactRepeater('treatyAmendments in vm.treatyAmendments track by treatyAmendments.id')).isPresent()).toBe(true);

      });

    afterAll(function () {

    });
});
