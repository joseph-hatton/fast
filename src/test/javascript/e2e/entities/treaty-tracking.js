'use strict';

describe('TreatyTracking e2e test', function () {

  var searchParameter=element(by.model('vm.searchQuery'));

    beforeAll(function () {
      browser.get('#/');
     //
      browser.driver.sleep(1000);
    });

    it('should load Home', function () {

      element(by.css('[ui-sref="deal"]')).click().then(function() {
        expect(element.all(by.css('h3')).first().getText()).toMatch(/Deals/);
      });
    });

    it('Search a Deal', function () {

      searchParameter.clear();
      searchParameter.sendKeys("Test");

      element(by.css('[ng-click="vm.search(vm.searchQuery)"]')).click();
      expect(element(by.exactRepeater('deal in vm.deals')).isPresent()).toBe(true);

      var dealName = element.all(by.repeater('deal in vm.deals').column('deal.name}'));

      element(by.exactRepeater('deal in vm.deals')).isPresent().then(function(result) {
        if (result ) {
          element(by.repeater('deal in vm.deals').row(0)).click();
        }else{
          searchParameter.clear();
          element(by.css('[ng-click="vm.search(vm.searchQuery)"]')).click();
          element(by.repeater('deal in vm.deals').row(0)).click();
        }
      });
    });

    it('should load treaties and Tracking home', function () {
      element(by.css('[ui-sref="treaty-deal({dealId:vm.dealId, page:vm.page})"]')).click();
      expect(element(by.exactRepeater('treaty in vm.treaties')).isPresent()).toBe(true);
      element(by.repeater('treaty in vm.treaties').row(0)).click();

    });

    it('should load TreatyTrackings', function () {
      element(by.css('[ui-sref="treaty-tracking-all({treatyId:vm.treatyId, dealId:vm.dealId})"]')).click();
    });

    it('should load create TreatyTracking dialog', function () {
      element(by.css('[ui-sref="treaty-tracking-all.new({dealId:vm.dealId,treatyId:vm.treatyId})"]')).click();
      expect(element(by.css('h4.modal-title')).getText()).toMatch(/Create or Edit a Treaty Tracking Record/);
    });

    it('should load create TreatyTracking dialog and Enter values', function () {


      var today = new Date();
      //var day = today.getDate();
      var month = today.getMonth()+1; //By default January count as 0
      var year = today.getFullYear();

      if(month<10) {
        month='0'+month
      }
      var yearmonth= year+'-'+month;
      element(by.model('vm.treatyTracking.reportingPeriod')).all(by.tagName('option')).get(1).click();
      element(by.model('vm.treatyTracking.reportingMonthYear')).clear();
      element(by.model('vm.treatyTracking.reportingMonthYear')).sendKeys(yearmonth);
      browser.sleep(1000);
      element(by.model('vm.treatyTracking.comments')).sendKeys("Created by Auto Testing");




      expect(element(by.css('button[type=submit]')).isPresent()).toBe(true);
      element(by.css('button[type=submit]')).click();
      //browser.pause();
    });



    afterAll(function () {

    });
});
