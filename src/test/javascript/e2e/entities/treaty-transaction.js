'use strict';

describe('TreatyTransaction e2e test', function () {
  var searchParameter=element(by.model('vm.searchQuery'));
  beforeAll(function () {
    browser.get('#/');
    browser.driver.sleep(1000);
  });

    it('should load Home', function () {

      element(by.css('[ui-sref="deal"]')).click().then(function() {
        expect(element.all(by.css('h3')).first().getText()).toMatch(/Deals/);
      });
    });

    it('Search a Deal', function () {

      searchParameter.clear();
      searchParameter.sendKeys("Test");

      element(by.css('[ng-click="vm.search(vm.searchQuery)"]')).click();
      expect(element(by.exactRepeater('deal in vm.deals')).isPresent()).toBe(true);

      element(by.exactRepeater('deal in vm.deals')).isPresent().then(function(result) {
        if (result ) {
          element(by.repeater('deal in vm.deals').row(0)).click();
        }else{
          searchParameter.clear();
          element(by.css('[ng-click="vm.search(vm.searchQuery)"]')).click();
          element(by.repeater('deal in vm.deals').row(0)).click();
        }
      });
    });

    it('should load treaties and Tracking home', function () {
      element(by.css('[ui-sref="treaty-deal({dealId:vm.dealId, page:vm.page})"]')).click();
      expect(element(by.exactRepeater('treaty in vm.treaties')).isPresent()).toBe(true);
      element(by.repeater('treaty in vm.treaties').row(0)).click();

    });

    it('should load Cash Transactions', function () {
      expect(element(by.css('[ui-sref="treaty-transaction-cash({id:vm.treatyId, dealId:vm.dealId})"]')).isPresent()).toBe(true);
      element(by.css('[ui-sref="treaty-transaction-cash({id:vm.treatyId, dealId:vm.dealId})"]')).isPresent().then(function(result){
        if(result){
          element(by.css('[ui-sref="treaty-transaction-cash({id:vm.treatyId, dealId:vm.dealId})"]')).click();
          element(by.css('[ng-click="vm.mainTransactionAdd()"]')).click();
          element(by.model('mainTransaction.transactionStatus')).all(by.tagName('option')).get(1).click();
          var today = new Date();
          var day = today.getDate();
          var month = today.getMonth()+1; //By default January count as 0
          var year = today.getFullYear();

          if(month<10) {
            month='0'+month
          }
          var yearmonthDay= year+''+month+''+day;
          element(by.model('mainTransaction.settlementDate')).clear();
          element(by.model('mainTransaction.settlementDate')).sendKeys(yearmonthDay);
          var yearmonth= year+''+month;
          element(by.model('mainTransaction.accountingDate')).clear();
          element(by.model('mainTransaction.accountingDate')).sendKeys(yearmonth);
          element(by.model('mainTransaction.settlementType')).all(by.tagName('option')).get(1).click();
          element(by.model('mainTransaction.fundingType')).all(by.tagName('option')).get(1).click();
          element(by.model('mainTransaction.currency')).all(by.tagName('option')).get(1).click();
          element(by.model('mainTransaction.netTotal')).clear();
          element(by.model('mainTransaction.netTotal')).sendKeys('100.00');

          element(by.model('transaction.allocationType')).all(by.tagName('option')).get(2).click();
          element(by.model('transaction.reportingDate')).clear();
          element(by.model('transaction.reportingDate')).sendKeys(yearmonth);
          element(by.model('transaction.company')).all(by.tagName('option')).get(1).click();
          element(by.model('transaction.amount')).clear();
          element(by.model('transaction.amount')).sendKeys('100.00');

          browser.pause();

        }
      });


    });

    afterAll(function () {

    });
});
