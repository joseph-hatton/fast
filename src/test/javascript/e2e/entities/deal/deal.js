'use strict';

describe('Deal e2e test', function () {
    var name = element(by.name('name'));
    var effectvieDate = element(by.model('vm.deal.effectiveDate'));
    var Clientname = element(by.name('Clientname'));
    var subtype = element(by.name('subtype'));
    var searchParameter=element(by.model('vm.searchQuery'));
    var dealTestName='Test Deal Auto';


    beforeAll(function () {
      browser.get('#/deal');
      browser.driver.wait(protractor.until.elementIsVisible(element(by.css('h3'))));
    });


    it('should load Home', function () {
        element(by.css('[ui-sref="deal"]')).click().then(function() {
        expect(element.all(by.css('h3')).first().getText()).toMatch(/Deals/);
      });
    });



    it('Click to create a New Deal', function () {
        element(by.css('[ui-sref="deal.new"]')).click().then(function() {
        expect(browser.getLocationAbsUrl())
          .toBe("/deal/new");
      });
        expect(element(by.css('h4.modal-title')).getText()).toMatch("Create or Edit a Deal");
    });


    it('Enter the Details for creating a New Deal', function () {
        expect(element(by.css('h4.modal-title')).getText()).toMatch("Create or Edit a Deal");

        var today = new Date();
        var day = today.getDate();
        var month = today.getMonth()+1; //By default January count as 0
        var year = today.getFullYear();
        if(day<10) {
          day='0'+day
        }
        if(month<10) {
          month='0'+month
        }
        today= year+''+month+''+day;
        var num=Math.floor((Math.random() * 100) + 1);
        //var dealName='Test Deal Auto'+' '+day;
        name.sendKeys(dealTestName+' '+num);
        element(by.css('option[value="FinRe"]')).click();
        element(by.css('option[value="Brokered"]')).click();
        element(by.css('option[value="2"]')).click();
        element(by.css('option[value="number:6"]')).click();
        element(by.css('option[value="number:16"]')).click();


        effectvieDate.clear();  // Note if you are facing error message related to clear. Comment this line
        effectvieDate.sendKeys(today);

      expect(element(by.css('button[type=submit]')).isEnabled()).toBe(false);
      var dealSaveButton =  element(by.css('button[type=submit]'));
      dealSaveButton.isEnabled().then(function(result) {
        if (result ) {

          element(by.css('button[type=submit]')).click();
        }else{
          element(by.css('[ng-click="vm.clear()"]')).click();
        }

      });
    });

    it('Search a Deal', function () {

      element(by.id('btnSearch')).click();
      searchParameter.clear();
      searchParameter.sendKeys(dealTestName);

      element(by.css('[ng-click="vm.search(vm.searchQuery)"]')).click();//working

      expect(element(by.exactRepeater('deal in vm.deals')).isPresent()).toBe(true);
      var dealName = element.all(by.repeater('deal in vm.deals').column('deal.name}'));
      element(by.repeater('deal in vm.deals').row(0)).click();
    });

    it('Edit a Deal', function () {

      element(by.css('[ui-sref="deal-detail.editDetail({dealId:vm.deal.id})"]')).click();
      expect(element(by.css('h4.modal-title')).getText()).toMatch("Create or Edit a Deal");

      expect(element(by.model('vm.deal.name')).isEnabled()).toBe(true);
      //expect(element(by.model('vm.deal.terminatedDate')).getAttribute('ng-readonly')).toEqual('true');
      expect(element(by.css('[ng-click="vm.openCalendar(terminatedDate)"]')).isPresent()).toBe(false);
      element(by.css('[ng-click="vm.clear()"]')).click();//cancel on edit screen
      element(by.css('[ui-sref="deal({page:vm.page})"]')).click();//back to the Deal List

      //browser.pause();
    });
});
