'use strict';

describe('Contact e2e test', function () {



    beforeAll(function () {
        browser.get('#/contact');
        browser.driver.wait(protractor.until.elementIsVisible(element(by.css('h3'))));

    });

    it('should load Contacts', function () {
      element(by.css('[ui-sref="contact"]')).click().then(function() {
            expect(element.all(by.css('h3')).first().getText()).toMatch(/Contacts/);
        });
    });

    it('should load create Contact dialog', function () {
        element(by.css('[ui-sref="contact.new"]')).click().then(function() {
            expect(element(by.css('h4')).getText()).toMatch(/Create or Edit Contact Information/);

        });
    });

    it('should accept the input values Contact dialog', function () {
      var num=Math.floor((Math.random() * 100) + 1);

      //contact Info
      element(by.model('vm.contact.firstName')).sendKeys("Test Kiya"+' '+num);
      element(by.model('vm.contact.lastName')).sendKeys("Lee");
      element(by.model('vm.contact.international')).click();

      element(by.model('vm.contact.phoneNumber')).sendKeys("2345678765");
      element(by.model('vm.contact.mobileNumber')).sendKeys("7202345678");
      element(by.model('vm.contact.email')).sendKeys("test@kiyaTest.com");

      //Address
      element(by.css('option[value="United States"]')).click();
      element(by.model('vm.contact.address1')).sendKeys("101 North ST.");
      element(by.model('vm.contact.city')).sendKeys("Chesterfield");
      element(by.css('option[value="MO"]')).click();
      element(by.model('vm.contact.postalCode')).sendKeys("63017");
      expect(element(by.css('button[type=submit]')).isEnabled()).toBe(true);
      element(by.css('button[type=submit]')).click();
     // browser.pause();
    });
    afterAll(function () {

    });
});
