'use strict';

describe('Company e2e test', function () {
  var companyNameData='Test Auto Company';
  var companyName = element(by.model('vm.company.name'));
  var city = element(by.model('vm.company.city'));
  var postalCode = element(by.model('vm.company.postalCode'));
  var searchParameter=element(by.model('vm.searchQuery'));


    beforeAll(function () {
      browser.get('#/company/mdm/company-mdm');
      browser.driver.wait(protractor.until.elementIsVisible(element(by.css('h3'))));
    });

    it('should load Companies', function () {

        element(by.css('[ui-sref="company-mdm"]')).click().then(function() {
            expect(element.all(by.css('h3')).first().getText()).toMatch(/Companies/);
        });

    });

    it('should match the browser URL', function () {
      expect(browser.getLocationAbsUrl()).toBe("/company/mdm/company-mdm");
    });

    it('should match the MDM and Non MDM LINKS', function () {
      expect(element(by.css('[ui-sref="company-mdm"]')).isPresent()).toBe(true);
      expect(element(by.css('[ui-sref="company"]')).isPresent()).toBe(true);
    });

    it('should load create Company dialog', function () {

      element(by.css('[ui-sref="company"]')).click().then(function() {
        expect(element(by.css('h3')).getText()).toMatch(/Companies/);

            //element(by.css('button.close')).click();
        element(by.css('[ui-sref="company.new"]')).click().then(function() {
          expect(browser.getLocationAbsUrl())
            .toBe("/company/non-mdm/new");
        });

        });
    });

    it('should Company dialog popup but Save button is disabled', function () {
      var companySaveButton =  element(by.css('button[type=submit]'));
      companySaveButton.isEnabled().then(function(result) {
        if (result ) {
          element(by.css('button[type=submit]')).click();
        }else{
          element(by.css('[ng-click="vm.clear()"]')).click();
        }});

    });

    it('should Company dialog popup and Save button is enabled', function () {
      element(by.css('[ui-sref="company.new"]')).click();
      var num=Math.floor((Math.random() * 100) + 1);
      companyName.sendKeys(companyNameData+''+num);
      element(by.css('option[value="United States"]')).click();
      city.sendKeys("Des Moines");
      element(by.css('option[value="Iowa"]')).click();
      postalCode.sendKeys("50266");
      var companySaveButton =  element(by.css('button[type=submit]'));
      companySaveButton.isEnabled().then(function(result) {
        if (result ) {
          element(by.css('button[type=submit]')).click();
        }else{
          element(by.css('[ng-click="vm.clear()"]')).click();
        }});
     // browser.switchTo().alert().accept();
      //browser.pause();
    });

    it('Search a Company', function () {


      searchParameter.clear();
      searchParameter.sendKeys("Test");

      element(by.css('[ng-click="vm.search(vm.searchQuery)"]')).click();//working


      expect(element(by.exactRepeater('company in vm.companies')).isPresent()).toBe(true);
      var dealName = element.all(by.repeater('deal in vm.deals').column('deal.name}'));

      element(by.repeater('company in vm.companies').row(0)).click();

    });
    afterAll(function () {

    });
});
