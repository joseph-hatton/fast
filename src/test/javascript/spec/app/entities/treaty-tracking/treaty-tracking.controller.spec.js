'use strict';

describe('Controller Tests', function () {

  describe('TreatyTracking Controller', function () {
    var Ctrl;
    var scope, state, paginationConstants, pagingParams, MockTreatyTracking, MockEntity, MockTreaty, MockTreatyTransactionSrv, MockInteractionsService, MockTreatyTrackingCustomSvr, MockParseLinks;

    // Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) { // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state = $rootScope.$state;
        MockEntity = {};
        pagingParams = {
          ascending: true,
          page: 1,
          predicate: "id",
          search: null,
          sort: "id,asc"
        };
        paginationConstants = {
          itemsPerPage: 10
        };
        MockTreatyTracking = jasmine.createSpyObj('TreatyTracking', ['save', 'update']);
        MockTreatyTransactionSrv = {
          subTransactionTracking: jasmine.createSpyObj('subTransactionTracking', ['get'])
        };
        MockTreatyTrackingCustomSvr = {
          generateNextTracking: jasmine.createSpyObj('generateNextTracking', ['nextTrackingRecord']),
          byDates: jasmine.createSpyObj('byDates', ['get'])
        };
        MockTreaty = jasmine.createSpyObj('Treaty', ['get']);
        MockInteractionsService = jasmine.createSpyObj('InteractionsService', ['alertParentScrollFalseDialog', 'showThenDialog', 'confirmBasicParentNoScrollDialog']);
        MockParseLinks = jasmine.createSpyObj('ParseLinks', ['parse']);
        Ctrl = $controller('TreatyTrackingController', {
          $scope: scope,
          $state: state,
          paginationConstants: paginationConstants,
          pagingParams: pagingParams,
          TreatyTracking: MockTreatyTracking,
          Entity: MockEntity,
          Treaty: MockTreaty,
          TreatyTransactionSrv: MockTreatyTransactionSrv,
          InteractionsService: MockInteractionsService,
          TreatyTrackingCustomSvr: MockTreatyTrackingCustomSvr,
          ParseLinks: MockParseLinks
        });
      }));

    describe('Initial state', function () {
      it('TreatyTracking Controller', function () {
        expect(Ctrl).toBeDefined();
      });
      it('TreatyTracking Load All Test', function () {
        expect(Ctrl.loadAll).toBeDefined();
      });

    });
    describe('Main Content', function () {
      describe('Clear', function () {
        it('should reset some properties and call vm.transition', function () {
          Ctrl.currentSearch = 'test';
          Ctrl.transition = jasmine.createSpy('transition');

          Ctrl.clear();

          expect(Ctrl.currentSearch).toBeNull();
          expect(Ctrl.transition).toHaveBeenCalled();
        });
      });
      describe('getTreatyInformation', function () {
        it('should call Treaty.get', function () {
          Ctrl.getTreatyInformation();
          expect(MockTreaty.get).toHaveBeenCalled();
        });
        describe('inside callback', function () {
          var result;
          beforeEach(function () {
            result = {};
            MockTreaty.get.and.callFake(function (obj, callback) {
              callback(result);
            });
            Ctrl.checkFrequency = jasmine.createSpy('checkFrequency');
          });
          it('should set reporting due date flag to false if reporting frequency is set, reporting due date is set, and stmntDueAsmGracePeriod is set', function () {
            result.reportingFrequency = {};
            result.reportingDueDate = {
              dayType: '',
              period: '',
              numberOfDays: ''
            };
            result.stmntDueAsmGracePeriod = {
              dayType: '',
              period: '',
              numberOfDays: ''
            };

            Ctrl.getTreatyInformation();

            expect(Ctrl.flagReportingDueDate).toBe(true);
          });
          it('should set reporting due date flag to false if reporting frequency is not set, reporting due date is set, and stmntDueAsmGracePeriod is set', function () {
            result.reportingFrequency = null;
            result.reportingDueDate = {
              dayType: '',
              period: '',
              numberOfDays: ''
            };
            result.stmntDueAsmGracePeriod = {
              dayType: '',
              period: '',
              numberOfDays: ''
            };

            Ctrl.getTreatyInformation();

            expect(Ctrl.flagReportingDueDate).toBe(false);
          });
          it('should set reporting due date flag to false if reporting frequency is set, reporting due date is not set, and stmntDueAsmGracePeriod is set', function () {
            result.reportingFrequency = {};
            result.reportingDueDate = null;
            result.stmntDueAsmGracePeriod = {
              dayType: '',
              period: '',
              numberOfDays: ''
            };

            Ctrl.getTreatyInformation();

            expect(Ctrl.flagReportingDueDate).toBe(false);
          });
          it('should set reporting due date flag to false if reporting frequency is set, reporting due date is set, and stmntDueAsmGracePeriod is not set', function () {
            result.reportingFrequency = {};
            result.reportingDueDate = {
              dayType: '',
              period: '',
              numberOfDays: ''
            };
            result.stmntDueAsmGracePeriod = null;

            Ctrl.getTreatyInformation();

            expect(Ctrl.flagReportingDueDate).toBe(false);
          });
          it('should set invalidTreatyMsg', function () {
            result.effectiveDate = null;
            result.reportingFrequency = null;
            Ctrl.invalidTreatyMsg = null;

            Ctrl.getTreatyInformation();

            expect(Ctrl.invalidTreatyMsg).not.toBe('');
          });
          it('should set invalidTreatyMsg', function () {
            result.effectiveDate = {};
            result.reportingFrequency = null;
            Ctrl.invalidTreatyMsg = null;

            Ctrl.getTreatyInformation();

            expect(Ctrl.invalidTreatyMsg).not.toBe('');
          });
          it('should set invalidTreatyMsg', function () {
            result.effectiveDate = null;
            result.reportingFrequency = {};
            Ctrl.invalidTreatyMsg = null;

            Ctrl.getTreatyInformation();

            expect(Ctrl.invalidTreatyMsg).not.toBe('');
          });
          it('should set invalidTreatyMsg', function () {
            result.effectiveDate = {};
            result.reportingFrequency = {};
            Ctrl.invalidTreatyMsg = null;

            Ctrl.getTreatyInformation();

            expect(Ctrl.invalidTreatyMsg).toBe('');
          });
          it('should set invalidTreaty to true', function () {
            result.effectiveDate = null;
            result.reportingFrequency = null;

            Ctrl.getTreatyInformation();

            expect(Ctrl.invalidTreaty).toBe(true);
          });
          it('should set invalidTreaty to false', function () {
            result.effectiveDate = {};
            result.reportingFrequency = null;

            Ctrl.getTreatyInformation();

            expect(Ctrl.invalidTreaty).toBe(true);
          });
          it('should set invalidTreaty to false', function () {
            result.effectiveDate = null;
            result.reportingFrequency = {};

            Ctrl.getTreatyInformation();

            expect(Ctrl.invalidTreaty).toBe(true);
          });
          it('should set invalidTreaty to false', function () {
            result.effectiveDate = {};
            result.reportingFrequency = {};

            Ctrl.getTreatyInformation();

            expect(Ctrl.invalidTreaty).toBe(false);
          });
          it('should call checkFrequency when reportingFrequency is set', function () {
            result.reportingFrequency = {};

            Ctrl.getTreatyInformation();

            expect(Ctrl.checkFrequency).toHaveBeenCalled();
          });
          it('should call checkFrequency when reportingFrequency is set', function () {
            result.reportingFrequency = null;

            Ctrl.getTreatyInformation();

            expect(Ctrl.checkFrequency).not.toHaveBeenCalled();
          });
        });
      });
      describe('checkFrequency', function () {
        beforeEach(function () {
          Ctrl.treaty = {
            reportingFrequency: {}
          };
        });
        it('should set TrackingFrequency to 4 quarters when monthly', function () {
          Ctrl.treaty.reportingFrequency.name = 'Monthly';

          Ctrl.checkFrequency({
            entityDates: [{
              entityType: "reportingdate"
            }]
          });

          var expected = [{
              frequency: 1,
              name: "1st Quarter"
            },
            {
              frequency: 2,
              name: "2nd Quarter"
            },
            {
              frequency: 3,
              name: "3rd Quarter"
            },
            {
              frequency: 4,
              name: "4th Quarter"
            }
          ];

          expected.forEach(function (item, key) {
            expect(Ctrl.trackingfrequency[key]).toEqual(item);
          });
        });
        it('should set TrackingFrequency to 4 quarters when quarterly', function () {
          Ctrl.treaty.reportingFrequency.name = 'Quarterly';

          Ctrl.checkFrequency({
            entityDates: [{
              entityType: "reportingdate"
            }]
          });

          var expected = [{
              frequency: 1,
              name: "1st Quarter"
            },
            {
              frequency: 2,
              name: "2nd Quarter"
            },
            {
              frequency: 3,
              name: "3rd Quarter"
            },
            {
              frequency: 4,
              name: "4th Quarter"
            }
          ];

          expected.forEach(function (item, key) {
            expect(Ctrl.trackingfrequency[key]).toEqual(item);
          });
        });
        it('should set TrackingFrequency to 2 halves when semi-annual', function () {
          Ctrl.treaty.reportingFrequency.name = 'Semi-Annual';

          Ctrl.checkFrequency({
            entityDates: [{
              entityType: "reportingdate"
            }]
          });

          var expected = [{
              frequency: 5,
              name: "1st Half"
            },
            {
              frequency: 11,
              name: "Last Half"
            }
          ];

          expected.forEach(function (item, key) {
            expect(Ctrl.trackingfrequency[key]).toEqual(item);
          });
        });
        it('should set TrackingFrequency to 1 when annual', function () {
          Ctrl.treaty.reportingFrequency.name = 'Annual';

          Ctrl.checkFrequency({
            entityDates: [{
              entityType: "reportingdate"
            }]
          });

          var expected = [{
            frequency: 11,
            name: "Year End"
          }];

          expected.forEach(function (item, key) {
            expect(Ctrl.trackingfrequency[key]).toEqual(item);
          });
        });
        it('should set TrackingFrequency to 1 when annual', function () {
          Ctrl.treaty.reportingFrequency.name = null;

          Ctrl.checkFrequency({
            entityDates: [{
              entityType: "reportingdate"
            }]
          });

          var expected = [{
            frequency: 0,
            name: "No Information available"
          }];

          expected.forEach(function (item, key) {
            expect(Ctrl.trackingfrequency[key]).toEqual(item);
          });
        });
        it('should not set TrackingFrequency when entityType isn\'t reportingdate', function () {
          Ctrl.treaty.reportingFrequency.name = null;

          Ctrl.checkFrequency({
            entityDates: [{
              entityType: ""
            }]
          });

          expect(Ctrl.trackingfrequency.length).toBe(0);
        });
      });
      describe('forDelete', function () {
        var result;
        beforeEach(function () {
          MockTreatyTransactionSrv.subTransactionTracking.get.and.callFake(function (obj, callback) {
            callback(result);
          });
        });
        it('Call showThenDialog when there are subtransactions and checked to be deleted', function () {
          result = [{}];

          Ctrl.forDelete({
            checked: true
          });

          expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
        });
        it('Add tracking onto delete list if no subtransactions exist', function () {
          result = [];
          Ctrl.deleteMultipleTracking = [];

          Ctrl.forDelete({
            checked: true
          });

          expect(Ctrl.deleteMultipleTracking.length).toBe(1);
        });
        it('Add tracking onto delete list if no subtransactions exist', function () {
          result = [];
          Ctrl.deleteMultipleTracking = [{
            id: 123
          }];

          Ctrl.forDelete({
            id: 123,
            checked: false
          });

          expect(Ctrl.deleteMultipleTracking.length).toBe(0);
        });
      });
      describe('deleteMultiple', function () {
        it('should remove the item from the main list and set the isActive flag to 0 and update', function () {
          MockInteractionsService.showThenDialog.and.callFake(function (dialog, success) {
            success();
          });
          var tracking = {
            id: 123,
            isActive: 1
          };
          Ctrl.treatyTrackings = [tracking];
          Ctrl.deleteMultipleTracking = [tracking];

          Ctrl.deleteMultiple();

          expect(Ctrl.treatyTrackings.length).toBe(0);
          expect(Ctrl.treatyTrackings.length).toBe(0);
          expect(MockTreatyTracking.update).toHaveBeenCalled();
        });
      });
      describe('deleteFlagChange', function () {
        it('should clear the selected items when set to false', function () {
          Ctrl.treatyTrackings = [{
              checked: true
            },
            {
              checked: true
            },
            {
              checked: true
            }
          ];
          Ctrl.deleteMultipleTracking = [{
              checked: true
            },
            {
              checked: true
            },
            {
              checked: true
            }
          ];
          Ctrl.deleteFlag = false;

          Ctrl.deleteFlagChange();

          for (var i = 0; i < Ctrl.treatyTrackings.length; i++)
            expect(Ctrl.treatyTrackings[i].checked).toBe(false);
          expect(Ctrl.deleteMultipleTracking.length).toBe(0);
        });
      });
      describe('closeTracking', function () {
        it('should update tracking to closed', function () {
          var tracking = {
            trackingStatus: "Open"
          };

          Ctrl.closeTracking(tracking);

          expect(tracking.trackingStatus).toBe('Closed');
          expect(MockTreatyTracking.update).toHaveBeenCalled();
        });
        it('should ask to generate next record and call save with new record on yes', function () {
          var tracking = {
            trackingStatus: "Open"
          };
          MockTreatyTracking.update.and.callFake(function (obj, callback) {
            callback({});
          });
          MockInteractionsService.showThenDialog.and.callFake(function (dialog, yes) {
            yes();
          });
          Ctrl.clear = jasmine.createSpy('clear');

          Ctrl.closeTracking(tracking);

          expect(MockTreatyTracking.save).toHaveBeenCalled();
          expect(MockTreatyTrackingCustomSvr.generateNextTracking.nextTrackingRecord).toHaveBeenCalled();
        });
      });
      describe('onSuccess', function () {
        it('should setup treatyTrackings info and save to treatyTrackins', function () {
          var data = [{}];
          calculateQuarters = jasmine.createSpy('calculateQuarters').and.returnValue(1);
          calculateMonth = jasmine.createSpy('calculateMonth').and.returnValue(2);

          Ctrl.onSuccess(data, jasmine.createSpy('headers'));

          expect(Ctrl.treatyTrackings.length).toBe(1);
          expect(Ctrl.treatyTrackings[0].quarter).toBe(1);
          expect(Ctrl.treatyTrackings[0].month).toBe(2);
        });
      });
      describe('Reset', function () {
        it('should reset filter dates and call loadAll', function () {
          Ctrl.searchStartDate = 'test';
          Ctrl.searchEndDate = 'test';
          Ctrl.loadAll = jasmine.createSpy('loadAll');

          Ctrl.reset();

          expect(Ctrl.searchStartDate).toBeNull();
          expect(Ctrl.searchEndDate).toBeNull();
          expect(Ctrl.loadAll).toHaveBeenCalled();
        });
      });
      describe('searchDates', function () {
        it('should call TreatyTrackingCustomSvr.byDates.get and set predicate and reverse', function () {
          Ctrl.predicate = null;

          Ctrl.searchDates();

          expect(MockTreatyTrackingCustomSvr.byDates.get).toHaveBeenCalled();
          expect(Ctrl.predicate).toBe('reportingMonthYear');
          expect(Ctrl.reverse).toBe(false);
        });
        it('should call TreatyTrackingCustomSvr.byDates.get and set predicate and reverse', function () {
          Ctrl.predicate = 'id,asc';

          Ctrl.searchDates();

          expect(MockTreatyTrackingCustomSvr.byDates.get).toHaveBeenCalled();
          expect(Ctrl.predicate).toBe('reportingMonthYear');
          expect(Ctrl.reverse).toBe(false);
        });
        it('should call TreatyTrackingCustomSvr.byDates.get and not set predicate and reverse', function () {
          Ctrl.predicate = 'notId';
          pagingParams.sort = 'notId';

          Ctrl.searchDates();

          expect(MockTreatyTrackingCustomSvr.byDates.get).toHaveBeenCalled();
          expect(Ctrl.predicate).toBe('notId');
        });
      });
      describe('linkToEditPage', function () {
        it('should call vm.go if disabled tracking is off', function () {
          Ctrl.go = jasmine.createSpy('go');
          Ctrl.treaty = {
            disabledTracking: 0
          };

          Ctrl.linkToEditPage();

          expect(Ctrl.go).toHaveBeenCalled();
        });
        it('should not call vm.go if disabled tracking is on', function () {
          Ctrl.go = jasmine.createSpy('go');
          Ctrl.treaty = {
            disabledTracking: 1
          };

          Ctrl.linkToEditPage();

          expect(Ctrl.go).not.toHaveBeenCalled();
        });
      });
    });
  });
});
