'use strict';

describe('Controller Tests', function () {

  describe('TreatyTracking Management Dialog Controller', function () {
    var $scope, $rootScope,defer;
    var MockEntity, MockTreatyTracking, MockTreaty, DeferFactory, MockGfsAssignee, MockPrincipal, MockInteractionsService,xrefTreatyCompaniesQueryResults;
    var Ctrl, MockDealTrackingFilterSrv, MockDealTrackingFilterItem, MockUibModalInstance, MockTreatyTrackingCustomSvr,MockXrefTreatyCustomCompany;

    beforeEach(inject(function ($injector, $controller, _DeferFactory_, $q) {
      DeferFactory = _DeferFactory_;
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      MockEntity = { treaty:{
        seriatimTypeTreaties:[],
        reportingDueDate:{},
        reportingDueYearEx:{},
        stmntDueCedGracePeriod:{},
        stmntDueCed: {},
        stmntDueAsmGracePeriod:{},
        stmntDueAsmBroker:{},
        deal: {}
      } };
      MockTreatyTrackingCustomSvr = {
        lasReportingMonthYear: jasmine.createSpyObj('lasReportingMonthYear', ['query']),
        calculateReportingDue: jasmine.createSpy('calculateReportingDue')
      };
      MockTreatyTracking = jasmine.createSpyObj('MockTreatyTracking', ['save', 'update']);
      MockUibModalInstance = jasmine.createSpy('MockUibModalInstance');
      MockDealTrackingFilterItem = [];
      MockDealTrackingFilterSrv = {
        reportingReportTracking: {
          get: function (obj, callback) {
            callback(MockDealTrackingFilterItem);
          }
        }
      };
      MockTreaty = jasmine.createSpyObj('Treaty', ['get']);
      MockGfsAssignee = jasmine.createSpyObj('GfsAssignee', ['query']);
      MockPrincipal = jasmine.createSpyObj('Principal', ['identity']);

      MockXrefTreatyCustomCompany = {
        treaties:jasmine.createSpyObj('treaties', ['query'])
      };

      defer= DeferFactory.generate();
      MockXrefTreatyCustomCompany.treaties.query.and.callFake(
        function(){
          return defer});


      MockPrincipal.identity.and.returnValue(DeferFactory.generate().promise);

      calculatingDate = jasmine.createSpy('calculatingDate')
      .and.returnValue({});
      MockInteractionsService = jasmine.createSpyObj('InteractionsService', ['confirmBasicParentNoScrollDialog', 'showThenDialog', 'alertParentScrollFalseDialog']);

      Ctrl = $controller("TreatyTrackingDialogController", {
        '$scope': $scope,
        '$uibModalInstance': MockUibModalInstance,
        'entity': MockEntity,
        'TreatyTracking': MockTreatyTracking,
        'DealTrackingFilterSrv': MockDealTrackingFilterSrv,
        'TreatyTrackingCustomSvr': MockTreatyTrackingCustomSvr,
        'Treaty': MockTreaty,
        '$q': $q,
        'GfsAssignee': MockGfsAssignee,
        'Principal': MockPrincipal,
        'InteractionsService': MockInteractionsService,
        'XrefTreatyCustomCompany': MockXrefTreatyCustomCompany
      });


      xrefTreatyCompaniesQueryResults = [{companyType:'Ceding ', company:{isRgaCompany:1}}];
      defer.resolve(xrefTreatyCompaniesQueryResults);
      $rootScope.$apply();
    }));


    describe('Init State', function () {
      it('Ctrl should be defined', function () {
        expect(Ctrl).toBeDefined();
        Ctrl.treatyTracking.cashDueFlag='Yes';
      });
    });
    describe('Main Content', function () {
      describe('reportingMonthYearChange', function () {
        it('should call reportingPeriodEndDealTracking', function () {
          Ctrl.reportingPeriodEndDealTracking = jasmine.createSpy('reportingPeriodEndDealTracking');
          Ctrl.reportingMonthYearChange();
          expect(Ctrl.reportingPeriodEndDealTracking).toHaveBeenCalled();
        });
      });

      describe('MockXrefTreatyCustomCompany.treaties.query', function () {
        it('should set up the vm.reportingConfirmDateFlag', function () {

          expect(Ctrl.reportingConfirmDateFlag).toBeDefined();
        });
      });
      describe('reportingPeriodEndDealTracking', function () {
        it('should set the correct data if a matching report is found', function () {
          var date = new Date("2017-01-15");
          MockDealTrackingFilterItem = [{
            password: "password"
          }];
          Ctrl.reportingPeriodEndDealTracking(date);
          expect(Ctrl.password).toBe("password");
        });
        it('should set the correct data to default if a matching report is not found', function () {
          var date = new Date("2017-01-15");
          Ctrl.reportingPeriodEndDealTracking(date);
          expect(Ctrl.password).not.toBe("password");
        });
      });
      describe('load', function () {
        beforeEach(function () {
          Ctrl.treaty = {
            stmntDueAsmGracePeriod:{period:''},
            stmntDueCedGracePeriod:{period:''},
            reportingDueDate:{}
          };
        });
        it('should get the treaty if it isn\'t set', function () {
          Ctrl.treaty = null;
          MockTreaty.get.and.callFake(function () {
            return DeferFactory.generate();
          });
          Ctrl.load();
          expect(MockTreaty.get).toHaveBeenCalled();
        });
        it('should not get the treaty if it is set', function () {
          Ctrl.load();
          expect(MockTreaty.get).not.toHaveBeenCalled();
        });
        it('should set the currencyOptions from the treaty currencies', function () {
        Ctrl.treaty.treatyCurrencies = [ {currency:'curr'}, {currency:'curr'}, {currency:'curr'} ];
          Ctrl.load();
         $rootScope.$apply();
        for(var i = 0; i < Ctrl.treaty.treatyCurrencies.length; i++)
          expect(Ctrl.currencyOptions[i]).toBe('curr');
       });
        it('should set seriatimDueDate if one is selected', function () {
          Ctrl.treaty.seriatimTypeTreaties = [
            {seriatimDueDateFlag:'Yes', calculationFlag:true, seriatimFreq:1, seriatimDueDate: {}},
            {seriatimDueDateFlag:'No', calculationFlag:true, seriatimFreq:2, seriatimDueDate: {}}
          ];

          Ctrl.load();
          $rootScope.$apply();

          expect(Ctrl.seriatimDueDate.frequency).toBe(1);
        });

        describe('should set calculatedReportingDueYex', function () {
          it('when treaty.reportingDueYearEx is null', function () {
            Ctrl.treaty.reportingDueYearEx = null;
            Ctrl.treaty.reportingFrequency = {};
            Ctrl.treatyTracking.reportingMonthYear = {};
            Ctrl.treatyTracking.calculatedReportingDueYex = 'test';
            Ctrl.load();
            $rootScope.$apply();
            expect(Ctrl.treatyTracking.calculatedReportingDueYex).toBeNull();
          });
          it('when treaty.reportingDueYearEx.period is null', function () {
            Ctrl.treaty.reportingDueYearEx = {};
            Ctrl.treaty.reportingDueYearEx.period = null;
            Ctrl.treaty.reportingFrequency = {};
            Ctrl.treatyTracking.reportingMonthYear = {};
            Ctrl.treatyTracking.calculatedReportingDueYex = 'test';
            Ctrl.load();
            $rootScope.$apply();
            expect(Ctrl.treatyTracking.calculatedReportingDueYex).toBeNull();
          });
          it('when treaty.reportingFrequency is null', function () {
            Ctrl.treaty.reportingDueYearEx = {};
            Ctrl.treaty.reportingDueYearEx.period = {};
            Ctrl.treaty.reportingFrequency = null;
            Ctrl.treatyTracking.reportingMonthYear = {};
            Ctrl.treatyTracking.calculatedReportingDueYex = 'test';
            Ctrl.load();
            $rootScope.$apply();
            expect(Ctrl.treatyTracking.calculatedReportingDueYex).toBeNull();
          });
          it('when treaty.reportingMonthYear is null', function () {
            Ctrl.treaty.reportingDueYearEx = {};
            Ctrl.treaty.reportingDueYearEx.period = {};
            Ctrl.treaty.reportingFrequency = {};
            Ctrl.treatyTracking.reportingMonthYear = null;
            Ctrl.treatyTracking.calculatedReportingDueYex = 'test';
            Ctrl.load();
            $rootScope.$apply();
            expect(Ctrl.treatyTracking.calculatedReportingDueYex).toBeNull();
          });
          it('when nothing is null', function () {
            Ctrl.treaty.reportingDueYearEx = {};
            Ctrl.treaty.reportingDueYearEx.period = {};
            Ctrl.treaty.reportingFrequency = {};
            Ctrl.treatyTracking.reportingMonthYear = {};
            Ctrl.treatyTracking.calculatedReportingDueYex = 'test';
            Ctrl.load();
            $rootScope.$apply();
            expect(Ctrl.treatyTracking.calculatedReportingDueYex).not.toBeNull();
          });
        });

        describe('should set calculatedCedingGraceDate', function () {
          it('when stmntDueCedGracePeriod is null', function () {

            Ctrl.treaty.stmntDueCedGracePeriod = null;
            Ctrl.treatyTracking.calculatedCedingGraceDate = '';
            Ctrl.load();
            $rootScope.$apply();
            expect(Ctrl.treatyTracking.calculatedCedingGraceDate.length).toBeGreaterThan(0);
          });
          it('when stmntDueCedGracePeriod.period is null', function () {
            Ctrl.treaty.stmntDueCedGracePeriod = {};
            Ctrl.treaty.stmntDueCedGracePeriod.period = null;
            Ctrl.treaty.stmntDueCedGracePeriod.dayType = {};
            Ctrl.treatyTracking.calculatedCedingGraceDate = '';
            Ctrl.load();
            $rootScope.$apply();
            expect(Ctrl.treatyTracking.calculatedCedingGraceDate.length).toBeGreaterThan(0);
          });
          it('when stmntDueCedGracePeriod.dayType is null', function () {
            Ctrl.treaty.stmntDueCedGracePeriod = {};
            Ctrl.treaty.stmntDueCedGracePeriod.period = {};
            Ctrl.treaty.stmntDueCedGracePeriod.dayType = null;
            Ctrl.treatyTracking.calculatedCedingGraceDate = '';
            Ctrl.load();
            $rootScope.$apply();
            expect(Ctrl.treatyTracking.calculatedCedingGraceDate.length).toBeGreaterThan(0);
          });
          it('when nothing is null', function () {
            Ctrl.treaty.stmntDueCedGracePeriod = {};
            Ctrl.treaty.stmntDueCedGracePeriod.period = {};
            Ctrl.treaty.stmntDueCedGracePeriod.dayType = {};
            Ctrl.treatyTracking.calculatedCedingGraceDate = '';
            Ctrl.load();
            $rootScope.$apply();
            expect(Ctrl.treatyTracking.calculatedCedingGraceDate.length).toBe(0);
          });
        });

        describe('stmntDueCedGracePeriod isn\'t null', function () {
          beforeEach(function () {
            Ctrl.treaty.stmntDueCedGracePeriod = {};
            Ctrl.treaty.stmntDueCedGracePeriod.period = {};
            Ctrl.treaty.stmntDueCedGracePeriod.dayType = {};
          });
          describe('when reportingReceivedDate is null', function () {
            beforeEach(function () {
              Ctrl.treatyTracking.reportingReceivedDate = null;
              Ctrl.treatyTracking.calculatedCedingGraceDate = '';
              Ctrl.treatyTracking.calculatedAssumingGraceDate = '';
            });
            it('should set calculatedCedingGraceDate when period is reporting recd', function () {
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.treaty.stmntDueCedGracePeriod.period = "Reporting Received";
              Ctrl.load();
              $rootScope.$apply();
              expect(Ctrl.treatyTracking.calculatedCedingGraceDate.length).toBeGreaterThan(0);
            });
            it('should not set calculatedCedingGraceDate when period is not reporting recd', function () {
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.treaty.stmntDueCedGracePeriod.period = "Not Reporting Received";
              Ctrl.load();
              $rootScope.$apply();
              expect(Ctrl.treatyTracking.calculatedCedingGraceDate.length).toBe(0);
            });
            it('should set calculatedAssumingGraceDate when period is reporting recd', function () {
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.treaty.stmntDueAsmGracePeriod.period = "Reporting Received";
              Ctrl.load();
              $rootScope.$apply();
              expect(Ctrl.treatyTracking.calculatedAssumingGraceDate.length).toBeGreaterThan(0);
            });
            it('should not set calculatedAssumingGraceDate when period is not reporting recd', function () {
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.treaty.stmntDueAsmGracePeriod.period = "Not Reporting Received";
              Ctrl.load();
              $rootScope.$apply();
              expect(Ctrl.treatyTracking.calculatedAssumingGraceDate.length).toBe(0);
            });
          });
         describe('when confirmationReceivedDate is null', function () {
            beforeEach(function () {
              Ctrl.treatyTracking.confirmationReceivedDate = null;
              Ctrl.treatyTracking.calculatedCedingGraceDate = '';
              Ctrl.treatyTracking.calculatedAssumingGraceDate = '';
            });
            it('should set calculatedCedingGraceDate when period is Reporting Confirmed', function () {
              Ctrl.treaty.stmntDueCedGracePeriod.period = "Reporting Confirmed";
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.reportingConfirmDateFlag= true;
              Ctrl.load();
              $rootScope.$apply();
             expect(Ctrl.treatyTracking.calculatedCedingGraceDate.length).toBeGreaterThan(0);
            });
            it('should not set calculatedCedingGraceDate when period is not Reporting Confirmed', function () {
              Ctrl.treaty.stmntDueCedGracePeriod.period = "Not Reporting Confirmed";
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.load();
              $rootScope.$apply();
              expect(Ctrl.treatyTracking.calculatedCedingGraceDate.length).toBe(0);
            });
            it('should set calculatedAssumingGraceDate when period is Reporting Confirmed', function () {
              Ctrl.treaty.stmntDueAsmGracePeriod.period = "Reporting Confirmed";
              Ctrl.reportingConfirmDateFlag= true;
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.load();
              $rootScope.$apply();
              expect(Ctrl.treatyTracking.calculatedAssumingGraceDate.length).toBeGreaterThan(0);
            });
            it('should not set calculatedAssumingGraceDate when period is not Reporting Confirmed', function () {
              Ctrl.treaty.stmntDueAsmGracePeriod.period = "Not Reporting Confirmed";
              Ctrl.load();
              $rootScope.$apply();
              expect(Ctrl.treatyTracking.calculatedCedingGraceDate.length).toBe(0);
            });
          });
        });
        it('should set brokerFeeDueDate when stmntDueAsmBroker isn\'t set', function () {
          Ctrl.treatyTracking.brokerFeeDueDate = '';
          Ctrl.treaty.stmntDueAsmBroker = null;
          Ctrl.load();
          $rootScope.$apply();
          expect(Ctrl.treatyTracking.brokerFeeDueDate.length).toBeGreaterThan(0);
        });
        it('should not set brokerFeeDueDate when stmntDueAsmBroker is set', function () {
          Ctrl.treatyTracking.brokerFeeDueDate = '';
          Ctrl.treaty.stmntDueAsmBroker = {period:{}};
          Ctrl.load();
          $rootScope.$apply();
          expect(Ctrl.treatyTracking.brokerFeeDueDate.length).toBe(0);
        });
        it('should set calculatedReportingDueDate', function () {
          Ctrl.treaty.reportingDueDate.period = null;
          Ctrl.treaty.reportingFrequency = null;
          Ctrl.treatyTracking.reportingMonthYear = null;
          Ctrl.treatyTracking.calculatedReportingDueDate = '';

          Ctrl.load();
          $rootScope.$apply();

          expect(Ctrl.treatyTracking.calculatedReportingDueDate.length).toBeGreaterThan(0);
        });
        it('should not set calculatedReportingDueDate', function () {
          Ctrl.treaty.reportingDueDate.period = {};
          Ctrl.treaty.reportingFrequency = {};
          Ctrl.treatyTracking.reportingMonthYear = {};
          Ctrl.treatyTracking.calculatedReportingDueDate = '';

          Ctrl.load();
          $rootScope.$apply();

          expect(Ctrl.treatyTracking.calculatedReportingDueDate.length).toBe(0);
        });

      });


      // describe('calculateReportingDue', function () {
      //   it('should set treatyTracking.calculatedReportingDueDate if monthYear is null', function () {
      //     Ctrl.treatyTracking.calculatedReportingDueDate = '';
      //     Ctrl.calculateReportingDue(null, null, null);
      //     expect(Ctrl.treatyTracking.calculatedReportingDueDate.length).toBeGreaterThan(0);
      //   });
      //   it('should set calculatedReportingDueDate if monthYear is not 12', function () {
      //     var date = new Date('2017-04-15');
      //     Ctrl.calculateReportingDue(null, null, date);
      //     expect(Ctrl.treatyTracking.calculatedReportingDueYex).toEqual(null);
      //     expect(Ctrl.treatyTracking.calculatedReportingDueDate).not.toEqual(null);
      //   });
      //   it('should set calculatedReportingDueDate if yearEndException is No', function () {
      //     var date = new Date('2017-12-15');
      //     Ctrl.treaty.yearEndException = 'No';
      //     Ctrl.calculateReportingDue(null, null, date);
      //     expect(Ctrl.treatyTracking.calculatedReportingDueYex).toEqual(null);
      //     expect(Ctrl.treatyTracking.calculatedReportingDueDate).not.toEqual(null);
      //   });
      //   it('should set calculatedReportingDueYex if yearEndException is Yes and Month is 12', function () {
      //     var date = new Date('2017-12-15');
      //     Ctrl.treaty.yearEndException = 'Yes';
      //     Ctrl.treatyTracking.treaty.deal = {
      //       yearEndMonth: 12,
      //       yearEndDay: 31
      //     };
      //     Ctrl.calculateReportingDue(null, null, date);
      //     expect(Ctrl.treatyTracking.calculatedReportingDueYex).not.toEqual(null);
      //     expect(Ctrl.treatyTracking.calculatedReportingDueDate).toEqual(null);
      //   });
      // });
      describe('calculateGracePeriodCedng', function () {
        it('should set cedingFlagQuarter to true when end quarter flag is true and cedquarterex is set', function () {
          Ctrl.endQuarterFlag = 1;
          Ctrl.treaty.stmntDueCedQuarterEx = { period:{}, dayType:{}, numberOfDays:{} };
          Ctrl.calculateGracePeriodCedng();
          expect(Ctrl.cedingFlagQuarter).toBe(true);
        });
        it('should set cedingFlagQuarter to false when end quarter flag is false and cedquarterex is set', function () {
          Ctrl.endQuarterFlag = 0;
          Ctrl.treaty.stmntDueCedQuarterEx = { period:{}, dayType:{}, numberOfDays:{} };
          Ctrl.calculateGracePeriodCedng();
          expect(Ctrl.cedingFlagQuarter).toBe(false);
        });
        it('should set cedingFlagQuarter to false when end quarter flag is true and cedquarterex is not set', function () {
          Ctrl.endQuarterFlag = 1;
          Ctrl.treaty.stmntDueCedQuarterEx = null;
          Ctrl.calculateGracePeriodCedng();
          expect(Ctrl.cedingFlagQuarter).toBe(false);
        });
      });
      describe('calculateGracePeriodAssuming', function () {
        it('should set cedingFlagQuarter to true when end quarter flag is true and cedquarterex is set', function () {
          Ctrl.endQuarterFlag = 1;
          Ctrl.treaty.stmntDueAsmQuarterEx = { period:{}, dayType:{}, numberOfDays:{} };
          Ctrl.calculateGracePeriodAssuming();
          expect(Ctrl.assumingFlagQuarter).toBe(true);
        });
        it('should set cedingFlagQuarter to false when end quarter flag is false and cedquarterex is set', function () {
          Ctrl.endQuarterFlag = 0;
          Ctrl.treaty.stmntDueAsmQuarterEx = { period:{}, dayType:{}, numberOfDays:{} };
          Ctrl.calculateGracePeriodAssuming();
          expect(Ctrl.assumingFlagQuarter).toBe(false);
        });
        it('should set cedingFlagQuarter to false when end quarter flag is true and cedquarterex is not set', function () {
          Ctrl.endQuarterFlag = 1;
          Ctrl.treaty.stmntDueAsmQuarterEx = null;
          Ctrl.calculateGracePeriodAssuming();
          expect(Ctrl.assumingFlagQuarter).toBe(false);
        });
        it('should set cedingFlagQuarter to false when end quarter flag is true and cedquarterex is not set', function () {
          calculatingDate.and.returnValue("2017-04-15:12:00");
          Ctrl.treaty.stmntDueAsmBroker = null;
          Ctrl.treatyTracking.brokerFeeDueDate = null;
          Ctrl.calculateGracePeriodAssuming();
          expect(Ctrl.treatyTracking.brokerFeeDueDate).toBeNull();
        });
      });
      describe('calculateSettlement', function () {
        var monthYear;
        beforeEach(function () {
          Ctrl.treatyTracking.reportingReceivedDate = '1';
          Ctrl.treatyTracking.confirmationReceivedDate = '2';
          monthYear = '3';
        });
        it('should call calculatingDate with reportingReceivedDate when period is reporting received', function () {
          var assumingEntity = { period:"Reporting Received" };
          Ctrl.treatyTracking.reportingReceivedDate = {};
          Ctrl.calculateSettlement(null, assumingEntity, monthYear, null);
          expect(calculatingDate).toHaveBeenCalledWith(null, assumingEntity, Ctrl.treatyTracking.reportingReceivedDate, null);
        });
        it('should call calculatingDate with confirmationReceivedDate when period is reporting confirmed', function () {
          var assumingEntity = { period:"Reporting Confirmed" };
          Ctrl.treatyTracking.reportingReceivedDate = {};
          Ctrl.treatyTracking.cashDueFlag='Yes';
          Ctrl.calculateSettlement(null, assumingEntity, monthYear, null);
          expect(calculatingDate).toHaveBeenCalledWith(null, assumingEntity, Ctrl.treatyTracking.confirmationReceivedDate, null);
        });
        it('should call calculatingDate with monthYear when period is anything else', function () {
          var assumingEntity = { period:"anything else" };
          Ctrl.calculateSettlement(null, assumingEntity, monthYear, null);
          expect(calculatingDate).toHaveBeenCalledWith(null, assumingEntity, monthYear, null);
        });
      });
      describe('save', function () {
        it('should call TreatyTracking update if id is set', function () {
          Ctrl.treatyTracking.id = 123;
          Ctrl.save();
          expect(MockTreatyTracking.update).toHaveBeenCalled();
        });
        it('should call TreatyTracking save if id is not set', function () {
          Ctrl.treatyTracking.id = null;
          Ctrl.save();
          expect(MockTreatyTracking.save).toHaveBeenCalled();
        });
      });
      it('xclear should show a dialog', function () {
        Ctrl.xclear();
        expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
      });
      it('openCalendar should set date to true', function () {
        var date = 'date';
        Ctrl.datePickerOpenStatus[date] = false;
        Ctrl.openCalendar(date);
        expect(Ctrl.datePickerOpenStatus[date]).toBe(true);
      });
      describe('futureDate', function () {
        it('should show a dialog if the chosen date is in the future', function () {
          Ctrl.futureDate(moment().add(2, "days").toDate());
          expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
        });
        it('should not show a dialog if the chosen date is not in the future', function () {
          Ctrl.futureDate(moment().subtract(2, "days").toDate());
          expect(MockInteractionsService.showThenDialog).not.toHaveBeenCalled();
        });
        it('should not show a dialog if the chosen date is today', function () {
          Ctrl.futureDate(new Date());
          expect(MockInteractionsService.showThenDialog).not.toHaveBeenCalled();
        });
      });
      it('remainInProcess should show a dialog', function () {
        Ctrl.remainInProcess();
        expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
      });
      describe('closeTrackingDialog', function () {
        it('should set trackingStatus to closed on yes', function () {
          Ctrl.treatyTracking.trackingStatus = "Open";
          MockInteractionsService.showThenDialog.and.callFake(function (dialog, yes) {
            yes();
          });
          Ctrl.closeTrackingDialog();
          expect(Ctrl.treatyTracking.trackingStatus).toBe("Closed");
        });
        it('should not ask to generate if disabled tracking is on', function () {
          Ctrl.treaty.disabledTracking = 1;
          MockInteractionsService.showThenDialog.and.callFake(function (dialog, yes) {
            yes();
          });
          Ctrl.closeTrackingDialog();
          expect(MockInteractionsService.showThenDialog).toHaveBeenCalledTimes(1);
        });
        it('should ask to generate if disabled tracking is off', function () {
          Ctrl.treaty.disabledTracking = 0;
          MockInteractionsService.showThenDialog.and.callFake(function (dialog, yes) {
            yes();
          });
          Ctrl.closeTrackingDialog();
          expect(MockInteractionsService.showThenDialog).toHaveBeenCalledTimes(2);
        });
        it('should not set trackingStatus to closed on no', function () {
          Ctrl.treatyTracking.trackingStatus = "Open";
          Ctrl.remainInProcess = jasmine.createSpy('remainInProcess');
          MockInteractionsService.showThenDialog.and.callFake(function (dialog, yes, no) {
            no();
          });
          Ctrl.closeTrackingDialog();
          expect(Ctrl.treatyTracking.trackingStatus).toBe("Open");
        });
      });
      describe('calculateDates', function () {
        beforeEach(function () {
          Ctrl.treaty.stmntDueAsm = {};
          Ctrl.treaty.stmntDueCed = {};
          Ctrl.treaty.settlementFrequency = {};
          Ctrl.treaty.stmntFrqncCeding = {};
          Ctrl.treaty.reportingFrequency = {};
          // Ctrl.treaty.deal.
        });
        describe('calculateReporting', function () {
          it('should set calculatedReportingDueDate when monthYear is null', function () {
            Ctrl.treatyTracking.calculatedReportingDueDate = '';
            Ctrl.calculateDates();
            expect(Ctrl.treatyTracking.calculatedReportingDueDate.length).toBeGreaterThan(0);
          });
          it('should set endQuarterFlag to true when monthyear is end quarter', function () {
            Ctrl.calculateDates("2017-03-15T12:00");
            expect(Ctrl.endQuarterFlag).toBe(true);
          });
          it('should set endQuarterFlag to false when monthyear is not end quarter', function () {
            Ctrl.calculateDates("2017-02-15T12:00");
            expect(Ctrl.endQuarterFlag).toBe(false);
          });
          it('should set reportingQuarterFlag to true when reportingDueQuarterEx is defined', function () {
            Ctrl.treaty.reportingDueQuarterEx = { period:{}, numberOfDays:{}, dayType:{} };
            Ctrl.calculateDates("2017-03-15T12:00");
            expect(Ctrl.reportingQuarterFlag).toBe(true);
          });
          it('should set reportingQuarterFlag to false when reportingDueQuarterEx is not defined', function () {
            Ctrl.treaty.reportingDueQuarterEx = null;
            Ctrl.calculateDates("2017-03-15T12:00");
            expect(Ctrl.reportingQuarterFlag).toBe(false);
          });
        });
        describe('param is 1', function () {
          it('should set calculatedSeriatimDueDate to calulatingDate\'s result when seriatimDueDate is set', function () {
            Ctrl.seriatimDueDate = {frequency:{}, numberOfDays:1};
            Ctrl.treatyTracking.reportingMonthYear="2017-12-01";
            Ctrl.calculateDates(null, 1);
           expect(Ctrl.treatyTracking.calculatedSeriatimDueDate).toEqual('Reporting Period Not Specified');
          });
          it('should set calculatedSeriatimDueDate to calulatingDate\'s result when seriatimDueDate is set', function () {
            Ctrl.seriatimDueDate = {};
            Ctrl.treatyTracking.reportingMonthYear="2017-12-01";
            Ctrl.calculateDates(null, 1);
          //  expect(typeof Ctrl.treatyTracking.calculatedSeriatimDueDate).toEqual("string");
          });
          describe('when stmntDueCedGracePeriod is set to something other than: [Reporting Received, Reporting Confirmed]', function () {
            beforeEach(function () {
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.treaty.stmntDueCedGracePeriod = {period:"other"};
              Ctrl.treaty.stmntFrqncCeding = {};
            });
            it('should set calculatedCedingGraceDate to a string when monthyear is null', function () {
              Ctrl.calculateDates(null, 1);
              expect(typeof Ctrl.treatyTracking.calculatedCedingGraceDate).toEqual("string");
            });
            it('should call calculateGracePeriodCedng when monthyear is not null', function () {
              Ctrl.calculateGracePeriodCedng = jasmine.createSpy('calculateGracePeriodCedng');
              Ctrl.calculateDates({}, 1);
              expect(Ctrl.calculateGracePeriodCedng).toHaveBeenCalled();
            });
          });
          describe('when stmntDueAsmGracePeriod is set to something other than: [Reporting Received, Reporting Confirmed]', function () {
            beforeEach(function () {
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.treaty.stmntDueAsmGracePeriod = {period:"other"};
              Ctrl.treaty.settlementFrequency = {};
              Ctrl.treaty.stmntFrqncCeding = {};
            });
            it('should set calculatedCedingGraceDate to a string when monthyear is null', function () {
              Ctrl.calculateDates(null, 1);
              expect(typeof Ctrl.treatyTracking.calculatedAssumingGraceDate).toEqual("string");
            });
            it('should call calculateGracePeriodCedng when monthyear is not null', function () {
              Ctrl.calculateGracePeriodAssuming = jasmine.createSpy('calculateGracePeriodAssuming');
              Ctrl.calculateDates({}, 1);
              expect(Ctrl.calculateGracePeriodAssuming).toHaveBeenCalled();
            });
          });
        });
        describe('param is 2', function () {
          describe('stmntDueCedGracePeriod is set to Reporting Received', function () {
            beforeEach(function () {
              Ctrl.treaty.stmntDueCedGracePeriod = {period:"Reporting Received"};
              Ctrl.treaty.stmntFrqncCeding = {};
            });
            it('should set calculatedCedingGraceDate to a string if reportingReceivedDate is null', function () {
              Ctrl.treatyTracking.reportingReceivedDate = null;
              Ctrl.calculateDates(null, 2);
              expect(typeof Ctrl.treatyTracking.calculatedCedingGraceDate).toEqual("string");
            });
            it('should call calculateGracePeriodCedng if reportingReceivedDate is not null', function () {
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.treatyTracking.reportingReceivedDate = {};
              Ctrl.calculateGracePeriodCedng = jasmine.createSpy('calculateGracePeriodCedng');
              Ctrl.calculateDates(null, 2);
              expect(Ctrl.calculateGracePeriodCedng).toHaveBeenCalled();
            });
          });
          describe('stmntDueAsmGracePeriod is set to Reporting Received', function () {
            beforeEach(function () {
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.treaty.stmntDueAsmGracePeriod = {period:"Reporting Received"};
              Ctrl.treaty.settlementFrequency = {};
            });
            it('should set calculatedCedingGraceDate to a string if reportingReceivedDate is null', function () {
              Ctrl.treatyTracking.reportingReceivedDate = null;
              Ctrl.calculateDates(null, 2);
              expect(typeof Ctrl.treatyTracking.calculatedAssumingGraceDate).toEqual("string");
            });
            it('should call calculateGracePeriodCedng if reportingReceivedDate is not null', function () {
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.treatyTracking.reportingReceivedDate = {};
              Ctrl.calculateGracePeriodAssuming = jasmine.createSpy('calculateGracePeriodAssuming');
              Ctrl.calculateDates(null, 2);
              expect(Ctrl.calculateGracePeriodAssuming).toHaveBeenCalled();
            });
          });
        });

        describe('param is 3', function () {
          describe('stmntDueCedGracePeriod is set to Reporting Confirmed', function () {
            beforeEach(function () {
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.treaty.stmntDueCedGracePeriod = {period:"Reporting Confirmed"};
              Ctrl.treaty.stmntFrqncCeding = {};
            });
            it('should set calculatedCedingGraceDate to a string if reportingReceivedDate is null', function () {
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.treatyTracking.confirmationReceivedDate = null;
              Ctrl.calculateDates(null, 3);
              expect(typeof Ctrl.treatyTracking.calculatedCedingGraceDate).toEqual("string");
            });
            it('should call calculateGracePeriodCedng if reportingReceivedDate is not null', function () {
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.treatyTracking.confirmationReceivedDate = {};
              Ctrl.calculateGracePeriodCedng = jasmine.createSpy('calculateGracePeriodCedng');
              Ctrl.calculateDates(null, 3);
              expect(Ctrl.calculateGracePeriodCedng).toHaveBeenCalled();
            });
          });
          describe('stmntDueAsmGracePeriod is set to Reporting Confirmed', function () {
            beforeEach(function () {
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.treaty.stmntDueAsmGracePeriod = {period:"Reporting Confirmed"};
              Ctrl.treaty.settlementFrequency = {};
            });
            it('should set calculatedCedingGraceDate to a string if reportingReceivedDate is null', function () {
              Ctrl.treatyTracking.confirmationReceivedDate = null;
              Ctrl.calculateDates(null, 3);
              expect(typeof Ctrl.treatyTracking.calculatedAssumingGraceDate).toEqual("string");
            });
            it('should call calculateGracePeriodCedng if reportingReceivedDate is not null', function () {
              Ctrl.treatyTracking.cashDueFlag='Yes';
              Ctrl.treatyTracking.confirmationReceivedDate = {};
              Ctrl.calculateGracePeriodAssuming = jasmine.createSpy('calculateGracePeriodAssuming');
              Ctrl.calculateDates(null, 3);
              expect(Ctrl.calculateGracePeriodAssuming).toHaveBeenCalled();
            });
          });
        });


      });
      describe('saveMessage', function () {
        beforeEach(function () {
          Ctrl.save = jasmine.createSpy('save');
        });
        it('should set autoGenerateFlag to 2 and call save when trackingStatus is closed', function () {
          Ctrl.treatyTracking.trackingStatus = 'Closed';
          Ctrl.saveMessage();
          expect(Ctrl.treatyTracking.autoGenerateFlag).toBe(2);
          expect(Ctrl.save).toHaveBeenCalled();
        });
        describe('trackingStatus is Open', function () {
          beforeEach(function () {
            Ctrl.treatyTracking.trackingStatus = "Open";
            Ctrl.closeTrackingDialog = jasmine.createSpy('closeTrackingDialog');
          });
          it('should call closeTrackingDialog when cashDueFlag is Yes and cashSentRecDate is calculated', function () {
            Ctrl.treatyTracking.cashDueFlag = "Yes";
            Ctrl.treatyTracking.cashSentRecDate = 'date';
            Ctrl.saveMessage();
            expect(Ctrl.closeTrackingDialog).toHaveBeenCalled();
          });
          it('should call closeTrackingDialog when cashDueFlag is No and cashSentRecDate is calculated', function () {
            Ctrl.treatyTracking.cashDueFlag = "No";
            Ctrl.treatyTracking.cashSentRecDate = 'date';
            Ctrl.saveMessage();
            expect(Ctrl.closeTrackingDialog).toHaveBeenCalled();
          });
          it('should not call closeTrackingDialog when cashDueFlag is Yes and cashSentRecDate is not calculated', function () {
            Ctrl.treatyTracking.cashDueFlag = "Yes";
            Ctrl.treatyTracking.cashSentRecDate = 'No Actual Settlements Yet';
            Ctrl.saveMessage();
            expect(Ctrl.closeTrackingDialog).not.toHaveBeenCalled();
            expect(Ctrl.save).toHaveBeenCalled();
          });
        });
      });
      describe('sync', function () {
        it('should set endQuarter flag to true if reportingMonthYear is end quarter', function () {
          Ctrl.treatyTracking.reportingMonthYear = "2017-03-15T12:00";
          Ctrl.sync();
          expect(Ctrl.endQuarterFlag).toBe(true);
        });
        it('should set endQuarter flag to false if reportingMonthYear is not end quarter', function () {
          Ctrl.treatyTracking.reportingMonthYear = "2017-02-15T12:00";
          Ctrl.sync();
          expect(Ctrl.endQuarterFlag).toBe(false);
        });
        // it('should set uiFalgEndYearEx flag to true if reportingMonthYear is end year', function () {
        //   Ctrl.uiFalgEndYearEx = false;
        //   Ctrl.treatyTracking.reportingMonthYear = "2017-12-15T12:00";
        //   Ctrl.treaty.yearEndException = 'Yes';
        //   Ctrl.sync();
        //   expect(Ctrl.uiFalgEndYearEx).toBe(true);
        // });
        it('should not set uiFalgEndYearEx flag to true if reportingMonthYear is not end year', function () {
          Ctrl.uiFalgEndYearEx = false;
          Ctrl.treatyTracking.reportingMonthYear = "2017-10-15T12:00";
          Ctrl.treaty.yearEndException = 'Yes';
          Ctrl.sync();
          expect(Ctrl.uiFalgEndYearEx).toBe(false);
        });
        it('should set calculatedSeriatimDueDate if seriatimDueDate is null', function () {
          Ctrl.seriatimDueDate = null;
          Ctrl.treatyTracking.calculatedSeriatimDueDate = '';
          Ctrl.sync();
     //     expect(Ctrl.treatyTracking.calculatedSeriatimDueDate.length).toBeGreaterThan(0);
        });
        it('should not set calculatedSeriatimDueDate if seriatimDueDate is not null', function () {
          Ctrl.seriatimDueDate = { period:{}, dayType:{}, numberOfDays:{} };
          Ctrl.treatyTracking.calculatedSeriatimDueDate = '';
          Ctrl.sync();
          expect(Ctrl.treatyTracking.calculatedSeriatimDueDate.length).toBe(0);
        });
      });
      describe('cashDueTypeChange', function(){
        describe('when ceding isn\'t set', function(){
          it('should notify that ceding must be set', function(){
            Ctrl.treaty.stmntDueCed = null;
            Ctrl.cashDueTypeChange("Ceding");
            //expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
          });
        });
        describe('when ceding is set', function(){
          it('should not notify that ceding must be set', function(){
            Ctrl.cashDueTypeChange("Ceding");
            expect(MockInteractionsService.showThenDialog).not.toHaveBeenCalled();
          });
          it('should set cashDueFlag to No and CashDueType to "" when "No Cash Due" is selected', function(){
            Ctrl.cashDueTypeSelected = "No Cash Due";

            Ctrl.cashDueTypeChange("No Cash Due");

            expect(Ctrl.treatyTracking.cashDueFlag).toBe("No");
            expect(Ctrl.treatyTracking.cashDueType).toBe("");
          });
          it('should set cashDueFlag to Yes and CashDueType to "Assuming" when "Assuming" is selected', function(){

            Ctrl.cashDueTypeSelected = "Assuming";

          //  Ctrl.cashDueTypeChange("Assuming");

          //  expect(Ctrl.treatyTracking.cashDueFlag).toBe("Yes");
           //expect(Ctrl.treatyTracking.cashDueType).toBe("Assuming");
          });
          it('should set cashDueFlag to Yes and CashDueType to "Ceding" when "Ceding" is selected', function(){

            Ctrl.cashDueTypeSelected = "Ceding";

         //   Ctrl.cashDueTypeChange("Ceding");

        //   expect(Ctrl.treatyTracking.cashDueFlag).toBe("Yes");
        // expect(Ctrl.treatyTracking.cashDueType).toBe("Ceding");
          });
        });
      });
      describe('cashDueTypeInit', function(){
        it('should set cash due selected to "No Cash Due" when cashDueFlag is No', function(){
          Ctrl.treatyTracking.cashDueFlag = 'No';
          Ctrl.treatyTracking.cashDueType = '';

          Ctrl.cashDueTypeInit();

          expect(Ctrl.cashDueTypeSelected).toBe("No Cash Due");
        });
        it('should set cash due selected to cashDueType when cashDueFlag is Yes and cashDueType is set', function(){
          Ctrl.treatyTracking.cashDueFlag = 'Yes';
          Ctrl.treatyTracking.cashDueType = 'Assuming';

          Ctrl.cashDueTypeInit();

          expect(Ctrl.cashDueTypeSelected).toBe("Assuming");
        });
        it('should set cash due selected to cashDueType when cashDueFlag is Yes and cashDueType is set', function(){
          Ctrl.treatyTracking.cashDueFlag = 'Yes';
          Ctrl.treatyTracking.cashDueType = 'Ceding';

          Ctrl.cashDueTypeInit();

          expect(Ctrl.cashDueTypeSelected).toBe("Ceding");
        });
        it('should set cash due selected to Blank when cashDueFlag is Yes and cashDueType is not set', function(){
          Ctrl.treatyTracking.cashDueFlag = 'Yes';
          Ctrl.treatyTracking.cashDueType = '';

          Ctrl.cashDueTypeInit();

          expect(Ctrl.cashDueTypeSelected).toBe("");
        });
      });
    });
  });
});
