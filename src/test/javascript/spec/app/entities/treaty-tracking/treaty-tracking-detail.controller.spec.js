'use strict';

describe('Controller Tests', function () {

  describe('TreatyTracking Management Detail Controller', function () {
    var $scope, $rootScope, MockState;
    var MockEntity, MockTreatyTracking;
    var Ctrl, MockDealTrackingFilterSrv, MockDealTrackingFilterItem;

    beforeEach(inject(function ($injector, $controller) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      MockEntity = {};
      MockTreatyTracking = jasmine.createSpy('MockTreatyTracking');
      MockDealTrackingFilterItem = [];
      MockDealTrackingFilterSrv = {
        reportingReportTracking: {
          get: function (obj, callback) {
            callback(MockDealTrackingFilterItem);
          }
        }
      };
      MockState = jasmine.createSpyObj('$state', ['go']);

      Ctrl = $controller("TreatyTrackingDetailController", {
        $scope: $scope,
        $rootScope: $rootScope,
        entity: MockEntity,
        TreatyTracking: MockTreatyTracking,
        DealTrackingFilterSrv: MockDealTrackingFilterSrv,
        $state: MockState
      });
    }));


    describe('Init State', function () {
      it('Ctrl should be defined', function () {
        expect(Ctrl).toBeDefined();
      });
    });
    describe('Main Content', function () {
      describe('reportingPeriodEndDealTracking', function () {
        it('should set the correct data if a matching report is found', function () {
          var date = new Date("2017-01-15");
          MockDealTrackingFilterItem = [{
            password: "password"
          }];
          Ctrl.reportingPeriodEndDealTracking(date);
          expect(Ctrl.password).toBe("password");
        });
        it('should set the correct data to default if a matching report is not found', function () {
          var date = new Date("2017-01-15");
          Ctrl.reportingPeriodEndDealTracking(date);
          expect(Ctrl.password).not.toBe("password");
        });
      });
      describe('load', function () {
        it('should load the seriatim items', function () {
          Ctrl.seriatimDueDate = new Date("2017-01-15");
          Ctrl.treatyTracking = {
            reportingMonthYear: new Date("2017-01-15"),
            treaty: {
              seriatimTypeTreaties: [
                { seriatimDueDateFlag: "Yes", calculationFlag: true, seriatimDueDate: new Date("2017-01-15") }
              ]
            }
          };
          Ctrl.treatyTracking.treaty.seriatimTypeTreaties[0].seriatimDueDate.period = 1;
          Ctrl.load();
          expect(Ctrl.seriatimDueDate.period).toBe(1);
        });
        it('should set endQuarterFlag if end of Quarter', function () {
          Ctrl.seriatimDueDate = new Date("2017-01-15");
          Ctrl.treatyTracking = {
            reportingMonthYear: new Date("2017-03-15"),
            treaty: {
              seriatimTypeTreaties: [
                { seriatimDueDateFlag: "Yes", calculationFlag: true, seriatimDueDate: new Date("2017-01-15") }
              ]
            }
          };
          Ctrl.treatyTracking.treaty.seriatimTypeTreaties[0].seriatimDueDate.period = 1;
          Ctrl.load();
          expect(Ctrl.endQuarterFlag).toBe(true);
        });
        it('should set endQuarterFlag if end of Quarter', function () {
          Ctrl.seriatimDueDate = new Date("2017-01-15");
          Ctrl.treatyTracking = {
            reportingMonthYear: new Date("2017-12-15"),
            treaty: {
              seriatimTypeTreaties: [
                { seriatimDueDateFlag: "Yes", calculationFlag: true, seriatimDueDate: new Date("2017-01-15") }
              ],
              yearEndException: 'Yes'
            }
          };
          Ctrl.treatyTracking.treaty.seriatimTypeTreaties[0].seriatimDueDate.period = 1;
          Ctrl.load();
          expect(Ctrl.uiFalgEndYearEx).toBe(true);
        });
      });
      describe('linkToEditPage', function () {
        it('should call vm.go if disabled tracking is off', function () {
          Ctrl.treaty = {disabledTracking: 0};

          Ctrl.linkToEditPage();

          expect(MockState.go).toHaveBeenCalled();
        });
        it('should not call vm.go if disabled tracking is on', function () {
          Ctrl.treaty = {disabledTracking: 1};

          Ctrl.linkToEditPage();

          expect(MockState.go).not.toHaveBeenCalled();
        });
      });
    });
  });
});
