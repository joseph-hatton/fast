'use strict';

describe('Controller Tests', function () {

  describe('TreatyTrackingDeleteController Delete Dialog Controller', function () {
    var $scope, $rootScope,defer,MockTreatyTransactionSrv;
    var MockEntity, MockTreatyTracking , DeferFactory, MockInteractionsService,MockUiModalInstance;
    var Ctrl, MockDealTrackingFilterSrv ,deferTransactions, MockTreatyTrackingCustomSvr;

    beforeEach(inject(function ($injector, $controller, _DeferFactory_, $q) {
      DeferFactory = _DeferFactory_;
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      MockEntity = { treaty:{
        seriatimTypeTreaties:[],
        reportingDueDate:{},
        reportingDueYearEx:{},
        stmntDueCedGracePeriod:{},
        stmntDueCed: {},
        stmntDueAsmGracePeriod:{},
        stmntDueAsmBroker:{},
        deal: {}
      } };
      deferTransactions= DeferFactory.generate();
      MockTreatyTrackingCustomSvr = {
        lasReportingMonthYear: jasmine.createSpyObj('lasReportingMonthYear', ['query']),
        calculateReportingDue: jasmine.createSpy('calculateReportingDue')
      };
      MockTreatyTracking = jasmine.createSpyObj('MockTreatyTracking', ['save', 'update']);
      MockInteractionsService = jasmine.createSpyObj('InteractionsService', ['confirmBasicParentNoScrollDialog', 'showThenDialog', 'alertParentScrollFalseDialog']);
      MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
      MockTreatyTransactionSrv={subTransactionTracking: jasmine.createSpyObj('subTransactionTracking', ['get'])};
      MockTreatyTransactionSrv.subTransactionTracking.get.and.returnValue(deferTransactions);
      Ctrl = $controller("TreatyTrackingDeleteController", {
        '$scope': $scope,
        '$uibModalInstance': MockUiModalInstance,
        'entity': MockEntity,
        'TreatyTracking': MockTreatyTracking,
        'DealTrackingFilterSrv': MockDealTrackingFilterSrv,
        'TreatyTransactionSrv': MockTreatyTransactionSrv
      });

    }));
    describe('Initial state', function () {
      it('TreatyTrackingDeleteController Controller', function() {
        expect(Ctrl).toBeDefined();
      });


      it('Clear', function() {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });
    });

  });
});
