'use strict';

describe('Controller Tests', function() {

    describe('Jhi Users Dialog Controller', function() {
        var $scope;
        var MockJhiUsers, MockInteractionsService, MockUibModalInstance;
        var DeferFactory, stubDefer, MockUser;
        var Ctrl;
        beforeEach(inject(
        function ($controller, $rootScope, _DeferFactory_) {
            $scope = $rootScope.$new();
            DeferFactory = _DeferFactory_;
            stubDefer = DeferFactory.generate();

            MockUibModalInstance = jasmine.createSpy('$uibModalInstance');

            MockJhiUsers = jasmine.createSpyObj('JhiUser', ['update', 'save']);
            
            MockJhiUsers.update.and.returnValue(stubDefer);
            MockJhiUsers.save.and.returnValue(stubDefer);

            MockUser = {};

            MockInteractionsService = jasmine.createSpyObj('InteractionsService', ['confirmBasicParentNoScrollDialog', 'showThenDialog']);
            
            Ctrl = $controller('JhiUsersDialogController', {
                $scope: $scope,
                $uibModalInstance: MockUibModalInstance,
                entity: MockUser,
                JhiUsers: MockJhiUsers,
                InteractionsService: MockInteractionsService
            });
        }));

        describe('Initial state', function () {
            it('should instantiate the controller properly', function () {
                expect(Ctrl).not.toBeUndefined();
            });
        });

        describe('Main Content', function(){
            describe('save', function(){
                beforeEach(function(){
                    Ctrl.prepPermissons = jasmine.createSpy('prepPermissons');
                });
                it('should call update if id is set', function(){
                    Ctrl.user.id = 123;
                    
                    Ctrl.save();

                    expect(MockJhiUsers.update).toHaveBeenCalled();
                });
                it('should call save if id is not set', function(){
                    Ctrl.user.id = null;
                    
                    Ctrl.save();

                    expect(MockJhiUsers.save).toHaveBeenCalled();
                });
            });
            describe('prepPermissons', function(){
                it('should remove the items not selected', function(){
                    Ctrl.user.auths = [
                        { authorityName: "test1" },
                        { authorityName: "test2" },
                    ];
                    Ctrl.authChoices = [
                        { name: 'test1', isChecked: true },
                        { name: 'test2', isChecked: false }
                    ];

                    Ctrl.prepPermissons();

                    expect(Ctrl.user.auths.length).toBe(1);
                    expect(Ctrl.user.auths[0].authorityName).toBe("test1");
                });
                it('should add the items selected', function(){
                    Ctrl.user.auths = [];
                    Ctrl.authChoices = [
                        { name: 'test1', isChecked: true },
                        { name: 'test2', isChecked: false }
                    ];

                    Ctrl.prepPermissons();


                    expect(Ctrl.user.auths.length).toBe(1);
                    expect(Ctrl.user.auths[0].authorityName).toBe("test1");
                    expect(Ctrl.user.auths[0].creationTs).toBeDefined();
                });
                it('should keep the items if nothing is changed', function(){
                    Ctrl.user.auths = [
                        { authorityName: "test1" }
                    ];
                    Ctrl.authChoices = [
                        { name: 'test1', isChecked: true },
                        { name: 'test2', isChecked: false }
                    ];

                    Ctrl.prepPermissons();


                    expect(Ctrl.user.auths.length).toBe(1);
                    expect(Ctrl.user.auths[0].authorityName).toBe("test1");
                    expect(Ctrl.user.auths[0].creationTs).toBeUndefined();
                });
            });
        });
    });
});
