'use strict';

describe('Controller Tests', function() {

    describe('AgreementType Controller', function() {
      var $scope, $rootScope;
      var MockEntity, MockAgreementType;
      var createController;
      var AgreementType;
      var Ctrl;
      var scope;
      var MockAgreementTypeSearch;
      var serviceResponse;
// Initialize the controller and a mock scope
      beforeEach(inject(

        function ($controller,$injector, $rootScope) {     // Don't bother injecting a 'real' modal
          scope = $rootScope.$new();
          var info = [{},{},{}];
          var info1 = [{}];
          MockAgreementType =
            {
              query: function(callBack){
                callBack(info);
              }
            };
            //jasmine.createSpyObj('AgreementType',['query']);
      //    spyOn(MockAgreementType,'query').and.callFake(function(callBack){
        //    callBack([{},{},{}]);
       //   });


          MockAgreementTypeSearch  =
            {
              query: function(searchQuery,callBackd){
                callBackd(info1);
              }
            };

          Ctrl = $controller('AgreementTypeController', {
            $scope:scope ,AgreementType:MockAgreementType,AgreementTypeSearch:MockAgreementTypeSearch
          });


        }));

      describe('Initial state', function () {
        it('should instantiate the controller properly', function () {
          expect(Ctrl).not.toBeUndefined();
        });

        it('AgreementType Controller', function() {
          expect(Ctrl).toBeDefined();
        });

        it('Load All Test', function() {
          expect(Ctrl.loadAll).toBeDefined();
          expect(Ctrl.agreementTypes.length).toBe(3);
        });

        it('Search function  test', function() {
          expect(Ctrl.search).toBeDefined();
          expect(Ctrl.searchQuery).toBeUndefined();
          Ctrl.searchQuery='123';
          expect(Ctrl.searchQuery).toBeDefined();
          Ctrl.agreementTypes=[];
          Ctrl.search();
          expect(Ctrl.agreementTypes.length).toBe(1);
        });
        it('Search function Null search test', function() {
          expect(Ctrl.search).toBeDefined();
          expect(Ctrl.searchQuery).toBeUndefined();
          Ctrl.agreementTypes=[];
          Ctrl.search();
          expect(Ctrl.agreementTypes.length).toBe(3);
        });





      });
    });
});
