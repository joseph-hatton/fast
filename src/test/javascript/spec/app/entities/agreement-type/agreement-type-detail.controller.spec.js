'use strict';

describe('Controller Tests', function() {

    describe('AgreementType Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockAgreementType;
        var Ctrl;

        beforeEach(inject(function($controller,$injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockAgreementType = {
              get: function (id, callback) {
                callback(id);
              }
            };

            Ctrl = $controller("AgreementTypeDetailController", {
              '$scope': $scope,
              '$rootScope': $rootScope,
              'entity': MockEntity ,
              'AgreementType': MockAgreementType
            });

        }));

      describe('Initial State', function () {
        it('AgreementType Detail Controller Should be Created', function () {
          expect(Ctrl).toBeDefined();
        });

        it('Load Should be Defined', function () {
          expect(Ctrl.load).toBeDefined();
        });
      });
    });

});
