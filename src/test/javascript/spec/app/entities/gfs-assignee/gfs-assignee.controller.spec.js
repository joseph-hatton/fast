'use strict';

describe('Controller Tests', function() {

  describe('GfsAssignee Controller', function() {

    var Ctrl;
    var scope;
    var MockState;
    var MockGfsAssignee;
    var MockGfsAssigneeSearch;
    var loadAllResult = [{}, {}, {}, {}, {}, {}];
    var searchResult = [{}];

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();

        MockState = jasmine.createSpy('MockState');

        MockGfsAssignee = {
          query: function (callback) {
            callback(loadAllResult);
          }
        };

        MockGfsAssigneeSearch = {
          query: function (searchQuery, callback) {
            callback(searchResult);
          }
        };

        Ctrl = $controller('GfsAssigneeController', {
          $scope: scope,
          $state: MockState,
          GfsAssignee: MockGfsAssignee,
          GfsAssigneeSearch: MockGfsAssigneeSearch
        });
      }));

    describe('Initial state', function () {
      it('GfsAssignee Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('Load All Test', function() {
        expect(Ctrl.loadAll).toBeDefined();
      });

      it('Search Test', function() {
        expect(Ctrl.search).toBeDefined();
      });
    });

    describe('Main Content Coverage', function () {
      it('Load All Test', function () {
        expect(Ctrl.gfsAssignees).toBe(loadAllResult);
      });

      it('Search Test No Search Query', function () {
        Ctrl.searchQuery = null;
        Ctrl.search();
        expect(Ctrl.gfsAssignees).toBe(loadAllResult);
      });

      it('Search Test Search Query', function () {
        Ctrl.searchQuery = 1;
        Ctrl.GfsAssignees = [];
        Ctrl.search();
        expect(Ctrl.gfsAssignees).toBe(searchResult);
      });
    })
  });
});
