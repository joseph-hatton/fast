'use strict';

describe('Controller Tests', function() {
  describe('GfsAssignee Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockGfsAssignee,MockUiModalInstance;
    var MockEntity, MockPreviousState;
    var success='Success';
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = jasmine.createSpyObj('MockEntity', ['get', 'update', 'save']);
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);


        MockGfsAssignee  =
          {
            get: function (id, callback) {
              callback(id);
            },
            update: function(id,success, error){ // for testing purposes, a flag of error will be set to trigger an 'error'
              if(!id.errorTestFlag)
                success(id);
              else
                error();
            },
            save: function (id, success, error) {
              if(!id.errorTestFlag)
                success(id);
              else
                error();
            }
          };

        Ctrl = $controller('GfsAssigneeDialogController', {
          $scope: scope,
          $state: state,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          GfsAssignee: MockGfsAssignee
        });
      }));

    describe('Initial state', function () {
      it('GfsAssigneeDialog Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('Load', function() {
        expect(Ctrl.load).toBeDefined();
      });

      it('Save', function() {
        expect(Ctrl.save).toBeDefined();
      });

      it('Clear', function() {
        expect(Ctrl.clear).toBeDefined();
      });
    });
  });
});
