'use strict';

describe('Controller Tests', function() {
  describe('GfsAssignee Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockGfsAssignee,MockUiModalInstance;
    var MockEntity, MockPreviousState;
    var success='Success';
    var info =1;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = jasmine.createSpyObj('MockEntity', ['get', 'update', 'save']);
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        MockGfsAssignee  =
          {
            delete: function(id,callBack){
              callBack(info);
            },
            update: function(id,success, error){ // for testing purposes, a flag of error will be set to trigger an 'error'
              if(!id.errorTestFlag)
                success(id);
              else
                error();
            },
            save: function (id, success, error) {
              if(!id.errorTestFlag)
                success(id);
              else
                error();
            }
          };

        Ctrl = $controller('GfsAssigneeDeleteController', {
          $scope: scope,
          $state: state,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          GfsAssignee: MockGfsAssignee
        });
      }));

    describe('Initial state', function () {
      it('GfsAssigneeDialog Delete Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('clear', function() {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });

      it('confirmDelete', function() {
        Ctrl.confirmDelete(123);
        expect(Ctrl.confirmDelete).toBeDefined();
      });
    });
  });
});
