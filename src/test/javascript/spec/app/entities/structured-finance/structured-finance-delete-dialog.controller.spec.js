'use strict';

describe('Controller Tests', function() {
  describe('StructuredFinanceDeleteController', function() {
    var Ctrl;
    var scope,state,MockStructuredFinance,MockUiModalInstance;
    var MockEntity, MockPreviousState, MockMdDialog;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = jasmine.createSpy('MockEntity');
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockStructuredFinance = {
          update: function (structuredFinanceObj, success, error){ success(structuredFinanceObj); }
        };
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        MockMdDialog = {
          alert: function () { return { textContent: function () { return { ok: function () { return { parent: function () {} }; } }; } }; },
          show: function (confirm) { return { then: function(callback) { callback() } }; }
        };
        Ctrl = $controller('StructuredFinanceDeleteController', {
          $scope: scope,
          $state: state, entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          StructuredFinance: MockStructuredFinance,
          $mdDialog: MockMdDialog
        });
      }));

    describe('Initial state', function () {
      it('StructuredFinanceDeleteController', function() {
        expect(Ctrl).toBeDefined();
      });
      it('confirmDelete ', function() {
        expect(Ctrl.confirmDelete).toBeDefined();
        Ctrl.confirmDelete(123);
        //expect(MockUiModalInstance.close).toHaveBeenCalled();
      });

      it('Clear', function() {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });
    });
  });
});
