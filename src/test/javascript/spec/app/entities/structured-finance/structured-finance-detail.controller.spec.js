'use strict';

describe('Controller Tests', function() {

  describe('StructuredFinance Detail Controller', function() {
    var Ctrl;
    var scope,state,paginationConstants,pagingParams, MockDeal, MockStructuredFinance, MockEntity;

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        pagingParams={ascending:true,page:1,predicate:"id",search:null,sort:"id,asc"};
        paginationConstants= {itemsPerPage: 10};
        MockStructuredFinance = jasmine.createSpy('MockStructuredFinance');
        MockDeal = {
          get: function (idObj, callback) {
            callback(idObj);
          }
        };

        MockEntity = {
          deal: {
            yearEndMonth: 3
          }
        };

        Ctrl = $controller('StructuredFinanceDetailController', {
          $scope: scope,
          $rootScope: $rootScope,
          $stateParams: { dealId:123 },
          entity: MockEntity,
          StructuredFinance: MockStructuredFinance,
          Deal: MockDeal
        });
      }));

    describe('Initial state', function () {
      it('StructuredFinance Detail Controller', function() {
        expect(Ctrl).toBeDefined();
      });
    });
    describe('Main Coverage', function () {
      describe('AngularForeach', function () {
        it('should set dealEndMonth with the correct month', function () {
          expect(Ctrl.dealEndMonth.id).toBe(3);
        });
      });
      describe('Load', function () {
        it('should run Deal.get', function () {
          expect(Ctrl.deal.id).toBe(123);
        });
      });
    });
  });
});
