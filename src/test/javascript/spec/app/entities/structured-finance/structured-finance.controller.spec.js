'use strict';

describe('Controller Tests', function() {

  describe('StructuredFinance Controller', function() {
    var Ctrl;
    var scope,state,paginationConstants,pagingParams, MockDeal, MockStructuredFinanceCustomSrv, MockParseLinks;
    var MockLoadAllQuery = [{},{},{}];

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state = { transitionTo: function (obj1, obj2) {} };
        pagingParams={ascending:true,page:1,predicate:"id",search:null,sort:"id,asc"};
        paginationConstants= {itemsPerPage: 10};
        MockDeal = {
          get: function (idObj, callback) {
            callback(idObj);
          }
        };
        MockStructuredFinanceCustomSrv = {
          query: function (obj, success, error) {
            success(MockLoadAllQuery, function(str){});
          }
        };
        MockParseLinks = {
          parse: function (msg) {}
        };
        Ctrl = $controller('StructuredFinanceController', {
          $scope: scope,
          $state: state,
          paginationConstants:paginationConstants,
          pagingParams:pagingParams,
          $stateParams: { dealId:123 },
          Deal: MockDeal,
          StructuredFinanceCustomSrv: MockStructuredFinanceCustomSrv,
          ParseLinks: MockParseLinks
        });
      }));

    describe('Initial state', function () {
      it('StructuredFinance Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('StructuredFinance Load All Test', function() {
        expect(Ctrl.loadPage).toBeDefined();
      });

      it('StructuredFinance Search Test', function() {
        expect(Ctrl.search).toBeDefined();
      });
    });
    describe('Main Coverage', function () {
      describe('Deal.get', function () {
        it('should set the deal to result', function () {
          expect(Ctrl.deal.id).toBe(123);
        });
      });
      describe('LoadAll', function () {
        it('should load all of the files', function () {
          expect(Ctrl.structuredFinances.length).toBe(MockLoadAllQuery.length);
        });
      });
      describe('LoadPage', function () {
        it('should set vm.page to the page', function () {
          var newPage = 123;
          Ctrl.page = null;
          Ctrl.loadPage(newPage);
          expect(Ctrl.page).toBe(newPage);
        });
      });
      describe('Clear', function () {
        it('should clear currentSearch', function () {
          Ctrl.currentSearch = "search";
          Ctrl.clear();
          expect(Ctrl.currentSearch).toBeNull();
        });
      });
      describe('Search', function () {
        it('should run clear when searchQuery is blank', function () {
          Ctrl.currentSearch = "search";
          Ctrl.search("");
          expect(Ctrl.currentSearch).toBeNull();
        });
        it('should run set vm.currentSearch to currentSearch', function () {
          var search = "search";
          Ctrl.currentSearch = null;
          Ctrl.search(search);
          expect(Ctrl.currentSearch).toBe(search);
        });
      });
    });
  });
});
