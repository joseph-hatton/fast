'use strict';

describe('Controller Tests', function() {
  describe('StructuredFinance Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockStructuredFinance,MockUiModalInstance;
    var MockEntity, MockPreviousState, MockMdDialog;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = jasmine.createSpy('MockEntity');
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockStructuredFinance = {
          update: function (structuredFinanceObj, success, error){ success(structuredFinanceObj); },
          save: function (structuredFinanceObj, success, error){ error(); }
        };
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        MockMdDialog = {
          alert: function () { return { textContent: function () { return { ok: function () { return { parent: function () {} }; } }; } }; },
          show: function (confirm) { return { then: function(callback) { callback() } }; }

        };
        Ctrl = $controller('StructuredFinanceDialogController', {
          $scope: scope,
          $state: state, entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          StructuredFinance: MockStructuredFinance,
          $mdDialog: MockMdDialog
        });
      }));

    describe('Initial state', function () {
      it('StructuredFinanceDialog Controller', function() {
        expect(Ctrl).toBeDefined();
      });
    });
    describe('Main Coverage', function () {
      describe('ChangeReportingFrequency', function () {
        describe('Reporting Frequency set', function () {
          it('should set day type to DayTypeOptions[0] when day type is empty', function () {
            Ctrl.structuredFinance.reportingFrequency = {};
            Ctrl.structuredFinance.dayType = '';
            Ctrl.changeReportingFrequency();
            expect(Ctrl.structuredFinance.dayType).not.toBe('');
          });
          it('should set month options when Monthly is set', function () {
            Ctrl.structuredFinance.reportingFrequency = { name: "Monthly" };
            Ctrl.changeReportingFrequency();
            expect(Ctrl.DayTypeOptionsRep).toBe(Ctrl.MonthlyDayTypeOptions);
          });
          it('should set Quarterly options when Quarterly is set', function () {
            Ctrl.structuredFinance.reportingFrequency = { name: "Quarterly" };
            Ctrl.changeReportingFrequency();
            expect(Ctrl.DayTypeOptionsRep).toBe(Ctrl.QuarterDayTypeOptions);
          });
          it('should set Semi-Annual options when Semi-Annual is set', function () {
            Ctrl.structuredFinance.reportingFrequency = { name: "Semi-Annual" };
            Ctrl.changeReportingFrequency();
            expect(Ctrl.DayTypeOptionsRep).toBe(Ctrl.SemiAnnualDayTypeOptions);
          });
          it('should set Annual options when Annual is set', function () {
            Ctrl.structuredFinance.reportingFrequency = { name: "Annual" };
            Ctrl.changeReportingFrequency();
            expect(Ctrl.DayTypeOptionsRep).toBe(Ctrl.AnnualDayTypeOptions);
          });
          it('should set everything to null when Occurrence is set', function () {
            Ctrl.structuredFinance.reportingFrequency = { name: "Occurrence" };
            Ctrl.changeReportingFrequency();
            expect(Ctrl.DayTypeOptionsRep).toBeNull();
          });
        });
        describe('Reporting Frequency not set', function () {
          it('should not set day type to DayTypeOptions[0] when day type is empty', function () {
            Ctrl.structuredFinance.reportingFrequency = null;
            Ctrl.structuredFinance.dayType = '';
            Ctrl.changeReportingFrequency();
            expect(Ctrl.structuredFinance.dayType).toBe('');
          });
        });
      });
      describe('Clear', function () {
        it('should run $uibModalInstance.close', function () {
          Ctrl.clear();
          expect(MockUiModalInstance.close).toHaveBeenCalled();
        });
      });
      describe('OpenCalendar', function () {
        it('should set datePickerOpenStatus[date] to true', function () {
          var item = 'item';
          Ctrl.datePickerOpenStatus[item] = false;
          Ctrl.openCalendar(item);
          expect(Ctrl.datePickerOpenStatus[item]).toBe(true);
        });
      });
      describe('Deal Change', function () {
        it('should set dealEndMonth with the correct month', function () {
            Ctrl.structuredFinance = {
              deal: {
                yearEndMonth: 3
              }
            };
            Ctrl.dealChange();
            expect(Ctrl.dealEndMonth.id).toBe(3);
        });
      });
      describe('Save', function () {
        it('should run StructuredFinance.update when id is set', function () {
          Ctrl.structuredFinance = { id: 123 };
          Ctrl.save();
          expect(MockUiModalInstance.close).toHaveBeenCalled();
        });
        it('should run StructuredFinance.save when id is not set', function () {
          Ctrl.structuredFinance = { id: null };
          Ctrl.save();
          expect(MockUiModalInstance.close).not.toHaveBeenCalled();
        });
      });
      describe('NumberOfDay', function () {
        describe('when param is 1', function () {
          var param = 1;
          it('NumberOfDaysYr is 0', function () {
            Ctrl.structuredFinance = { numberOfDaysYr: 0 };
            Ctrl.numberOfDay(param);
            expect(Ctrl.structuredFinance.dayTypeYr).toBe('On');
          });
          it('NumberOfDaysYr is less than 0', function () {
            var param = 1;
            Ctrl.structuredFinance = { numberOfDaysYr: -1 };
            Ctrl.numberOfDay(param);
            expect(Ctrl.structuredFinance.dayTypeYr).not.toBe('On');
          });
        });
        describe('when param is not 1', function () {
          var param = 0;
          it('NumberOfDays is 0', function () {
            Ctrl.structuredFinance = { numberOfDays: 0 };
            Ctrl.numberOfDay(param);
            expect(Ctrl.structuredFinance.dayType).toBe('On');
          });
          it('NumberOfDays is less than 0', function () {
            Ctrl.structuredFinance = { numberOfDays: -1 };
            Ctrl.numberOfDay(param);
            expect(Ctrl.structuredFinance.dayType).not.toBe('On');
          });
        });
      });
    });
  });
});
