'use strict';

describe('Controller Tests', function() {

    describe('Reports Detail Controller', function() {
      var $scope, $rootScope, $sce;
      var MockEntity, MockPreviousState, MockReports;
      var Ctrl;

      beforeEach(inject(function($injector, $controller) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        $sce = { trustAsResourceUrl: function (url) { return url; } };
        MockEntity = jasmine.createSpy('MockEntity');
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockReports = jasmine.createSpy('MockReports');

        Ctrl = $controller("ReportsDetailController", {
          '$scope': $scope,
          '$rootScope': $rootScope,
          '$sce': $sce,
          'entity': MockEntity,
          'previousState': MockPreviousState,
          'Reports': MockReports
        });

        Ctrl.reports.reportLink = 'report';
        Ctrl.reports.tableautLink = 'tableau';
      }));

      describe('Initial State', function () {
        it('should have Ctrl defined', function () {
          expect(Ctrl).toBeDefined();
        });

        it('should have SwitchChange defined', function () {
          expect(Ctrl.switchChange).toBeDefined();
        });
      });

      describe('Main Content Coverage', function () {
        describe('SwitchChange', function () {
          it('should show the report Link with SSRS', function () {
            Ctrl.flagChange = 'SSRS';
            Ctrl.switchChange();
            expect(Ctrl.reportLink).toBe(Ctrl.reports.reportLink);
          });

          it('should show the report Link without SSRS', function () {
            Ctrl.flagChange = 'tableaut';
            Ctrl.switchChange();
            expect(Ctrl.reportLink).toBe(Ctrl.reports.tableautLink);
          });
        });
      });
    });
});
