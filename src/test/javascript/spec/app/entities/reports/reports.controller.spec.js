'use strict';

describe('Controller Tests', function() {

  describe('Reports Controller', function() {

    var Ctrl;
    var scope;
    var MockReports;
    var MockReportsSearch;
    var loadAllResult = [{},{},{}], searchResult = [{}];

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();

        MockReports = {
          query: function (callback) {
            callback(loadAllResult);
          }
        };

        MockReportsSearch = {
          query: function (search, callback) {
            callback(searchResult);
          }
        };

        Ctrl = $controller('ReportsController', {
          $scope: scope,
          Reports: MockReports,
          ReportsSearch: MockReportsSearch
        });
      }));

    describe('Initial state', function () {
      it('Reports Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('Load All Test', function() {
        expect(Ctrl.loadAll).toBeDefined();
      });

      it('Search Test', function() {
        expect(Ctrl.search).toBeDefined();
      });
    });

    describe('Main Content Cover', function () {
      it('Load All Test', function () {
        expect(Ctrl.reports).toBe(loadAllResult);
      });

      it('Search No Query Test', function () {
        Ctrl.searchQuery = null;
        Ctrl.search();
        expect(Ctrl.reports).toBe(loadAllResult);
      });

      it('Search Query Test', function () {
        Ctrl.searchQuery = 1;
        Ctrl.search();
        expect(Ctrl.reports).toBe(searchResult);
      });
    });
  });
});
