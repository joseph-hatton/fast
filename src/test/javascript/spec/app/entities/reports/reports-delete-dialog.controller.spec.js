'use strict';

describe('Controller Tests', function() {
  describe('Reports Dialog Delete  Controller', function() {
    var Ctrl;
    var scope,state,MockReports,MockUiModalInstance;
    var MockEntity, MockPreviousState;
    var success='Success';
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = jasmine.createSpyObj('MockEntity', ['get', 'update', 'save']);
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);


        MockReports  =
          {
            delete: function (id, callback) {
              return "Success deletion";
            }
          };

        Ctrl = $controller('ReportsDeleteController', {
          $scope: scope,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          Reports: MockReports
        });
      }));

    describe('Initial state', function () {
      it('ReportsDialog Delete Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('confirmDelete ', function() {
        expect(Ctrl.confirmDelete).toBeDefined();
        Ctrl.confirmDelete(123);
      });


      it('Clear', function() {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });
    });
  });
});
