'use strict';

describe('Service Tests', function() {
  describe('Dialog Service', function() {
    var ds = {};
    var MockScope,state,MockItem, Mockitem, MockUiModalInstance, $stateParams;
    var MockEntity, MockPreviousState;
    var success='Success';
    beforeEach(inject(
      function (_DialogService_) {     // Don't bother injecting a 'real' modal
        ds.prototype = Object.create(_DialogService_.prototype);
        MockEntity = jasmine.createSpyObj('MockEntity', ['get', 'update', 'save']);
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        MockScope = jasmine.createSpyObj('$scope', ['$emit']);
        Mockitem = {id: null};

        MockItem = {
          get: function (id, callback) {
            callback(id);
          },
          update: function (id, success, error) { // for testing purposes, a flag of error will be set to trigger an 'error'
            if (!id.errorTestFlag)
              success(id);
            else
              error();
          },
          save: function (id, success, error) {
            if (!id.errorTestFlag)
              success(id);
            else
              error();
          }
        };

        _DialogService_.call(ds, MockScope, $stateParams, MockUiModalInstance, MockEntity, MockItem, Mockitem, "Item", function () {});
      }));

    describe('Initial state', function () {
      it('DialogService to be defined', function() {
        expect(ds).toBeDefined();
      });

      it('Load', function() {
        expect(ds.load).toBeDefined();
      });

      it('Save', function() {
        expect(ds.save).toBeDefined();
      });

      it('Clear', function() {
        expect(ds.clear).toBeDefined();
      });
    });

    describe('Main Content Coverage', function () {
      it('Load Test', function () {
        var id = 123;
        ds.load(id);
        expect(ds.item.id).toBe(id);
      });

      it('Save Test (id is set) save success', function () {
        var item = { id: 123, errorTestFlag: false };
        ds.item = item;
        ds.save();
        expect(MockUiModalInstance.close).toHaveBeenCalled();
        expect(ds.isSaving).toBe(false);
      });

      it('Save Test (id is set) save error', function () {
        var item = { id: 123, errorTestFlag: true };
        ds.item = item;
        ds.save();
        expect(MockUiModalInstance.close).not.toHaveBeenCalled();
        expect(ds.isSaving).toBe(false);
      });

      it('Save Test (id is not set) save success', function () {
        var item = { id: null, errorTestFlag: false };
        ds.item = item;
        ds.save();
        expect(MockUiModalInstance.close).toHaveBeenCalled();
        expect(ds.isSaving).toBe(false);
      });

      it('Save Test (id is not set) save error', function () {
        var item = { id: null, errorTestFlag: true };
        ds.item = item;
        ds.save();
        expect(MockUiModalInstance.close).not.toHaveBeenCalled();
        expect(ds.isSaving).toBe(false);
      });

      it('Clear Test', function () {
        ds.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });
    })
  });
});
