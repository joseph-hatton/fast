'use strict';

describe('Controller Tests', function() {

  describe('Home Controller', function() {
    var Ctrl,MockPrincipal,$rootScope,MockTreatyAmendmentsNoPage,MockTreatySrv,MockTreatyTrackingCustomSvr;
    var scope,state,paginationConstants,pagingParams,MockGfsAssignee,MockIntro;
    var DeferFactory, tmSrvDefer,account,introSrvDefer,defer,MockTreatyTransaction;

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller,$injector,$q,$http,$log,$window,_DeferFactory_) {
        DeferFactory = _DeferFactory_;   // Don't bother injecting a 'real' modal
        $rootScope = $injector.get('$rootScope');
        scope = $rootScope.$new();
        state =$rootScope.$state;
        pagingParams={ascending:true,page:1,predicate:"id",search:null,sort:"id,asc"};
        paginationConstants= {itemsPerPage: 10};
        MockIntro = jasmine.createSpyObj('Intro', ['setupIntroSteps']);
        introSrvDefer = DeferFactory.generate();
        MockIntro.setupIntroSteps.and.returnValue(introSrvDefer.promise);
        MockPrincipal = jasmine.createSpyObj('MockPrincipal', ['identity']);
        account={login:'123',authorities:['ROLE_APP_FINRE_OPS_ADMIN']};
        tmSrvDefer = DeferFactory.generate();
        MockPrincipal.identity.and.returnValue(tmSrvDefer.promise);

        MockTreatySrv=jasmine.createSpyObj('TreatySrv', ['notificationInputStatus']);
        MockTreatySrv.notificationInputStatus={query:function(){}};

        MockGfsAssignee=jasmine.createSpyObj('GfsAssignee', ['query']);
        MockGfsAssignee.query=function(){};
        defer = DeferFactory.generate();

        MockTreatyTrackingCustomSvr = {
          seritatimDueDate: jasmine.createSpyObj('seritatimDueDate', ['get']),
          seriatimDueDateExpired: jasmine.createSpyObj('seriatimDueDateExpired', ['query']),
          settlementCedingExpired: jasmine.createSpyObj('settlementCedingExpired', ['query']),
          settlementAssumingExpired: jasmine.createSpyObj('settlementAssumingExpired', ['query']),
          clearedReportingNotifications: jasmine.createSpyObj('clearedReportingNotifications', ['query']),
          reportingDueDateExpired: jasmine.createSpyObj('reportingDueDateExpired', ['query']),
          reportingDueDatePastGracePeriod: jasmine.createSpyObj('reportingDueDatePastGracePeriod', ['query']),
          expirationReportingDue: jasmine.createSpyObj('expirationReportingDue', ['query']),
          assumingAlmostPastDue: jasmine.createSpyObj('assumingAlmostPastDue', ['query']),
          assumingAlmostExpired: jasmine.createSpyObj('assumingAlmostExpired', ['query']),
          assumingExpired: jasmine.createSpyObj('assumingExpired', ['query']),
          cedingAlmostPastDue: jasmine.createSpyObj('cedingAlmostPastDue', ['query']),
          cedingAlmostExpired: jasmine.createSpyObj('cedingAlmostExpired', ['query']),
          cedingExpired: jasmine.createSpyObj('cedingExpired', ['query']),
          expirationAssuming: jasmine.createSpyObj('expirationAssuming', ['query']),
          expirationCeding: jasmine.createSpyObj('expirationCeding', ['query']),
          reportingDueExpired: jasmine.createSpyObj('reportingDueExpired', ['query'])
        };

        MockTreatyTrackingCustomSvr.seritatimDueDate.get.and.returnValue(defer);
        MockTreatyTrackingCustomSvr.seriatimDueDateExpired.query.and.returnValue(defer);
        MockTreatyTrackingCustomSvr.settlementCedingExpired.query.and.returnValue(defer);
        MockTreatyTrackingCustomSvr.settlementAssumingExpired.query.and.returnValue(defer);
        MockTreatyTrackingCustomSvr.clearedReportingNotifications.query.and.returnValue(defer);
        MockTreatyTrackingCustomSvr.reportingDueDateExpired.query.and.returnValue(defer);
        MockTreatyTrackingCustomSvr.reportingDueDatePastGracePeriod.query.and.returnValue(defer);
        MockTreatyTrackingCustomSvr.expirationReportingDue.query.and.returnValue(defer);
        MockTreatyTrackingCustomSvr.assumingAlmostPastDue.query.and.returnValue(defer);
        MockTreatyTrackingCustomSvr.assumingAlmostExpired.query.and.returnValue(defer);
        MockTreatyTrackingCustomSvr.assumingExpired.query.and.returnValue(defer);
        MockTreatyTrackingCustomSvr.cedingAlmostPastDue.query.and.returnValue(defer);
        MockTreatyTrackingCustomSvr.cedingAlmostExpired.query.and.returnValue(defer);
        MockTreatyTrackingCustomSvr.cedingExpired.query.and.returnValue(defer);
        MockTreatyTrackingCustomSvr.expirationAssuming.query.and.returnValue(defer);
        MockTreatyTrackingCustomSvr.expirationCeding.query.and.returnValue(defer);
        MockTreatyTrackingCustomSvr.reportingDueExpired.query.and.returnValue(defer);

        Ctrl = $controller('HomeController', {
          $scope: scope,
          $window: $window,
          $state: state,
          Principal: MockPrincipal,
          $rootScope:$rootScope,
         // 'LoginService': MockLoginService,
          //'TreatyAmendmentsNoPage': MockTreatyAmendmentsNoPage,
          TreatySrv: MockTreatySrv,
          TreatyTrackingCustomSvr:MockTreatyTrackingCustomSvr,
          GfsAssignee: MockGfsAssignee,
         // 'TreatyTransactionSrv':MockTreatyTransactionSrv,
          //'TreatyTransaction':MockTreatyTransaction,
          Intro:MockIntro,
         $q:$q,
         // 'InteractionsService':MockInteractionsService,
          //'TreatyTracking':MockTreatyTracking,
          $log:$log,
          $http:$http
        });
      }));


    describe('Initial state', function () {
      it('Home Controller', function() {
        tmSrvDefer.resolve(account);
        expect(Ctrl).toBeDefined();
      });

  });

  });
});
