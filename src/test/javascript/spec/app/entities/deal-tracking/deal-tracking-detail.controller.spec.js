'use strict';

describe('Controller Tests', function() {

    describe('DealTracking Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockDealTracking;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockDealTracking = jasmine.createSpy('MockDealTracking');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'DealTracking': MockDealTracking
            };
            createController = function() {
                $injector.get('$controller')("DealTrackingDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'finreApp:dealTrackingUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
