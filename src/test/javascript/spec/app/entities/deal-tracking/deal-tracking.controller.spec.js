'use strict';

describe('Controller Tests', function() {

  describe('DealTracking Controller', function() {
    var Ctrl;
    var scope,state,paginationConstants,pagingParams, MockDeal, MockDealTrackingFilterSrv, MockParseLinks, MockDealTrackingSrv;
    var resultDealGet = [{}];
    var data,MockIntro,deferPrincipal, data1,$rootScope,$scope,DeferFactory,MockPrincipal;

// Initialize the controller and a mock scope
    beforeEach(inject(function ($injector, $controller, _DeferFactory_ ,$stateParams,DateUtils,$q) {
      DeferFactory = _DeferFactory_;
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
        pagingParams={ascending:true,page:1,predicate:"id",search:null,sort:"id,asc"};
        paginationConstants= {itemsPerPage: 10};
        MockDeal = {
          get: function (id, callback) {
            callback(resultDealGet);
          }
        };
        MockPrincipal = jasmine.createSpyObj('MockPrincipal', ['identity']);
        deferPrincipal = DeferFactory.generate();

        MockPrincipal.identity.and.returnValue(deferPrincipal);
      MockIntro =jasmine.createSpyObj('Intro', ['setupIntroSteps']);

      MockIntro.setupIntroSteps.and.returnValue(DeferFactory.generate().promise);
        MockDealTrackingFilterSrv = {
          reportingReport: {
            get: function(query, success, error){
              data = [{ reportingPeriodEndDate: '2017-01-01' }];
              success(data, function (message) {return message});
            }
          }
        };

        MockParseLinks = {
          parse: function (message) {
            return message;
          }
        };

        MockDealTrackingSrv = {
          'base': {
            query: function (entry, success, error) {
              data1 = [{reportingPeriodEndDate: '2017-01-01'}];
              success(data1, function (message) {
                return message;
              });
            }
          }
        };

        Ctrl = $controller('DealTrackingController', {
          $scope: scope,
          $state: state,
          paginationConstants:paginationConstants,
          pagingParams:pagingParams,
          Deal: MockDeal,
          Intro:MockIntro,
          DealTrackingFilterSrv: MockDealTrackingFilterSrv,
          ParseLinks: MockParseLinks,
          DealTrackingSrv: MockDealTrackingSrv,
          $q: $q
        });
      }));

    describe('Initial state', function () {
      it('DealTracking Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('DealTracking Load All Test', function() {
        expect(Ctrl.loadAll).toBeDefined();
      });

      it('DealTracking Search Test', function() {
        expect(Ctrl.search).toBeDefined();
      });
    });

    describe('Main Content Coverage', function () {
      it('Load All On Success Test', function () {
        var expectedMonth = "January", expectedQuarter = "2017 Q1";
        Ctrl.page = 5;
        Ctrl.searchStartDate = new Date("2017-01-15");
        Ctrl.searchEndDate = new Date("2017-01-15");
        pagingParams.search = true;

        Ctrl.loadAll();

        expect(data1[0].month).toBe(expectedMonth);
        expect(data1[0].reportingPeriodEndDate).toBe(expectedQuarter);
        expect(Ctrl.page).toBe(1);
      });

      it('On Success Date Filter Test', function () {
        var expectedMonth = "January", expectedQuarter = "2017 Q1";
        Ctrl.page = 5;
        Ctrl.searchStartDate = new Date("2017-01-15");
        Ctrl.searchEndDate = new Date("2017-01-15");

        Ctrl.filterByReportingPeriod();

        expect(data[0].month).toBe(expectedMonth);
        expect(data[0].reportingPeriodEndDate).toBe(expectedQuarter);
      });

      it('openCalendar Test', function () {
        expect(Ctrl.datePickerOpenStatus['searchStartDate']).toBe(false);
        Ctrl.openCalendar('searchStartDate');
        expect(Ctrl.datePickerOpenStatus['searchStartDate']).toBe(true);
      });

      it('Deal Get Test', function () {
        expect(Ctrl.deal).toBe(resultDealGet);
      });

      it('LoadPageTest', function () {
        Ctrl.page = 1;
        expect(Ctrl.page).toBe(1);
        Ctrl.transition = function () { };
        Ctrl.loadPage(2);
        expect(Ctrl.page).toBe(2);
      });

      it('Search No Query Test', function () {
        Ctrl.transition = function(){};
        Ctrl.clear = jasmine.createSpy('clear');
        Ctrl.search(null);
        expect(Ctrl.clear).toHaveBeenCalled();
      });

      it('Search with Query Test', function () {
        Ctrl.transition = function(){};
        Ctrl.clear = jasmine.createSpy('clear');
        Ctrl.searchQuery = [{}];
        Ctrl.search([{}]);
        expect(Ctrl.clear).not.toHaveBeenCalled();
      });

      it('Clear Test', function () {
        Ctrl.transition = function(){};
        Ctrl.page = 5;
        Ctrl.reverse = false;
        Ctrl.clear();
        expect(Ctrl.page).toBe(1);
        expect(Ctrl.reverse).toBe(true);
      });

      it('Reset Test', function () {
        Ctrl.loadAll = function(){};
        Ctrl.searchStartDate = {};
        Ctrl.searchEndDate = {};
        Ctrl.reset();
        expect(Ctrl.searchStartDate).toBeNull();
        expect(Ctrl.searchEndDate).toBeNull();
      });

      it('ForDelete tracking.checked true Test', function () {
        Ctrl.deleteMultipleTracking = [];
        var tracking = {checked: true};

        Ctrl.forDelete(tracking);

        expect(Ctrl.deleteMultipleTracking.length).toBe(1);
      });

      it('ForDelete tracking.checked false Test', function () {
        Ctrl.deleteMultipleTracking = [];
        var tracking = {id:1, checked: false};
        var tracking2 = {id:1};
        Ctrl.deleteMultipleTracking.push(tracking2);

        Ctrl.forDelete(tracking);

        expect(Ctrl.deleteMultipleTracking.length).toBe(0);
      });

      it('deleteFlagChange flag false test', function () {
        Ctrl.deleteFlag = false;
        Ctrl.dealTrackings = [{checked: true}];
        Ctrl.deleteFlagChange();
        expect(Ctrl.dealTrackings[0].checked).toBe(false);
      });

      it('deleteFlagChange flag true test', function () {
        Ctrl.deleteFlag = true;
        Ctrl.dealTrackings = [{checked: true}];
        Ctrl.deleteFlagChange();
        expect(Ctrl.dealTrackings[0].checked).toBe(true);
      });
    });

    describe('Account Load', function () {

      it('Principal Account', function () {
        it('Principal Account', function () {
          var account={login:'123',authorities:['ROLE_APP_FINRE_OPS_ADMIN']};
          deferPrincipal.resolve(account);
          $rootScope.$apply();
          expect(MockPrincipal.identity).toHaveBeenCalled();
          expect(Ctrl.lastUser).toBe('123');
          expect(Ctrl.authority).toBe('Admin');
        });
        it('User Account', function () {
          var account={login:'123',authorities:['ROLE_APP_FINRE']};
          deferPrincipal.resolve(account);
          $rootScope.$apply();
          expect(MockPrincipal.identity).toHaveBeenCalled();
          expect(Ctrl.lastUser).toBe('123');
          expect(Ctrl.authority).toBe('User');
        });
      });

    });


  });
});
