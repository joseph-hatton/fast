'use strict';

describe('Controller Tests', function() {
  describe('DealTracking Delete Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockDealTracking, Mockdeal, MockUiModalInstance;
    var MockEntity, MockPreviousState;
    var info =1;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = {};
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        MockDealTracking = {
          update: function (idObj, callback) {
            callback(info);
          }
        };

        Ctrl = $controller('DealTrackingDeleteController', {
          $scope: scope,
          $state: state, entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          DealTracking: MockDealTracking
        });
      }));

    describe('Initial state', function () {
      it('DealTrackingDeleteController  Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('clear', function() {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });

      it('confirmDelete', function() {
        expect(Ctrl.confirmDelete).toBeDefined();
        Ctrl.confirmDelete(123);
      });
    });

  });
});
