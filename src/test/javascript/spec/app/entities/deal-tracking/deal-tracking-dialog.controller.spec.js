'use strict';

describe('Controller Tests', function() {
  describe('DealTracking Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockDealTracking, MockDeal, Mockdeal, MockUiModalInstance;
    var MockEntity, MockPreviousState;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = {};
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        MockDealTracking = {
          get: function () {},
          update: function (dealTrackingObj, success, error) {
            success(dealTrackingObj);
          },
          save: function (dealTrackingObj, success, error) {
            error();
          }
        };
        Mockdeal = {};
        MockDeal = {
          get: function (idObj, callback) {
            callback(Mockdeal);
          }
        };
        Ctrl = $controller('DealTrackingDialogController', {
          $scope: scope,
          $state: state, entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          DealTracking: MockDealTracking,
          Deal:MockDeal
        });
      }));

    describe('Initial state', function () {
      it('DealTrackingDialog Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('OpenCalendar', function() {
        expect(Ctrl.openCalendar).toBeDefined();
      });

      it('Save', function() {
        expect(Ctrl.save).toBeDefined();
      });

      it('Clear', function() {
        expect(Ctrl.clear).toBeDefined();
      });

      it('ClearField', function() {
        expect(Ctrl.clearField).toBeDefined();
      });

      it('SaveMessage', function() {
        expect(Ctrl.saveMessage).toBeDefined();
      });

      it('RemainInProcess', function() {
        expect(Ctrl.remainInProcess).toBeDefined();
      });

      it('GetNextReportingPeriodDate', function() {
        expect(Ctrl.getNextReportingPeriodDate).toBeDefined();
      });

      it('NextTrackingRecord', function() {
        expect(Ctrl.nextTrackingRecord).toBeDefined();
      });
    });

    describe('Main Content Coverage', function () {
      describe('OpenCalendar', function () {
        it('should set datePickerOpenStatus to true', function () {
          var item = 'item';
          Ctrl.datePickerOpenStatus[item] = false;
          Ctrl.openCalendar(item);
          expect(Ctrl.datePickerOpenStatus[item]).toBe(true);
        });
      });

      describe('Deal.get', function () {
        it('should callback and update deal and dealTracking.deal', function () {
          expect(Ctrl.deal).toBeDefined();
          expect(Ctrl.dealTracking.deal).toBeDefined();
        });
      });

      describe('onSaveSuccessNextTracking', function () {
        it('should call uiModalInstance.close', function () {
          Ctrl.dealTracking.id = 123;
          Ctrl.dealTracking.dealTrackingStatus = 'Closed';
          MockDealTracking.save = function (item, success, error) {
            success();
          };
          Ctrl.save(0);
          expect(Ctrl.isSaving).toBe(false);
          expect(MockUiModalInstance.close).toHaveBeenCalled();
        });
      });

      describe('Save', function () {
        it('should set dealTrackingStatus to Open when param is 1', function () {
          Ctrl.dealTracking.dealTrackingStatus = 'Closed';
          Ctrl.save(1);
          expect(Ctrl.dealTracking.dealTrackingStatus).toBe('Open');
        });

        it('should not set dealTrackingStatus when param is not 1', function () {
          Ctrl.dealTracking.dealTrackingStatus = 'Closed';
          Ctrl.save(0);
          expect(Ctrl.dealTracking.dealTrackingStatus).toBe('Closed');
        });

        it('should update when id is set', function () {
          Ctrl.dealTracking.id = 123;
          Ctrl.dealTrackingStatus = 'Open';
          Ctrl.save(0);
          expect(Ctrl.isSaving).toBe(false);
          expect(MockUiModalInstance.close).toHaveBeenCalled();
        });

        it('should save when id is not set', function () {
          Ctrl.dealTracking.id = null;
          Ctrl.save(0);
          expect(Ctrl.isSaving).toBe(false);
          expect(MockUiModalInstance.close).not.toHaveBeenCalled();
        });
      });
      describe('Clear', function () {
        it('should run uiModalInstance.dismiss', function () {
          Ctrl.clear();
          expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
        });
      });
      describe('ClearField', function () {
        it('should set financeDate to null when the financeDateFlag is set to 1', function () {
          Ctrl.dealTracking.financeDate = "date";
          Ctrl.dealTracking.financeDateFlag = 1;
          Ctrl.clearField();
          expect(Ctrl.dealTracking.financeDate).toBeNull();
        });
        it('should not set financeDate when the financeDateFlag is not set to 1', function () {
          Ctrl.dealTracking.financeDate = "date";
          Ctrl.dealTracking.financeDateFlag = 0;
          Ctrl.clearField();
          expect(Ctrl.dealTracking.financeDate).toBe("date");
        });
        it('should set reportingArmDate to null when the reportingArmFlag is set to 1', function () {
          Ctrl.dealTracking.reportingArmDate = "date";
          Ctrl.dealTracking.reportingArmFlag = 1;
          Ctrl.clearField();
          expect(Ctrl.dealTracking.reportingArmDate).toBeNull();
        });
        it('should not set reportingArmDate when the reportingArmFlag is not set to 1', function () {
          Ctrl.dealTracking.reportingArmDate = "date";
          Ctrl.dealTracking.reportingArmFlag = 0;
          Ctrl.clearField();
          expect(Ctrl.dealTracking.reportingArmDate).toBe("date");
        });
      });
      describe('VerifySave', function () {
        beforeEach(function () {
          Ctrl.save = jasmine.createSpy('save');
          Ctrl.saveMessage = jasmine.createSpy('saveMessage');
        });
        it('should not call save or saveMessage if reportingDate isn\'t set', function () {
          var editForm = {
            reportingPeriodEndDate: { $invalid:true }
          };
          Ctrl.verifySave(editForm);
          expect(Ctrl.save).not.toHaveBeenCalled();
          expect(Ctrl.saveMessage).not.toHaveBeenCalled();
        });
        it('should call saveMessage but not save(1) if reportingDate is set and the other items aren\'t true', function () {
          Ctrl.dealTracking.reportingArmFlag = 1;
          Ctrl.dealTracking.financeDateFlag = 1;
          var editForm = {
            reportingPeriodEndDate: { $invalid:false }
          };
          Ctrl.verifySave(editForm);
          expect(Ctrl.save).not.toHaveBeenCalled();
          expect(Ctrl.saveMessage).toHaveBeenCalled();
        });
        it('should call saveMessage but not save if reportingDate is set and the other items are: [RM,StatEst] --> [0,1]', function () {
          Ctrl.dealTracking.reportingArmFlag = 1;
          Ctrl.dealTracking.financeDateFlag = 0;
          var editForm = {
            reportingPeriodEndDate: { $invalid:false },
            reportArmDate: { $invalid:true },
            financeDate: { $invalid: false }
          };
          Ctrl.verifySave(editForm);
          expect(Ctrl.save).not.toHaveBeenCalled();
          expect(Ctrl.saveMessage).toHaveBeenCalled();
        });
        it('should call saveMessage but not save if reportingDate is set and the other items are: [RM,StatEst] --> [1,0]', function () {
          Ctrl.dealTracking.reportingArmFlag = 0;
          Ctrl.dealTracking.financeDateFlag = 1;
          var editForm = {
            reportingPeriodEndDate: { $invalid:false },
            reportArmDate: { $invalid:false },
            financeDate: { $invalid: true }
          };
          Ctrl.verifySave(editForm);
          expect(Ctrl.save).not.toHaveBeenCalled();
          expect(Ctrl.saveMessage).toHaveBeenCalled();
        });
        it('should call saveMessage but not save if reportingDate is set and the other items are: [RM,StatEst] --> [1,1]', function () {
          Ctrl.dealTracking.reportingArmFlag = 0;
          Ctrl.dealTracking.financeDateFlag = 0;
          var editForm = {
            reportingPeriodEndDate: { $invalid:false },
            reportArmDate: { $invalid:false },
            financeDate: { $invalid: false }
          };
          Ctrl.verifySave(editForm);
          expect(Ctrl.save).not.toHaveBeenCalled();
          expect(Ctrl.saveMessage).toHaveBeenCalled();
        });
        it('should call save but not saveMessage if reportingDate is set and the other items are: [RM,StatEst] --> [0,1]', function () {
          Ctrl.dealTracking.reportingArmFlag = 1;
          Ctrl.dealTracking.financeDateFlag = 0;
          var editForm = {
            reportingPeriodEndDate: { $invalid:false },
            reportArmDate: { $invalid:false },
            financeDate: { $invalid: true }
          };
          Ctrl.verifySave(editForm);
          expect(Ctrl.save).toHaveBeenCalled();
          expect(Ctrl.saveMessage).not.toHaveBeenCalled();
        });
        it('should call save but not saveMessage if reportingDate is set and the other items are: [RM,StatEst] --> [1,0]', function () {
          Ctrl.dealTracking.reportingArmFlag = 0;
          Ctrl.dealTracking.financeDateFlag = 1;
          var editForm = {
            reportingPeriodEndDate: { $invalid:false },
            reportArmDate: { $invalid:true },
            financeDate: { $invalid: false }
          };
          Ctrl.verifySave(editForm);
          expect(Ctrl.save).toHaveBeenCalled();
          expect(Ctrl.saveMessage).not.toHaveBeenCalled();
        });
        it('should call save but not saveMessage if reportingDate is set and the other items are: [RM,StatEst] --> [1,1]', function () {
          Ctrl.dealTracking.reportingArmFlag = 0;
          Ctrl.dealTracking.financeDateFlag = 0;
          var editForm = {
            reportingPeriodEndDate: { $invalid:false },
            reportArmDate: { $invalid:true },
            financeDate: { $invalid: true }
          };
          Ctrl.verifySave(editForm);
          expect(Ctrl.save).toHaveBeenCalled();
          expect(Ctrl.saveMessage).not.toHaveBeenCalled();
        });
      });
    });
  });
});
