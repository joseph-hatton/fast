'use strict';

describe('Controller Tests', function() {

    describe('Treaty Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockTreaty, MockTreatyManagement, MockTreatyTransaction, MockTreatyManagementCustomSrv, MockDeal, MockProduct, MockTreatySrv;
        var Ctrl, MockInteractionsService, MockTreatyTrackingCustomSvr,MockState;
        var q, MockPrincipal,loadAllDefer, DeferFactory, MockIntro;

        beforeEach(inject(function($injector, $controller, $q, _DeferFactory_) {
            DeferFactory = _DeferFactory_;
            q = $q;
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockTreaty = jasmine.createSpyObj('MockTreaty', ['save', 'update']);
            MockTreatyManagement = jasmine.createSpyObj('MockTreatyManagement', ['save']);
            MockTreatyTransaction = jasmine.createSpy('MockTreatyTransaction');
            MockDeal = jasmine.createSpy('MockDeal');
            MockProduct = jasmine.createSpy('MockProduct');
            MockTreatyManagementCustomSrv = jasmine.createSpyObj('TreatyManagementCustomSrv', ['get']);
            MockInteractionsService = jasmine.createSpyObj('InteractionsService', ['confirmBasicDialog', 'showThenDialog', 'confirmDialogWithTitle', 'alertDialogWithTitle', 'alertParentScrollFalseDialog']);
            MockTreatyTrackingCustomSvr = { count: jasmine.createSpyObj('count', ['get']) };
            MockTreatyTrackingCustomSvr.count.get.and.callFake(function () { return DeferFactory.generate(); });
            MockTreatySrv = {
              enterpriseKeyVersion: jasmine.createSpyObj('TreatySrv', ['query'])
            };
            MockState = jasmine.createSpyObj('$state', ['go']);
            loadAllDefer = DeferFactory.generate();
            MockTreatyManagementCustomSrv.get.and.returnValue(loadAllDefer);
            MockIntro = jasmine.createSpyObj('Intro', ['setupIntroSteps']);
            MockIntro.setupIntroSteps.and.returnValue(DeferFactory.generate().$promise);
          MockPrincipal = jasmine.createSpyObj('MockPrincipal', ['identity']);
          MockPrincipal.identity.and.returnValue(DeferFactory.generate().$promise);
            Ctrl = $controller("TreatyDetailController", {
              '$scope': $scope,
              '$rootScope': $rootScope,
              'entity': MockEntity ,
              'Treaty': MockTreaty,
              'TreatyManagement': MockTreatyManagement,
              'TreatyTransaction': MockTreatyTransaction,
              'Deal': MockDeal,
              'Product': MockProduct,
              'TreatyManagementCustomSrv': MockTreatyManagementCustomSrv,
              'InteractionsService': MockInteractionsService,
              'TreatyTrackingCustomSvr': MockTreatyTrackingCustomSvr,
              '$state': MockState,
              'TreatySrv': MockTreatySrv,
              'Intro': MockIntro,
              'Principal': MockPrincipal
            });
        }));

        describe('Initial Setup', function() {
          it('Controller should be defined', function() {
              expect(Ctrl).toBeDefined();
          });
        });
        describe('Main Content', function () {
          describe('LoadAll', function () {
            var result;
            it('should set inputStatus to In Process when id is not set', function () {
              result = { treaty: { deal: { yearEndMonth: 1 } } };
              Ctrl.treaty={ deal: { yearEndMonth: 1 } };
              loadAllDefer.resolve(result);
              $rootScope.$apply();
              Ctrl.loadAll();
              expect(Ctrl.treatyManagement.inputStatus).toBe('In Process');
            });
            it('should set treaty to result.treaty when id is set', function () {
              result = {
                id:321,
                treaty: {id:123, deal:{ yearEndMonth: 1 }},
                riskFees: []
              };

              loadAllDefer.resolve(result);
              $rootScope.$apply();

              Ctrl.loadAll();

              expect(Ctrl.treaty.id).toBe(123);
              expect(Ctrl.treatyManagement.id).toBe(321);
            });
          });
          describe('copyBasicInfo', function () {
            it('should set all ids to null on treatyCopy', function () {
              var cp;
              MockTreaty.save.and.callFake(function (copy) {
                cp = copy;
                return q.defer().promise;
              });
              var treaty = {
                id:123,
                mdmId:123,
                treatyName:"test",
                reportingDueYearEx: { id:123 },
                reportingDueDate: { id:123 },
                reportingDueQuarterEx: { id:123 },
                stmntDueAsm: { id:123 },
                stmntDueAsmBroker: { id:123 },
                stmntDueAsmGracePeriod: { id:123 },
                stmntDueAsmQuarterEx: { id:123 },
                stmntDueCed: { id:123 },
                stmntDueCedGracePeriod: { id:123 },
                stmntDueCedQuarterEx: { id:123 },
                treatyCurrencies: [ {id:123} ],
                deal: {},
                agreementType: {},
                productStructure: {}
              };

              Ctrl.copyBasicInfo(treaty);

              expect(cp).not.toBeNull();
              expect(cp.id).toBeNull();
              expect(cp.mdmId).toBeNull();
              expect(cp.treatyName).toBeNull();
              expect(cp.reportingDueYearEx.id).toBeNull();
              expect(cp.reportingDueDate.id).toBeNull();
              expect(cp.reportingDueQuarterEx.id).toBeNull();
              expect(cp.stmntDueAsm.id).toBeNull();
              expect(cp.stmntDueAsmBroker.id).toBeNull();
              expect(cp.stmntDueAsmGracePeriod.id).toBeNull();
              expect(cp.stmntDueAsmQuarterEx.id).toBeNull();
              expect(cp.stmntDueCed.id).toBeNull();
              expect(cp.stmntDueCedGracePeriod.id).toBeNull();
              expect(cp.stmntDueCedQuarterEx.id).toBeNull();
              expect(cp.treatyCurrencies[0].id).toBeNull();
            });
          });
          describe('copyTreatyManagement', function () {
            it('should save a copy of treaty management for the new treaty', function () {
              var old = {
                id:123,
                riskFees: [ {id:123} ]
              };
              var tmCopy;
              MockTreatyManagement.save.and.callFake(function(copy){
                tmCopy = copy;
                return DeferFactory.generate();
              });

              Ctrl.copyTreatyManagement(old, {});

              expect(tmCopy.id).toBeNull();
              expect(tmCopy.riskFees[0].id).toBeNull();
              expect(MockTreatyManagement.save).toHaveBeenCalled();
            });
          });
          describe('copyTreaty', function () {
            it('should call a dialog, call copyBasicInfo, and call copyTreatyManagement', function () {
              var tmSrvDefer = DeferFactory.generate();
              MockTreatyManagementCustomSrv.get.and.callFake(function () {
                return tmSrvDefer;
              });
              MockInteractionsService.showThenDialog.and.callFake(function (dialog, yes) {
                if(yes)
                  yes();
              });
              var copyBasicInfoDeferred = q.defer();
              Ctrl.copyBasicInfo = jasmine.createSpy('copyBasicInfo').and.callFake(function () {
                return copyBasicInfoDeferred.promise;
              });
              var copyTreatyManagementDeferred = q.defer();
              Ctrl.copyTreatyManagement = jasmine.createSpy('copyTreatyManagement').and.callFake(function () {
                return copyTreatyManagementDeferred.promise;
              });

              Ctrl.copyTreaty({ deal:{}, agreementType:{}, productStructure:{}});

              expect(Ctrl.copyTreatyManagement).not.toHaveBeenCalled();
              copyBasicInfoDeferred.resolve({});
              copyTreatyManagementDeferred.resolve();
              tmSrvDefer.resolve({treaty:{}});
              $rootScope.$apply();

              expect(MockInteractionsService.showThenDialog).toHaveBeenCalledTimes(2);
              expect(Ctrl.copyBasicInfo).toHaveBeenCalled();
              expect(Ctrl.copyTreatyManagement).toHaveBeenCalled();
            });
          });
          describe('updateMdmAttributes', function () {
            it('should update the mdm attributes that need to be updated', function () {
              Ctrl.treatyMdmUpdating = {};
              Ctrl.treatyMdmUpdating.mdmAttributes= [
                {id:0, enterpriseId:0,  legalDoc:'123',lastUser:'abc', content:'c'},
                {id:1, enterpriseId:1,  legalDoc:'123',lastUser:'abc', content:'c'},
                {id:2, enterpriseId:2,  legalDoc:'123',lastUser:'abc', content:'c'},
                {id:3, enterpriseId:3,  legalDoc:'123',lastUser:'abc', content:'c'}
              ];
              var newItems = [
                {id:111, enterpriseId:0, legalDoc:'123',lastUser:'abc', content:'a', defer: DeferFactory.generate()},
                {id:null, enterpriseId:1, legalDoc:'123',lastUser:'abc', content:'a', defer: DeferFactory.generate()},
                {id:111, enterpriseId:2, legalDoc:'123',lastUser:'abc', content:'a', defer: DeferFactory.generate()},
                {id:null, enterpriseId:3, legalDoc:'123',lastUser:'abc', content:'a', defer: DeferFactory.generate()}
              ];
              MockTreatySrv.enterpriseKeyVersion.query.and.callFake(function (params) {
                return newItems[params.id].defer;
              });

              Ctrl.updateMdmAttributes();

              for(var i = 0; i < newItems.length; i++)
                newItems[i].defer.resolve(newItems[i]);
              $rootScope.$apply();

              expect(Ctrl.treatyMdmUpdating.mdmAttributes[0].id).toBe(0);
              expect(Ctrl.treatyMdmUpdating.mdmAttributes[1].id).toBe(1);
              expect(Ctrl.treatyMdmUpdating.mdmAttributes[2].id).toBe(2);
              expect(Ctrl.treatyMdmUpdating.mdmAttributes[3].id).toBe(3);

              expect(Ctrl.treatyMdmUpdating.mdmAttributes[0].content).toBe('a');
              expect(Ctrl.treatyMdmUpdating.mdmAttributes[1].content).toBe('c');
              expect(Ctrl.treatyMdmUpdating.mdmAttributes[2].content).toBe('a');
              expect(Ctrl.treatyMdmUpdating.mdmAttributes[3].content).toBe('c');

              expect(MockTreaty.update).toHaveBeenCalledTimes(1);
            });
          });
        });
    });

});
