'use strict';

describe('Controller Tests', function () {
    describe('Treaty Dialog Controller', function () {
        var Ctrl;
        var scope, state, DeferFactory, $rootScope;
        var MockEntity, MockXrefTreatyCustomCompany, MockStateParams, MockUiModalInstance, MockTreatyStatus;
        var MockFrequency, MockDeal, MockProductStructure, MockAgreementType, MockCurrency, MockSeriatimType;
        var MockPrincipal, MockTreatySrv, MockInteractionsService,deferPrincipal;

          beforeEach(inject(function ($injector, $controller, _DeferFactory_ ,$stateParams,DateUtils,$q) {
            DeferFactory = _DeferFactory_;
            $rootScope = $injector.get('$rootScope');
            scope = $rootScope.$new();
            state = $rootScope.$state;
            var stubDefer = DeferFactory.generate();
            deferPrincipal= DeferFactory.generate();
            MockEntity = {seriatimTypeTreaties: [], reportingDueDate: {}, stmntDueAsm: {}, stmntDueAsmGracePeriod: {}};
            MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
            MockTreatyStatus = jasmine.createSpyObj('TreatyStatus', ['query']);
            MockFrequency = jasmine.createSpyObj('Frequency', ['query']);
            MockFrequency.query.and.returnValue(stubDefer);
            MockProductStructure = jasmine.createSpyObj('ProductStructure', ['query']);
            MockProductStructure.query.and.returnValue(stubDefer);
            MockAgreementType = jasmine.createSpyObj('AgreementType', ['query']);
            MockAgreementType.query.and.returnValue(stubDefer);
            MockCurrency = jasmine.createSpyObj('Currency', ['query']);
            MockCurrency.query.and.returnValue(stubDefer);
            MockSeriatimType = jasmine.createSpyObj('SeriatimType', ['query']);
            MockSeriatimType.query.and.returnValue(stubDefer);
            MockDeal = jasmine.createSpyObj('Deal', ['get']);
            MockDeal.get.and.returnValue(stubDefer);
             MockPrincipal = jasmine.createSpyObj('MockPrincipal', ['identity']);
            MockPrincipal.identity.and.returnValue(deferPrincipal.promise);
            MockXrefTreatyCustomCompany = {
                treaties: jasmine.createSpyObj('treaties', ['query'])
            };
            MockTreatySrv = {
                mdmTreaty: jasmine.createSpyObj('mdmTreaty', ['query'])
            };
            MockInteractionsService = jasmine.createSpyObj('InteractionsService', ['alertParentScrollFalseDialog', 'showThenDialog', 'confirmBasicParentNoScrollDialog', 'showFinallyDialog']);
            MockStateParams = {};
            Ctrl = $controller('TreatyDialogController', {
                $scope: scope,
                $state: state,
                Principal: MockPrincipal,
                entity: MockEntity,
                $uibModalInstance: MockUiModalInstance,
                XrefTreatyCustomCompany: MockXrefTreatyCustomCompany,
                $stateParams: MockStateParams,
                TreatyStatus: MockTreatyStatus,
                Frequency: MockFrequency,
                Deal: MockDeal,
                ProductStructure: MockProductStructure,
                AgreementType: MockAgreementType,
                Currency: MockCurrency,
                SeriatimType: MockSeriatimType,
                TreatySrv: MockTreatySrv,
                InteractionsService: MockInteractionsService,
                $q: $q
            });
        }));

        describe('Initial state', function () {
            it('TreatyDialog Controller', function () {
                expect(Ctrl).toBeDefined();

            });
        });

        describe('Main Content', function(){
            describe('load', function(){
                var defer;
                beforeEach(function(){
                    defer = DeferFactory.generate();
                    MockXrefTreatyCustomCompany.treaties.query.and.returnValue(defer);
                    MockEntity.deal = {productSubtype:{}};
                });
                it('should call XrefTreatyCustomCompany.treaties.query when stateParams.id is set', function(){
                    MockStateParams.id = 1;
                    Ctrl.load();
                    expect(MockXrefTreatyCustomCompany.treaties.query).toHaveBeenCalled();
                });
                it('should not call XrefTreatyCustomCompany.treaties.query when stateParams.id is not set', function(){
                    Ctrl.load();
                    expect(MockXrefTreatyCustomCompany.treaties.query).not.toHaveBeenCalled();
                });
                it('should set reportingConfirmDateFlag to true if it is a rga company and the company type is ceding', function(){
                    MockStateParams.id = 1;
                    Ctrl.load();

                    defer.resolve([{
                        companyType: "Ceding",
                        company: { isRgaCompany: 1 }
                    }]);
                    $rootScope.$apply();

                    expect(Ctrl.reportingConfirmDateFlag).toBe(true);
                });
                it('should set reportingConfirmDateFlag to false if it is not a rga company and the company type is ceding', function(){
                    MockStateParams.id = 1;
                    Ctrl.load();

                    defer.resolve([{
                        companyType: "Ceding",
                        company: { isRgaCompany: 0 }
                    }]);
                    $rootScope.$apply();

                    expect(Ctrl.reportingConfirmDateFlag).toBe(false);
                });
                it('should not set reportingConfirmDateFlag if the company type is not ceding', function(){
                    MockStateParams.id = 1;
                    Ctrl.reportingConfirmDateFlag = "test";
                    Ctrl.load();

                    defer.resolve([{
                        companyType: "Assuming"
                    }]);
                    $rootScope.$apply();

                    expect(Ctrl.reportingConfirmDateFlag).toBe("test");
                });
                describe('Frequency.query', function(){
                    var defer;
                    beforeEach(function(){
                        defer = DeferFactory.generate();
                        MockFrequency.query.and.returnValue(defer);
                    });
                    it("should not set the brokerFeeSwitchFlag if it already set", function(){
                        MockStateParams.id = 1;
                        Ctrl.treaty.treatyCurrencies = [];
                        Ctrl.latepaymentFn = jasmine.createSpy('latepaymentFn');
                        Ctrl.changeReportingFrequency = jasmine.createSpy('changeReportingFrequency');
                        Ctrl.changeFrequencyDueAssuming = jasmine.createSpy('changeFrequencyDueAssuming');
                        Ctrl.changeFrequencyDueCeding = jasmine.createSpy('changeFrequencyDueCeding');
                        Ctrl.treaty.brokerFeeSwitchFlag = 'Do not touch';

                        Ctrl.load();
                        defer.resolve([]);
                        $rootScope.$apply();

                        expect(Ctrl.treaty.brokerFeeSwitchFlag).toBe('Do not touch');
                    });
                });
                describe('ProductStructure.query', function(){
                    var defer;
                    beforeEach(function(){
                        defer = DeferFactory.generate();
                        MockProductStructure.query.and.returnValue(defer);
                        Ctrl.treaty.productStructure = null;
                    });
                    it('should set structureOptions to result', function(){
                      var result = [
                        {name:'Other - Structured Finance'},
                        {name:'test'}
                      ];
                      Ctrl.treaty.deal.productSubtype.name = 'something else';

                        defer.resolve(result);
                        $rootScope.$apply();

                        expect(Ctrl.structureOptions).not.toBeNull();
                    });
                    it('should not set productStrucutre if the subtype is not Structured', function(){
                        var result = [
                             {name:'Other - Structured Finance'},
                             {name:'test'}
                        ];
                        Ctrl.treaty.deal.productSubtype.name = 'something else';

                        Ctrl.load();
                        defer.resolve(result);
                        $rootScope.$apply();

                        expect(Ctrl.treaty.productStructure).toBeNull();
                    });
                });
            });
            describe('selectFirstItem', function(){
                it('should not select anything if seriatimDueDateFlag is false', function(){
                    Ctrl.dueDateSelected = null;
                    Ctrl.treaty = {
                        seriatimTypeTreaties: [
                            { seriatimDueDateFlag:"No" }
                        ]
                    };

                    Ctrl.selectFirstItem();

                    expect(Ctrl.dueDateSelected).toBeNull();
                });
                it('should select the first item if the due date is yes', function(){
                    Ctrl.dueDateSelected = null;
                    Ctrl.treaty = {
                        seriatimTypeTreaties: [
                            { seriatimDueDateFlag:"Yes" },
                            { seriatimDueDateFlag:"Yes" }
                        ]
                    };

                    Ctrl.selectFirstItem();

                    expect(Ctrl.dueDateSelected).toBe(0);
                });
                it('should select the second item if the due date is no on the first but yes on the second', function(){
                    Ctrl.dueDateSelected = null;
                    Ctrl.treaty = {
                        seriatimTypeTreaties: [
                            { seriatimDueDateFlag:"No" },
                            { seriatimDueDateFlag:"Yes" }
                        ]
                    };

                    Ctrl.selectFirstItem();

                    expect(Ctrl.dueDateSelected).toBe(1);
                });
                it('should not auto select anything when an item is already selected', function(){
                    Ctrl.dueDateSelected = null;
                    Ctrl.treaty = {
                        seriatimTypeTreaties: [
                            { seriatimDueDateFlag:"Yes" },
                            { seriatimDueDateFlag:"Yes", calculationFlag:true }
                        ]
                    };

                    Ctrl.selectFirstItem();

                    expect(Ctrl.dueDateSelected).toBe(1);
                });
            });
            it('cleanImport should set ekExist to null', function(){
                Ctrl.ekExist = {};

                Ctrl.cleanImport();

                expect(Ctrl.ekExist).toBeNull();
            });
            describe('importEK', function(){
                beforeEach(function(){
                    Ctrl.ekData = {
                        businessUnitName:''
                    };
                });
                it('should set usStatRisk to Yes if B', function(){
                    Ctrl.ekData.usStatRisk = 'B';

                    Ctrl.importEK();

                    expect(Ctrl.ekData.usStatRisk).toBe('Yes');
                });
                it('should set usStatRisk to Yes if S', function(){
                    Ctrl.ekData.usStatRisk = 'S';

                    Ctrl.importEK();

                    expect(Ctrl.ekData.usStatRisk).toBe('Yes');
                });
                it('should set usStatRisk to No if anything else', function(){
                    Ctrl.ekData.usStatRisk = 'Z';

                    Ctrl.importEK();

                    expect(Ctrl.ekData.usStatRisk).toBe('No');
                });
                it('should set effectiveStatusCd to Yes if A', function(){
                    Ctrl.ekData.effectiveStatusCd = 'A';

                    Ctrl.importEK();

                    expect(Ctrl.ekData.effectiveStatusCd).toBe('Active');
                });
                it('should set effectiveStatusCd to No if anything else', function(){
                    Ctrl.ekData.effectiveStatusCd = 'Z';

                    Ctrl.importEK();

                    expect(Ctrl.ekData.effectiveStatusCd).toBe('Inactive');
                });
                describe('if mdmOn is 0', function(){
                    beforeEach(function(){
                        Ctrl.mdmOn = 0;
                        Ctrl.searchCurrecyByCode = jasmine.createSpy('searchCurrecyByCode');
                        MockEntity.mdmAttributes = [];
                    });
                    it('and productSubtype is Brokered should add mdmAttributes, call searchCurrencyByCode, and set mdmOn to 1', function(){
                        Ctrl.treaty.deal = {
                            productSubtype: { name:'Brokered' }
                        };

                        Ctrl.importEK();

                        expect(Ctrl.treaty.mdmAttributes.length).toBe(1);
                        expect(Ctrl.searchCurrecyByCode).toHaveBeenCalled();
                        expect(Ctrl.mdmOn).toBe(1);
                    });
                    describe('and productSubtype is not Brokered', function(){
                        var defer;
                        beforeEach(function(){
                            Ctrl.treaty.deal = {
                                productSubtype: { name:'Non-Brokered' }
                            };
                            defer = DeferFactory.generate();
                            MockTreatySrv.mdmTreaty.query.and.returnValue(defer);
                        });
                        it('should call TreatySrv.mdmTreaty.query', function(){
                            Ctrl.importEK();

                            expect(MockTreatySrv.mdmTreaty.query).toHaveBeenCalled();
                        });
                        it('should not create a confirm dialog when matches are not found and productStructure\'s do match', function(){
                            Ctrl.ekData.productStructure = {name:'ps'};
                            Ctrl.treaty.productStructure = {name:'ps'};

                            Ctrl.importEK();
                            defer.resolve([]);
                            $rootScope.$apply();

                            expect(MockInteractionsService.alertParentScrollFalseDialog).not.toHaveBeenCalled();
                        });
                    });
                });
            });
            describe('disableAlreadyAddedCurrencies', function(){
                beforeEach(function(){
                    Ctrl.currencyOptions = [
                        { code: "USD", name: "US Dollar" },
                        { code: "CAD", name: "Canadian Dollar" }
                    ];
                    MockEntity.treatyCurrencies = [
                        { currency: { code: "USD", name: "US Dollar" } }
                    ];
                });
                it('should disable the items in the treatyCurrencies in the main dropdown', function(){
                    Ctrl.disableAlreadyAddedCurrencies();

                    expect(Ctrl.currencyOptions[0].disabled).toBeTruthy();
                    expect(Ctrl.currencyOptions[0].name).toBe("US Dollar - Already Added");

                    expect(Ctrl.currencyOptions[1].disabled).toBeFalsy();
                    expect(Ctrl.currencyOptions[1].name).toBe("Canadian Dollar");
                });
            });
            describe('changeReportingFrequency', function(){
                describe('if reportingFrequency is set', function(){
                    beforeEach(function(){
                        MockEntity.reportingFrequency = {};
                        Ctrl.changeFlagDate = jasmine.createSpy('changeFlagDate');
                        Ctrl.changYearExceptFlag = jasmine.createSpy('changYearExceptFlag');
                    });
                    it('should set reporting due date if it isn\'t set', function(){
                        Ctrl.treaty.reportingDueDate = null;

                        Ctrl.changeReportingFrequency();

                        expect(Ctrl.treaty.reportingDueDate).not.toBeNull();
                    });
                    it('should not set reporting due date if it is set', function(){
                        Ctrl.treaty.reportingDueDate = { id:1 };

                        Ctrl.changeReportingFrequency();

                        expect(Ctrl.treaty.reportingDueDate.id).toBe(1);
                    });
                    it('when rptingFreq is Monthly should set options to monthly options', function(){
                        Ctrl.DayTypeOptionsRep = {};
                        Ctrl.treaty.reportingFrequency.name = 'Monthly';

                        Ctrl.changeReportingFrequency();

                        expect(Ctrl.DayTypeOptionsRep).toEqual(Ctrl.MonthlyDayTypeOptions);
                        expect(Ctrl.treaty.rptDueMonthFlag).not.toBe("No");
                        expect(Ctrl.changeFlagDate).not.toHaveBeenCalled();
                    });
                    it('when rptingFreq is Quarterly should set options to quarterly options', function(){
                        Ctrl.DayTypeOptionsRep = {};
                        Ctrl.treaty.reportingFrequency.name = 'Quarterly';

                        Ctrl.changeReportingFrequency();

                        expect(Ctrl.DayTypeOptionsRep).toEqual(Ctrl.QuarterDayTypeOptions);
                        expect(Ctrl.treaty.rptDueMonthFlag).toBe("No");
                        expect(Ctrl.changeFlagDate).toHaveBeenCalled();
                    });
                    it('when rptingFreq is Semi-Annual should set options to Semi-Annual options', function(){
                        Ctrl.DayTypeOptionsRep = {};
                        Ctrl.treaty.reportingFrequency.name = 'Semi-Annual';

                        Ctrl.changeReportingFrequency();

                        expect(Ctrl.DayTypeOptionsRep).toEqual(Ctrl.SemiAnnualDayTypeOptions);
                        expect(Ctrl.treaty.rptDueMonthFlag).toBe("No");
                        expect(Ctrl.changeFlagDate).toHaveBeenCalled();
                    });
                    it('when rptingFreq is Annual should set options to Annual options and not set number of days to 0 if dayType is not blank', function(){
                        Ctrl.DayTypeOptionsRep = {};
                        Ctrl.treaty.reportingFrequency.name = 'Annual';
                        Ctrl.treaty.reportingDueDate = { dayType: 'test' };

                        Ctrl.changeReportingFrequency();

                        expect(Ctrl.DayTypeOptionsRep).toEqual(Ctrl.AnnualDayTypeOptions);
                        expect(Ctrl.treaty.rptDueMonthFlag).toBe("No");
                        expect(Ctrl.treaty.yearEndException).toBe("No");
                        expect(Ctrl.changeFlagDate).toHaveBeenCalled();
                        expect(Ctrl.changYearExceptFlag).toHaveBeenCalled();
                        expect(Ctrl.treaty.reportingDueDate.numberOfDays).not.toBe(0);
                    });
                    it('when dayType is Year End number of days is set to 0', function(){
                        Ctrl.treaty.reportingFrequency.name = 'Annual';
                        Ctrl.treaty.reportingDueDate = { dayType: 'Year End' };

                        Ctrl.changeReportingFrequency();

                        expect(Ctrl.treaty.reportingDueDate.numberOfDays).toBe(0);
                    });
                    it('when dayType is Half Year number of days is set to 0', function(){
                        Ctrl.treaty.reportingFrequency.name = 'Annual';
                        Ctrl.treaty.reportingDueDate = { dayType: 'Half Year' };

                        Ctrl.changeReportingFrequency();

                        expect(Ctrl.treaty.reportingDueDate.numberOfDays).toBe(0);
                    });
                    it('when dayType is Month End number of days is set to 0', function(){
                        Ctrl.treaty.reportingFrequency.name = 'Annual';
                        Ctrl.treaty.reportingDueDate = { dayType: 'Month End' };

                        Ctrl.changeReportingFrequency();

                        expect(Ctrl.treaty.reportingDueDate.numberOfDays).toBe(0);
                    });
                    it('when dayType is none of the needed number of days is set to 0', function(){
                        Ctrl.treaty.reportingFrequency.name = 'Annual';
                        Ctrl.treaty.reportingDueDate = { dayType: 'test' };

                        Ctrl.changeReportingFrequency();

                        expect(Ctrl.treaty.reportingDueDate.numberOfDays).not.toBe(0);
                    });
                });
                it('if reportingFrequency isn\'t set', function(){
                    Ctrl.changeReportingFrequency();

                    expect(Ctrl.treaty.reportingDueDate).toBeNull();
                    expect(Ctrl.treaty.reportingDueYearEx).toBeNull();
                    expect(Ctrl.treaty.yearEndException).toBe('No');
                });
            });
            describe('vm.changeFrequencyDueAssuming', function(){
                describe('when settlementFrequency is set', function(){
                    beforeEach(function(){
                        MockEntity.settlementFrequency = {};
                        Ctrl.periodOptionDue = ['test1'];
                        Ctrl.DayTypeOptions = ['test2'];
                        MockEntity.settlementFrequency = {};
                        Ctrl.changeFlagDate = jasmine.createSpy('changeFlagDate');
                    });
                    it('should set stmndDueAsm and stmndDueAsmGracePeiod if not set', function(){
                        Ctrl.changeFrequencyDueAssuming();

                        expect(Ctrl.treaty.stmntDueAsm).not.toBeUndefined();
                        expect(Ctrl.treaty.stmntDueAsmGracePeriod).not.toBeUndefined();
                    });
                    it('should not set stmndDueAsm and stmndDueAsmGracePeiod if set', function(){
                        Ctrl.treaty.stmntDueAsm = { id:1 };
                        Ctrl.treaty.stmntDueAsmGracePeriod = { id:2 };

                        Ctrl.changeFrequencyDueAssuming();

                        expect(Ctrl.treaty.stmntDueAsm.id).toBe(1);
                        expect(Ctrl.treaty.stmntDueAsmGracePeriod.id).toBe(2);
                    });
                    it('should set periodOptionPastDue if settlementFrequency is monthly', function(){
                        Ctrl.MonthlyTypeOptions = "test";
                        MockEntity.settlementFrequency.name = "Monthly";

                        Ctrl.changeFrequencyDueAssuming(0);

                        expect(Ctrl.periodOptionPastDue).toBe("test");
                        expect(Ctrl.periodOptionDue).toBe("test");
                    });
                    it('should set periodOptionPastDue if settlementFrequency is quarterly', function(){
                        Ctrl.QuarterTypeOptions = "test";
                        MockEntity.settlementFrequency.name = "Quarterly";

                        Ctrl.changeFrequencyDueAssuming(0);

                        expect(Ctrl.periodOptionPastDue).toBe("test");
                        expect(Ctrl.periodOptionDue).toBe("test");
                    });
                });
            });
            describe('load and check MDM attributes', function(){
              it('should set usStatRisk to Yes if B', function(){
                MockStateParams.hash='mdmAttr';
                Ctrl.treaty={
                  "productStructure" : {
                    "id" : 13,
                    "name" : "Annuity Modified Coinsurance",
                    "structureCode" : "AM"
                  } ,deal:
                { "name" : "AEGON SONY FINRE",
                  "dealStatus" : "Active",
                  "clientDealName" : "Aegon SONY",
                  "terminatedDate" : null,
                  "effectiveDate" : "2014-04-01",
                  "yearEndMonth" : 3,
                  "yearEndDay" : 31,
                  "maturityDate" : null,
                  "comments" : null,
                  "isActive" : 1,
                  "usStatRisk" : "No",
                  "mdmId" : 989333309,
                  "eucUse" : {
                  "id" : 1,
                    "name" : "Financial",
                    "entityType" : "use",
                    "description" : "Financial"
                },
                  "eucComplexity" : {
                  "id" : 12,
                    "name" : "Medium",
                    "entityType" : "complexity",
                    "description" : "Medium"
                },
                  "eucRisk" : {
                  "id" : 5,
                    "name" : "Low",
                    "entityType" : "risk",
                    "description" : "Low"
                },
                  "eucSignificance" : {
                  "id" : 8,
                    "name" : "Low",
                    "entityType" : "significance",
                    "description" : "Low"
                },
                  "productLine" : {
                  "id" : 2600,
                    "name" : "FinRe",
                    "description" : "Financial Reinsurance"
                },
                  "productSubtype" : {
                  "id" : 4,
                    "name" : "FinRe",
                    "description" : "FinRe"
                },
                  "country" : {
                  "id" : 7,
                    "name" : "Japan",
                    "region" : "AP (excl. Australia)",
                    "description" : "AP (excl. Australia)"
                },
                  "gfsAssignee" : {
                  "id" : 2,
                    "firstName" : "Rissa",
                    "lastName" : "Rutherford",
                    "description" : "Rissa Rutherford"
                }
                },mdmAttributes: [{
                  "id" : 387,
                  "enterpriseId" : 402581,
                  "affiliateType" : "3rd Party",
                  "ekComment" : "AUD",
                  "deal" : "Aegon SONY FinRe",
                  "usStatRisk" : "Yes",
                  "effectiveStatusCd" : "A",
                  "businessUnitId" : 13062,
                  "businessUnitName" : "GLJPA-aud-JAPAN",
                  "corporateIndicator" : "No",
                  "legalDoc" : "N/A",
                    "productLineName":"this is a test",
                    "productLine" : {
                      "id" : 2600,
                      "name" : "this is a test",
                      "description" : "Financial Reinsurance"
                    },
                  "productStructure" : {
                    "id" : 13,
                    "name" : "Annuity Modified Coinsurance",
                    "structureCode" : "AM"}

                  }]};
                Ctrl.load();
                expect(Ctrl.EKproductLine).toBe(true);
                expect(Ctrl.mdm ).toBe(true);
              });

            });
            describe('Account Load', function () {
              it('Principal Account', function () {
                var account={login:'123',authorities:['ROLE_APP_FINRE_OPS_ADMIN']};
                deferPrincipal.resolve(account);
                $rootScope.$apply();
                expect(MockPrincipal.identity).toHaveBeenCalled();
                expect(Ctrl.lastUser).toBe('123');
                expect(Ctrl.authority).toBe('Admin');
              });
              it('User Account', function () {
                var account={login:'123',authorities:['ROLE_APP_FINRE']};
                deferPrincipal.resolve(account);
                $rootScope.$apply();
                expect(MockPrincipal.identity).toHaveBeenCalled();
                expect(Ctrl.lastUser).toBe('123');
                expect(Ctrl.authority).toBe('User');
              });
            });

            describe('checkMDM', function () {
            it('checkMDM', function () {
              Ctrl.treaty={
                "productStructure" : {
                  "id" : 13,
                  "name" : "Annuity Modified Coinsurance",
                  "structureCode" : "AM"
                } ,deal:
                  { "name" : "AEGON SONY FINRE",
                    "dealStatus" : "Active",
                    "clientDealName" : "Aegon SONY",
                    "terminatedDate" : null,
                    "effectiveDate" : "2014-04-01",
                    "yearEndMonth" : 3,
                    "yearEndDay" : 31,
                    "maturityDate" : null,
                    "comments" : null,
                    "isActive" : 1,
                    "usStatRisk" : "No",
                    "mdmId" : 989333309,
                    "eucUse" : {
                      "id" : 1,
                      "name" : "Financial",
                      "entityType" : "use",
                      "description" : "Financial"
                    },
                    "eucComplexity" : {
                      "id" : 12,
                      "name" : "Medium",
                      "entityType" : "complexity",
                      "description" : "Medium"
                    },
                    "eucRisk" : {
                      "id" : 5,
                      "name" : "Low",
                      "entityType" : "risk",
                      "description" : "Low"
                    },
                    "eucSignificance" : {
                      "id" : 8,
                      "name" : "Low",
                      "entityType" : "significance",
                      "description" : "Low"
                    },
                    "productLine" : {
                      "id" : 2600,
                      "name" : "FinRe",
                      "description" : "Financial Reinsurance"
                    },
                    "productSubtype" : {
                      "id" : 4,
                      "name" : "FinRe",
                      "description" : "FinRe"
                    },
                    "country" : {
                      "id" : 7,
                      "name" : "Japan",
                      "region" : "AP (excl. Australia)",
                      "description" : "AP (excl. Australia)"
                    },
                    "gfsAssignee" : {
                      "id" : 2,
                      "firstName" : "Rissa",
                      "lastName" : "Rutherford",
                      "description" : "Rissa Rutherford"
                    }
                  },mdmAttributes: [{
                  "id" : 387,
                  "enterpriseId" : 402581,
                  "affiliateType" : "3rd Party",
                  "ekComment" : "AUD",
                  "deal" : "Aegon SONY FinRe",
                  "usStatRisk" : "Yes",
                  "effectiveStatusCd" : "A",
                  "businessUnitId" : 13062,
                  "businessUnitName" : "GLJPA-aud-JAPAN",
                  "corporateIndicator" : "No",
                  "legalDoc" : "N/A",
                  "productLineName":"this is a test",
                  "productLine" : {
                    "id" : 2600,
                    "name" : "this is a test",
                    "description" : "Financial Reinsurance"
                  },
                  "productStructure" : {
                    "id" : 13,
                    "name" : "Annuity Modified Coinsurance",
                    "structureCode" : "AM"}

                }]};
              expect(Ctrl.checkMDM).toBeDefined();
            });
          });

        });
    });
});
