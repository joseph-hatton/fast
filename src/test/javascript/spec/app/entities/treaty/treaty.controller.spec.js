'use strict';

describe('Controller Tests', function() {

  describe('Treaty Controller', function() {
    var Ctrl;
    var scope,state,paginationConstants,pagingParams;

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state = jasmine.createSpyObj('state', ['go', 'transitionTo']);
        pagingParams={ascending:true,page:1,predicate:"id",search:null,sort:"id,asc"};
        paginationConstants= {itemsPerPage: 10};
        Ctrl = $controller('TreatyController', {
          $scope: scope,
          $state: state,
          paginationConstants:paginationConstants,
          pagingParams:pagingParams
        });
      }));

    describe('Initial state', function () {
      it('Treaty Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('Treaty Load All Test', function() {
        expect(Ctrl.loadAll).toBeDefined();
      });
    });
    describe("Main Content", function(){
      describe('DeleteTreaty', function(){
        it('should not delete if the deal is terminated', function(){
          Ctrl.deal = {dealStatus: "Terminated"};

          Ctrl.deleteTreaty();

          expect(state.go).not.toHaveBeenCalled();
        });
        it('should delete if the deal is not terminated', function(){
          Ctrl.deal = {dealStatus: "Active"};
          
          Ctrl.deleteTreaty();

          expect(state.go).toHaveBeenCalled();
        });
      });
      describe('ChangeReplicateFlag', function(){
        it('should set replicateFlag to false if the dealStatus is Terminated', function () {
          Ctrl.replicateFlag = true;
          Ctrl.deal = { dealStatus:"Terminated" };

          Ctrl.changeReplicateFlag();

          expect(Ctrl.replicateFlag).toBe(false);
        });
        it('should not set the replicate flag to false if the dealStatus is not Terminated', function () {
          Ctrl.replicateFlag = true;
          Ctrl.deal = { dealStatus:"Active" };

          Ctrl.changeReplicateFlag();

          expect(Ctrl.replicateFlag).toBe(true);
        });
      });
    });
  });
});
