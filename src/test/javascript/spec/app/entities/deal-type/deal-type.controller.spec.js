'use strict';

describe('Controller Tests', function() {

  describe('DealType Controller', function() {
    var Ctrl;
    var scope;
    var MockDealType;
    var info = [{},{},{}];
// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        MockDealType =
          {
            query: function(callBack){
              callBack(info);
            }
          };
        Ctrl = $controller('DealTypeController', {
          $scope: scope, DealType:MockDealType
        });
      }));

    describe('Initial state', function () {
      it('DealType Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('Load All Test / call back ', function() {
        expect(Ctrl.loadAll).toBeDefined();
        Ctrl.loadAll();
        expect(Ctrl.dealTypes.length).toBe(3);
      });


    });
  });
});
