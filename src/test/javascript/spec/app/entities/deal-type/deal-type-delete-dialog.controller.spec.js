'use strict';

describe('Controller Tests', function() {
  describe('Delete Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockDealType, MockUiModalInstance;
    var MockEntity, MockPreviousState;
    var info =1;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = {};
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        MockDealType = {
          delete: function (idObj, callback) {
            callback(info);
          }
        };

        Ctrl = $controller('DealTypeDeleteController', {
          $scope: scope,
          $state: state, entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          DealType: MockDealType
        });
      }));

    describe('Initial state', function () {
      it('DealTypeDeleteController  Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('clear', function() {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });

      it('confirmDelete', function() {
        expect(Ctrl.confirmDelete).toBeDefined();
        Ctrl.confirmDelete(123);
      });
    });

  });
});
