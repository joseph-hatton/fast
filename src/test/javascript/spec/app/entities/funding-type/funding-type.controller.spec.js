'use strict';

describe('Controller Tests', function() {

  describe('FundingType Controller', function() {

    var Ctrl;
    var scope;
    var MockFundingType;
    var MockFundingTypeSearch;
    var result = [{},{}];
    var queryResult = [{},{},{},{}];
    var MockState;

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();

        MockState = jasmine.createSpy('MockState');

        MockFundingType = {
          query: function (callback) {
            callback(result);
          }
        };

        MockFundingTypeSearch = {
          query: function (searchQuery, callback) {
            callback(queryResult);
          }
        };

        Ctrl = $controller('FundingTypeController', {
          $scope: scope,
          $state: MockState,
          FundingType: MockFundingType,
          FundingTypeSearch: MockFundingTypeSearch
        });
      }));

    describe('Initial state', function () {
      it('FundingType Controller', function() {
        expect(Ctrl).toBeDefined();
      });
    });
    describe('Main Content Coverage', function () {
      it('Load All Test', function () {
        expect(Ctrl.loadAll).toBeDefined();
        expect(Ctrl.fundingTypes).toBe(result);
      });

      it('Search Test No Search Query', function () {
        expect(Ctrl.search).toBeDefined();
        Ctrl.searchQuery = null;
        Ctrl.search();
        expect(Ctrl.fundingTypes).toBe(result);
      });

      it('Search Test Search Query', function () {
        Ctrl.fundingTypes=[];
        expect(Ctrl.search).toBeDefined();
        Ctrl.searchQuery = 12;
        Ctrl.search();
        expect(Ctrl.fundingTypes.length).toBe(4);
      });
    });
  });
});
