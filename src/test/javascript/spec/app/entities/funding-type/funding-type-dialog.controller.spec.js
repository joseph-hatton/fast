'use strict';

describe('Controller Tests', function() {
  describe('FundingType Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockFundingType,MockUiModalInstance;
    var MockEntity, MockPreviousState, MockStateParams, MockTimeout, flag = true;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        //state =$rootScope.$state;
        MockEntity = jasmine.createSpy('MockEntity');
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        if (flag)
          MockStateParams = {  };
        else
          MockStateParams = { fundingTypeId: "" };
        MockFundingType = {
          update: function (fundingType, success, error) {
            success(fundingType);
          },
          save: function (fundingType, success, error) { error(); },
          get: function (idObj, callback) { callback(idObj); }
        };
        state = { go: function (str, obj) { } };
        MockTimeout = function(callback, amount){
          callback();
        };

        Ctrl = $controller('FundingTypeDialogController', {
          $scope: scope,
          $state: state,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          $stateParams: MockStateParams,
          FundingType: MockFundingType,
          $timeout: MockTimeout
        });
      }));

    describe('Initial state', function () {
      it('FundingTypeDialog Controller', function () {
        expect(Ctrl).toBeDefined();
      });
    });
  });
});
