'use strict';

describe('Controller Tests', function() {
  describe('FundingType Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockFundingType,MockUiModalInstance;
    var MockEntity, MockPreviousState, MockStateParams, MockTimeout, flag = true;
    var info=1;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        //state =$rootScope.$state;
        MockEntity = jasmine.createSpy('MockEntity');
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        if (flag)
          MockStateParams = {  };
        else
          MockStateParams = { fundingTypeId: "" };
        MockFundingType = {
          delete: function(id,callBack){
            callBack(info);
          },
          update: function (fundingType, success, error) {
            success(fundingType);
          },
          save: function (fundingType, success, error) { error(); }
        };
        state = { go: function (str, obj) { } };
        MockTimeout = function(callback, amount){
          callback();
        };

        Ctrl = $controller('FundingTypeDeleteController', {
          $scope: scope,
          $state: state,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          $stateParams: MockStateParams,
          FundingType: MockFundingType,
          $timeout: MockTimeout
        });
      }));

    describe('Initial state', function () {
      it('FundingTypeDialog Delete Controller', function () {
        expect(Ctrl).toBeDefined();
      });

      it('clear', function() {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });

      it('confirmDelete', function() {
        Ctrl.confirmDelete(123);
        expect(Ctrl.confirmDelete).toBeDefined();
      });
    });
  });
});
