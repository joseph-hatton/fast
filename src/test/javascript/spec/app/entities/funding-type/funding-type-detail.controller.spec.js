'use strict';

describe('Controller Tests', function() {

  describe('FundingType  Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockFundingType;
    var info =1;
    var Ctrl;

    beforeEach(inject(function($controller,$injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      MockEntity = jasmine.createSpy('MockEntity');

      MockFundingType  =
        {
          get: function(id,callBack){
            callBack(info);
          }
        };

      Ctrl = $controller('FundingTypeDetailController', {
        $scope: $scope,
        $rootScope: $rootScope,
        entity: MockEntity ,
        FundingType: MockFundingType
      });

    }));
    describe('Initial State', function () {
      it('AgreementType Detail Controller Should be Created', function () {
        expect(Ctrl).toBeDefined();
      });

      it('Load Should be Defined', function () {
        expect(Ctrl.load).toBeDefined();
      });
    });
  });
});
