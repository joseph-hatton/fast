'use strict';

describe('Controller Tests', function() {
  describe('Contact Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockContact,MockUiModalInstance;
    var MockEntity, MockPreviousState, MockStateParams, MockTimeout, flag = true;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        //state =$rootScope.$state;
        MockEntity = jasmine.createSpy('MockEntity');
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        if (flag)
          MockStateParams = {  };
        else
          MockStateParams = { contactId: "" };
        MockContact = {
          update: function (contact, success, error) {
            success(contact);
          },
          save: function (contact, success, error) { },
          get: function (idObj, callback) { }
        };
        state = { go: function (str, obj) { } };
        MockTimeout = function(callback, amount){
          callback();
        };

        Ctrl = $controller('ContactDialogController', {
          $scope: scope,
          $state: state,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          $stateParams: MockStateParams,
          Contact: MockContact,
          $timeout: MockTimeout
        });
      }));

    describe('Initial state', function () {
      it('ContactDialog Controller', function () {
        expect(Ctrl).toBeDefined();
      });
    });
    describe('Main Coverage', function () {
      describe('Selected Country', function () {
        describe('United States', function () {
          it('should run', function () {
            Ctrl.contact = { country: { name: "United States" } };
            Ctrl.selectedCountry();
            expect(Ctrl.contact.international).toBe(1);
          })
        });
        describe('Not United States', function () {
          it('should run', function () {
            Ctrl.contact = { country: { name: "United Kingdom" } };
            Ctrl.selectedCountry();
            expect(Ctrl.contact.international).toBe(0);
          });
        });
      });

      describe('Clear', function () {
        it('should run', function () {
          Ctrl.clear();
          expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
        });
      });

      describe('OpenCalendar', function () {
        it('should run', function () {
          var rec = "updatedDate";
          Ctrl.datePickerOpenStatus[rec] = false;
          Ctrl.openCalendar(rec);
          expect(Ctrl.datePickerOpenStatus[rec]).toBe(true);
        });
      });

      describe('Save Test', function () {
        describe('Country United States', function () {
          it('should be run', function () {
            Ctrl.contact = { country: { name: "United States" }, province: true };
            Ctrl.save();
            expect(Ctrl.contact.province).toBeNull();
          });
        });

        describe('Country not United States', function () {
          it('should be run', function () {
            Ctrl.contact = { country: { name: "United Kingdom" }, state: true };
            Ctrl.save();
            expect(Ctrl.contact.state).toBeNull();
          });
        });

        describe('Contact Salutation', function () {
          it('is not null', function () {
            Ctrl.contact = { country: { name: "United States" }, province: true, salutation: { name: "Mr." } };
            Ctrl.save();
            expect(Ctrl.contact.salutation).toBe("Mr.");
          });
          it('is null', function () {
            Ctrl.contact = { country: { name: "United States" }, province: true, salutation: null };
            Ctrl.save();
            expect(Ctrl.contact.salutation).toBeNull();
          });
        });

        describe('Contact Id', function () {
          it('is Null', function () {
            Ctrl.contact = { country: { name: "United States" }, province: true, salutation: null, id: null };
            Ctrl.lastUser = "lastUser";
            Ctrl.save();
            expect(Ctrl.contact.lastUser).toBe("lastUser");
          });
        });
      });

      describe('OnSaveSuccess', function () {
        describe('id', function () {
          it('is set', function () {
            Ctrl.contact = { country: { name: "United States" }, province: true, salutation: null, id: 123 };
            Ctrl.save();
            expect(Ctrl.isSaving).toBe(false);
            expect(MockUiModalInstance.close).toHaveBeenCalled();
            flag = false;
          });

          it('is not set', function () {
            Ctrl.contact = { country: { name: "United States" }, province: true, salutation: null, id: 123 };
            Ctrl.save();
            expect(Ctrl.isSaving).toBe(true);
            expect(MockUiModalInstance.close).toHaveBeenCalled();
          });
        });
      });
    });
  });
});
