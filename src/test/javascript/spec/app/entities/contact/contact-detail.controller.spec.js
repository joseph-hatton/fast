'use strict';

describe('Controller Tests', function() {

    describe('Contact Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockContact, MockCompany;
        var Ctrl;

        beforeEach(inject(function($controller, $injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockContact = jasmine.createSpyObj('MockContact', ['get']);
            MockCompany = jasmine.createSpy('MockCompany');

            Ctrl = $controller("ContactDetailController", {
              $scope: $scope,
              $rootScope: $rootScope,
              entity: MockEntity,
              Contact: MockContact,
              Company: MockCompany
            });
        }));

        describe('Initial Setup', function() {
          it('Contact Detail Controller Defined', function() {
            expect(Ctrl).toBeDefined();
          });

          it('Load Defined', function() {
            expect(Ctrl.load).toBeDefined();
          });
        });

        describe('Main Code Coverage', function () {
          describe('Load', function () {
            it('runs', function () {
              Ctrl.load(1);
              expect(MockContact.get).toHaveBeenCalled();
            });
          });
        });
    });
});
