'use strict';

describe('Controller Tests', function() {
  describe('Contact Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockContact,MockUiModalInstance,MockXrefTreatyCustomContact,MockXrefTreatyContact;
    var MockEntity, MockPreviousState, MockStateParams, MockTimeout, flag = true;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        //state =$rootScope.$state;
        MockEntity = jasmine.createSpy('MockEntity');
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        if (flag)
          MockStateParams = {  };
        else
          MockStateParams = { contactId: "" };
        MockContact = {
          delete: function (idObj, callback) { }
        };
        state = { go: function (str, obj) { } };
        MockTimeout = function(callback, amount){
          callback();
        };
        MockXrefTreatyCustomContact={contacts:jasmine.createSpyObj('contacts', ['query'])};


        MockXrefTreatyContact = {
          delete: function (idObj, callback) { }
        };
        Ctrl = $controller('ContactDeleteController', {
          $scope: scope,
          $state: state,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          $stateParams: MockStateParams,
          Contact: MockContact,
          $timeout: MockTimeout,
          XrefTreatyCustomContact:MockXrefTreatyCustomContact,
          XrefTreatyContact:MockXrefTreatyContact
        });
      }));

    describe('Initial state', function () {
      it('ContactDialog Controller', function () {
        expect(Ctrl).toBeDefined();
      });

      it('clear ', function () {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });
      it('confirmDelete  ', function () {
        expect(Ctrl.confirmDelete).toBeDefined();
      });
    });
  });
});
