'use strict';

describe('Controller Tests', function() {

  describe('Contact Controller', function() {
    var Ctrl;
    var scope,state,paginationConstants,pagingParams;
    var MockContact, mockContactCustomSrv;
    var resultSearch= [{},{},{}];
    var resultCOntact=[{},{},{},{},{}];
    var info = [{},{},{}];
    var infoSearch = [{}];
    var infoSearchMain=[{},{},{},{},{}];
// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope,ContactCustomSrv) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        pagingParams={ascending:true,page:1,predicate:"id",search:null,sort:"id,asc"};
        paginationConstants= {itemsPerPage: 10};
        mockContactCustomSrv =ContactCustomSrv;
        function myMockHeadersFunction(key) {
          if (key === 'link')
            return '</api/contact/contact?page=0&size=10>; rel="last",</api/contact/non-mdm?page=0&size=10>; rel="first"';
          return 1;
        }

        MockContact={query: function(searchString,callBack){
          callBack(resultCOntact,myMockHeadersFunction);
        }};

        mockContactCustomSrv.fullSearch={
          query: function(searchString,callBack){
            callBack(resultSearch,myMockHeadersFunction);
          }
        };

        Ctrl = $controller('ContactController', {
          $scope: scope,
          $state: state,
          Contact:MockContact,
          ContactCustomSrv:mockContactCustomSrv,
          paginationConstants:paginationConstants,
          pagingParams:pagingParams
        });
      }));


    describe('Initial state', function () {
      it('Contact Controller', function() {
        expect(Ctrl).toBeDefined();
      });
      it('Contact Load All Test', function() {
        expect(Ctrl.loadAll).toBeDefined();
        Ctrl.loadAll();
        expect(Ctrl.contacts).toBeDefined();
        expect(Ctrl.contacts.length).toBe(5);
      });

      it('Contact Load All Test with Search', function() {
        pagingParams.search='abc';
        expect(Ctrl.loadAll).toBeDefined();
        Ctrl.loadAll();
        expect(Ctrl.contacts).toBeDefined();
        expect(Ctrl.contacts.length).toBe(3);
      });



      it('Contact  Search ', function() {
        Ctrl.searchQuery='abc';
        expect(Ctrl.search).toBeDefined();
        Ctrl.transition= function (){return 'OK'};
        Ctrl.search(Ctrl.searchQuery);
        expect(Ctrl.contacts).toBeDefined();
        expect(Ctrl.contacts.length).toBe(5);

      });

      it('Contact  Search Null load ALL ', function() {
        Ctrl.searchQuery=null;
        expect(Ctrl.searchQuery).toBeNull();
        Ctrl.transition= function (){return 'OK'};
        Ctrl.search(Ctrl.searchQuery);
        expect(Ctrl.contacts).toBeDefined();
        expect(Ctrl.contacts.length).toBe(5);

      });

      it('Contact  Clear search ', function() {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.transition= function (){return 'OK'};
        Ctrl.clear();
        expect(Ctrl.contacts).toBeDefined();
        expect(Ctrl.contacts.length).toBe(5);
      });

  })
  });
});
