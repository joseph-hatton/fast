'use strict';

describe('Controller Tests', function() {

    describe('RiskFee Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockRiskFee;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockRiskFee = jasmine.createSpy('MockRiskFee');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'RiskFee': MockRiskFee
            };
            createController = function() {
                $injector.get('$controller')("RiskFeeDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'finreApp:riskFeeUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
