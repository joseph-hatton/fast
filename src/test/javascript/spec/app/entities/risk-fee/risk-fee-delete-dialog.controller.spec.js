'use strict';

describe('Controller Tests', function() {
  describe('RiskFeeDeleteController', function() {
    var Ctrl;
    var scope,state,MockRiskFee,MockUiModalInstance;
    var MockEntity, MockPreviousState;
    var success='Success';
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = jasmine.createSpyObj('MockEntity', ['get', 'update', 'save']);
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);

        MockRiskFee  =
          {
            delete: function (id, callback) {
              return "Success deletion";
            }
          };

        Ctrl = $controller('RiskFeeDeleteController', {
          $scope: scope,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          RiskFee: MockRiskFee
        });
      }));

    describe('Initial state', function () {
      it('RiskFee Delete Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('confirmDelete ', function() {
        expect(Ctrl.confirmDelete).toBeDefined();
        Ctrl.confirmDelete(123);
        //expect(MockUiModalInstance.close).toHaveBeenCalled();
      });


      it('Clear', function() {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });
    });
  });
});
