'use strict';

describe('Controller Tests', function () {

  describe('TreatyTransaction Cash Controller', function () {
    var $scope, $rootScope,MockCurrency;
    var MockEntity, MockTreatyTransactionReliefSrv, MockCountry,MockPrincipal,DeferFactory,MockFundingType;
    var MockCurrencyCustomSrv,tmSrvDefer2,MockDeal,MockTreatyTransactionReliefFilter;
    var MockTreatyTransactionRelief,MockCompanyCustomSrv,MockXrefTreatyCustomCompany,MockAllocationType;
    var Ctrl,deferExchangeRate, resultExchangeRate,account3;
    var account2,identity ,defer2, account,tmSrvDefer,defer, MockTreatyTransactionSrv,MockTransactionStatus, MockTreatyTracking, MockIntro,deferStatus;

    beforeEach(inject(function ($injector, $controller, _DeferFactory_ ,$stateParams,DateUtils,$q) {



      DeferFactory = _DeferFactory_;
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      defer2= DeferFactory.generate();
      tmSrvDefer = DeferFactory.generate();
      tmSrvDefer2 = DeferFactory.generate();
      defer= DeferFactory.generate();
      deferExchangeRate= DeferFactory.generate();
      deferStatus= DeferFactory.generate();
      MockEntity = jasmine.createSpy('MockEntity');
      MockPrincipal = jasmine.createSpyObj('MockPrincipal', ['identity']);
      MockPrincipal.identity.and.returnValue(tmSrvDefer.promise);
      account={login:'123',authorities:['ROLE_APP_FINRE_OPS_ADMIN']};
      account2={login:'1823',authorities:["ROLE_APP_FINRE_VIEW_ONLY"]};
      account3={login:'18',authorities:["ROLE_APP_FINRE"]};
      MockTreatyTransactionReliefSrv = {
        base: jasmine.createSpy('base', ['get'])};
      MockTreatyTransactionReliefSrv.base.get = function(){return 1};
      MockCurrency = jasmine.createSpyObj('MockCurrency', ['query']);
      MockDeal = jasmine.createSpyObj('Deal', ['get']);
      MockCompanyCustomSrv=jasmine.createSpyObj('MockCompanyCustomSrv', ['companies']);
      MockCompanyCustomSrv.companies={query:function(){}};
      MockCountry = jasmine.createSpyObj('Country', ['get']);
      MockTreatyTracking = jasmine.createSpyObj('MockTreatyTracking', ['save', 'update']);
      MockCurrencyCustomSrv=jasmine.createSpyObj('CurrencyCustomSrv', ['rate']);
      MockCurrencyCustomSrv.rate={get:function(){}};
      MockTreatyTransactionReliefFilter = {
        reportingReport:jasmine.createSpyObj('reportingReport', ['get'])
      };
      MockTreatyTransactionRelief = jasmine.createSpyObj('TransactionRelief', ['save', 'update']);
      MockIntro =jasmine.createSpyObj('Intro', ['setupIntroSteps']);
      MockIntro.setupIntroSteps.and.returnValue(DeferFactory.generate().promise);
      resultExchangeRate={
        "id" : 849830969,
        "code" : "AUD",
        "name" : "Australian Dollar",
        "description" : "Australian Dollar",
        "year" : 2017,
        "exchangeRate" : 0.718964,
        "effectiveDate" : "2017-12-01"
      };
      MockCurrencyCustomSrv = { rate:jasmine.createSpyObj('rate', ['get']) };
      MockCurrencyCustomSrv.rate.get.and.returnValue(deferExchangeRate);


      Ctrl = $controller("TreatyTransactionReliefController", {
        '$scope': $scope,
        '$rootScope': $rootScope,
        '$stateParams':$stateParams,
        'entity': MockEntity,
        'Principal': MockPrincipal,
        'TreatyTransactionReliefSrv': MockTreatyTransactionReliefSrv,
        'TreatyTransactionRelief': MockTreatyTransactionRelief,
        'Currency':MockCurrency,
        'Deal': MockDeal,
        'CompanyCustomSrv':MockCompanyCustomSrv,
        'Country': MockCountry,
        'CurrencyCustomSrv':MockCurrencyCustomSrv,
        'TreatyTransactionReliefFilter': MockTreatyTransactionReliefFilter,
        'Intro':MockIntro,
        '$q': $q
      });


    }));


    describe('Init State', function () {
      it('Ctrl should be defined', function () {
        tmSrvDefer.resolve(account);
        Ctrl.treatyId=123;
        $rootScope.$apply();
        expect(Ctrl).toBeDefined();
      });
    });

    describe('Main Content', function () {

      describe('Account Load', function () {
        it('Principal Account', function () {
          tmSrvDefer.resolve(account);
          Ctrl.treatyId=123;
          $rootScope.$apply();
          expect(MockPrincipal.identity).toHaveBeenCalled();
          expect(Ctrl.lastUser).toBeDefined();
          expect(Ctrl.lastUser).toBe('123');
        });

        it('Principal Account view only', function () {
          tmSrvDefer.resolve(account2);
          Ctrl.treatyId=123;
          $rootScope.$apply();
          expect(Ctrl.authority).toBe('ViewOnly');
        });

        it('Principal Account User', function () {
          tmSrvDefer.resolve(account3);
          Ctrl.treatyId=123;
          $rootScope.$apply();
          expect(Ctrl.authority).toBe('User');
        });

      });
      describe('Exchange Rate', function () {
        it('Calculation', function () {
          var date = new Date("2017-03-01");
          var mainTransaction= {fundingType:{name:'Wire-Out'},convertedNetTotal:123,netTotal:123,
            subTransactions:[{id:123,amount:123,convertedAmount:123}] ,currency:{id:123,code:'AUD'}, reportingDate :date };
          deferExchangeRate.resolve(resultExchangeRate);
          expect(Ctrl.exchangeRate).toBeDefined();
          Ctrl.exchangeRate(mainTransaction);
          $rootScope.$apply();
          expect(MockCurrencyCustomSrv.rate.get).toHaveBeenCalled();
          expect(Ctrl.currentExchangeRate).toBeDefined();
          expect(Ctrl.currentExchangeRate).toBe(0.718964);
          expect(mainTransaction.subTransactions[0].convertedAmount).toBeLessThan(123);
        });
      });
      describe('SubTransactionCurrency', function () {
        it('Calculation by subtransaction change', function () {
          var date = new Date("2017-03-01");
          var mainTransaction= {fundingType:{name:'Wire-Out'},convertedNetTotal:123,netTotal:123,subTransaction:{id:123,amount:123} ,currency:{id:123,code:'AUD'}, reportingDate :date };
          var transaction= {amount:123};
          deferExchangeRate.resolve(resultExchangeRate);
          Ctrl.SubTransactionCurrency(transaction,mainTransaction);
          $rootScope.$apply();
          expect(Ctrl.SubTransactionCurrency).toBeDefined();
          expect(MockCurrencyCustomSrv.rate.get).toHaveBeenCalled();
          expect(Ctrl.currentExchangeRate).toBe(0.718964);
        });
      });

      describe('Structured Check', function () {
        it('Structured- product sub type ', function () {
          var date = new Date("2017-03-01");
          var mainTransaction= {fundingType:{name:'Wire-Out'},convertedNetTotal:123,netTotal:123,
            subTransactions:[{id:123,amount:123,convertedAmount:123}] ,currency:{id:123,code:'AUD'}, reportingDate :date };
          var transaction={id:123,amount:123,convertedAmount:123,company:{name:null}};
          Ctrl.deal={productSubtype:{name:'Structured'}};
          Ctrl.companyRga={name:'RGA COMPANY'};
          expect(Ctrl.structured).toBeDefined();
          Ctrl.structured(transaction,mainTransaction);
          $rootScope.$apply();
          expect(transaction.company.name).toBe('RGA COMPANY');
        });
      });
      describe('setFormInvalid', function () {
        it('setFormInvalid', function () {
          Ctrl.isSavingCompany=null;
          expect(Ctrl.setFormInvalid).toBeDefined();
          Ctrl.setFormInvalid(true);
          expect( Ctrl.isSavingCompany).toBe(true);
        });
      });

      describe('USD ', function () {
        it('USD ', function () {
          var transaction={id:123,amount:111,currency:{exchangeRate:2},convertedAmount:123,company:{name:null}};
          expect(Ctrl.USD).toBeDefined();
          Ctrl.USD(transaction);
          expect(transaction.convertedAmount).toBe(222);
        });
      });

      describe('removeTransaction', function () {
        it('removeTransaction ', function () {
          Ctrl.mainTransactionList=[ {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} },
            {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
              netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} }
          ];
          expect(Ctrl.removeTransaction).toBeDefined();
          expect(Ctrl.mainTransactionList.length).toBe(2);
          Ctrl.removeTransaction(0);
        });
      });
      describe('addTransaction ', function () {
        it('addTransaction', function () {
          Ctrl.transactionList=[ {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} },
            {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
              netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} }
          ];
          Ctrl.deal={productSubtype:{name:'Structured'}};
          expect(Ctrl.addTransaction).toBeDefined();
          expect(Ctrl.transactionList.length).toBe(2);
          Ctrl.addTransaction ();
          expect(Ctrl.transactionList.length).toBe(3);
        });
      });

      describe('addSubTransaction ', function () {
        it('addSubTransaction ', function () {
          var mainTransaction= {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123}] ,currency:{id:123,code:'AUD'} };
          Ctrl.treatyTransaction={treaty:{deal:{id:123}}};
          expect(Ctrl.addSubTransaction).toBeDefined();
          expect(mainTransaction.subTransactions.length).toBe(1);
          Ctrl.addSubTransaction(mainTransaction,2);
          expect(mainTransaction.subTransactions.length).toBe(2);
        });
      });
      describe('remove sub ', function () {
        it('removeSubTransaction ', function () {
          var mainTransaction= {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} };
          Ctrl.treatyTransaction={treaty:{deal:{id:123}}};
          expect(Ctrl.removeSubTransaction).toBeDefined();
          expect(mainTransaction.subTransactions.length).toBe(2);
          Ctrl.removeSubTransaction(mainTransaction,1);
          expect(mainTransaction.subTransactions.length).toBe(1);
        });
      });

      describe('date picker', function () {
        it('DateOpen', function () {
          var trans= {settlementDateOpened:false ,accountingDateOpened:false,reportingDateOpened:false};
          expect(Ctrl.openReportingDate).toBeDefined();
          Ctrl.openReportingDate(trans);
          expect(trans.reportingDateOpened).toBe(true);
        });
      });
      describe('copyTransaction ', function () {
        it('copyTransaction', function () {
          Ctrl.transactionList=[ {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} },
            {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
              netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} }
          ];
          Ctrl.treatyTransaction={treaty:{treatyStatus:{name:'Active'},deal:{id:123}}};
          expect(Ctrl.copyTransaction).toBeDefined();
          expect(Ctrl.transactionList.length).toBe(2);
          Ctrl.copyTransaction(0);
          expect(Ctrl.transactionList.length).toBe(3);
        });
      });

      describe('sortBy', function () {
        it('changeStatusFunding Expected ', function () {

          expect(Ctrl.sortBy).toBeDefined();
          Ctrl.sortBy('Period');
          expect(Ctrl.propertyName).toBe('Period');
          expect(Ctrl.sortBy).toBeDefined();
          Ctrl.sortBy(null);
          expect(Ctrl.propertyName).toBe(null)
        });
      });

      describe('reset', function () {
        it('reset Form ', function () {
        var form= { $dirty:true, $setPristine : function () {$dirty:true}};
       var $setPristine = function () {};
          Ctrl.searchEndDate="2017-01-01";
         expect(Ctrl.reset).toBeDefined();
          Ctrl.reset(form);
          expect(form.$dirty).toBe(false);
        expect(Ctrl.searchEndDate).toBe(null);

        });
      });

      describe('collapse', function () {
        it('collapse Transaction ', function () {
          Ctrl.transactionList=[ {isCollapsed:true,fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} },
            {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
              netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} }
          ];
          expect(Ctrl.collapse).toBeDefined();
          Ctrl.collapse(1);
          expect(Ctrl.transactionList[0].isCollapsed).toBe(false);
          Ctrl.collapse(2);
          expect(Ctrl.transactionList[0].isCollapsed).toBe(true);

        });
      });

      describe('move', function () {
        it('move', function () {
        var list= [{id:1},{id:2},{id:3}];
          expect(Ctrl.move).toBeDefined();
          expect(list[1].id).toBe(2);
          Ctrl.move(list,1,'Down');
          expect(list[1].id).toBe(3);

        });
      });


    });
  });

});
