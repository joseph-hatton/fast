'use strict';

describe('Controller Tests', function () {

  describe('TreatyTransaction Cash Finance Dialog Controller', function () {
    var $scope, $rootScope;
    var MockEntity, MockTreatyTransaction, MockTreaty, MockUibModalInstance, MockContactCustomSrv, MockEmail;
    var Ctrl, contactQueryResult;

    beforeEach(inject(function ($injector, $controller) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      MockEntity = jasmine.createSpy('MockEntity');
      MockTreatyTransaction = jasmine.createSpyObj('MockTreatyTransaction', ['update']);
      MockTreaty = jasmine.createSpy('MockTreaty');
      MockUibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['dismiss', 'clear']);
      MockContactCustomSrv = { name: { query: function (obj, success, error) { success(contactQueryResult); }} };
      MockEmail = { send:jasmine.createSpyObj('send', ['email']) };

      Ctrl = $controller("TreatyTransactionCashFinanceDialogController", {
        $scope: $scope,
        $rootScope: $rootScope,
        entity: MockEntity,
        TreatyTransaction: MockTreatyTransaction,
        $uibModalInstance: MockUibModalInstance,
        ContactCustomSrv: MockContactCustomSrv,
        Email: MockEmail
      });
    }));


    describe('Init State', function () {
      it('Ctrl should be defined', function () {
        expect(Ctrl).toBeDefined();
      });
    });
    describe('Main Content', function () {
      describe('Clear', function () {
        it('should close the modal', function () {
          Ctrl.clear();
          expect(MockUibModalInstance.dismiss).toHaveBeenCalled();
        });
      });
      describe('NewSearch', function () {
        it('should set list of contacts to matching results if length is greater than 1', function () {
          contactQueryResult = [{},{}];
          Ctrl.newsearch('test');
          expect(Ctrl.listOfContacts.length).toBe(2);
        });
        it('should not search if length is less than or equal to 1', function () {
          contactQueryResult = [{},{}];
          Ctrl.newsearch('t');
          expect(Ctrl.listOfContacts.length).toBe(0);
        });
      });
      describe('addContact', function () {
        it('should add the contact when it isn\'t already added', function () {
          Ctrl.selectedContactsInfo = [{ id:321 }];
          var contact = { id:123 };
          Ctrl.searchQuery = 'content';
          Ctrl.addContact(contact);
          expect(Ctrl.selectedContactsInfo.length).toBe(2);
          expect(Ctrl.searchQuery).toBe('');
        });
        it('should not add the contact when it is already added', function () {
          Ctrl.selectedContactsInfo = [{ id:123 }];
          var contact = { id:123 };
          Ctrl.searchQuery = 'content';
          Ctrl.addContact(contact);
          expect(Ctrl.selectedContactsInfo.length).toBe(1);
          expect(Ctrl.searchQuery).toBe('');
        });
      })
      describe('removeContact', function () {
        it('should remove the contact', function () {
          Ctrl.selectedContactsInfo = [{},{}];
          Ctrl.removeContact(0);
          expect(Ctrl.selectedContactsInfo.length).toBe(1);
        });
      });
      describe('sendToFinance', function () {
        it('should setup the selectedContacts string', function () {
          Ctrl.selectedContactsInfo = [
            { email:'email1' },
            { email:'email2' },
            { email:'email3' }
          ];
          Ctrl.sendToFinance();
          expect(Ctrl.selectedContacts).toBe('email1,email2,email3');
          expect(MockEmail.send.email).toHaveBeenCalled();
        });
        describe('onSendSuccess', function () {
          it('should set financeNotification to 1', function () {
            MockEmail.send.email = function (obj, success) {
              success();
            };
            Ctrl.sendToFinance();
            expect(Ctrl.treatyTransaction.financeNotification).toBe(1);
          });
        });
        describe('onSendError', function () {
          it('should set financeNotification to 0', function () {
            MockEmail.send.email = function (obj, success, error) {
              error();
            };
            Ctrl.sendToFinance();
            expect(Ctrl.treatyTransaction.financeNotification).toBe(0);
          });
        });
      });
    });
  });

});
