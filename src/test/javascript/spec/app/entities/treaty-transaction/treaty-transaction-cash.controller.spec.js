'use strict';

describe('Controller Tests', function () {

  describe('TreatyTransaction Cash Controller', function () {
    var $scope, $rootScope,loadAllDefer;
    var MockEntity, MockTreatyTransaction, MockTreaty,MockPrincipal,DeferFactory,MockFundingType;
    var MockCurrencyCustomSrv;
    var MockTreatyTrackingCustomSvr,MockCompanyCustomSrv,MockXrefTreatyCustomCompany,MockAllocationType;
    var account2,Ctrl,account3,allocationResult,deferCustomCompany,customCompanyResult;
    var identity ,defer2, account,tmSrvDefer,defer, MockTreatyTransactionSrv,MockTransactionStatus, MockTreatyTracking, MockIntro,deferStatus;
    var resultTreaty,deferTreaty,deferAllocation, resultExchangeRate,deferExchangeRate;

    beforeEach(inject(function ($injector, $controller, _DeferFactory_ ,$stateParams,DateUtils,$q) {
      DeferFactory = _DeferFactory_;
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      MockEntity = jasmine.createSpy('MockEntity');
      MockTreatyTransaction = jasmine.createSpy('MockTreatyTransaction');
      deferAllocation = DeferFactory.generate();
      defer= DeferFactory.generate();
      deferExchangeRate= DeferFactory.generate();
      deferCustomCompany= DeferFactory.generate();
      defer2= DeferFactory.generate();
      MockFundingType = jasmine.createSpyObj('MockFundingType', ['query']);
      account2={login:'1823',authorities:["ROLE_APP_FINRE_VIEW_ONLY"]};
      account3={login:'18',authorities:["ROLE_APP_FINRE"]};
      MockFundingType.query.and.callFake(
        function(result){
          return defer});
      MockTreaty = jasmine.createSpyObj('MockTreaty', ['get']);
      MockTreaty.get.and.callFake(
        function(result){
          return deferTreaty});
      MockAllocationType= jasmine.createSpyObj('MockAllocationType', ['query']);
      MockAllocationType.query.and.callFake(
        function(result){
          return deferAllocation});

      resultExchangeRate={
        "id" : 849830969,
        "code" : "AUD",
        "name" : "Australian Dollar",
        "description" : "Australian Dollar",
        "year" : 2017,
        "exchangeRate" : 0.718964,
        "effectiveDate" : "2017-12-01"
      };
      resultTreaty = {
        id: 111,
          treatyStatus: { name:'Active' },
        mdmAttributes: [{ id:123 ,ekComment:'test' },{id:13 ,ekComment:'null' }],
        treatyCurrencies: {currency:{id:123, code:'USD'}},
        deal:{id:123,productLine:{name:'Finre'}}
      };
      MockTreatyTrackingCustomSvr = {
        count:jasmine.createSpyObj('count', ['get'])
      };
      MockTreatyTrackingCustomSvr.count.get.and.callFake(
        function(){
          return defer2});


      MockPrincipal = jasmine.createSpyObj('MockPrincipal', ['identity']);
      account={login:'123',authorities:['ROLE_APP_FINRE_OPS_ADMIN']};
      tmSrvDefer = DeferFactory.generate();
      MockPrincipal.identity.and.returnValue(tmSrvDefer.promise);

      MockXrefTreatyCustomCompany={treaties:jasmine.createSpyObj('treaties', ['query'])};
      MockXrefTreatyCustomCompany.treaties.query.and.returnValue(deferCustomCompany);

     allocationResult= [ {
        "id" : 9,
        "name" : "Cash Surplus Relief - FinRe",
        "description" : "Cash Surplus Relief - FinRe"
      }, {
        "id" : 11,
        "name" : "Cash Surplus Relief - Alt Fee",
        "description" : "Cash Surplus Relief - Alt Fee"
      }, {
        "id" : 1,
        "name" : "FinRe Fee",
        "description" : "FinRe Fee"
      }, {
        "id" : 3,
        "name" : "Tax",
        "description" : "Tax"
      }, {
        "id" : 4,
        "name" : "Cash Surplus Relief",
        "description" : "Cash Surplus Relief"
      }, {
        "id" : 8,
        "name" : "Alt Fee",
        "description" : "Alternate Fee"
      }, {
        "id" : 7,
        "name" : "Deferred Income",
        "description" : "Deferred Income"
      } ];

      MockCompanyCustomSrv=jasmine.createSpyObj('MockCompanyCustomSrv', ['rgaCompanies']);
      MockCompanyCustomSrv.rgaCompanies={query:function(){}};



      MockCurrencyCustomSrv = { rate:jasmine.createSpyObj('rate', ['get']) };

    MockCurrencyCustomSrv.rate.get.and.returnValue(deferExchangeRate);

      MockTreatyTransactionSrv = {
        transactions: {
          get: function (obj, callback) {
            callback({},{});
          }
        }
      };
      defer= DeferFactory.generate();

      deferStatus= DeferFactory.generate();
      deferTreaty=DeferFactory.generate();
      MockTransactionStatus=jasmine.createSpyObj('TransactionStatus', ['query']);

      MockTransactionStatus.query.and.callFake(
        function(){
          return deferStatus});
      MockTreatyTracking = jasmine.createSpyObj('MockTreatyTracking', ['save', 'update']);
      MockIntro =jasmine.createSpyObj('Intro', ['setupIntroSteps']);

      MockIntro.setupIntroSteps.and.returnValue(DeferFactory.generate().promise);



      Ctrl = $controller("TreatyTransactionCashController", {
        '$scope': $scope,
        '$rootScope': $rootScope,
        '$stateParams':{ id:123 },
        'DateUtils': DateUtils,
        'FundingType': MockFundingType,
        'TreatyTrackingCustomSvr':MockTreatyTrackingCustomSvr,
        'TreatyTransactionSrv': MockTreatyTransactionSrv,
        'TransactionStatus':MockTransactionStatus,
        'TreatyTracking': MockTreatyTracking,
        'Intro':MockIntro,
          'entity': MockEntity,
          'TreatyTransaction': MockTreatyTransaction,
          'Principal': MockPrincipal,
          'Treaty': MockTreaty,
          'CompanyCustomSrv':MockCompanyCustomSrv,
          'XrefTreatyCustomCompany':MockXrefTreatyCustomCompany,
          'AllocationType':MockAllocationType,
          'CurrencyCustomSrv':MockCurrencyCustomSrv,
          '$q': $q
        });


      var resultFunding =[{id: 3, name: "Wire-In", description: "Wire incoming"} ,
                    {id: 5, name: "N/A", description: "N/A"}];
      defer.resolve(resultFunding);
      defer2.resolve({amount:6});
      deferStatus.resolve([
      {id: 3, name: "Expected", description: "Expected"},
      {id: 1, name: "Actual", description: "Actual"},
      {id: 2, name: "Estimated", description: "Estimated"}]);




    }));


    describe('Init State', function () {
      it('Ctrl should be defined', function () {
        tmSrvDefer.resolve(account);
        Ctrl.treatyId=123;
        $rootScope.$apply();
        expect(Ctrl).toBeDefined();

      });
    });
    describe('Main Content', function () {
      describe('Reorder', function () {
        it('should reorder so that the newest items are first', function () {
          Ctrl.mainTransactionList = [
            {subTransactions: [{id: 1}]},
            {subTransactions: [{id: 2}]}
          ];
          Ctrl.reorder();
          expect(Ctrl.mainTransactionList[0].subTransactionsMin).toBe(2);
          expect(Ctrl.mainTransactionList[1].subTransactionsMin).toBe(1);
        });
        it('should not need to reorder when already in order', function () {
          Ctrl.mainTransactionList = [
            {subTransactions: [{id: 2}]},
            {subTransactions: [{id: 1}]}
          ];
          Ctrl.reorder();
          expect(Ctrl.mainTransactionList[0].subTransactionsMin).toBe(2);
          expect(Ctrl.mainTransactionList[1].subTransactionsMin).toBe(1);
        });
        it('openCalendar should set date to true', function () {
          var date = 'date';
          Ctrl.datePickerOpenStatus[date] = false;
          Ctrl.openCalendar(date);
          expect(Ctrl.datePickerOpenStatus[date]).toBe(true);
        });
      });
      describe('Treaty Load', function () {
        it('should get the treaty if it isn\'t set', function () {
          deferAllocation.resolve(allocationResult);
          deferTreaty.resolve(resultTreaty);
          $rootScope.$apply();
          expect(Ctrl.treatyTransaction.treaty).toBeDefined();
          expect(Ctrl.treatyTransaction.treaty.id).toBe(111);
        });
      });
      describe('Account Load', function () {

        it('Principal Account', function () {
          tmSrvDefer.resolve(account);

          Ctrl.treatyId=123;
          $rootScope.$apply();
         expect(MockPrincipal.identity).toHaveBeenCalled();
        expect(Ctrl.treatyTransaction.lastUser).toBeDefined();
           expect(Ctrl.treatyTransaction.lastUpdate).toBeDefined();
          expect(Ctrl.lastUser).toBe('123');
        });

        it('Principal Account', function () {
          tmSrvDefer.resolve(account);

          Ctrl.treatyId=123;
          $rootScope.$apply();
          expect(MockPrincipal.identity).toHaveBeenCalled();
          expect(Ctrl.treatyTransaction.lastUser).toBeDefined();
          expect(Ctrl.treatyTransaction.lastUpdate).toBeDefined();
          expect(Ctrl.lastUser).toBe('123');
          expect(Ctrl.authority).toBe('Admin');
        });

       it('Principal Account view only', function () {
         tmSrvDefer.resolve(account2);
         Ctrl.treatyId=123;
         $rootScope.$apply();
          expect(Ctrl.authority).toBe('ViewOnly');
        });

        it('Principal Account User', function () {
          tmSrvDefer.resolve(account3);
          Ctrl.treatyId=123;
          $rootScope.$apply();
          expect(Ctrl.authority).toBe('User');
        });
      });
      describe('FundingType', function () {
        it('FundingType Loading', function () {
          Ctrl.treatyId=123;
          $rootScope.$apply();
          expect(Ctrl.fundingTypeOptions).toBeDefined();
          expect(Ctrl.fundingTypeOptionsActual).toBeDefined();
          expect(Ctrl.fundingTypeNa).toBeDefined();
        });
      });
      describe('CompanyCustomSrv', function () {
        it('CompanyCustomSrv rgaCompanies', function () {
          expect(Ctrl.rgaCompaniesCheck).toBeDefined();
          expect(Ctrl.legalEntityOptions).toBeDefined();

        });
      });
      describe('Tracking count', function () {
        it('Tracking count Loading', function () {
         // expect(MockTreatyTrackingCustomSvr.count.get).toHaveBeenCalled();
          Ctrl.treatyId=123;
          $rootScope.$apply();
          expect(Ctrl.trackingCount).toBeDefined();
          expect(Ctrl.trackingCount).toBe(6);
        });
      });
      describe('Status - Transaction ', function () {
        it('Get status #1', function () {
          Ctrl.getStatus(1);
          var result=[
            {id: 3, name: "Expected", description: "Expected"},
            {id: 1, name: "Actual", description: "Actual"},
            {id: 2, name: "Estimated", description: "Estimated"}];
          deferStatus.resolve(result);
          $rootScope.$apply();
         expect(MockTransactionStatus.query).toHaveBeenCalled();

          expect(Ctrl.transactionStatuses.length).toBe(3);
        });

        it('Get status #0', function () {
          Ctrl.getStatus(0);
          var result=[
            {id: 3, name: "Expected", description: "Expected"},
            {id: 1, name: "Actual", description: "Actual"},
            {id: 2, name: "Estimated", description: "Estimated"}];
          deferStatus.resolve(result);
          $rootScope.$apply();
          expect(MockTransactionStatus.query).toHaveBeenCalled();

          expect(Ctrl.transactionStatuses.length).toBe(1);
        });
        it('Get status #2', function () {
          Ctrl.getStatus(2);
          var result=[
            {id: 3, name: "Expected", description: "Expected"},
            {id: 1, name: "Actual", description: "Actual"},
            {id: 2, name: "Estimated", description: "Estimated"}];
          deferStatus.resolve(result);
          $rootScope.$apply();
          expect(MockTransactionStatus.query).toHaveBeenCalled();
          expect(Ctrl.transactionStatuses.length).toBe(2);
        });
      });

      describe('Exchange Rate', function () {
        it('Calculation', function () {
          var date = new Date("2017-03-01");
          var mainTransaction= {fundingType:{name:'Wire-Out'},convertedNetTotal:123,netTotal:123,subTransaction:{id:123,amount:123} ,currency:{id:123,code:'AUD'}, accountingDate:date };
          Ctrl.treatyId=123;
          deferExchangeRate.resolve(resultExchangeRate);
          Ctrl.exchangeRate(mainTransaction);
          $rootScope.$apply();
          expect(MockCurrencyCustomSrv.rate.get).toHaveBeenCalled();
          expect(Ctrl.currentExchangeRate).toBeDefined();
          expect(Ctrl.currentExchangeRate).toBe(0.718964);
        });
      });

      describe('SubTransactionCurrency', function () {
        it('Calculation by subtransaction change', function () {
          var date = new Date("2017-03-01");
          var mainTransaction= {fundingType:{name:'Wire-Out'},convertedNetTotal:123,netTotal:123,subTransaction:{id:123,amount:123} ,currency:{id:123,code:'AUD'}, accountingDate:date };
          var transaction= {amount:123};
          Ctrl.treatyId=123;
          deferExchangeRate.resolve(resultExchangeRate);
          Ctrl.SubTransactionCurrency(transaction,mainTransaction);
          $rootScope.$apply();
          expect(Ctrl.SubTransactionCurrency).toBeDefined();
          expect(MockCurrencyCustomSrv.rate.get).toHaveBeenCalled();
          expect(Ctrl.currentExchangeRate).toBe(0.718964);
        });
      });

      describe('date picker', function () {
        it('DateOpen', function () {
          var trans= {settlementDateOpened:false ,accountingDateOpened:false,reportingDateOpened:false};
          expect(Ctrl.openSettlementDate).toBeDefined();
          Ctrl.openSettlementDate(trans);
          expect(trans.settlementDateOpened).toBe(true);
          expect(Ctrl.openAccountingDate).toBeDefined();
          Ctrl.openAccountingDate(trans);
          expect(trans.accountingDateOpened).toBe(true);
          expect(Ctrl.openReportingDate).toBeDefined();
          Ctrl.openReportingDate(trans);
          expect(trans.reportingDateOpened).toBe(true);
        });
      });

      describe('addSubTransaction ', function () {
        it('addSubTransaction ', function () {
          var mainTransaction= {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123}] ,currency:{id:123,code:'AUD'} };
          Ctrl.treatyTransaction={treaty:{deal:{id:123}}};
          expect(Ctrl.addSubTransaction).toBeDefined();
          expect(mainTransaction.subTransactions.length).toBe(1);
          Ctrl.addSubTransaction(mainTransaction);
          expect(mainTransaction.subTransactions.length).toBe(2);
        });
      });

      describe('remove sub ', function () {
        it('removeSubTransaction ', function () {
          var mainTransaction= {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} };
          Ctrl.treatyTransaction={treaty:{deal:{id:123}}};
          expect(Ctrl.removeSubTransaction).toBeDefined();
          expect(mainTransaction.subTransactions.length).toBe(2);
          Ctrl.removeSubTransaction(mainTransaction,1);
          expect(mainTransaction.subTransactions.length).toBe(1);
        });
      });

      describe('removeTransaction', function () {
        it('removeTransaction ', function () {
          Ctrl.mainTransactionList=[ {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} },
            {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
              netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} }
          ];
          Ctrl.firstName='Lucia';
          Ctrl.authority='Admin';
          Ctrl.treatyTransaction={treaty:{deal:{id:123,gfsAssignee:{firstName:'Lucia'}}}};
          expect(Ctrl.removeTransaction).toBeDefined();
          expect(Ctrl.mainTransactionList.length).toBe(2);
          Ctrl.removeTransaction(1);
          expect(Ctrl.mainTransactionList.length).toBe(1);
        });
      });

      describe('mainTransactionAdd', function () {
        it('mainTransactionAdd - aCTIVE treaty', function () {
          Ctrl.mainTransactionList=[ {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} },
            {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
              netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} }
          ];
          Ctrl.treatyTransaction={treaty:{treatyStatus:{name:'Active'},deal:{id:123}}};
          expect(Ctrl.mainTransactionAdd).toBeDefined();
          expect(Ctrl.mainTransactionList.length).toBe(2);
          Ctrl.mainTransactionAdd();
          Ctrl.addMain();
          expect(Ctrl.mainTransactionList.length).toBe(3);
        });
      });

      describe('MockXrefTreatyCustomCompany', function () {
        it('MockXrefTreatyCustomCompany - Result', function () {
          Ctrl.mainTransactionList=[ {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} },
            {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
              netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} }
          ];
          Ctrl.treatyTransaction={treaty:{treatyStatus:{name:'Active'},deal:{id:123}}};
          var result =[{},{},{}];
          deferCustomCompany.resolve(result);
          $rootScope.$apply();
          Ctrl.mainTransactionAdd();
          expect(MockXrefTreatyCustomCompany.treaties.query).toHaveBeenCalled();
        });
        it('MockXrefTreatyCustomCompany - Result', function () {
        var result =null;
          Ctrl.treatyTransaction={treaty:{treatyStatus:{name:'Active'},deal:{id:123}}};
        deferCustomCompany.resolve(result);
        $rootScope.$apply();
        Ctrl.mainTransactionAdd();
        expect(MockXrefTreatyCustomCompany.treaties.query).toHaveBeenCalled();
        });
      });


      describe('copyTransaction ', function () {
        it('copyTransaction', function () {
          Ctrl.mainTransactionList=[ {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} },
            {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
              netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} }
          ];
          Ctrl.treatyTransaction={treaty:{treatyStatus:{name:'Active'},deal:{id:123}}};
          expect(Ctrl.copyTransaction).toBeDefined();
          expect(Ctrl.mainTransactionList.length).toBe(2);
          Ctrl.copyTransaction(1);
          expect(Ctrl.mainTransactionList.length).toBe(3);
        });
      });

      describe('Change Funding Status ', function () {
        it('changeStatusFunding Estimated', function () {
          var mainTransaction= {transactionStatus:{name:'Estimated'},fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} };
          Ctrl.treatyTransaction={treaty:{agreementType:{name:'Retro – Internal'},treatyStatus:{name:'Active'},deal:{id:123}}};
          expect(Ctrl.changeStatusFunding).toBeDefined();
          Ctrl.changeStatusFunding(mainTransaction,1);
          expect(mainTransaction.settlementDate).toBe(null);
        });

        it('changeStatusFunding Actual Retro – Internal', function () {
          var mainTransaction= {transactionStatus:{name:'Actual'},fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} ,
            treaty:{agreementType:{name:'Retro – Internal'},treatyStatus:{name:'Active'},deal:{id:123}}
          };
          Ctrl.treatyTransaction={treaty:{agreementType:{name:'Retro – Internal'},treatyStatus:{name:'Active'},deal:{id:123}}};
          expect(Ctrl.changeStatusFunding).toBeDefined();
          Ctrl.fundingTypeNa='N/A';
          Ctrl.changeStatusFunding(mainTransaction,1);
          expect(mainTransaction.fundingType).toBe('N/A');
        });

        it('changeStatusFunding Actual ', function () {
          var mainTransaction= {transactionStatus:{name:'Actual'},fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} ,
            treaty:{agreementType:{name:'Inbound Treaty'},treatyStatus:{name:'Active'},deal:{id:123}}
          };
          Ctrl.treatyTransaction={treaty:{agreementType:{name:'Inbound Treaty'},treatyStatus:{name:'Active'},deal:{id:123}}};
          expect(Ctrl.changeStatusFunding).toBeDefined();
          Ctrl.fundingTypeNa='N/A';
          Ctrl.changeStatusFunding(mainTransaction,1);
          expect(mainTransaction.fundingType).toBe(null);
        });

        it('changeStatusFunding Expected ', function () {
          var mainTransaction= {transactionStatus:{name:'Expected'},fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} ,
            treaty:{agreementType:{name:'Inbound Treaty'},treatyStatus:{name:'Active'},deal:{id:123}}
          };
          Ctrl.treatyTransaction={treaty:{agreementType:{name:'Inbound Treaty'},treatyStatus:{name:'Active'},deal:{id:123}}};
          expect(Ctrl.changeStatusFunding).toBeDefined();
          Ctrl.fundingTypeNa='N/A';
          Ctrl.changeStatusFunding(mainTransaction,1);
          expect(mainTransaction.fundingType).toBe(null);
        });
      });

      describe('checkFundingType', function () {
        it('checkFundingType  subTransactionLevel', function () {
        var transaction ={
          fundingType : {
            "id" : 3,
              "name" : "Wire-In",
              "description" : "Wire incoming"
          },netTotal : 119289.52,
          convertedNetTotal : 119289.52
          };
          expect(Ctrl.checkFundingType).toBeDefined();
          Ctrl.checkFundingType(transaction);
          expect(transaction.netTotal).toBe(119289.52);
        });
        it('checkFundingType  subTransactionLevel', function () {
          var transaction ={
            fundingType : {
              "name" : "Wire-Out",
              "description" : "Wire incoming"
            },netTotal : 119289.52,
            convertedNetTotal : 119289.52
          };
          expect(Ctrl.checkFundingType).toBeDefined();
          Ctrl.checkFundingType(transaction);
          expect(transaction.netTotal).toBe(119289.52);
        });
      });

      describe('sortBy', function () {
        it('changeStatusFunding Expected ', function () {

          expect(Ctrl.sortBy).toBeDefined();
          Ctrl.sortBy('Period');
          expect(Ctrl.propertyName).toBe('Period');
          expect(Ctrl.sortBy).toBeDefined();
          Ctrl.sortBy(null);
          expect(Ctrl.propertyName).toBe(null)
        });
      });

      describe('filter', function () {
        beforeEach(function(){
          MockTreatyTransactionSrv.transactions.get = jasmine.createSpy('get');
          MockTreatyTransactionSrv.transactions.get.and.returnValue(DeferFactory.generate())
        })
        it('filter', function () {
            var form= { $dirty:true};
          expect(Ctrl.filter).toBeDefined();
          Ctrl.filter(form);
          expect(form.$dirty).toBe(false);
        });
        it('loadAllCash', function () {
          expect(Ctrl.loadAllCash).toBeDefined();
          Ctrl.flagDirty=1;
          Ctrl.loadAllCash();
          expect(Ctrl.flagDirty).toBe(0);
        });
      });

      describe('reset', function () {
        it('reset Form ', function () {
          var form= { $dirty:true};
          Ctrl.searchEndDate="2017-01-01";
          Ctrl.searchEndDate="2017-01-01";
          expect(Ctrl.reset).toBeDefined();
          Ctrl.reset(1,form);
          expect(form.$dirty).toBe(false);
          expect(Ctrl.searchEndDate).toBe(null);
          Ctrl.searchEndDate="2017-01-01";
          Ctrl.reset(2,form);
          expect(Ctrl.searchEndDate).toBe("2017-01-01");

        });
      });

      describe('collapse', function () {
        it('collapse Transaction ', function () {
          Ctrl.mainTransactionList=[ {isCollapsed:true,fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} },
            {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
              netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} }
          ];
          expect(Ctrl.collapse).toBeDefined();
          Ctrl.collapse(1);
          expect(Ctrl.mainTransactionList[0].isCollapsed).toBe(false);
          expect(Ctrl.expand).toBe(0);
          Ctrl.collapse(2);
          expect(Ctrl.mainTransactionList[0].isCollapsed).toBe(true);
          expect(Ctrl.expand).toBe(1);
        });
      });


      describe('checkCompanyEk ', function () {
        it('checkCompanyEk for subTransaction', function () {
          var mainTransaction= {company:{legalCode:'LDP'}, transactionStatus:{name:'Estimated'},fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} };
          var transaction={id:123,amount:123,mdm:{legalCode:'LDP'}};
          var transaction2={id:123,amount:123,mdm:null};
          expect(Ctrl.checkCompanyEk).toBeDefined();
          Ctrl.checkCompanyEk(mainTransaction,transaction);
          Ctrl.checkCompanyEk(mainTransaction,transaction2);
          expect(Ctrl.updateLegalEntity).toBeDefined();
          Ctrl.updateLegalEntity(mainTransaction);
        });
      });

      describe('changeSubTracking ', function () {
        it('changeSubTracking for subTransaction', function () {
          Ctrl.listOfTrackingIds=[{trackingId:123},{trackingId:13}];
          var tracking = {id:123};
          expect(Ctrl.changeSubTracking).toBeDefined();
          Ctrl.changeSubTracking(tracking,"123","234");
        });
      });

    });
  });

});
