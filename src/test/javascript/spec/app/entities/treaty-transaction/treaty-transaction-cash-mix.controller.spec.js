'use strict';

describe('Controller Tests', function () {

  describe('TreatyTransaction Cash MIX Controller', function () {
    var $scope, $rootScope,MockTreatySrv,deferDeal;
    var MockEntity, MockTreatyTransaction,resultExchangeRate, MockTreaty,MockPrincipal,DeferFactory,MockFundingType;
    var MockCurrencyCustomSrv;
    var MockTreatyTrackingCustomSvr,MockCompanyCustomSrv,deferExchangeRate,MockXrefTreatyCustomCompany,MockAllocationType;
    var account2,Ctrl,MockTreatyTransactionMixSrv,deferNopages,deferDeals;
    var identity,deferTreaty,deferAllocation ,defer2,allocationResult,resultTreaty, account,tmSrvDefer,defer, MockTreatyTransactionSrv,MockTransactionStatus, MockTreatyTracking, MockIntro,deferStatus;

    beforeEach(inject(function ($injector, $controller, _DeferFactory_ ,$stateParams,DateUtils,$q) {
      DeferFactory = _DeferFactory_;
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      deferTreaty=DeferFactory.generate();
      deferAllocation=DeferFactory.generate();
      deferNopages=DeferFactory.generate();
      deferDeals=DeferFactory.generate();
      deferExchangeRate= DeferFactory.generate();
      MockEntity = jasmine.createSpy('MockEntity');
      MockTreatyTransaction = jasmine.createSpy('MockTreatyTransaction');
      MockTreaty = jasmine.createSpyObj('MockTreaty', ['get']);
      MockTreaty.get.and.callFake(
        function(result){
          return deferTreaty});

      resultExchangeRate={
        "id" : 849830969,
        "code" : "AUD",
        "name" : "Australian Dollar",
        "description" : "Australian Dollar",
        "year" : 2017,
        "exchangeRate" : 0.718964,
        "effectiveDate" : "2017-12-01"
      };
      MockAllocationType= jasmine.createSpyObj('MockAllocationType', ['query']);
      MockAllocationType.query.and.callFake(
        function(result){
          return deferAllocation});
      resultTreaty = {
        id: 111,
        treatyStatus: { name:'Active' },
        mdmAttributes: [{ id:123 ,ekComment:'test' },{id:13 ,ekComment:'null' }],
        treatyCurrencies: {currency:{id:123, code:'USD'}},
        deal:{id:123,productLine:{name:'Finre'}},
        agreementType :{name:'Fee'}
      };
      defer= DeferFactory.generate();
      defer2= DeferFactory.generate();
      MockFundingType = jasmine.createSpyObj('MockFundingType', ['query']);
      account2={login:'1823',authorities:["ROLE_APP_FINRE_VIEW_ONLY"]};
      MockFundingType.query.and.callFake(
        function(result){
          return defer});

      MockTreatyTrackingCustomSvr = {
        count:jasmine.createSpyObj('count', ['get'])
      };
      MockTreatyTrackingCustomSvr.count.get.and.callFake(
        function(){
          return defer2});

      MockPrincipal = jasmine.createSpyObj('MockPrincipal', ['identity']);


      account={login:'123',authorities:['ROLE_APP_FINRE_OPS_ADMIN']};


      tmSrvDefer = DeferFactory.generate();
      MockPrincipal.identity.and.returnValue(tmSrvDefer.promise);


      MockTreatyTransactionMixSrv=jasmine.createSpyObj('MockTreatyTransactionMixSrv', ['get']);

      MockCompanyCustomSrv=jasmine.createSpyObj('MockCompanyCustomSrv', ['rgaCompanies']);
      MockCompanyCustomSrv.rgaCompanies={query:function(){}};

      MockXrefTreatyCustomCompany=jasmine.createSpyObj('XrefTreatyCustomCompany', ['treaties']);
      MockXrefTreatyCustomCompany.treaties={query:function(){}};
      MockCurrencyCustomSrv = { rate:jasmine.createSpyObj('rate', ['get']) };
      MockCurrencyCustomSrv.rate.get.and.returnValue(deferExchangeRate);

      MockTreatySrv = {
        nopages:jasmine.createSpyObj('nopages', ['query']),
        deals:jasmine.createSpyObj('deals', ['query'])
      };

      MockTreatyTransactionSrv = {
        transactions: {
          get: function (obj, callback) {
            callback();
          }
        }
      };
      allocationResult=
        [ {
        "id" : 9,
        "name" : "Cash Surplus Relief - FinRe",
        "description" : "Cash Surplus Relief - FinRe"
      }, {
        "id" : 11,
        "name" : "Cash Surplus Relief - Alt Fee",
        "description" : "Cash Surplus Relief - Alt Fee"
      }, {
        "id" : 1,
        "name" : "FinRe Fee",
        "description" : "FinRe Fee"
      }, {
        "id" : 3,
        "name" : "Tax",
        "description" : "Tax"
      }, {
        "id" : 4,
        "name" : "Cash Surplus Relief",
        "description" : "Cash Surplus Relief"
      }, {
        "id" : 8,
        "name" : "Alt Fee",
        "description" : "Alternate Fee"
      }, {
        "id" : 7,
        "name" : "Deferred Income",
        "description" : "Deferred Income"
      } ];
      defer= DeferFactory.generate();
      deferDeal= DeferFactory.generate();
      deferStatus= DeferFactory.generate();
      MockTransactionStatus=jasmine.createSpyObj('TransactionStatus', ['query']);

      MockTransactionStatus.query.and.callFake(
        function(){
          return deferStatus});
      MockTreatyTracking = jasmine.createSpyObj('MockTreatyTracking', ['save', 'update']);
      MockIntro =jasmine.createSpyObj('Intro', ['setupIntroSteps']);
      MockIntro.setupIntroSteps.and.returnValue(DeferFactory.generate().promise);
      MockTreatySrv.nopages.query.and.returnValue(deferNopages);
      MockTreatySrv.deals.query.and.returnValue(deferDeals);

      Ctrl = $controller("TreatyTransactionCashMixController", {
        '$scope': $scope,
        '$rootScope': $rootScope,
        '$stateParams':{ id:123 },
        'DateUtils': DateUtils,
        'FundingType': MockFundingType,
        'TreatySrv': MockTreatySrv,
        'TreatyTrackingCustomSvr':MockTreatyTrackingCustomSvr,
        'TreatyTransactionSrv': MockTreatyTransactionSrv,
        'TreatyTransactionMixSrv': MockTreatyTransactionMixSrv,
        'TransactionStatus':MockTransactionStatus,
        'TreatyTracking': MockTreatyTracking,
        'Intro':MockIntro,
        'entity': MockEntity,
        'TreatyTransaction': MockTreatyTransaction,
        'Principal': MockPrincipal,
        'Treaty': MockTreaty,
        'CompanyCustomSrv':MockCompanyCustomSrv,
        'XrefTreatyCustomCompany':MockXrefTreatyCustomCompany,
        'AllocationType':MockAllocationType,
        'CurrencyCustomSrv':MockCurrencyCustomSrv,
        '$q': $q
      });


      var resultFunding =[{id: 3, name: "Wire-In", description: "Wire incoming"} ,
        {id: 5, name: "N/A", description: "N/A"}];
      defer.resolve(resultFunding);
      defer2.resolve({amount:6});
      deferStatus.resolve([
        {id: 3, name: "Expected", description: "Expected"},
        {id: 1, name: "Actual", description: "Actual"},
        {id: 2, name: "Estimated", description: "Estimated"}]);




    }));


    describe('Init State', function () {
      it('Ctrl should be defined', function () {
        tmSrvDefer.resolve(account);
        Ctrl.treatyId=123;
        $rootScope.$apply();
        expect(Ctrl).toBeDefined();

      });
    });
    describe('Main Content', function () {
      describe('Reorder', function () {
        it('should reorder so that the newest items are first', function () {
          Ctrl.mainTransactionList = [
            {subTransactions: [{id: 1}]},
            {subTransactions: [{id: 2}]}
          ];
          Ctrl.reorder();
          expect(Ctrl.mainTransactionList[0].subTransactionsMin).toBe(2);
          expect(Ctrl.mainTransactionList[1].subTransactionsMin).toBe(1);
        });
        it('should not need to reorder when already in order', function () {
          Ctrl.mainTransactionList = [
            {subTransactions: [{id: 2}]},
            {subTransactions: [{id: 1}]}
          ];
          Ctrl.reorder();
          expect(Ctrl.mainTransactionList[0].subTransactionsMin).toBe(2);
          expect(Ctrl.mainTransactionList[1].subTransactionsMin).toBe(1);
        });
        it('openCalendar should set date to true', function () {
          var date = 'date';
          Ctrl.datePickerOpenStatus[date] = false;
          Ctrl.openCalendar(date);
          expect(Ctrl.datePickerOpenStatus[date]).toBe(true);
        });
      });
      describe('Treaty Load', function () {
        it('should get the treaty if it isn\'t set', function () {

          deferTreaty.resolve(resultTreaty);
          $rootScope.$apply();
          expect(Ctrl.treatyTransaction.treaty).toBeDefined();
          expect(Ctrl.treatyTransaction.treaty.id).toBe(111);
          resultTreaty = {
            id: 111,
            treatyStatus: { name:'Active' },
            mdmAttributes: [{ id:123 ,ekComment:'test' },{id:13 ,ekComment:'null' }],
            treatyCurrencies: {currency:{id:123, code:'USD'}},
            deal:{id:123,productLine:{name:'Finre'}},
            agreementType :{name:'Fee'}
          };
        });
        it('should get the treaty if it isn\'t set', function () {
          resultTreaty = {
            id: 333,
            treatyStatus: { name:'Pending' },
            mdmAttributes: [{ id:123 ,ekComment:'test' },{id:13 ,ekComment:'null' }],
            treatyCurrencies: {currency:{id:123, code:'USD'}},
            deal:{id:123,productLine:{name:'Finre'}},
            agreementType :{name:'Fee'}
          };
          deferTreaty.resolve(resultTreaty);
          $rootScope.$apply();
          expect(Ctrl.treatyTransaction.treaty).toBeDefined();
          expect(Ctrl.treatyTransaction.treaty.id).toBe(333);

        });
        it('should get the treaty if it isn\'t set', function () {
          resultTreaty = {
            id: 222,
            treatyStatus: { name:'Active' },
            mdmAttributes: [{ id:123 ,ekComment:'test' },{id:13 ,ekComment:'null' }],
            treatyCurrencies: {currency:{id:123, code:'USD'}},
            deal:{id:123,productLine:{name:'Finre'}},
            agreementType :{name:'Retro – Internal'}
          };
          deferTreaty.resolve(resultTreaty);
          $rootScope.$apply();
          expect(Ctrl.treatyTransaction.treaty).toBeDefined();
          expect(Ctrl.treatyTransaction.treaty.id).toBe(222);

        });
      });
      describe('Account Load', function () {

        it('Principal Account', function () {
          tmSrvDefer.resolve(account);
          Ctrl.treatyId=123;
          $rootScope.$apply();
          expect(MockPrincipal.identity).toHaveBeenCalled();
          expect(Ctrl.treatyTransaction.lastUser).toBeDefined();
          expect(Ctrl.treatyTransaction.lastUser).toBe('123');
        });

        it('Principal Account', function () {
          tmSrvDefer.resolve(account);
          Ctrl.treatyId=123;
          $rootScope.$apply();
          expect(MockPrincipal.identity).toHaveBeenCalled();
          expect(Ctrl.treatyTransaction.lastUser).toBeDefined();
          expect(Ctrl.treatyTransaction.lastUser).toBe('123');
          expect(Ctrl.authority).toBe('Admin');
        });

        it('Principal Account view only', function () {
          tmSrvDefer.resolve(account2);
          Ctrl.treatyId=123;
          $rootScope.$apply();
          expect(Ctrl.authority).toBe('ViewOnly');
        });

      });
      describe('FundingType', function () {
        it('FundingType Loading', function () {
          Ctrl.treatyId=123;
          $rootScope.$apply();
          expect(Ctrl.fundingTypeOptions).toBeDefined();
          expect(Ctrl.fundingTypeOptionsActual).toBeDefined();
          expect(Ctrl.fundingTypeNa).toBeDefined();
        });
      });
      describe('CompanyCustomSrv', function () {
        it('CompanyCustomSrv rgaCompanies', function () {
          expect(Ctrl.rgaCompaniesCheck).toBeDefined();
          expect(Ctrl.legalEntityOptions).toBeDefined();

        });
      });
      describe('Tracking count', function () {
        it('Tracking count Loading', function () {
          // expect(MockTreatyTrackingCustomSvr.count.get).toHaveBeenCalled();
          Ctrl.treatyId=123;
          $rootScope.$apply();
          expect(Ctrl.trackingCount).toBeDefined();
          expect(Ctrl.trackingCount).toBe(6);
        });
      });
      describe('Status - Transaction ', function () {
        it('Get status #1', function () {
          Ctrl.getStatus(1);
          var result=[
            {id: 3, name: "Expected", description: "Expected"},
            {id: 1, name: "Actual", description: "Actual"},
            {id: 2, name: "Estimated", description: "Estimated"}];
          deferStatus.resolve(result);
          $rootScope.$apply();
          expect(MockTransactionStatus.query).toHaveBeenCalled();

          expect(Ctrl.transactionStatuses.length).toBe(3);
        });

        it('Get status #0', function () {
          Ctrl.getStatus(0);
          var result=[
            {id: 3, name: "Expected", description: "Expected"},
            {id: 1, name: "Actual", description: "Actual"},
            {id: 2, name: "Estimated", description: "Estimated"}];
          deferStatus.resolve(result);
          $rootScope.$apply();
          expect(MockTransactionStatus.query).toHaveBeenCalled();

          expect(Ctrl.transactionStatuses.length).toBe(1);
        });
        it('Get status #2', function () {
          Ctrl.getStatus(2);
          var result=[
            {id: 3, name: "Expected", description: "Expected"},
            {id: 1, name: "Actual", description: "Actual"},
            {id: 2, name: "Estimated", description: "Estimated"}];
          deferStatus.resolve(result);
          $rootScope.$apply();
          expect(MockTransactionStatus.query).toHaveBeenCalled();
          expect(Ctrl.transactionStatuses.length).toBe(2);
        });
      });
      describe('Account Load', function () {
        it('Principal Account', function () {
          var result= [{id:1,dealStatus:'Terminated'},{id:1,dealStatus:'Active'}];
          Ctrl.deals=[];
          deferNopages.resolve(result);
          $rootScope.$apply();
          expect(MockTreatySrv.nopages.query).toHaveBeenCalled();
          expect(Ctrl.deals.length).toBe(2);
          expect(Ctrl.deals[0].disabled).toBe(true);
        });
      });
      describe('date picker', function () {
        it('DateOpen', function () {
          var trans= {settlementDateOpened:false ,accountingDateOpened:false,reportingDateOpened:false};
          expect(Ctrl.openSettlementDate).toBeDefined();
          Ctrl.openSettlementDate(trans);
          expect(trans.settlementDateOpened).toBe(true);
          expect(Ctrl.openAccountingDate).toBeDefined();
          Ctrl.openAccountingDate(trans);
          expect(trans.accountingDateOpened).toBe(true);
          expect(Ctrl.openReportingDate).toBeDefined();
          Ctrl.openReportingDate(trans);
          expect(trans.reportingDateOpened).toBe(true);
        });
      });
      describe('treatyChange ', function () {
        it('treatyChange ', function () {
          Ctrl.treatyTransaction={treaty:{id:123}};
          var tran={legalEntityOptions:[] };
          expect(Ctrl.treatyChange).toBeDefined();
        });
      });
      describe('Allocation Load', function () {
        it('Allocation', function () {
          deferAllocation.resolve(allocationResult);
          $rootScope.$apply();
          expect(Ctrl.allocationTypes.length).toBe(6);
        });
      });
      describe('Exchange Rate', function () {
        it('Calculation', function () {
          var date = new Date("2017-03-01");
          var mainTransaction= {fundingType:{name:'Wire-Out'},convertedNetTotal:123,netTotal:123,subTransaction:{id:123,amount:123} ,currency:{id:123,code:'AUD'}, accountingDate:date };
          Ctrl.treatyId=123;
          deferExchangeRate.resolve(resultExchangeRate);
          Ctrl.exchangeRate(mainTransaction);
          $rootScope.$apply();
          expect(MockCurrencyCustomSrv.rate.get).toHaveBeenCalled();
          expect(Ctrl.currentExchangeRate).toBeDefined();
          expect(Ctrl.currentExchangeRate).toBe(0.718964);
        });
      });
      describe('SubTransactionCurrency', function () {
        it('Calculation by subtransaction change', function () {
          var date = new Date("2017-03-01");
          var mainTransaction= {fundingType:{name:'Wire-Out'},convertedNetTotal:123,netTotal:123,subTransaction:{id:123,amount:123} ,currency:{id:123,code:'AUD'}, accountingDate:date };
          var transaction= {amount:123};
          Ctrl.treatyId=123;
          deferExchangeRate.resolve(resultExchangeRate);
          Ctrl.SubTransactionCurrency(transaction,mainTransaction);
          $rootScope.$apply();
          expect(Ctrl.SubTransactionCurrency).toBeDefined();
          expect(MockCurrencyCustomSrv.rate.get).toHaveBeenCalled();
          expect(Ctrl.currentExchangeRate).toBe(0.718964);
        });
      });
      describe('addSubTransaction ', function () {
        it('addSubTransaction ', function () {
          var mainTransaction= {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123}] ,currency:{id:123,code:'AUD'} };
          Ctrl.treatyTransaction={treaty:{id:123,deal:{id:123}}};
          expect(Ctrl.addSubTransaction).toBeDefined();
          expect(mainTransaction.subTransactions.length).toBe(1);
          Ctrl.addSubTransaction(mainTransaction);
          expect(mainTransaction.subTransactions.length).toBe(2);
        });
      });
      describe('remove sub ', function () {
        it('removeSubTransaction ', function () {
          var mainTransaction= {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} };
          Ctrl.treatyTransaction={treaty:{deal:{id:123}}};
          expect(Ctrl.removeSubTransaction).toBeDefined();
          expect(mainTransaction.subTransactions.length).toBe(2);
          Ctrl.removeSubTransaction(mainTransaction,1);
          expect(mainTransaction.subTransactions.length).toBe(1);
        });
      });
      describe('copyTransaction ', function () {
        it('copyTransaction', function () {
          Ctrl.mainTransactionList=[ {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} },
            {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
              netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} }
          ];
          Ctrl.treatyTransaction={treaty:{treatyStatus:{name:'Active'},deal:{id:123}}};
          expect(Ctrl.copyTransaction).toBeDefined();
          expect(Ctrl.mainTransactionList.length).toBe(2);
          Ctrl.copyTransaction(1);
          expect(Ctrl.mainTransactionList.length).toBe(3);
        });
      });
      describe('Change Funding Status ', function () {
        it('changeStatusFunding Estimated', function () {
          var mainTransaction= {transactionStatus:{name:'Estimated'},fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} };
          Ctrl.treatyTransaction={treaty:{agreementType:{name:'Retro – Internal'},treatyStatus:{name:'Active'},deal:{id:123}}};
          expect(Ctrl.changeStatusFunding).toBeDefined();
          Ctrl.changeStatusFunding(mainTransaction,1);
          expect(mainTransaction.settlementDate).toBe(null);
        });

        it('changeStatusFunding Actual Retro – Internal', function () {
          var mainTransaction= {transactionStatus:{name:'Actual'},fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} ,
            treaty:{agreementType:{name:'Retro – Internal'},treatyStatus:{name:'Active'},deal:{id:123}}
          };
          Ctrl.treatyTransaction={treaty:{agreementType:{name:'Retro – Internal'},treatyStatus:{name:'Active'},deal:{id:123}}};
          expect(Ctrl.changeStatusFunding).toBeDefined();
          Ctrl.fundingTypeNa='N/A';
          Ctrl.changeStatusFunding(mainTransaction,1);
          expect(mainTransaction.fundingType).toBe('N/A');
        });

        it('changeStatusFunding Actual ', function () {
          var mainTransaction= {transactionStatus:{name:'Actual'},fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} ,
            treaty:{agreementType:{name:'Inbound Treaty'},treatyStatus:{name:'Active'},deal:{id:123}}
          };
          Ctrl.treatyTransaction={treaty:{agreementType:{name:'Inbound Treaty'},treatyStatus:{name:'Active'},deal:{id:123}}};
          expect(Ctrl.changeStatusFunding).toBeDefined();
          Ctrl.fundingTypeNa='N/A';
          Ctrl.changeStatusFunding(mainTransaction,1);
          expect(mainTransaction.fundingType).toBe(null);
        });

        it('changeStatusFunding Expected ', function () {
          var mainTransaction= {transactionStatus:{name:'Expected'},fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} ,
            treaty:{agreementType:{name:'Inbound Treaty'},treatyStatus:{name:'Active'},deal:{id:123}}
          };
          Ctrl.treatyTransaction={treaty:{agreementType:{name:'Inbound Treaty'},treatyStatus:{name:'Active'},deal:{id:123}}};
          expect(Ctrl.changeStatusFunding).toBeDefined();
          Ctrl.fundingTypeNa='N/A';
          Ctrl.changeStatusFunding(mainTransaction,1);
          expect(mainTransaction.fundingType).toBe(null);
        });
      });
      describe('checkFundingType', function () {
        it('checkFundingType  subTransactionLevel', function () {
          var transaction ={
            fundingType : {
              "id" : 3,
              "name" : "Wire-In",
              "description" : "Wire incoming"
            },netTotal : 119289.52,
            convertedNetTotal : 119289.52
          };
          expect(Ctrl.checkFundingType).toBeDefined();
          Ctrl.checkFundingType(transaction);
          expect(transaction.netTotal).toBe(119289.52);
        });
        it('checkFundingType  subTransactionLevel', function () {
          var transaction ={
            fundingType : {
              "name" : "Wire-Out",
              "description" : "Wire incoming"
            },netTotal : 119289.52,
            convertedNetTotal : 119289.52
          };
          expect(Ctrl.checkFundingType).toBeDefined();
          Ctrl.checkFundingType(transaction);
          expect(transaction.netTotal).toBe(119289.52);
        });
      });
      describe('changeMixTransaction ', function () {
        it('changeMixTransaction  ', function () {
          var mainTransaction= {mixTransactions :1,fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{deal:{id:123},id:123,amount:123,treaty:{id:123,deal:{id:123}} ,currency:{id:123,code:'AUD'}}] };
          Ctrl.treatyTransaction={treaty:{id:123,deal:{id:123}}};
          expect(Ctrl.changeMixTransaction).toBeDefined();
          Ctrl.changeMixTransaction(mainTransaction);

        });

      });
      describe('treatyByDeal ', function () {
        it('treatyByDeal  ', function () {
          var result =[{treatyStatus:{name:'Terminated'}}, {treatyStatus:{name:'Active'}}, {treatyStatus:{name:'Terminated'}}];
          deferDeals.resolve(result);
          $rootScope.$apply();
          Ctrl.treatyTransaction={treaty:{id:123,deal:{id:123}}};
          deferDeals.resolve(result);
          expect(Ctrl.treatyByDeal).toBeDefined();
          Ctrl.treatyByDeal(123);
        });
      });
      describe('changeDeal  ', function () {
        it('changeDeal   ', function () {
          var transaction ={
            deal:{id:123},
            fundingType : {
              "id" : 3,
              "name" : "Wire-In",
              "description" : "Wire incoming"
            },netTotal : 119289.52,
            convertedNetTotal : 119289.52
          };

          expect(Ctrl.changeDeal).toBeDefined();
          Ctrl.changeDeal(transaction);
          expect(transaction.treaties).toBeDefined();
        });
      });
      describe('sortBy', function () {
        it('changeStatusFunding Expected ', function () {

          expect(Ctrl.sortBy).toBeDefined();
          Ctrl.sortBy('Period');
          expect(Ctrl.propertyName).toBe('Period');
          expect(Ctrl.sortBy).toBeDefined();
          Ctrl.sortBy(null);
          expect(Ctrl.propertyName).toBe(null)
        });
      });
      describe('reset', function () {
        it('reset Form ', function () {
          var form= { $dirty:true};
          Ctrl.searchEndDate="2017-01-01";
          expect(Ctrl.reset).toBeDefined();
          Ctrl.reset(form);
          expect(form.$dirty).toBe(false);
          expect(Ctrl.searchEndDate).toBe(null);

        });
      });
      describe('collapse', function () {
        it('collapse Transaction ', function () {
          Ctrl.mainTransactionList=[ {isCollapsed:true,fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} },
            {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
              netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} }
          ];
          expect(Ctrl.collapse).toBeDefined();
          Ctrl.collapse(1);
          expect(Ctrl.mainTransactionList[0].isCollapsed).toBe(false);
          Ctrl.collapse(2);
          expect(Ctrl.mainTransactionList[0].isCollapsed).toBe(true);

        });
      });
      describe('defaultCompany ', function () {
        it('collapse Transaction ', function () {
          expect(Ctrl.defaultCompany).toBeDefined();
          Ctrl.defaultCompany();
        });
      });
      describe('updateLegalEntity  ', function () {
        it('collapse Transaction ', function () {
          var mainTransaction= {fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123,mdm:{id:123}}] ,currency:{id:123,code:'AUD'} };
          expect(Ctrl.updateLegalEntity).toBeDefined();
          Ctrl.updateLegalEntity(mainTransaction);
          expect(mainTransaction.subTransactions[0].mdm).toBe(null);
        });
      });

      describe('checkCompanyEk ', function () {
        it('checkCompanyEk for subTransaction', function () {
          var mainTransaction= {company:{legalCode:'LDP'}, transactionStatus:{name:'Estimated'},fundingType:{name:'Wire-Out'},deal:123,convertedNetTotal:123,
            netTotal:123,subTransactions:[{id:123,amount:123},{id:1223,amount:123}] ,currency:{id:123,code:'AUD'} };
          var transaction={id:123,amount:123,mdm:{legalCode:'LDP'}};
          var transaction2={id:123,amount:123,mdm:null};
          expect(Ctrl.checkCompanyEk).toBeDefined();
          Ctrl.checkCompanyEk(mainTransaction,transaction);
          Ctrl.checkCompanyEk(mainTransaction,transaction2);
          expect(Ctrl.updateLegalEntity).toBeDefined();
          Ctrl.updateLegalEntity(mainTransaction);
        });
      });

      describe('changeSubTracking ', function () {
        it('changeSubTracking for subTransaction', function () {
          Ctrl.listOfTrackingIds=[{trackingId:123},{trackingId:13}];
          var tracking = {id:123};
          expect(Ctrl.changeSubTracking).toBeDefined();
          Ctrl.changeSubTracking(tracking,"123","234");
        });
      });
    });
  });

});
