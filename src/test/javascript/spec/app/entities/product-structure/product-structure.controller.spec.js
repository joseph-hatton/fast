'use strict';

describe('Controller Tests', function() {

  describe('ProductStructure Controller', function() {

    var Ctrl;
    var scope;
    var MockProductStructure;
    var MockProductStructureSearch;
    var loadAllResult = [{},{},{}], searchResult = [{}];

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();

        MockProductStructure = {
          query: function (callback) {
            callback(loadAllResult);
          }
        };

        MockProductStructureSearch = {
          query: function (search, callback) {
            callback(searchResult);
          }
        };

        Ctrl = $controller('ProductStructureController', {
          $scope: scope,
          ProductStructure: MockProductStructure,
          ProductStructureSearch: MockProductStructureSearch
        });
      }));

    describe('Initial state', function () {
      it('ProductStructure Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('Load All Test', function() {
        expect(Ctrl.loadAll).toBeDefined();
      });

      it('Search Test', function() {
        expect(Ctrl.search).toBeDefined();
      });
    });

    describe('Main Content Cover', function () {
      it('Load All Test', function () {
        expect(Ctrl.productStructures).toBe(loadAllResult);
      });

      it('Search No Query Test', function () {
        Ctrl.searchQuery = null;
        Ctrl.search();
        expect(Ctrl.productStructures).toBe(loadAllResult);
      });

      it('Search Query Test', function () {
        Ctrl.searchQuery = 1;
        Ctrl.search();
        expect(Ctrl.productStructures).toBe(searchResult);
      });
    });
  });
});
