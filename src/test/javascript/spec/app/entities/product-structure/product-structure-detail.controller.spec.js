'use strict';

describe('Controller Tests', function() {

  describe('ProductStructure Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockProductStructure;
    var Ctrl;

    beforeEach(inject(function($controller,$injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      MockEntity = jasmine.createSpy('MockEntity');
      MockProductStructure = {
        get: function (id, callback) {
          callback(id);
        }
      };

      Ctrl = $controller("ProductStructureDetailController", {
        '$scope': $scope,
        '$rootScope': $rootScope,
        'entity': MockEntity ,
        'ProductStructure': MockProductStructure
      });

    }));

    describe('Initial State', function () {
      it('ProductStructure Detail Controller Should be Created', function () {
        expect(Ctrl).toBeDefined();
      });

      it('Load Should be Defined', function () {
        expect(Ctrl.load).toBeDefined();
      });
    });
  });

});
