'use strict';

describe('Controller Tests', function() {

    describe('StructuredTracking Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockStructuredTracking;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockStructuredTracking = jasmine.createSpy('MockStructuredTracking');


            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'StructuredTracking': MockStructuredTracking
            };
            createController = function() {
                $injector.get('$controller')("StructuredTrackingDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'finreApp:structuredTrackingUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
