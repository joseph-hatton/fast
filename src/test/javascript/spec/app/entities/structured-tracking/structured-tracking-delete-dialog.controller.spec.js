'use strict';

describe('Controller Tests', function() {
  describe('StructuredTrackingDeleteController', function() {
    var Ctrl;
    var scope,state,MockStructuredTracking,MockUiModalInstance, MockStructuredFinance;
    var MockEntity, MockPreviousState, MockStateParams, MockDeal, MockDealItem;
    var Timeout, MockInteractionService, MockStructuredFinanceObj;
    beforeEach(inject(
      function ($controller, $rootScope, $filter, $timeout) {     // Don't bother injecting a 'real' modal
        Timeout = $timeout;
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = {
          id: 123,
          structuredFinance: MockStructuredFinanceObj
        };
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockStructuredTracking = {
          update: function (structuredTracking, success, error) {
            success(structuredTracking);
          }
        };
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        MockStateParams = {
          dealId: 123,
          financeId: 123
        };
        MockInteractionService = jasmine.createSpyObj('InteractionService',
          ['confirmElaborateDialog', 'confirmBasicDialog', 'alertDialog', 'alertParentScrollFalseDialog', 'showThenDialog', 'showFinallyDialog']);
        MockStructuredFinanceObj = {
          yearEndException: 'no',
          reportingFrequency: { name:'Monthly' },
          numberOfDays: 12,
          numberOfDaysYr: 12,
          dayType: "Calendar Days After",
          dayTypeYr: "Calendar Days After",
          period: 'Year End',
          periodYr: 'Year End'
        };
        MockStructuredFinance = {
          get: function (idObj, callback) {
            callback(MockStructuredFinanceObj);
          }
        };
        function calculatingDate (){ }
        Ctrl = $controller('StructuredTrackingDeleteController', {
          $scope: scope,
          $state: state,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          $stateParams: MockStateParams,
          $filter:$filter,
          $timeout: $timeout,
          InteractionsService: MockInteractionService,
          StructuredTracking: MockStructuredTracking
        });
      }));

    describe('Initial state', function () {
      it('StructuredTrackingDialog Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('confirmDelete ', function() {
        expect(Ctrl.confirmDelete).toBeDefined();
        Ctrl.confirmDelete(123);
        //expect(MockUiModalInstance.close).toHaveBeenCalled();
      });

      it('Clear', function() {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });
    });

  });
});
