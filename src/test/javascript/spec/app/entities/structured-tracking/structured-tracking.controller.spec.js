'use strict';

describe('Controller Tests', function() {

  describe('StructuredTracking Controller', function() {
    var Ctrl;
    var scope,state,paginationConstants,pagingParams,stateParams,MockEntity,MockStructuredFinance, MockStructuredTrackingCustomSrv,MockParseLinks;
    var MockLoadAllQuery;

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state = { transitionTo: function (obj1, obj2) {} };
        stateParams={dealId:21420,financeId:22944};
        pagingParams={ascending:true,page:1,predicate:"id",search:null,sort:"id,asc"};
        paginationConstants= {itemsPerPage: 10};
        MockEntity = jasmine.createSpy('MockEntity');
        MockStructuredFinance = jasmine.createSpy('MockStructuredFinance');
        MockLoadAllQuery = {
          clientReportingDate: "01/2017",
          deal: {id:1234},
          structuredFinance: {
            reportingFrequency: {
              name: "Monthly"
            }
          }
        };
        MockStructuredTrackingCustomSrv = {
          structuredTrackingByFinance: {
            query: function (obj, success, error) {
              success([MockLoadAllQuery], function(str){});
            }
        }
      };
        MockParseLinks = {
          parse: function (msg) {}
        };
        Ctrl = $controller('StructuredTrackingController', {
          $scope: scope,
          $state: state,
          $stateParams:stateParams,
          entity: MockEntity ,
          StructuredFinance: MockStructuredFinance,
          paginationConstants:paginationConstants,
          pagingParams:pagingParams,
          StructuredTrackingCustomSrv: MockStructuredTrackingCustomSrv,
          ParseLinks: MockParseLinks
        });
      }));

    describe('Initial state', function () {
      it('StructuredTracking Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('StructuredTracking Load All Test', function() {
        expect(Ctrl.loadPage).toBeDefined();
      });

      it('StructuredTracking Search Test', function() {
        expect(Ctrl.search).toBeDefined();
      });
    });
    describe('Main Coverage', function () {
      describe('Load All', function () {
        describe('OnSuccess', function () {
          it('should set the deal', function () {
            expect(Ctrl.deal.id).toBe(MockLoadAllQuery.deal.id);
          });
          it('should set Monthly parameters when reportingFrequency is set to Monthly', function () {
            expect(Ctrl.dateOptions.dateDisabled).toBeUndefined();
            expect(Ctrl.dateOptions.datepickerMode).toBe('month');
          });
          it('should set Quarterly parameters when reportingFrequency is set to Quarterly', function () {
            MockLoadAllQuery.structuredFinance.reportingFrequency.name = "Quarterly";
            Ctrl.reset();
            expect(Ctrl.dateOptions.dateDisabled).toBeDefined();
            expect(Ctrl.dateOptions.datepickerMode).toBe('month');
          });
          it('should set Annual parameters when reportingFrequency is set to Annual', function () {
            MockLoadAllQuery.structuredFinance.reportingFrequency.name = "Annual";
            Ctrl.reset();
            expect(Ctrl.dateOptions.dateDisabled).toBeUndefined();
            expect(Ctrl.dateOptions.datepickerMode).toBe('year');
          });
          it('should set Semi-Annual parameters when reportingFrequency is set to Semi-Annual', function () {
            MockLoadAllQuery.structuredFinance.reportingFrequency.name = "Semi-Annual";
            Ctrl.reset();
            expect(Ctrl.dateOptions.dateDisabled).toBeDefined();
            expect(Ctrl.dateOptions.datepickerMode).toBe('month');
          });
        });
      });
      describe('LoadPage', function () {
        it('should set vm.page to the page', function () {
          var newPage = 123;
          Ctrl.page = null;
          Ctrl.loadPage(newPage);
          expect(Ctrl.page).toBe(newPage);
        });
      });
      describe('Clear', function () {
        it('should clear currentSearch', function () {
          Ctrl.currentSearch = "search";
          Ctrl.clear();
          expect(Ctrl.currentSearch).toBeNull();
        });
      });
      describe('Search', function () {
        it('should run clear when searchQuery is blank', function () {
          Ctrl.currentSearch = "search";
          Ctrl.search("");
          expect(Ctrl.currentSearch).toBeNull();
        });
        it('should run set vm.currentSearch to currentSearch', function () {
          var search = "search"
          Ctrl.currentSearch = null;
          Ctrl.search(search);
          expect(Ctrl.currentSearch).toBe(search);
        });
      });
      describe('Display Quarterly', function () {
        beforeEach(function () {
          MockLoadAllQuery.structuredFinance.reportingFrequency.name = "Quarterly";
          Ctrl.reset();
        });
        describe('should be true', function () {
          it('for 0', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,0,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
          it('for 1', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,1,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
          it('for 3', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,3,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
          it('for 4', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,4,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
          it('for 6', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,6,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
          it('for 7', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,7,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
          it('for 9', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,9,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
          it('for 10', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,10,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
        });
        describe('should be false', function () {
          it('for 2', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,2,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(false);
          });
          it('for 5', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,5,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(false);
          });
          it('for 8', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,8,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(false);
          });
          it('for 11', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,11,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(false);
          });
        });
      });
      describe('Display Semi-Annual', function () {
        beforeEach(function () {
          MockLoadAllQuery.structuredFinance.reportingFrequency.name = "Semi-Annual";
          Ctrl.reset();
        });
        describe('should be true', function () {
          it('for 0', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,0,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
          it('for 1', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,1,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
          it('for 2', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,2,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
          it('for 3', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,3,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
          it('for 4', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,4,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
          it('for 6', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,6,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
          it('for 7', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,7,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
          it('for 8', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,8,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
          it('for 9', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,9,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
          it('for 10', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,10,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(true);
          });
        });
        describe('should be false', function () {
          it('for 5', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,5,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(false);
          });
          it('for 11', function () {
            var data = {
              mode: 'month',
              date: new Date(2017,11,15)
            };
            var result = Ctrl.dateOptions.dateDisabled(data);
            expect(result).toBe(false);
          });
        });
      });
      describe('OpenCalendar', function () {
        it('should set datePickerOpenStatus[date] to true', function () {
          var date = "date";
          Ctrl.datePickerOpenStatus[date] = false;
          Ctrl.openCalendar(date);
          expect(Ctrl.datePickerOpenStatus[date]).toBe(true);
        });
      });
    });
  });
});
