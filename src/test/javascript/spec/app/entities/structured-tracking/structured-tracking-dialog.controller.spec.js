'use strict';

describe('Controller Tests', function() {
  describe('StructuredTracking Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockStructuredTracking,MockUiModalInstance, MockStructuredFinance;
    var MockEntity, MockPreviousState, MockStateParams, MockDeal, MockDealItem;
    var Timeout, MockInteractionService, MockStructuredFinanceObj;
    beforeEach(inject(
      function ($controller, $rootScope, $filter, $timeout) {     // Don't bother injecting a 'real' modal
        Timeout = $timeout;
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = {
          id: 123,
          structuredFinance: MockStructuredFinanceObj
        };
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockStructuredTracking = {
          update: function (structuredTracking, success, error) {
            success(structuredTracking);
          },
          save: function (structuredTracking, success, error) {
            error();
          }
        };
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        MockStateParams = {
          dealId: 123,
          financeId: 123
        };
        MockDealItem = {
          yearEndMonth: 3
        };
        MockDeal = {
          get: function (idObj, callback) {
            callback(MockDealItem);
          }
        };
        MockInteractionService = jasmine.createSpyObj('InteractionService',
          ['confirmElaborateDialog', 'confirmBasicDialog', 'alertDialog', 'alertParentScrollFalseDialog', 'showThenDialog', 'showFinallyDialog']);
        MockStructuredFinanceObj = {
          yearEndException: 'no',
          reportingFrequency: { name:'Monthly' },
          numberOfDays: 12,
          numberOfDaysYr: 12,
          dayType: "Calendar Days After",
          dayTypeYr: "Calendar Days After",
          period: 'Year End',
          periodYr: 'Year End'
        };
        MockStructuredFinance = {
          get: function (idObj, callback) {
            callback(MockStructuredFinanceObj);
          }
        };
        function calculatingDate (){ }
        Ctrl = $controller('StructuredTrackingDialogController', {
          $scope: scope,
          $state: state,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          $stateParams: MockStateParams,
          Deal: MockDeal,
          $filter:$filter,
          $timeout: $timeout,
          InteractionsService: MockInteractionService,
          StructuredTracking: MockStructuredTracking,
          StructuredFinance: MockStructuredFinance
        });
      }));

    describe('Initial state', function () {
      it('StructuredTrackingDialog Controller', function() {
        expect(Ctrl).toBeDefined();
      });
    });
    describe('Main Content Coverage', function () {
      describe('Save', function () {
        beforeEach(function () {
          Ctrl.structuredTracking.trackingStatus = 'Closed';
          Ctrl.structuredTracking.creationUser = null;
          Ctrl.lastUser = {};
        });
        it('should run update when id is set', function () {
          Ctrl.structuredTracking.id = 123;
          Ctrl.save(1);
          expect(Ctrl.structuredTracking.trackingStatus).toBe('Open');
          expect(Ctrl.structuredTracking.creationUser).toBeNull();
        });
        it('should run save when id is not set', function () {
          Ctrl.structuredTracking.id = null;
          Ctrl.save(0);
          expect(Ctrl.structuredTracking.trackingStatus).toBe('Closed');
          expect(Ctrl.structuredTracking.creationUser).not.toBeNull();
        });
      });
      describe('Clear', function () {
        it('should call UiModalInstance.dismiss', function(){
          Ctrl.clear();
          expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
        });
      });
      describe('OpenCalendar', function () {
        it('should set datePickerOpenStatus[date] to true', function () {
          var item = 'item';
          Ctrl.datePickerOpenStatus[item] = false;
          Ctrl.openCalendar(item);
          expect(Ctrl.datePickerOpenStatus[item]).toBe(true);
        });
      });
      describe('RemainInProcess', function () {
        it('should call the showFinallyDialog', function () {
          Ctrl.remainInProcess();
          expect(MockInteractionService.showFinallyDialog).toHaveBeenCalled();
        });
      });
      describe('SaveMessage', function () {
        it('should not run a dialog if tracking status is closed and param is 2', function () {
          Ctrl.structuredTracking.trackingStatus = 'Closed';
          Ctrl.saveMessage(2);
          expect(MockInteractionService.showThenDialog).not.toHaveBeenCalled();
        });
        it('should run a dialog if tracking status is not closed and param is 2', function () {
          Ctrl.structuredTracking.trackingStatus = 'Open';
          Ctrl.saveMessage(2);
          expect(MockInteractionService.showThenDialog).toHaveBeenCalled();
        });
        it('should not run a dialog if tracking status is closed and param is 1', function () {
          Ctrl.structuredTracking.trackingStatus = 'Closed';
          Ctrl.saveMessage(1);
          expect(MockInteractionService.showThenDialog).not.toHaveBeenCalled();
        });
        it('should run a dialog if tracking status is not closed and param is 1', function () {
          Ctrl.structuredTracking.trackingStatus = 'Open';
          Ctrl.saveMessage(1);
          expect(MockInteractionService.showThenDialog).toHaveBeenCalled();
        });
      });
      describe('getNextReportingPeriodDate', function () {
        it('should return end of month when date is 2017-06-01', function () {
          var date = "2017-06-01", frequency = "Monthly";
          var newDate = Ctrl.getNextReportingPeriodDate(date, frequency);
          expect(newDate.toString()).toBe(moment('2017-07-01').toString());
        });
        it('should return end of quarter when date is 2017-06-01', function () {
          var date = "2017-06-01", frequency = "Quarterly";
          var newDate = Ctrl.getNextReportingPeriodDate(date, frequency);
          expect(newDate.toString()).toBe(moment('2017-09-01').toString());
        });
        it('should return end of half-year when date is 2017-05-01', function () {
          var date = "2017-05-01", frequency = "Semi-Annual";
          var newDate = Ctrl.getNextReportingPeriodDate(date, frequency);
          expect(newDate.toString()).toBe(moment('2017-11-01').toString());
        });
        it('should return end of yearl when date is 2017-06-01', function () {
          var date = "2017-06-01", frequency = "Annual";
          var newDate = Ctrl.getNextReportingPeriodDate(date, frequency);
          expect(newDate.toString()).toBe(moment('2018-06-01').toString());
        });
      });
      describe('nextTrackingRecord', function () {
        it('should return a new TrackingRecord', function () {
          Ctrl.structuredTracking.structuredFinance.reportingFrequency = {name: "Quarterly"};
          var result = Ctrl.nextTrackingRecord({structuredFinance: {reportingFrequency: {name: ""}}});
          expect(result).toBeDefined();
          expect(result.id).toBeNull();
        });
      });
      describe('onSaveSuccess', function () {
        it('should update the current record first, then save the generated record', function () {
          Ctrl.structuredTracking.trackingStatus = 'Closed';
          Ctrl.structuredTracking.autoGenerateFlag = 1;
          Ctrl.save(0);
          expect(MockUiModalInstance.close).not.toHaveBeenCalled();
        });
      });
      describe('CalculateDate', function () {
        it('should leave CalcReportingDueDate null',  function () {
          Ctrl.calculateDate('2017-10-05', 'Yes', Ctrl.structuredTracking);
          expect(MockEntity.calcReportingDueDate).toBeNull();
          expect(MockEntity.calcReportingDueYex).not.toBeNull();
        });
      });
      describe('SetDateOptions', function () {
        it('should not set dateDisabled when ReportingFrequency is Monthly', function () {
          Ctrl.setDateOptions(Ctrl.dateOptions, 'Monthly');
          expect(Ctrl.dateOptions.dateDisabled).toBeUndefined();
        });
        it('should set dateDisabled when ReportingFrequency is Quarterly', function () {
          Ctrl.setDateOptions(Ctrl.dateOptions, 'Quarterly');
          expect(Ctrl.dateOptions.dateDisabled).toBeDefined();
        });
        it('should set dateDisabled when ReportingFrequency is Semi-Annual', function () {
          Ctrl.setDateOptions(Ctrl.dateOptions, 'Semi-Annual');
          expect(Ctrl.dateOptions.dateDisabled).toBeDefined();
        });
        it('should set dateDisabled when ReportingFrequency is Annual', function () {
          Ctrl.setDateOptions(Ctrl.dateOptions, 'Annual');
          expect(Ctrl.dateOptions.dateDisabled).toBeUndefined();
          expect(Ctrl.dateOptions.minMode).not.toBeNull();
        });
      });
    });
  });
});
