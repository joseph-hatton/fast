'use strict';

describe('Controller Tests', function () {

  describe('ContactsCompany Detail Controller', function () {
    var $scope, $rootScope,DeferFactory,defer;
    var MockEntity,MockTreatyTrackingCustomSvr,MockPrincipal, MockContactType, MockTreaty, MockContactCustomSrv, MockXrefTreatyCustomCompany, MockInteractionsService,
      MockStateParams,MockXrefTreatyCustomContact, MockTreatyTransactionSrv, MockCompanyCustomSrv, MockXrefTreatyCompany, MockXrefTreatyContact;
    var mockTreatyItem,MockContactSubtype, mockContactTypeItem, contactSearchResults, companySearchResults, xrefTreatyCompaniesQueryResults, treatyTransactionSrvQueryResults,MockIntro;
    var Ctrl;

    beforeEach(inject(function ($injector, $controller,_DeferFactory_) {
      DeferFactory = _DeferFactory_;
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      MockContactSubtype = jasmine.createSpy('MockContactSubtype',['query']);
      MockEntity = jasmine.createSpy('MockEntity');
      MockStateParams = jasmine.createSpy('$stateParams');
      mockContactTypeItem = [
        { name:'Broker' },
        { name:'Client' },
        { name:'Internal' }
      ];
      MockContactType = {
        query: function (callback) {
          callback(mockContactTypeItem);
        }
      };


      MockPrincipal = jasmine.createSpyObj('Principal', ['identity']);
      MockPrincipal.identity.and.returnValue(DeferFactory.generate().promise);

      MockXrefTreatyCustomContact = {
        treaties:jasmine.createSpyObj('treaties', ['query'])
      };
      MockXrefTreatyCustomContact.treaties.query.and.callFake(
        function(){
          return defer});

      MockXrefTreatyCustomCompany = {
        treaties:jasmine.createSpyObj('treaties', ['query'])
      };

      MockXrefTreatyCustomCompany = {
        treaties:jasmine.createSpyObj('treaties', ['query'])
      };


      MockTreatyTrackingCustomSvr = {
        count:jasmine.createSpyObj('count', ['get'])
      };
      MockContactSubtype.query= function(){return [{},{}]};
      defer= DeferFactory.generate();
      MockTreatyTrackingCustomSvr.count.get.and.callFake(
        function(){
          return defer});

      MockXrefTreatyCustomCompany.treaties.query.and.callFake(
        function(){
          return defer});
      mockTreatyItem =  { id:123, treatyStatus:{name:'Pending'} };
      MockTreaty = {
        get: function (obj, callback) {
          callback(mockTreatyItem);
        }
      };

      MockContactCustomSrv = {
        name: {
          query: function (obj, callback) {
            callback(contactSearchResults);
          }
        }
      };

      MockCompanyCustomSrv = {
        companies: {
          query: function (obj, callback) {
            callback(companySearchResults);
          }
        }
      };

      MockInteractionsService = jasmine.createSpyObj('InteractionsService', ['alertParentScrollFalseDialog', 'showThenDialog']);
      MockTreatyTransactionSrv = {
        companyCount: {
          query: function (obj, callback) {
            callback(treatyTransactionSrvQueryResults);
          }
        }
      };
      MockIntro =jasmine.createSpyObj('Intro', ['setupIntroSteps']);
      MockIntro.setupIntroSteps.and.returnValue(DeferFactory.generate().$promise);
      MockXrefTreatyCompany = jasmine.createSpyObj('XrefTreatyCompany', ['save', 'update', 'delete']);
      MockXrefTreatyContact = jasmine.createSpyObj('XrefTreatyContact', ['save', 'update', 'delete']);

      Ctrl = $controller("ContactsCompanyDetailController", {
        '$scope': $scope,
        '$rootScope': $rootScope,
        'entity': MockEntity,
        'ContactType': MockContactType,
        'Treaty': MockTreaty,
        'ContactCustomSrv': MockContactCustomSrv,
        'CompanyCustomSrv': MockCompanyCustomSrv,
        'InteractionsService': MockInteractionsService,
        '$stateParams': MockStateParams,
        'TreatyTransactionSrv': MockTreatyTransactionSrv,
        'XrefTreatyCompany': MockXrefTreatyCompany,
        'XrefTreatyContact': MockXrefTreatyContact,
        'XrefTreatyCustomCompany': MockXrefTreatyCustomCompany,
        'TreatyTrackingCustomSvr':MockTreatyTrackingCustomSvr,
        'XrefTreatyCustomContact':MockXrefTreatyCustomContact,
        'ContactSubtype':MockContactSubtype,
        'Principal': MockPrincipal,
        'Intro':MockIntro
      });
      xrefTreatyCompaniesQueryResults = [{companyType:'Ceding ', company:{isRgaCompany:1, name:'abc'}}];
      defer.resolve(xrefTreatyCompaniesQueryResults);
      $rootScope.$apply();
    }));


    describe('Init State', function () {
      it('Ctrl should be defined', function () {
        expect(Ctrl).toBeDefined();
      });
    });
    describe('Main Content', function () {
      describe('Treaty.Get', function () {
        it('should set treaty', function () {
          expect(Ctrl.treaty).toBeDefined();
        });
        describe('ContactType.query', function () {
          it('should set the default selected Contact Type as the Broker Option', function () {
            expect(Ctrl.selectedContactType.name).toBe('Broker');
          })
        });
        describe('searchContacts', function () {
          it('should return the matching records', function () {
            Ctrl.selectedContact = 'contact';
            contactSearchResults = [{},{}];
            var result = Ctrl.searchContacts();
            expect(result).toBe(contactSearchResults);
          });
        });
        describe('MockXrefTreatyCustomCompany.treaties.query', function () {
          it('should save the companies query in companies', function () {
            expect(Ctrl.xrefTreatyCompanies).toBe(xrefTreatyCompaniesQueryResults);
          });
        });
        describe('addCompany', function () {
          it('should show dialog when contact is already added', function () {
            Ctrl.companyTypeOption = { name:'132' };
            Ctrl.checkCompany = function () { return false; };
            $scope.addCompany();
            expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
          });
          it('should add item to list to be saved', function () {
            Ctrl.companyTypeOption = { name:'132' };
            Ctrl.saveCompanies = function () { };
            Ctrl.checkCompany = function () { return true; };
            $scope.addCompany();
            expect(Ctrl.xrefTreatyCompanies.length).toBe(2);
          });
        });
        describe('removeCompany', function () {
          it('should not delete if tied to a transaction', function () {
            var index = 0;
            Ctrl.treaty = { id:123 };
            Ctrl.xrefTreatyCompanies = [{id:1231,   company:{ id:123 } }];
            treatyTransactionSrvQueryResults = [{}];
            var company ={id:1231, company:{ id:123 } };
            $scope.removeCompany(company,index);
            expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
          });
          it('should delete if not tied to a transaction', function () {
            var index = 0;
            Ctrl.treaty = { id:123 };
            Ctrl.xrefTreatyCompanies = [{id:1231,  company:{ id:123 } }];
            Ctrl.saveCompanies = function () { };
            treatyTransactionSrvQueryResults = [];
            var company ={id:1231, company:{ id:123 } };
            $scope.removeCompany(company,index);
         //   expect(Ctrl.deleteXrefTreatyCompanies.length).toBe(1);
            expect(Ctrl.xrefTreatyCompanies.length).toBe(0);
          });
        });
        describe('searchCompanies', function () {
     //     it('should return the matching records', function () {
        //    Ctrl.selectedCompany = 'company';
         //   companySearchResults = [{},{}];
        //    var result = Ctrl.searchCompanies();
         //   expect(result).toBe(companySearchResults);
        //  });
        });
        describe('saveCompanies', function () {
          it('should save when treaty.id is null', function () {
            Ctrl.xrefTreatyCompanies = [{ id:null }];
            Ctrl.saveCompanies();
            expect(MockXrefTreatyCompany.save).toHaveBeenCalled();
          });
          it('should update when treaty.id is not null', function () {
            Ctrl.xrefTreatyCompanies = [{ id:123 }];
            Ctrl.saveCompanies();
            expect(MockXrefTreatyCompany.update).toHaveBeenCalled();
          });

        });
        describe('checkContactExist', function () {
          it('should return false if there is a match', function () {
            Ctrl.xrefTreatyContacts = [{ contact:{ id:123 } }];
            var contact = { id:123 };
            var result = Ctrl.checkContactExist(contact);
            expect(result).toBe(false);
          });
          it('should return true if there is not a match', function () {
            Ctrl.xrefTreatyContacts = [{ contact:{ id:321 } }];
            var contact = { id:123 };
            var result = Ctrl.checkContactExist(contact);
            expect(result).toBe(true);
          });
        });
        describe('checkCompany', function () {
          it('should return false if there is a match', function () {
            Ctrl.xrefTreatyCompanies = [{ company:{ id:123 } }];
            var company = { id:123 };
            var result = Ctrl.checkCompany(company);
            expect(result).toBe(false);
          });
          it('should return true if there is not a match', function () {
            Ctrl.xrefTreatyCompanies = [{ company:{ id:321 } }];
            var company = { id:123 };
            var result = Ctrl.checkCompany(company);
            expect(result).toBe(true);
          });
        });
        describe('AddContact', function () {
          it('should show dialog and not save if the contact already exists', function () {
            Ctrl.checkContactExist = function () { return false; };
            Ctrl.saveContacts = jasmine.createSpy('saveContacts');
            $scope.addContact();
            expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
            expect(Ctrl.saveContacts).not.toHaveBeenCalled();
          });
          it('should not save if the contact doesn\'t already exist', function () {
            Ctrl.checkContactExist = function () { return true; };
            Ctrl.saveContacts = jasmine.createSpy('saveContacts');
            $scope.addContact();
            expect(MockInteractionsService.showThenDialog).not.toHaveBeenCalled();
            expect(Ctrl.saveContacts).toHaveBeenCalled();
          });
        });
        describe('removeContact', function () {
          it('should add the item to the delete list and remove it from the main list, then save', function () {
            Ctrl.saveContacts = jasmine.createSpy('saveContacts');
            Ctrl.xrefTreatyContacts = [{}];
            $scope.removeContact(0);
            expect(Ctrl.deleteXrefTreatyContacts.length).toBe(1);
            expect(Ctrl.xrefTreatyContacts.length).toBe(0);
            expect(Ctrl.saveContacts).toHaveBeenCalled();
          });
        });
        describe('saveContacts', function () {
          it('should save when treaty.id is null', function () {
            Ctrl.xrefTreatyContacts = [{ id:null }];
            Ctrl.saveContacts();
            expect(MockXrefTreatyContact.save).toHaveBeenCalled();
          });
          it('should update when treaty.id is not null', function () {
            Ctrl.xrefTreatyContacts = [{ id:123 }];
            Ctrl.saveContacts();
            expect(MockXrefTreatyContact.update).toHaveBeenCalled();
          });
          it('should delete all elements in deleteXrefTreatyContacts', function () {
            Ctrl.deleteXrefTreatyContacts = [{}];
            Ctrl.saveContacts();
            expect(MockXrefTreatyContact.delete).toHaveBeenCalled();
          });
        });
      });
    });
  });
});

