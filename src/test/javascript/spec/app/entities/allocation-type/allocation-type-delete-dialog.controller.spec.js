'use strict';

describe('Controller Tests', function() {
  describe('Allocation Type Delete Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockAllocationType,MockUiModalInstance;
    var MockEntity, MockPreviousState;
    var success='Success';
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = jasmine.createSpyObj('MockEntity', ['get', 'update', 'save']);
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);


        MockAllocationType  =
          {
            get: function (id, callback) {
              callback(id);
            },
            update: function(id,success, error){ // for testing purposes, a flag of error will be set to trigger an 'error'
              if(!id.errorTestFlag)
                success(id);
              else
                error();
            },
            save: function (id, success, error) {
              if(!id.errorTestFlag)
                success(id);
              else
                error();
            },
            delete: function (id, success, error) {
              if(!id.errorTestFlag)
                success(id);
              else
                error();
            }
          };

        Ctrl = $controller('AllocationTypeDeleteController', {
          $scope: scope,
          $state: state,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          AllocationType: MockAllocationType
        });
      }));

    describe('Initial state', function () {
      it('AllocationTypeDialog Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('confirmDelete', function() {
        expect(Ctrl.confirmDelete).toBeDefined();
      });

      it('Clear', function() {
        expect(Ctrl.clear).toBeDefined();
      });
    });
  });
});
