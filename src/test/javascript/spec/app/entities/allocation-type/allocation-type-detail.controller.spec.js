'use strict';

describe('Controller Tests', function() {

    describe('AllocationType Management Detail Controller', function() {
      var $scope, $rootScope;
      var MockEntity, MockAllocationType;
      var Ctrl;

      beforeEach(inject(function ($controller, $injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockAllocationType = {
          get: function (id, callback) {
            callback(id);
          }
        }

        Ctrl = $controller("AllocationTypeDetailController", {
          '$scope': $scope,
          '$rootScope': $rootScope,
          'entity': MockEntity,
          'AllocationType': MockAllocationType
        });
      }));

      describe('Initial State', function () {
        it('AllocationType Detail Controller Should be Created', function () {
          expect(Ctrl).toBeDefined();
        });

        it('Load Should be Defined', function () {
          expect(Ctrl.load).toBeDefined();
        });
      });
    });
});
