'use strict';

describe('Controller Tests', function() {

  describe('AllocationType Controller', function() {
    var Ctrl;
    var scope;
    var MockAllocationType;
    var info = [{},{},{}];
// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        MockAllocationType =
          {
            query: function(callBack){
              callBack(info);
            }
          };
        Ctrl = $controller('AllocationTypeController', {
          $scope: scope, AllocationType:MockAllocationType
        });
      }));

    describe('Initial state', function () {
      it('AllocationType Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('Load All Test / call back ', function() {
        expect(Ctrl.loadAll).toBeDefined();
        Ctrl.loadAll();
        expect(Ctrl.allocationTypes.length).toBe(3);
      });


    });
  });
});
