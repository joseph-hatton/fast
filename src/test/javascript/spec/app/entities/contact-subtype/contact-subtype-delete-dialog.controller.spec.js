'use strict';

describe('Controller Tests', function() {
  describe('ContactSubtype Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockContactSubtype,MockUiModalInstance;
    var MockEntity, MockPreviousState, MockStateParams, MockTimeout, flag = true;
    var contactSubtype=1;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        //state =$rootScope.$state;
        MockEntity = jasmine.createSpy('MockEntity');
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        if (flag)
          MockStateParams = {  };
        else
          MockStateParams = { contactSubtypeId: "" };
        MockContactSubtype = {
          delete: function (success, error) {
            return contactSubtype;
          }
        };
        state = { go: function (str, obj) { } };
        MockTimeout = function(callback, amount){
          callback();
        };

        Ctrl = $controller('ContactSubtypeDeleteController', {
          $scope: scope,
          $state: state,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          $stateParams: MockStateParams,
          ContactSubtype: MockContactSubtype,
          $timeout: MockTimeout
        });
      }));

    describe('Initial state', function () {
      it('ContactSubtypeDialog Controller', function () {
        expect(Ctrl).toBeDefined();
      });
      it('clear ', function () {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });
      it('confirmDelete  ', function () {
        expect(Ctrl.confirmDelete).toBeDefined();
        Ctrl.confirmDelete(123);
      });
    });
  });
});
