'use strict';

describe('Controller Tests', function() {

    describe('ContactSubtype  Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockContactSubtype;
        var info =1;
          var Ctrl;

        beforeEach(inject(function($controller,$injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');

          MockContactSubtype  =
            {
              get: function(id,callBack){
                callBack(info);
              }
            };

          Ctrl = $controller('ContactSubtypeDetailController', {
            $scope: $scope,
            $rootScope: $rootScope,
            entity: MockEntity ,
            ContactSubtype: MockContactSubtype
          });

        }));


      describe('Initial State', function () {
        it('AgreementType Detail Controller Should be Created', function () {
          expect(Ctrl).toBeDefined();
        });

        it('Load Should be Defined', function () {
          expect(Ctrl.load).toBeDefined();
        });
      });
    });
});
