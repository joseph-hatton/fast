'use strict';

describe('Controller Tests', function() {

  describe('ContactSubtype Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockContactSubtype;
    var createController;
    var ContactSubtype;
    var Ctrl;
    var scope;
    var MockContactSubtypeSearch;
    var serviceResponse;
// Initialize the controller and a mock scope
    beforeEach(inject(

      function ($controller,$injector, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        var info = [{},{},{}];
        var info1 = [{}];
        MockContactSubtype =
          {
            query: function(callBack){
              callBack(info);
            }
          };
        //jasmine.createSpyObj('ContactSubtype',['query']);
        //    spyOn(MockContactSubtype,'query').and.callFake(function(callBack){
        //    callBack([{},{},{}]);
        //   });


        MockContactSubtypeSearch  =
          {
            query: function(searchQuery,callBackd){
              callBackd(info1);
            }
          };

        Ctrl = $controller('ContactSubtypeController', {
          $scope:scope ,ContactSubtype:MockContactSubtype,ContactSubtypeSearch:MockContactSubtypeSearch
        });


      }));

    describe('Initial state', function () {
      it('should instantiate the controller properly', function () {
        expect(Ctrl).not.toBeUndefined();
      });

      it('ContactSubtype Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('Load All Test', function() {
        expect(Ctrl.loadAll).toBeDefined();
        expect(Ctrl.contactSubtypes.length).toBe(3);
      });

      it('Search function  test', function() {
        expect(Ctrl.search).toBeDefined();
        expect(Ctrl.searchQuery).toBeUndefined();
        Ctrl.searchQuery='123';
        expect(Ctrl.searchQuery).toBeDefined();
        Ctrl.contactSubtypes=[];
        Ctrl.search();
        expect(Ctrl.contactSubtypes.length).toBe(1);
      });
      it('Search function Null search test', function() {
        expect(Ctrl.search).toBeDefined();
        expect(Ctrl.searchQuery).toBeUndefined();
        Ctrl.contactSubtypes=[];
        Ctrl.search();
        expect(Ctrl.contactSubtypes.length).toBe(3);
      });





    });
  });
});
