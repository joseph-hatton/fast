'use strict';

describe('Controller Tests', function() {
  describe('ContactSubtype Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockContactSubtype,MockUiModalInstance;
    var MockEntity, MockPreviousState, MockStateParams, MockTimeout, flag = true;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        //state =$rootScope.$state;
        MockEntity = jasmine.createSpy('MockEntity');
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        if (flag)
          MockStateParams = {  };
        else
          MockStateParams = { contactSubtypeId: "" };
        MockContactSubtype = {
          update: function (contactSubtype, success, error) {
            success(contactSubtype);
          },
          save: function (contactSubtype, success, error) { error(); },
          get: function (idObj, callback) { callback(idObj); }
        };
        state = { go: function (str, obj) { } };
        MockTimeout = function(callback, amount){
          callback();
        };

        Ctrl = $controller('ContactSubtypeDialogController', {
          $scope: scope,
          $state: state,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          $stateParams: MockStateParams,
          ContactSubtype: MockContactSubtype,
          $timeout: MockTimeout
        });
      }));

    describe('Initial state', function () {
      it('ContactSubtypeDialog Controller', function () {
        expect(Ctrl).toBeDefined();
      });
    });
  });
});
