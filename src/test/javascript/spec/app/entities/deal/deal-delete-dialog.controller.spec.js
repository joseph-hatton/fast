'use strict';

describe('Controller Tests', function() {
  describe('Deal Dialog Controller', function() {
    var Ctrl, $rootScope;
    var scope,state,MockDeal,MockUiModalInstance, MockInteractionsService;
    var MockEntity, MockPreviousState, EucQueryResult, DeferFactory, MockTreatySrv;
    var  MockDealObj = { yearEndMonth: 3,  effectiveDate: moment(), terminatedDate: moment(), maturityDate: moment()};
    beforeEach(inject(
      function ($controller, _$rootScope_, _DeferFactory_) {     // Don't bother injecting a 'real' modal
        DeferFactory = _DeferFactory_;
        $rootScope = _$rootScope_;
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = {};
        var stubDefer = DeferFactory.generate();
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockDeal = jasmine.createSpyObj('MockDeal', ['save', 'update','delete','get']);
        MockDeal.save.and.returnValue(stubDefer);
        MockDeal.update.and.returnValue(stubDefer);
        MockDeal.delete.and.returnValue(stubDefer);
        MockDeal = {
          get: function (id, callback) {
            callback(MockDealObj);
          }
        };
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        MockInteractionsService = jasmine.createSpyObj('InteractionsService', ['confirmBasicParentNoScrollDialog', 'alertParentScrollFalseDialog', 'showThenDialog']);
        MockTreatySrv = {
          deals: jasmine.createSpyObj('deals', ['query'])
        };


        Ctrl = $controller('DealDeleteController', {
          $scope: scope,
          $state: state, entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          Deal: MockDeal,
          InteractionsService: MockInteractionsService,
          TreatySrv: MockTreatySrv
        });
      }));

    describe('Initial state', function () {
      it('Delete deal Controller', function () {
        expect(Ctrl).toBeDefined();
      });
      it('clear ', function () {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });
      it('confirmDelete  ', function () {
        expect(Ctrl.confirmDelete).toBeDefined();
      });

    });

  });
});
