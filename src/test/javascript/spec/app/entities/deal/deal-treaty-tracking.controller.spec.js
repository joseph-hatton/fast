'use strict';

describe('Controller Tests', function() {

  describe('Deal Controller', function() {
    var Ctrl,MockDeal,MockTreatySrv,MockTreatyTrackingCustomSvr,deferDeals,deferTracking;
    var $rootScope, $scope,state,DeferFactory,MockEntity,deferDealsTreaty;

      beforeEach(inject(function ($injector, $controller, _DeferFactory_ ,$stateParams,DateUtils,$q) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        DeferFactory = _DeferFactory_;

        state = jasmine.createSpyObj('state', ['transitionTo']);
        deferDeals=DeferFactory.generate();
        deferTracking=DeferFactory.generate();
        deferDealsTreaty=DeferFactory.generate();
        MockEntity = jasmine.createSpy('MockEntity');
        MockTreatySrv = {
          deals:jasmine.createSpyObj('deals', ['query'])
        };
        MockTreatySrv.deals.query.and.returnValue(deferDealsTreaty);
        MockDeal = jasmine.createSpyObj('Deal', ['get']);
        MockDeal.get.and.returnValue(deferDeals);

        MockTreatyTrackingCustomSvr = {
          tracking:jasmine.createSpyObj('tracking', ['query'])
        };
        MockTreatyTrackingCustomSvr.tracking.query.and.returnValue(deferTracking);

        Ctrl = $controller('DealTreatyTrackingController', {
          $scope: $scope,
          $rootScope: $rootScope,
          $stateParams:{ id:123 ,dealId:222,page:1},
          Deal:MockDeal,
          entity: MockEntity,
          TreatySrv:MockTreatySrv,
          TreatyTrackingCustomSvr: MockTreatyTrackingCustomSvr,
          $q: $q
        });
      }));

    describe('Initial state', function () {
      it('DealTreatyTracking Controller', function() {
        expect(Ctrl).toBeDefined();
      });

    });
    describe('Deal', function () {
      it('Get', function() {
        var deal = {id:123};
        deferDeals.resolve(deal);
        $rootScope.$apply();
        expect(MockDeal.get).toHaveBeenCalled();
        expect(Ctrl.deal).toBeDefined();
        expect(Ctrl.deal.id).toBe(123);
      });
    });
    describe('TreatySrv', function () {
      it('TreatySrv', function() {
        Ctrl.deal.treaties=[];
        var deals = [{id:123},{id:123},{id:123}];
        deferDealsTreaty.resolve(deals);
        $rootScope.$apply();
        expect(MockTreatySrv.deals.query).toHaveBeenCalled();
        expect(Ctrl.deal.treaties.length).toBe(3);
      });
    });
    describe('TreatyTrackingCustomSvr', function () {
      it('TreatyTrackingCustomSvr', function() {
        var trackings =[{quarter:1 },{quarter:1 },{quarter:1 }];
        var deals = [{id:123},{id:123},{id:123}];
        deferDealsTreaty.resolve(deals);
        deferTracking.resolve(trackings);
        $rootScope.$apply();
        expect(MockTreatyTrackingCustomSvr.tracking.query).toHaveBeenCalled();

      });
    });
  });
});
