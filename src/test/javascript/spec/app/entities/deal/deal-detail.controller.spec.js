'use strict';

describe('Controller Tests', function() {

  describe('Deal Management Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockDeal, MockDealObj = { yearEndMonth: 3,  effectiveDate: moment(), terminatedDate: moment(), maturityDate: moment()};
    var Ctrl;

    beforeEach(inject(function ($controller, $injector, $filter) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      MockEntity = {};
      MockDeal = {
        get: function (id, callback) {
          callback(MockDealObj);
        }
      };

      Ctrl = $controller("DealDetailController", {
        $scope: $scope,
        $rootScope: $rootScope,
        entity: MockEntity,
        Deal: MockDeal,
        $filter: $filter
      });
    }));

    describe('Initial State', function () {
      it('Deal Detail Controller Should be Created', function () {
        expect(Ctrl).toBeDefined();
      });

      it('Load Should be Defined', function () {
        expect(Ctrl.load).toBeDefined();
      });
    });
    describe('Main Content', function () {
      describe('Deal.get', function () {
        it('should set the yearEndMonth name and date details', function () {
          expect(Ctrl.deal.yearEndMonth).toBe('March');
        });
      });
    });
  });
});
