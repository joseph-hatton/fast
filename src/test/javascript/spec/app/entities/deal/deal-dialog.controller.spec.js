'use strict';

describe('Controller Tests', function() {
  describe('Deal Dialog Controller', function() {
    var Ctrl, $rootScope;
    var scope,state,MockDeal,MockUiModalInstance, MockInteractionsService, MockDealCustomSrv, MockEucUse, MockProductLine, MockProductSubtype, MockCountry, MockGfsAssignee;
    var MockEntity,MockDealType, MockPreviousState, dealCustomSrvQueryResult, EucQueryResult, DeferFactory, searchDealsExactDefer, MockPrincipal;
    beforeEach(inject(
      function ($controller, _$rootScope_, _DeferFactory_) {     // Don't bother injecting a 'real' modal
        DeferFactory = _DeferFactory_;
        $rootScope = _$rootScope_;
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = {};
        var stubDefer = DeferFactory.generate();
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockDeal = jasmine.createSpyObj('MockDeal', ['save', 'update']);
        MockDeal.save.and.returnValue(stubDefer);
        MockDeal.update.and.returnValue(stubDefer);
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        MockInteractionsService = jasmine.createSpyObj('InteractionsService', ['confirmBasicParentNoScrollDialog', 'alertParentScrollFalseDialog', 'showThenDialog']);
        MockDealCustomSrv = {
          searchDealsExact: jasmine.createSpyObj('searchDealsExact', ['query'])
        };
        searchDealsExactDefer = DeferFactory.generate();
        MockDealCustomSrv.searchDealsExact.query.and.returnValue(searchDealsExactDefer);
        MockProductLine = jasmine.createSpyObj('ProductLine', ['query']);
        MockProductSubtype = jasmine.createSpyObj('ProductSubtype', ['query']);
        MockCountry = jasmine.createSpyObj('Country', ['query']);
        MockGfsAssignee = jasmine.createSpyObj('GfsAssignee', ['query']);
        MockDealType = jasmine.createSpyObj('DealType', ['query']);
        MockPrincipal = jasmine.createSpyObj('Principal', ['identity']);
        MockPrincipal.identity.and.returnValue(stubDefer.promise);

        EucQueryResult = [
          { entityType:'use' },
          { entityType:'significance' },
          { entityType:'complexity' },
          { entityType:'risk' }
        ];

        MockEucUse = {
          query: function (callback) {
            callback(EucQueryResult);
          }
        };
        Ctrl = $controller('DealDialogController', {
          $scope: scope,
          $state: state, entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          Deal: MockDeal,
          InteractionsService: MockInteractionsService,
          DealCustomSrv: MockDealCustomSrv,
          EucUse: MockEucUse,
          ProductLine: MockProductLine,
          ProductSubtype: MockProductSubtype,
          Country:MockCountry,
          GfsAssignee: MockGfsAssignee,
          Principal: MockPrincipal,
          DealType: MockDealType
        });
      }));

    describe('Initial state', function () {
      it('DealDialog Controller', function() {
        expect(Ctrl).toBeDefined();
      });

    });
    describe('Main Content', function () {
      describe('Save', function () {
        it('should show dialog when duplicate name on new creation and not save', function () {
          dealCustomSrvQueryResult = [{ id:123 }];
          Ctrl.save(null);
          searchDealsExactDefer.resolve(dealCustomSrvQueryResult);
          $rootScope.$apply();
          expect(MockDeal.save).not.toHaveBeenCalled();
          expect(MockDeal.update).not.toHaveBeenCalled();
          expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
        });
        it('should show dialog when new duplicate name on update and not update', function () {
          Ctrl.deal.id = 321;
          dealCustomSrvQueryResult = [{ id:123 }];
          Ctrl.save(null);
          searchDealsExactDefer.resolve(dealCustomSrvQueryResult);
          $rootScope.$apply();
          expect(MockDeal.save).not.toHaveBeenCalled();
          expect(MockDeal.update).not.toHaveBeenCalled();
          expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
        });
        it('should not show dialog when no duplicate name on new creation and save', function () {
          dealCustomSrvQueryResult = [];
          Ctrl.save(null);
          searchDealsExactDefer.resolve(dealCustomSrvQueryResult);
          $rootScope.$apply();
          expect(MockDeal.save).toHaveBeenCalled();
          expect(MockDeal.update).not.toHaveBeenCalled();
          expect(MockInteractionsService.showThenDialog).not.toHaveBeenCalled();
        });
        it('should not show dialog when no duplicate name on update and update', function () {
          Ctrl.deal.id = 123;
          dealCustomSrvQueryResult = [];
          Ctrl.save(null);
          searchDealsExactDefer.resolve(dealCustomSrvQueryResult);
          $rootScope.$apply();
          expect(MockDeal.save).not.toHaveBeenCalled();
          expect(MockDeal.update).toHaveBeenCalled();
          expect(MockInteractionsService.showThenDialog).not.toHaveBeenCalled();
        });
        it('should not show dialog when name isn\'t updated on update and update', function () {
          Ctrl.deal.id = 123;
          dealCustomSrvQueryResult = [{ id:123 }];
          Ctrl.save(null);
          searchDealsExactDefer.resolve(dealCustomSrvQueryResult);
          $rootScope.$apply();
          expect(MockDeal.save).not.toHaveBeenCalled();
          expect(MockDeal.update).toHaveBeenCalled();
          expect(MockInteractionsService.showThenDialog).not.toHaveBeenCalled();
        });
      });
      describe('eucRules', function () {
        it('should set risk to low when significance is low and complexity is low', function () {
          Ctrl.eucUsesRisk = [ { name:'Low' }, { name:'Medium' }, { name:'High' } ];
          Ctrl.deal.eucSignificance = { name: 'Low' };
          Ctrl.deal.eucComplexity = { name: 'Low' };
          Ctrl.eucRules();
          expect(Ctrl.deal.eucRisk.name).toBe('Low');
        });
        it('should set risk to low when significance is low and complexity is medium', function () {
          Ctrl.eucUsesRisk = [ { name:'Low' }, { name:'Medium' }, { name:'High' } ];
          Ctrl.deal.eucSignificance = { name: 'Low' };
          Ctrl.deal.eucComplexity = { name: 'Medium' };
          Ctrl.eucRules();
          expect(Ctrl.deal.eucRisk.name).toBe('Low');
        });
        it('should set risk to low when significance is low and complexity is high', function () {
          Ctrl.eucUsesRisk = [ { name:'Low' }, { name:'Medium' }, { name:'High' } ];
          Ctrl.deal.eucSignificance = { name: 'Low' };
          Ctrl.deal.eucComplexity = { name: 'High' };
          Ctrl.eucRules();
          expect(Ctrl.deal.eucRisk.name).toBe('Low');
        });
        it('should set risk to low when significance is moderate and complexity is low', function () {
          Ctrl.eucUsesRisk = [ { name:'Low' }, { name:'Medium' }, { name:'High' } ];
          Ctrl.deal.eucSignificance = { name: 'Moderate' };
          Ctrl.deal.eucComplexity = { name: 'Low' };
          Ctrl.eucRules();
          expect(Ctrl.deal.eucRisk.name).toBe('Low');
        });
        it('should set risk to medium when significance is moderate and complexity is medium', function () {
          Ctrl.eucUsesRisk = [ { name:'Low' }, { name:'Medium' }, { name:'High' } ];
          Ctrl.deal.eucSignificance = { name: 'Moderate' };
          Ctrl.deal.eucComplexity = { name: 'Medium' };
          Ctrl.eucRules();
          expect(Ctrl.deal.eucRisk.name).toBe('Medium');
        });
        it('should set risk to medium when significance is moderate and complexity is high', function () {
          Ctrl.eucUsesRisk = [ { name:'Low' }, { name:'Medium' }, { name:'High' } ];
          Ctrl.deal.eucSignificance = { name: 'Moderate' };
          Ctrl.deal.eucComplexity = { name: 'High' };
          Ctrl.eucRules();
          expect(Ctrl.deal.eucRisk.name).toBe('Medium');
        });
        it('should set risk to medium when significance is critical and complexity is low', function () {
          Ctrl.eucUsesRisk = [ { name:'Low' }, { name:'Medium' }, { name:'High' } ];
          Ctrl.deal.eucSignificance = { name: 'Critical' };
          Ctrl.deal.eucComplexity = { name: 'Low' };
          Ctrl.eucRules();
          expect(Ctrl.deal.eucRisk.name).toBe('Medium');
        });
        it('should set risk to high when significance is critical and complexity is medium', function () {
          Ctrl.eucUsesRisk = [ { name:'Low' }, { name:'Medium' }, { name:'High' } ];
          Ctrl.deal.eucSignificance = { name: 'Critical' };
          Ctrl.deal.eucComplexity = { name: 'Medium' };
          Ctrl.eucRules();
          expect(Ctrl.deal.eucRisk.name).toBe('High');
        });
        it('should set risk to high when significance is critical and complexity is high', function () {
          Ctrl.eucUsesRisk = [ { name:'Low' }, { name:'Medium' }, { name:'High' } ];
          Ctrl.deal.eucSignificance = { name: 'Critical' };
          Ctrl.deal.eucComplexity = { name: 'High' };
          Ctrl.eucRules();
          expect(Ctrl.deal.eucRisk.name).toBe('High');
        });
      });
      describe('xclear', function () {
        it('should show a dialog', function () {
          Ctrl.xclear();
          expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
        });
      });
      describe('clear', function () {
        it('should close the modal', function () {
          Ctrl.clear();
          expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
        });
      });
      describe('subTypeChange', function () {
        it('should set comments to be empty and set maturityDate to empty if productSubtype is not Structured', function () {
          Ctrl.deal.productSubtype = 'Not Structured';
          Ctrl.deal.comments = 'content';
          Ctrl.deal.maturityDate = 'content';
          Ctrl.subTypeChange();
          expect(Ctrl.deal.comments).toBe('');
          expect(Ctrl.deal.maturityDate).toBe('');
        });
        it('should set comments to be empty and not set maturityDate to empty if productSubtype is Structured', function () {
          Ctrl.deal.productSubtype = 'Structured';
          Ctrl.deal.comments = 'content';
          Ctrl.deal.maturityDate = 'content';
          Ctrl.subTypeChange();
          expect(Ctrl.deal.comments).toBe('');
          expect(Ctrl.deal.maturityDate).not.toBe('');
        });
      });
      describe('dealStatusChange', function () {
        it('should set terminatedDate to blank if active', function () {
          Ctrl.deal.dealStatus = 'Active';
          Ctrl.dealStatusChange();
          expect(Ctrl.deal.terminatedDate).toBe('');
        });
        it('should not set terminatedDate to blank if Terminated', function () {
          Ctrl.deal.terminatedDate = 'date';
          Ctrl.deal.dealStatus = 'Terminated';
          Ctrl.dealStatusChange();
          expect(Ctrl.deal.terminatedDate).not.toBe('');
        });
        it('should call dialog and close if attTerminate is 1', function () {
          Ctrl.attTerminate = 1;
          Ctrl.dealStatusChange();
          expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
        });
        it('should not call dialog if attTerminate is not 1', function () {
          Ctrl.attTerminate = 0;
          Ctrl.dealStatusChange();
          expect(MockInteractionsService.showThenDialog).not.toHaveBeenCalled();
        });
      });
      describe('EucUse.query', function () {
        it('should set the correct eucUse arrays', function () {
          expect(Ctrl.eucUses.length).toBe(1);
          expect(Ctrl.eucUsesR.length).toBe(1);
          expect(Ctrl.eucUsesC.length).toBe(1);
          expect(Ctrl.eucUsesRisk.length).toBe(1);
        });
      });
    });
  });
});
