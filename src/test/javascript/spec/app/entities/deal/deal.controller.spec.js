'use strict';

describe('Controller Tests', function() {

  describe('Deal Controller', function() {
    var Ctrl;
    var scope,state,paginationConstants,pagingParams, MockDealTrackingSrv, DealTrackingQueryResult;

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state = jasmine.createSpyObj('state', ['transitionTo']);
        pagingParams={ascending:true,page:1,predicate:"id",search:null,sort:"id,asc"};
        paginationConstants= {itemsPerPage: 10};
        MockDealTrackingSrv = {
          'base': {
            query: function (idObj, callback) {
              callback(DealTrackingQueryResult);
            }
          },
          'all':{
            get:function () {
              return {$promise:{then:function(){return {then:function(){}};}}}
            }
          }
        };
        Ctrl = $controller('DealController', {
          $scope: scope,
          $state: state,
          paginationConstants:paginationConstants,
          pagingParams:pagingParams,
          DealTrackingSrv: MockDealTrackingSrv
        });
      }));

    describe('Initial state', function () {
      it('Deal Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('Deal Load All Test', function() {
        expect(Ctrl.loadAll).toBeDefined();
      });

      it('Deal Search Test', function() {
        expect(Ctrl.search).toBeDefined();
      });
    });
    describe('Main Content', function () {
      describe('AdvanceFilter', function () {
        it('should set the appropiate items.', function () {
          Ctrl.filterDeal = true;
          Ctrl.productLines = [{}];
          Ctrl.advanceFilter();
          expect(Ctrl.productLineSelect).not.toBeNull();
        });
      });
      describe('filterDealFunc', function () {

      });
      describe('search', function () {
        it('should set current search to the query', function () {
          var query = 'query';
          Ctrl.search(query);
          expect(Ctrl.currentSearch).toBe(query.toUpperCase());
        });
        it('should not set current search to the query when the query is null', function () {
          Ctrl.currentSearch = 'oldQuery';
          var query = null;
          Ctrl.search(query);
          expect(Ctrl.currentSearch).toBe(null);
        });
      });
      describe('loadPage', function () {
        it('should set page to the current page', function () {
          var page = 123;
          Ctrl.loadPage(page);
          expect(Ctrl.page).toBe(page);
        });
      });
      describe('clear', function () {
        it('should default everything', function () {
          Ctrl.currentSearch = 'oldQuery';
          Ctrl.originalDealsListCopy = [];
          Ctrl.clear();
          expect(Ctrl.currentSearch).toBeNull();
        });
      });
      describe('AdvancedFilterNg', function () {
        var value;
        beforeEach(function () {
          value = {
            id: 123,
            country: {id:123},
            productSubtype: {id:123},
            gfsAssignee: {id:123},
            usStatRisk: 'Yes',
            dealStatus: 'Active'
          };
        });
        it('should return true always when filterdeal is off', function () {
          Ctrl.filterDeal = false;
          var result = Ctrl.filterFunc(value);
          expect(result).toBe(true);
        });
        it('should return true when all items match', function () {
          Ctrl.dealSelect = [{id:123}];
          Ctrl.countrySelect = [{id:123}];
          Ctrl.productSubtypeSelect = [{id:123}];
          Ctrl.GFSAssigneeSelect = [{id:123}];
          Ctrl.usStatSelect = ['Yes'];
          Ctrl.dealStatusSelect = ['Active'];
          Ctrl.filterDeal = true;
          var result = Ctrl.filterFunc(value);
          expect(result).toBe(true);
        });
        it('should return false when dealStatus doesn\'t match', function () {
          Ctrl.dealSelect = [{id:123}];
          Ctrl.countrySelect = [{id:123}];
          Ctrl.productSubtypeSelect = [{id:123}];
          Ctrl.GFSAssigneeSelect = [{id:123}];
          Ctrl.usStatSelect = ['Yes'];
          Ctrl.dealStatusSelect = ['Terminated'];
          Ctrl.filterDeal = true;
          var result = Ctrl.filterFunc(value);
          expect(result).toBe(false);
        });
        it('should return false when usStatSelect doesn\'t match', function () {
          Ctrl.dealSelect = [{id:123}];
          Ctrl.countrySelect = [{id:123}];
          Ctrl.productSubtypeSelect = [{id:123}];
          Ctrl.GFSAssigneeSelect = [{id:123}];
          Ctrl.usStatSelect = ['No'];
          Ctrl.dealStatusSelect = ['Active'];
          Ctrl.filterDeal = true;
          var result = Ctrl.filterFunc(value);
          expect(result).toBe(false);
        });
        it('should return false when GFSAssignee doesn\'t match', function () {
          Ctrl.dealSelect = [{id:123}];
          Ctrl.countrySelect = [{id:123}];
          Ctrl.productSubtypeSelect = [{id:123}];
          Ctrl.GFSAssigneeSelect = [{id:321}];
          Ctrl.usStatSelect = ['Yes'];
          Ctrl.dealStatusSelect = ['Active'];
          Ctrl.filterDeal = true;
          var result = Ctrl.filterFunc(value);
          expect(result).toBe(false);
        });
        it('should return false when productSubtype doesn\'t match', function () {
          Ctrl.dealSelect = [{id:123}];
          Ctrl.countrySelect = [{id:123}];
          Ctrl.productSubtypeSelect = [{id:321}];
          Ctrl.GFSAssigneeSelect = [{id:123}];
          Ctrl.usStatSelect = ['Yes'];
          Ctrl.dealStatusSelect = ['Active'];
          Ctrl.filterDeal = true;
          var result = Ctrl.filterFunc(value);
          expect(result).toBe(false);
        });
        it('should return false when country doesn\'t match', function () {
          Ctrl.dealSelect = [{id:123}];
          Ctrl.countrySelect = [{id:321}];
          Ctrl.productSubtypeSelect = [{id:123}];
          Ctrl.GFSAssigneeSelect = [{id:123}];
          Ctrl.usStatSelect = ['Yes'];
          Ctrl.dealStatusSelect = ['Active'];
          Ctrl.filterDeal = true;
          var result = Ctrl.filterFunc(value);
          expect(result).toBe(false);
        });
        it('should return false when dealSelect doesn\'t match', function () {
          Ctrl.dealSelect = [{id:321}];
          Ctrl.countrySelect = [{id:123}];
          Ctrl.productSubtypeSelect = [{id:123}];
          Ctrl.GFSAssigneeSelect = [{id:123}];
          Ctrl.usStatSelect = ['Yes'];
          Ctrl.dealStatusSelect = ['Active'];
          Ctrl.filterDeal = true;
          var result = Ctrl.filterFunc(value);
          expect(result).toBe(false);
        });
      });
      // describe('On Success', function () {
      //   it('should update the trackingItem quarters', function () {
      //     DealTrackingQueryResult = [
      //       { dealTrackingStatus: 'Open', reportingPeriodEndDate: '2017-09-01' },
      //       { dealTrackingStatus: 'Open', reportingPeriodEndDate: '2017-09-01' },
      //       { dealTrackingStatus: 'Open', reportingPeriodEndDate: '2017-09-01' }
      //     ];
      //     Ctrl.dealsList = [
      //       { isActive:1 }
      //     ];
      //     Ctrl.filterDeal = true;
      //     Ctrl.onSuccess(DealTrackingQueryResult, null);
      //     expect(Ctrl.deals[0].trackingItem.length).toBe(DealTrackingQueryResult.length);
      //     for(var i = 0; i < Ctrl.deals[0].trackingItem.length; i++)
      //       expect(Ctrl.deals[0].trackingItem[i].quarter).toBe('2017 Q3');
      //   });
      // });
    });
  });
});
