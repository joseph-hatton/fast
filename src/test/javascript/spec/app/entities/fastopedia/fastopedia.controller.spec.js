'use strict';

describe('Controller Tests', function() {

    describe('Fastopedia Controller', function() {
        var $scope, $rootScope;
        var MockFastopedia, DeferFactory, MockInteractionsService, MockPrincipal;
        var Ctrl;
        beforeEach(inject(
        function ($controller, _$rootScope_, _DeferFactory_) {
            $rootScope = _$rootScope_;
            $scope = $rootScope.$new();
            DeferFactory = _DeferFactory_;

            MockFastopedia = jasmine.createSpyObj('Fastopedia', ['loadAll', 'update', 'save', 'search']);
            MockFastopedia.update.and.callFake(function(){
                return DeferFactory.generate();
            });
            MockFastopedia.save.and.callFake(function(){
                return DeferFactory.generate();
            });
            MockFastopedia.loadAll.and.callFake(function(){
                return DeferFactory.generate();
            });
            MockFastopedia.search.and.callFake(function(){
                return DeferFactory.generate();
            });
            MockInteractionsService = jasmine.createSpyObj('InteractionsService', ['confirmBasicParentNoScrollDialog', 'showThenDialog']);
            MockPrincipal = jasmine.createSpyObj('Principal', ['isAdmin']);
            MockPrincipal.isAdmin.and.returnValue(DeferFactory.generate().promise);
            
            Ctrl = $controller('FastopediaController', {
                $scope: $scope,
                Fastopedia: MockFastopedia,
                InteractionsService: MockInteractionsService,
                Principal: MockPrincipal
            });
        }));

        describe('Initial state', function () {
            it('should instantiate the controller properly', function () {
                expect(Ctrl).not.toBeUndefined();
            });
        });

        describe('Main Content', function(){
            describe('loadAll', function(){
                it('should load all and sort by id desc', function(){
                    var items = [
                        {id:1},
                        {id:2},
                        {id:3}
                    ];
                    Ctrl.buildParentChildrenRelationships = jasmine.createSpy('buildParentChildrenRelationships');
                    Ctrl.order = jasmine.createSpy('order');
                    var defer = DeferFactory.generate();
                    MockFastopedia.loadAll.and.callFake(function(){
                        return defer;
                    });

                    Ctrl.loadAll();
                    defer.resolve(items);
                    $rootScope.$apply();

                    expect(Ctrl.documents.length).toBe(3);
                    expect(Ctrl.buildParentChildrenRelationships).toHaveBeenCalled();
                    expect(Ctrl.order).toHaveBeenCalled();
                });
            });
            describe('saveContent', function(){
                it('should save the item if id is null', function(){
                    var item = {};

                    Ctrl.saveContent(item);

                    expect(MockFastopedia.save).toHaveBeenCalled();
                });
                it('should update the item if id is not null', function(){
                    var item = {
                        id:123
                    };

                    Ctrl.saveContent(item);

                    expect(MockFastopedia.update).toHaveBeenCalled();
                });
            });
            describe('Edit', function(){
                it('should set editing to true', function(){
                    var item = {};
                    
                    Ctrl.edit(item);
    
                    expect(item.editing).toBe(true);
                });
            });
            describe('DoneEditing', function(){
                it('should set editing to false', function(){
                    var item = {};
                    
                    Ctrl.doneEditing(item);
    
                    expect(item.editing).toBe(false);
                });
            });
            describe('createNewItem', function(){
                it('should add a new parent item when parentId isn\'t set', function(){
                    Ctrl.documentsMaster = [];
                    Ctrl.documents = [];
                    var defer = DeferFactory.generate();
                    Ctrl.saveContent = jasmine.createSpy('saveContent');
                    Ctrl.saveContent.and.callFake(function(){
                        return defer.promise;
                    });

                    Ctrl.createNewItem();
                    defer.resolve();
                    $rootScope.$apply();

                    expect(Ctrl.documents.length).toBe(1);
                    expect(Ctrl.documentsMaster.length).toBe(1);
                });
                it('should add a new child item when parentId is set to a parentId', function(){
                    var item = {id:1, subdocuments:[]};
                    Ctrl.documentsMaster = [item];
                    Ctrl.documents = [item];
                    var defer = DeferFactory.generate();
                    Ctrl.saveContent = jasmine.createSpy('saveContent');
                    Ctrl.saveContent.and.callFake(function(){
                        return defer.promise;
                    });

                    Ctrl.createNewItem(1);
                    defer.resolve();
                    $rootScope.$apply();

                    expect(Ctrl.documents.length).toBe(1);
                    expect(Ctrl.documentsMaster.length).toBe(2);

                    expect(Ctrl.documents[0].subdocuments.length).toBe(1);
                });
            });
            describe('remove', function(){
                it('should do nothing if there are subdocuments', function(){
                    var arr = [{subdocuments:[{}]}];

                    Ctrl.remove(arr, 0);

                    expect(MockInteractionsService.showThenDialog).not.toHaveBeenCalled();
                });
                it('should do nothing if the user selects no', function(){
                    var arr = [{subdocuments:[]}];

                    Ctrl.remove(arr, 0);

                    expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
                    expect(arr[0].isActive).toBeUndefined();
                });
                it('should set isActive to 0 and save if the user selects yes', function(){
                    var arr = [{subdocuments:[]}];
                    MockInteractionsService.showThenDialog.and.callFake(function(dialog, yes){
                        yes();
                    });
                    Ctrl.saveAll = jasmine.createSpy('saveAll');
                    Ctrl.saveAll.and.callFake(function(){
                        return DeferFactory.generate().promise;
                    });

                    Ctrl.remove(arr, 0);

                    expect(arr[0].isActive).toBe(0);
                    expect(Ctrl.saveAll).toHaveBeenCalled();
                });
                it('should remove the item from the gui if the user selects yes after saving', function(){
                    var arr = [{subdocuments:[]}];
                    MockInteractionsService.showThenDialog.and.callFake(function(dialog, yes){
                        yes();
                    });
                    var defer = DeferFactory.generate();
                    Ctrl.saveAll = jasmine.createSpy('saveAll');
                    Ctrl.saveAll.and.callFake(function(){
                        return defer.promise;
                    });

                    Ctrl.remove(arr, 0);
                    defer.resolve();
                    $rootScope.$apply();

                    expect(arr.length).toBe(0);
                });
            });
            describe('buildParentChildrenRelationships', function(){
                it('should put the correct children in the subdocuments for the parents', function(){
                    Ctrl.documents = [
                        { id:1, parentId:0 },
                        { id:2, parentId:1 },
                        { id:3, parentId:2 }
                    ];
                    
                    Ctrl.buildParentChildrenRelationships();

                    expect(Ctrl.documents[0].subdocuments.length).toBe(1);
                    expect(Ctrl.documents[0].subdocuments[0].id).toBe(2);
                    expect(Ctrl.documents[0].subdocuments[0].subdocuments.length).toBe(1);
                    expect(Ctrl.documents[0].subdocuments[0].subdocuments[0].id).toBe(3);
                });
            });
            describe('order', function(){
                it('should filter out children from vm.documents', function(){
                    Ctrl.documentsMaster = Ctrl.documents = [
                        { id:1, parentId:0 },
                        { id:2, parentId:1 },
                        { id:3, parentId:2 }
                    ];
                    
                    Ctrl.order();

                    expect(Ctrl.documents.length).toBe(1);
                });
                it('should sort all items based on their "order" for the subdocuments', function(){
                    Ctrl.documentsMaster = Ctrl.documents = [
                        { 
                            id:1, 
                            parentId:0,
                            subdocuments: [
                                { id:2, parentId:1, order:1 },
                                { id:3, parentId:1, order:0 }
                            ] 
                        }
                    ];
                    
                    Ctrl.order();

                    expect(Ctrl.documents[0].subdocuments[0].id).toBe(3);
                    expect(Ctrl.documents[0].subdocuments[1].id).toBe(2);
                });
                it('should sort all items based on their "order" for the documents', function(){
                    Ctrl.documentsMaster = Ctrl.documents = [
                        { id:1, parentId:0, order:1 },
                        { id:2, parentId:0, order:2 },
                        { id:3, parentId:0, order:0 }
                    ];
                    
                    Ctrl.order();

                    expect(Ctrl.documents[0].id).toBe(3);
                    expect(Ctrl.documents[1].id).toBe(1);
                    expect(Ctrl.documents[2].id).toBe(2);
                });
            });
            describe('moveUp', function(){
                it('should move the item up in the array', function(){
                    var list = [
                        { order:1, id: 1 },
                        { order:2, id: 2 },
                        { order:3, id: 3 },
                    ];

                    Ctrl.moveUp(list, 1);

                    expect(list[0].id).toBe(2);
                    expect(list[1].id).toBe(1);
                    expect(list[2].id).toBe(3);

                    expect(list[0].order).toBe(1);
                    expect(list[1].order).toBe(2);
                    expect(list[2].order).toBe(3);
                });
            });
            describe('moveDown', function(){
                it('should move the item down in the array', function(){
                    var list = [
                        { order:1, id: 1 },
                        { order:2, id: 2 },
                        { order:3, id: 3 },
                    ];
                    
                    Ctrl.moveDown(list, 1);

                    expect(list[0].id).toBe(1);
                    expect(list[1].id).toBe(3);
                    expect(list[2].id).toBe(2);

                    expect(list[0].order).toBe(1);
                    expect(list[1].order).toBe(2);
                    expect(list[2].order).toBe(3);
                });
            });
            it('toggleOpen should set item\'s open to the opposite value', function(){
                var item = {};
                expect(item.open).toBe(undefined);
                Ctrl.toggleOpen(item);
                expect(item.open).toBe(true);
                Ctrl.toggleOpen(item);
                expect(item.open).toBe(false);
            });
            it('search should call vm.fuse.search and set searching to true', function(){                
                Ctrl.search();

                expect(MockFastopedia.search).toHaveBeenCalled();
                expect(Ctrl.searching).toBe(true);
            });
            it('clearSearch should set searching to false and clear the searchQuery', function(){
                Ctrl.searchQuery = "query";
                
                Ctrl.clearSearch();

                expect(Ctrl.searching).toBe(false);
                expect(Ctrl.searchQuery).toBe("");
            });
            describe('enterOnSearch', function(){
                beforeEach(function(){
                    Ctrl.search = jasmine.createSpy('search');
                });
                it('should call search when the key pressed is 13 (Enter)', function(){
                    var e = {
                        which: 13
                    };
    
                    Ctrl.enterOnSearch(e);
    
                    expect(Ctrl.search).toHaveBeenCalled();
                });
                it('should not call search when the key pressed is not 13 (Enter)', function(){
                    var e = {
                        which: 10
                    };
    
                    Ctrl.enterOnSearch(e);
    
                    expect(Ctrl.search).not.toHaveBeenCalled();
                });
            });
        });
    });
});
