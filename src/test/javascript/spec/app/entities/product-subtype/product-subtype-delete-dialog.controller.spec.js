'use strict';

describe('Controller Tests', function() {
  describe('ProductSubtypeDeleteController', function() {
    var Ctrl;
    var scope,state,MockProductSubtype,MockUiModalInstance;
    var MockEntity, MockPreviousState;
    var success='Success';
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = jasmine.createSpyObj('MockEntity', ['get', 'update', 'save']);
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);


        MockProductSubtype  =
          {
            delete: function (id, callback) {
              callback(id);
            }
          };

        Ctrl = $controller('ProductSubtypeDeleteController', {
          $scope: scope,
          $state: state,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          ProductSubtype: MockProductSubtype
        });
      }));

    describe('Initial state', function () {
      it('ProductSubtypeDialog Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('clear', function() {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });

      it('confirmDelete', function() {
        expect(Ctrl.confirmDelete).toBeDefined();
      });
    });
  });
});
