'use strict';

describe('Controller Tests', function() {

  describe('ProductSubtype Controller', function() {

    var Ctrl;
    var scope;
    var MockProductSubtype;
    var MockProductSubtypeSearch;
    var loadAllResult = [{},{},{}], searchResult = [{}];

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();

        MockProductSubtype = {
          query: function (callback) {
            callback(loadAllResult);
          }
        };

        MockProductSubtypeSearch = {
          query: function (search, callback) {
            callback(searchResult);
          }
        };

        Ctrl = $controller('ProductSubtypeController', {
          $scope: scope,
          ProductSubtype: MockProductSubtype,
          ProductSubtypeSearch: MockProductSubtypeSearch
        });
      }));

    describe('Initial state', function () {
      it('ProductSubtype Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('Load All Test', function() {
        expect(Ctrl.loadAll).toBeDefined();
      });

      it('Search Test', function() {
        expect(Ctrl.search).toBeDefined();
      });
    });

    describe('Main Content Cover', function () {
      it('Load All Test', function () {
        expect(Ctrl.productSubtypes).toBe(loadAllResult);
      });

      it('Search No Query Test', function () {
        Ctrl.searchQuery = null;
        Ctrl.search();
        expect(Ctrl.productSubtypes).toBe(loadAllResult);
      });

      it('Search Query Test', function () {
        Ctrl.searchQuery = 1;
        Ctrl.search();
        expect(Ctrl.productSubtypes).toBe(searchResult);
      });
    });
  });
});
