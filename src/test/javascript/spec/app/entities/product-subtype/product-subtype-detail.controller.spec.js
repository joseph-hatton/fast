'use strict';

describe('Controller Tests', function() {

  describe('ProductSubtype Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockProductSubtype;
    var Ctrl;

    beforeEach(inject(function($controller,$injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      MockEntity = jasmine.createSpy('MockEntity');
      MockProductSubtype = {
        get: function (id, callback) {
          callback(id);
        }
      };

      Ctrl = $controller("ProductSubtypeDetailController", {
        '$scope': $scope,
        '$rootScope': $rootScope,
        'entity': MockEntity ,
        'ProductSubtype': MockProductSubtype
      });

    }));

    describe('Initial State', function () {
      it('ProductSubtype Detail Controller Should be Created', function () {
        expect(Ctrl).toBeDefined();
      });

      it('Load Should be Defined', function () {
        expect(Ctrl.load).toBeDefined();
      });
    });
  });

});
