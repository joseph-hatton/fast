'use strict';

describe('Controller Tests', function() {
  describe('ProductLine Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockProductLine,MockUiModalInstance;
    var MockEntity, MockPreviousState;
    var success='Success';
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = jasmine.createSpyObj('MockEntity', ['get', 'update', 'save']);
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);


        MockProductLine  =
          { delete: function (id, callback) {
            callback(id);
          }
          };

        Ctrl = $controller('ProductLineDeleteController', {
          $scope: scope,
          $state: state,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          ProductLine: MockProductLine
        });
      }));

    describe('Initial state', function () {
      it('ProductLineDialog Delete Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('confirmDelete ', function() {
        expect(Ctrl.confirmDelete).toBeDefined();
        Ctrl.confirmDelete(123);
      });

      it('Clear', function() {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });
    });
  });
});
