'use strict';

describe('Controller Tests', function() {

  describe('ProductLine Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockProductLine;
    var Ctrl;

    beforeEach(inject(function($controller,$injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      MockEntity = jasmine.createSpy('MockEntity');
      MockProductLine = {
        get: function (id, callback) {
          callback(id);
        }
      };

      Ctrl = $controller("ProductLineDetailController", {
        '$scope': $scope,
        '$rootScope': $rootScope,
        'entity': MockEntity ,
        'ProductLine': MockProductLine
      });

    }));

    describe('Initial State', function () {
      it('ProductLine Detail Controller Should be Created', function () {
        expect(Ctrl).toBeDefined();
      });

      it('Load Should be Defined', function () {
        expect(Ctrl.load).toBeDefined();
      });
    });
  });

});
