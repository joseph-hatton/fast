'use strict';

describe('Controller Tests', function() {

  describe('ProductLine Controller', function() {

    var Ctrl;
    var scope;
    var MockProductLine;
    var MockProductLineSearch;
    var loadAllResult = [{},{},{}], searchResult = [{}];

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();

        MockProductLine = {
          query: function (callback) {
            callback(loadAllResult);
          }
        };

        MockProductLineSearch = {
          query: function (search, callback) {
            callback(searchResult);
          }
        };

        Ctrl = $controller('ProductLineController', {
          $scope: scope,
          ProductLine: MockProductLine,
          ProductLineSearch: MockProductLineSearch
        });
      }));

    describe('Initial state', function () {
      it('ProductLine Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('Load All Test', function() {
        expect(Ctrl.loadAll).toBeDefined();
      });

      it('Search Test', function() {
        expect(Ctrl.search).toBeDefined();
      });
    });

    describe('Main Content Cover', function () {
      it('Load All Test', function () {
        expect(Ctrl.productLines).toBe(loadAllResult);
      });

      it('Search No Query Test', function () {
        Ctrl.searchQuery = null;
        Ctrl.search();
        expect(Ctrl.productLines).toBe(loadAllResult);
      });

      it('Search Query Test', function () {
        Ctrl.searchQuery = 1;
        Ctrl.search();
        expect(Ctrl.productLines).toBe(searchResult);
      });
    });
  });
});
