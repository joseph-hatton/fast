'use strict';

describe('Controller Tests', function() {

  describe('RiskManagement Controller', function() {
    var Ctrl;
    var scope,state,paginationConstants,pagingParams;
    var MockTreatySrv;
    var MockDealList = [{},{},{}];

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state = jasmine.createSpyObj('state', ['transitionTo']);
        pagingParams={ascending:true,page:1,predicate:"id",search:null,sort:"id,asc"};
        paginationConstants= {itemsPerPage: 10};
        MockTreatySrv = {
          nopages: {
            query: function (callback) {
              callback(MockDealList);
            }
          }
        };
        Ctrl = $controller('RiskManagementController', {
          $scope: scope,
          $state: state,
          paginationConstants:paginationConstants,
          pagingParams:pagingParams,
          TreatySrv: MockTreatySrv
        });
      }));

    describe('Initial state', function () {
      it('RiskManagement Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('RiskManagement Load All Test', function() {
        expect(Ctrl.loadAll).toBeDefined();
      });

      it('RiskManagement Search Test', function() {
        expect(Ctrl.search).toBeDefined();
      });
    });
    describe('Main Content Coverage', function () {
      describe('TreatySrv.nopages', function () {
        it('should add query results to dealsList', function () {
          expect(Ctrl.dealsList.length).toBe(MockDealList.length);
        });
      });
      describe('OpenCalendar', function () {
        it('should set datePickerOpenStatus[date] to true', function () {
          var item = 'item';
          Ctrl.datePickerOpenStatus[item] = false;
          Ctrl.openCalendar(item);
          expect(Ctrl.datePickerOpenStatus[item]).toBe(true);
        });
      });
      describe('DisplayQuarterly', function () {
        it('should respond true when data.mode is month and data.date.getMonth is 0', function () {
          var data = {
            mode: 'month',
            date: { getMonth: function () { return 0; } }
          };

          var result = Ctrl.dateOptions.dateDisabled(data);

          expect(result).toBe(true);
        });

        it('should respond false when data.mode is month and data.date.getMonth is 2', function () {
          var data = {
            mode: 'month',
            date: { getMonth: function () { return 2; } }
          };

          var result = Ctrl.dateOptions.dateDisabled(data);

          expect(result).toBe(false);
        });
      });
      describe('GetQuarter', function () {
        it('should set the correct quarter', function () {
          var date = '2017-03';
          Ctrl.getQuarter(date);
          expect(Ctrl.quarter).toBe(1);
          date = '2017-09';
          Ctrl.getQuarter(date);
          expect(Ctrl.quarter).toBe(3);
        });
      });
      describe('LoadPage', function () {
        it('should set the page and run transition', function () {
          var page = "page";
          Ctrl.page = null;
          Ctrl.loadPage(page);
          expect(Ctrl.page).toBe(page);
          expect(state.transitionTo).toHaveBeenCalled();
        });
      });
      describe('Search', function () {
        it('should clear and return if searchQuery is falsy', function () {
          Ctrl.currentSearch = "search";
          Ctrl.search(null);
          expect(Ctrl.currentSearch).toBeNull();
        });
        it('should update the screen with the results is truthy', function () {
          var query = "query";
          Ctrl.currentSearch = null;
          Ctrl.search(query);
          expect(Ctrl.currentSearch).not.toBeNull();
          expect(Ctrl.currentSearch).toBe(query);
        });
      });
    });
  });
});
