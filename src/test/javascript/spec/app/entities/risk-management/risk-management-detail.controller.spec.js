'use strict';

describe('Controller Tests', function () {

  describe('RiskManagement Management Detail Controller', function () {
    var $scope, $rootScope;
    var MockEntity, MockRiskManagement, MockInteractionsService, MockRiskCustomSrv, MockRiskItem;
    var Ctrl;

    beforeEach(inject(function ($controller, $injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockRiskManagement = {
          update: function (obj, callback) {
            callback(obj);
          }
        };
        MockInteractionsService = jasmine.createSpyObj('InteractionsService', ['confirmBasicParentNoScrollDialog', 'showThenDialog']);
        MockRiskCustomSrv = {
          getByDeal: {
            query: function (queryObj, callback) {
              callback(MockRiskItem);
            }
          }
        };
        Ctrl = $controller("RiskManagementDetailController", {
          '$scope': $scope,
          '$rootScope': $rootScope,
          'entity': MockEntity,
          'RiskManagement': MockRiskManagement,
          'InteractionsService': MockInteractionsService,
          'RiskCustomSrv': MockRiskCustomSrv
        });
    }));


    describe('Initial State', function () {
      it('RiskManagement Controller', function () {
        expect(Ctrl).toBeDefined();
      });

    });
    describe('Main Content Coverage', function () {
      describe('OpenCalendar', function () {
        it('should set datePickerOpenStatus[date] to true', function () {
          var item = 'item';
          Ctrl.datePickerOpenStatus[item] = false;
          Ctrl.openCalendar(item);
          expect(Ctrl.datePickerOpenStatus[item]).toBe(true);
        });
      });
      describe('SaveValue', function () {
        it('should copy riskManagement to RiskManagementCopy', function () {
          Ctrl.riskManagementCopy = {yes:'no'};
          Ctrl.saveValue({yes:'yes'});
          expect(Ctrl.riskManagementCopy.yes).toBe('yes');
        });
      });
      describe('Transition', function () {
        it('should order and set risk', function () {
          var array = {risk:[1,2,3,4]};
          Ctrl.transition(array);
          expect(array.risk.length).toBe(4);
        });
      });
      describe('DisplayQuarterly', function () {
        it('should respond true when data.mode is month and data.date.getMonth is 0', function () {
          var data = {
            mode: 'month',
            date: {
              getMonth: function () {
                return 0;
              }
            }
          };
          var result = Ctrl.dateOptions.dateDisabled(data);
          expect(result).toBe(true);
        });

        it('should respond false when data.mode is month and data.date.getMonth is 2', function () {
          var data = {
            mode: 'month',
            date: {
              getMonth: function () {
                return 2;
              }
            }
          };
          var result = Ctrl.dateOptions.dateDisabled(data);
          expect(result).toBe(false);
        });
      });
      describe('GetQuarter', function () {
        it('should set the correct quarter', function () {
          var date = '2017-03';
          Ctrl.getQuarter(date);
          expect(Ctrl.quarter).toBe(1);
          date = '2017-09';
          Ctrl.getQuarter(date);
          expect(Ctrl.quarter).toBe(3);
        });
      });
      describe('AcceptChanges', function () {
        it('should show dialog to accept', function () {
          Ctrl.acceptChanges(null);
          expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
        });
      });
      describe('LoadQuarter', function () {
        it('should build categorized risk table', function () {
          MockRiskItem = [
            { currency: { code: 'JPY' } },
            { currency: { code: 'USD' } },
            { currency: { code: 'USD' } },
            { currency: { code: 'CAD' } },
            { currency: { code: 'CAD' } },
            { currency: { code: 'CAD' } }
          ];
          Ctrl.loadQuarter();
          expect(Ctrl.accordion.length).toBe(3);
          expect(Ctrl.accordion[0].risk.length).toBe(1);
          expect(Ctrl.accordion[1].risk.length).toBe(2);
          expect(Ctrl.accordion[2].risk.length).toBe(3);
        });
      });
      describe('UpdateCancel', function () {
        it('should set riskManagement to the backup', function () {
          Ctrl.riskManagementCopy.id = 123;
          var riskManagement = {};
          var name = 'amount00';
          var editForm = {
            $setPristine: function(){}
          };
          editForm[name] = {
            $setPristine: function(){}
          };
          Ctrl.updateCancel(riskManagement, editForm, "0", 0);
          expect(riskManagement.id).toBe(Ctrl.riskManagementCopy.id);
        });
      });
      describe('UpdateRisk', function () {
        it('should update the risk', function () {
          var riskManagement = {};
          var name = 'amount00';
          var editForm = {
            $setPristine: jasmine.createSpy('$setPristine')
          };
          editForm[name] = {
            $setPristine: jasmine.createSpy('$setPristine')
          };
          Ctrl.updateRisk(riskManagement, editForm, "0", 0);
          expect(editForm[name].$setPristine).toHaveBeenCalled();
          expect(editForm.$setPristine).toHaveBeenCalled();
        });
      });
      describe('ChangeAmount', function () {
        it('should set the correct value', function () {
          var rm = {
            amount: 2500,
            currency: {
              exchangeRate: 0.0088
            }
          };
          Ctrl.changeAmount(rm);
          expect(rm.convertedAmount).toBe('22.00');
        });
      });
      describe('FilterByCode', function () {
        it('should remove the appropriate lists', function () {
          Ctrl.accordion = [
            { code: 'JPY' },
            { code: 'USD' },
            { code: 'CAD' }
          ];
          Ctrl.currencyCode = [
            { code: 'JPY' },
            { code: 'USD' },
            { code: 'CAD' }
          ];
          Ctrl.currencyCodeSelected = [
            'JPY',
            'CAD'
          ];
          Ctrl.filterByCode();
          expect(Ctrl.accordion.length).toBe(2);
          expect(Ctrl.currencyCode.length).toBe(2);
        })
      });
      describe('Reset', function () {
        it('should call loadQuarter and set filter to false', function () {
          Ctrl.reset();
          expect(Ctrl.filter).toBe(false);
        })
      });
      describe('Expand', function () {
        it('should remove the appropriate lists', function () {
          Ctrl.accordion = [
            { status: false },
            { status: false },
            { status: false }
          ];
          Ctrl.expand();
          for (var i = 0; i < 3; i++)
            expect(Ctrl.accordion[i].status).toBe(true);
        });
      });
      describe('Collapse', function () {
        it('should remove the appropriate lists', function () {
          Ctrl.accordion = [
            { status: true },
            { status: true },
            { status: true }
          ];
          Ctrl.collapse();
          for (var i = 0; i < 3; i++)
            expect(Ctrl.accordion[i].status).toBe(false);
        });
      });
    });
  });
});
