'use strict';

describe('Controller Tests', function() {
  describe('RiskManagement Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockRiskManagement,MockUiModalInstance;
    var MockEntity, MockPreviousState, MockRiskCustomSrv, getByDateObj, MockInteractionsService;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = jasmine.createSpy('MockEntity');
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockRiskManagement = {
          get: function (idObj, callback) {
            callback(idObj);
          },
          delete: jasmine.createSpy('delete')
        };
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        MockRiskCustomSrv = {
          getByDate: {
            query: function (obj, callback) {
              callback(getByDateObj);
            }
          },
          upload: {
            query: function (riskManagement, success, error) {
              success(riskManagement);
            }
          }
        };
        MockInteractionsService = {
          alertParentScrollFalseDialog: jasmine.createSpy('alertParentScrollFalseDialog'),
          confirmBasicParentNoScrollDialog: jasmine.createSpy('confirmBasicParentNoScrollDialog'),
          showThenDialog: function (dialog, yes, no) {
            yes();
          }
        };
        Ctrl = $controller('RiskManagementDialogController', {
          $scope: scope,
          $state: state, entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          RiskManagement: MockRiskManagement,
          RiskCustomSrv: MockRiskCustomSrv,
          InteractionsService: MockInteractionsService
        });
      }));

    describe('Initial state', function () {
      it('RiskManagementDialog Controller', function() {
        expect(Ctrl).toBeDefined();
      });
    });

    describe('Main Content Coverage', function () {
      describe('Load', function () {
        it('should set the riskManagement object to the result', function () {
          var id = 123;
          Ctrl.load(id);
          expect(Ctrl.riskManagement.id).toBe(id);
        });
      });
      describe('OpenCalendar', function () {
        it('should set datePickerOpenStatus[date] to true', function () {
          var item = 'item';
          Ctrl.datePickerOpenStatus[item] = false;
          Ctrl.openCalendar(item);
          expect(Ctrl.datePickerOpenStatus[item]).toBe(true);
        });
      });
      describe('DisplayQuarterly', function () {
        it('should respond true when data.mode is month and data.date.getMonth is 0', function () {
          var data = {
            mode: 'month',
            date: { getMonth: function () { return 0; } }
          };

          var result = Ctrl.dateOptions.dateDisabled(data);

          expect(result).toBe(true);
        });

        it('should respond false when data.mode is month and data.date.getMonth is 2', function () {
          var data = {
            mode: 'month',
            date: { getMonth: function () { return 2; } }
          };

          var result = Ctrl.dateOptions.dateDisabled(data);

          expect(result).toBe(false);
        });
      });
      describe('GetQuarter', function () {
        it('should set the correct quarter', function () {
          var date = '2017-03';
          Ctrl.getQuarter(date);
          expect(Ctrl.quarter).toBe(1);
          date = '2017-09';
          Ctrl.getQuarter(date);
          expect(Ctrl.quarter).toBe(3);
        });
      });
      describe('Clear', function () {
        it('should run $uibModalInstance.dismiss', function () {
          Ctrl.clear();
          expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
        });
      });
      describe('CheckDate', function () {
        describe('RiskCustomSrv.getByDate.query', function () {
          it('should set warningMessage if an overwrite would take place', function () {
            var date = "01/01/2017";
            getByDateObj = [{},{}];
            Ctrl.checkDate(date);
            expect(Ctrl.warningMessage).not.toBeNull();
          });
          it('should unset warningMessage if an overwrite would not take place', function () {
            var date = "01/01/2017";
            getByDateObj = [];
            Ctrl.checkDate(date);
            expect(Ctrl.warningMessage).toBeNull();
          });
        });
      });
      describe('Save', function () {
        it('should create Dialog', function () {
          Ctrl.save();
          expect(MockInteractionsService.confirmBasicParentNoScrollDialog).toHaveBeenCalled();
        });
        describe('OnSaveSuccess', function () {
          it('should delete all items in riskManagementDelete', function () {
            Ctrl.riskManagement.statusMessage = 'Successful!';
            Ctrl.overwrite = true;
            Ctrl.riskManagementDelete = [{},{},{}];
            Ctrl.save();
            expect(MockRiskManagement.delete).toHaveBeenCalledTimes(3);
          });
          it('should not delete anything when there isn\'t anything to delete', function () {
            Ctrl.riskManagement.statusMessage = 'Successful!';
            Ctrl.overwrite = false;
            Ctrl.riskManagementDelete = [];
            Ctrl.save();
            expect(MockRiskManagement.delete).not.toHaveBeenCalled();
          });
        });
      });
    });
  });
});
