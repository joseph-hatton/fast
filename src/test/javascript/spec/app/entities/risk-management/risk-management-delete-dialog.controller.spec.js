'use strict';

describe('Controller Tests', function() {
  describe('RiskManagement Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockRiskManagement,MockUiModalInstance;
    var MockEntity, MockPreviousState, MockRiskCustomSrv, getByDateObj, MockInteractionsService;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = jasmine.createSpy('MockEntity');
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockRiskManagement = {
          delete: function (idObj, callback) {
            return "Success deletion";
          }
        };
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);

        MockInteractionsService = {
          alertParentScrollFalseDialog: jasmine.createSpy('alertParentScrollFalseDialog'),
          confirmBasicParentNoScrollDialog: jasmine.createSpy('confirmBasicParentNoScrollDialog'),
          showThenDialog: function (dialog, yes, no) {
            yes();
          }
        };
        Ctrl = $controller('RiskManagementDeleteController', {
          $scope: scope,
          $state: state, entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          RiskManagement: MockRiskManagement
        });
      }));

    describe('Initial state', function () {
      it('RiskManagementDelete Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('confirmDelete ', function() {
        expect(Ctrl.confirmDelete).toBeDefined();
        Ctrl.confirmDelete(123);
        //expect(MockUiModalInstance.close).toHaveBeenCalled();
      });


      it('Clear', function() {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });
    });


  });
});
