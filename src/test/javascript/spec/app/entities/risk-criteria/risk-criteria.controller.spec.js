'use strict';

describe('Controller Tests', function() {

  describe('RiskCriteria Controller', function() {
    var Ctrl;
    var scope,state,paginationConstants,pagingParams;

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        pagingParams={ascending:true,page:1,predicate:"id",search:null,sort:"id,asc"};
        paginationConstants= {itemsPerPage: 10};
        Ctrl = $controller('RiskCriteriaController', {
          $scope: scope,
          $state: state,
          paginationConstants:paginationConstants,
          pagingParams:pagingParams
        });
      }));

    describe('Initial state', function () {
      it('RiskCriteria Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('RiskCriteria Load All Test', function() {
        expect(Ctrl.loadAll).toBeDefined();
      });

      it('RiskCriteria Search Test', function() {
        expect(Ctrl.search).toBeDefined();
      });
    });
  });
});
