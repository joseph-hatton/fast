'use strict';

describe('Controller Tests', function() {
  describe('RiskCriteriaDeleteController', function() {
    var Ctrl;
    var scope,state,MockRiskCriteria,MockUiModalInstance;
    var MockEntity, MockPreviousState;
    var success='Success';
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = jasmine.createSpyObj('MockEntity', ['get', 'update', 'save']);
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);

        MockRiskCriteria  =
          {
            delete: function (id, callback) {
              return "Success deletion";
            }
          };

        Ctrl = $controller('RiskCriteriaDeleteController', {
          $scope: scope,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          RiskCriteria: MockRiskCriteria
        });
      }));

    describe('Initial state', function () {
      it('RiskCriteria Delete Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('confirmDelete ', function() {
        expect(Ctrl.confirmDelete).toBeDefined();
        Ctrl.confirmDelete(123);
        //expect(MockUiModalInstance.close).toHaveBeenCalled();
      });


      it('Clear', function() {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });
    });
  });
});
