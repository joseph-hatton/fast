'use strict';

describe('Controller Tests', function() {

  describe('SeriatimType Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockSeriatimType;
    var Ctrl;

    beforeEach(inject(function($controller,$injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      MockEntity = jasmine.createSpy('MockEntity');
      MockSeriatimType = {
        get: function (id, callback) {
          callback(id);
        }
      };

      Ctrl = $controller("SeriatimTypeDetailController", {
        '$scope': $scope,
        '$rootScope': $rootScope,
        'entity': MockEntity ,
        'SeriatimType': MockSeriatimType
      });

    }));

    describe('Initial State', function () {
      it('SeriatimType Detail Controller Should be Created', function () {
        expect(Ctrl).toBeDefined();
      });

      it('Load Should be Defined', function () {
        expect(Ctrl.load).toBeDefined();
      });
    });
  });

});
