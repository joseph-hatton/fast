'use strict';

describe('Controller Tests', function() {
  describe('SeriatimType Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockSeriatimType,MockUiModalInstance;
    var MockEntity, MockPreviousState;
    var success='Success';
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = jasmine.createSpyObj('MockEntity', ['get', 'update', 'save']);
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);


        MockSeriatimType  =
          {
            get: function (id, callback) {
              callback(id);
            },
            update: function(id,success, error){ // for testing purposes, a flag of error will be set to trigger an 'error'
              if(!id.errorTestFlag)
                success(id);
              else
                error();
            },
            save: function (id, success, error) {
              if(!id.errorTestFlag)
                success(id);
              else
                error();
            }
          };

        Ctrl = $controller('SeriatimTypeDialogController', {
          $scope: scope,
          $state: state,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          SeriatimType: MockSeriatimType
        });
      }));

    describe('Initial state', function () {
      it('SeriatimTypeDialog Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('Load', function() {
        expect(Ctrl.load).toBeDefined();
      });

      it('Save', function() {
        expect(Ctrl.save).toBeDefined();
      });

      it('Clear', function() {
        expect(Ctrl.clear).toBeDefined();
      });
    });
  });
});
