'use strict';

describe('Controller Tests', function() {

  describe('SeriatimType Controller', function() {

    var Ctrl;
    var scope, MockSeriatimType, MockSeriatimTypeSearch;
    var loadAllResult = [{},{},{}], searchResult = [{}];

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();

        MockSeriatimType = {
          query: function (callback) {
            callback(loadAllResult);
          }
        };

        MockSeriatimTypeSearch = {
          query: function (search, callback) {
            callback(searchResult);
          }
        };

        Ctrl = $controller('SeriatimTypeController', {
          $scope: scope,
          SeriatimType: MockSeriatimType,
          SeriatimTypeSearch: MockSeriatimTypeSearch
        });
      }));

    describe('Initial state', function () {
      it('SeriatimType Controller', function() {
        expect(Ctrl).toBeDefined();
      });
      it('Load All Test', function() {
        expect(Ctrl.loadAll).toBeDefined();
      });
      it('Search Test', function() {
        expect(Ctrl.search).toBeDefined();
      });
    });
    describe('Main Content Cover', function () {
      it('Load All Test', function () {
        expect(Ctrl.seriatimTypes.length).toBe(loadAllResult.length);
      });
      it('Search No Query Test', function () {
        Ctrl.searchQuery = null;
        Ctrl.search();
        expect(Ctrl.seriatimTypes.length).toBe(loadAllResult.length);
      });
      it('Search Query Test', function () {
        Ctrl.searchQuery = 1;
        Ctrl.search();
        expect(Ctrl.seriatimTypes.length).toBe(searchResult.length);
      });
    });
  });
});
