'use strict';

describe('Controller Tests', function() {

  describe('Company Controller', function() {
    var Ctrl;
    var scope,state,paginationConstants,pagingParams;
    var MockCompanyCustomSrv;
    var info = [{},{},{}];
    var infoSearch = [{}];
    var infoSearchMain=[{},{},{},{},{}];
    var page, size ,MockCompanySearch,sort,MockParseLinks;


// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope,CompanyCustomSrv,ParseLinks) {     // Don't bother injecting a 'real' modal

        scope = $rootScope.$new();
        state =$rootScope.$state;
        paginationConstants= {itemsPerPage: 10};
        pagingParams={ascending:true,page:1,predicate:"id",search:null,sort:"id,asc"};
        page=pagingParams.page;
          size=paginationConstants.itemsPerPage;

        function myMockHeadersFunction(key) {
          if (key === 'link')
            return '</api/companies/non-mdm?page=0&size=10>; rel="last",</api/companies/non-mdm?page=0&size=10>; rel="first"';
          return 1;
        }

        MockCompanySearch= {
          query: function(searchString,callBack){
            callBack(infoSearchMain,myMockHeadersFunction);
          }
        };
        MockCompanyCustomSrv  = CompanyCustomSrv;
         MockCompanyCustomSrv.nonMdmCompanies.query= function(searchString,callBack){
           callBack(info,myMockHeadersFunction);
        };
        MockParseLinks=ParseLinks;
        MockCompanyCustomSrv.searchNonMdmCompanies.query= function(searchString,callBackSearch){
          callBackSearch(infoSearch,myMockHeadersFunction);
        };



        Ctrl = $controller('CompanyController', {
          $scope: scope,
          $state: state,CompanySearch:MockCompanySearch,
          ParseLinks:MockParseLinks,
          CompanyCustomSrv:MockCompanyCustomSrv,
          paginationConstants:paginationConstants,
          pagingParams:pagingParams
        });
      }));

    describe('Initial state', function () {
      it('Company Controller', function() {
        expect(Ctrl).toBeDefined();
      });
      it('Company Load All Test', function() {
        expect(Ctrl.loadAll).toBeDefined();
        Ctrl.loadAll();
        expect(Ctrl.companies).toBeDefined();
        expect(Ctrl.companies.length).toBe(3);
      });

      it('Company Load All Test with Search', function() {
        pagingParams.search='abc';
        expect(Ctrl.loadAll).toBeDefined();
        Ctrl.loadAll();
        expect(Ctrl.companies).toBeDefined();
        expect(Ctrl.companies.length).toBe(5);
      });


      it('Company New Search No MDM Company ', function() {
        Ctrl.searchQuery='abc';
        expect(Ctrl.newsearch).toBeDefined();
        Ctrl.newsearch();
        expect(Ctrl.companies).toBeDefined();
        expect(Ctrl.companies.length).toBe(1);

      });

      it('Company  Search ', function() {
        Ctrl.searchQuery='abc';
        expect(Ctrl.search).toBeDefined();
        Ctrl.transition= function (){return 'OK'};
        Ctrl.search(Ctrl.searchQuery);
        expect(Ctrl.companies).toBeDefined();
        expect(Ctrl.companies.length).toBe(3);

      });

      it('Company  Search Null load ALL ', function() {
        Ctrl.searchQuery=null;
        expect(Ctrl.searchQuery).toBeNull();
        Ctrl.transition= function (){return 'OK'};
        Ctrl.search(Ctrl.searchQuery);
        expect(Ctrl.companies).toBeDefined();
        expect(Ctrl.companies.length).toBe(3);

      });

      it('Company  Clear search ', function() {
        expect(Ctrl.clear).toBeDefined();
        Ctrl.transition= function (){return 'OK'};
        Ctrl.clear();
        expect(Ctrl.companies).toBeDefined();
        expect(Ctrl.companies.length).toBe(3);
      });


    });
  });
});
