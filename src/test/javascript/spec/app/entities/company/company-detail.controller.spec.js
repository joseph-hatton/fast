'use strict';

describe('Controller Tests', function () {

    describe('Company Detail Controller', function () {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockCompany, MockContact, MockCompanyCustomSrv;
        var Ctrl;
        var action = function(){};

        beforeEach(inject(function ($controller, $injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockCompany = {
              update: function (company, success, error) { }
            };
            MockContact = jasmine.createSpy('MockContact');
            MockCompanyCustomSrv = { searchMdmCompanies: { query: function (searchStringObj, success, error) {
              action(success, error);
            }}};

            Ctrl = $controller("CompanyDetailController", {
              $scope: $scope,
              $rootScope: $rootScope,
              entity: MockEntity,
              previousState: MockPreviousState,
              Company: MockCompany,
              Contact: MockContact,
              CompanyCustomSrv: MockCompanyCustomSrv
            });
        }));


        describe('Initial Setup', function () {
          it('Company Detail Controller Creation', function () {
              expect(Ctrl).toBeDefined();
          });

          it('NewSearch Defined', function () {
            expect(Ctrl.newsearch).toBeDefined();
          });

          it('MergeMdmCompany Defined', function () {
            expect(Ctrl.newsearch).toBeDefined();
          });
        });

        describe('Main Code Coverage', function () {
          describe('New Search', function () {
            it('Should run SearchMdmCompanies.Query', function () {
              Ctrl.searchingCompany = false;
              Ctrl.newsearch("string");
              expect(Ctrl.searchingCompany).toBe(true);
            });

            describe('OnSuccess', function () {
              it('Should Run', function () {
                var data = [
                  { companyName: "CompanyName" }
                ];
                action = function (success, error) {
                  success(data, function (task) {});
                }

                Ctrl.newsearch("string");

                expect(Ctrl.companiesAbc.length).toBe(1);
                expect(Ctrl.companiesAbc[0].name).toBe("CompanyName");
              });
            });

            describe('OnError', function () {
              it('Should Run', function () {
                action = function (success, error) {
                  var err = "";
                  error(err);
                };
                console = jasmine.createSpyObj(console, ["log"]);

                Ctrl.newsearch("string");

                expect(console.log).toHaveBeenCalled();
              });
            });
          });

          describe('MergeMdmCompany', function () {
            it('should run', function () {
              Ctrl.selectedCompany = {
                companyName: "companyName",
                clientCompanyId: 123,
                legalCode: 123,
                legalName: "legalName",
                affiliateName: "affiliateName",
                mdmId: 123
              };

              Ctrl.mergeMdmCompany({});

            });
          });
        });
    });

});
