'use strict';

describe('Controller Tests', function () {
  describe('Company Dialog Controller', function () {
    var Ctrl;
    var scope, state, MockUiModalInstance;
    var MockEntity, MockPreviousState, MockCompany, MockState, MockStateParams, flag = true, unlock = true, MockMdDialog, MockCompanyCustomSrv, companyList;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state = $rootScope.$state;
        MockEntity = jasmine.createSpy('MockEntity');
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        MockCompany = {
          get: function (idObj, callback) {
            callback({id: idObj, country: {name: "United States"}});
          },
          update: function (company, success, error) {
            success(company);
          },
          save: function (company, success, error) {
            error();
          }
        };
        MockCompanyCustomSrv = { companies: {
          query: function (Query, callback) {
            callback(companyList);
          }
        }};
        // jasmine.spyOn(MockCompany, 'get').andCallThrough();
        MockState = {
          query: function (callback) {
            callback("test");
          }
        };
        if (unlock) {
          if (flag) {
            MockStateParams = {id: undefined};
          } else {
            MockStateParams = {id: 123};
          }
        } else {
          MockStateParams = {id: undefined};
        }

        //Ugly, but works...Used to test the save method
        MockMdDialog = { confirm: function () { return { textContent: function(){ return {ok: function () { return {cancel:function(){ return {parent:function(){}}}} }} } }; },
                         alert: function(){return {textContent:function(){return {ok:function(){return {parent:function(){}};}};}};},
                         show: function(){
                           return {
                             then: function(success, bad){
                               if(bad !== undefined)
                                bad();
                             }
                           };
                         }
        };

        Ctrl = $controller('CompanyDialogController', {
          $scope: scope,
          $state: state,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          Company: MockCompany,
          $stateParams: MockStateParams,
          State: MockState,
          $mdDialog: MockMdDialog,
          CompanyCustomSrv: MockCompanyCustomSrv

        });
      }));

    describe('Initial state', function () {
      it('CompanyDialog Controller', function () {
        expect(Ctrl).toBeDefined();
      });

      it('SelectedCountry', function () {
        expect(Ctrl.selectedCountry).toBeDefined();
      });
    });

    describe('Main Code Coverage', function () {
      describe('Selected Country Tests', function () {
        describe('United States', function () {
          beforeEach(function () {
            Ctrl.company.country = {name: "United States"};
            Ctrl.states = [
              {id: 123, description: "Missouri"}
            ];
          });

          it('State === description', function () {
            Ctrl.company.state = "Missouri";
            Ctrl.selectedCountry();
            expect(Ctrl.company.state.id).toBe(123);
          });

          it('State !== description', function () {
            Ctrl.company.state = "Illinois";
            Ctrl.selectedCountry();
            expect(Ctrl.company.state.id).toBeUndefined();
          });
        });

        describe('Not United States', function () {
          beforeEach(function () {
            Ctrl.company.country = {name: "United Kingdom"};
          });

          it('State not filled', function () {
            Ctrl.company.state = null;
            Ctrl.selectedCountry();
            expect(Ctrl.company.state).toBeNull();
          });

          it('State filled', function () {
            Ctrl.company.state = {description: "desc"};
            Ctrl.selectedCountry();
            expect(Ctrl.company.state).toBe("desc");
          });
        });

        it('Region', function () {
          var region = "North America";
          Ctrl.company.country = {region: region};
          Ctrl.company.region = null;
          Ctrl.company.state = null;
          Ctrl.selectedCountry();
          expect(Ctrl.company.region).toBe(region);
        });
      });

      describe('State Query Test', function () {
        it('id is undefined', function () {
          expect(Ctrl.company.id).toBeUndefined();
          flag = false;
        });

        it('id is NOT undefined', function () {
          expect(Ctrl.company.id.id).toBe(123);
          unlock = !unlock;
        });
      });

      describe('SaveConfirm Test', function () {
        it('Company Id Not Null', function () {
          Ctrl.company = {id: 123, name: "CompanyName", state: {description: "Missouri"}};

          Ctrl.saveConfirm();

          expect(MockUiModalInstance.close).toHaveBeenCalled();
          expect(Ctrl.isSaving).toBe(false);
        });

        it('Company Id Null', function () {
          Ctrl.company = {id: null, name: "CompanyName", state: {description: "Missouri"}};

          Ctrl.saveConfirm();

          expect(MockUiModalInstance.close).not.toHaveBeenCalled();
          expect(Ctrl.isSaving).toBe(false);
        });
      });

      it('Clear Test', function () {
        Ctrl.clear();
        expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
      });
      describe('Save Test', function () {
        describe('Company Id not set', function () {
          beforeEach(function () {
            Ctrl.company = {id:null, name: "Company"};

          });

          it('Same Name', function () {
            companyList = [
              {id:123, name: "Company"}
            ];

            Ctrl.save();

            expect(MockUiModalInstance.dismiss).not.toHaveBeenCalled();
          });

          it('Similar Name', function () {
            companyList = [
              {id:123, name: "Compan"}
            ];

            Ctrl.save();

            expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
          });
        });
      });
    });
  });
});
