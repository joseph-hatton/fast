'use strict';

describe('Controller Tests', function () {
  describe('Company Delete Dialog Controller', function () {
    var Ctrl,MockXrefTreatyCustomCompany;
    var scope, state, MockUiModalInstance;
    var MockEntity, MockPreviousState, MockCompany, MockState, MockStateParams, flag = true, unlock = true, MockMdDialog, MockCompanyCustomSrv, companyList;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state = $rootScope.$state;
        MockEntity = jasmine.createSpy('MockEntity');
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockXrefTreatyCustomCompany = { companies:jasmine.createSpyObj('companies', ['query']) }
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        MockCompany = {
          get: function (idObj, callback) {
            callback({id: idObj, country: {name: "United States"}});
          },
          update: function (company, success, error) {
            success(company);
          },
          save: function (company, success, error) {
            error();
          }
        };
        MockCompanyCustomSrv = { companies: {
          query: function (Query, callback) {
            callback(companyList);
          }
        }};
        // jasmine.spyOn(MockCompany, 'get').andCallThrough();
        MockState = {
          query: function (callback) {
            callback("test");
          }
        };
        if (unlock) {
          if (flag) {
            MockStateParams = {id: undefined};
          } else {
            MockStateParams = {id: 123};
          }
        } else {
          MockStateParams = {id: undefined};
        }

        //Ugly, but works...Used to test the save method



        Ctrl = $controller('CompanyDeleteController', {
          $scope: scope,
          $state: state,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          Company: MockCompany,
          $stateParams: MockStateParams,
          State: MockState,
          CompanyCustomSrv: MockCompanyCustomSrv,
          XrefTreatyCustomCompany: MockXrefTreatyCustomCompany

        });
      }));

    describe('Initial state', function () {
      it('CompanyDeleteDialog Controller', function () {
        expect(Ctrl).toBeDefined();
      });

      it('clear ', function () {
        expect(Ctrl.clear).toBeDefined();
      });
      it('confirmDelete  ', function () {
        expect(Ctrl.confirmDelete).toBeDefined();
      });
    });

  });
});
