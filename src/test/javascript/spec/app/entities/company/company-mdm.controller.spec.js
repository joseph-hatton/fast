'use strict';

describe('Controller Tests', function() {

    describe('Company Mdm Controller', function() {
        var Ctrl;
        var scope,state, DeferFactory, $rootScope;
        var MockPagingParams, MockCompanyCustomSrv, MockStatInfo, MockIntro;
        var statInfoLoadByTitleDefer, MockCountry;

        beforeEach(inject(function ($controller, _$rootScope_, CompanyCustomSrv,ParseLinks, _DeferFactory_) {
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();
            state =$rootScope.$state;
            DeferFactory = _DeferFactory_;
            MockPagingParams = jasmine.createSpy('PagingParams');
            MockCompanyCustomSrv = {
                refreshAllMdmCompanies: jasmine.createSpyObj('refreshAllMdmCompanies', ['run']),
                mdmCompanies: jasmine.createSpyObj('mdmCompanies', ['query'])
            };
            MockStatInfo = {
                modify: jasmine.createSpyObj('modify', ['save', 'update']),
                load: jasmine.createSpyObj('load', ['byTitle'])
            };
            statInfoLoadByTitleDefer = DeferFactory.generate();
            MockStatInfo.load.byTitle.and.returnValue(statInfoLoadByTitleDefer);
            MockIntro = jasmine.createSpyObj('Intro', ['setupIntroSteps']);
            MockIntro.setupIntroSteps.and.returnValue(DeferFactory.generate().promise);
          MockCountry=jasmine.createSpyObj('Country', ['query']);
            Ctrl = $controller('CompanyMdmController', {
                $scope: scope,
                $state: state,
                pagingParams: MockPagingParams,
                CompanyCustomSrv: MockCompanyCustomSrv,
                StatInfo: MockStatInfo,
                Intro: MockIntro,
                Country:MockCountry

            });
        }));

        describe('Setup', function () {
            it('Company Controller', function() {
                expect(Ctrl).toBeDefined();
            });
        });
        describe('refreshAllMdmCompanies', function(){
            var defer;
            beforeEach(function(){
                defer = DeferFactory.generate();
                Ctrl.loadAll = jasmine.createSpy('loadAll');
            });
            it('should set refreshingAllMdm to true and call CompanyCustomSrv.refreshAllMdmCompanies', function(){
                MockCompanyCustomSrv.refreshAllMdmCompanies.run.and.returnValue(defer);

                Ctrl.refreshAllMdmCompanies();

                expect(Ctrl.refreshingAllMdm).toBe(true);
                expect(MockCompanyCustomSrv.refreshAllMdmCompanies.run).toHaveBeenCalled();
            });
            it('should setup statInfo if it isn\'t set', function(){
                MockCompanyCustomSrv.refreshAllMdmCompanies.run.and.returnValue(defer);

                Ctrl.refreshAllMdmCompanies();
                var results = [{},{}];
                defer.resolve(results);
                $rootScope.$apply();

                expect(Ctrl.statInfo).toBeDefined();
                expect(Ctrl.statInfo.title).toBe(Ctrl.statInfoTitle);
                expect(Ctrl.statInfo.lastUpdatedCount).toBe(results.length);
                expect(Ctrl.statInfo.lastUpdated).toBeDefined();
                expect(MockStatInfo.modify.save).toHaveBeenCalled();
                expect(Ctrl.refreshingAllMdm).toBe(false);
                expect(Ctrl.loadAll).toHaveBeenCalled();
            });
            it('should not setup statInfo if it is set', function(){
                MockCompanyCustomSrv.refreshAllMdmCompanies.run.and.returnValue(defer);
                Ctrl.statInfo = {
                    id:123,
                    title: "..."
                };

                Ctrl.refreshAllMdmCompanies();
                var results = [{},{}];
                defer.resolve(results);
                $rootScope.$apply();

                expect(Ctrl.statInfo.title).not.toBe(Ctrl.statInfoTitle);
                expect(Ctrl.statInfo.lastUpdatedCount).toBe(results.length);
                expect(Ctrl.statInfo.lastUpdated).toBeDefined();
                expect(MockStatInfo.modify.update).toHaveBeenCalled();
                expect(Ctrl.refreshingAllMdm).toBe(false);
                expect(Ctrl.loadAll).toHaveBeenCalled();
            });
        });
        describe('StatInfo.load.byTitle', function(){
            beforeEach(function(){
                Ctrl.refreshAllMdmCompanies = jasmine.createSpy('refreshAllMdmCompanies');
            });
            it('should set statInfo to result when the result is valid', function(){
                var result = {};

                statInfoLoadByTitleDefer.resolve(result);
                $rootScope.$apply();

                expect(Ctrl.statInfo).toBe(result);
            });
            it('should refresh when the result is invalid because the length is 0', function(){
                var reason = {
                    result: []
                };

                statInfoLoadByTitleDefer.reject(reason);
                $rootScope.$apply();

                expect(Ctrl.refreshAllMdmCompanies).toHaveBeenCalled();
                expect(MockStatInfo.modify.update).not.toHaveBeenCalled();
            });
            it('should "delete" all stats for the page when the result is invalid because the length is greater than 0, then refresh', function(){
                var reason = {
                    result: [{},{}]
                };
                var defers = [];
                defers.push(DeferFactory.generate());
                defers.push(DeferFactory.generate());
                MockStatInfo.modify.update.and.returnValues(defers[0], defers[1]);

                statInfoLoadByTitleDefer.reject(reason);
                $rootScope.$apply();

                expect(Ctrl.refreshAllMdmCompanies).not.toHaveBeenCalled();
                expect(MockStatInfo.modify.update).toHaveBeenCalledTimes(2);

                defers[0].resolve();
                defers[1].resolve();
                $rootScope.$apply();

                expect(Ctrl.refreshAllMdmCompanies).toHaveBeenCalled();
            });
        });
    });
});
