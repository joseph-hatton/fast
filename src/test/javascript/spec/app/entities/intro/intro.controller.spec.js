'use strict';

describe('Controller Tests', function() {

    describe('Intro Controller', function() {
        var $scope;
        var MockIntro, MockInteractionsService;
        var DeferFactory;
        var Ctrl;
        beforeEach(inject(
        function ($controller, $rootScope, _DeferFactory_) {
            $scope = $rootScope.$new();
            DeferFactory = _DeferFactory_;

            MockIntro = {
                "load": jasmine.createSpyObj('load', ['all']),
                "modify": jasmine.createSpyObj('modify', ['update', 'save'])
            };
            MockIntro.modify.update.and.callFake(function(){
                return DeferFactory.generate();
            });
            MockIntro.modify.save.and.callFake(function(){
                return DeferFactory.generate();
            });
            MockIntro.load.all.and.callFake(function(){
                return DeferFactory.generate();
            });
            MockInteractionsService = jasmine.createSpyObj('InteractionsService', ['confirmBasicParentNoScrollDialog', 'showThenDialog']);
            
            Ctrl = $controller('IntroController', {
                $scope: $scope,
                Intro: MockIntro,
                InteractionsService: MockInteractionsService
            });
        }));

        describe('Initial state', function () {
            it('should instantiate the controller properly', function () {
                expect(Ctrl).not.toBeUndefined();
            });
        });

        describe('Main Content', function(){
            describe('loadAll', function () {
                it('should load the intros', function(){
                    MockIntro.load.all.and.callFake(function(callback){
                        callback([{},{}]);
                        return DeferFactory.generate();
                    });

                    Ctrl.loadAll();

                    expect(Ctrl.intros.length).toBe(2);
                });
            });
            describe('savAll', function(){
                it('should save each item in intros', function(){
                    Ctrl.intros = [{},{}];

                    Ctrl.saveAll();

                    expect(MockIntro.modify.save).toHaveBeenCalledTimes(2);
                });
            });
            describe('moveUp', function(){
                it('should move the item up in the array', function(){
                    var steps = [
                        { order:1, id: 1 },
                        { order:2, id: 2 },
                        { order:3, id: 3 },
                    ];

                    var intro = { "steps": steps };
                    
                    Ctrl.moveUp(intro, 1);

                    expect(steps[0].id).toBe(2);
                    expect(steps[1].id).toBe(1);
                    expect(steps[2].id).toBe(3);

                    expect(steps[0].order).toBe(1);
                    expect(steps[1].order).toBe(2);
                    expect(steps[2].order).toBe(3);
                });
            });
            describe('moveDown', function(){
                it('should move the item down in the array', function(){
                    var steps = [
                        { order:1, id: 1 },
                        { order:2, id: 2 },
                        { order:3, id: 3 },
                    ];

                    var intro = { "steps": steps };
                    
                    Ctrl.moveDown(intro, 1);

                    expect(steps[0].id).toBe(1);
                    expect(steps[1].id).toBe(3);
                    expect(steps[2].id).toBe(2);

                    expect(steps[0].order).toBe(1);
                    expect(steps[1].order).toBe(2);
                    expect(steps[2].order).toBe(3);
                });
            });
            describe('addStep', function(){
                it('should add an item to steps for the item', function(){
                    var intro = { steps: [] };

                    Ctrl.addStep(intro);

                    expect(intro.steps.length).toBe(1);
                });
            });
            describe('editStep', function(){
                it('should add an item to steps for the item', function(){
                    var intro = { steps: [{
                        content: "content"
                    }] };

                    Ctrl.editStep(intro, 0);

                    expect(intro.steps[0].contentCache).toBe("content");
                    expect(intro.steps[0].editing).toBe(true);
                });
            });
            describe('doneEditingStep', function(){
                it('should set editing to false', function(){
                    var intro = { steps: [{}] };

                    Ctrl.doneEditingStep(intro, 0);

                    expect(intro.steps[0].editing).toBe(false);
                });
            });
            describe('cancelEditingStep', function(){
                it('should set editing to false', function(){
                    var intro = { steps: [{
                        content: "c1",
                        contentCache: "c2"
                    }] };
                    Ctrl.doneEditingStep = jasmine.createSpy("doneEditingStep");

                    Ctrl.cancelEditingStep(intro, 0);

                    expect(intro.steps[0].content).toBe("c2");
                    expect(Ctrl.doneEditingStep).toHaveBeenCalled();
                });
            });
            describe('editIntro', function(){
                it('should set editing to false', function(){
                    var intro = {
                        description: "desc",
                        title: "title"
                    };

                    Ctrl.editIntro(intro);

                    expect(intro.descriptionCache).toBe("desc");
                    expect(intro.titleCache).toBe("title");
                    expect(intro.editing).toBe(true);
                });
            });
            describe('doneEditingIntro', function(){
                it('should set editing to false', function(){
                    var intro = {};

                    Ctrl.doneEditingIntro(intro);

                    expect(intro.editing).toBe(false);
                });
            });
            describe('cancelEditingIntro', function(){
                it('should set editing to false', function(){
                    var intro = {
                        descriptionCache: "desc",
                        titleCache: "title"
                    };
                    Ctrl.doneEditingIntro = jasmine.createSpy('doneEditingIntro');

                    Ctrl.cancelEditingIntro(intro);

                    expect(intro.description).toBe("desc");
                    expect(intro.title).toBe("title");
                    expect(Ctrl.doneEditingIntro).toHaveBeenCalled();
                });
            });
            describe('removeStep', function(){
                it('should set isActive to 0 if it\'s not in use', function(){
                    var intro = {
                        steps: [
                            { inUse: 0, isActive: 1, id: 1 },
                            { inUse: 0, isActive: 1, id: 2 },
                            { inUse: 0, isActive: 1, id: 3 }
                        ]
                    };


                    Ctrl.removeStep(intro, 0);

                    expect(intro.steps[0].id).toBe(2);
                    expect(intro.steps[1].id).toBe(3);
                    expect(intro.steps[2].id).toBe(1);
                    
                    expect(intro.steps[0].isActive).toBe(1);
                    expect(intro.steps[1].isActive).toBe(1);
                    expect(intro.steps[2].isActive).toBe(0);

                    expect(intro.steps[0].order).toBe(1);
                    expect(intro.steps[1].order).toBe(2);
                });
                it('should set not isActive to 0 if it\'s in use', function(){
                    var intro = {
                        steps: [
                            { inUse: 1, isActive: 1, id: 1 },
                            { inUse: 0, isActive: 1, id: 2 },
                            { inUse: 0, isActive: 1, id: 3 }
                        ]
                    };


                    Ctrl.removeStep(intro, 0);

                    expect(intro.steps[0].isActive).toBe(1);
                    expect(intro.steps[1].isActive).toBe(1);
                    expect(intro.steps[2].isActive).toBe(1);
                });
            });

            describe('removeIntro', function(){
                describe('when yes is selected,', function(){
                    beforeEach(function(){
                        MockInteractionsService.showThenDialog.and.callFake(function(dialog, yes){
                            yes();
                        });
                    });
                    it('should set isActive to 0 if it doesn\'t have any steps', function(){
                        var intro = { steps:[], isActive:1 };
    
                        Ctrl.removeIntro(intro);
    
                        expect(intro.isActive).toBe(0);
                    });
                    it('should not set isActive to 0 if it has any steps', function(){
                        var intro = { steps:[{isActive:1}], isActive:1 };
    
                        Ctrl.removeIntro(intro);
    
                        expect(intro.isActive).toBe(1);
                    });
                });
                it('when no is selected, should not set isActive to 0 if it has any steps', function(){
                    var intro = { steps:[], isActive:1 };

                    Ctrl.removeIntro(intro);

                    expect(intro.isActive).toBe(1);
                });
            });
            describe('createNewIntro', function(){
                it('should add a new intro to intros', function(){
                    Ctrl.intros = [];

                    Ctrl.createNewIntro();

                    expect(Ctrl.intros.length).toBe(1);
                });
            });
        });
    });
});
