'use strict';

describe('Controller Tests', function () {

  describe('TreatyManagement Detail Controller', function () {
    var $scope, $rootScope;
    var MockEntity, MockTreatyManagement, MockTreatyManagementCustomSrv;
    var Ctrl, MockItem;

    beforeEach(inject(function ($controller, $injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      MockEntity = jasmine.createSpy('MockEntity');
      MockTreatyManagement = jasmine.createSpy('MockTreatyManagement');
      MockItem = {
        id: 123,
        entityComments: [
          {entityType: ''}
        ],
        riskFees: []
      };
      MockTreatyManagementCustomSrv = {
        get: function (idObj, callback) {
          callback(MockItem);
        }
      };
      Ctrl = $controller("TreatyManagementDetailController", {
        $scope: $scope,
        $rootScope: $rootScope,
        entity: MockEntity,
        TreatyManagement: MockTreatyManagement,
        TreatyManagementCustomSrv: MockTreatyManagementCustomSrv
      });
    }));


    describe('Initial', function () {
      describe('TreatyManagement Detail Controller', function () {
        it('should be defined', function () {
          expect(Ctrl).toBeDefined();
        });
      });
    });
    describe('Main Content', function () {
      describe('TreatyManagementCustomSrv', function () {
        it('should set quota items when entityType is QuotaShareCommentsMgnt', function () {
          MockItem.entityComments[0].entityType = 'QuotaShareCommentsMgnt';
          Ctrl.load(MockTreatyManagementCustomSrv, {});
          expect(Ctrl.QuotaShareCommentsMgnt).toBeDefined();
        });
        it('should set treaty items when entityType is TreatyReferenceSeriatimMgnt', function () {
          MockItem.entityComments[0].entityType = 'TreatyReferenceSeriatimMgnt';
          Ctrl.load(MockTreatyManagementCustomSrv, {});
          expect(Ctrl.TreatyReferenceSeriatimMgnt).toBeDefined();
        });
        it('should set seriatim items when entityType is CommentsSeriatimMgnt', function () {
          MockItem.entityComments[0].entityType = 'CommentsSeriatimMgnt';
          Ctrl.load(MockTreatyManagementCustomSrv, {});
          expect(Ctrl.CommentsSeriatimMgnt).toBeDefined();
        });
        it('should set treaty ref items when entityType is TreatyReferenceIntPeMgnt', function () {
          MockItem.entityComments[0].entityType = 'TreatyReferenceIntPeMgnt';
          Ctrl.load(MockTreatyManagementCustomSrv, {});
          expect(Ctrl.TreatyReferenceIntPeMgnt).toBeDefined();
        });
        it('should set comments items when entityType is CommentsIntPeMgnt', function () {
          MockItem.entityComments[0].entityType = 'CommentsIntPeMgnt';
          Ctrl.load(MockTreatyManagementCustomSrv, {});
          expect(Ctrl.CommentsIntPeMgnt).toBeDefined();
        });
        it('should set recap items when entityType is TreatyReferenceRecapMgnt', function () {
          MockItem.entityComments[0].entityType = 'TreatyReferenceRecapMgnt';
          Ctrl.load(MockTreatyManagementCustomSrv, {});
          expect(Ctrl.TreatyReferenceRecapMgnt).toBeDefined();
        });
        it('should set recap comments items when entityType is RecaptureCommentsRecapMgnt', function () {
          MockItem.entityComments[0].entityType = 'RecaptureCommentsRecapMgnt';
          Ctrl.load(MockTreatyManagementCustomSrv, {});
          expect(Ctrl.RecaptureCommentsRecapMgnt).toBeDefined();
        });
      });
    });
  });
});
