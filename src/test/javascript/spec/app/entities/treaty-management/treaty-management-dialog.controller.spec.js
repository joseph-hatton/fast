'use strict';

describe('Controller Tests', function() {
  describe('TreatyManagement Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockTreatyManagement,MockUiModalInstance;
    var MockEntity, MockPreviousState, MockInteractionsService;
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = jasmine.createSpy('MockEntity');
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockTreatyManagement = {
          update: function (obj, success, error) {
            error();
          },
          save: function (obj, success, error) {
            success(obj);
          }
        };
        MockInteractionsService = jasmine.createSpyObj('InteractionsService',
          ['showThenDialog', 'confirmBasicParentNoScrollDialog', 'alertParentScrollFalseDialog', 'showFinallyDialog']);
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        Ctrl = $controller('TreatyManagementDialogController', {
          $scope: scope,
          $state: state, entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          TreatyManagement: MockTreatyManagement,
          InteractionsService: MockInteractionsService
        });
      }));

    describe('Initial state', function () {
      it('TreatyManagementDialog Controller', function() {
        expect(Ctrl).toBeDefined();
      });
    });
    describe('Main Content', function () {
      describe('getAllEntityTypes', function () {
        it('should set the appropriate entity types', function () {
          Ctrl.treatyManagement.inputStatus = "Done";
          Ctrl.treatyManagement.confirmationRequested = 'Maybe';
          Ctrl.treatyManagement.dayTypePosition = undefined;
          Ctrl.treatyManagement.brokerPeriod = undefined;
          Ctrl.treatyManagement.treaty = null;

          Ctrl.getAllEntityTypes({});

          expect(Ctrl.treatyManagement.treaty).toBeDefined();
          expect(Ctrl.treatyManagement.inputStatus).toBe('In Process');
          expect(Ctrl.treatyManagement.confirmationRequested).toBe('No');
          expect(Ctrl.treatyManagement.dayTypePosition).toBe('');
          expect(Ctrl.treatyManagement.brokerPeriod).toBe('');
        });
        it('should set the appropriate entity types when ifs evaluate to false', function () {
          Ctrl.treatyManagement.inputStatus = "Complete";
          Ctrl.treatyManagement.confirmationRequested = 'Yes';
          Ctrl.treatyManagement.dayTypePosition = {};
          Ctrl.treatyManagement.brokerPeriod = {};
          Ctrl.treatyManagement.treaty = null;

          Ctrl.getAllEntityTypes({});

          expect(Ctrl.treatyManagement.treaty).toBeDefined();
          expect(Ctrl.treatyManagement.inputStatus).toBe('Complete');
          expect(Ctrl.treatyManagement.confirmationRequested).toBe('Yes');
          expect(Ctrl.treatyManagement.dayTypePosition).not.toBe('');
          expect(Ctrl.treatyManagement.brokerPeriod).not.toBe('');
        });
      });
      describe('loadAccordionSelected', function () {
        it('should set general to true when hash matches', function () {
          var $stateParams = { hash: 'generalInfo' };
          Ctrl.loadAccordionSelected($stateParams);
          expect(Ctrl.general).toBe(true);
        });
        it('should set recapture to true when hash matches', function () {
          var $stateParams = { hash: 'recaptureInfo' };
          Ctrl.loadAccordionSelected($stateParams);
          expect(Ctrl.recapture).toBe(true);
        });
        it('should set riskFee to true when hash matches', function () {
          var $stateParams = { hash: 'riskFeeInfo' };
          Ctrl.loadAccordionSelected($stateParams);
          expect(Ctrl.riskFee).toBe(true);
        });
        it('should set brokerInfo to true when hash matches', function () {
          var $stateParams = { hash: 'brokerInfo' };
          Ctrl.loadAccordionSelected($stateParams);
          expect(Ctrl.brokerInfo).toBe(true);
        });
        it('should set refund to true when hash matches', function () {
          var $stateParams = { hash: 'refund' };
          Ctrl.loadAccordionSelected($stateParams);
          expect(Ctrl.refund).toBe(true);
        });
      });
      describe('Save', function () {
        describe('onSaveSuccess', function () {
          it('should close the Modal', function () {
            Ctrl.treatyManagement.id = null;
            Ctrl.treaty = {reason:"Replicated"};
            Ctrl.save();
            expect(MockUiModalInstance.close).toHaveBeenCalled();
            expect(Ctrl.isSaving).toBe(false);
          });
        });
        describe('onSaveError', function () {
          it('should not close the Modal', function () {
            Ctrl.treatyManagement.id = 123;
            Ctrl.treatyManagement.treaty = {};
            Ctrl.save();
            expect(MockUiModalInstance.close).not.toHaveBeenCalled();
            expect(Ctrl.isSaving).toBe(false);
          });
        });
      });
      describe('Clear', function () {
        it('to call a confirmation dialog', function () {
          Ctrl.clear();
          expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
        });
      });
      describe('addRiskFee', function () {
        it('should add the current form data to a risk fee then add the risk fee to the list', function () {
          Ctrl.treaty = {};
          Ctrl.addRiskFee();
          Ctrl.addRiskFee();
          expect(Ctrl.treatyManagement.riskFees.length).toBe(2);
        });
      });
      describe('removeRiskFee', function () {
        it('should add the current form data to a risk fee then add the risk fee to the list', function () {
          Ctrl.treatyManagement.riskFees = [{},{},{}];
          Ctrl.removeRiskFee(1);
          expect(Ctrl.treatyManagement.riskFees.length).toBe(2);
        });
      });
      describe('remainInProcess', function () {
        it('should call a alert dialog', function () {
          Ctrl.remainInProcess();
          expect(MockInteractionsService.showFinallyDialog).toHaveBeenCalled();
        });
      });
      describe('movedToComplete', function () {
        it('should call a alert dialog', function () {
          Ctrl.movedToComplete();
          expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
        });
      });
      describe('SaveMessage', function () {
        it('should confirm if inputStatus isn\'t complete', function () {
          Ctrl.treatyManagement.inputStatus = 'In Process';
          Ctrl.saveMessage();
          expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
        });
        it('should not confirm if inputStatus is complete', function () {
          Ctrl.treatyManagement.inputStatus = 'Complete';
          Ctrl.treaty = { reason:"Replicated" };
          Ctrl.saveMessage();
          expect(MockInteractionsService.showThenDialog).not.toHaveBeenCalled();
        });
      });
      describe('calendarizationChange', function () {
        it('should set treatyReference and comments when calendarization is no', function () {
          Ctrl.treatyManagement.calendarization = 'No';
          Ctrl.treatyManagement.calendarizationTreatyRefer = '132';
          Ctrl.treatyManagement.calendarizationComments = 'bad';
          Ctrl.calendarizationChange();
          expect(Ctrl.treatyManagement.calendarizationTreatyRefer).toBe('');
          expect(Ctrl.treatyManagement.calendarizationComments).toBe('');
        });
        it('should not set treatyReference and comments when calendarization is not no', function () {
          Ctrl.treatyManagement.calendarization = 'Yes';
          Ctrl.treatyManagement.calendarizationTreatyRefer = '132';
          Ctrl.treatyManagement.calendarizationComments = 'bad';
          Ctrl.calendarizationChange();
          expect(Ctrl.treatyManagement.calendarizationTreatyRefer).toBe('132');
          expect(Ctrl.treatyManagement.calendarizationComments).toBe('bad');
        });
      });
      describe('refundChange', function () {
        it('should set eliminationReference and comments when refund is no', function () {
          Ctrl.treatyManagement.refundEliminationFlag = 'No';
          Ctrl.treatyManagement.refundEliminationRef = '132';
          Ctrl.treatyManagement.refundComments = 'bad';
          Ctrl.refundChange();
          expect(Ctrl.treatyManagement.refundEliminationRef).toBe('');
          expect(Ctrl.treatyManagement.refundComments).toBe('');
        });
        it('should not set eliminationReference and comments when refund is not no', function () {
          Ctrl.treatyManagement.refundEliminationFlag = 'Yes';
          Ctrl.treatyManagement.refundEliminationRef = '132';
          Ctrl.treatyManagement.refundComments = 'bad';
          Ctrl.refundChange();
          expect(Ctrl.treatyManagement.refundEliminationRef).toBe('132');
          expect(Ctrl.treatyManagement.refundComments).toBe('bad');
        });
      });
      describe('accountingPeriodChange', function () {
        it('should set treaty ref to \'\'', function () {
          Ctrl.treatyManagement.accountingFrequency = false;
          Ctrl.treatyManagement.accountingTreatyReference = "hi";
          Ctrl.accountingPeriodChange();
          expect(Ctrl.treatyManagement.accountingTreatyReference).toBe('');
        });
      });
      describe('quotaShareChange', function () {
        it('should set comment to \'\'', function () {
          Ctrl.treatyManagement.quotaShare = undefined;
          Ctrl.treatyManagement.quotaShareComment = "hi";
          Ctrl.quotaShareChange();
          expect(Ctrl.treatyManagement.quotaShareComment).toBe('');
        });
      });
      describe('changeRecapDate', function () {
        it('should set comment to \'\'', function () {
          Ctrl.treatyManagement.summaryDate = undefined;
          Ctrl.treatyManagement.recaptureComment= "hi";
          Ctrl.changeRecapDate();
          expect(Ctrl.treatyManagement.recaptureComment).toBe('');
        });
      });
      describe('treatyManCustomCallback', function () {
        it('should set the right items', function () {
          var result = {
            riskFees: [{}],
            calendarization: 'No',
            confirmationRequested: 'No'
          };
          Ctrl.treatyManCustomCallback(result);
          expect(Ctrl.band).toBe(1);
          expect(Ctrl.treatyManagement.confirmationRequested).toBe('No');
          expect(Ctrl.treatyManagement.calendarizationTreatyRefer).toBe('');
          expect(Ctrl.treatyManagement.confirmationRequestedComment).toBe('');
        });
      });
      describe('LoadTreatyInfo', function () {
        it('should set the correct info', function () {
          var result = { treatyCurrencies: [
            { currency: 'USD' },
            { currency: 'CAD' }
          ]};
          Ctrl.treatyManagement.refundEliminationFlag = null;
          Ctrl.treatyManagement.calendarization = null;
          Ctrl.loadTreatyInfo(result);
          expect(Ctrl.treatyManagement.refundEliminationFlag).toBe('No');
          expect(Ctrl.treatyManagement.calendarization).toBe('No');
          expect(Ctrl.currencyOptions.length).toBe(2);
        });
      });
    });
  });
});
