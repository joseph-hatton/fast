'use strict';

describe('Service Tests', function () {
  describe('Detail Service', function () {
    var ds = {};
    var MockScope, MockRootScope, MockItem, Mockitem, MockStateParams;
    beforeEach(inject(
      function (_DetailService_, $injector) {     // Don't bother injecting a 'real' modal
        ds.prototype = Object.create(_DetailService_.prototype);
        MockRootScope = $injector.get('$rootScope');
        MockScope = MockRootScope.$new();

        MockItem = {
          get: function (id, callback) {
            callback(id);
          }
        };

        _DetailService_.call(ds, MockScope, MockRootScope, MockStateParams, MockItem, Mockitem, 'Item');
      }));

    describe('Initial State', function () {
      it('Detail Service Should be Created', function () {
        expect(ds).toBeDefined();
      });

      it('Load Should be Defined', function () {
        expect(ds.load).toBeDefined();
      });
    });

    describe('Main Test Coverage', function () {
      it('Load Test', function () {
        var id = 123;
        ds.load(id);
        expect(ds.item.id).toBe(id);
      });
    });
  });
});
