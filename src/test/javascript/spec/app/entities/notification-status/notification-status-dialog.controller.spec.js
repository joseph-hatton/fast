'use strict';

describe('Controller Tests', function() {
  describe('NotificationStatus Dialog Controller', function() {
    var Ctrl;
    var scope,state,MockNotificationStatus,MockUiModalInstance;
    var MockEntity, MockPreviousState;
    var success='Success';
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = jasmine.createSpyObj('MockEntity', ['get', 'update', 'save']);
        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);


        MockNotificationStatus  =
          {
            get: function (id, callback) {
              callback(id);
            },
            update: function(id,success, error){ // for testing purposes, a flag of error will be set to trigger an 'error'
              if(!id.errorTestFlag)
                success(id);
              else
                error();
            },
            save: function (id, success, error) {
              if(!id.errorTestFlag)
                success(id);
              else
                error();
            }
          };

        Ctrl = $controller('NotificationStatusDialogController', {
          $scope: scope,
          entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          NotificationStatus: MockNotificationStatus
        });
      }));

    describe('Initial state', function () {
      it('NotificationStatusDialog Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('Load', function() {
        expect(Ctrl.load).toBeDefined();
      });

      it('Save', function() {
        expect(Ctrl.save).toBeDefined();
      });

      it('Clear', function() {
        expect(Ctrl.clear).toBeDefined();
      });
    });
  });
});
