'use strict';

describe('Controller Tests', function() {

  describe('NotificationStatus  Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockNotificationStatus;
    var info =1;
    var Ctrl;

    beforeEach(inject(function($controller,$injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      MockEntity = jasmine.createSpy('MockEntity');

      MockNotificationStatus  =
        {
          get: function(id,callBack){
            callBack(info);
          }
        };

      Ctrl = $controller('NotificationStatusDetailController', {
        $scope: $scope,
        $rootScope: $rootScope,
        entity: MockEntity ,
        NotificationStatus: MockNotificationStatus
      });

    }));
    describe('Initial State', function () {
      it('AgreementType Detail Controller Should be Created', function () {
        expect(Ctrl).toBeDefined();
      });

      it('Load Should be Defined', function () {
        expect(Ctrl.load).toBeDefined();
      });
    });


  });

});
