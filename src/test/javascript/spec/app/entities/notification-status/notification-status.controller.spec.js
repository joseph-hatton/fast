'use strict';

describe('Controller Tests', function() {

  describe('NotificationStatus Controller', function() {

    var Ctrl;
    var scope;
    var MockState;
    var MockNotificationStatus;
    var MockNotificationStatusSearch;
    var loadAllResult = [{}, {}, {}, {}, {}, {}];
    var searchResult = [{}];

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();

        MockState = jasmine.createSpy('MockState');

        MockNotificationStatus = {
          query: function (callback) {
            callback(loadAllResult);
          }
        };

        MockNotificationStatusSearch = {
          query: function (searchQuery, callback) {
            callback(searchResult);
          }
        };

        Ctrl = $controller('NotificationStatusController', {
          $scope: scope,
          $state: MockState,
          NotificationStatus: MockNotificationStatus,
          NotificationStatusSearch: MockNotificationStatusSearch
        });
      }));

    describe('Initial state', function () {
      it('NotificationStatus Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('Load All Test', function() {
        expect(Ctrl.loadAll).toBeDefined();
      });

      it('Search Test', function() {
        expect(Ctrl.search).toBeDefined();
      });
    });

    describe('Main Content Coverage', function () {
      it('Load All Test', function () {
        expect(Ctrl.notificationStatuses).toBe(loadAllResult);
      });

      it('Search Test No Search Query', function () {
        Ctrl.searchQuery = null;
        Ctrl.search();
        expect(Ctrl.notificationStatuses).toBe(loadAllResult);
      });

      it('Search Test Search Query', function () {
        Ctrl.searchQuery = 1;
        Ctrl.NotificationStatuses = [];
        Ctrl.search();
        expect(Ctrl.notificationStatuses).toBe(searchResult);
      });
    })
  });
});
