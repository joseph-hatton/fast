'use strict';

describe('Controller Tests', function() {

    describe('TreatyAmendments Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockTreatyAmendments, MockTreaty;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockTreatyAmendments = jasmine.createSpy('MockTreatyAmendments');
            MockTreaty = jasmine.createSpy('MockTreaty');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'TreatyAmendments': MockTreatyAmendments,
                'Treaty': MockTreaty
            };
            createController = function() {
                $injector.get('$controller')("TreatyAmendmentsDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'finreApp:treatyAmendmentsUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
