'use strict';

describe('Controller Tests', function() {

  describe('TreatyAmendments Controller', function() {
    var Ctrl;
    var scope,state,paginationConstants,pagingParams, MockTreaty, MockTreatyAmendmentsCustomSrv, MockParseLinks, MockTreatyAmendmentsAll, MockAlertService, MockTreatyAmendmentsSearch, MockLocation;
    var queryResults = [{},{},{}];

// Initialize the controller and a mock scope
    beforeEach(inject(
      function ($controller, $rootScope) {     // Don't bother injecting a 'real' modal
        scope = $rootScope.$new();
        state = jasmine.createSpyObj('state', ['transitionTo', 'go']);
        MockAlertService = jasmine.createSpyObj('AlertService', ['error']);
        pagingParams={ascending:true,page:1,predicate:"id",search:null,sort:"id,asc"};
        paginationConstants= {itemsPerPage: 10};
        MockTreaty = {
          get: function (idObj, callback) {
            callback(idObj);
          }
        };
        MockTreatyAmendmentsCustomSrv = {
          query: function (obj, success, error) {
            success(MockLoadAllQuery, function(str){});
          }
        };
        MockParseLinks = {
          parse: function (msg) {}
        };
        MockTreatyAmendmentsAll = {
          query: function (obj, success, error) {
            success(queryResults, function(){});
          }
        };
        MockTreatyAmendmentsSearch = {
          query: function (obj, success, error) {
            error({data:{message:''}});
          }
        };
        MockLocation = {
          search: function () {
            return {};
          }
        };
        Ctrl = $controller('TreatyAmendmentsController', {
          $location: MockLocation,
          $state: state,
          paginationConstants:paginationConstants,
          pagingParams:pagingParams,
          $stateParams: { dealId:123, treatyId:321 },
          Treaty: MockTreaty,
          ParseLinks: MockParseLinks,
          TreatyAmendmentsAll: MockTreatyAmendmentsAll,
          AlertService: MockAlertService,
          TreatyAmendmentsSearch: MockTreatyAmendmentsSearch,
          $scope: scope
        });
      }));

    describe('Initial state', function () {
      it('TreatyAmendments Controller', function() {
        expect(Ctrl).toBeDefined();
      });

      it('TreatyAmendments Load All Test', function() {
        expect(Ctrl.loadPage).toBeDefined();
      });

      it('TreatyAmendments Search Test', function() {
        expect(Ctrl.search).toBeDefined();
      });
    });
    describe('Main Coverage', function () {
      describe('Load All', function () {
        describe('Treaty.get', function () {
          it('should set the treaty to result', function () {
            expect(Ctrl.treaty.id).toBe(321);
          });
        });
        describe('OnSuccess', function () {
          it('should save the result in data in totalItems', function () {
            expect(Ctrl.treatyAmendments.length).toBe(queryResults.length);
          });
        });
      });
      describe('deleteAmendment', function () {
        it('should never allow the user to delete an amendment if they aren\'t an admin', function () {
          Ctrl.actualUser = 'User';
          Ctrl.treaty = { treatyStatus: { id:1 } };

          Ctrl.deleteAmendment();

          expect(state.go).not.toHaveBeenCalled();
        });
        it('should never allow the user to delete an amendment if they aren\'t an admin', function () {
          Ctrl.actualUser = 'User';
          Ctrl.treaty = { treatyStatus: { id:4 } };

          Ctrl.deleteAmendment();

          expect(state.go).not.toHaveBeenCalled();
        });
        it('should never allow the user to delete an amendment if the treaty status is terminated', function () {
          Ctrl.actualUser = 'Admin';
          Ctrl.treaty = { treatyStatus: { id:4 } };

          Ctrl.deleteAmendment();

          expect(state.go).not.toHaveBeenCalled();
        });
        it('should allow the user to delete an amendment if the treaty status is not terminated', function () {
          Ctrl.actualUser = 'Admin';
          Ctrl.treaty = { treatyStatus: { id:3 } };

          Ctrl.deleteAmendment();

          expect(state.go).toHaveBeenCalled();
        });
      });
    });
  });
});
