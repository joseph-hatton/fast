'use strict';

describe('Controller Tests', function() {
  describe('TreatyAmendments Dialog Controller', function() {
    var Ctrl, q, loadAllDefer, $rootScope,MockPrincipal;
    var scope,state,MockTreatyAmendments,MockUiModalInstance, MockTreaty;
    var MockEntity, MockPreviousState, MockNotificationStatus, MockInteractionsService;
    var NotificationQueryResult = [{ name: 'Notified' }, { name: 'Confirmed' }];
    beforeEach(inject(
      function ($controller,$q, $injector) {     // Don't bother injecting a 'real' modal
        q = $q;
        $rootScope = $injector.get('$rootScope');
        loadAllDefer = getDeferForResource();
        scope = $rootScope.$new();
        state =$rootScope.$state;
        MockEntity = jasmine.createSpy('MockEntity');
        MockPrincipal = jasmine.createSpy('Principal');

        MockPreviousState = jasmine.createSpy('MockPreviousState');
        MockUiModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
        MockInteractionsService = jasmine.createSpyObj('InteractionsService', ['confirmBasicDialog', 'showThenDialog']);
        MockTreaty = jasmine.createSpyObj('Treaty', ['update', 'get']);

        MockTreaty.update.and.callFake(function () {
          return loadAllDefer;
        });

        MockPrincipal = jasmine.createSpyObj('Principal', ['identity']);

        MockPrincipal.identity.and.callFake(function () {
          return getDeferForResource().$promise;
        });



        MockNotificationStatus = {
          query: function (callback) {
            callback(NotificationQueryResult);
          }
        };



        MockTreatyAmendments = {
          get: function(idObj, callback){
            callback({});
          },
          update: function (treatyAmendments, success, fail) {
            success(treatyAmendments);
          },
          save: function (treatyAmendments, success, fail) {
            fail();
          }
        };
        Ctrl = $controller('TreatyAmendmentsDialogController', {
          $scope: scope,
          $rootScope: $rootScope,
          $state: state, entity: MockEntity,
          $uibModalInstance: MockUiModalInstance,
          Principal:MockPrincipal,
          NotificationStatus: MockNotificationStatus,
          InteractionsService: MockInteractionsService,
          Treaty: MockTreaty,
          TreatyAmendments: MockTreatyAmendments
        });
      }));

    function getDeferForResource() {
      var i = q.defer();
      i.$promise = i.promise;
      return i;
    }


    describe('Initial state', function () {
      it('TreatyAmendmentsDialog Controller', function() {
        expect(Ctrl).toBeDefined();
      });
    });
    describe('Main Coverage', function () {
      describe('NotificationStatus.query', function () {
        it('should store the Notified Notifications and Confirmed Notifications', function () {
          expect(Ctrl.notificationNotified.name).toBe('Notified');
          expect(Ctrl.notificationConfirmed.name).toBe('Confirmed');
        });
      });
      describe('Save', function () {
        describe('OnSaveSuccess', function () {
          it('should call update when id is set', function () {
            Ctrl.treatyAmendments = {
              id: 123,
              treaty: {
                id: 123,
                treatyStatus: { name:'Active' }
              }
            };

            loadAllDefer.resolve(Ctrl.treatyAmendments);
            $rootScope.$apply();

            Ctrl.save();
            expect(MockTreaty.update).toHaveBeenCalled();
            expect(Ctrl.treatyAmendments.treaty.treatyStatus.name).toBe('Amended');
            expect(Ctrl.isSaving).toBe(false);

          });
        it('should call save when id is not set', function () {
         Ctrl.treatyAmendments = {
             id: null
            };

          loadAllDefer.resolve(Ctrl.treatyAmendments);
          $rootScope.$apply();
           Ctrl.save();
           expect(MockTreaty.update).not.toHaveBeenCalled();
           expect(Ctrl.isSaving).toBe(false);
          });
        });
      });
      describe('Clear', function () {
        it('should call $uibModalInstance.dismiss', function () {
          Ctrl.clear();
          expect(MockUiModalInstance.dismiss).toHaveBeenCalled();
        });
      });
      describe('Clearx', function () {
        it('should call confirm and show on InteractionsService', function () {
          Ctrl.clearx();
          expect(MockInteractionsService.confirmBasicDialog).toHaveBeenCalled();
          expect(MockInteractionsService.showThenDialog).toHaveBeenCalled();
        });
      });
      describe('OpenCalendar', function () {
        it('should set datePickerOpenStatus[date] to true', function () {
          var date = 'date';
          Ctrl.datePickerOpenStatus[date] = false;
          Ctrl.openCalendar(date);
          expect(Ctrl.datePickerOpenStatus[date]).toBe(true);
        });
      });
    });
  });
});
