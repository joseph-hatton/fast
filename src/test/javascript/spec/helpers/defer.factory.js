(function() {
  'use strict';
  angular
    .module('finreApp')
    .factory('DeferFactory', DeferFactory);

  DeferFactory.$inject = ['$q'];

  function DeferFactory ($q) {
    var factory = {};

    factory.generate = function () {
      var i = $q.defer();
      i.$promise = i.promise;
      return i;
    };

    return factory;
  }
})();
