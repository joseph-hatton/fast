package com.rgare.global.finre.repository;


import com.rgare.global.finre.domain.AuditRiskManagement;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuditRiskManagementRepositoryUnitTest {

  private static final Long ID = 1L;

  @Mock
  private AuditRiskManagementRepository  mockAuditRiskManagementRepository;

  private AuditRiskManagement auditRiskManagement;
  private List<AuditRiskManagement> expectedResult;

  @Before
  public void setup() {
    expectedResult = new ArrayList<AuditRiskManagement>();
    auditRiskManagement = new AuditRiskManagement();
    auditRiskManagement.setId(ID);
    auditRiskManagement.setAction("Action");
    auditRiskManagement.setComments("Comments");
    expectedResult.add(auditRiskManagement);

  }

  @Test
  public void save() {
    AuditRiskManagement savedAgreementType = new AuditRiskManagement();
    auditRiskManagement.setAction("Action");
    auditRiskManagement.setComments("Comments");
    when(mockAuditRiskManagementRepository.save(auditRiskManagement)).thenReturn(savedAgreementType);
    mockAuditRiskManagementRepository.save(savedAgreementType);

    verify(mockAuditRiskManagementRepository, times(1)).save(savedAgreementType);
  }

  @Test
  public void findAll() {
    when(mockAuditRiskManagementRepository.findAll()).thenReturn(expectedResult);

    List<AuditRiskManagement> actual = mockAuditRiskManagementRepository.findAll();

    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void findOne() {
    when(mockAuditRiskManagementRepository.findOne(ID)).thenReturn(auditRiskManagement);

    AuditRiskManagement actual = mockAuditRiskManagementRepository.findOne(ID);

    assertThat(actual).isEqualTo(auditRiskManagement);
  }

  @Test
  public void delete() {
    mockAuditRiskManagementRepository.delete(ID);

    verify(mockAuditRiskManagementRepository, times(1)).delete(ID);
  }

}
