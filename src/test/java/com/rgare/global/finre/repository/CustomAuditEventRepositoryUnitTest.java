package com.rgare.global.finre.repository;


import com.rgare.global.finre.domain.PersistentAuditEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomAuditEventRepositoryUnitTest {

  private static final Long ID = 1L;
  private static final String NAME = "NAME";
  private static final String ACTON = "ACTION";


  @Mock
  private PersistenceAuditEventRepository  mockPersistenceAuditEventRepository;

  private PersistentAuditEvent customAuditEvent;

  private List<AuditEvent> expectedResult;

  private List<PersistentAuditEvent> list;
  private PageImpl<PersistentAuditEvent> trackingPage;

  private Pageable pageable;

  @Before
  public void setup() {
    customAuditEvent = new PersistentAuditEvent();

    list = Arrays.asList(customAuditEvent);

    trackingPage = new PageImpl<>(list);


    pageable = new PageRequest(1, 5);


  }

  @Test
  public void findByAuditEventDateAfter() {
    AuditEvent savedAgreementType = new AuditEvent(NAME,ACTON);
    when(mockPersistenceAuditEventRepository.findByAuditEventDateAfter(LocalDateTime.now())).thenReturn(list);

//    verify(mockPersistenceAuditEventRepository, times(1)).findByAuditEventDateAfter(LocalDateTime.now());
  }
//
//  @Test
//  public void findAll() {
//    when(mockCustomAuditEventRepository.findAll()).thenReturn(expectedResult);
//
//    List<CustomAuditEvent> actual = mockCustomAuditEventRepository.findAll();
//
//    assertThat(actual).isEqualTo(expectedResult);
//  }
//
//  @Test
//  public void findOne() {
//    when(mockCustomAuditEventRepository.findOne(ID)).thenReturn(customAuditEvent);
//
//    CustomAuditEvent actual = mockCustomAuditEventRepository.findOne(ID);
//
//    assertThat(actual).isEqualTo(customAuditEvent);
//  }
//
//  @Test
//  public void delete() {
//    mockCustomAuditEventRepository.delete(ID);
//
//    verify(mockCustomAuditEventRepository, times(1)).delete(ID);
//  }

}
