package com.rgare.global.finre.repository;


import com.rgare.global.finre.domain.AuditFinanceEmail;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuditFinanceEmailRepositoryUnitTest {

  private static final Long ID = 1L;

  @Mock
  private AuditFinanceEmailRepository  mockAuditFinanceEmailRepository;

  private AuditFinanceEmail auditFinanceEmail;
  private List<AuditFinanceEmail> expectedResult;

  @Before
  public void setup() {
    expectedResult = new ArrayList<AuditFinanceEmail>();
    auditFinanceEmail = new AuditFinanceEmail();
    auditFinanceEmail.setId(ID);
    auditFinanceEmail.setComments("comments");
    expectedResult.add(auditFinanceEmail);

  }

  @Test
  public void save() {
    AuditFinanceEmail savedAgreementType = new AuditFinanceEmail();
    savedAgreementType.setComments("saved risk criteria");
    when(mockAuditFinanceEmailRepository.save(auditFinanceEmail)).thenReturn(savedAgreementType);
    mockAuditFinanceEmailRepository.save(savedAgreementType);

    verify(mockAuditFinanceEmailRepository, times(1)).save(savedAgreementType);
  }

  @Test
  public void findAll() {
    when(mockAuditFinanceEmailRepository.findAll()).thenReturn(expectedResult);

    List<AuditFinanceEmail> actual = mockAuditFinanceEmailRepository.findAll();

    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void findOne() {
    when(mockAuditFinanceEmailRepository.findOne(ID)).thenReturn(auditFinanceEmail);

    AuditFinanceEmail actual = mockAuditFinanceEmailRepository.findOne(ID);

    assertThat(actual).isEqualTo(auditFinanceEmail);
  }

  @Test
  public void delete() {
    mockAuditFinanceEmailRepository.delete(ID);

    verify(mockAuditFinanceEmailRepository, times(1)).delete(ID);
  }

}
