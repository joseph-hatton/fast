package com.rgare.global.finre.repository;


import com.rgare.global.finre.domain.AgreementType;
import com.rgare.global.finre.repository.search.AgreementTypeSearchRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AgreementTypeRepositoryUnitTest {

  private static final Long ID = 1L;

  @Mock
  private AgreementTypeRepository  mockAgreementTypeRepository;

  @Mock
  private AgreementTypeSearchRepository mockAgreementTypeSearchRepository;

  private AgreementType agreementType;
  private List<AgreementType> expectedResult;

  @Before
  public void setup() {
    expectedResult = new ArrayList<AgreementType>();
    agreementType = new AgreementType();
    agreementType.setId(ID);
    agreementType.setDescription("description");
    agreementType.setName("name");
    expectedResult.add(agreementType);

  }

  @Test
  public void save() {
    AgreementType savedAgreementType = new AgreementType();
    savedAgreementType.setName("saved risk criteria");
    when(mockAgreementTypeRepository.save(agreementType)).thenReturn(savedAgreementType);
    mockAgreementTypeRepository.save(savedAgreementType);

    verify(mockAgreementTypeRepository, times(1)).save(savedAgreementType);
  }

  @Test
  public void findAll() {
    when(mockAgreementTypeRepository.findAll()).thenReturn(expectedResult);

    List<AgreementType> actual = mockAgreementTypeRepository.findAll();

    assertThat(actual).isEqualTo(expectedResult);
    verifyZeroInteractions(mockAgreementTypeSearchRepository);
  }

  @Test
  public void findOne() {
    when(mockAgreementTypeRepository.findOne(ID)).thenReturn(agreementType);

    AgreementType actual = mockAgreementTypeRepository.findOne(ID);

    assertThat(actual).isEqualTo(agreementType);
    verifyZeroInteractions(mockAgreementTypeSearchRepository);
  }

  @Test
  public void delete() {
    mockAgreementTypeRepository.delete(ID);

    verify(mockAgreementTypeRepository, times(1)).delete(ID);
    verifyZeroInteractions(mockAgreementTypeSearchRepository);
  }

}
