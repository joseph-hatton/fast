package com.rgare.global.finre.repository;


import com.rgare.global.finre.domain.AllocationType;
import com.rgare.global.finre.repository.search.AllocationTypeSearchRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AllocationTypeRepositoryUnitTest {

  private static final Long ID = 1L;

  @Mock
  private AllocationTypeRepository  mockAllocationTypeRepository;

  @Mock
  private AllocationTypeSearchRepository mockAllocationTypeSearchRepository;

  private AllocationType allocationType;
  private List<AllocationType> expectedResult;

  @Before
  public void setup() {
    expectedResult = new ArrayList<AllocationType>();
    allocationType = new AllocationType();
    allocationType.setId(ID);
    allocationType.setDescription("description");
    allocationType.setName("name");
    expectedResult.add(allocationType);

  }

  @Test
  public void save() {
    AllocationType savedAgreementType = new AllocationType();
    savedAgreementType.setName("saved risk criteria");
    when(mockAllocationTypeRepository.save(allocationType)).thenReturn(savedAgreementType);
    mockAllocationTypeRepository.save(savedAgreementType);

    verify(mockAllocationTypeRepository, times(1)).save(savedAgreementType);
  }

  @Test
  public void findAll() {
    when(mockAllocationTypeRepository.findAll()).thenReturn(expectedResult);

    List<AllocationType> actual = mockAllocationTypeRepository.findAll();

    assertThat(actual).isEqualTo(expectedResult);
    verifyZeroInteractions(mockAllocationTypeSearchRepository);
  }

  @Test
  public void findOne() {
    when(mockAllocationTypeRepository.findOne(ID)).thenReturn(allocationType);

    AllocationType actual = mockAllocationTypeRepository.findOne(ID);

    assertThat(actual).isEqualTo(allocationType);
    verifyZeroInteractions(mockAllocationTypeSearchRepository);
  }

  @Test
  public void delete() {
    mockAllocationTypeRepository.delete(ID);

    verify(mockAllocationTypeRepository, times(1)).delete(ID);
    verifyZeroInteractions(mockAllocationTypeSearchRepository);
  }

}
