package com.rgare.global.finre.repository;


import com.rgare.global.finre.domain.AuditRiskFee;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuditRiskFeeRepositoryUnitTest {

  private static final Long ID = 1L;

  @Mock
  private AuditRiskFeeRepository  mockAuditRiskFeeRepository;

  private AuditRiskFee auditRiskFee;
  private List<AuditRiskFee> expectedResult;

  @Before
  public void setup() {
    expectedResult = new ArrayList<AuditRiskFee>();
    auditRiskFee = new AuditRiskFee();
    auditRiskFee.setId(ID);
    auditRiskFee.setAction("Action");
    auditRiskFee.setComments("Comments");
    expectedResult.add(auditRiskFee);

  }

  @Test
  public void save() {
    AuditRiskFee savedAgreementType = new AuditRiskFee();
    auditRiskFee.setAction("Action");
    auditRiskFee.setComments("Comments");
    when(mockAuditRiskFeeRepository.save(auditRiskFee)).thenReturn(savedAgreementType);
    mockAuditRiskFeeRepository.save(savedAgreementType);

    verify(mockAuditRiskFeeRepository, times(1)).save(savedAgreementType);
  }

  @Test
  public void findAll() {
    when(mockAuditRiskFeeRepository.findAll()).thenReturn(expectedResult);

    List<AuditRiskFee> actual = mockAuditRiskFeeRepository.findAll();

    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void findOne() {
    when(mockAuditRiskFeeRepository.findOne(ID)).thenReturn(auditRiskFee);

    AuditRiskFee actual = mockAuditRiskFeeRepository.findOne(ID);

    assertThat(actual).isEqualTo(auditRiskFee);
  }

  @Test
  public void delete() {
    mockAuditRiskFeeRepository.delete(ID);

    verify(mockAuditRiskFeeRepository, times(1)).delete(ID);
  }

}
