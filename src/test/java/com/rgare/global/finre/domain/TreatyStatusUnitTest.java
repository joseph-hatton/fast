package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class TreatyStatusUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";
  private static final String TREATY_STATUS_CODE = "CODE";

  private TreatyStatus treatyStatus;

  String expectedResult = "TreatyStatus{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    ", treatyStatusCode='" + TREATY_STATUS_CODE + "'" +
    '}';

  @Before
  public void setup() {
    treatyStatus = new TreatyStatus();
    treatyStatus.setId(ID);
    treatyStatus.setName(NAME);
    treatyStatus.setDescription(DESCRIPTION);
    treatyStatus.setTreatyStatusCode(TREATY_STATUS_CODE);
  }

  @Test
  public void equalsGetTest() {
    Long id = treatyStatus.getId();
    String name = treatyStatus.getName();
    String description = treatyStatus.getDescription();
    String treatyStatusCode = treatyStatus.getTreatyStatusCode();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
    assertThat(treatyStatusCode).isEqualTo(TREATY_STATUS_CODE);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = treatyStatus.equals(treatyStatus);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = treatyStatus.equals(new TreatyStatus());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    TreatyStatus treatyStatus1 = new TreatyStatus();
    treatyStatus1.setId(1L);
    boolean same = treatyStatus.equals(treatyStatus1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    TreatyStatus treatyStatus1 = new TreatyStatus();
    treatyStatus1.setId(null);
    boolean same = treatyStatus.equals(treatyStatus1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    TreatyStatus treatyStatus1 = new TreatyStatus();
    treatyStatus1.setId(ID);
    treatyStatus.setId(null);
    boolean same = treatyStatus.equals(treatyStatus1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = treatyStatus.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = treatyStatus.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = treatyStatus.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = treatyStatus.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
