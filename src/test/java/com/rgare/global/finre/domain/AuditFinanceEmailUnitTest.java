package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuditFinanceEmailUnitTest {

  private static final Long ID = 1L;
  private static final Long TREATY_ID = 1L;
  private static final Long RISK_FEE_ID = 1L;
  private static final String TREATY_STATUS = "STATUS";
  private static final String ACTION = "ACTION";
  private static final String COMMENTS = "COMMENTS";
  private static final String CREATION_USER = "USER";
  private static final String MGMT_INPUT_STATUS = "STATUS";

  LocalDateTime ldt = LocalDateTime.of(2017, Month.JULY, 19, 16, 30);
  private  ZonedDateTime ZONED_DATE_TIME = ldt.atZone(ZoneId.of("Europe/Paris"));
  private AuditFinanceEmail auditFinanceEmail;

  String expectedResult = "AuditFinanceEmail{" +
    "id=" + ID +
      ", comments='" + COMMENTS + "'" +
      ", creationUser='" + CREATION_USER + "'" +
      ", creationTs='" + ZONED_DATE_TIME + "'" +
      '}';

  @Before
  public void setup() {

    auditFinanceEmail = new AuditFinanceEmail();
    auditFinanceEmail.setId(ID);
    auditFinanceEmail.setComments(COMMENTS);
    auditFinanceEmail.setCreationUser(CREATION_USER);
    auditFinanceEmail.setCreationTs(ZONED_DATE_TIME);
  }

  @Test
  public void equalsGetTest() {
    Long id = auditFinanceEmail.getId();
    String comments = auditFinanceEmail.getComments();
    String createUser = auditFinanceEmail.getCreationUser();
    ZonedDateTime zonedDateTime = auditFinanceEmail.getCreationTs();
    assertThat(id).isEqualTo(ID);
    assertThat(comments).isEqualTo(COMMENTS);
    assertThat(createUser).isEqualTo(CREATION_USER);
    assertThat(zonedDateTime).isEqualTo(ZONED_DATE_TIME);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = auditFinanceEmail.equals(auditFinanceEmail);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = auditFinanceEmail.equals(new AuditFinanceEmail());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    AuditFinanceEmail auditFinanceEmail1 = new AuditFinanceEmail();
    auditFinanceEmail1.setId(1L);
    boolean same = auditFinanceEmail.equals(auditFinanceEmail1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    AuditFinanceEmail auditFinanceEmail1 = new AuditFinanceEmail();
    auditFinanceEmail1.setId(null);
    boolean same = auditFinanceEmail.equals(auditFinanceEmail1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdIsNullAnotherTest() {
    AuditFinanceEmail auditFinanceEmail1 = new AuditFinanceEmail();
    auditFinanceEmail1.setId(ID);
    auditFinanceEmail.setId(null);
    boolean same = auditFinanceEmail.equals(auditFinanceEmail1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = auditFinanceEmail.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = auditFinanceEmail.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = auditFinanceEmail.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = auditFinanceEmail.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
