package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class PersistentTokenUnitTest {

  private static final String ID = "1";

  private static final String TOKEN_VALUE = "TOKEN";
  private static final LocalDate TOKEN_DATE = LocalDate.now();
  private static final String IP_ADDRESS = "ADDRESS";
  private static final String USER_AGENT = "USER_AGENT";

  private static final String USER_AGENT_MAX = "USER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENTUSER_AGENT";

  private static final User USER = new User();

  private PersistentToken persistentToken;

  String expectedResult = "PersistentToken{" +
    "series='" + ID + '\'' +
    ", tokenValue='" + TOKEN_VALUE + '\'' +
    ", tokenDate=" + TOKEN_DATE +
    ", ipAddress='" + IP_ADDRESS + '\'' +
    ", userAgent='" + USER_AGENT + '\'' +
    "}";

  @Before
  public void setup() {

    persistentToken = new PersistentToken();
    persistentToken.setSeries(ID);
    persistentToken.setTokenValue(TOKEN_VALUE);
    persistentToken.setTokenDate(TOKEN_DATE);
    persistentToken.setIpAddress(IP_ADDRESS);
    persistentToken.setUserAgent(USER_AGENT_MAX);
    persistentToken.setUserAgent(USER_AGENT);
    persistentToken.setUser(USER);
  }

  @Test
  public void equalsGetTest() {
    String id = persistentToken.getSeries();
    String ipAddress = persistentToken.getIpAddress();
    String tokenValue = persistentToken.getTokenValue();
    LocalDate tokenDate = persistentToken.getTokenDate();
    String userAgent = persistentToken.getUserAgent();
    User user = persistentToken.getUser();
    assertThat(id).isEqualTo(ID);
    assertThat(ipAddress).isEqualTo(IP_ADDRESS);
    assertThat(tokenValue).isEqualTo(TOKEN_VALUE);
    assertThat(tokenDate).isEqualTo(TOKEN_DATE);
    assertThat(userAgent).isEqualTo(USER_AGENT);
    assertThat(user).isEqualTo(USER);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = persistentToken.equals(persistentToken);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = persistentToken.equals(new PersistentToken());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideNotEqualsSeriesTest() {
    persistentToken.setSeries("2");
    PersistentToken thisPersistent = new PersistentToken();
    thisPersistent.setSeries("2");
    boolean same = persistentToken.equals(thisPersistent);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = persistentToken.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareObjectsTest() {
    boolean same = persistentToken.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = persistentToken.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = persistentToken.hashCode();

    assertThat(actual).isEqualTo(ID.hashCode());
  }


}
