package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Calendar;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class DealUnitTest {

  private static final Long ID = 1L;
  private static final Long MDM_ID = 1L;

  private static final String NAME = "NAME";
  private static final String DEAL_STATUS = "STATUS";
  private static final String CLIENT_DEAL_NAME = "DEAL_NAME";
  private static final LocalDate TERMINATED_DATE = LocalDate.now();
  private static final LocalDate EFFECTIVE_DATE = LocalDate.now();
  private static final LocalDate MATURITY_DATE = LocalDate.now();
  private static final String COMMENTS = "COMMENTS";
  private static final String US_TRANSFTER_RISK = "US_TRANSFER_RIST";

  private static final Integer IS_ACTIVE = 1;
  private static final Integer YEAR_END_DAY = 1;
  private static final Integer YEAR_END_MONTH = 1;

  private static final EucUse EUC_USE = new EucUse();
  private static final EucUse EUC_COMPLEXITY = new EucUse();
  private static final EucUse EUC_RISK = new EucUse();
  private static final EucUse EUC_SIGNIFICANCE = new EucUse();

  private static final GfsAssignee GFS_ASSIGNEE = new GfsAssignee();
  private static final ProductLine PRODUCT_LINE = new ProductLine();
  private static final ProductSubtype PRODUCT_SUBTYPE = new ProductSubtype();
  private static final Country COUNTRY = new Country();

  private Deal deal;

  String expectedResult = "Deal{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", usStatRisk='" + US_TRANSFTER_RISK + "'" +
    ", clientDealName='" + CLIENT_DEAL_NAME + "'" +
    '}';

  @Before
  public void setup() {

    deal = new Deal();
    deal.setId(ID);
    deal.setName(null);
    deal.setName(NAME);
    deal.setClientDealName(CLIENT_DEAL_NAME);
    deal.setDealStatus(DEAL_STATUS);
    deal.setTerminatedDate(TERMINATED_DATE);
    deal.setEffectiveDate(EFFECTIVE_DATE);
    deal.setYearEndDay(YEAR_END_DAY);
    deal.setYearEndMonth(YEAR_END_MONTH);
    deal.setMaturityDate(MATURITY_DATE);
    deal.setComments(COMMENTS);
    deal.setIsActive(IS_ACTIVE);
    deal.setUsStatRisk(US_TRANSFTER_RISK);
    deal.setMdmId(MDM_ID);
    deal.setEucUse(EUC_USE);
    deal.setEucComplexity(EUC_COMPLEXITY);
    deal.setEucRisk(EUC_RISK);
    deal.setEucSignificance(EUC_SIGNIFICANCE);
    deal.setGfsAssignee(GFS_ASSIGNEE);
    deal.setProductLine(PRODUCT_LINE);
    deal.setProductSubtype(PRODUCT_SUBTYPE);
    deal.setCountry(COUNTRY);
  }

  @Test
  public void equalsGetTest() {
    Long id = deal.getId();
    String name = deal.getName();
    String clientDealName = deal.getClientDealName();
    String dealStatus = deal.getDealStatus();
    LocalDate terminatedDate = deal.getTerminatedDate();
    LocalDate effectiveDate = deal.getEffectiveDate();
    LocalDate maturityDate = deal.getMaturityDate();
    Integer yearEndDay = deal.getYearEndDay();
    Integer yearEndMonth =deal.getYearEndMonth();
    String comments = deal.getComments();
    Integer isActive = deal.getIsActive();
    String usStatRisk = deal.getUsStatRisk();
    Long mdmId = deal.getMdmId();
    EucUse eucUse = deal.getEucUse();
    EucUse eucComplexity = deal.getEucComplexity();
    EucUse eucRisk = deal.getEucRisk();
    EucUse eucSignificance = deal.getEucSignificance();
    GfsAssignee gfsAssignee = deal.getGfsAssignee();
    ProductLine productLine = deal.getProductLine();
    ProductSubtype productSubtype = deal.getProductSubtype();
    Country country = deal.getCountry();

    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(clientDealName).isEqualTo(CLIENT_DEAL_NAME);
    assertThat(dealStatus).isEqualTo(DEAL_STATUS);
    assertThat(terminatedDate).isEqualTo(TERMINATED_DATE);
    assertThat(effectiveDate).isEqualTo(EFFECTIVE_DATE);
    assertThat(maturityDate).isEqualTo(MATURITY_DATE);
    assertThat(yearEndDay).isEqualTo(YEAR_END_DAY);
    assertThat(yearEndMonth).isEqualTo(YEAR_END_MONTH);
    assertThat(comments).isEqualTo(COMMENTS);
    assertThat(isActive).isEqualTo(IS_ACTIVE);
    assertThat(usStatRisk).isEqualTo(US_TRANSFTER_RISK);
    assertThat(mdmId).isEqualTo(MDM_ID);
    assertThat(eucUse).isEqualTo(EUC_USE);
    assertThat(eucComplexity).isEqualTo(EUC_COMPLEXITY);
    assertThat(eucRisk).isEqualTo(EUC_RISK);
    assertThat(eucSignificance).isEqualTo(EUC_SIGNIFICANCE);
    assertThat(gfsAssignee).isEqualTo(GFS_ASSIGNEE);
    assertThat(productLine).isEqualTo(PRODUCT_LINE);
    assertThat(productSubtype).isEqualTo(PRODUCT_SUBTYPE);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = deal.equals(deal);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = deal.equals(new Deal());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    Deal deal1 = new Deal();
    deal1.setId(1L);
    boolean same = deal.equals(deal1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    Deal deal1 = new Deal();
    deal1.setId(null);
    boolean same = deal.equals(deal1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    Deal deal1 = new Deal();
    deal1.setId(ID);
    deal.setId(null);
    boolean same = deal.equals(deal1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = deal.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = deal.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = deal.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = deal.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
