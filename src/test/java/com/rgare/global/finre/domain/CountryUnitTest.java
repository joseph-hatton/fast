package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class CountryUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String REGION = "REGION";
  private static final String DESCRIPTION = "DESCRIPTION";

  private Country country;

  String expectedResult = "Country{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", region='" + REGION + "'" +
    ", description='" + DESCRIPTION + "'" +
    '}';

  @Before
  public void setup() {

    country = new Country();
    country.setId(ID);
    country.setRegion(REGION);
    country.setDescription(DESCRIPTION);
    country.setName(NAME);

  }

  @Test
  public void equalsGetTest() {
    Long id = country.getId();
    String name = country.getName();
    String description = country.getDescription();
    String region = country.getRegion();

    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
    assertThat(region).isEqualTo(REGION);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = country.equals(country);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = country.equals(new Country());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    Country country1 = new Country();
    country1.setId(1L);
    boolean same = country.equals(country1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    Country country1 = new Country();
    country1.setId(null);
    boolean same = country.equals(country1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    Country country1 = new Country();
    country1.setId(ID);
    country.setId(null);
    boolean same = country.equals(country1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = country.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = country.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = country.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = country.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
