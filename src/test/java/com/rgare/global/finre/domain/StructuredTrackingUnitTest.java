package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class StructuredTrackingUnitTest {

  private static final Long ID = 1L;

  private static final Integer IS_ACTIVE = 1;
  private static final LocalDate CLIENT_REPORTING_DATE = LocalDate.now();
  private static final LocalDate CALC_REPORTING_DUE_DATE = LocalDate.now();
  private static final LocalDate CALC_REPORTING_DUE_YEX = LocalDate.now();
  private static final LocalDate REPORTING_RECEIPT_DATE = LocalDate.now();
  private static final Integer AUTO_GENERATED_FLAG  = 1;
  private static final String COMMENTS = "COMMENTS";
  private static final String TRACKING_STATUS = "STATUS";

  private static final Deal DEAL = new Deal();
  private static final StructuredFinance STRUCTURED_FINANCE = new StructuredFinance();

  private StructuredTracking structuredTracking;

  String expectedResult = "StructuredTracking{" +
    "id=" + ID +
    ", clientReportingDate='" + CLIENT_REPORTING_DATE + "'" +
    ", reportingReceiptDate='" + REPORTING_RECEIPT_DATE + "'" +
    ", trackingStatus='" + TRACKING_STATUS + "'" +
    ", comments='" + COMMENTS + "'" +
    '}';

  @Before
  public void setup() {
    structuredTracking = new StructuredTracking();
    structuredTracking.setId(ID);
    structuredTracking.setIsActive(IS_ACTIVE);
    structuredTracking.setClientReportingDate(CLIENT_REPORTING_DATE);
    structuredTracking.setCalcReportingDueDate(CALC_REPORTING_DUE_DATE);
    structuredTracking.setCalcReportingDueYex(CALC_REPORTING_DUE_YEX);
    structuredTracking.setReportingReceiptDate(REPORTING_RECEIPT_DATE);
    structuredTracking.setAutoGenerateFlag(AUTO_GENERATED_FLAG);
    structuredTracking.setTrackingStatus(TRACKING_STATUS);
    structuredTracking.setComments(COMMENTS);
    structuredTracking.setDeal(DEAL);
    structuredTracking.setStructuredFinance(STRUCTURED_FINANCE);
  }

  @Test
  public void equalsGetTest() {
    Long id = structuredTracking.getId();
    Integer isActive = structuredTracking.getIsActive();
    LocalDate clientReportingDate = structuredTracking.getClientReportingDate();
    LocalDate calcReportingDueDate = structuredTracking.getCalcReportingDueDate();
    LocalDate calcReportingDueYex = structuredTracking.getCalcReportingDueYex();
    LocalDate reportingReceiptDate = structuredTracking.getReportingReceiptDate();
    Integer autoGenerateFlag = structuredTracking.getAutoGenerateFlag();
    String trackingStatus = structuredTracking.getTrackingStatus();
    String comments = structuredTracking.getComments();
    Deal deal = structuredTracking.getDeal();
    StructuredFinance structuredFinance =  structuredTracking.getStructuredFinance();

    assertThat(id).isEqualTo(ID);
    assertThat(isActive).isEqualTo(IS_ACTIVE);
    assertThat(clientReportingDate).isEqualTo(CLIENT_REPORTING_DATE);
    assertThat(calcReportingDueDate).isEqualTo(CALC_REPORTING_DUE_DATE);
    assertThat(calcReportingDueYex).isEqualTo(CALC_REPORTING_DUE_YEX);
    assertThat(reportingReceiptDate).isEqualTo(REPORTING_RECEIPT_DATE);
    assertThat(autoGenerateFlag).isEqualTo(AUTO_GENERATED_FLAG);
    assertThat(trackingStatus).isEqualTo(TRACKING_STATUS);
    assertThat(comments).isEqualTo(COMMENTS);
    assertThat(deal).isEqualTo(DEAL);
    assertThat(structuredFinance).isEqualTo(STRUCTURED_FINANCE);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = structuredTracking.equals(structuredTracking);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = structuredTracking.equals(new StructuredTracking());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    StructuredTracking structuredTracking1 = new StructuredTracking();
    structuredTracking1.setId(1L);
    boolean same = structuredTracking.equals(structuredTracking1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    StructuredTracking structuredTracking1 = new StructuredTracking();
    structuredTracking1.setId(null);
    boolean same = structuredTracking.equals(structuredTracking1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    StructuredTracking structuredTracking1 = new StructuredTracking();
    structuredTracking1.setId(ID);
    structuredTracking.setId(null);
    boolean same = structuredTracking.equals(structuredTracking1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = structuredTracking.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = structuredTracking.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = structuredTracking.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = structuredTracking.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
