package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class TreatyUnitTest {

  private static final Long ID = 1L;

  private static final String period = "period";
  private static final String broker = "broker";
  private static final LocalDate effectiveDate = LocalDate.now();
  private static final LocalDate terminationDate = LocalDate.now();
  private static final String reason = "reason";
  private static final String copyTreatyMsg = "copyTreatyMsg";
  private static final String stmntCedMonthFlag = "stmntCedMonthFlag";
  private static final String smntAsmMonthFlag = "smntAsmMonthFlag";
  private static final String rptDueMonthFlag = "rptDueMonthFlag";
  private static final String notification = "notification";
  private static final BigDecimal brokerFeeAmount = new BigDecimal(1.1);
  private static final BigDecimal brokerFeePercentage = new BigDecimal(1.2);
  private static final Integer isActive = 1;
  private static final Integer notificationStatus = 2;
  private static final String comments = "comments";
  private static final String treatyReference = "treatyReference";
  private static final String treatyName = "treatyName";
  private static final String gracePeriodComments = "gracePeriodComments";
  private static final String latePayment = "latePayment";
  private static final String cedingFlag = "cedingFlag";
  private static final String inputStatus = "inputStatus";
  private static final String brokerFeeSwitchFlag = "brokerFeeSwitchFlag";
  private static final String minBrokerFeeFlag = "minBrokerFeeFlag";
  private static final String gpTrCeding = "gpTrCeding";
  private static final String gpCmmtsCeding = "gpCmmtsCeding";
  private static final String treatyRefIntPltyComment = "treatyRefIntPltyComment";
  private static final String intPenaltyComment = "intPenaltyComment";
  private static final String currencyComments = "currencyComments";
  private static final String reportingProductCmmnt = "reportingProductCmmnt";
  private static final String yearEndException = "yearEndException";
  private static final Long mdmId = 3L;
  private static final Integer disabledTracking = 3;
  private static final Currency brokerCurrency = new Currency();
  private static final Deal deal = new Deal();
  private static final ProductStructure productStructure = new ProductStructure();
  private static final AgreementType agreementType = new AgreementType();
  private static final Frequency settlementFrequency = new Frequency();
  private static final Frequency reportingFrequency = new Frequency();
  private static final Frequency stmntFrqncCeding = new Frequency();
  private static final Frequency stmntPastdueFrqncCeding = new Frequency();
  private static final TreatyStatus treatyStatus = new TreatyStatus();
  private static final Frequency pastDueFrequency = new Frequency();
  private static final Frequency reportingFrequencyMgmt = new Frequency();
  private static final TreatySeriatim treatySeriatim = new TreatySeriatim();
  private static final EntityDate reportingDueDate = new EntityDate();
  private static final EntityDate reportingDueYearEx = new EntityDate();
  private static final EntityDate reportingDueQuarterEx = new EntityDate();
  private static final EntityDate stmntDueCedQuarterEx = new EntityDate();
  private static final EntityDate stmntDueCedGracePeriod = new EntityDate();
  private static final EntityDate stmntDueCed = new EntityDate();
  private static final EntityDate stmntDueAsmQuarterEx = new EntityDate();
  private static final EntityDate stmntDueAsmGracePeriod = new EntityDate();
  private static final EntityDate stmntDueAsm = new EntityDate();
  private static final EntityDate stmntDueAsmBroker = new EntityDate();

  private Treaty treaty;

  @Before
  public void setup() {

    treaty = new Treaty();
    treaty.setId(ID);
    treaty.setPeriod(period);
    treaty.setBroker(broker);
    treaty.setEffectiveDate(effectiveDate);
    treaty.setTerminationDate(terminationDate);
    treaty.setReason(reason);
    treaty.setCopyTreatyMsg(copyTreatyMsg);
    treaty.setStmntCedMonthFlag(stmntCedMonthFlag);
    treaty.setSmntAsmMonthFlag(smntAsmMonthFlag);
    treaty.setRptDueMonthFlag(rptDueMonthFlag);
    treaty.setNotification(notification);
    treaty.setBrokerFeeAmount(brokerFeeAmount);
    treaty.setIsActive(isActive);
    treaty.setNotificationStatus(notificationStatus);
    treaty.setComments(comments);
    treaty.setTreatyReference(treatyReference);
    treaty.setTreatyName(treatyName);
    treaty.setGracePeriodComments(gracePeriodComments);
    treaty.setLatePayment(latePayment);
    treaty.setCedingFlag(cedingFlag);
    treaty.setInputStatus(inputStatus);
    treaty.setBrokerFeeSwitchFlag(brokerFeeSwitchFlag);
    treaty.setMinBrokerFeeFlag(minBrokerFeeFlag);
    treaty.setGpTrCeding(gpTrCeding);
    treaty.setGpCmmtsCeding(gpCmmtsCeding);
    treaty.setTreatyRefIntPltyComment(treatyRefIntPltyComment);
    treaty.setIntPenaltyComment(intPenaltyComment);
    treaty.setCurrencyComments(currencyComments);
    treaty.setReportingProductCmmnt(reportingProductCmmnt);
    treaty.setYearEndException(yearEndException);
    treaty.setMdmId(mdmId);
    treaty.setBrokerCurrency(brokerCurrency);
    treaty.setDeal(deal);
    treaty.setProductStructure(productStructure);
    treaty.setAgreementType(agreementType);
    treaty.setSettlementFrequency(settlementFrequency);
    treaty.setReportingFrequency(reportingFrequency);
    treaty.setStmntFrqncCeding(stmntFrqncCeding);
    treaty.setStmntPastdueFrqncCeding(stmntPastdueFrqncCeding);
    treaty.setTreatyStatus(treatyStatus);
    treaty.setPastDueFrequency(pastDueFrequency);
    treaty.setReportingFrequencyMgmt(reportingFrequencyMgmt);
    treaty.setTreatySeriatim(treatySeriatim);
    treaty.setReportingDueDate(reportingDueDate);
    treaty.setReportingDueYearEx(reportingDueYearEx);
    treaty.setReportingDueQuarterEx(reportingDueQuarterEx);
    treaty.setStmntDueCedQuarterEx(stmntDueCedQuarterEx);
    treaty.setStmntDueAsmGracePeriod(stmntDueAsmGracePeriod);
    treaty.setStmntDueCed(stmntDueCed);
    treaty.setStmntDueAsm(stmntDueAsm);
    treaty.setStmntDueAsmQuarterEx(stmntDueAsmQuarterEx);
    treaty.setStmntDueCedGracePeriod(stmntDueCedGracePeriod);
    treaty.setStmntDueAsmBroker(stmntDueAsmBroker);
    treaty.setBrokerFeePercentage(brokerFeePercentage);

    Set<TreatyCurrency> treatyCurrencies = new HashSet<>();
    treatyCurrencies.add(new TreatyCurrency());
    treaty.setTreatyCurrencies(treatyCurrencies);

    Set<MdmAttributes> mdmAttributes = new HashSet<>();
    mdmAttributes.add(new MdmAttributes());
    treaty.setMdmAttributes(mdmAttributes);
    treaty.setDisabledTracking(1);

    Set<TreatySeriatimType> treatySeriatimTypes = new HashSet<>();
    treatySeriatimTypes.add(new TreatySeriatimType());
    treaty.setSeriatimTypeTreaties(treatySeriatimTypes);

    Set<YearEndRegulatory> yearEndRegulatories = new HashSet<>();
    yearEndRegulatories.add(new YearEndRegulatory());
    treaty.setYearEndRegulatoryList(yearEndRegulatories);
  }

  @Test
  public void equalsGetTest() {
    Long id = treaty.getId();
    String period1 = treaty.getPeriod();
    String broker1 = treaty.getBroker();
    LocalDate effectiveDate1 = treaty.getEffectiveDate();
    LocalDate terminationDate1 = treaty.getTerminationDate();
    String reason1 = treaty.getReason();
    String copyTreatyMsg1 = treaty.getCopyTreatyMsg();
    String stmntCedMonthFlag1 = treaty.getStmntCedMonthFlag();
    String smntAsmMonthFlag1 = treaty.getSmntAsmMonthFlag();
    String rptDueMonthFlag1 = treaty.getRptDueMonthFlag();
    String notification1 = treaty.getNotification();
    BigDecimal brokerFeeAmount1 = treaty.getBrokerFeeAmount();
    Integer isActive1 = treaty.getIsActive();
    Integer notificationStatus1 = treaty.getNotificationStatus();
    String comments1 = treaty.getComments();
    String treatyReference1 = treaty.getTreatyReference();
    String treatyName1 = treaty.getTreatyName();
    String gracePeriodComments1 = treaty.getGracePeriodComments();
    String latePayment1 = treaty.getLatePayment();
    String cedingFlag1 = treaty.getCedingFlag();
    String inputStatus1 = treaty.getInputStatus();
    String brokerFeeSwitchFlag1 = treaty.getBrokerFeeSwitchFlag();
    String minBrokerFeeFlag1 = treaty.getMinBrokerFeeFlag();
    String gpTrCeding1 = treaty.getGpTrCeding();
    String gpCmmtsCeding1 = treaty.getGpCmmtsCeding();
    String treatyRefIntPltyComment1 = treaty.getTreatyRefIntPltyComment();
    String intPenaltyComment1 = treaty.getIntPenaltyComment();
    String currencyComments1 = treaty.getCurrencyComments();
    String reportingProductCmmnt1 = treaty.getReportingProductCmmnt();
    String yearEndException1 = treaty.getYearEndException();
    Long mdmId1 = treaty.getMdmId();
    Currency brokerCurrency1 = treaty.getBrokerCurrency();
    Deal deal1 = treaty.getDeal();
    ProductStructure productStructure1 = treaty.getProductStructure();
    AgreementType agreementType1 = treaty.getAgreementType();
    Frequency settlementFrequency1 = treaty.getSettlementFrequency();
    Frequency reportingFrequency1 = treaty.getReportingFrequency();
    Frequency stmntFrqncCeding1 = treaty.getStmntFrqncCeding();
    Frequency stmntPastdueFrqncCeding1 = treaty.getStmntPastdueFrqncCeding();
    TreatyStatus treatyStatus1 = treaty.getTreatyStatus();
    Frequency pastDueFrequency1 = treaty.getPastDueFrequency();
    Frequency reportingFrequencyMgmt1 = treaty.getReportingFrequencyMgmt();
    TreatySeriatim treatySeriatim1 = treaty.getTreatySeriatim();
    EntityDate reportingDueDate1 = treaty.getReportingDueDate();
    EntityDate reportingDueYearEx1 = treaty.getReportingDueYearEx();
    EntityDate reportingDueQuarterEx1 = treaty.getReportingDueQuarterEx();
    EntityDate stmntDueCedQuarterEx1 = treaty.getStmntDueCedQuarterEx();
    EntityDate stmntDueAsmGracePeriod1 = treaty.getStmntDueAsmGracePeriod();
    EntityDate stmntDueCed1 = treaty.getStmntDueCed();
    EntityDate stmntDueAsm1 = treaty.getStmntDueAsm();
    EntityDate stmntDueAsmQuarterEx1 = treaty.getStmntDueAsmQuarterEx();
    EntityDate stmntDueCedGracePeriod1 = treaty.getStmntDueCedGracePeriod();
    EntityDate stmntDueAsmBroker1 = treaty.getStmntDueAsmBroker();
    Set<TreatyCurrency> treatyCurrency = treaty.getTreatyCurrencies();

    long serialID = Treaty.getSerialVersionUID();
    Set<MdmAttributes> attributes = treaty.getMdmAttributes();
    Integer disabledTracking = treaty.getDisabledTracking();
    Set<TreatySeriatimType> types = treaty.getSeriatimTypeTreaties();
    Set<YearEndRegulatory> endRegulatories = treaty.getYearEndRegulatoryList();
    BigDecimal brokerFeePercentage1 = treaty.getBrokerFeePercentage();

    assertThat(id).isEqualTo(ID);
    assertThat(period1).isEqualTo(period);
    assertThat(broker1).isEqualTo(broker);
    assertThat(effectiveDate1).isEqualTo(effectiveDate);
    assertThat(terminationDate1).isEqualTo(terminationDate);
    assertThat(reason1).isEqualTo(reason);
    assertThat(copyTreatyMsg1).isEqualTo(copyTreatyMsg);
    assertThat(stmntCedMonthFlag1).isEqualTo(stmntCedMonthFlag);
    assertThat(smntAsmMonthFlag1).isEqualTo(smntAsmMonthFlag);
    assertThat(rptDueMonthFlag1).isEqualTo(rptDueMonthFlag);
    assertThat(notification1).isEqualTo(notification);
    assertThat(brokerFeeAmount1).isEqualTo(brokerFeeAmount);
    assertThat(isActive1).isEqualTo(isActive);
    assertThat(notificationStatus1).isEqualTo(notificationStatus);
    assertThat(comments1).isEqualTo(comments);
    assertThat(treatyReference1).isEqualTo(treatyReference);
    assertThat(treatyName1).isEqualTo(treatyName);
    assertThat(gracePeriodComments1).isEqualTo(gracePeriodComments);
    assertThat(latePayment1).isEqualTo(latePayment);
    assertThat(cedingFlag1).isEqualTo(cedingFlag);
    assertThat(inputStatus1).isEqualTo(inputStatus);
    assertThat(brokerFeeSwitchFlag1).isEqualTo(brokerFeeSwitchFlag);
    assertThat(minBrokerFeeFlag1).isEqualTo(minBrokerFeeFlag);
    assertThat(productStructure1).isEqualTo(productStructure);
    assertThat(agreementType1).isEqualTo(agreementType);
    assertThat(settlementFrequency1).isEqualTo(settlementFrequency);
    assertThat(reportingFrequency1).isEqualTo(reportingFrequency);
    assertThat(stmntFrqncCeding1).isEqualTo(stmntFrqncCeding);
    assertThat(stmntPastdueFrqncCeding1).isEqualTo(stmntPastdueFrqncCeding);
    assertThat(treatyStatus1).isEqualTo(treatyStatus);
    assertThat(pastDueFrequency1).isEqualTo(pastDueFrequency);
    assertThat(reportingFrequencyMgmt1).isEqualTo(reportingFrequencyMgmt);
    assertThat(treatySeriatim1).isEqualTo(treatySeriatim);
    assertThat(reportingDueDate1).isEqualTo(reportingDueDate);
    assertThat(reportingDueYearEx1).isEqualTo(reportingDueYearEx);
    assertThat(reportingDueQuarterEx1).isEqualTo(reportingDueQuarterEx);
    assertThat(stmntDueCedQuarterEx1).isEqualTo(stmntDueCedQuarterEx);
    assertThat(stmntDueAsmGracePeriod1).isEqualTo(stmntDueAsmGracePeriod);
    assertThat(stmntDueAsm1).isEqualTo(stmntDueAsm);
    assertThat(stmntDueAsmQuarterEx1).isEqualTo(stmntDueAsmQuarterEx);
    assertThat(stmntDueCed1).isEqualTo(stmntDueCed);
    assertThat(stmntDueCedGracePeriod1).isEqualTo(stmntDueCedGracePeriod);
    assertThat(stmntDueAsmBroker1).isEqualTo(stmntDueAsmBroker);
    assertThat(gpCmmtsCeding1).isEqualTo(gpCmmtsCeding);
    assertThat(gpTrCeding1).isEqualTo(gpTrCeding);
    assertThat(treatyRefIntPltyComment1).isEqualTo(treatyRefIntPltyComment);
    assertThat(intPenaltyComment1).isEqualTo(intPenaltyComment);
    assertThat(currencyComments1).isEqualTo(currencyComments);
    assertThat(reportingProductCmmnt1).isEqualTo(reportingProductCmmnt);
    assertThat(yearEndException1).isEqualTo(yearEndException);
    assertThat(mdmId1).isEqualTo(mdmId);
    assertThat(brokerCurrency1).isEqualTo(brokerCurrency);
    assertThat(deal1).isEqualTo(deal);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = treaty.equals(treaty);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = treaty.equals(new Treaty());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    Treaty treaty1 = new Treaty();
    treaty1.setId(1L);
    boolean same = treaty.equals(treaty1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    Treaty treaty1 = new Treaty();
    treaty1.setId(null);
    boolean same = treaty.equals(treaty1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    Treaty treaty1 = new Treaty();
    treaty1.setId(ID);
    treaty.setId(null);
    boolean same = treaty.equals(treaty1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = treaty.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = treaty.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {
    String reportingFrequencyStr = "";
    String stmntDueAsmGracePeriodName = "";
    String stmntDueCedGracePeriodName = "";
    if (reportingFrequency == null) {
      reportingFrequencyStr = "";
    } else {
      reportingFrequencyStr = reportingFrequency.getName();
    }
    if (settlementFrequency == null) {
      stmntDueAsmGracePeriodName = "";
    } else {
      stmntDueAsmGracePeriodName = settlementFrequency.getName();
    }

    if (stmntFrqncCeding == null) {
      stmntDueCedGracePeriodName = "";
    } else {
      stmntDueCedGracePeriodName = stmntFrqncCeding.getName();
    }

    String expectedResult = "Treaty{" +
      " treatyStatus='" + treatyStatus.getName() + "'" +
      ", effectiveDate='" + effectiveDate + "'" +
      ", reportingDueDate='" + reportingFrequencyStr + " " + reportingDueDate + "'" +
      ", stmntDueAsmGracePeriod='" +stmntDueAsmGracePeriodName + " " +  stmntDueAsmGracePeriod + "'" +
      ", stmntDueCedGracePeriod='" +stmntDueCedGracePeriodName + " " +  stmntDueCedGracePeriod + "'" +
      ", InputStatus='" + inputStatus + "'" +
      '}';

    String actual = treaty.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = treaty.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
