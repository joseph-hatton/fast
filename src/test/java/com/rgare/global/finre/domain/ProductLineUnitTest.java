package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductLineUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  private ProductLine productLine;

  String expectedResult = "ProductLine{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    '}';

  @Before
  public void setup() {
    productLine = new ProductLine();
    productLine.setId(ID);
    productLine.setName(NAME);
    productLine.setDescription(DESCRIPTION);
  }

  @Test
  public void equalsGetTest() {
    Long id = productLine.getId();
    String name = productLine.getName();
    String description = productLine.getDescription();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = productLine.equals(productLine);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = productLine.equals(new ProductLine());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    ProductLine productLine1 = new ProductLine();
    productLine1.setId(1L);
    boolean same = productLine.equals(productLine1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    ProductLine productLine1 = new ProductLine();
    productLine1.setId(null);
    boolean same = productLine.equals(productLine1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    ProductLine productLine1 = new ProductLine();
    productLine1.setId(ID);
    productLine.setId(null);
    boolean same = productLine.equals(productLine1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = productLine.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = productLine.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = productLine.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = productLine.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
