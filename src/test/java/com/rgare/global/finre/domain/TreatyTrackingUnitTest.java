package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class TreatyTrackingUnitTest {

  private static final Long ID = 1L;

  private static final LocalDate calculatedReportingDueDate = LocalDate.now();
  private static final LocalDate calculatedSeriatimDueDate = LocalDate.now();
  private static final LocalDate calculatedReportingDueYex = LocalDate.now();
  private static final LocalDate calculatedAssumingGraceDate = LocalDate.now();
  private static final LocalDate calculatedAssumingDate = LocalDate.now();
  private static final LocalDate calculatedCedingGraceDate = LocalDate.now();
  private static final LocalDate reportingReceivedDate = LocalDate.now();
  private static final LocalDate cashUploadDate = LocalDate.now();
  private static final Integer isActive = 1;
  private static final Integer flagEmailRec = 2;
  private static final Integer flagEmail = 3;
  private static final LocalDate reportingMonthYear = LocalDate.now();
  private static final String comments = "comments";
  private static final String settlementDue = "settlementDue";
  private static final String trackingStatus = "trackingStatus";
  private static final LocalDate riskMgmtDate = LocalDate.now();
  private static final LocalDate emailDate = LocalDate.now();
  private static final LocalDate brokerFeeDueDate = LocalDate.now();
  private static final LocalDate confirmationReceivedDate = LocalDate.now();
  private static final LocalDate cashSentRecDate = LocalDate.now();
  private static final String reportingPeriod = "reportingPeriod";
  private static final Integer autoGenerateFlag = 4;
  private static final String cashDueFlag = "cashDueFlag";
  private static final LocalDate seriatimReceivedDate = LocalDate.now();
  private static final Currency currency = new Currency();
  private static final Treaty treaty = new Treaty();

  private TreatyTracking treatyTracking;

  String expectedResult = "TreatyTracking{" +
    "id=" + ID +
    ", calculatedReportingDueDate='" + calculatedReportingDueDate + "'" +
    ", reportingReceivedDate='" + reportingReceivedDate + "'" +
    ", cashUploadDate='" + cashUploadDate + "'" +
    ", comments='" + comments + "'" +
    ", riskMgmtDate='" + riskMgmtDate + "'" +
    ", emailDate='" + emailDate + "'" +
    ", confirmationReceivedDate='" + confirmationReceivedDate + "'" +
    ", cashSentRecDate='" + cashSentRecDate + "'" +
    ", trackingStatus='" + trackingStatus + "'" +
    '}';

  @Before
  public void setup() {
    treatyTracking = new TreatyTracking();
    treatyTracking.setId(ID);
    treatyTracking.setCalculatedReportingDueDate(calculatedReportingDueDate);
    treatyTracking.setCalculatedSeriatimDueDate(calculatedSeriatimDueDate);
    treatyTracking.setCalculatedReportingDueYex(calculatedReportingDueYex);
    treatyTracking.setCalculatedAssumingGraceDate(calculatedAssumingGraceDate);
    treatyTracking.setCalculatedAssumingDate(calculatedAssumingDate);
    treatyTracking.setCalculatedCedingGraceDate(calculatedCedingGraceDate);
    treatyTracking.setReportingReceivedDate(reportingReceivedDate);
    treatyTracking.setCashUploadDate(cashUploadDate);
    treatyTracking.setIsActive(isActive);
    treatyTracking.setFlagEmailRec(flagEmailRec);
    treatyTracking.setFlagEmail(flagEmail);
    treatyTracking.setReportingMonthYear(reportingMonthYear);
    treatyTracking.setComments(comments);
    treatyTracking.setSettlementDue(settlementDue);
    treatyTracking.setTrackingStatus(trackingStatus);
    treatyTracking.setRiskMgmtDate(riskMgmtDate);
    treatyTracking.setEmailDate(emailDate);
    treatyTracking.setBrokerFeeDueDate(brokerFeeDueDate);
    treatyTracking.setConfirmationReceivedDate(confirmationReceivedDate);
    treatyTracking.setCashSentRecDate(cashSentRecDate);
    treatyTracking.setReportingPeriod(reportingPeriod);
    treatyTracking.setAutoGenerateFlag(autoGenerateFlag);
    treatyTracking.setCashDueFlag(cashDueFlag);
    treatyTracking.setSeriatimReceivedDate(seriatimReceivedDate);
    treatyTracking.setCurrency(currency);
    treatyTracking.setTreaty(treaty);
  }

  @Test
  public void equalsGetTest() {
    Long id = treatyTracking.getId();
    LocalDate calculatedReportingDueDate1 = treatyTracking.getCalculatedReportingDueDate();
    LocalDate calculatedSeriatimDueDate1 = treatyTracking.getCalculatedSeriatimDueDate();
    LocalDate calculatedReportingDueYex1 = treatyTracking.getCalculatedReportingDueYex();
    LocalDate calculatedAssumingGraceDate1 = treatyTracking.getCalculatedAssumingGraceDate();
    LocalDate calculatedAssumingDate1 = treatyTracking.getCalculatedAssumingDate();
    LocalDate calculatedCedingGraceDate1 = treatyTracking.getCalculatedCedingGraceDate();
    LocalDate reportingReceivedDate1 = treatyTracking.getReportingReceivedDate();
    LocalDate cashUploadDate1 = treatyTracking.getCashUploadDate();
    Integer isActive1 = treatyTracking.getIsActive();
    Integer flagEmailRec1 = treatyTracking.getFlagEmailRec();
    Integer flagEmail1 = treatyTracking.getFlagEmail();
    LocalDate reportingMonthYear1 = treatyTracking.getReportingMonthYear();
    String comments1 = treatyTracking.getComments();
    String settlementDue1 = treatyTracking.getSettlementDue();
    String trackingStatus1 = treatyTracking.getTrackingStatus();
    LocalDate riskMgmtDate1 = treatyTracking.getRiskMgmtDate();
    LocalDate emailDate1 = treatyTracking.getEmailDate();
    LocalDate brokerFeeDueDate1 = treatyTracking.getBrokerFeeDueDate();
    LocalDate confirmationReceivedDate1 = treatyTracking.getConfirmationReceivedDate();
    LocalDate cashSentRecDate1 = treatyTracking.getCashSentRecDate();
    String reportingPeriod1 = treatyTracking.getReportingPeriod();
    Integer autoGenerateFlag1 = treatyTracking.getAutoGenerateFlag();
    String cashDueFlag1 = treatyTracking.getCashDueFlag();
    Currency currency1 = treatyTracking.getCurrency();
    Treaty treaty1 = treatyTracking.getTreaty();
    LocalDate seriatimDateRecd = treatyTracking.getSeriatimReceivedDate();
    assertThat(id).isEqualTo(ID);
    assertThat(calculatedReportingDueDate1).isEqualTo(calculatedReportingDueDate);
    assertThat(calculatedSeriatimDueDate1).isEqualTo(calculatedSeriatimDueDate);
    assertThat(calculatedReportingDueYex1).isEqualTo(calculatedReportingDueYex);
    assertThat(calculatedAssumingGraceDate1).isEqualTo(calculatedAssumingGraceDate);
    assertThat(calculatedAssumingDate1).isEqualTo(calculatedAssumingDate);
    assertThat(calculatedCedingGraceDate1).isEqualTo(calculatedCedingGraceDate);
    assertThat(reportingReceivedDate1).isEqualTo(reportingReceivedDate);
    assertThat(cashUploadDate1).isEqualTo(cashUploadDate);
    assertThat(isActive1).isEqualTo(isActive);
    assertThat(flagEmailRec1).isEqualTo(flagEmailRec);
    assertThat(flagEmail1).isEqualTo(flagEmail);
    assertThat(reportingMonthYear1).isEqualTo(reportingMonthYear);
    assertThat(comments1).isEqualTo(comments);
    assertThat(settlementDue1).isEqualTo(settlementDue);
    assertThat(trackingStatus1).isEqualTo(trackingStatus);
    assertThat(riskMgmtDate1).isEqualTo(riskMgmtDate);
    assertThat(emailDate1).isEqualTo(emailDate);
    assertThat(brokerFeeDueDate1).isEqualTo(brokerFeeDueDate);
    assertThat(confirmationReceivedDate1).isEqualTo(confirmationReceivedDate);
    assertThat(cashSentRecDate1).isEqualTo(cashSentRecDate);
    assertThat(reportingPeriod1).isEqualTo(reportingPeriod);
    assertThat(autoGenerateFlag1).isEqualTo(autoGenerateFlag);
    assertThat(cashDueFlag1).isEqualTo(cashDueFlag);
    assertThat(currency1).isEqualTo(currency);
    assertThat(treaty1).isEqualTo(treaty);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = treatyTracking.equals(treatyTracking);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = treatyTracking.equals(new TreatyTracking());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    TreatyTracking treatyTracking1 = new TreatyTracking();
    treatyTracking1.setId(1L);
    boolean same = treatyTracking.equals(treatyTracking1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    TreatyTracking treatyTracking1 = new TreatyTracking();
    treatyTracking1.setId(null);
    boolean same = treatyTracking.equals(treatyTracking1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    TreatyTracking treatyTracking1 = new TreatyTracking();
    treatyTracking1.setId(ID);
    treatyTracking.setId(null);
    boolean same = treatyTracking.equals(treatyTracking1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = treatyTracking.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = treatyTracking.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = treatyTracking.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = treatyTracking.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
