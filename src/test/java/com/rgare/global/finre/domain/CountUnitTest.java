package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class CountUnitTest {

  private static final Long ID = 1L;


  private Count count;



  @Before
  public void setup() {

    count = new Count(ID);
    count.setAmount(ID);

  }

  @Test
  public void equalsGetTest() {
    Long id = count.getAmount();
    assertThat(id).isEqualTo(ID);
  }
}
