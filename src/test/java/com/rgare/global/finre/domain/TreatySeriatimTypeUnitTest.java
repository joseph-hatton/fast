package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class TreatySeriatimTypeUnitTest {

  private static final Long ID = 1L;
  private static final String SERIATIM_DUE_DATE_FLAG = "FLAG";
  private static final String TREATY_SERIATIM_COMMENT = "COMMENT";
  private static final String TREATY_SERIATIM_REFERENCE = "REFERENCE";
  private static final Boolean CALCULATION_FLAG = new Boolean(true);

  private static final EntityDate SERIATIM_DUE_DATE = new EntityDate();
  private static final Frequency SERIATIM_FREQ = new Frequency();
  private static final SeriatimType SERIATIM_TYPE = new SeriatimType();

  private TreatySeriatimType treatySeriatimType;

  String expectedResult = "TreatySeriatimType{" +
    "id=" + ID +
    '}';

  @Before
  public void setup() {

    treatySeriatimType = new TreatySeriatimType();
    treatySeriatimType.setId(ID);
    treatySeriatimType.setSeriatimDueDateFlag(SERIATIM_DUE_DATE_FLAG);
    treatySeriatimType.setTreatySeriatimComment(TREATY_SERIATIM_COMMENT);
    treatySeriatimType.setTreatySeriatimReference(TREATY_SERIATIM_REFERENCE);
    treatySeriatimType.setCalculationFlag(CALCULATION_FLAG);
    treatySeriatimType.setSeriatimDueDate(SERIATIM_DUE_DATE);
    treatySeriatimType.setSeriatimFreq(SERIATIM_FREQ);
    treatySeriatimType.setSeriatimType(SERIATIM_TYPE);
  }

  @Test
  public void equalsGetTest() {
    Long id = treatySeriatimType.getId();
    String seriatimDueDateFlag = treatySeriatimType.getSeriatimDueDateFlag();
    String treatySeriatimComment = treatySeriatimType.getTreatySeriatimComment();
    String treatySeriatimReference = treatySeriatimType.getTreatySeriatimReference();
    Boolean calculationFlag = treatySeriatimType.getCalculationFlag();
    EntityDate seriatimDueDate = treatySeriatimType.getSeriatimDueDate();
    Frequency seriatimFreq = treatySeriatimType.getSeriatimFreq();
    SeriatimType seriatimType = treatySeriatimType.getSeriatimType();
    assertThat(id).isEqualTo(ID);
    assertThat(seriatimDueDateFlag).isEqualTo(SERIATIM_DUE_DATE_FLAG);
    assertThat(treatySeriatimComment).isEqualTo(TREATY_SERIATIM_COMMENT);
    assertThat(treatySeriatimReference).isEqualTo(TREATY_SERIATIM_REFERENCE);
    assertThat(calculationFlag).isEqualTo(CALCULATION_FLAG);
    assertThat(seriatimDueDate).isEqualTo(SERIATIM_DUE_DATE);
    assertThat(seriatimFreq).isEqualTo(SERIATIM_FREQ);
    assertThat(seriatimType).isEqualTo(SERIATIM_TYPE);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = treatySeriatimType.equals(treatySeriatimType);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = treatySeriatimType.equals(new TreatySeriatimType());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    TreatySeriatimType treatySeriatimType1 = new TreatySeriatimType();
    treatySeriatimType1.setId(1L);
    boolean same = treatySeriatimType.equals(treatySeriatimType1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    TreatySeriatimType treatySeriatimType1 = new TreatySeriatimType();
    treatySeriatimType1.setId(null);
    boolean same = treatySeriatimType.equals(treatySeriatimType1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    TreatySeriatimType treatySeriatimType1 = new TreatySeriatimType();
    treatySeriatimType1.setId(ID);
    treatySeriatimType.setId(null);
    boolean same = treatySeriatimType.equals(treatySeriatimType1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = treatySeriatimType.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = treatySeriatimType.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = treatySeriatimType.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = treatySeriatimType.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
