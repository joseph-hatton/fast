package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ContactTypeUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  private ContactType contactType;

  String expectedResult = "ContactType{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    '}';

  @Before
  public void setup() {
    contactType = new ContactType();
    contactType.setId(ID);
    contactType.setDescription(DESCRIPTION);
    contactType.setName(NAME);

  }

  @Test
  public void equalsGetTest() {
    Long id = contactType.getId();
    String name = contactType.getName();
    String description = contactType.getDescription();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = contactType.equals(contactType);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = contactType.equals(new ContactType());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    ContactType contactType1 = new ContactType();
    contactType1.setId(1L);
    boolean same = contactType.equals(contactType1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    ContactType contactType1 = new ContactType();
    contactType1.setId(null);
    boolean same = contactType.equals(contactType1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    ContactType contactType1 = new ContactType();
    contactType1.setId(ID);
    contactType.setId(null);
    boolean same = contactType.equals(contactType1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = contactType.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = contactType.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = contactType.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = contactType.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
