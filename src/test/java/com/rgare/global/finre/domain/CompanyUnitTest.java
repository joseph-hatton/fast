package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class CompanyUnitTest {

  private static final Long ID = 1L;
  private static final String NAME = "NAME";
  private static final String AFFILIATE_NAME = "AFFILIATE_NAME";
  private static final String COMPANY_TYPE = "COMPANY_TYPE";
  private static final String CITY = "CITY";
  private static final String STATE = "STATE";
  private static final String POSTAL_CODE = "POSTAL_CODE";
  private static final String REGION = "REGION";
  private static final String COUNTRY_NAME = "COUNTRY_NAME";
  private static final Long CLIENT_ID = 1L;
  private static final Long MDM_ID = 1L;
  private static final Integer IS_ACTIVE = 1;
  private static final String LEGAL_CODE = "LEGAL_CODE";
  private static final String LEGAL_NAME = "LEGAL_NAME";
  private static final Integer IS_RGA_COMPANY = 1;
  private static final String NAME_MOD = "NAME*";

  private static final Country COUNTRY = new Country();


  private Company company;

  String expectedResult = "Company{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", affiliateName='" + AFFILIATE_NAME + "'" +
    ", companyType='" + COMPANY_TYPE + "'" +
    ", city='" + CITY + "'" +
    ", state='" + STATE + "'" +
    ", postalCode='" + POSTAL_CODE + "'" +
    ", countryName='" + COUNTRY_NAME + "'" +
    ", region='" + REGION + "'" +
    ", isActive='" + IS_ACTIVE + "'" +
    ", clientCompanyId='" + CLIENT_ID + "'" +
    ", mdmId='" + MDM_ID + "'" +
    ", legalCode='" + LEGAL_CODE + "'" +
    ", legalName='" + LEGAL_NAME + "'" +
    '}';

  @Before
  public void setup() {

    company = new Company();
    company.setId(ID);
    company.setName(null);
    company.setName(NAME);
    company.setAffiliateName(null);
    company.setAffiliateName(AFFILIATE_NAME);
    company.setCompanyType(COMPANY_TYPE);
    company.setCity(CITY);
    company.setState(STATE);
    company.setPostalCode(POSTAL_CODE);
    company.setRegion(REGION);
    company.setCountryName(COUNTRY_NAME);
    company.setClientCompanyId(CLIENT_ID);
    company.setMdmId(MDM_ID);
    company.setIsActive(IS_ACTIVE);
    company.setLegalCode(LEGAL_CODE);
    company.setLegalName(LEGAL_NAME);
    company.setIsRgaCompany(IS_RGA_COMPANY);
    company.setCountry(COUNTRY);
  }

  @Test
  public void equalsGetTest() {
    Long id = company.getId();
    String name = company.getName();
    String affiliateName =  company.getAffiliateName();
    String companyType = company.getCompanyType();
    String city = company.getCity();
    String state = company.getState();
    String postalCode = company.getPostalCode();
    String region = company.getRegion();
    String countryName = company.getCountryName();
    Long clientCompanyId = company.getClientCompanyId();
    Long mdmId = company.getMdmId();
    Integer isActive = company.getIsActive();
    String legalCode = company.getLegalCode();
    String legalName = company.getLegalName();
    Integer isRgaCompany = company.getIsRgaCompany();
    Country country = company.getCountry();

    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(affiliateName).isEqualTo(AFFILIATE_NAME);
    assertThat(companyType).isEqualTo(COMPANY_TYPE);
    assertThat(city).isEqualTo(CITY);
    assertThat(state).isEqualTo(STATE);
    assertThat(postalCode).isEqualTo(POSTAL_CODE);
    assertThat(region).isEqualTo(REGION);
    assertThat(countryName).isEqualTo(COUNTRY_NAME);
    assertThat(clientCompanyId).isEqualTo(CLIENT_ID);
    assertThat(mdmId).isEqualTo(MDM_ID);
    assertThat(isActive).isEqualTo(IS_ACTIVE);
    assertThat(legalCode).isEqualTo(LEGAL_CODE);
    assertThat(legalName).isEqualTo(legalName);
    assertThat(isRgaCompany).isEqualTo(IS_RGA_COMPANY);
    assertThat(country).isEqualTo(COUNTRY);
  }


  @Test
  public void equalsGetWhereClientIsNullTest() {
    Long id = company.getId();

    company.setClientCompanyId(null);
    String name = company.getName();

    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME_MOD);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = company.equals(company);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = company.equals(new Company());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    Company company1 = new Company();
    company1.setId(1L);
    boolean same = company.equals(company1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    Company company1 = new Company();
    company1.setId(null);
    boolean same = company.equals(company1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    Company company1 = new Company();
    company1.setId(ID);
    company.setId(null);
    boolean same = company.equals(company1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = company.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = company.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = company.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = company.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
