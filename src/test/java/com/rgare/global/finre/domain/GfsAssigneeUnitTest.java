package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class GfsAssigneeUnitTest {

  private static final Long ID = 1L;

  private static final String FIRST_NAME = "FIRST_NAME";
  private static final String LAST_NAME = "LAST_NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  private GfsAssignee gfsAssignee;

  String expectedResult = "GfsAssignee{" +
    "id=" + ID +
    ", firstName='" + FIRST_NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    ", lastName='" + LAST_NAME + "'" +
    '}';

  @Before
  public void setup() {
    gfsAssignee = new GfsAssignee();
    gfsAssignee.setId(ID);
    gfsAssignee.setFirstName(FIRST_NAME);
    gfsAssignee.setLastName(LAST_NAME);
    gfsAssignee.setDescription(DESCRIPTION);
  }

  @Test
  public void equalsGetTest() {
    Long id = gfsAssignee.getId();
    String firstName = gfsAssignee.getFirstName();
    String lastName = gfsAssignee.getLastName();
    String description = gfsAssignee.getDescription();
    assertThat(id).isEqualTo(ID);
    assertThat(firstName).isEqualTo(FIRST_NAME);
    assertThat(lastName).isEqualTo(LAST_NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = gfsAssignee.equals(gfsAssignee);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = gfsAssignee.equals(new GfsAssignee());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    GfsAssignee gfsAssignee1 = new GfsAssignee();
    gfsAssignee1.setId(1L);
    boolean same = gfsAssignee.equals(gfsAssignee1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    GfsAssignee gfsAssignee1 = new GfsAssignee();
    gfsAssignee1.setId(null);
    boolean same = gfsAssignee.equals(gfsAssignee1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    GfsAssignee gfsAssignee1 = new GfsAssignee();
    gfsAssignee1.setId(ID);
    gfsAssignee.setId(null);
    boolean same = gfsAssignee.equals(gfsAssignee1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = gfsAssignee.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = gfsAssignee.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = gfsAssignee.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = gfsAssignee.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
