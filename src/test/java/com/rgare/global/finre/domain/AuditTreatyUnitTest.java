package com.rgare.global.finre.domain;

import cucumber.api.java.cs.A;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuditTreatyUnitTest {

  private static final Long ID = 1L;
  private static final Long DEAL_ID = 1L;
  private static final Long TREATY_ID = 1L;
  private static final String ACTION = "ACTION";
  private static final String NAME = "NAME";
  private static final String COMMENTS = "COMMENTS";
  private static final String CREATION_USER = "USER";

  LocalDateTime ldt = LocalDateTime.of(2017, Month.JULY, 19, 16, 30);
  private  ZonedDateTime ZONED_DATE_TIME = ldt.atZone(ZoneId.of("Europe/Paris"));
  private AuditTreaty auditTreaty;

  String expectedResult = "AuditTreaty{" +
    "id=" + ID +
    ", treatyId='" + TREATY_ID + "'" +
    ", action='" + ACTION + "'" +
    ", comments='" + COMMENTS + "'" +
    ", name='" + NAME + "'" +
    ", creationUser='" + CREATION_USER + "'" +
    '}';

  @Before
  public void setup() {

    auditTreaty = new AuditTreaty();
    auditTreaty.setId(ID);
    auditTreaty.setComments(COMMENTS);
    auditTreaty.setAction(ACTION);
    auditTreaty.setTreatyId(TREATY_ID);
    auditTreaty.setName(NAME);
    auditTreaty.setCreationUser(CREATION_USER);
    auditTreaty.setCreationTs(ZONED_DATE_TIME);
  }

  @Test
  public void equalsGetTest() {
    Long id = auditTreaty.getId();
    Long treatyId = auditTreaty.getTreatyId();
    String name = auditTreaty.getName();
    String action = auditTreaty.getAction();
    String comments = auditTreaty.getComments();
    String createUser = auditTreaty.getCreationUser();
    ZonedDateTime createTS = auditTreaty.getCreationTs();

    assertThat(id).isEqualTo(ID);
    assertThat(treatyId).isEqualTo(TREATY_ID);
    assertThat(action).isEqualTo(ACTION);
    assertThat(comments).isEqualTo(COMMENTS);
    assertThat(createUser).isEqualTo(CREATION_USER);
    assertThat(createTS).isEqualTo(ZONED_DATE_TIME);
    assertThat(name).isEqualTo(NAME);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = auditTreaty.equals(auditTreaty);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = auditTreaty.equals(new AuditTreaty());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    AuditTreaty auditTreaty1 = new AuditTreaty();
    auditTreaty1.setId(1L);
    boolean same = auditTreaty.equals(auditTreaty1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    AuditTreaty auditTreaty1 = new AuditTreaty();
    auditTreaty1.setId(null);
    boolean same = auditTreaty.equals(auditTreaty1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    AuditTreaty auditTreaty1 = new AuditTreaty();
    auditTreaty1.setId(ID);
    auditTreaty.setId(null);
    boolean same = auditTreaty.equals(auditTreaty1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = auditTreaty.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = auditTreaty.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = auditTreaty.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = auditTreaty.hashCode();
    assertThat(actual).isEqualTo(ID.intValue());
  }


}
