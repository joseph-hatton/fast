package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class MdmCompanyUnitTest {

  private static final Long ID = 1L;
  private static final Long MDM_ID = 1L;
  private static final Long CLIENT_COMPANY_ID = 1L;
  private static final String AFFILIATE_NAME = "NAME";
  private static final String COUNTRY_NAME = "COUNTRY";
  private static final String STATE = "STATE";
  private static final String CITY = "CITY";
  private static final String LEGAL_CODE= "LEGAL_CODE";
  private static final String LEGAL_NAME= "LEGAL_NAME";
  private static final String COMPANY_NAME = "NAME";

  private MdmCompany mdmCompany;

  String expectedResult ="MdmCompany{" +
    "id=" + ID +
    ", mdmIid=" + MDM_ID +
    ", clientCompanyId='" + CLIENT_COMPANY_ID + '\'' +
    ", affiliatename='" + AFFILIATE_NAME + '\'' +
    ", countryName='" + COUNTRY_NAME + '\'' +
    ", state='" + STATE + '\'' +
    ", city='" + CITY + '\'' +
    ", legalName='" + LEGAL_NAME + '\'' +
    ", legalCode='" + LEGAL_CODE + '\'' +
    ", companyName='" + COMPANY_NAME + '\'' +
    '}';

  @Before
  public void setup() {

    mdmCompany = new MdmCompany();
    mdmCompany.setId(ID);
    mdmCompany.setMdmId(MDM_ID);
    mdmCompany.setAffiliatename(AFFILIATE_NAME);
    mdmCompany.setClientCompanyId(CLIENT_COMPANY_ID);
    mdmCompany.setCountryName(COUNTRY_NAME);
    mdmCompany.setState(STATE);
    mdmCompany.setCity(CITY);
    mdmCompany.setLegalCode(LEGAL_CODE);
    mdmCompany.setLegalName(LEGAL_NAME);
    mdmCompany.setCompanyName(COMPANY_NAME);
  }

  @Test
  public void equalsGetTest() {
    Long id = mdmCompany.getId();
    Long mdmId = mdmCompany.getMdmId();
    String affiliatename = mdmCompany.getAffiliatename();
    Long clientCompanyId = mdmCompany.getClientCompanyId();
    String countryName = mdmCompany.getCountryName();
    String state = mdmCompany.getState();
    String city = mdmCompany.getCity();
    String legalCode = mdmCompany.getLegalCode();
    String legalName = mdmCompany.getLegalName();
    String companyName = mdmCompany.getCompanyName();

    assertThat(id).isEqualTo(ID);
    assertThat(mdmId).isEqualTo(ID);
    assertThat(affiliatename).isEqualTo(AFFILIATE_NAME);
    assertThat(clientCompanyId).isEqualTo(CLIENT_COMPANY_ID);
    assertThat(countryName).isEqualTo(COUNTRY_NAME);
    assertThat(state).isEqualTo(STATE);
    assertThat(city).isEqualTo(CITY);
    assertThat(legalCode).isEqualTo(LEGAL_CODE);
    assertThat(legalName).isEqualTo(LEGAL_NAME);
    assertThat(companyName).isEqualTo(COMPANY_NAME);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = mdmCompany.equals(mdmCompany);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = mdmCompany.equals(new MdmCompany());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    MdmCompany mdmCompany1 = new MdmCompany();
    mdmCompany1.setId(ID);
    mdmCompany1.setCompanyName(COMPANY_NAME);
    mdmCompany1.setClientCompanyId(CLIENT_COMPANY_ID);
    mdmCompany1.setLegalName(LEGAL_NAME);
    boolean same = mdmCompany.equals(mdmCompany1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsObjectNotEqualTest() {
    boolean same = mdmCompany.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsClientIDIsNotEqualTest() {
    MdmCompany mdmCompany1 = new MdmCompany();
    mdmCompany1.setId(ID);
    mdmCompany1.setClientCompanyId(3L);
    boolean same = mdmCompany.equals(mdmCompany1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIDIsNotEqualTest() {
    MdmCompany mdmCompany1 = new MdmCompany();
    mdmCompany1.setId(3L);
    boolean same = mdmCompany.equals(mdmCompany1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsLegalNameIsNotEqualTest() {
    MdmCompany mdmCompany1 = new MdmCompany();
    mdmCompany1.setClientCompanyId(CLIENT_COMPANY_ID);
    mdmCompany1.setLegalName("NOT_LEGAL_NAME");
    boolean same = mdmCompany.equals(mdmCompany1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = mdmCompany.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = mdmCompany.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int expected = ID.hashCode();
    int actual = mdmCompany.hashCode();

    assertThat(expected).isEqualTo(actual);
  }


}
