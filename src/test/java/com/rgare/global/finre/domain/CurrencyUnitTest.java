package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class CurrencyUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String CODE = "CODE";
  private static final String DESCRIPTION = "DESCRIPTION";
  private static final Integer YEAR = 1;
  private static final Double EXCHANGE_RATE = 1.1;

  private static final LocalDate EFFECTIVE_DATE = LocalDate.now();

  private Currency currency;

  String expectedResult = "Currency{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", code='" + CODE + "'" +
    ", description='" + DESCRIPTION + "'" +
    ", exchangeRate='" + EXCHANGE_RATE + "'" +
    ", year='" + YEAR + "'" +
    '}';

  @Before
  public void setup() {

    currency = new Currency();
    currency.setId(ID);
    currency.setYear(YEAR);
    currency.setCode(CODE);
    currency.setDescription(DESCRIPTION);
    currency.setName(NAME);
    currency.setExchangeRate(EXCHANGE_RATE);
    currency.setEffectiveDate(EFFECTIVE_DATE);
  }

  @Test
  public void equalsGetTest() {
    Long id = currency.getId();
    String name = currency.getName();
    Integer year = currency.getYear();
    String code = currency.getCode();
    Double exchangeRate = currency.getExchangeRate();
    String description = currency.getDescription();
    LocalDate effectiveDate = currency.getEffectiveDate();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
    assertThat(year).isEqualTo(YEAR);
    assertThat(code).isEqualTo(CODE);
    assertThat(exchangeRate).isEqualTo(EXCHANGE_RATE);
    assertThat(effectiveDate).isEqualTo(EFFECTIVE_DATE);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = currency.equals(currency);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = currency.equals(new Currency());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    Currency currency1 = new Currency();
    currency1.setId(1L);
    boolean same = currency.equals(currency1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    Currency currency1 = new Currency();
    currency1.setId(null);
    boolean same = currency.equals(currency1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    Currency currency1 = new Currency();
    currency1.setId(ID);
    currency.setId(null);
    boolean same = currency.equals(currency1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = currency.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = currency.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = currency.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = currency.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
