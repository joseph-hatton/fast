package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class EntityDateUnitTest {

  private static final Long ID = 1L;
  private static final String ENTITY_TYPE = "TYPE";
  private static final Integer NUMBER_OF_DAYS = 1;
  private static final String DAY_TYPE = "DAY_TYPE";
  private static final String PERIOD = "PERIOD";
  private static final String YEAR_TYPE = "YEAR_TPE";
  private static final String COMMENTS = "COMMENTS";
  private static final String TREATY_REF_COMMENTS = "REF_COMMENTS";


  private EntityDate entityDate;

  String expectedResult = NUMBER_OF_DAYS+" "+DAY_TYPE+" "+PERIOD+" ";

  @Before
  public void setup() {

    entityDate = new EntityDate();
    entityDate.setId(ID);
    entityDate.setEntityType(ENTITY_TYPE);
    entityDate.setNumberOfDays(NUMBER_OF_DAYS);
    entityDate.setDayType(DAY_TYPE);
    entityDate.setPeriod(PERIOD);
    entityDate.setYearType(YEAR_TYPE);
    entityDate.setComments(COMMENTS);
    entityDate.setTreatyRefComments(TREATY_REF_COMMENTS);
  }

  @Test
  public void equalsGetTest() {
    Long id = entityDate.getId();
    String entityType = entityDate.getEntityType();
    Integer numberOfDays = entityDate.getNumberOfDays();
    String dayType = entityDate.getDayType();
    String period = entityDate.getPeriod();
    String yearType = entityDate.getYearType();
    String comments = entityDate.getComments();
    String treatyRefComments = entityDate.getTreatyRefComments();

    assertThat(id).isEqualTo(ID);
    assertThat(entityType).isEqualTo(ENTITY_TYPE);
    assertThat(numberOfDays).isEqualTo(NUMBER_OF_DAYS);
    assertThat(dayType).isEqualTo(DAY_TYPE);
    assertThat(period).isEqualTo(PERIOD);
    assertThat(yearType).isEqualTo(YEAR_TYPE);
    assertThat(comments).isEqualTo(COMMENTS);
    assertThat(treatyRefComments).isEqualTo(TREATY_REF_COMMENTS);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = entityDate.equals(entityDate);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = entityDate.equals(new EntityDate());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    EntityDate entityDate1 = new EntityDate();
    entityDate1.setId(1L);
    boolean same = entityDate.equals(entityDate1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    EntityDate entityDate1 = new EntityDate();
    entityDate1.setId(null);
    boolean same = entityDate.equals(entityDate1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    EntityDate entityDate1 = new EntityDate();
    entityDate1.setId(ID);
    entityDate.setId(null);
    boolean same = entityDate.equals(entityDate1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = entityDate.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = entityDate.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = entityDate.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = entityDate.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
