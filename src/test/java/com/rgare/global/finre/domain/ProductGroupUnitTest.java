package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductGroupUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  private ProductGroup productGroup;

  String expectedResult = "ProductGroup{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    '}';

  @Before
  public void setup() {
    productGroup = new ProductGroup();
    productGroup.setId(ID);
    productGroup.setName(NAME);
    productGroup.setDescription(DESCRIPTION);
  }

  @Test
  public void equalsGetTest() {
    Long id = productGroup.getId();
    String name = productGroup.getName();
    String description = productGroup.getDescription();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = productGroup.equals(productGroup);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = productGroup.equals(new ProductGroup());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    ProductGroup productGroup1 = new ProductGroup();
    productGroup1.setId(1L);
    boolean same = productGroup.equals(productGroup1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    ProductGroup productGroup1 = new ProductGroup();
    productGroup1.setId(null);
    boolean same = productGroup.equals(productGroup1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    ProductGroup productGroup1 = new ProductGroup();
    productGroup1.setId(ID);
    productGroup.setId(null);
    boolean same = productGroup.equals(productGroup1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = productGroup.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = productGroup.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = productGroup.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = productGroup.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
