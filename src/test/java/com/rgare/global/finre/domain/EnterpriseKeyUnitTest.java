package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class EnterpriseKeyUnitTest {

  private static final Long ID = 1L;
  private static final String NAME = "NAME";
  private static final Long ENTERPRISE_ID = 1L;

  private static final String DEAL = "DEAL";
  private static final String AFFILIATE_TYPE = "TYPE";
  private static final String CORPORATE_INDICATOR = "INDICATOR";
  private static final String BUSINESS_UNIT_ID = "1";
  private static final String BUSINESS_UNIT_NAME = "NAME";
  private static final String LEGAL_DOC = "LEGAL_DOC";
  private static final String REPORTING_PRODUCT_ID = "PRODUCT_ID";
  private static final String REPORTING_PRODUCT = "PRODUCT";
  private static final String CLIENT_COMPANY_ID = "CLIENT";
  private static final String CLIENT_COMPANY_NAME = "CLIENT_NAME";
  private static final String PRODUCT_LINE_NAME = "PRODUCT_LINE_NAME";
  private static final Long PRODUCT_LINE_ID = 1L;

  private static final Long VERSION_NUMBER = 1L;
  private static final Long VERSION_DATE = 1L;
  private static final String LEGAL_CODE = "LEGAL_CODE";
  private static final String LEGAL_NAME = "LEGAL_NAME";
  private static final String EFFECTIVE_CODE = "CODE";


  private static final ProductStructure PRODUCT_STRUCTURE = new ProductStructure();
  private EnterpriseKey enterpriseKey;

  String expectedResult = "EnterpriseKey{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    '}';

  @Before
  public void setup() {

    enterpriseKey = new EnterpriseKey();
    enterpriseKey.setId(ID);
    enterpriseKey.setName(NAME);
    enterpriseKey.setEnterpriseId(ENTERPRISE_ID);
    enterpriseKey.setAffiliateType(AFFILIATE_TYPE);
    enterpriseKey.setCorporateIndicator(CORPORATE_INDICATOR);
    enterpriseKey.setBusinessUnitId(BUSINESS_UNIT_ID);
    enterpriseKey.setBusinessUnitName(BUSINESS_UNIT_NAME);
    enterpriseKey.setLegalDoc(LEGAL_DOC);
    enterpriseKey.setReportingProductId(REPORTING_PRODUCT_ID);
    enterpriseKey.setReportingProduct(REPORTING_PRODUCT);
    enterpriseKey.setProductLineId(PRODUCT_LINE_ID);
    enterpriseKey.setProductLineName(PRODUCT_LINE_NAME);
    enterpriseKey.setClientCompanyId(CLIENT_COMPANY_ID);
    enterpriseKey.setClientCompanyName(CLIENT_COMPANY_NAME);
    enterpriseKey.setVersionNumber(VERSION_NUMBER);
    enterpriseKey.setVersionDate(VERSION_DATE);
    enterpriseKey.setLegalCode(LEGAL_CODE);
    enterpriseKey.setLegalName(LEGAL_NAME);
    enterpriseKey.setDeal(DEAL);
    enterpriseKey.setProductStructure(PRODUCT_STRUCTURE);
    enterpriseKey.setEffectiveStatusCd(EFFECTIVE_CODE);
  }

  @Test
  public void equalsGetTest() {
    Long id = enterpriseKey.getId();
    String name = enterpriseKey.getName();
    Long enterpriseId = enterpriseKey.getEnterpriseId();
    String affiliateType = enterpriseKey.getAffiliateType();
    String corporateIndicator = enterpriseKey.getCorporateIndicator();
    String businessUnitId = enterpriseKey.getBusinessUnitId();
    String businessUnitName = enterpriseKey.getBusinessUnitName();
    String legalDoc = enterpriseKey.getLegalDoc();
    String reportingProductId = enterpriseKey.getReportingProductId();
    String reportingProduct = enterpriseKey.getReportingProduct();
    Long productLineId = enterpriseKey.getProductLineId();
    String productLineName = enterpriseKey.getProductLineName();
    String clientCompanyId = enterpriseKey.getClientCompanyId();
    String clientCompanyName = enterpriseKey.getClientCompanyName();
    Long versionNumber = enterpriseKey.getVersionNumber();
    Long versionDate = enterpriseKey.getVersionDate();
    String legalCode = enterpriseKey.getLegalCode();
    String legalName = enterpriseKey.getLegalName();
    String deal = enterpriseKey.getDeal();
    ProductStructure productStructure = enterpriseKey.getProductStructure();
    String effectiveStatusCd = enterpriseKey.getEffectiveStatusCd();

    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(enterpriseId).isEqualTo(ENTERPRISE_ID);
    assertThat(affiliateType).isEqualTo(AFFILIATE_TYPE);
    assertThat(corporateIndicator).isEqualTo(CORPORATE_INDICATOR);
    assertThat(businessUnitId).isEqualTo(BUSINESS_UNIT_ID);
    assertThat(businessUnitName).isEqualTo(BUSINESS_UNIT_NAME);
    assertThat(legalDoc).isEqualTo(LEGAL_DOC);
    assertThat(reportingProductId).isEqualTo(REPORTING_PRODUCT_ID);
    assertThat(reportingProduct).isEqualTo(REPORTING_PRODUCT);
    assertThat(productLineId).isEqualTo(PRODUCT_LINE_ID);
    assertThat(productLineName).isEqualTo(PRODUCT_LINE_NAME);
    assertThat(clientCompanyId).isEqualTo(CLIENT_COMPANY_ID);
    assertThat(clientCompanyName).isEqualTo(CLIENT_COMPANY_NAME);
    assertThat(versionNumber).isEqualTo(VERSION_NUMBER);
    assertThat(versionDate).isEqualTo(VERSION_DATE);
    assertThat(legalCode).isEqualTo(LEGAL_CODE);
    assertThat(legalName).isEqualTo(LEGAL_NAME);
    assertThat(deal).isEqualTo(DEAL);
    assertThat(productStructure).isEqualTo(PRODUCT_STRUCTURE);

  }

  @Test
  public void overrideEqualsTest() {
    boolean same = enterpriseKey.equals(enterpriseKey);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = enterpriseKey.equals(new EnterpriseKey());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    EnterpriseKey enterpriseKey1 = new EnterpriseKey();
    enterpriseKey1.setId(1L);
    boolean same = enterpriseKey.equals(enterpriseKey1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    EnterpriseKey enterpriseKey1 = new EnterpriseKey();
    enterpriseKey1.setId(null);
    boolean same = enterpriseKey.equals(enterpriseKey1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    EnterpriseKey enterpriseKey1 = new EnterpriseKey();
    enterpriseKey1.setId(ID);
    enterpriseKey.setId(null);
    boolean same = enterpriseKey.equals(enterpriseKey1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = enterpriseKey.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = enterpriseKey.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = enterpriseKey.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = enterpriseKey.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
