package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ContactUnitTest {

  private static final Long ID = 1L;
  private static final String SALUTATION = "SALUTATION";
  private static final String FIRST_NAME = "FIRSTNAME";
  private static final String LAST_NAME = "LASTNAME";
  private static final String CONTACT_COMPANY_NAME = "CONTACTCOMPANYNAME";
  private static final String ADDRESS1 = "ADDRESS1";
  private static final String ADDRESS2 = "ADDRESS2";
  private static final String CITY = "CITY";
  private static final State STATE = null;
  private static final String POSTAL_CODE = "POSTAL_CODE";
  private static final String PHONE = "PHONE";
  private static final String FAX = "FAX";
  private static final String MOBILE = "MOBILE";
  private static final String EMAIL = "EMAIL";
  private static final String COMMENTS = "COMMENTS";
  private static final String PROVINCE = "PROVINCE";
  private static final Integer INTERNATIONAL = 1;

  private static final Country COUNTRY = new Country();

  private Contact contact;

  String expectedResult = "Contact{" +
    "id=" + ID +
    ", salutation='" + SALUTATION + "'" +
    ", firstName='" + FIRST_NAME + "'" +
    ", lastName='" + LAST_NAME + "'" +
    ", contactCompanyName='" + CONTACT_COMPANY_NAME + "'" +
    ", address1='" + ADDRESS1 + "'" +
    ", address2='" + ADDRESS2 + "'" +
    ", city='" + CITY + "'" +
    ", state='" + STATE + "'" +
    ", postalCode='" + POSTAL_CODE + "'" +
    ", phoneNumber='" + PHONE + "'" +
    ", faxNumber='" + FAX + "'" +
    ", mobileNumber='" + MOBILE + "'" +
    ", email='" + EMAIL + "'" +
    ", comments='" + COMMENTS + "'" +
    ", province='" + PROVINCE + "'" +
    ", international='" + INTERNATIONAL + "'" +
    '}';

  @Before
  public void setup() {

    contact = new Contact();
    contact.setId(ID);
    contact.setSalutation(SALUTATION);
    contact.setFirstName(null);
    contact.setFirstName(FIRST_NAME);
    contact.setContactCompanyName(null);
    contact.setContactCompanyName(CONTACT_COMPANY_NAME);
    contact.setLastName(null);
    contact.setLastName(LAST_NAME);
    contact.setAddress1(ADDRESS1);
    contact.setAddress2(ADDRESS2);
    contact.setCity(CITY);
    contact.setState(null);
    contact.setPostalCode(POSTAL_CODE);
    contact.setPhoneNumber(PHONE);
    contact.setFaxNumber(FAX);
    contact.setMobileNumber(MOBILE);
    contact.setEmail(EMAIL);
    contact.setComments(COMMENTS);
    contact.setProvince(PROVINCE);
    contact.setInternational(INTERNATIONAL);
    contact.setCountry(COUNTRY);

  }

  @Test
  public void equalsGetTest() {
    Long id = contact.getId();
    String salutation = contact.getSalutation();
    String firstName = contact.getFirstName();
    String lastName = contact.getLastName();
    String contactCompanyName = contact.getContactCompanyName();
    String address1 = contact.getAddress1();
    String address2 = contact.getAddress2();
    String city = contact.getCity();
    State state = contact.getState();
    String postalCode = contact.getPostalCode();
    String phoneNumber = contact.getPhoneNumber();
    String faxNumber = contact.getFaxNumber();
    String mobileNumber = contact.getMobileNumber();
    String email = contact.getEmail();
    String comments = contact.getComments();
    String province = contact.getProvince();
    Integer international = contact.getInternational();
    Country country = contact.getCountry();

    assertThat(id).isEqualTo(ID);
    assertThat(salutation).isEqualTo(SALUTATION);
    assertThat(firstName).isEqualTo(FIRST_NAME);
    assertThat(lastName).isEqualTo(LAST_NAME);
    assertThat(contactCompanyName).isEqualTo(CONTACT_COMPANY_NAME);
    assertThat(address1).isEqualTo(ADDRESS1);
    assertThat(address2).isEqualTo(ADDRESS2);
    assertThat(city).isEqualTo(CITY);
    assertThat(state).isEqualTo(STATE);
    assertThat(postalCode).isEqualTo(POSTAL_CODE);
    assertThat(phoneNumber).isEqualTo(PHONE);
    assertThat(faxNumber).isEqualTo(FAX);
    assertThat(mobileNumber).isEqualTo(MOBILE);
    assertThat(email).isEqualTo(EMAIL);
    assertThat(comments).isEqualTo(COMMENTS);
    assertThat(province).isEqualTo(PROVINCE);
    assertThat(international).isEqualTo(INTERNATIONAL);
    assertThat(international).isEqualTo(INTERNATIONAL);
    assertThat(country).isEqualTo(COUNTRY);

  }

  @Test
  public void overrideEqualsTest() {
    boolean same = contact.equals(contact);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = contact.equals(new Contact());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    Contact contact1 = new Contact();
    contact1.setId(1L);
    boolean same = contact.equals(contact1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    Contact contact1 = new Contact();
    contact1.setId(null);
    boolean same = contact.equals(contact1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    Contact contact1 = new Contact();
    contact1.setId(ID);
    contact.setId(null);
    boolean same = contact.equals(contact1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = contact.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = contact.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = contact.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = contact.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
