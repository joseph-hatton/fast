package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class SeriatimTypeUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  private SeriatimType seriatimType;

  String expectedResult = "SeriatimType{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    '}';

  @Before
  public void setup() {
    seriatimType = new SeriatimType();
    seriatimType.setId(ID);
    seriatimType.setName(NAME);
    seriatimType.setDescription(DESCRIPTION);
  }

  @Test
  public void equalsGetTest() {
    Long id = seriatimType.getId();
    String name = seriatimType.getName();
    String description = seriatimType.getDescription();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = seriatimType.equals(seriatimType);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = seriatimType.equals(new SeriatimType());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    SeriatimType seriatimType1 = new SeriatimType();
    seriatimType1.setId(1L);
    boolean same = seriatimType.equals(seriatimType1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    SeriatimType seriatimType1 = new SeriatimType();
    seriatimType1.setId(null);
    boolean same = seriatimType.equals(seriatimType1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    SeriatimType seriatimType1 = new SeriatimType();
    seriatimType1.setId(ID);
    seriatimType.setId(null);
    boolean same = seriatimType.equals(seriatimType1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = seriatimType.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = seriatimType.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = seriatimType.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = seriatimType.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
