package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class RiskManagementUnitTest {

  private static final Long ID = 1L;

  private static final BigDecimal AMOUNT = new BigDecimal(1.1);
  private static final BigDecimal CONVERTED_AMOUNT = new BigDecimal(2.2);
  private static final String UPLOAD_TEXT = "UPLOAD_TEXT";
  private static final String STATUS_MSG = "STATUS_MSG";

  private static final Country COUNTRY = new Country();
  private static final Currency CURRENCY = new Currency();
  private static final RiskCriteria RISK_CRITERIA = new RiskCriteria();
  private static final LocalDate MONTH_YEAR = LocalDate.now();

  private static final String CREATE_USER = "CREATE_USER";
  private static final String LAST_USER = "LAST_USER";

  private static final ZonedDateTime CREATED_TS = ZonedDateTime.now();
  private static final ZonedDateTime LAST_DATE = ZonedDateTime.now();


  private RiskManagement riskManagement;

  String expectedResult = "RiskManagement{" +
    "id=" + ID +
    '}';

  @Before
  public void setup() {
    riskManagement = new RiskManagement();
    riskManagement.setId(ID);
    riskManagement.setAmount(AMOUNT);
    riskManagement.setConvertedAmount(CONVERTED_AMOUNT);
    riskManagement.setUploadText(UPLOAD_TEXT);
    riskManagement.setStatusMessage(STATUS_MSG);
    riskManagement.setCountry(COUNTRY);
    riskManagement.setCurrency(CURRENCY);
    riskManagement.setRiskCriteria(RISK_CRITERIA);
    riskManagement.setMonthYear(MONTH_YEAR);

    riskManagement.setCreationTs(CREATED_TS);
    riskManagement.setCreationUser(CREATE_USER);
    riskManagement.setLastUpdate(LAST_DATE);
    riskManagement.setLastUser(LAST_USER);
  }

  @Test
  public void equalsGetTest() {
    Long id = riskManagement.getId();
    BigDecimal amount =  riskManagement.getAmount();
    BigDecimal convertedAmount = riskManagement.getConvertedAmount();
    String uploadText = riskManagement.getUploadText();
    String statusMessage = riskManagement.getStatusMessage();
    Country country =  riskManagement.getCountry();
    Currency currency =  riskManagement.getCurrency();
    RiskCriteria riskCriteria =  riskManagement.getRiskCriteria();
    LocalDate monthYear =  riskManagement.getMonthYear();

    String creationUser = riskManagement.getCreationUser();
    String lastUser = riskManagement.getLastUser();
    ZonedDateTime creationTs = riskManagement.getCreationTs();
    ZonedDateTime lastUpdate = riskManagement.getLastUpdate();

    assertThat(id).isEqualTo(ID);
    assertThat(amount).isEqualTo(AMOUNT);
    assertThat(convertedAmount).isEqualTo(CONVERTED_AMOUNT);
    assertThat(uploadText).isEqualTo(UPLOAD_TEXT);
    assertThat(statusMessage).isEqualTo(STATUS_MSG);
    assertThat(country).isEqualTo(COUNTRY);
    assertThat(currency).isEqualTo(CURRENCY);
    assertThat(riskCriteria).isEqualTo(RISK_CRITERIA);
    assertThat(monthYear).isEqualTo(MONTH_YEAR);
    assertThat(creationUser).isEqualTo(CREATE_USER);
    assertThat(creationTs).isEqualTo(CREATED_TS);
    assertThat(lastUpdate).isEqualTo(LAST_DATE);
    assertThat(lastUser).isEqualTo(LAST_USER);

  }

  @Test
  public void overrideEqualsTest() {
    boolean same = riskManagement.equals(riskManagement);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = riskManagement.equals(new RiskManagement());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    RiskManagement riskManagement1 = new RiskManagement();
    riskManagement1.setId(1L);
    boolean same = riskManagement.equals(riskManagement1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    RiskManagement riskManagement1 = new RiskManagement();
    riskManagement1.setId(null);
    boolean same = riskManagement.equals(riskManagement1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    RiskManagement riskManagement1 = new RiskManagement();
    riskManagement1.setId(ID);
    riskManagement.setId(null);
    boolean same = riskManagement.equals(riskManagement1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = riskManagement.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = riskManagement.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = riskManagement.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = riskManagement.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
