package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class TreatySeriatimUnitTest {

  private static final Long ID = 1L;
  private static final Long TREATY_ID = 1L;

  private static final String CREATE_USER = "CREATE_USER";
  private static final String LAST_USER = "LAST_USER";

  private static final ZonedDateTime CREATED_TS = ZonedDateTime.now();
  private static final ZonedDateTime LAST_DATE = ZonedDateTime.now();


  private TreatySeriatim treatySeriatim;

  String expectedResult = "TreatySeriatim{" +
    "id=" + ID +
    ", treatyId='" + TREATY_ID + "'" +
    '}';

  @Before
  public void setup() {
    treatySeriatim = new TreatySeriatim();
    treatySeriatim.setId(ID);
    treatySeriatim.setTreatyId(TREATY_ID);
    treatySeriatim.setCreationTs(CREATED_TS);
    treatySeriatim.setCreationUser(CREATE_USER);
    treatySeriatim.setLastUpdate(LAST_DATE);
    treatySeriatim.setLastUser(LAST_USER);
  }

  @Test
  public void equalsGetTest() {
    Long id = treatySeriatim.getId();
    Long treatyId = treatySeriatim.getTreatyId();
    String creationUser = treatySeriatim.getCreationUser();
    String lastUser = treatySeriatim.getLastUser();
    ZonedDateTime creationTs = treatySeriatim.getCreationTs();
    ZonedDateTime lastUpdate = treatySeriatim.getLastUpdate();

    assertThat(id).isEqualTo(ID);
    assertThat(treatyId).isEqualTo(TREATY_ID);
    assertThat(creationUser).isEqualTo(CREATE_USER);
    assertThat(creationTs).isEqualTo(CREATED_TS);
    assertThat(lastUpdate).isEqualTo(LAST_DATE);
    assertThat(lastUser).isEqualTo(LAST_USER);

  }

  @Test
  public void overrideEqualsTest() {
    boolean same = treatySeriatim.equals(treatySeriatim);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = treatySeriatim.equals(new TreatySeriatim());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    TreatySeriatim treatySeriatim1 = new TreatySeriatim();
    treatySeriatim1.setId(1L);
    boolean same = treatySeriatim.equals(treatySeriatim1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    TreatySeriatim treatySeriatim1 = new TreatySeriatim();
    treatySeriatim1.setId(null);
    boolean same = treatySeriatim.equals(treatySeriatim1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    TreatySeriatim treatySeriatim1 = new TreatySeriatim();
    treatySeriatim1.setId(ID);
    treatySeriatim.setId(null);
    boolean same = treatySeriatim.equals(treatySeriatim1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = treatySeriatim.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = treatySeriatim.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = treatySeriatim.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = treatySeriatim.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
