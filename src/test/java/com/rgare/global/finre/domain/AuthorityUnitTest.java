package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorityUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";

  private Authority authority;

  String expectedResult = "Authority{" +
    "name='NAME'" +
    '}';

  @Before
  public void setup() {

    authority = new Authority();
    authority.setName(NAME);

  }

  @Test
  public void equalsGetTest() {
    String name = authority.getName();

    assertThat(name).isEqualTo(NAME);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = authority.equals(authority);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = authority.equals(new Authority());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsNameIsNullTest() {
    authority.setName(null);
    Authority thisAuthority = new Authority();
    thisAuthority.setName(NAME);
    boolean same = authority.equals(thisAuthority);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareNameTest() {
    authority.setName(NAME);
    Authority thisAuthority = new Authority();
    thisAuthority.setName(NAME);
    boolean same = authority.equals(thisAuthority);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsCompareNameNotEqualsTest() {
    authority.setName(NAME);
    Authority thisAuthority = new Authority();
    thisAuthority.setName("NAME1");
    boolean same = authority.equals(thisAuthority);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareNameNotEqualTest() {
    authority.setName(NAME);
    Authority thisAuthority = new Authority();
    thisAuthority.setName("NAME1");
    boolean same = authority.equals(thisAuthority);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = authority.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = authority.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = authority.hashCode();

    assertThat(actual).isGreaterThan(ID.intValue());
  }


}
