package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class SettlementTypeUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";
  private static final String DEFAULT_FLAG = "FLAG";
  private static final String LAST_USER = "USER";
  private static final String CREATION_USER = "CREATION_USER";
  private static final LocalDate LAST_UPDATE = LocalDate.now();
  private static final LocalDate CREATTION_TS = LocalDate.now();
  private static final LocalDate EFF_BEGIN_TS = LocalDate.now();
  private static final LocalDate EFF_END_TS = LocalDate.now();

  private static final Integer SORT_ORDER = 1;

  private SettlementType settlementType;

  String expectedResult = "SettlementType{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    ", sortOrder='" + SORT_ORDER + "'" +
    ", default_flag='" + DEFAULT_FLAG + "'" +
    ", lastUser='" + LAST_USER + "'" +
    ", lastUpdate='" + LAST_UPDATE + "'" +
    ", creationTS='" + CREATTION_TS + "'" +
    ", creationUser='" + CREATION_USER + "'" +
    ", effBeginTs='" + EFF_BEGIN_TS + "'" +
    ", effEndTs='" + EFF_END_TS + "'" +
    '}';

  @Before
  public void setup() {
    settlementType = new SettlementType();
    settlementType.setId(ID);
    settlementType.setName(NAME);
    settlementType.setDescription(DESCRIPTION);
    settlementType.setSortOrder(SORT_ORDER);
    settlementType.setDefault_flag(DEFAULT_FLAG);
    settlementType.setLastUser(LAST_USER);
    settlementType.setLastUpdate(LAST_UPDATE);
    settlementType.setCreationTS(CREATTION_TS);
    settlementType.setCreationUser(CREATION_USER);
    settlementType.setEffBeginTs(EFF_BEGIN_TS);
    settlementType.setEffEndTs(EFF_END_TS);
  }

  @Test
  public void equalsGetTest() {
    Long id = settlementType.getId();
    String name = settlementType.getName();
    String description = settlementType.getDescription();
    Integer sortOrder = settlementType.getSortOrder();
    String defaultFlag = settlementType.getDefault_flag();
    String lastUser = settlementType.getLastUser();
    LocalDate lastUpdate = settlementType.getLastUpdate();
    LocalDate creationTS = settlementType.getCreationTS();
    String creationUser = settlementType.getCreationUser();
    LocalDate effBeginTs = settlementType.getEffBeginTs();
    LocalDate effEndTs = settlementType.getEffEndTs();

    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
    assertThat(sortOrder).isEqualTo(SORT_ORDER);
    assertThat(defaultFlag).isEqualTo(DEFAULT_FLAG);
    assertThat(lastUser).isEqualTo(LAST_USER);
    assertThat(lastUpdate).isEqualTo(LAST_UPDATE);
    assertThat(creationTS).isEqualTo(CREATTION_TS);
    assertThat(creationUser).isEqualTo(CREATION_USER);
    assertThat(effBeginTs).isEqualTo(EFF_BEGIN_TS);
    assertThat(effEndTs).isEqualTo(EFF_END_TS);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = settlementType.equals(settlementType);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = settlementType.equals(new SettlementType());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    SettlementType settlementType1 = new SettlementType();
    settlementType1.setId(1L);
    boolean same = settlementType.equals(settlementType1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    SettlementType settlementType1 = new SettlementType();
    settlementType1.setId(null);
    boolean same = settlementType.equals(settlementType1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    SettlementType settlementType1 = new SettlementType();
    settlementType1.setId(ID);
    settlementType.setId(null);
    boolean same = settlementType.equals(settlementType1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = settlementType.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = settlementType.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = settlementType.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = settlementType.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
