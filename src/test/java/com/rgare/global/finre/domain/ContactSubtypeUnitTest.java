package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ContactSubtypeUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  private ContactSubtype contactSubtype;

  String expectedResult = "ContactSubtype{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    '}';

  @Before
  public void setup() {

    contactSubtype = new ContactSubtype();
    contactSubtype.setId(ID);
    contactSubtype.setDescription(DESCRIPTION);
    contactSubtype.setName(NAME);

    contactSubtype.name(NAME);
  }

  @Test
  public void equalsGetTest() {
    Long id = contactSubtype.getId();
    String name = contactSubtype.getName();
    String description = contactSubtype.getDescription();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = contactSubtype.equals(contactSubtype);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = contactSubtype.equals(new ContactSubtype());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    ContactSubtype contactSubtype1 = new ContactSubtype();
    contactSubtype1.setId(1L);
    boolean same = contactSubtype.equals(contactSubtype1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    ContactSubtype contactSubtype1 = new ContactSubtype();
    contactSubtype1.setId(null);
    boolean same = contactSubtype.equals(contactSubtype1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    ContactSubtype contactSubtype1 = new ContactSubtype();
    contactSubtype1.setId(ID);
    contactSubtype.setId(null);
    boolean same = contactSubtype.equals(contactSubtype1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = contactSubtype.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = contactSubtype.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = contactSubtype.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = contactSubtype.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
