package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserUnitTest {

  private static final Long ID = 1L;

  private static final String LOGIN = "login";
  private static final String FIRSTNAME = "FIRSTNAME";
  private static final String LASTNAME = "LASTNAME";
  private static final String EMAIL = "EMAIL";
  private static final String IMAGEURL = "IMAGEURL";
  private static final String PASSWORD = "PASSWORD";
  private static final boolean ACTIVATED = true;
  private static final String LANGKEY = "LANGKEY";
  private static final String ACTIVATION_KEY = "ACTIVATION";
  private static final String RESET_KEY = "RESET";
  private static final ZonedDateTime RESET_DATE = ZonedDateTime.now();

  private static final String SERIES = "SERIES";

  private static final int LOGIN_HASHCODE = "login".hashCode();

  private Set<Authority> authorities = new HashSet<>();

  private Set<PersistentToken> persistentTokens = new HashSet<>();

  private User user;

  String expectedResult = "User{" +
    "login='" + LOGIN + '\'' +
    ", firstName='" + FIRSTNAME + '\'' +
    ", lastName='" + LASTNAME + '\'' +
    ", email='" + EMAIL + '\'' +
    ", imageUrl='" + IMAGEURL + '\'' +
    ", activated='" + ACTIVATED + '\'' +
    ", langKey='" + LANGKEY + '\'' +
    ", activationKey='" + ACTIVATION_KEY + '\'' +
    "}";

  @Before
  public void setup() {

    user = new User();
    user.setId(ID);
    user.setLogin(LOGIN);
    user.setFirstName(FIRSTNAME);
    user.setLastName(LASTNAME);
    user.setPassword(PASSWORD);
    user.setEmail(EMAIL);
    user.setActivated(ACTIVATED);
    user.setLangKey(LANGKEY);
    user.setImageUrl(IMAGEURL);
    user.setActivationKey(ACTIVATION_KEY);
    user.setResetKey(RESET_KEY);
    user.setResetDate(RESET_DATE);

    authorities.add(new Authority());
    user.setAuthorities(authorities);

    PersistentToken persistentToken = new PersistentToken();
    persistentToken.setSeries(SERIES);
    persistentTokens.add(persistentToken);
    user.setPersistentTokens(persistentTokens);
  }

  @Test
  public void equalsGetTest() {
    Long id = user.getId();
    String login = user.getLogin();
    String firstName = user.getFirstName();
    String lastName = user.getLastName();
    String password = user.getPassword();
    String email = user.getEmail();
    boolean activated = user.getActivated();
    String langKey = user.getLangKey();
    String imageUrl = user.getImageUrl();
    String activationKey = user.getActivationKey();
    String resetKey = user.getResetKey();
    ZonedDateTime resetDate = user.getResetDate();
    Set<PersistentToken> tokens = user.getPersistentTokens();

    assertThat(id).isEqualTo(ID);
    assertThat(login).isEqualTo(LOGIN);
    assertThat(firstName).isEqualTo(FIRSTNAME);
    assertThat(lastName).isEqualTo(LASTNAME);
    assertThat(password).isEqualTo(PASSWORD);
    assertThat(email).isEqualTo(EMAIL);
    assertThat(activated).isEqualTo(ACTIVATED);
    assertThat(langKey).isEqualTo(LANGKEY);
    assertThat(imageUrl).isEqualTo(IMAGEURL);
    assertThat(activationKey).isEqualTo(ACTIVATION_KEY);
    assertThat(resetKey).isEqualTo(RESET_KEY);
    assertThat(resetDate).isEqualTo(RESET_DATE);
    assertThat(tokens).isEqualTo(persistentTokens);

  }

  @Test
  public void overrideEqualsTest() {
    boolean same = user.equals(user);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = user.equals(new User());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsObjectNotEqualTest() {
    boolean same = user.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = user.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = user.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int expected = user.getLogin().hashCode();

    int actual = user.hashCode();
    assertThat(expected).isEqualTo(actual);
  }


}
