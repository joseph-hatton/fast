package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class TreatyManagementUnitTest {

  private static final Long ID = 1L;
  private static final BigDecimal QUOTE_SHARE = new BigDecimal(1.1);
  private static final LocalDate SUMMARY_DATE = LocalDate.now();
  private static final Integer IS_ACTIVE = 1;
  private static final String INPUT_STATUS = "STATUS";
  private static final String GENERAL_COMMENT_REFUND = "GENERAL_COMMENT";
  private static final String CALENDARIZATION = "CALENDARIZATION";
  private static final String CALENDARIZATION_TREATY_REFER = "REFER";
  private static final String CONFIRMATION_REQUESTED = "CONFIRMATION";
  private static final String CONFIRMATION_REQUESTED_COMMENTS = "CONFIRMATION_COMMENTS";
  private static final String ACCOUNTING_TREATY_REFERENCE = "REFERENCE";
  private static final String QUOTA_SHARE_COMMENT = "QUOTA_COMMENT";
  private static final String TREATY_REFERENCE_RECAPTURE = "RECAPTURE";
  private static final String RECAPTURE_COMMENT = "RECAPTURE_COMMENT";
  private static final String CALENDARIZATION_COMMENTS = "CALENDARIZATION_COMMENTS";
  private static final String REFUND_ELIMINATION_FLAG = "FLAG";
  private static final String REFUND_ELIMINATION_REF = "REF";
  private static final String REFUND_COMMENTS = "REFUND_COMMENTS";
  private static final Treaty TREATY = new Treaty();
  private static final Frequency ACCOUNTING_FREQUENCY = new Frequency();

  private TreatyManagement treatyManagement;

  String expectedResult = "TreatyManagement{" +
    "id=" + ID +
    ", quotaShare='" + QUOTE_SHARE + "'" +
    ", summaryDate='" + SUMMARY_DATE + "'" +
    ", isActive='" + IS_ACTIVE + "'" +
    ", accountingFrequency='" + ACCOUNTING_FREQUENCY  + "'" +
    ", calendarization='" + CALENDARIZATION  + "'" +
    ", calendarizationTreatyRefer='" + CALENDARIZATION_TREATY_REFER  + "'" +
    ", confirmationRequested='" + CONFIRMATION_REQUESTED  + "'" +
    ", confirmationRequestedComment='" + CONFIRMATION_REQUESTED_COMMENTS  + "'" +
    ", accountingTreatyReference='" + ACCOUNTING_TREATY_REFERENCE  + "'" +
    '}';

  @Before
  public void setup() {
    treatyManagement = new TreatyManagement();
    treatyManagement.setId(ID);
    treatyManagement.setIsActive(IS_ACTIVE);
    treatyManagement.setQuotaShare(QUOTE_SHARE);
    treatyManagement.setSummaryDate(SUMMARY_DATE);
    treatyManagement.setInputStatus(INPUT_STATUS);
    treatyManagement.setGeneralCommentRefund(GENERAL_COMMENT_REFUND);
    treatyManagement.setTreaty(TREATY);
    treatyManagement.setCalendarization(CALENDARIZATION);
    treatyManagement.setCalendarizationTreatyRefer(CALENDARIZATION_TREATY_REFER);
    treatyManagement.setConfirmationRequested(CONFIRMATION_REQUESTED);
    treatyManagement.setConfirmationRequestedComment(CONFIRMATION_REQUESTED_COMMENTS);
    treatyManagement.setAccountingTreatyReference(ACCOUNTING_TREATY_REFERENCE);
    treatyManagement.setQuotaShareComment(QUOTA_SHARE_COMMENT);
    treatyManagement.setTreatyReferenceRecapture(TREATY_REFERENCE_RECAPTURE);
    treatyManagement.setRecaptureComment(RECAPTURE_COMMENT);
    treatyManagement.setCalendarizationComments(CALENDARIZATION_COMMENTS);
    treatyManagement.setRefundEliminationFlag(REFUND_ELIMINATION_FLAG);
    treatyManagement.setRefundEliminationRef(REFUND_ELIMINATION_REF);
    treatyManagement.setRefundComments(REFUND_COMMENTS);
    treatyManagement.setAccountingFrequency(ACCOUNTING_FREQUENCY);
  }

  @Test
  public void equalsGetTest() {
    Long id = treatyManagement.getId();
    Integer isActive = treatyManagement.getIsActive();
    BigDecimal quotaShare = treatyManagement.getQuotaShare();
    LocalDate summaryDate = treatyManagement.getSummaryDate();
    String inputStatus = treatyManagement.getInputStatus();
    String generalCommentRefund = treatyManagement.getGeneralCommentRefund();
    Treaty treaty = treatyManagement.getTreaty();
    String calendarization = treatyManagement.getCalendarization();
    String calendarizationTreatyRefer = treatyManagement.getCalendarizationTreatyRefer();
    String confirmationRequested = treatyManagement.getConfirmationRequested();
    String confirmationRequestedComment = treatyManagement.getConfirmationRequestedComment();
    String accountingTreatyReference = treatyManagement.getAccountingTreatyReference();
    String quotaShareComment = treatyManagement.getQuotaShareComment();
    String treatyReferenceRecapture = treatyManagement.getTreatyReferenceRecapture();
    String recaptureComment = treatyManagement.getRecaptureComment();
    String calendarizationComments = treatyManagement.getCalendarizationComments();
    String refundEliminationFlag = treatyManagement.getRefundEliminationFlag();
    String refundEliminationRef = treatyManagement.getRefundEliminationRef();
    String refundComments = treatyManagement.getRefundComments();
    Frequency accountingFrequency = treatyManagement.getAccountingFrequency();

    assertThat(id).isEqualTo(ID);
    assertThat(isActive).isEqualTo(IS_ACTIVE);
    assertThat(quotaShare).isEqualTo(QUOTE_SHARE);
    assertThat(summaryDate).isEqualTo(SUMMARY_DATE);
    assertThat(inputStatus).isEqualTo(INPUT_STATUS);
    assertThat(generalCommentRefund).isEqualTo(GENERAL_COMMENT_REFUND);
    assertThat(treaty).isEqualTo(TREATY);
    assertThat(calendarization).isEqualTo(CALENDARIZATION);
    assertThat(calendarizationTreatyRefer).isEqualTo(CALENDARIZATION_TREATY_REFER);
    assertThat(confirmationRequested).isEqualTo(CONFIRMATION_REQUESTED);
    assertThat(confirmationRequestedComment).isEqualTo(CONFIRMATION_REQUESTED_COMMENTS);
    assertThat(accountingTreatyReference).isEqualTo(ACCOUNTING_TREATY_REFERENCE);
    assertThat(quotaShareComment).isEqualTo(QUOTA_SHARE_COMMENT);
    assertThat(treatyReferenceRecapture).isEqualTo(TREATY_REFERENCE_RECAPTURE);
    assertThat(recaptureComment).isEqualTo(RECAPTURE_COMMENT);
    assertThat(calendarizationComments).isEqualTo(CALENDARIZATION_COMMENTS);
    assertThat(refundEliminationFlag).isEqualTo(REFUND_ELIMINATION_FLAG);
    assertThat(refundEliminationRef).isEqualTo(REFUND_ELIMINATION_REF);
    assertThat(refundComments).isEqualTo(REFUND_COMMENTS);
    assertThat(accountingFrequency).isEqualTo(ACCOUNTING_FREQUENCY);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = treatyManagement.equals(treatyManagement);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = treatyManagement.equals(new TreatyManagement());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    TreatyManagement treatyManagement1 = new TreatyManagement();
    treatyManagement1.setId(1L);
    boolean same = treatyManagement.equals(treatyManagement1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    TreatyManagement treatyManagement1 = new TreatyManagement();
    treatyManagement1.setId(null);
    boolean same = treatyManagement.equals(treatyManagement1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    TreatyManagement treatyManagement1 = new TreatyManagement();
    treatyManagement1.setId(ID);
    treatyManagement.setId(null);
    boolean same = treatyManagement.equals(treatyManagement1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = treatyManagement.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = treatyManagement.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = treatyManagement.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = treatyManagement.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
