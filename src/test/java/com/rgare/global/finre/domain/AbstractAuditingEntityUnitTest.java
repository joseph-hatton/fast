package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class AbstractAuditingEntityUnitTest {

  private static final Long ID = 1L;


  private static final String CREATE_USER = "CREATE_USER";
  private static final String LAST_USER = "LAST_USER";

  private static final ZonedDateTime CREATED_TS = ZonedDateTime.now();
  private static final ZonedDateTime LAST_DATE = ZonedDateTime.now();


  private AbstractAuditingEntity abstractAuditingEntity;

  @Before
  public void setup() {
    abstractAuditingEntity = new AbstractAuditingEntitySubClass();

    abstractAuditingEntity.setCreatedDate(CREATED_TS);
    abstractAuditingEntity.setCreatedBy(CREATE_USER);
    abstractAuditingEntity.setLastModifiedDate(LAST_DATE);
    abstractAuditingEntity.setLastModifiedBy(LAST_USER);
  }

  @Test
  public void equalsGetTest() {

    String creationUser = abstractAuditingEntity.getCreatedBy();
    String lastUser = abstractAuditingEntity.getLastModifiedBy();
    ZonedDateTime creationTs = abstractAuditingEntity.getCreatedDate();
    ZonedDateTime lastUpdate = abstractAuditingEntity.getLastModifiedDate();

    assertThat(creationUser).isEqualTo(CREATE_USER);
    assertThat(creationTs).isEqualTo(CREATED_TS);
    assertThat(lastUpdate).isEqualTo(LAST_DATE);
    assertThat(lastUser).isEqualTo(LAST_USER);

  }

  @Test
  public void overrideEqualsTest() {
    boolean same = abstractAuditingEntity.equals(abstractAuditingEntity);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = abstractAuditingEntity.equals(new AbstractAuditingEntitySubClass());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = abstractAuditingEntity.equals(null);
    assertThat(same).isFalse();
  }

}
class AbstractAuditingEntitySubClass extends AbstractAuditingEntity {

}
