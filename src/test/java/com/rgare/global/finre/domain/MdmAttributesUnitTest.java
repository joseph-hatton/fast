package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class MdmAttributesUnitTest {

  private static final Long ID = 1L;
  private static final Long ENTERPRISE_ID = 1L;
  private static final Long DEAL_ID = 1L;
  private static final Long CLIENT_COMPANY_ID = 1L;
  private static final Long VERSION_NUMBER = 1L;
  private static final Long VERSION_DATE = 1L;


  private static final String AFFILIATE_TYPE = "TYPE";
  private static final String EK_COMMENT = "COMMENT";
  private static final String DEAL = "DEAL_NAME";
  private static final String US_TRANSFTER_RISK = "US_TRANSFER_RIST";

  private static final String EFFECTIVE_STATUS_CD = "STATUS_CD";
  private static final Long BUSINESS_UNIT_ID = 1L;
  private static final String BUSINESS_UNIT_NAME = "NAME";
  private static final String CORPORATE_INDICATOR = "INDICATOR";
  private static final String LEGAL_DOC= "LEGAL_DOC";
  private static final String LEGAL_CODE= "LEGAL_CODE";
  private static final String LEGAL_NAME= "LEGAL_NAME";
  private static final String REPORTING_PRODUCT_ID = "PRODUCT_ID";
  private static final String REPORTING_PRODUCT = "PRODUCT";

  private static final String PRODUCT_LINE_ID = "PRODUCT_LINE_ID";
  private static final String PRODUCT_LINE_NAME = "PRODUCT_LINE_NAME";
  private static final String CLIENT_COMPANY_NAME = "CLIENT_COMPANY_NAME";

  private static final String FORM_OF_REINSURANCE = "FORM_OF_REINSURANCE";
  private static final Long PRODUCT_GROUP_ID = 1L ;
  private static final String PRODUCT_GROUP_NAME = "PRODUCT_GROUP_NAME";
  private static final Long PRODUCT_TYPE_ID = 2L;
  private static final String PRODUCT_TYPE_NAME = "PRODUCT_TYPE_NAME";
  private static final String RISK_TRANSFER_GAAP = "RISK_TRANSFER_GAAP";
  private static final String RISK_TRANSFER_LOCAL = "RISK_TRANSFER_LOCAL";
  private static final String RISK_TRANSFER_STATE = "RISK_TRANSFER_STATE";

  private static final ProductStructure PRODUCT_STRUCTURE = new ProductStructure();

  private MdmAttributes mdmAttributes;

  String expectedResult = "MdmAttributes{" +
    "id=" + ID +
    ", enterpriseId='" + ENTERPRISE_ID + "'" +
    ", affiliateType='" + AFFILIATE_TYPE + "'" +
    ", businessUnitId='" + BUSINESS_UNIT_ID + "'" +
    ", businessUnitName='" + BUSINESS_UNIT_NAME + "'" +
    ", corporateIndicator='" + CORPORATE_INDICATOR + "'" +
    ", legalDoc='" + LEGAL_DOC + "'" +
    ", reportingProduct='" + REPORTING_PRODUCT + "'" +
    ", formOfReinsurance='" + FORM_OF_REINSURANCE + "'" +
    ", productGroupId='" + PRODUCT_GROUP_ID + "'" +
    ", productGroupName='" + PRODUCT_GROUP_NAME + "'" +
    ", productTypeId='" + PRODUCT_TYPE_ID + "'" +
    ", productTypeName='" + PRODUCT_TYPE_NAME + "'" +
    ", riskTransferGaap='" + RISK_TRANSFER_GAAP + "'" +
    ", riskTransferLocal='" + RISK_TRANSFER_LOCAL + "'" +
    ", riskTransferState='" + RISK_TRANSFER_STATE + "'" +
    '}';

  @Before
  public void setup() {

    mdmAttributes = new MdmAttributes();
    mdmAttributes.setId(ID);
    mdmAttributes.setEnterpriseId(ENTERPRISE_ID);
    mdmAttributes.setAffiliateType(AFFILIATE_TYPE);
    mdmAttributes.setEkComment(EK_COMMENT);
    mdmAttributes.setDeal(DEAL);
    mdmAttributes.setUsStatRisk(US_TRANSFTER_RISK);
    mdmAttributes.setEffectiveStatusCd(EFFECTIVE_STATUS_CD);
    mdmAttributes.setBusinessUnitId(BUSINESS_UNIT_ID);
    mdmAttributes.setBusinessUnitName(BUSINESS_UNIT_NAME);
    mdmAttributes.setCorporateIndicator(CORPORATE_INDICATOR);
    mdmAttributes.setLegalDoc(LEGAL_DOC);
    mdmAttributes.setReportingProductId(REPORTING_PRODUCT_ID);
    mdmAttributes.setReportingProduct(REPORTING_PRODUCT);
    mdmAttributes.setDealId(DEAL_ID);
    mdmAttributes.setClientCompanyId(CLIENT_COMPANY_ID);
    mdmAttributes.setClientCompanyName(CLIENT_COMPANY_NAME);
    mdmAttributes.setProductLineId(PRODUCT_LINE_ID);
    mdmAttributes.setProductLineName(PRODUCT_LINE_NAME);
    mdmAttributes.setLegalCode(LEGAL_CODE);
    mdmAttributes.setLegalName(LEGAL_NAME);
    mdmAttributes.setVersionNumber(VERSION_NUMBER);
    mdmAttributes.setVersionDate(VERSION_DATE);
    mdmAttributes.setProductStructure(PRODUCT_STRUCTURE);
    mdmAttributes.setFormOfReinsurance(FORM_OF_REINSURANCE);
    mdmAttributes.setProductGroupId(PRODUCT_GROUP_ID);
    mdmAttributes.setProductGroupName(PRODUCT_GROUP_NAME);
    mdmAttributes.setProductTypeId(PRODUCT_TYPE_ID);
    mdmAttributes.setProductTypeName(PRODUCT_TYPE_NAME);
    mdmAttributes.setRiskTransferGaap(RISK_TRANSFER_GAAP);
    mdmAttributes.setRiskTransferLocal(RISK_TRANSFER_LOCAL);
    mdmAttributes.setRiskTransferState(RISK_TRANSFER_STATE);
  }

  @Test
  public void equalsGetTest() {
    Long id = mdmAttributes.getId();
    Long enterpriseId = mdmAttributes.getEnterpriseId();
    String affiliateType = mdmAttributes.getAffiliateType();
    String ekComment = mdmAttributes.getEkComment();
    String deal = mdmAttributes.getDeal();
    String usStatRisk = mdmAttributes.getUsStatRisk();
    String effectiveStatusCd = mdmAttributes.getEffectiveStatusCd();
    Long businessUnitId = mdmAttributes.getBusinessUnitId();
    String businessUnitName = mdmAttributes.getBusinessUnitName();
    String corporateIndicator = mdmAttributes.getCorporateIndicator();
    String legalDoc = mdmAttributes.getLegalDoc();
    String reportingProductId = mdmAttributes.getReportingProductId();
    String reportingProduct = mdmAttributes.getReportingProduct();
    Long dealId = mdmAttributes.getDealId();
    Long clientCompanyId = mdmAttributes.getClientCompanyId();
    String clientCompanyName = mdmAttributes.getClientCompanyName();
    String productLineId = mdmAttributes.getProductLineId();
    String productLineName = mdmAttributes.getProductLineName();
    String legalCode = mdmAttributes.getLegalCode();
    String legalName = mdmAttributes.getLegalName();
    Long versionNumber = mdmAttributes.getVersionNumber();
    Long versionDate = mdmAttributes.getVersionDate();

    String formOfReinsurance = mdmAttributes.getFormOfReinsurance();
    Long productGroupId = mdmAttributes.getProductGroupId();
    String productGroupName = mdmAttributes.getProductGroupName();
    Long productTypeId = mdmAttributes.getProductTypeId();
    String productTypeName = mdmAttributes.getProductTypeName();
    String riskTransferGaap =  mdmAttributes.getRiskTransferGaap();
    String riskTransferLocal =  mdmAttributes.getRiskTransferLocal();
    String riskTransferState =  mdmAttributes.getRiskTransferState();

    ProductStructure productStructure = mdmAttributes.getProductStructure();

    assertThat(id).isEqualTo(ID);
    assertThat(enterpriseId).isEqualTo(ENTERPRISE_ID);
    assertThat(affiliateType).isEqualTo(AFFILIATE_TYPE);
    assertThat(ekComment).isEqualTo(EK_COMMENT);
    assertThat(deal).isEqualTo(DEAL);
    assertThat(usStatRisk).isEqualTo(US_TRANSFTER_RISK);
    assertThat(effectiveStatusCd).isEqualTo(EFFECTIVE_STATUS_CD);
    assertThat(businessUnitId).isEqualTo(BUSINESS_UNIT_ID);
    assertThat(businessUnitName).isEqualTo(BUSINESS_UNIT_NAME);
    assertThat(corporateIndicator).isEqualTo(CORPORATE_INDICATOR);
    assertThat(legalDoc).isEqualTo(LEGAL_DOC);
    assertThat(reportingProductId).isEqualTo(REPORTING_PRODUCT_ID);
    assertThat(reportingProduct).isEqualTo(REPORTING_PRODUCT);
    assertThat(dealId).isEqualTo(DEAL_ID);
    assertThat(clientCompanyId).isEqualTo(CLIENT_COMPANY_ID);
    assertThat(clientCompanyName).isEqualTo(CLIENT_COMPANY_NAME);
    assertThat(productLineId).isEqualTo(PRODUCT_LINE_ID);
    assertThat(productLineName).isEqualTo(PRODUCT_LINE_NAME);
    assertThat(legalCode).isEqualTo(LEGAL_CODE);
    assertThat(legalName).isEqualTo(LEGAL_NAME);
    assertThat(versionNumber).isEqualTo(VERSION_NUMBER);
    assertThat(versionDate).isEqualTo(VERSION_DATE);
    assertThat(formOfReinsurance).isEqualTo(FORM_OF_REINSURANCE);
    assertThat(productGroupId).isEqualTo(PRODUCT_GROUP_ID);
    assertThat(productGroupName).isEqualTo(PRODUCT_GROUP_NAME);
    assertThat(productTypeId).isEqualTo(PRODUCT_TYPE_ID);
    assertThat(productTypeName).isEqualTo(PRODUCT_TYPE_NAME);
    assertThat(riskTransferGaap).isEqualTo(RISK_TRANSFER_GAAP);
    assertThat(riskTransferLocal).isEqualTo(RISK_TRANSFER_LOCAL);
    assertThat(riskTransferState).isEqualTo(RISK_TRANSFER_STATE);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = mdmAttributes.equals(mdmAttributes);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = mdmAttributes.equals(new MdmAttributes());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    MdmAttributes mdmAttributes1 = new MdmAttributes();
    mdmAttributes1.setId(1L);
    boolean same = mdmAttributes.equals(mdmAttributes1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    MdmAttributes mdmAttributes1 = new MdmAttributes();
    mdmAttributes1.setId(null);
    boolean same = mdmAttributes.equals(mdmAttributes1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    MdmAttributes mdmAttributes1 = new MdmAttributes();
    mdmAttributes1.setId(ID);
    mdmAttributes.setId(null);
    boolean same = mdmAttributes.equals(mdmAttributes1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = mdmAttributes.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = mdmAttributes.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {
    String actual = mdmAttributes.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = mdmAttributes.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
