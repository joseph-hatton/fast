package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class FrequencyUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  private Frequency frequency;

  String expectedResult = "Frequency{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    '}';

  @Before
  public void setup() {
    frequency = new Frequency();
    frequency.setId(ID);
    frequency.setName(NAME);
    frequency.setDescription(DESCRIPTION);
  }

  @Test
  public void equalsGetTest() {
    Long id = frequency.getId();
    String name = frequency.getName();
    String description = frequency.getDescription();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = frequency.equals(frequency);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = frequency.equals(new Frequency());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    Frequency frequency1 = new Frequency();
    frequency1.setId(1L);
    boolean same = frequency.equals(frequency1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    Frequency frequency1 = new Frequency();
    frequency1.setId(null);
    boolean same = frequency.equals(frequency1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    Frequency frequency1 = new Frequency();
    frequency1.setId(ID);
    frequency.setId(null);
    boolean same = frequency.equals(frequency1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = frequency.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = frequency.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = frequency.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = frequency.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
