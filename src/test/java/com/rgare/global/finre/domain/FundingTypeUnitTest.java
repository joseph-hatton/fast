package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class FundingTypeUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  private FundingType fundingType;

  String expectedResult = "FundingType{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    '}';

  @Before
  public void setup() {
    fundingType = new FundingType();
    fundingType.setId(ID);
    fundingType.setName(NAME);
    fundingType.setDescription(DESCRIPTION);
  }

  @Test
  public void equalsGetTest() {
    Long id = fundingType.getId();
    String name = fundingType.getName();
    String description = fundingType.getDescription();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = fundingType.equals(fundingType);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = fundingType.equals(new FundingType());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    FundingType fundingType1 = new FundingType();
    fundingType1.setId(1L);
    boolean same = fundingType.equals(fundingType1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    FundingType fundingType1 = new FundingType();
    fundingType1.setId(null);
    boolean same = fundingType.equals(fundingType1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    FundingType fundingType1 = new FundingType();
    fundingType1.setId(ID);
    fundingType.setId(null);
    boolean same = fundingType.equals(fundingType1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = fundingType.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = fundingType.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = fundingType.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = fundingType.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
