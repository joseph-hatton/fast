package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ReportsUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";
  private static final String REPORT_LINK = "LINK";
  private static final String TABLEAU_LINK = "TABLEAU";
  private static final Integer FLAG_FORMAT = 1;

  private Reports reports;

  String expectedResult = "Reports{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", reportLink='" + REPORT_LINK + "'" +
    '}';

  @Before
  public void setup() {
    reports = new Reports();
    reports.setId(ID);
    reports.setName(NAME);
    reports.setDescription(DESCRIPTION);
    reports.setReportLink(REPORT_LINK);
    reports.setFlagFormat(FLAG_FORMAT);
    reports.setTableautLink(TABLEAU_LINK);

    reports.name(NAME);
    reports.reportLink(REPORT_LINK);
  }

  @Test
  public void equalsGetTest() {
    Long id = reports.getId();
    String name = reports.getName();
    String description = reports.getDescription();
    String reportLink = reports.getReportLink();
    Integer flagFormat = reports.getFlagFormat();
    String tableautLink = reports.getTableautLink();

    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
    assertThat(reportLink).isEqualTo(REPORT_LINK);
    assertThat(flagFormat).isEqualTo(FLAG_FORMAT);
    assertThat(tableautLink).isEqualTo(TABLEAU_LINK);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = reports.equals(reports);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = reports.equals(new Reports());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    Reports reports1 = new Reports();
    reports1.setId(1L);
    boolean same = reports.equals(reports1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    Reports reports1 = new Reports();
    reports1.setId(null);
    boolean same = reports.equals(reports1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    Reports reports1 = new Reports();
    reports1.setId(ID);
    reports.setId(null);
    boolean same = reports.equals(reports1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = reports.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = reports.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = reports.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = reports.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
