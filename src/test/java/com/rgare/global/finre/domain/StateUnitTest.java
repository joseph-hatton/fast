package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class StateUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";
  private static final String CODE = "CODE";

  private State state;

  String expectedResult = "State{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", code='" + CODE + "'" +
    ", description='" + DESCRIPTION + "'" +
    '}';

  @Before
  public void setup() {
    state = new State();
    state.setId(ID);
    state.setName(NAME);
    state.setDescription(DESCRIPTION);
    state.setCode(CODE);
  }

  @Test
  public void equalsGetTest() {
    Long id = state.getId();
    String name = state.getName();
    String description = state.getDescription();
    String code = state.getCode();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
    assertThat(code).isEqualTo(CODE);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = state.equals(state);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = state.equals(new State());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    State state1 = new State();
    state1.setId(1L);
    boolean same = state.equals(state1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    State state1 = new State();
    state1.setId(null);
    boolean same = state.equals(state1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    State state1 = new State();
    state1.setId(ID);
    state.setId(null);
    boolean same = state.equals(state1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = state.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = state.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = state.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = state.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
