package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuditTransactionsUnitTest {

  private static final Long ID = 1L;
  private static final Long TREATY_ID = 1L;
  private static final Long TRANSACTION_ID = 1L;
  private static final String TREATY_STATUS = "STATUS";
  private static final String ACTION = "ACTION";
  private static final String COMMENTS = "COMMENTS";
  private static final String CREATION_USER = "USER";
  private static final String NAME = "NAME";

  private AuditTransactions auditTransactions;

  String expectedResult = "AuditTransactions{" +
    "id=" + ID +
    ", treatyId='" + TREATY_ID + "'" +
    ", transactionId='" + TRANSACTION_ID + "'" +
    ", name='" + NAME + "'" +
    ", action='" + ACTION + "'" +
    ", comments='" + COMMENTS + "'" +
    ", creationUser='" + CREATION_USER + "'" +
    '}';

  @Before
  public void setup() {

    auditTransactions = new AuditTransactions();
    auditTransactions.setId(ID);
    auditTransactions.setTreatyId(TREATY_ID);
    auditTransactions.setTransactionId(TRANSACTION_ID);
    auditTransactions.setName(NAME);
    auditTransactions.setAction(ACTION);
    auditTransactions.setComments(COMMENTS);
    auditTransactions.setCreationUser(CREATION_USER);
  }

  @Test
  public void equalsGetTest() {
    Long id = auditTransactions.getId();
    Long treatyId = auditTransactions.getTreatyId();
    Long transactionId = auditTransactions.getTransactionId();
    String name = auditTransactions.getName();
    String action = auditTransactions.getAction();
    String comments = auditTransactions.getComments();
    String createUser = auditTransactions.getCreationUser();

    assertThat(id).isEqualTo(ID);
    assertThat(treatyId).isEqualTo(TREATY_ID);
    assertThat(transactionId).isEqualTo(TRANSACTION_ID);
    assertThat(action).isEqualTo(ACTION);
    assertThat(comments).isEqualTo(COMMENTS);
    assertThat(createUser).isEqualTo(CREATION_USER);
    assertThat(name).isEqualTo(NAME);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = auditTransactions.equals(auditTransactions);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = auditTransactions.equals(new AuditTransactions());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    AuditTransactions auditTransactions1 = new AuditTransactions();
    auditTransactions1.setId(1L);
    boolean same = auditTransactions.equals(auditTransactions1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    AuditTransactions auditTransactions1 = new AuditTransactions();
    auditTransactions1.setId(null);
    boolean same = auditTransactions.equals(auditTransactions1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    AuditTransactions auditTransactions1 = new AuditTransactions();
    auditTransactions1.setId(ID);
    auditTransactions.setId(null);
    boolean same = auditTransactions.equals(auditTransactions1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = auditTransactions.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = auditTransactions.equals(null);
    assertThat(same).isFalse();
  }


  @Test
  public void toStringTest() {

    String actual = auditTransactions.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = auditTransactions.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
