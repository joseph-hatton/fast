package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class TreatySubtransactionCashUnitTest {

  private static final Long ID = 1L;
  private static final BigDecimal AMOUNT = new BigDecimal(1.1);
  private static final BigDecimal CONVERTED_AMOUNT = new BigDecimal(2.2);
  private static final LocalDate REPORTING_DATE = LocalDate.now();
  private static final Integer IS_ACTIVE = 1;
  private static final String REASON = "REASON";
  private static final Treaty TREATY = new Treaty();
  private static final AllocationType ALLOCATION_TYPE = new AllocationType();
  private static final LocalDate SETTLEMENT_DATE = LocalDate.now();
  private static final MdmAttributes MDM_ATTRIBUTES = new MdmAttributes();
  private static final TreatyTracking TREATY_TRACKING = new TreatyTracking();


  private TreatySubtransactionCash treatySubtransactionCash;

  String expectedResult = "TreatySubtransactionCash{" +
    "id=" + ID +
    ", amount='" + AMOUNT + "'" +
    ", convertedAmount='" + CONVERTED_AMOUNT + "'" +
    ", reportingDate='" + REPORTING_DATE + "'" +
    '}';

  @Before
  public void setup() {
    treatySubtransactionCash = new TreatySubtransactionCash();
    treatySubtransactionCash.setId(ID);
    treatySubtransactionCash.setIsActive(IS_ACTIVE);
    treatySubtransactionCash.setAmount(AMOUNT);
    treatySubtransactionCash.setConvertedAmount(CONVERTED_AMOUNT);
    treatySubtransactionCash.setReportingDate(REPORTING_DATE);
    treatySubtransactionCash.setSettlementDate(SETTLEMENT_DATE);
    treatySubtransactionCash.setTreaty(TREATY);
    treatySubtransactionCash.setAllocationType(ALLOCATION_TYPE);
    treatySubtransactionCash.setMdm(MDM_ATTRIBUTES);
    treatySubtransactionCash.setTracking(TREATY_TRACKING);
  }

  @Test
  public void equalsGetTest() {
    Long id = treatySubtransactionCash.getId();
    Integer isActive = treatySubtransactionCash.getIsActive();
    BigDecimal amount = treatySubtransactionCash.getAmount();
    BigDecimal convertedAmount = treatySubtransactionCash.getConvertedAmount();
    LocalDate reportingDate = treatySubtransactionCash.getReportingDate();
    LocalDate settlementDate = treatySubtransactionCash.getSettlementDate();
    Treaty treaty = treatySubtransactionCash.getTreaty();
    AllocationType allocationType = treatySubtransactionCash.getAllocationType();
    MdmAttributes mdm = treatySubtransactionCash.getMdm();
    TreatyTracking tracking = treatySubtransactionCash.getTracking();
    assertThat(id).isEqualTo(ID);
    assertThat(isActive).isEqualTo(IS_ACTIVE);
    assertThat(amount).isEqualTo(AMOUNT);
    assertThat(convertedAmount).isEqualTo(CONVERTED_AMOUNT);
    assertThat(reportingDate).isEqualTo(REPORTING_DATE);
    assertThat(settlementDate).isEqualTo(SETTLEMENT_DATE);
    assertThat(treaty).isEqualTo(TREATY);
    assertThat(allocationType).isEqualTo(ALLOCATION_TYPE);
    assertThat(mdm).isEqualTo(MDM_ATTRIBUTES);
    assertThat(tracking).isEqualTo(TREATY_TRACKING);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = treatySubtransactionCash.equals(treatySubtransactionCash);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = treatySubtransactionCash.equals(new TreatySubtransactionCash());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    TreatySubtransactionCash treatySubtransactionCash1 = new TreatySubtransactionCash();
    treatySubtransactionCash1.setId(1L);
    boolean same = treatySubtransactionCash.equals(treatySubtransactionCash1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    TreatySubtransactionCash treatySubtransactionCash1 = new TreatySubtransactionCash();
    treatySubtransactionCash1.setId(null);
    boolean same = treatySubtransactionCash.equals(treatySubtransactionCash1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    TreatySubtransactionCash treatySubtransactionCash1 = new TreatySubtransactionCash();
    treatySubtransactionCash1.setId(ID);
    treatySubtransactionCash.setId(null);
    boolean same = treatySubtransactionCash.equals(treatySubtransactionCash1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = treatySubtransactionCash.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = treatySubtransactionCash.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = treatySubtransactionCash.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = treatySubtransactionCash.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
