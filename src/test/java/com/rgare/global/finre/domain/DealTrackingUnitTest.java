package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class DealTrackingUnitTest {

  private static final Long ID = 1L;
  private static final LocalDate FINANCE_DATE = LocalDate.now();
  private static final LocalDate REPORTING_ARM_DATE = LocalDate.now();
  private static final LocalDate REPORTING_VALIDATION_DATE = LocalDate.now();
  private static final LocalDate REPORTING_PERIOD_END_DATE = LocalDate.now();
  private static final String PASSWORD = "PASSWORD";
  private static final Integer AUTO_GENERATE_FLAG = 1;
  private static final Integer IS_ACTIVE = 1;
  private static final String DEAL_TRACKING_STATUS = "STATUS";
  private static final Integer FINANCE_DATE_FLAG = 1;
  private static final Integer REPORTING_ARM_FLAG = 1;
  private static final String REPORTING_FLAG = "FLAG";
  private static final String REPORTING_REVISED_FLAG = "FLAG";

  private static final Deal DEAL = new Deal();

  private DealTracking dealTracking;

  String expectedResult = "DealTracking{" +
    "id=" + ID +
    '}';

  @Before
  public void setup() {

    dealTracking = new DealTracking();
    dealTracking.setId(ID);
    dealTracking.setFinanceDate(FINANCE_DATE);
    dealTracking.setReportingArmDate(REPORTING_ARM_DATE);
    dealTracking.setReportingValidationDate(REPORTING_VALIDATION_DATE);
    dealTracking.setReportingPeriodEndDate(REPORTING_PERIOD_END_DATE);
    dealTracking.setPassword(PASSWORD);
    dealTracking.setAutoGenerateFlag(AUTO_GENERATE_FLAG);
    dealTracking.setIsActive(IS_ACTIVE);
    dealTracking.setDealTrackingStatus(DEAL_TRACKING_STATUS);
    dealTracking.setFinanceDateFlag(FINANCE_DATE_FLAG);
    dealTracking.setReportingArmFlag(REPORTING_ARM_FLAG);
    dealTracking.setReportingFlag(REPORTING_FLAG);
    dealTracking.setReportingRevisedFlag(REPORTING_REVISED_FLAG);
    dealTracking.setDeal(DEAL);
  }

  @Test
  public void equalsGetTest() {
    Long id = dealTracking.getId();
    LocalDate financeDate = dealTracking.getFinanceDate();
    LocalDate reportingArmDate = dealTracking.getReportingArmDate();
    LocalDate reportingValidationDate = dealTracking.getReportingValidationDate();
    LocalDate reportingPeriodEndDate = dealTracking.getReportingPeriodEndDate();
    String password = dealTracking.getPassword();
    Integer autoGenerateFlag = dealTracking.getAutoGenerateFlag();
    Integer isActive = dealTracking.getIsActive();
    String dealTrackingStatus = dealTracking.getDealTrackingStatus();
    Integer financeDateFlag = dealTracking.getFinanceDateFlag();
    Integer reportingArmFlag = dealTracking.getReportingArmFlag();
    String reportingFlag = dealTracking.getReportingFlag();
    String reportingRevisedFlag = dealTracking.getReportingRevisedFlag();
    Deal deal = dealTracking.getDeal();

    assertThat(id).isEqualTo(ID);
    assertThat(financeDate).isEqualTo(FINANCE_DATE);
    assertThat(reportingArmDate).isEqualTo(REPORTING_ARM_DATE);
    assertThat(reportingValidationDate).isEqualTo(REPORTING_VALIDATION_DATE);
    assertThat(reportingPeriodEndDate).isEqualTo(REPORTING_PERIOD_END_DATE);
    assertThat(password).isEqualTo(PASSWORD);
    assertThat(autoGenerateFlag).isEqualTo(AUTO_GENERATE_FLAG);
    assertThat(isActive).isEqualTo(IS_ACTIVE);
    assertThat(dealTrackingStatus).isEqualTo(DEAL_TRACKING_STATUS);
    assertThat(financeDateFlag).isEqualTo(FINANCE_DATE_FLAG);
    assertThat(reportingArmDate).isEqualTo(REPORTING_ARM_DATE);
    assertThat(reportingArmFlag).isEqualTo(REPORTING_ARM_FLAG);
    assertThat(reportingFlag).isEqualTo(REPORTING_FLAG);
    assertThat(reportingRevisedFlag).isEqualTo(REPORTING_REVISED_FLAG);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = dealTracking.equals(dealTracking);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = dealTracking.equals(new DealTracking());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    DealTracking dealTracking1 = new DealTracking();
    dealTracking1.setId(1L);
    boolean same = dealTracking.equals(dealTracking1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    DealTracking dealTracking1 = new DealTracking();
    dealTracking1.setId(null);
    boolean same = dealTracking.equals(dealTracking1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    DealTracking dealTracking1 = new DealTracking();
    dealTracking1.setId(ID);
    dealTracking.setId(null);
    boolean same = dealTracking.equals(dealTracking1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = dealTracking.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = dealTracking.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = dealTracking.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = dealTracking.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
