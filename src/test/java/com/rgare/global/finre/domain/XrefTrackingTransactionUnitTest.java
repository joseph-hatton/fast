package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class XrefTrackingTransactionUnitTest {

  private static final Long ID = 1L;

  private static final Long TREATY_TRANSACTION_CASH_ID = 1L;
  private static final Long TREATY_CASH_SUBTRANSACTION_ID = 2L;
  private static final Long TREATY_TRACKING_ID = 3L;
  private static final Long CURRENCY_ID = 3L;

  private XrefTrackingTransaction xrefTrackingTransaction;

  String expectedResult = "XrefTrackingTransaction{" +
    "id=" + ID +
    ", treatyTransactionCashId='" + TREATY_TRANSACTION_CASH_ID + "'" +
    ", treatyCashSubtransactionId='" + TREATY_CASH_SUBTRANSACTION_ID + "'" +
    ", treatyTrackingId='" + TREATY_TRACKING_ID + "'" +
    ", currencyId='" + CURRENCY_ID + "'" +

    '}';

  @Before
  public void setup() {
    xrefTrackingTransaction = new XrefTrackingTransaction();
    xrefTrackingTransaction.setId(ID);
    xrefTrackingTransaction.setTreatyTransactionCashId(TREATY_TRANSACTION_CASH_ID);
    xrefTrackingTransaction.setTreatyCashSubtransactionId(TREATY_CASH_SUBTRANSACTION_ID);
    xrefTrackingTransaction.setTreatyTrackingId(TREATY_TRACKING_ID);
    xrefTrackingTransaction.setCurrencyId(CURRENCY_ID);
  }

  @Test
  public void equalsGetTest() {
    Long id = xrefTrackingTransaction.getId();
    Long treatyTransactionCashId = xrefTrackingTransaction.getTreatyTransactionCashId();
    Long treatyCashSubtransactionId = xrefTrackingTransaction.getTreatyCashSubtransactionId();
    Long treatyTrackingId = xrefTrackingTransaction.getTreatyTrackingId();
    Long currencyId = xrefTrackingTransaction.getCurrencyId();
    assertThat(treatyTransactionCashId).isEqualTo(TREATY_TRANSACTION_CASH_ID);
    assertThat(treatyCashSubtransactionId).isEqualTo(TREATY_CASH_SUBTRANSACTION_ID);
    assertThat(treatyTrackingId).isEqualTo(TREATY_TRACKING_ID);
    assertThat(currencyId).isEqualTo(CURRENCY_ID);
    assertThat(id).isEqualTo(ID);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = xrefTrackingTransaction.equals(xrefTrackingTransaction);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = xrefTrackingTransaction.equals(new XrefTrackingTransaction());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    XrefTrackingTransaction xrefTrackingTransaction1 = new XrefTrackingTransaction();
    xrefTrackingTransaction1.setId(1L);
    boolean same = xrefTrackingTransaction.equals(xrefTrackingTransaction1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    XrefTrackingTransaction xrefTrackingTransaction1 = new XrefTrackingTransaction();
    xrefTrackingTransaction1.setId(null);
    boolean same = xrefTrackingTransaction.equals(xrefTrackingTransaction1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    XrefTrackingTransaction xrefTrackingTransaction1 = new XrefTrackingTransaction();
    xrefTrackingTransaction1.setId(ID);
    xrefTrackingTransaction.setId(null);
    boolean same = xrefTrackingTransaction.equals(xrefTrackingTransaction1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = xrefTrackingTransaction.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = xrefTrackingTransaction.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = xrefTrackingTransaction.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = xrefTrackingTransaction.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
