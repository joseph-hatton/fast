package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class EucUseUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";
  private static final String ENTITY_TYPE = "TYPE";

  private EucUse eucUse;

  String expectedResult = "EucUse{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    '}';

  @Before
  public void setup() {

    eucUse = new EucUse();
    eucUse.setId(ID);
    eucUse.setName(NAME);
    eucUse.setDescription(DESCRIPTION);
    eucUse.setEntityType(ENTITY_TYPE);
  }

  @Test
  public void equalsGetTest() {
    Long id = eucUse.getId();
    String name = eucUse.getName();
    String description = eucUse.getDescription();
    String entityType = eucUse.getEntityType();

    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
    assertThat(entityType).isEqualTo(ENTITY_TYPE);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = eucUse.equals(eucUse);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = eucUse.equals(new EucUse());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    EucUse eucUse1 = new EucUse();
    eucUse1.setId(1L);
    boolean same = eucUse.equals(eucUse1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    EucUse eucUse1 = new EucUse();
    eucUse1.setId(null);
    boolean same = eucUse.equals(eucUse1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    EucUse eucUse1 = new EucUse();
    eucUse1.setId(ID);
    eucUse.setId(null);
    boolean same = eucUse.equals(eucUse1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = eucUse.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = eucUse.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = eucUse.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = eucUse.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
