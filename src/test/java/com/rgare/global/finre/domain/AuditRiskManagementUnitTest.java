package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuditRiskManagementUnitTest {

  private static final Long ID = 1L;
  private static final Long DEAL_ID = 1L;
  private static final Long RISK_FEE_ID = 1L;
  private static final Long RISK_MANAGEMENT_ID = 1L;
  private static final String TREATY_STATUS = "STATUS";
  private static final String ACTION = "ACTION";
  private static final String COMMENTS = "COMMENTS";
  private static final String CREATION_USER = "USER";
  private static final String MGMT_INPUT_STATUS = "STATUS";

  LocalDateTime ldt = LocalDateTime.of(2017, Month.JULY, 19, 16, 30);
  private  ZonedDateTime ZONED_DATE_TIME = ldt.atZone(ZoneId.of("Europe/Paris"));
  private AuditRiskManagement auditRiskManagement;

  String expectedResult = "AuditRiskManagement{" +
    "id=" + ID +
      '}';

  @Before
  public void setup() {

    auditRiskManagement = new AuditRiskManagement();
    auditRiskManagement.setId(ID);
    auditRiskManagement.setComments(COMMENTS);
    auditRiskManagement.setAction(ACTION);
    auditRiskManagement.setDealId(DEAL_ID);
    auditRiskManagement.setRiskManagementId(RISK_MANAGEMENT_ID);
    auditRiskManagement.setCreationUser(CREATION_USER);
    auditRiskManagement.setCreationTs(ZONED_DATE_TIME);
  }

  @Test
  public void equalsGetTest() {
    Long id = auditRiskManagement.getId();
    Long dealId = auditRiskManagement.getDealId();
    Long riskManagementId = auditRiskManagement.getRiskManagementId();
    String action = auditRiskManagement.getAction();
    String comments = auditRiskManagement.getComments();
    String createUser = auditRiskManagement.getCreationUser();
    ZonedDateTime zonedDateTime = auditRiskManagement.getCreationTs();

    assertThat(id).isEqualTo(ID);
    assertThat(dealId).isEqualTo(DEAL_ID);
    assertThat(riskManagementId).isEqualTo(RISK_MANAGEMENT_ID);
    assertThat(action).isEqualTo(ACTION);
    assertThat(comments).isEqualTo(COMMENTS);
    assertThat(createUser).isEqualTo(CREATION_USER);
    assertThat(zonedDateTime).isEqualTo(ZONED_DATE_TIME);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = auditRiskManagement.equals(auditRiskManagement);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = auditRiskManagement.equals(new AuditRiskManagement());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    AuditRiskManagement auditRiskManagement1 = new AuditRiskManagement();
    auditRiskManagement1.setId(1L);
    boolean same = auditRiskManagement.equals(auditRiskManagement1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    AuditRiskManagement auditRiskManagement1 = new AuditRiskManagement();
    auditRiskManagement1.setId(null);
    boolean same = auditRiskManagement.equals(auditRiskManagement1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    AuditRiskManagement auditRiskManagement1 = new AuditRiskManagement();
    auditRiskManagement1.setId(ID);
    auditRiskManagement.setId(null);
    boolean same = auditRiskManagement.equals(auditRiskManagement1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = auditRiskManagement.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = auditRiskManagement.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = auditRiskManagement.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = auditRiskManagement.hashCode();
    assertThat(actual).isEqualTo(ID.intValue());
  }


}
