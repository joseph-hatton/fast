package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class PersistentAuditEventUnitTest {

  private static final Long ID = 1L;

  private static final String PRINCIPAL = "PRINCIPAL";
  private static final LocalDateTime AUDIT_EVENT_DATE = LocalDateTime.MAX;
  private static final String AUDIT_EVENT_TYPE = "TYPE";
  private Map<String, String> DATA = new HashMap<>();

  private PersistentAuditEvent persistentAuditEvent;

  @Before
  public void setup() {

    persistentAuditEvent = new PersistentAuditEvent();
    persistentAuditEvent.setId(ID);
    persistentAuditEvent.setPrincipal(PRINCIPAL);
    persistentAuditEvent.setAuditEventDate(AUDIT_EVENT_DATE);
    persistentAuditEvent.setAuditEventType(AUDIT_EVENT_TYPE);
    DATA.put("TEST","TEST");
    persistentAuditEvent.setData(DATA);
  }

  @Test
  public void equalsGetTest() {
    Long id = persistentAuditEvent.getId();
    String principal = persistentAuditEvent.getPrincipal();
    String auditEventType = persistentAuditEvent.getAuditEventType();
    LocalDateTime auditEventDate = persistentAuditEvent.getAuditEventDate();
    assertThat(id).isEqualTo(ID);
    assertThat(principal).isEqualTo(PRINCIPAL);
    assertThat(auditEventType).isEqualTo(AUDIT_EVENT_TYPE);
    assertThat(auditEventDate).isEqualTo(AUDIT_EVENT_DATE);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = persistentAuditEvent.equals(persistentAuditEvent);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = persistentAuditEvent.equals(new PersistentAuditEvent());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = persistentAuditEvent.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringInputValuesTest() {

    persistentAuditEvent.toString();
    assertThat(ID).isEqualTo(persistentAuditEvent.getId());
    assertThat(AUDIT_EVENT_DATE).isEqualTo(persistentAuditEvent.getAuditEventDate());
    assertThat(AUDIT_EVENT_TYPE).isEqualTo(persistentAuditEvent.getAuditEventType());
    assertThat(PRINCIPAL).isEqualTo(persistentAuditEvent.getPrincipal());
    assertThat(DATA).isEqualTo(persistentAuditEvent.getData());
  }


}
