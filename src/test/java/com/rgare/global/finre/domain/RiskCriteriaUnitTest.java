package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class RiskCriteriaUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  private RiskCriteria riskCriteria;

  String expectedResult = "RiskCriteria{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    '}';

  @Before
  public void setup() {

    riskCriteria = new RiskCriteria();
    riskCriteria.setId(ID);
    riskCriteria.setName(NAME);
    riskCriteria.setDescription(DESCRIPTION);
  }

  @Test
  public void equalsGetTest() {
    Long id = riskCriteria.getId();
    String name = riskCriteria.getName();
    String description = riskCriteria.getDescription();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = riskCriteria.equals(riskCriteria);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = riskCriteria.equals(new RiskCriteria());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    RiskCriteria riskCriteria1 = new RiskCriteria();
    riskCriteria1.setId(1L);
    boolean same = riskCriteria.equals(riskCriteria1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    RiskCriteria riskCriteria1 = new RiskCriteria();
    riskCriteria1.setId(null);
    boolean same = riskCriteria.equals(riskCriteria1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    RiskCriteria riskCriteria1 = new RiskCriteria();
    riskCriteria1.setId(ID);
    riskCriteria.setId(null);
    boolean same = riskCriteria.equals(riskCriteria1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = riskCriteria.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = riskCriteria.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = riskCriteria.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = riskCriteria.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
