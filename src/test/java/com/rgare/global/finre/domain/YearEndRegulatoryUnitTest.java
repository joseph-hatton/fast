package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class YearEndRegulatoryUnitTest {

  private static final Long ID = 1L;

  private static final String REGULATORY_DUE_DATE_FLAG = "FLAG";
  private static final String YEAR_END_REGULATORY_TYPE = "TYPE";

  private static final Treaty TREATY = new Treaty();
  private static final EntityDate REGULATORY_DUE_DATE = new EntityDate();
  private static final Frequency REGULATORY_FREQUENCY = new Frequency();

  private YearEndRegulatory yearEndRegulatory;

  String expectedResult = "YearEndRegulatory{" +
    "id=" + ID +
    '}';

  @Before
  public void setup() {
    yearEndRegulatory = new YearEndRegulatory();
    yearEndRegulatory.setId(ID);
    yearEndRegulatory.setRegulatoryDueDateFlag(REGULATORY_DUE_DATE_FLAG);
    yearEndRegulatory.setYearEndRegulatoryType(YEAR_END_REGULATORY_TYPE);
    yearEndRegulatory.setRegulatoryDueDate(REGULATORY_DUE_DATE);
    yearEndRegulatory.setRegulatoryFrequency(REGULATORY_FREQUENCY);
  }

  @Test
  public void equalsGetTest() {
    Long id = yearEndRegulatory.getId();
    String regulatoryDueDateFlag = yearEndRegulatory.getRegulatoryDueDateFlag();
    String regulatoryType = yearEndRegulatory.getYearEndRegulatoryType();
    EntityDate regulatoryDueDate = yearEndRegulatory.getRegulatoryDueDate();
    Frequency regulatoryFrequency = yearEndRegulatory.getRegulatoryFrequency();
    assertThat(id).isEqualTo(ID);
    assertThat(regulatoryDueDateFlag).isEqualTo(REGULATORY_DUE_DATE_FLAG);
    assertThat(regulatoryType).isEqualTo(YEAR_END_REGULATORY_TYPE);
    assertThat(regulatoryDueDate).isEqualTo(REGULATORY_DUE_DATE);
    assertThat(regulatoryFrequency).isEqualTo(REGULATORY_FREQUENCY);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = yearEndRegulatory.equals(yearEndRegulatory);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = yearEndRegulatory.equals(new YearEndRegulatory());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    YearEndRegulatory yearEndRegulatory1 = new YearEndRegulatory();
    yearEndRegulatory1.setId(1L);
    boolean same = yearEndRegulatory.equals(yearEndRegulatory1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    YearEndRegulatory yearEndRegulatory1 = new YearEndRegulatory();
    yearEndRegulatory1.setId(null);
    boolean same = yearEndRegulatory.equals(yearEndRegulatory1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    YearEndRegulatory yearEndRegulatory1 = new YearEndRegulatory();
    yearEndRegulatory1.setId(ID);
    yearEndRegulatory.setId(null);
    boolean same = yearEndRegulatory.equals(yearEndRegulatory1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = yearEndRegulatory.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = yearEndRegulatory.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = yearEndRegulatory.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = yearEndRegulatory.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
