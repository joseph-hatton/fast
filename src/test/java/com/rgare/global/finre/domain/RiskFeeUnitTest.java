package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class RiskFeeUnitTest {

  private static final Long ID = 1L;
  private static final Long TREATY_ID = 1L;

  private static final BigDecimal RATE = new BigDecimal(1);
  private static final String TREATY_REFERENCE = "REFERENCE";
  private static final String PERIOD = "PERIOD";
  private static final String COMMENTS = "COMMENTS";
  private static final Integer MINIMUM_RISK_FEE = 1;
  private static final BigDecimal MINIMUM_RISK_FEE_AMOUNT = new BigDecimal(1);

  private static final Currency CURRENCY = new Currency();

  private RiskFee riskFee;

  String expectedResult = "RiskFee{" +
    "id=" + ID +
    ", rate='" + RATE + "'" +
    ", treatyId='" + TREATY_ID + "'" +
    ", period='" + PERIOD + "'" +
    '}';

  @Before
  public void setup() {

    riskFee = new RiskFee();
    riskFee.setId(ID);
    riskFee.setTreatyId(TREATY_ID);
    riskFee.setTreatyReference(TREATY_REFERENCE);
    riskFee.setRate(RATE);
    riskFee.setPeriod(PERIOD);
    riskFee.setComments(COMMENTS);
    riskFee.setMinimumRiskFee(MINIMUM_RISK_FEE);
    riskFee.setMinimumRiskFeeAmount(MINIMUM_RISK_FEE_AMOUNT);
    riskFee.setRiskFeeCurrency(CURRENCY);
  }

  @Test
  public void equalsGetTest() {
    Long id = riskFee.getId();
    Long treatyId = riskFee.getTreatyId();
    String treatyReference = riskFee.getTreatyReference();
    BigDecimal rate = riskFee.getRate();
    String period = riskFee.getPeriod();
    String comments = riskFee.getComments();
    Integer minimumRiskFee = riskFee.getMinimumRiskFee();
    BigDecimal minimumRiskFeeAmount = riskFee.getMinimumRiskFeeAmount();
    Currency currency = riskFee.getRiskFeeCurrency();

    assertThat(id).isEqualTo(ID);
    assertThat(treatyId).isEqualTo(TREATY_ID);
    assertThat(treatyReference).isEqualTo(TREATY_REFERENCE);
    assertThat(rate).isEqualTo(RATE);
    assertThat(period).isEqualTo(PERIOD);
    assertThat(comments).isEqualTo(COMMENTS);
    assertThat(minimumRiskFee).isEqualTo(MINIMUM_RISK_FEE);
    assertThat(minimumRiskFeeAmount).isEqualTo(MINIMUM_RISK_FEE_AMOUNT);
    assertThat(currency).isEqualTo(CURRENCY);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = riskFee.equals(riskFee);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = riskFee.equals(new RiskFee());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    RiskFee riskFee1 = new RiskFee();
    riskFee1.setId(1L);
    boolean same = riskFee.equals(riskFee1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    RiskFee riskFee1 = new RiskFee();
    riskFee1.setId(null);
    boolean same = riskFee.equals(riskFee1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    RiskFee riskFee1 = new RiskFee();
    riskFee1.setId(ID);
    riskFee.setId(null);
    boolean same = riskFee.equals(riskFee1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = riskFee.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = riskFee.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = riskFee.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = riskFee.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
