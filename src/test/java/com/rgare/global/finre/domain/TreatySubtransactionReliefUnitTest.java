package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class TreatySubtransactionReliefUnitTest {

  private static final Long ID = 1L;
  private static final BigDecimal AMOUNT = new BigDecimal(1.1);
  private static final BigDecimal CONVERTED_AMOUNT = new BigDecimal(2.2);
  private static final Integer ASSUMING = 1;
  private static final Integer ULTIMATE = 2;
  private static final Integer INTERIM = 3;
  private static final Integer INTERNAL = 4;

  private static final Company COMPANY = new Company();
  private static final TreatyTransactionRelief TREATY_TRANSACTION_RELIEF = new TreatyTransactionRelief();

  private TreatySubtransactionRelief treatySubtransactionRelief;

  String expectedResult = "TreatySubtransactionRelief{" +
    "id=" + ID +
    ", amount='" + AMOUNT + "'" +
    ", convertedAmount='" + CONVERTED_AMOUNT + "'" +
    '}';

  @Before
  public void setup() {
    treatySubtransactionRelief = new TreatySubtransactionRelief();
    treatySubtransactionRelief.setId(ID);
    treatySubtransactionRelief.setAmount(AMOUNT);
    treatySubtransactionRelief.setConvertedAmount(CONVERTED_AMOUNT);
    treatySubtransactionRelief.setAssuming(ASSUMING);
    treatySubtransactionRelief.setUltimate(ULTIMATE);
    treatySubtransactionRelief.setInterim(INTERIM);
    treatySubtransactionRelief.setInternal(INTERNAL);
    treatySubtransactionRelief.setCompany(COMPANY);
    treatySubtransactionRelief.setTreatyTransactionRelief(TREATY_TRANSACTION_RELIEF);
  }

  @Test
  public void equalsGetTest() {
    Long id = treatySubtransactionRelief.getId();
    BigDecimal amount = treatySubtransactionRelief.getAmount();
    BigDecimal convertedAmount = treatySubtransactionRelief.getConvertedAmount();
    Integer assuming = treatySubtransactionRelief.getAssuming();
    Integer ultimate = treatySubtransactionRelief.getUltimate();
    Integer interim = treatySubtransactionRelief.getInterim();
    Integer internal = treatySubtransactionRelief.getInternal();
    Company company = treatySubtransactionRelief.getCompany();
    TreatyTransactionRelief treatyTransactionRelief = treatySubtransactionRelief.getTreatyTransactionRelief();
    assertThat(id).isEqualTo(ID);
    assertThat(amount).isEqualTo(AMOUNT);
    assertThat(convertedAmount).isEqualTo(CONVERTED_AMOUNT);
    assertThat(assuming).isEqualTo(ASSUMING);
    assertThat(ultimate).isEqualTo(ULTIMATE);
    assertThat(interim).isEqualTo(INTERIM);
    assertThat(internal).isEqualTo(INTERNAL);
    assertThat(company).isEqualTo(COMPANY);
    assertThat(treatyTransactionRelief).isEqualTo(TREATY_TRANSACTION_RELIEF);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = treatySubtransactionRelief.equals(treatySubtransactionRelief);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = treatySubtransactionRelief.equals(new TreatySubtransactionRelief());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    TreatySubtransactionRelief treatySubtransactionRelief1 = new TreatySubtransactionRelief();
    treatySubtransactionRelief1.setId(1L);
    boolean same = treatySubtransactionRelief.equals(treatySubtransactionRelief1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    TreatySubtransactionRelief treatySubtransactionRelief1 = new TreatySubtransactionRelief();
    treatySubtransactionRelief1.setId(null);
    boolean same = treatySubtransactionRelief.equals(treatySubtransactionRelief1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    TreatySubtransactionRelief treatySubtransactionRelief1 = new TreatySubtransactionRelief();
    treatySubtransactionRelief1.setId(ID);
    treatySubtransactionRelief.setId(null);
    boolean same = treatySubtransactionRelief.equals(treatySubtransactionRelief1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = treatySubtransactionRelief.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = treatySubtransactionRelief.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = treatySubtransactionRelief.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = treatySubtransactionRelief.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
