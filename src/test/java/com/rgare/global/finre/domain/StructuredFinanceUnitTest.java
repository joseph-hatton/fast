package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class StructuredFinanceUnitTest {

  private static final Long ID = 1L;

  private static final Integer IS_ACTIVE = 1;
  private static final String REPORTING_CATEGORY = "CATEGORY";
  private static final String REPORTING_REQ = "REQ";
  private static final String PREPARER = "PREPARER";
  private static final String RPT_REF_DOC = "RPT_REF_DOC";
  private static final String NOTE_INCREASE_DOC = "NOTE";
  private static final LocalDate RPT_REQ_EFF_DATE = LocalDate.now();
  private static final String COMMENTS = "COMMENTS";
  private static final String  CONTENT_SPECIFICATIONS = "SPECIFICATIONS";
  private static final String FILE_NAME = "FILE";
  private static final Integer NUMBER_OF_DAYS  = 1;
  private static final String DAY_TYPE = "DAY_TYPE";
  private static final String PERIOD = "PERIOD";
  private static final String  REPORTING_DUE_COMMENT = "REPORTING_DUE_COMMENT";
  private static final String YEAR_END_EXCEPTION = "YEAR_END_EXCEPTION";
  private static final Integer NUMBER_OF_DAYS_YR  = 1;
  private static final String DAY_TYPE_YR = "DAY_TYPE_YR";
  private static final String PERIOD_YR = "PERIOD_YR";
  private static final String REPORTING_DUE_COMMENT_YR = "REPORTING_DUE_COMMENT_YR";
  private static final Deal DEAL = new Deal();
  private static final Frequency FREQUENCY = new Frequency();

  private StructuredFinance structuredFinance;

  String expectedResult = "StructuredFinance{" +
    "id=" + ID +
    '}';

  @Before
  public void setup() {

    structuredFinance = new StructuredFinance();
    structuredFinance.setId(ID);
    structuredFinance.setIsActive(IS_ACTIVE);
    structuredFinance.setReportingCategory(REPORTING_CATEGORY);
    structuredFinance.setReportingReq(REPORTING_REQ);
    structuredFinance.setPreparer(PREPARER);
    structuredFinance.setRptRefDoc(RPT_REF_DOC);
    structuredFinance.setNoteIncreaseDoc(NOTE_INCREASE_DOC);
    structuredFinance.setRptReqEffDate(RPT_REQ_EFF_DATE);
    structuredFinance.setComments(COMMENTS);
    structuredFinance.setContentSpecifications(CONTENT_SPECIFICATIONS);
    structuredFinance.setFileName(FILE_NAME);
    structuredFinance.setNumberOfDays(NUMBER_OF_DAYS);
    structuredFinance.setDayType(DAY_TYPE);
    structuredFinance.setPeriod(PERIOD);
    structuredFinance.setReportingDueComment(REPORTING_DUE_COMMENT);
    structuredFinance.setYearEndException(YEAR_END_EXCEPTION);
    structuredFinance.setNumberOfDaysYr(NUMBER_OF_DAYS_YR);
    structuredFinance.setDayTypeYr(DAY_TYPE_YR);
    structuredFinance.setPeriodYr(PERIOD_YR);
    structuredFinance.setReportingDueCommentYr(REPORTING_DUE_COMMENT_YR);
    structuredFinance.setDeal(DEAL);
    structuredFinance.setReportingFrequency(FREQUENCY);
  }

  @Test
  public void equalsGetTest() {
    Long id = structuredFinance.getId();
    Integer isActive = structuredFinance.getIsActive();
    String reportingCategory = structuredFinance.getReportingCategory();
    String reportingReq = structuredFinance.getReportingReq();
    String preparer = structuredFinance.getPreparer();
    String rptRefDoc = structuredFinance.getRptRefDoc();
    String noteIncreaseDoc = structuredFinance.getNoteIncreaseDoc();
    LocalDate rptReqEffDate = structuredFinance.getRptReqEffDate();
    String comments = structuredFinance.getComments();
    String contentSpecifications = structuredFinance.getContentSpecifications();
    String fileName = structuredFinance.getFileName();
    Integer numberOfDays = structuredFinance.getNumberOfDays();
    String dayType = structuredFinance.getDayType();
    String period = structuredFinance.getPeriod();
    String reportingDueComment = structuredFinance.getReportingDueComment();
    String yearEndException = structuredFinance.getYearEndException();
    Integer numberOfDaysYr = structuredFinance.getNumberOfDaysYr();
    String dayTypeYr = structuredFinance.getDayTypeYr();
    String periodYr = structuredFinance.getPeriodYr();
    String reportingDueCommentYr = structuredFinance.getReportingDueCommentYr();
    Deal deal = structuredFinance.getDeal();
    Frequency reportingFrequency = structuredFinance.getReportingFrequency();

    assertThat(id).isEqualTo(ID);
    assertThat(isActive).isEqualTo(IS_ACTIVE);
    assertThat(reportingCategory).isEqualTo(REPORTING_CATEGORY);
    assertThat(reportingReq).isEqualTo(REPORTING_REQ);
    assertThat(preparer).isEqualTo(PREPARER);
    assertThat(rptRefDoc).isEqualTo(RPT_REF_DOC);
    assertThat(noteIncreaseDoc).isEqualTo(NOTE_INCREASE_DOC);
    assertThat(rptReqEffDate).isEqualTo(RPT_REQ_EFF_DATE);
    assertThat(comments).isEqualTo(COMMENTS);
    assertThat(contentSpecifications).isEqualTo(CONTENT_SPECIFICATIONS);
    assertThat(fileName).isEqualTo(FILE_NAME);
    assertThat(numberOfDays).isEqualTo(NUMBER_OF_DAYS);
    assertThat(dayType).isEqualTo(DAY_TYPE);
    assertThat(period).isEqualTo(PERIOD);
    assertThat(reportingDueComment).isEqualTo(REPORTING_DUE_COMMENT);
    assertThat(yearEndException).isEqualTo(YEAR_END_EXCEPTION);
    assertThat(numberOfDaysYr).isEqualTo(NUMBER_OF_DAYS_YR);
    assertThat(dayTypeYr).isEqualTo(DAY_TYPE_YR);
    assertThat(periodYr).isEqualTo(PERIOD_YR);
    assertThat(reportingDueCommentYr).isEqualTo(REPORTING_DUE_COMMENT_YR);
    assertThat(deal).isEqualTo(DEAL);
    assertThat(reportingFrequency).isEqualTo(FREQUENCY);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = structuredFinance.equals(structuredFinance);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = structuredFinance.equals(new StructuredFinance());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    StructuredFinance structuredFinance1 = new StructuredFinance();
    structuredFinance1.setId(1L);
    boolean same = structuredFinance.equals(structuredFinance1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    StructuredFinance structuredFinance1 = new StructuredFinance();
    structuredFinance1.setId(null);
    boolean same = structuredFinance.equals(structuredFinance1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    StructuredFinance structuredFinance1 = new StructuredFinance();
    structuredFinance1.setId(ID);
    structuredFinance.setId(null);
    boolean same = structuredFinance.equals(structuredFinance1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = structuredFinance.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = structuredFinance.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = structuredFinance.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = structuredFinance.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
