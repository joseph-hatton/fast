package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class NotificationStatusUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";
  private static final String DEFAULT_FLAG = "FLAG";
  private static final String LAST_USER = "USER";
  private static final String CREATION_USER = "CREATION_USER";
  private static final LocalDate LAST_UPDATE = LocalDate.now();
  private static final LocalDate CREATTION_TS = LocalDate.now();
  private static final LocalDate EFF_BEGIN_TS = LocalDate.now();
  private static final LocalDate EFF_END_TS = LocalDate.now();

  private static final Integer SORT_ORDER = 1;

  private NotificationStatus notificationStatus;

  String expectedResult = "NotificationStatus{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    ", sortOrder='" + SORT_ORDER + "'" +
    ", default_flag='" + DEFAULT_FLAG + "'" +
    ", lastUser='" + LAST_USER + "'" +
    ", lastUpdate='" + LAST_UPDATE + "'" +
    ", creationTS='" + CREATTION_TS + "'" +
    ", creationUser='" + CREATION_USER + "'" +
    ", effBeginTs='" + EFF_BEGIN_TS + "'" +
    ", effEndTs='" + EFF_END_TS + "'" +
    '}';

  @Before
  public void setup() {

    notificationStatus = new NotificationStatus();
    notificationStatus.setId(ID);
    notificationStatus.setName(NAME);
    notificationStatus.setDescription(DESCRIPTION);
    notificationStatus.setSortOrder(SORT_ORDER);
    notificationStatus.setDefault_flag(DEFAULT_FLAG);
    notificationStatus.setLastUser(LAST_USER);
    notificationStatus.setLastUpdate(LAST_UPDATE);
    notificationStatus.setCreationTS(CREATTION_TS);
    notificationStatus.setCreationUser(CREATION_USER);
    notificationStatus.setEffBeginTs(EFF_BEGIN_TS);
    notificationStatus.setEffEndTs(EFF_END_TS);
  }

  @Test
  public void equalsGetTest() {
    Long id = notificationStatus.getId();
    String name = notificationStatus.getName();
    String description = notificationStatus.getDescription();
    Integer sortOrder = notificationStatus.getSortOrder();
    String defaultFlag = notificationStatus.getDefault_flag();
    String lastUser = notificationStatus.getLastUser();
    LocalDate lastUpdate = notificationStatus.getLastUpdate();
    LocalDate creationTS = notificationStatus.getCreationTS();
    String creationUser = notificationStatus.getCreationUser();
    LocalDate effBeginTs = notificationStatus.getEffBeginTs();
    LocalDate effEndTs = notificationStatus.getEffEndTs();

    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
    assertThat(sortOrder).isEqualTo(SORT_ORDER);
    assertThat(defaultFlag).isEqualTo(DEFAULT_FLAG);
    assertThat(lastUser).isEqualTo(LAST_USER);
    assertThat(lastUpdate).isEqualTo(LAST_UPDATE);
    assertThat(creationTS).isEqualTo(CREATTION_TS);
    assertThat(creationUser).isEqualTo(CREATION_USER);
    assertThat(effBeginTs).isEqualTo(EFF_BEGIN_TS);
    assertThat(effEndTs).isEqualTo(EFF_END_TS);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = notificationStatus.equals(notificationStatus);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = notificationStatus.equals(new NotificationStatus());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    NotificationStatus notificationStatus1 = new NotificationStatus();
    notificationStatus1.setId(1L);
    boolean same = notificationStatus.equals(notificationStatus1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    NotificationStatus notificationStatus1 = new NotificationStatus();
    notificationStatus1.setId(null);
    boolean same = notificationStatus.equals(notificationStatus1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    NotificationStatus notificationStatus1 = new NotificationStatus();
    notificationStatus1.setId(ID);
    notificationStatus.setId(null);
    boolean same = notificationStatus.equals(notificationStatus1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = notificationStatus.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = notificationStatus.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = notificationStatus.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = notificationStatus.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
