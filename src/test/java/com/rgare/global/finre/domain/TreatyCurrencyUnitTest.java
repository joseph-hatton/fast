package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class TreatyCurrencyUnitTest {

  private static final Long ID = 1L;

  private static final Currency CURRENCY = new Currency();
  private static final String CREATE_USER = "CREATE_USER";
  private static final String LAST_USER = "LAST_USER";

  private static final ZonedDateTime CREATED_TS = ZonedDateTime.now();
  private static final ZonedDateTime LAST_DATE = ZonedDateTime.now();

  private TreatyCurrency treatyCurrency;

  String expectedResult = "TreatyCurrency{" +
    "id=" + ID +
    '}';

  @Before
  public void setup() {
    treatyCurrency = new TreatyCurrency();
    treatyCurrency.setId(ID);
    treatyCurrency.setCurrency(CURRENCY);
    treatyCurrency.setCreationTs(CREATED_TS);
    treatyCurrency.setCreationUser(CREATE_USER);
    treatyCurrency.setLastUpdate(LAST_DATE);
    treatyCurrency.setLastUser(LAST_USER);
  }

  @Test
  public void equalsGetTest() {
    Long id = treatyCurrency.getId();
    Currency currency = treatyCurrency.getCurrency();
    String creationUser = treatyCurrency.getCreationUser();
    String lastUser = treatyCurrency.getLastUser();
    ZonedDateTime creationTs = treatyCurrency.getCreationTs();
    ZonedDateTime lastUpdate = treatyCurrency.getLastUpdate();
    assertThat(id).isEqualTo(ID);
    assertThat(currency).isEqualTo(CURRENCY);
    assertThat(creationUser).isEqualTo(CREATE_USER);
    assertThat(creationTs).isEqualTo(CREATED_TS);
    assertThat(lastUpdate).isEqualTo(LAST_DATE);
    assertThat(lastUser).isEqualTo(LAST_USER);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = treatyCurrency.equals(treatyCurrency);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = treatyCurrency.equals(new TreatyCurrency());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    TreatyCurrency treatyCurrency1 = new TreatyCurrency();
    treatyCurrency1.setId(1L);
    boolean same = treatyCurrency.equals(treatyCurrency1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    TreatyCurrency treatyCurrency1 = new TreatyCurrency();
    treatyCurrency1.setId(null);
    boolean same = treatyCurrency.equals(treatyCurrency1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    TreatyCurrency treatyCurrency1 = new TreatyCurrency();
    treatyCurrency1.setId(ID);
    treatyCurrency.setId(null);
    boolean same = treatyCurrency.equals(treatyCurrency1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = treatyCurrency.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = treatyCurrency.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = treatyCurrency.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = treatyCurrency.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
