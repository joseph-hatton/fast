package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class XrefTreatyContactUnitTest {

  private static final Long ID = 1L;

  private static final Long TREATY_ID = 1L;
  private static final Contact CONTACT = new Contact();
  private static final ContactSubtype CONTACT_SUBTYPE = new ContactSubtype();
  private static final ContactType CONTACT_TYPE = new ContactType();
  private static final String CREATE_USER = "CREATE_USER";
  private static final String LAST_USER = "LAST_USER";

  private static final ZonedDateTime CREATED_TS = ZonedDateTime.now();
  private static final ZonedDateTime LAST_DATE = ZonedDateTime.now();

  private XrefTreatyContact xrefTreatyContact;

  String expectedResult = "XrefTreatyContact{" +
    "id=" + ID +
    ", treatyId='" + TREATY_ID + "'" +
    ", contact='" + CONTACT + "'" +
    ", lastUser='" + LAST_USER + "'" +
    '}';

  @Before
  public void setup() {

    xrefTreatyContact = new XrefTreatyContact();
    xrefTreatyContact.setId(ID);
    xrefTreatyContact.setTreatyId(TREATY_ID);
    xrefTreatyContact.setContact(CONTACT);
    xrefTreatyContact.setContactSubtype(CONTACT_SUBTYPE);
    xrefTreatyContact.setContactType(CONTACT_TYPE);
    xrefTreatyContact.setCreationTs(CREATED_TS);
    xrefTreatyContact.setCreationUser(CREATE_USER);
    xrefTreatyContact.setLastUpdate(LAST_DATE);
    xrefTreatyContact.setLastUser(LAST_USER);
  }

  @Test
  public void equalsGetTest() {
    Long id = xrefTreatyContact.getId();
    Long treatyId = xrefTreatyContact.getTreatyId();
    Contact contact = xrefTreatyContact.getContact();
    ContactType contactType = xrefTreatyContact.getContactType();
    ContactSubtype contactSubtype = xrefTreatyContact.getContactSubtype();

    String creationUser = xrefTreatyContact.getCreationUser();
    String lastUser = xrefTreatyContact.getLastUser();
    ZonedDateTime creationTs = xrefTreatyContact.getCreationTs();
    ZonedDateTime lastUpdate = xrefTreatyContact.getLastUpdate();

    assertThat(creationUser).isEqualTo(CREATE_USER);
    assertThat(creationTs).isEqualTo(CREATED_TS);
    assertThat(lastUpdate).isEqualTo(LAST_DATE);
    assertThat(lastUser).isEqualTo(LAST_USER);
    assertThat(id).isEqualTo(ID);
    assertThat(treatyId).isEqualTo(TREATY_ID);
    assertThat(contact).isEqualTo(CONTACT);
    assertThat(contactType).isEqualTo(CONTACT_TYPE);
    assertThat(contactSubtype).isEqualTo(CONTACT_SUBTYPE);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = xrefTreatyContact.equals(xrefTreatyContact);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = xrefTreatyContact.equals(new XrefTreatyContact());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    XrefTreatyContact xrefTreatyContact1 = new XrefTreatyContact();
    xrefTreatyContact1.setId(1L);
    boolean same = xrefTreatyContact.equals(xrefTreatyContact1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    XrefTreatyContact xrefTreatyContact1 = new XrefTreatyContact();
    xrefTreatyContact1.setId(null);
    boolean same = xrefTreatyContact.equals(xrefTreatyContact1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    XrefTreatyContact xrefTreatyContact1 = new XrefTreatyContact();
    xrefTreatyContact1.setId(ID);
    xrefTreatyContact.setId(null);
    boolean same = xrefTreatyContact.equals(xrefTreatyContact1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = xrefTreatyContact.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = xrefTreatyContact.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = xrefTreatyContact.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = xrefTreatyContact.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
