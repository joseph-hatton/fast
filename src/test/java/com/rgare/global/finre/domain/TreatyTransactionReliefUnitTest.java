package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class TreatyTransactionReliefUnitTest {

  private static final Long ID = 1L;
  private static final BigDecimal NET_TOTAL = new BigDecimal(1.1);
  private static final BigDecimal CONVERTED_NET_TOTAL = new BigDecimal(2.2);
  private static final LocalDate REPORTING_DATE = LocalDate.now();

  private static final String TRANSACTION_STATUS = "STATUS";
  private static final Integer IS_ACTIVE = 2;

  private static final Deal DEAL = new Deal();
  private static final Currency CURRENCY = new Currency();
  private static final Country COUNTRY = new Country();

  private static final TreatySubtransactionRelief TREATY_SUBTRANSACTION_RELIEF = new TreatySubtransactionRelief();

  private TreatyTransactionRelief treatyTransactionRelief;

  String expectedResult = "TreatyTransactionRelief{" +
    "id=" + ID +
    ", reportingDate='" + REPORTING_DATE + "'" +
    ", country='" + COUNTRY + "'" +
    ", netTotal='" + NET_TOTAL + "'" +
    ", convertedNetTotal='" + CONVERTED_NET_TOTAL + "'" +
    ", isActive='" + IS_ACTIVE + "'" +
    '}';

  @Before
  public void setup() {
    List<TreatySubtransactionRelief> treatySubtransactionReliefSet = new ArrayList<TreatySubtransactionRelief>();
    treatyTransactionRelief = new TreatyTransactionRelief();
    treatyTransactionRelief.setId(ID);
    treatyTransactionRelief.setNetTotal(NET_TOTAL);
    treatyTransactionRelief.setConvertedNetTotal(CONVERTED_NET_TOTAL);
    treatyTransactionRelief.setReportingDate(REPORTING_DATE);
    treatyTransactionRelief.setTransactionStatus(TRANSACTION_STATUS);
    treatyTransactionRelief.setIsActive(IS_ACTIVE);
    treatyTransactionRelief.setDeal(DEAL);
    treatyTransactionRelief.setCurrency(CURRENCY);
    treatyTransactionRelief.setCountry(COUNTRY);
    treatySubtransactionReliefSet.add(TREATY_SUBTRANSACTION_RELIEF);
    treatyTransactionRelief.setSubTransactions(treatySubtransactionReliefSet);
  }

  @Test
  public void equalsGetTest() {
    Long id = treatyTransactionRelief.getId();
    BigDecimal netTotal = treatyTransactionRelief.getNetTotal();
    BigDecimal convertedNetTotal =  treatyTransactionRelief.getConvertedNetTotal();
    LocalDate reportingDate = treatyTransactionRelief.getReportingDate();
    String transactionStatus =treatyTransactionRelief.getTransactionStatus();
    Integer isActive = treatyTransactionRelief.getIsActive();
    Deal deal = treatyTransactionRelief.getDeal();
    Currency currency = treatyTransactionRelief.getCurrency();
    Country country = treatyTransactionRelief.getCountry();
    List<TreatySubtransactionRelief> treatySubtransactionReliefs = treatyTransactionRelief.getSubTransactions();

    assertThat(netTotal).isEqualTo(NET_TOTAL);
    assertThat(convertedNetTotal).isEqualTo(CONVERTED_NET_TOTAL);
    assertThat(reportingDate).isEqualTo(REPORTING_DATE);
    assertThat(transactionStatus).isEqualTo(TRANSACTION_STATUS);
    assertThat(isActive).isEqualTo(IS_ACTIVE);
    assertThat(deal).isEqualTo(DEAL);
    assertThat(currency).isEqualTo(CURRENCY);
    assertThat(country).isEqualTo(COUNTRY);
    assertThat(id).isEqualTo(ID);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = treatyTransactionRelief.equals(treatyTransactionRelief);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = treatyTransactionRelief.equals(new TreatyTransactionRelief());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    TreatyTransactionRelief treatyTransactionRelief1 = new TreatyTransactionRelief();
    treatyTransactionRelief1.setId(1L);
    boolean same = treatyTransactionRelief.equals(treatyTransactionRelief1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    TreatyTransactionRelief treatyTransactionRelief1 = new TreatyTransactionRelief();
    treatyTransactionRelief1.setId(null);
    boolean same = treatyTransactionRelief.equals(treatyTransactionRelief1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    TreatyTransactionRelief treatyTransactionRelief1 = new TreatyTransactionRelief();
    treatyTransactionRelief1.setId(ID);
    treatyTransactionRelief.setId(null);
    boolean same = treatyTransactionRelief.equals(treatyTransactionRelief1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = treatyTransactionRelief.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = treatyTransactionRelief.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = treatyTransactionRelief.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = treatyTransactionRelief.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
