package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class AllocationTypeUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  private AllocationType allocationType;


  String expectedResult = "AllocationType{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    '}';

  @Before
  public void setup() {

    allocationType = new AllocationType();
    allocationType.setId(ID);
    allocationType.setDescription(DESCRIPTION);
    allocationType.setName(NAME);

  }

  @Test
  public void equalsGetTest() {
    Long id = allocationType.getId();
    String name = allocationType.getName();
    String description = allocationType.getDescription();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = allocationType.equals(allocationType);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = allocationType.equals(new AllocationType());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    AllocationType allocationType1 = new AllocationType();
    allocationType1.setId(1L);
    boolean same = allocationType.equals(allocationType1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    AllocationType allocationType1 = new AllocationType();
    allocationType1.setId(null);
    boolean same = allocationType.equals(allocationType1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    AllocationType allocationType1 = new AllocationType();
    allocationType1.setId(ID);
    allocationType.setId(null);
    boolean same = allocationType.equals(allocationType1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = allocationType.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = allocationType.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = allocationType.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = allocationType.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
