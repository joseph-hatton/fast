package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class TransactionStatusUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  private TransactionStatus transactionStatus;

  String expectedResult = "TransactionStatus{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    '}';

  @Before
  public void setup() {
    transactionStatus = new TransactionStatus();
    transactionStatus.setId(ID);
    transactionStatus.setName(NAME);
    transactionStatus.setDescription(DESCRIPTION);
  }

  @Test
  public void equalsGetTest() {
    Long id = transactionStatus.getId();
    String name = transactionStatus.getName();
    String description = transactionStatus.getDescription();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = transactionStatus.equals(transactionStatus);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = transactionStatus.equals(new TransactionStatus());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    TransactionStatus transactionStatus1 = new TransactionStatus();
    transactionStatus1.setId(1L);
    boolean same = transactionStatus.equals(transactionStatus1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    TransactionStatus transactionStatus1 = new TransactionStatus();
    transactionStatus1.setId(null);
    boolean same = transactionStatus.equals(transactionStatus1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    TransactionStatus transactionStatus1 = new TransactionStatus();
    transactionStatus1.setId(ID);
    transactionStatus.setId(null);
    boolean same = transactionStatus.equals(transactionStatus1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = transactionStatus.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = transactionStatus.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = transactionStatus.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = transactionStatus.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
