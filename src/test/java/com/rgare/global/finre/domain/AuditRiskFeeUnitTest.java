package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuditRiskFeeUnitTest {

  private static final Long ID = 1L;
  private static final Long TREATY_ID = 1L;
  private static final Long RISK_FEE_ID = 1L;
  private static final String TREATY_STATUS = "STATUS";
  private static final String ACTION = "ACTION";
  private static final String COMMENTS = "COMMENTS";
  private static final String CREATION_USER = "USER";
  private static final String MGMT_INPUT_STATUS = "STATUS";

  private AuditRiskFee auditRiskFee;

  String expectedResult = "AuditRiskFee{" +
    "id=" + ID +
    ", treatyId='" + TREATY_ID + "'" +
    ", riskFeeId='" + RISK_FEE_ID + "'" +
    ", treatyStatus='" + TREATY_STATUS + "'" +
    ", action='" + ACTION + "'" +
    ", comments='" + COMMENTS + "'" +
    ", creationUser='" + CREATION_USER + "'" +
    ", managementInputStatus='" + MGMT_INPUT_STATUS + "'" +
    '}';

  @Before
  public void setup() {

    auditRiskFee = new AuditRiskFee();
    auditRiskFee.setId(ID);
    auditRiskFee.setTreatyId(TREATY_ID);
    auditRiskFee.setRiskFeeId(RISK_FEE_ID);
    auditRiskFee.setTreatyStatus(TREATY_STATUS);
    auditRiskFee.setAction(ACTION);
    auditRiskFee.setComments(COMMENTS);
    auditRiskFee.setCreationUser(CREATION_USER);
    auditRiskFee.setManagementInputStatus(MGMT_INPUT_STATUS);
  }

  @Test
  public void equalsGetTest() {
    Long id = auditRiskFee.getId();
    Long treatyId = auditRiskFee.getTreatyId();
    Long riskFeeId = auditRiskFee.getRiskFeeId();
    String status = auditRiskFee.getTreatyStatus();
    String action = auditRiskFee.getAction();
    String comments = auditRiskFee.getComments();
    String createUser = auditRiskFee.getCreationUser();
    String mgmtInputStatus = auditRiskFee.getManagementInputStatus();

    assertThat(id).isEqualTo(ID);
    assertThat(treatyId).isEqualTo(TREATY_ID);
    assertThat(riskFeeId).isEqualTo(RISK_FEE_ID);
    assertThat(status).isEqualTo(TREATY_STATUS);
    assertThat(action).isEqualTo(ACTION);
    assertThat(comments).isEqualTo(COMMENTS);
    assertThat(createUser).isEqualTo(CREATION_USER);
    assertThat(mgmtInputStatus).isEqualTo(MGMT_INPUT_STATUS);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = auditRiskFee.equals(auditRiskFee);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = auditRiskFee.equals(new AuditRiskFee());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    AuditRiskFee auditRiskFee1 = new AuditRiskFee();
    auditRiskFee1.setId(1L);
    boolean same = auditRiskFee.equals(auditRiskFee1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    AuditRiskFee auditRiskFee1 = new AuditRiskFee();
    auditRiskFee1.setId(null);
    boolean same = auditRiskFee.equals(auditRiskFee1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    AuditRiskFee auditRiskFee1 = new AuditRiskFee();
    auditRiskFee1.setId(ID);
    auditRiskFee.setId(null);
    boolean same = auditRiskFee.equals(auditRiskFee1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = auditRiskFee.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = auditRiskFee.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = auditRiskFee.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = auditRiskFee.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
