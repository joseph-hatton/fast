package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class TreatyTransactionCashUnitTest {

  private static final Long ID = 1L;

  private static final LocalDate settlementDate = LocalDate.now();
  private static final BigDecimal netTotal = new BigDecimal(1.1);
  private static final BigDecimal convertedNetTotal = new BigDecimal(2.2);
  private static final Integer isActive = 1;
  private static final Integer financeNotification = 2;
  private static final Integer mixTransactions = 3;
  private static final LocalDate accountingDate = LocalDate.now();
  private static final String alterConvertedUser = "alterConvertedUser";
  private static final LocalDate alterConvertedDate = LocalDate.now();
  private static final Integer currencyConvertedFlag = 4;
  private static final BigDecimal originalConvertedAmount = new BigDecimal(3.3);
  private static final String contacts = "contacts";
  private static final String sender = "sender";
  private static final String emailComment = "emailComment";
  private static final String fileContent = "fileContent";
  private static final String fileName = "fileName";
  private static final String fileType = "fileType";
  private static final Treaty treaty = new Treaty();
  private static final Currency currency = new Currency();
  private static final FundingType fundingType = new FundingType();
  private static final Company company = new Company();
  private static final TransactionStatus transactionStatus = new TransactionStatus();
  private static final ZonedDateTime lastUpdate = LocalDate.now().atStartOfDay(ZoneId.systemDefault());
  private static final String lastUser = "lastUser";

  private TreatyTransactionCash treatyTransactionCash;

  String expectedResult = "TreatyTransactionCash{" +
    "id=" + ID +
    ", originalConvertedAmount='" + originalConvertedAmount + "'" +
    ", mixTransactions='" + mixTransactions + "'" +
    ", convertedNetTotal='" + convertedNetTotal + "'" +
    ", isActive='" + isActive + "'" +
    ", lastUser='" + lastUser + "'" +
    ", lastUpdate='" + lastUpdate + "'" +
    ", transactionStatus='" + transactionStatus.getName() + "'" +
    ", contacts='" + contacts + "'" +
    '}';

  @Before
  public void setup() {

    treatyTransactionCash = new TreatyTransactionCash();
    treatyTransactionCash.setId(ID);
    treatyTransactionCash.setSettlementDate(settlementDate);
    treatyTransactionCash.setNetTotal(netTotal);
    treatyTransactionCash.setConvertedNetTotal(convertedNetTotal);
    treatyTransactionCash.setIsActive(isActive);
    treatyTransactionCash.setFinanceNotification(financeNotification);
    treatyTransactionCash.setMixTransactions(mixTransactions);
    treatyTransactionCash.setAccountingDate(accountingDate);
    treatyTransactionCash.setAlterConvertedUser(alterConvertedUser);
    treatyTransactionCash.setAlterConvertedDate(alterConvertedDate);
    treatyTransactionCash.setCurrencyConvertedFlag(currencyConvertedFlag);
    treatyTransactionCash.setOriginalConvertedAmount(originalConvertedAmount);
    treatyTransactionCash.setContacts(contacts);
    treatyTransactionCash.setSender(sender);
    treatyTransactionCash.setEmailComment(emailComment);
    treatyTransactionCash.setFileContent(fileContent);
    treatyTransactionCash.setFileName(fileName);
    treatyTransactionCash.setFileType(fileType);
    treatyTransactionCash.setTreaty(treaty);
    treatyTransactionCash.setCurrency(currency);
    treatyTransactionCash.setFundingType(fundingType);
    treatyTransactionCash.setCompany(company);
    treatyTransactionCash.setTransactionStatus(transactionStatus);
    treatyTransactionCash.setLastUser(lastUser);
    treatyTransactionCash.setLastUpdate(lastUpdate);

    Set<TreatySubtransactionCash> treatySubtransactionCashes = new HashSet<>();
    treatySubtransactionCashes.add(new TreatySubtransactionCash());
    treatyTransactionCash.setSubTransactions(treatySubtransactionCashes);

  }

  @Test
  public void equalsGetTest() {
    Long id = treatyTransactionCash.getId();
    LocalDate settlementDate1 = treatyTransactionCash.getSettlementDate();
    BigDecimal netTotal1 = treatyTransactionCash.getNetTotal();
    BigDecimal convertedNetTotal1 = treatyTransactionCash.getConvertedNetTotal();
    Integer isActive1 = treatyTransactionCash.getIsActive();
    Integer financeNotification1 = treatyTransactionCash.getFinanceNotification();
    Integer mixTransactions1 = treatyTransactionCash.getMixTransactions();
    LocalDate accountingDate1 = treatyTransactionCash.getAccountingDate();
    String alterConvertedUser1 = treatyTransactionCash.getAlterConvertedUser();
    LocalDate alterConvertedDate1 = treatyTransactionCash.getAlterConvertedDate();
    Integer currencyConvertedFlag1 = treatyTransactionCash.getCurrencyConvertedFlag();
    BigDecimal originalConvertedAmount1 = treatyTransactionCash.getOriginalConvertedAmount();
    String contacts1 = treatyTransactionCash.getContacts();
    String sender1 = treatyTransactionCash.getSender();
    String emailComment1 = treatyTransactionCash.getEmailComment();
    String fileContent1 = treatyTransactionCash.getFileContent();
    String fileName1 = treatyTransactionCash.getFileName();
    String fileType1 = treatyTransactionCash.getFileType();
    Treaty treaty1 = treatyTransactionCash.getTreaty();
    Currency currency1 = treatyTransactionCash.getCurrency();
    FundingType fundingType1 = treatyTransactionCash.getFundingType();
    Company company1 = treatyTransactionCash.getCompany();
    TransactionStatus transactionStatus1 = treatyTransactionCash.getTransactionStatus();
    String lastUser1 = treatyTransactionCash.getLastUser();
    ZonedDateTime lastUpdate1 = treatyTransactionCash.getLastUpdate();

    Set<TreatySubtransactionCash> treatySubtransactionCashes = treatyTransactionCash.getSubTransactions();

    assertThat(id).isEqualTo(ID);
    assertThat(settlementDate1).isEqualTo(settlementDate);
    assertThat(netTotal1).isEqualTo(netTotal);
    assertThat(convertedNetTotal1).isEqualTo(convertedNetTotal);
    assertThat(isActive1).isEqualTo(isActive);
    assertThat(financeNotification1).isEqualTo(financeNotification);
    assertThat(mixTransactions1).isEqualTo(mixTransactions);
    assertThat(accountingDate1).isEqualTo(accountingDate);
    assertThat(alterConvertedDate1).isEqualTo(alterConvertedDate);
    assertThat(currencyConvertedFlag1).isEqualTo(currencyConvertedFlag);
    assertThat(originalConvertedAmount1).isEqualTo(originalConvertedAmount);
    assertThat(contacts1).isEqualTo(contacts);
    assertThat(sender1).isEqualTo(sender);
    assertThat(emailComment1).isEqualTo(emailComment);
    assertThat(fileContent1).isEqualTo(fileContent);
    assertThat(fileName1).isEqualTo(fileName);
    assertThat(fileType1).isEqualTo(fileType);
    assertThat(treaty1).isEqualTo(treaty);
    assertThat(currency1).isEqualTo(currency);
    assertThat(fundingType1).isEqualTo(fundingType);
    assertThat(transactionStatus1).isEqualTo(transactionStatus);
    assertThat(lastUpdate1).isEqualTo(lastUpdate);
    assertThat(company1).isEqualTo(company);
    assertThat(lastUser1).isEqualTo(lastUser);
    assertThat(alterConvertedUser1).isEqualTo(alterConvertedUser);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = treatyTransactionCash.equals(treatyTransactionCash);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = treatyTransactionCash.equals(new TreatyTransactionCash());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    TreatyTransactionCash treatyTransactionCash1 = new TreatyTransactionCash();
    treatyTransactionCash1.setId(1L);
    boolean same = treatyTransactionCash.equals(treatyTransactionCash1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    TreatyTransactionCash treatyTransactionCash1 = new TreatyTransactionCash();
    treatyTransactionCash1.setId(null);
    boolean same = treatyTransactionCash.equals(treatyTransactionCash1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    TreatyTransactionCash treatyTransactionCash1 = new TreatyTransactionCash();
    treatyTransactionCash1.setId(ID);
    treatyTransactionCash.setId(null);
    boolean same = treatyTransactionCash.equals(treatyTransactionCash1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = treatyTransactionCash.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = treatyTransactionCash.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = treatyTransactionCash.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = treatyTransactionCash.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
