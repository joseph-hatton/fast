package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductStructureUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String STRUCTURE_CODE = "CODE";

  private ProductStructure productStructure;

  String expectedResult = "ProductStructure{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", structureCode='" + STRUCTURE_CODE + "'" +
    '}';

  @Before
  public void setup() {
    productStructure = new ProductStructure();
    productStructure.setId(ID);
    productStructure.setName(NAME);
    productStructure.setStructureCode(STRUCTURE_CODE);
  }

  @Test
  public void equalsGetTest() {
    Long id = productStructure.getId();
    String name = productStructure.getName();
    String structureCode = productStructure.getStructureCode();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(structureCode).isEqualTo(STRUCTURE_CODE);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = productStructure.equals(productStructure);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = productStructure.equals(new ProductStructure());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    ProductStructure productStructure1 = new ProductStructure();
    productStructure1.setId(1L);
    boolean same = productStructure.equals(productStructure1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    ProductStructure productStructure1 = new ProductStructure();
    productStructure1.setId(null);
    boolean same = productStructure.equals(productStructure1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    ProductStructure productStructure1 = new ProductStructure();
    productStructure1.setId(ID);
    productStructure.setId(null);
    boolean same = productStructure.equals(productStructure1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = productStructure.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = productStructure.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = productStructure.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = productStructure.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
