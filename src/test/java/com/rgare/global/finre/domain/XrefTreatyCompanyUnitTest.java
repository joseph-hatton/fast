package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class XrefTreatyCompanyUnitTest {

  private static final Long ID = 1L;

  private static final Long TREATY_ID = 1L;
  private static final String COMPANY_TYPE = "TYPE";
  private static final Company COMPANY = new Company();

  private static final String CREATE_USER = "CREATE_USER";
  private static final String LAST_USER = "LAST_USER";

  private static final ZonedDateTime CREATED_TS = ZonedDateTime.now();
  private static final ZonedDateTime LAST_DATE = ZonedDateTime.now();

  private XrefTreatyCompany xrefTreatyCompany;

  String expectedResult = "XrefTreatyCompany{" +
    "id=" + ID +
    ", treatyId='" + TREATY_ID + "'" +
    '}';

  @Before
  public void setup() {
    xrefTreatyCompany = new XrefTreatyCompany();
    xrefTreatyCompany.setId(ID);
    xrefTreatyCompany.setTreatyId(TREATY_ID);
    xrefTreatyCompany.setCompany(COMPANY);
    xrefTreatyCompany.setCompanyType(COMPANY_TYPE);
    xrefTreatyCompany.setCreationTs(CREATED_TS);
    xrefTreatyCompany.setCreationUser(CREATE_USER);
    xrefTreatyCompany.setLastUpdate(LAST_DATE);
    xrefTreatyCompany.setLastUser(LAST_USER);

  }

  @Test
  public void equalsGetTest() {
    Long id = xrefTreatyCompany.getId();
    Long treatyId = xrefTreatyCompany.getTreatyId();
    Company company = xrefTreatyCompany.getCompany();
    String companyType = xrefTreatyCompany.getCompanyType();

    String creationUser = xrefTreatyCompany.getCreationUser();
    String lastUser = xrefTreatyCompany.getLastUser();
    ZonedDateTime creationTs = xrefTreatyCompany.getCreationTs();
    ZonedDateTime lastUpdate = xrefTreatyCompany.getLastUpdate();

    assertThat(creationUser).isEqualTo(CREATE_USER);
    assertThat(creationTs).isEqualTo(CREATED_TS);
    assertThat(lastUpdate).isEqualTo(LAST_DATE);
    assertThat(lastUser).isEqualTo(LAST_USER);
    assertThat(id).isEqualTo(ID);
    assertThat(treatyId).isEqualTo(TREATY_ID);
    assertThat(company).isEqualTo(COMPANY);
    assertThat(companyType).isEqualTo(COMPANY_TYPE);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = xrefTreatyCompany.equals(xrefTreatyCompany);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = xrefTreatyCompany.equals(new XrefTreatyCompany());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    XrefTreatyCompany xrefTreatyCompany1 = new XrefTreatyCompany();
    xrefTreatyCompany1.setId(1L);
    boolean same = xrefTreatyCompany.equals(xrefTreatyCompany1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    XrefTreatyCompany xrefTreatyCompany1 = new XrefTreatyCompany();
    xrefTreatyCompany1.setId(null);
    boolean same = xrefTreatyCompany.equals(xrefTreatyCompany1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    XrefTreatyCompany xrefTreatyCompany1 = new XrefTreatyCompany();
    xrefTreatyCompany1.setId(ID);
    xrefTreatyCompany.setId(null);
    boolean same = xrefTreatyCompany.equals(xrefTreatyCompany1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = xrefTreatyCompany.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = xrefTreatyCompany.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = xrefTreatyCompany.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = xrefTreatyCompany.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
