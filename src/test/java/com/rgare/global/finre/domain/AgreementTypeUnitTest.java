package com.rgare.global.finre.domain;

import com.rgare.global.finre.domain.AgreementType;
import com.rgare.global.finre.repository.AgreementTypeRepository;
import com.rgare.global.finre.repository.search.AgreementTypeSearchRepository;
import com.rgare.global.finre.service.impl.RiskCriteriaServiceImpl;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class AgreementTypeUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  private AgreementType agreementType;

  String expectedResult = "AgreementType{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    '}';

  @Before
  public void setup() {

    agreementType = new AgreementType();
    agreementType.setId(ID);
    agreementType.setName(NAME);
    agreementType.setDescription(DESCRIPTION);
  }

  @Test
  public void equalsGetTest() {
    Long id = agreementType.getId();
    String name = agreementType.getName();
    String description = agreementType.getDescription();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = agreementType.equals(agreementType);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = agreementType.equals(new AgreementType());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    AgreementType agreementType1 = new AgreementType();
    agreementType1.setId(1L);
    boolean same = agreementType.equals(agreementType1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = agreementType.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    AgreementType agreementType1 = new AgreementType();
    agreementType1.setId(null);
    boolean same = agreementType.equals(agreementType1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    AgreementType agreementType1 = new AgreementType();
    agreementType1.setId(ID);
    agreementType.setId(null);
    boolean same = agreementType.equals(agreementType1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = agreementType.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = agreementType.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = agreementType.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
