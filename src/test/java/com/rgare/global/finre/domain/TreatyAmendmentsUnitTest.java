package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class TreatyAmendmentsUnitTest {

  private static final Long ID = 1L;
  private static final String AMENDMENT_NAME = "NAME";
  private static final Integer IS_ACTIVE = 1;

  private static final LocalDate EFFECTIVE_DATE = LocalDate.now();
  private static final String COMMENTS = "COMMENTS";
  private static final String REASON = "REASON";

  private static final NotificationStatus NOTIFICATION_STATUS = new NotificationStatus();

  private static final Treaty TREATY = new Treaty();

  private TreatyAmendments treatyAmendments;

  String expectedResult = "TreatyAmendments{" +
    "id=" + ID +
    ", amendmentName='" + AMENDMENT_NAME + "'" +
    ", effectiveDate='" + EFFECTIVE_DATE + "'" +
    ", comments='" + COMMENTS + "'" +
    ", reason='" + REASON + "'" +
    ", isActive='" + IS_ACTIVE + "'" +
    '}';

  @Before
  public void setup() {
    treatyAmendments = new TreatyAmendments();
    treatyAmendments.setId(ID);
    treatyAmendments.setIsActive(IS_ACTIVE);
    treatyAmendments.setAmendmentName(AMENDMENT_NAME);
    treatyAmendments.setEffectiveDate(EFFECTIVE_DATE);
    treatyAmendments.setComments(COMMENTS);
    treatyAmendments.setReason(REASON);
    treatyAmendments.setTreaty(TREATY);
    treatyAmendments.setNotificationStatus(NOTIFICATION_STATUS);
  }

  @Test
  public void equalsGetTest() {
    Long id = treatyAmendments.getId();
    Integer isActive = treatyAmendments.getIsActive();
    String amendmentName = treatyAmendments.getAmendmentName();
    LocalDate effectiveDate =  treatyAmendments.getEffectiveDate();
    String comments = treatyAmendments.getComments();
    String reason = treatyAmendments.getReason();
    Treaty treaty = treatyAmendments.getTreaty();
    NotificationStatus notificationStatus = treatyAmendments.getNotificationStatus();

    assertThat(id).isEqualTo(ID);
    assertThat(isActive).isEqualTo(IS_ACTIVE);
    assertThat(amendmentName).isEqualTo(AMENDMENT_NAME);
    assertThat(effectiveDate).isEqualTo(EFFECTIVE_DATE);
    assertThat(comments).isEqualTo(COMMENTS);
    assertThat(reason).isEqualTo(REASON);
    assertThat(treaty).isEqualTo(TREATY);
    assertThat(notificationStatus).isEqualTo(NOTIFICATION_STATUS);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = treatyAmendments.equals(treatyAmendments);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = treatyAmendments.equals(new TreatyAmendments());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    TreatyAmendments treatyAmendments1 = new TreatyAmendments();
    treatyAmendments1.setId(1L);
    boolean same = treatyAmendments.equals(treatyAmendments1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    TreatyAmendments treatyAmendments1 = new TreatyAmendments();
    treatyAmendments1.setId(null);
    boolean same = treatyAmendments.equals(treatyAmendments1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    TreatyAmendments treatyAmendments1 = new TreatyAmendments();
    treatyAmendments1.setId(ID);
    treatyAmendments.setId(null);
    boolean same = treatyAmendments.equals(treatyAmendments1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = treatyAmendments.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = treatyAmendments.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = treatyAmendments.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = treatyAmendments.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
