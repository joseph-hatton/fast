package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class FastopediaUnitTest {

  private static final Long ID = 1L;

  private static final String TITLE = "TITLE";
  private static final String CONTENT = "CONTENT";
  private static final Integer ISACTIVE = 1;
  private static final Integer ORDER = 1;
  private static final Integer PARENTID = 1;

  private Fastopedia Fastopedia;

  String expectedResult = "Fastopedia{" +
    "id=" + ID +
    ", title='" + TITLE + "'" +
    ", content='" + CONTENT + "'" +
    '}';

  @Before
  public void setup() {

    Fastopedia = new Fastopedia();
    Fastopedia.setId(ID);
    Fastopedia.setTitle(TITLE);
    Fastopedia.setContent(CONTENT);
    Fastopedia.setOrder(ORDER);
  }

  @Test
  public void fastopediaGetTest() {
    Long id = Fastopedia.getId();
    String title = Fastopedia.getTitle();
    String content = Fastopedia.getContent();
    Integer order = Fastopedia.getOrder();

    assertThat(id).isEqualTo(ID);
    assertThat(title).isEqualTo(TITLE);
    assertThat(content).isEqualTo(CONTENT);
    assertThat(order).isEqualTo(ORDER);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = Fastopedia.equals(Fastopedia);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = Fastopedia.equals(new Fastopedia());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    Fastopedia Fastopedia1 = new Fastopedia();
    Fastopedia1.setId(1L);
    boolean same = Fastopedia.equals(Fastopedia1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    Fastopedia Fastopedia1 = new Fastopedia();
    Fastopedia1.setId(null);
    boolean same = Fastopedia.equals(Fastopedia1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    Fastopedia Fastopedia1 = new Fastopedia();
    Fastopedia1.setId(ID);
    Fastopedia.setId(null);
    boolean same = Fastopedia.equals(Fastopedia1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = Fastopedia.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = Fastopedia.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = Fastopedia.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = Fastopedia.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
