package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductTypeUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  private ProductType productType;

  String expectedResult = "ProductType{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    '}';

  @Before
  public void setup() {
    productType = new ProductType();
    productType.setId(ID);
    productType.setName(NAME);
    productType.setDescription(DESCRIPTION);
  }

  @Test
  public void equalsGetTest() {
    Long id = productType.getId();
    String name = productType.getName();
    String description = productType.getDescription();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = productType.equals(productType);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = productType.equals(new ProductType());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    ProductType productType1 = new ProductType();
    productType1.setId(1L);
    boolean same = productType.equals(productType1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    ProductType productType1 = new ProductType();
    productType1.setId(null);
    boolean same = productType.equals(productType1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    ProductType productType1 = new ProductType();
    productType1.setId(ID);
    productType.setId(null);
    boolean same = productType.equals(productType1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = productType.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = productType.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = productType.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = productType.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
