package com.rgare.global.finre.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductSubtypeUnitTest {

  private static final Long ID = 1L;

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  private ProductSubtype productSubtype;

  String expectedResult = "ProductSubtype{" +
    "id=" + ID +
    ", name='" + NAME + "'" +
    ", description='" + DESCRIPTION + "'" +
    '}';

  @Before
  public void setup() {
    productSubtype = new ProductSubtype();
    productSubtype.setId(ID);
    productSubtype.setName(NAME);
    productSubtype.setDescription(DESCRIPTION);
  }

  @Test
  public void equalsGetTest() {
    Long id = productSubtype.getId();
    String name = productSubtype.getName();
    String description = productSubtype.getDescription();
    assertThat(id).isEqualTo(ID);
    assertThat(name).isEqualTo(NAME);
    assertThat(description).isEqualTo(DESCRIPTION);
  }

  @Test
  public void overrideEqualsTest() {
    boolean same = productSubtype.equals(productSubtype);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideNotEqualsTest() {
    boolean same = productSubtype.equals(new ProductSubtype());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIdsTest() {
    ProductSubtype productSubtype1 = new ProductSubtype();
    productSubtype1.setId(1L);
    boolean same = productSubtype.equals(productSubtype1);
    assertThat(same).isTrue();
  }

  @Test
  public void overrideEqualsIdIsNullTest() {
    ProductSubtype productSubtype1 = new ProductSubtype();
    productSubtype1.setId(null);
    boolean same = productSubtype.equals(productSubtype1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareIsNullTest() {
    ProductSubtype productSubtype1 = new ProductSubtype();
    productSubtype1.setId(ID);
    productSubtype.setId(null);
    boolean same = productSubtype.equals(productSubtype1);
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsCompareClassTest() {
    boolean same = productSubtype.equals(new Object());
    assertThat(same).isFalse();
  }

  @Test
  public void overrideEqualsIsNullTest() {
    boolean same = productSubtype.equals(null);
    assertThat(same).isFalse();
  }

  @Test
  public void toStringTest() {

    String actual = productSubtype.toString();
    assertThat(actual).isEqualTo(expectedResult);
  }

  @Test
  public void hashcodeTest() {

    int actual = productSubtype.hashCode();

    assertThat(actual).isEqualTo(ID.intValue());
  }


}
