package com.rgare.global.finre.service.dto;

import com.rgare.global.finre.config.Constants;
import com.rgare.global.finre.domain.Authority;
import com.rgare.global.finre.domain.User;
import org.hibernate.validator.constraints.Email;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A DTO representing a user, with his authorities.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserDTOUnitTest {

    private static final Long ID = 1L;

    private static final String LOGIN = "login";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String EMAIL = "EMAIL";
    private static final String IMAGE_URL = "IMAGE";
    private static final boolean ACTIVATED = false;
    private static final String LANG_KEY = "LANG_KEY";
    private static final String CREATED_BY = "CREATED_BY";
    private static final ZonedDateTime CREATED_DATE = ZonedDateTime.now();
    private static final String LAST_MODIFIED_BY = "LAST_MODIFIED_BY";
    private static final ZonedDateTime LAST_MODIFIED_DATE = ZonedDateTime.now();
    private Set<String> AUTHORITIES;

    private UserDTO userDTO;

    private User user;

    private String expectedResult =
      "UserDTO{" +
      "login='" + LOGIN + '\'' +
    ", firstName='" + FIRST_NAME + '\'' +
    ", lastName='" + LAST_NAME + '\'' +
    ", email='" + EMAIL + '\'' +
    ", imageUrl='" + IMAGE_URL + '\'' +
    ", activated=" + ACTIVATED +
    ", langKey='" + LANG_KEY + '\'' +
    ", createdBy=" + CREATED_BY +
    ", createdDate=" + CREATED_DATE +
    ", lastModifiedBy='" + LAST_MODIFIED_BY + '\'' +
    ", lastModifiedDate=" + LAST_MODIFIED_DATE +
    ", authorities=" + null +
    "}";

    @Before
    public void setup() {
        userDTO = new UserDTO();

        Set<Authority> authorities = new HashSet<Authority>();
        Authority authority = new Authority();
        authority.setName("ADMIN");
        authorities.add(authority);
        user = new User();
        user.setId(ID);
        user.setLogin(LOGIN);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setEmail(EMAIL);
        user.setActivated(ACTIVATED);
        user.setImageUrl(IMAGE_URL);
        user.setLangKey(LANG_KEY);
        user.setCreatedBy(CREATED_BY);
        user.setCreatedDate(CREATED_DATE);
        user.setLastModifiedBy(LAST_MODIFIED_BY);
        user.setLastModifiedDate(LAST_MODIFIED_DATE);
        user.setAuthorities(authorities);

      userDTO = new UserDTO(user);
      userDTO.setId(ID);
      userDTO.setLastModifiedDate(LAST_MODIFIED_DATE);
    }

    @Test
    public void testGetters(){
      Set<Authority> AUTHORITIES = new HashSet<Authority>();
      Authority authority = new Authority();
      authority.setName("ADMIN");
      AUTHORITIES.add(authority);

      userDTO = new UserDTO(user);
      Long  id = userDTO.getId();
      String login = userDTO.getLogin();
      String firstName = userDTO.getFirstName();
      String lastName = userDTO.getLastName();
      String email = userDTO.getEmail();
      String imageUrl = userDTO.getImageUrl();
      String langKey = userDTO.getLangKey();
      String createdBy = userDTO.getCreatedBy();
      ZonedDateTime createdDate = userDTO.getCreatedDate();
      String lastModifiedBy = userDTO.getLastModifiedBy();
      ZonedDateTime lastModifiedDate = userDTO.getLastModifiedDate();
      Set<String> authorities = userDTO.getAuthorities();

      assertThat(id).isEqualTo(ID);
      assertThat(login).isEqualTo(LOGIN);
      assertThat(firstName).isEqualTo(FIRST_NAME);
      assertThat(lastName).isEqualTo(LAST_NAME);
      assertThat(email).isEqualTo(EMAIL);
      assertThat(imageUrl).isEqualTo(IMAGE_URL);
      assertThat(langKey).isEqualTo(LANG_KEY);
      assertThat(createdBy).isEqualTo(CREATED_BY);
      assertThat(createdDate).isEqualTo(CREATED_DATE);
      assertThat(lastModifiedBy).isEqualTo(LAST_MODIFIED_BY);
      assertThat(lastModifiedDate).isEqualTo(LAST_MODIFIED_DATE);
    }

  @Test
  public void testConstructorGetters(){
    Set<String> AUTHORITIES = new HashSet<String>();
    AUTHORITIES.add("ADMIN");

    userDTO = new
    UserDTO(ID, LOGIN, FIRST_NAME, LAST_NAME,
      EMAIL, ACTIVATED, IMAGE_URL, LANG_KEY,
      CREATED_BY, CREATED_DATE, LAST_MODIFIED_BY, LAST_MODIFIED_DATE,
      AUTHORITIES);

    Long  id = userDTO.getId();
    String login = userDTO.getLogin();
    String firstName = userDTO.getFirstName();
    String lastName = userDTO.getLastName();
    String email = userDTO.getEmail();
    String imageUrl = userDTO.getImageUrl();
    String langKey = userDTO.getLangKey();
    String createdBy = userDTO.getCreatedBy();
    ZonedDateTime createdDate = userDTO.getCreatedDate();
    String lastModifiedBy = userDTO.getLastModifiedBy();
    ZonedDateTime lastModifiedDate = userDTO.getLastModifiedDate();
    Set<String> authorities = userDTO.getAuthorities();

    assertThat(id).isEqualTo(ID);
    assertThat(login).isEqualTo(LOGIN);
    assertThat(firstName).isEqualTo(FIRST_NAME);
    assertThat(lastName).isEqualTo(LAST_NAME);
    assertThat(email).isEqualTo(EMAIL);
    assertThat(imageUrl).isEqualTo(IMAGE_URL);
    assertThat(langKey).isEqualTo(LANG_KEY);
    assertThat(createdBy).isEqualTo(CREATED_BY);
    assertThat(createdDate).isEqualTo(CREATED_DATE);
    assertThat(lastModifiedBy).isEqualTo(LAST_MODIFIED_BY);
    assertThat(lastModifiedDate).isEqualTo(LAST_MODIFIED_DATE);
  }
}
