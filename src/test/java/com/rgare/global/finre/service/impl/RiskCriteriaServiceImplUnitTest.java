package com.rgare.global.finre.service.impl;

import com.rgare.global.finre.domain.RiskCriteria;
import com.rgare.global.finre.repository.RiskCriteriaRepository;
import com.rgare.global.finre.repository.search.RiskCriteriaSearchRepository;
import com.rgare.global.finre.service.RiskCriteriaService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class RiskCriteriaServiceImplUnitTest {

  private static final Long ID = 1L;

  private RiskCriteriaService riskCriteriaService;

  @Mock
  private RiskCriteriaRepository mockRiskCriteriaRepository;

  @Mock
  private RiskCriteriaSearchRepository mockRiskCriteriaSearchRepository;

  private List<RiskCriteria> expectedResult;
  private RiskCriteria riskCriteria;

  @Before
  public void setup() {
    riskCriteriaService = new RiskCriteriaServiceImpl(mockRiskCriteriaRepository, mockRiskCriteriaSearchRepository);
    expectedResult = new ArrayList<RiskCriteria>();
    riskCriteria = new RiskCriteria();
    riskCriteria.setId(ID);
    riskCriteria.setDescription("description");
    riskCriteria.setName("name");
    expectedResult.add(riskCriteria);
  }

  @Test
  public void save() {
    RiskCriteria savedRiskCriteria = new RiskCriteria();
    savedRiskCriteria.setName("saved risk criteria");
    when(mockRiskCriteriaRepository.save(riskCriteria)).thenReturn(savedRiskCriteria);

    riskCriteriaService.save(riskCriteria);

    verify(mockRiskCriteriaSearchRepository, times(1)).save(savedRiskCriteria);
  }

  @Test
  public void findAll() {
    when(mockRiskCriteriaRepository.findAll()).thenReturn(expectedResult);

    List<RiskCriteria> actual = riskCriteriaService.findAll();

    assertThat(actual).isEqualTo(expectedResult);
    verifyZeroInteractions(mockRiskCriteriaSearchRepository);
  }

  @Test
  public void findOne() {
    when(mockRiskCriteriaRepository.findOne(ID)).thenReturn(riskCriteria);

    RiskCriteria actual = riskCriteriaService.findOne(ID);

    assertThat(actual).isEqualTo(riskCriteria);
    verifyZeroInteractions(mockRiskCriteriaSearchRepository);
  }

  @Test
  public void delete() {
    riskCriteriaService.delete(ID);

    verify(mockRiskCriteriaRepository, times(1)).delete(ID);
    verify(mockRiskCriteriaSearchRepository, times(1)).delete(ID);
  }

  @Test
  public void search() {
    String query = "x=1";
    ArgumentCaptor<QueryBuilder> argumentCaptor = ArgumentCaptor.forClass(QueryBuilder.class);

    when(mockRiskCriteriaSearchRepository.search(argumentCaptor.capture())).thenReturn(expectedResult);

    List<RiskCriteria> actual = riskCriteriaService.search(query);

    assertThat(actual).isEqualTo(expectedResult);
    assertThat(argumentCaptor.getValue().toString()).contains(query);
    verifyZeroInteractions(mockRiskCriteriaRepository);
  }
}
