package com.rgare.global.finre.service.dto;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.multipart.MultipartFile;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class UploadDTOUnitTest {

  private static final Long ID = 1L;

  private MultipartFile MULTIPART_FILE;
  private static final String EXTRA = "EXTRA";
  private static final String FILE = "FILE";


  private UploadDTO uploadDTO;

  String expectedResult = "UserDTO{" +
    "extraField='" + EXTRA + '\'' +
    ", file='" +   null + '\'' +
    "}";

  @Before
  public void setup() {
    uploadDTO = new UploadDTO();
    uploadDTO.setExtraField(EXTRA);
    uploadDTO.setFile(null);
  }

  @Test
  public void equalsGetTest() {
    String extra = uploadDTO.getExtraField();
    MultipartFile file = uploadDTO.getFile();
    assertThat(extra).isEqualTo(EXTRA);
    assertThat(file).isNull();
  }

  @Test
  public void overrideEqualsGetTest() {
    String result = uploadDTO.toString();
    assertThat(result).isEqualTo(expectedResult);
  }

}
