package com.rgare.global.finre.cucumber.stepdefs;

import com.rgare.global.finre.FinreApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = FinreApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
