package com.rgare.global.finre.security;

import com.google.common.collect.Sets;
import com.rgare.global.finre.domain.Authority;
import com.rgare.global.finre.domain.User;
import com.rgare.global.finre.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserDetailsLoaderUnitTest {

  private static final String PRINCIP_RGARE_NET = "princip@RGARE.NET";
  @Mock
  private UserService mockUserService;

  private UserDetailsLoader userDetailsLoader;

  private User user;

  @Before
  public void setUp() throws Exception {
    userDetailsLoader = new UserDetailsLoader();
    userDetailsLoader.setUserService(mockUserService);
    user = new User();
    user.setId(42l);
    user.setLogin(PRINCIP_RGARE_NET);
    Authority authority = new Authority();
    authority.setName("ADMIN");
    user.setAuthorities(Sets.newHashSet(authority));
  }

  @Test
  public void loadUserDetails_notFound() {
    when(mockUserService.getUserWithAuthoritiesByLogin(PRINCIP_RGARE_NET)).thenReturn(Optional.empty());
    PreAuthenticatedAuthenticationToken token = new PreAuthenticatedAuthenticationToken("princip", "creds");

    UserDetails userDetails = userDetailsLoader.loadUserDetails(token);

    assertThat(userDetails.getUsername()).isEqualTo("princip");
    assertThat(userDetails.getPassword()).isEqualTo("");
    assertThat(userDetails.getAuthorities().size()).isEqualTo(0);
  }

  @Test
  public void loadUserDetails_found() {
    when(mockUserService.getUserWithAuthoritiesByLogin(PRINCIP_RGARE_NET)).thenReturn(Optional.of(user));
    PreAuthenticatedAuthenticationToken token = new PreAuthenticatedAuthenticationToken("princip", "creds");

    UserDetails userDetails = userDetailsLoader.loadUserDetails(token);

    assertThat(userDetails.getUsername()).isEqualTo("princip");
    assertThat(userDetails.getPassword()).isEqualTo("");
    assertThat(userDetails.getAuthorities().size()).isEqualTo(1);
    assertThat(userDetails.getAuthorities().toArray()[0].toString()).isEqualTo("ADMIN");
  }

}
