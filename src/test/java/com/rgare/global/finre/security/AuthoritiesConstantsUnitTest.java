package com.rgare.global.finre.security;

import com.rgare.global.finre.config.Constants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class AuthoritiesConstantsUnitTest {

  @Test
  public void createConstantsTest(){

    String ADMIN = "ROLE_APP_FINRE_OPS_ADMIN";
    String ANONYMOUS_USER = "ROLE_ANONYMOUS";
    AuthoritiesConstants constants = new AuthoritiesConstants();
    assertThat(constants).isNotNull();

    assertThat(AuthoritiesConstants.ADMIN).isEqualTo(ADMIN);
    assertThat(AuthoritiesConstants.ANONYMOUS).isEqualTo(ANONYMOUS_USER);

  }
}
