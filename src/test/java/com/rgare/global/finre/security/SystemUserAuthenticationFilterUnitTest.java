package com.rgare.global.finre.security;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class SystemUserAuthenticationFilterUnitTest {

  private SystemUserAuthenticationFilter filter = new SystemUserAuthenticationFilter();

  @Test
  public void getPreAuthenticatedPrincipal() {
    String expectedUserName = System.getProperty("user.name");

    Object actual = filter.getPreAuthenticatedPrincipal(null);

    assertThat((String) actual).isEqualTo(expectedUserName);
  }

  @Test
  public void getPreAuthenticatedCredentials() {
    Object actual = filter.getPreAuthenticatedCredentials(null);

    assertThat((String) actual).isEqualTo("N/A");
  }

}
