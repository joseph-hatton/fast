package com.rgare.global.finre.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ConstantsUnitTest {

  @Test
  public void createConstantsTest(){

    String SYSTEM_ACCOUNT = "system";
    String ANONYMOUS_USER = "anonymoususer";
    Constants constants = new Constants();
    assertThat(constants).isNotNull();

    assertThat(Constants.SYSTEM_ACCOUNT).isEqualTo(SYSTEM_ACCOUNT);
    assertThat(Constants.ANONYMOUS_USER).isEqualTo(ANONYMOUS_USER);

  }
}
