package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.FundingType;
import com.rgare.global.finre.repository.FundingTypeRepository;
import com.rgare.global.finre.repository.search.FundingTypeSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FundingTypeResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private FundingTypeRepository mockFundingTypeRepository;

  @Mock
  private FundingTypeSearchRepository mockFundingTypeSearchRepository;

  private FundingType fundingType;

  private FundingType result;

  private FundingTypeResource resource;

  private Pageable pageable;
  private PageImpl<FundingType> trackingPage;

  private List<FundingType> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new FundingTypeResource(mockFundingTypeRepository, mockFundingTypeSearchRepository);
    fundingType = new FundingType();
    fundingType.setName(NAME);
    fundingType.setDescription(DESCRIPTION);

    result = new FundingType();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);


    list = Arrays.asList(fundingType);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    FundingType savedFundingType = new FundingType();
    savedFundingType = new FundingType();
    savedFundingType.setName(NAME);
    savedFundingType.setDescription(DESCRIPTION);

    when(mockFundingTypeRepository.save(savedFundingType)).thenReturn(result);

    ResponseEntity<FundingType> responseEntity = resource.createFundingType(savedFundingType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    FundingType savedFundingType = new FundingType();
    savedFundingType = new FundingType();
    savedFundingType.setId(ID);
    savedFundingType.setName(NAME);
    savedFundingType.setDescription(DESCRIPTION);

    ResponseEntity<FundingType> responseEntity = resource.createFundingType(savedFundingType);

    verifyZeroInteractions(mockFundingTypeRepository, mockFundingTypeSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    FundingType savedFundingType = new FundingType();
    savedFundingType = new FundingType();
    savedFundingType.setId(ID);
    savedFundingType.setName(NAME);
    savedFundingType.setDescription(DESCRIPTION);

    when(mockFundingTypeRepository.save(savedFundingType)).thenReturn(result);

    ResponseEntity<FundingType> responseEntity = resource.updateFundingType(savedFundingType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    FundingType savedFundingType = new FundingType();
    savedFundingType = new FundingType();
    savedFundingType.setId(null);
    savedFundingType.setName(NAME);
    savedFundingType.setDescription(DESCRIPTION);

    when(mockFundingTypeRepository.save(savedFundingType)).thenReturn(result);

    ResponseEntity<FundingType> responseEntity = resource.updateFundingType(savedFundingType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockFundingTypeRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<FundingType> responseEntity = resource.getFundingType(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockFundingTypeRepository.findAll()).thenReturn(list);

    List<FundingType> fundingTypes = resource.getAllFundingTypes();

    assertThat(fundingTypes).isNotNull();
    assertThat(fundingTypes.size()).isGreaterThan(0);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteFundingType(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockFundingTypeRepository).delete(ID);
    verify(mockFundingTypeSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockFundingTypeSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<FundingType> response = resource.searchFundingTypes(query);

    assertThat(response).isNotNull();
  }



}
