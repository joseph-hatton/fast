package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.Deal;
import com.rgare.global.finre.repository.DealRepository;
import com.rgare.global.finre.repository.search.DealSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DealResourceUnitTest {

  private static final Long ID = 100L;
  private static final String STATUS = "STATUS";
  private static final String COMMENTS = "COMMENTS";

  @Mock
  private DealRepository mockDealRepository;

  @Mock
  private DealSearchRepository mockDealSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private Deal deal;

  private Deal result;

  private DealResource resource;

  private Pageable pageable;
  private PageImpl<Deal> trackingPage;

  private List<Deal> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new DealResource(mockDealRepository, mockDealSearchRepository);
    deal = new Deal();
    deal.setDealStatus(STATUS);
    deal.setComments(COMMENTS);

    result = new Deal();
    result.setId(ID);
    result.setDealStatus(STATUS);
    result.setComments(COMMENTS);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(deal);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    Deal savedDeal = new Deal();
    savedDeal = new Deal();
    savedDeal.setDealStatus(STATUS);
    savedDeal.setComments(COMMENTS);

    when(mockDealRepository.save(savedDeal)).thenReturn(result);

    ResponseEntity<Deal> responseEntity = resource.createDeal(savedDeal);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    Deal savedDeal = new Deal();
    savedDeal = new Deal();
    savedDeal.setId(ID);
    savedDeal.setDealStatus(STATUS);
    savedDeal.setComments(COMMENTS);

    ResponseEntity<Deal> responseEntity = resource.createDeal(savedDeal);

    verifyZeroInteractions(mockDealRepository, mockDealSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    Deal savedDeal = new Deal();
    savedDeal = new Deal();
    savedDeal.setId(ID);
    savedDeal.setDealStatus(STATUS);
    savedDeal.setComments(COMMENTS);

    when(mockDealRepository.save(savedDeal)).thenReturn(result);

    ResponseEntity<Deal> responseEntity = resource.updateDeal(savedDeal);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhenIDIsNullSuccessful() throws URISyntaxException {
    Deal savedDeal = new Deal();
    savedDeal = new Deal();
    savedDeal.setId(null);
    savedDeal.setDealStatus(STATUS);
    savedDeal.setComments(COMMENTS);

    when(mockDealRepository.save(savedDeal)).thenReturn(result);

    ResponseEntity<Deal> responseEntity = resource.updateDeal(savedDeal);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockDealRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<Deal> responseEntity = resource.getDeal(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllDealsSuccessful() throws URISyntaxException {

    when(mockDealRepository.findDealsByIsActive(1, pageable)).thenReturn(trackingPage);

    ResponseEntity<List<Deal>> responseEntity = resource.getAllDeals(pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllDealsNoPageSuccessful() throws URISyntaxException {

    when(mockDealRepository.findDealsByIsActive(1)).thenReturn(list);

    ResponseEntity<List<Deal>> responseEntity = resource.getAllDealsNoPages();

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteDeal(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockDealRepository).delete(ID);
    verify(mockDealSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatys_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockDealSearchRepository.search(any(QueryBuilder.class), eq(pageable))).thenReturn(trackingPage);

    ResponseEntity<List<Deal>> response = resource.searchDeals(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }

  @Test
  public void searchDeals_existingDealssuccessful() throws URISyntaxException {
    String query = "a=b";
    when(mockDealRepository.searchExistingDealQuery(query, pageable)).thenReturn(trackingPage);

    ResponseEntity<List<Deal>> response = resource.searchExistingDeals(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }

  @Test
  public void searchDeals_exactDealsSearchSuccessful() throws URISyntaxException {
    String query = "a=b";
    when(mockDealRepository.findAllByNameIgnoreCaseAndIsActiveAndDealStatus(query,1,"Active")).thenReturn(list);
    List<Deal> response = resource.searchExactActiveExistingDeals(query);
    assertThat(response).isNotNull();
  }


}
