package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.XrefTreatyContact;
import com.rgare.global.finre.repository.XrefTreatyContactRepository;
import com.rgare.global.finre.repository.search.XrefTreatyContactSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class XrefTreatyContactResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private XrefTreatyContactRepository mockXrefTreatyContactRepository;

  @Mock
  private XrefTreatyContactSearchRepository mockXrefTreatyContactSearchRepository;

  private XrefTreatyContact xrefTreatyContact;

  private XrefTreatyContact result;

  private XrefTreatyContactResource resource;

  private Pageable pageable;
  private PageImpl<XrefTreatyContact> trackingPage;

  private List<XrefTreatyContact> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new XrefTreatyContactResource(mockXrefTreatyContactRepository, mockXrefTreatyContactSearchRepository);
    xrefTreatyContact = new XrefTreatyContact();
    xrefTreatyContact.setLastUser(NAME);
    xrefTreatyContact.setTreatyId(2L);

    result = new XrefTreatyContact();
    result.setId(ID);
    result.setLastUser(NAME);
    result.setTreatyId(2L);


    list = Arrays.asList(xrefTreatyContact);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    XrefTreatyContact savedXrefTreatyContact = new XrefTreatyContact();
    savedXrefTreatyContact = new XrefTreatyContact();
    savedXrefTreatyContact.setLastUser(NAME);
    savedXrefTreatyContact.setTreatyId(2L);

    when(mockXrefTreatyContactRepository.save(savedXrefTreatyContact)).thenReturn(result);

    ResponseEntity<XrefTreatyContact> responseEntity = resource.createXrefTreatyContact(savedXrefTreatyContact);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    XrefTreatyContact savedXrefTreatyContact = new XrefTreatyContact();
    savedXrefTreatyContact = new XrefTreatyContact();
    savedXrefTreatyContact.setId(ID);
    savedXrefTreatyContact.setLastUser(NAME);
    savedXrefTreatyContact.setTreatyId(2L);

    ResponseEntity<XrefTreatyContact> responseEntity = resource.createXrefTreatyContact(savedXrefTreatyContact);

    verifyZeroInteractions(mockXrefTreatyContactRepository, mockXrefTreatyContactSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    XrefTreatyContact savedXrefTreatyContact = new XrefTreatyContact();
    savedXrefTreatyContact = new XrefTreatyContact();
    savedXrefTreatyContact.setId(ID);
    savedXrefTreatyContact.setLastUser(NAME);
    savedXrefTreatyContact.setTreatyId(2L);

    when(mockXrefTreatyContactRepository.save(savedXrefTreatyContact)).thenReturn(result);

    ResponseEntity<XrefTreatyContact> responseEntity = resource.updateXrefTreatyContact(savedXrefTreatyContact);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    XrefTreatyContact savedXrefTreatyContact = new XrefTreatyContact();
    savedXrefTreatyContact = new XrefTreatyContact();
    savedXrefTreatyContact.setId(null);
    savedXrefTreatyContact.setLastUser(NAME);
    savedXrefTreatyContact.setTreatyId(2L);

    when(mockXrefTreatyContactRepository.save(savedXrefTreatyContact)).thenReturn(result);

    ResponseEntity<XrefTreatyContact> responseEntity = resource.updateXrefTreatyContact(savedXrefTreatyContact);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockXrefTreatyContactRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<XrefTreatyContact> responseEntity = resource.getXrefTreatyContact(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockXrefTreatyContactRepository.findAll()).thenReturn(list);

    List<XrefTreatyContact> allXrefTreatyContacts = resource.getAllXrefTreatyContacts();

    assertThat(allXrefTreatyContacts).isNotNull();
  }

  @Test
  public void getAllData_retreiveAllByTreatyIdSuccessful() throws URISyntaxException {

    when(mockXrefTreatyContactRepository.findAllByTreatyId(ID)).thenReturn(list);

    ResponseEntity<List<XrefTreatyContact>> allXrefTreatyContacts = resource.getAllTreatiesByTreatyId(ID);

    assertThat(allXrefTreatyContacts).isNotNull();
    assertThat(allXrefTreatyContacts.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllByContactIdSuccessful() throws URISyntaxException {

    when(mockXrefTreatyContactRepository.findAllByContactId(ID)).thenReturn(list);

    ResponseEntity<List<XrefTreatyContact>> allXrefTreatyContacts = resource.getAllByContactId(ID);

    assertThat(allXrefTreatyContacts).isNotNull();
    assertThat(allXrefTreatyContacts.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteXrefTreatyContact(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockXrefTreatyContactRepository).delete(ID);
    verify(mockXrefTreatyContactSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockXrefTreatyContactSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<XrefTreatyContact> response = resource.searchXrefTreatyContacts(query);

    assertThat(response).isNotNull();
  }



}
