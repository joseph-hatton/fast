package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.Country;
import com.rgare.global.finre.repository.CountryRepository;
import com.rgare.global.finre.repository.search.CountrySearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CountryResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String REGION = "REGION";

  @Mock
  private CountryRepository mockCountryRepository;

  @Mock
  private CountrySearchRepository mockCountrySearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private Country country;

  private Country result;

  private CountryResource resource;

  private Pageable pageable;
  private PageImpl<Country> trackingPage;

  private List<Country> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new CountryResource(mockCountryRepository, mockCountrySearchRepository);
    country = new Country();
    country.setName(NAME);
    country.setRegion(REGION);

    result = new Country();
    result.setId(ID);
    result.setName(NAME);
    result.setRegion(REGION);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(country);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    Country savedCountry = new Country();
    savedCountry = new Country();
    savedCountry.setName(NAME);
    savedCountry.setRegion(REGION);

    when(mockCountryRepository.save(savedCountry)).thenReturn(result);

    ResponseEntity<Country> responseEntity = resource.createCountry(savedCountry);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    Country savedCountry = new Country();
    savedCountry = new Country();
    savedCountry.setId(ID);
    savedCountry.setName(NAME);
    savedCountry.setRegion(REGION);

    ResponseEntity<Country> responseEntity = resource.createCountry(savedCountry);

    verifyZeroInteractions(mockCountryRepository, mockCountrySearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    Country savedCountry = new Country();
    savedCountry = new Country();
    savedCountry.setId(ID);
    savedCountry.setName(NAME);
    savedCountry.setRegion(REGION);

    when(mockCountryRepository.save(savedCountry)).thenReturn(result);

    ResponseEntity<Country> responseEntity = resource.updateCountry(savedCountry);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    Country savedCountry = new Country();
    savedCountry = new Country();
    savedCountry.setId(null);
    savedCountry.setName(NAME);
    savedCountry.setRegion(REGION);

    when(mockCountryRepository.save(savedCountry)).thenReturn(result);

    ResponseEntity<Country> responseEntity = resource.updateCountry(savedCountry);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockCountryRepository.findAll()).thenReturn(list);

    List<Country> allCountries = resource.getAllCountries();

    assertThat(allCountries).isNotNull();
    assertThat(allCountries.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockCountryRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<Country> responseEntity = resource.getCountry(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteCountry(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockCountryRepository).delete(ID);
    verify(mockCountrySearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockCountrySearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<Country> response = resource.searchCountries(query);

    assertThat(response).isNotNull();
  }



}
