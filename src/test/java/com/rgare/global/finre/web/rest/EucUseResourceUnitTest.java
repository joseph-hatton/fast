package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.EucUse;
import com.rgare.global.finre.repository.EucUseRepository;
import com.rgare.global.finre.repository.search.EucUseSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EucUseResourceUnitTest {

  private static final Long ID = 100L;
  private static final String TREATY_COMMENTS = "TREATY_COMMENTS";
  private static final String COMMENTS = "COMMENTS";

  @Mock
  private EucUseRepository mockEucUseRepository;

  @Mock
  private EucUseSearchRepository mockEucUseSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private EucUse eucUse;

  private EucUse result;

  private EucUseResource resource;

  private Pageable pageable;
  private PageImpl<EucUse> trackingPage;

  private List<EucUse> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new EucUseResource(mockEucUseRepository, mockEucUseSearchRepository);
    eucUse = new EucUse();
    eucUse.setName(TREATY_COMMENTS);
    eucUse.setDescription(COMMENTS);

    result = new EucUse();
    result.setId(ID);
    result.setName(TREATY_COMMENTS);
    result.setDescription(COMMENTS);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(eucUse);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    EucUse savedEucUse = new EucUse();
    savedEucUse = new EucUse();
    savedEucUse.setName(TREATY_COMMENTS);
    savedEucUse.setDescription(COMMENTS);

    when(mockEucUseRepository.save(savedEucUse)).thenReturn(result);

    ResponseEntity<EucUse> responseEntity = resource.createEucUse(savedEucUse);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    EucUse savedEucUse = new EucUse();
    savedEucUse = new EucUse();
    savedEucUse.setId(ID);
    savedEucUse.setName(TREATY_COMMENTS);
    savedEucUse.setDescription(COMMENTS);

    ResponseEntity<EucUse> responseEntity = resource.createEucUse(savedEucUse);

    verifyZeroInteractions(mockEucUseRepository, mockEucUseSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    EucUse savedEucUse = new EucUse();
    savedEucUse = new EucUse();
    savedEucUse.setId(ID);
    savedEucUse.setName(TREATY_COMMENTS);
    savedEucUse.setDescription(COMMENTS);

    when(mockEucUseRepository.save(savedEucUse)).thenReturn(result);

    ResponseEntity<EucUse> responseEntity = resource.updateEucUse(savedEucUse);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }


  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    EucUse savedEucUse = new EucUse();
    savedEucUse = new EucUse();
    savedEucUse.setId(null);
    savedEucUse.setName(TREATY_COMMENTS);
    savedEucUse.setDescription(COMMENTS);

    when(mockEucUseRepository.save(savedEucUse)).thenReturn(result);

    ResponseEntity<EucUse> responseEntity = resource.updateEucUse(savedEucUse);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockEucUseRepository.findAll()).thenReturn(list);

    List<EucUse> eucUses = resource.getAllEucUses();

    assertThat(eucUses).isNotNull();
    assertThat(eucUses.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockEucUseRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<EucUse> responseEntity = resource.getEucUse(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteEucUse(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockEucUseRepository).delete(ID);
    verify(mockEucUseSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockEucUseSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<EucUse> response = resource.searchEucUses(query);

    assertThat(response).isNotNull();
  }



}
