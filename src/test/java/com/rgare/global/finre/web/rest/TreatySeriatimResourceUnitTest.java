package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.TreatySeriatim;
import com.rgare.global.finre.repository.TreatySeriatimRepository;
import com.rgare.global.finre.repository.search.TreatySeriatimSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TreatySeriatimResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private TreatySeriatimRepository mockTreatySeriatimRepository;

  @Mock
  private TreatySeriatimSearchRepository mockTreatySeriatimSearchRepository;

  private TreatySeriatim treatySeriatim;

  private TreatySeriatim result;

  private TreatySeriatimResource resource;

  private Pageable pageable;
  private PageImpl<TreatySeriatim> trackingPage;

  private List<TreatySeriatim> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new TreatySeriatimResource(mockTreatySeriatimRepository, mockTreatySeriatimSearchRepository);
    treatySeriatim = new TreatySeriatim();
    treatySeriatim.setLastUser(NAME);
    treatySeriatim.setCreationUser(DESCRIPTION);

    result = new TreatySeriatim();
    result.setId(ID);
    result.setLastUser(NAME);
    result.setCreationUser(DESCRIPTION);


    list = Arrays.asList(treatySeriatim);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    TreatySeriatim savedTreatySeriatim = new TreatySeriatim();
    savedTreatySeriatim = new TreatySeriatim();
    savedTreatySeriatim.setLastUser(NAME);
    savedTreatySeriatim.setCreationUser(DESCRIPTION);

    when(mockTreatySeriatimRepository.save(savedTreatySeriatim)).thenReturn(result);

    ResponseEntity<TreatySeriatim> responseEntity = resource.createTreatySeriatim(savedTreatySeriatim);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    TreatySeriatim savedTreatySeriatim = new TreatySeriatim();
    savedTreatySeriatim = new TreatySeriatim();
    savedTreatySeriatim.setId(ID);
    savedTreatySeriatim.setLastUser(NAME);
    savedTreatySeriatim.setCreationUser(DESCRIPTION);

    ResponseEntity<TreatySeriatim> responseEntity = resource.createTreatySeriatim(savedTreatySeriatim);

    verifyZeroInteractions(mockTreatySeriatimRepository, mockTreatySeriatimSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    TreatySeriatim savedTreatySeriatim = new TreatySeriatim();
    savedTreatySeriatim = new TreatySeriatim();
    savedTreatySeriatim.setId(ID);
    savedTreatySeriatim.setLastUser(NAME);
    savedTreatySeriatim.setCreationUser(DESCRIPTION);

    when(mockTreatySeriatimRepository.save(savedTreatySeriatim)).thenReturn(result);

    ResponseEntity<TreatySeriatim> responseEntity = resource.updateTreatySeriatim(savedTreatySeriatim);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    TreatySeriatim savedTreatySeriatim = new TreatySeriatim();
    savedTreatySeriatim = new TreatySeriatim();
    savedTreatySeriatim.setId(null);
    savedTreatySeriatim.setLastUser(NAME);
    savedTreatySeriatim.setCreationUser(DESCRIPTION);

    when(mockTreatySeriatimRepository.save(savedTreatySeriatim)).thenReturn(result);

    ResponseEntity<TreatySeriatim> responseEntity = resource.updateTreatySeriatim(savedTreatySeriatim);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockTreatySeriatimRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<TreatySeriatim> responseEntity = resource.getTreatySeriatim(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockTreatySeriatimRepository.findAll(pageable)).thenReturn(trackingPage);

    ResponseEntity<List<TreatySeriatim>> responseEntity = resource.getAllTreatySeriatims(pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllByTreatySuccessful() throws URISyntaxException {

    when(mockTreatySeriatimRepository.findAllByTreatyId(ID,pageable)).thenReturn(trackingPage);

    ResponseEntity<List<TreatySeriatim>> responseEntity = resource.getAllByTreatyId(ID,pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteTreatySeriatim(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockTreatySeriatimRepository).delete(ID);
    verify(mockTreatySeriatimSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockTreatySeriatimSearchRepository.search(any(QueryBuilder.class),eq(pageable))).thenReturn(trackingPage);

    ResponseEntity<List<TreatySeriatim>> response = resource.searchTreatySeriatims(query, pageable);

    assertThat(response).isNotNull();
  }



}
