package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.StructuredTracking;
import com.rgare.global.finre.repository.StructuredTrackingRepository;
import com.rgare.global.finre.repository.search.StructuredTrackingSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StructuredTrackingResourceUnitTest {

  private static final Long ID = 100L;
  private static final String STATUS = "STATUS";
  private static final String COMMENT = "COMMENT";

  @Mock
  private StructuredTrackingRepository mockStructuredTrackingRepository;

  @Mock
  private StructuredTrackingSearchRepository mockStructuredTrackingSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private StructuredTracking structuredTracking;

  private StructuredTracking result;

  private StructuredTrackingResource resource;

  private Pageable pageable;
  private PageImpl<StructuredTracking> trackingPage;

  private List<StructuredTracking> list;

  private ArgumentCaptor<LocalDate> startCaptor;
  private ArgumentCaptor<LocalDate> endCaptor;

  @Before
  public void setup() throws URISyntaxException {
    resource = new StructuredTrackingResource(mockStructuredTrackingRepository, mockStructuredTrackingSearchRepository);
    structuredTracking = new StructuredTracking();
    structuredTracking.setTrackingStatus(STATUS);
    structuredTracking.setComments(COMMENT);

    result = new StructuredTracking();
    result.setId(ID);
    result.setTrackingStatus(STATUS);
    result.setComments(COMMENT);

    pageable = new PageRequest(1, 5);

    startCaptor = ArgumentCaptor.forClass(LocalDate.class);
    endCaptor = ArgumentCaptor.forClass(LocalDate.class);


    list = Arrays.asList(structuredTracking);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    StructuredTracking savedStructuredTracking = new StructuredTracking();
    savedStructuredTracking = new StructuredTracking();
    savedStructuredTracking.setTrackingStatus(STATUS);
    savedStructuredTracking.setComments(COMMENT);

    when(mockStructuredTrackingRepository.save(savedStructuredTracking)).thenReturn(result);

    ResponseEntity<StructuredTracking> responseEntity = resource.createStructuredTracking(savedStructuredTracking);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    StructuredTracking savedStructuredTracking = new StructuredTracking();
    savedStructuredTracking = new StructuredTracking();
    savedStructuredTracking.setId(ID);
    savedStructuredTracking.setTrackingStatus(STATUS);
    savedStructuredTracking.setComments(COMMENT);

    ResponseEntity<StructuredTracking> responseEntity = resource.createStructuredTracking(savedStructuredTracking);

    verifyZeroInteractions(mockStructuredTrackingRepository, mockStructuredTrackingSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    StructuredTracking savedStructuredTracking = new StructuredTracking();
    savedStructuredTracking = new StructuredTracking();
    savedStructuredTracking.setId(ID);
    savedStructuredTracking.setTrackingStatus(STATUS);
    savedStructuredTracking.setComments(COMMENT);

    when(mockStructuredTrackingRepository.save(savedStructuredTracking)).thenReturn(result);

    ResponseEntity<StructuredTracking> responseEntity = resource.updateStructuredTracking(savedStructuredTracking);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhenIDIsNullSuccessful() throws URISyntaxException {
    StructuredTracking savedStructuredTracking = new StructuredTracking();
    savedStructuredTracking = new StructuredTracking();
    savedStructuredTracking.setId(null);
    savedStructuredTracking.setTrackingStatus(STATUS);
    savedStructuredTracking.setComments(COMMENT);

    when(mockStructuredTrackingRepository.save(savedStructuredTracking)).thenReturn(result);

    ResponseEntity<StructuredTracking> responseEntity = resource.updateStructuredTracking(savedStructuredTracking);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockStructuredTrackingRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<StructuredTracking> responseEntity = resource.getStructuredTracking(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockStructuredTrackingRepository.findAll(pageable)).thenReturn(trackingPage);

    ResponseEntity<List<StructuredTracking>> responseEntity = resource.getAllStructuredTrackings(pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getByDealIdData_retreiveByDealSuccessful() throws URISyntaxException {

    when(mockStructuredTrackingRepository.findAllByDealIdAndIsActive(ID,pageable,1)).thenReturn(trackingPage);

    ResponseEntity<List<StructuredTracking>> responseEntity = resource.getStructuredTrackingByDealId(ID,pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getByFinanceIdData_retreiveByFinanceSuccessful() throws URISyntaxException {

    when(mockStructuredTrackingRepository.findAllByStructuredFinanceIdAndIsActive(ID,pageable,1)).thenReturn(trackingPage);

    ResponseEntity<List<StructuredTracking>> responseEntity = resource.getStructuredTrackingByDealIdAndFinanceID(ID,pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getByDatesData_retreiveByDatesSuccessful() throws URISyntaxException {
    when(mockStructuredTrackingRepository.findAllByStructuredFinanceIdAndClientReportingDateBetweenAndIsActive(ID, LocalDate.of(2017,2,3),
      LocalDate.of(2017,5,6), pageable,1)).thenReturn(trackingPage);

    ResponseEntity<List<StructuredTracking>> responseEntity = resource.getStructuredTrackingByDate(ID,2017,2,3,
      2017,5,6,pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteStructuredTracking(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockStructuredTrackingRepository).delete(ID);
    verify(mockStructuredTrackingSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockStructuredTrackingSearchRepository.search(any(QueryBuilder.class), eq(pageable))).thenReturn(trackingPage);

    ResponseEntity<List<StructuredTracking>> response = resource.searchStructuredTrackings(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }



}
