package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.RiskCriteria;
import com.rgare.global.finre.repository.RiskCriteriaRepository;
import com.rgare.global.finre.repository.search.RiskCriteriaSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import com.rgare.global.finre.service.RiskCriteriaService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RiskCriteriaResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private RiskCriteriaRepository mockRiskCriteriaRepository;

  @Mock
  private RiskCriteriaSearchRepository mockRiskCriteriaSearchRepository;

  @Mock
  private RiskCriteriaService mockRiskCriteriaService;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private RiskCriteria riskCriteria;

  private RiskCriteria result;

  private RiskCriteriaResource resource;

  private Pageable pageable;
  private PageImpl<RiskCriteria> trackingPage;

  private List<RiskCriteria> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new RiskCriteriaResource(mockRiskCriteriaService);
    riskCriteria = new RiskCriteria();
    riskCriteria.setName(NAME);
    riskCriteria.setDescription(DESCRIPTION);

    result = new RiskCriteria();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(riskCriteria);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    RiskCriteria savedRiskCriteria = new RiskCriteria();
    savedRiskCriteria = new RiskCriteria();
    savedRiskCriteria.setName(NAME);
    savedRiskCriteria.setDescription(DESCRIPTION);

    when(mockRiskCriteriaService.save(savedRiskCriteria)).thenReturn(result);

    ResponseEntity<RiskCriteria> responseEntity = resource.createRiskCriteria(savedRiskCriteria);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    RiskCriteria savedRiskCriteria = new RiskCriteria();
    savedRiskCriteria = new RiskCriteria();
    savedRiskCriteria.setId(ID);
    savedRiskCriteria.setName(NAME);
    savedRiskCriteria.setDescription(DESCRIPTION);

    ResponseEntity<RiskCriteria> responseEntity = resource.createRiskCriteria(savedRiskCriteria);

    verifyZeroInteractions(mockRiskCriteriaService, mockRiskCriteriaSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    RiskCriteria savedRiskCriteria = new RiskCriteria();
    savedRiskCriteria = new RiskCriteria();
    savedRiskCriteria.setId(ID);
    savedRiskCriteria.setName(NAME);
    savedRiskCriteria.setDescription(DESCRIPTION);

    when(mockRiskCriteriaService.save(savedRiskCriteria)).thenReturn(result);

    ResponseEntity<RiskCriteria> responseEntity = resource.updateRiskCriteria(savedRiskCriteria);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    RiskCriteria savedRiskCriteria = new RiskCriteria();
    savedRiskCriteria = new RiskCriteria();
    savedRiskCriteria.setId(null);
    savedRiskCriteria.setName(NAME);
    savedRiskCriteria.setDescription(DESCRIPTION);

    when(mockRiskCriteriaService.save(savedRiskCriteria)).thenReturn(result);

    ResponseEntity<RiskCriteria> responseEntity = resource.updateRiskCriteria(savedRiskCriteria);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockRiskCriteriaService.findOne(ID)).thenReturn(result);

    ResponseEntity<RiskCriteria> responseEntity = resource.getRiskCriteria(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockRiskCriteriaService.findAll()).thenReturn(list);

    List<RiskCriteria> allRiskCriteria = resource.getAllRiskCriteria();

    assertThat(allRiskCriteria).isNotNull();
    assertThat(allRiskCriteria.size()).isGreaterThan(0);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteRiskCriteria(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verifyZeroInteractions(mockRiskCriteriaRepository);
    verifyZeroInteractions(mockRiskCriteriaSearchRepository);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockRiskCriteriaSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<RiskCriteria> response = resource.searchRiskCriteria(query);

    assertThat(response).isNotNull();
  }



}
