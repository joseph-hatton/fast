package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.*;
import com.rgare.global.finre.repository.AuditRiskManagementRepository;
import com.rgare.global.finre.repository.RiskManagementRepository;
import com.rgare.global.finre.repository.search.RiskManagementSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import com.rgare.global.finre.service.RiskManagementUploadService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RiskManagementUnitTest {

  private static final Long ID = 100L;
  private static final String UPDATE_ACTION = "UPDATE";
  private static final String MESSAGE = "MESSAGE";

  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";


  private static final String CREATE_USER = "CREATE_USER";
  private static final String LAST_USER = "LAST_USER";

  private static final String COMMENT = "COMMENT";

  private static final ZonedDateTime CREATED_TS = ZonedDateTime.now();
  private static final ZonedDateTime LAST_DATE = ZonedDateTime.now();

  @Mock
  private RiskManagementRepository mockRiskManagementRepository;

  @Mock
  private AuditRiskManagementRepository mockAuditRiskManagementRepository;

  @Mock
  private RiskManagementSearchRepository mockRiskManagementSearchRepository;

  @Mock
  private RiskManagementUploadService mockRiskManagementUploadService;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private RiskManagement riskManagement;

  private RiskManagement result;

  private RiskManagementResource resource;

  private Pageable pageable;
  private PageImpl<RiskManagement> trackingPage;

  private List<RiskManagement> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new RiskManagementResource(mockRiskManagementRepository, mockAuditRiskManagementRepository, mockRiskManagementSearchRepository,
      mockRiskManagementUploadService);
    riskManagement = new RiskManagement();
    riskManagement.setId(ID);
    riskManagement.setLastUpdate(LAST_DATE);
    riskManagement.setLastUser(LAST_USER);
    riskManagement.setCreationTs(CREATED_TS);
    riskManagement.setCreationUser(CREATE_USER);
    Deal deal = new Deal();
    deal.setId(ID);
    riskManagement.setDeal(deal);

    result = new RiskManagement();
    result.setId(ID);


    pageable = new PageRequest(1, 5);


    list = Arrays.asList(riskManagement);
    trackingPage = new PageImpl<>(list);

  }

  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    RiskManagement savedRiskManagement = new RiskManagement();
    savedRiskManagement = new RiskManagement();

    when(mockRiskManagementRepository.save(savedRiskManagement)).thenReturn(result);

    ResponseEntity<RiskManagement> responseEntity = resource.createRiskManagement(savedRiskManagement);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    RiskManagement savedRiskManagement = new RiskManagement();
    savedRiskManagement = new RiskManagement();
    savedRiskManagement.setId(ID);

    ResponseEntity<RiskManagement> responseEntity = resource.createRiskManagement(savedRiskManagement);

    verifyZeroInteractions(mockRiskManagementRepository, mockRiskManagementSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void getAllData_retrieveAllRisksSuccessful() throws Exception {
    RiskManagement savedRiskManagement = new RiskManagement();
    savedRiskManagement = new RiskManagement();

    when(mockRiskManagementRepository.findAll(pageable)).thenReturn(trackingPage);

    ResponseEntity<List<RiskManagement>> allRiskManagements = resource.getAllRiskManagements(pageable);

    verifyZeroInteractions(mockRiskManagementSearchRepository);
    assertThat(allRiskManagements).isNotNull();
    assertThat(allRiskManagements.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getOneData_retrieveOneRiskSuccessful() throws Exception {
    RiskManagement savedRiskManagement = new RiskManagement();
    savedRiskManagement = new RiskManagement();

    when(mockRiskManagementRepository.findOne(ID)).thenReturn(riskManagement);

    ResponseEntity<RiskManagement> allRiskManagements = resource.getRiskManagement(ID);

    verifyZeroInteractions(mockRiskManagementSearchRepository);
    assertThat(allRiskManagements).isNotNull();
    assertThat(allRiskManagements.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getData_retrieveAllRisksBetweenDatesSuccessful() throws Exception {
    RiskManagement savedRiskManagement = new RiskManagement();
    savedRiskManagement = new RiskManagement();

    when(mockRiskManagementRepository.findAllByDealIdAndMonthYear(ID,LocalDate.now())).thenReturn(list);

    List<RiskManagement> allRiskManagements = resource.getRiskManagements(ID, 2017,3,4);

    verifyZeroInteractions(mockRiskManagementSearchRepository);
    assertThat(allRiskManagements).isNotNull();
  }

  @Test
  public void getData_retrieveAllRiskSuccessful() throws Exception {
    RiskManagement savedRiskManagement = new RiskManagement();
    savedRiskManagement = new RiskManagement();

    when(mockRiskManagementRepository.findByMonthYear(LocalDate.of(2017,3,4))).thenReturn(list);

    List<RiskManagement> allRiskManagements = resource.getRiskManagement(2017,3,4);

    verifyZeroInteractions(mockRiskManagementSearchRepository);
    assertThat(allRiskManagements).isNotNull();
  }

  @Test
  public void getData_retrieveRiskByDateSuccessful() throws Exception {
    RiskManagement savedRiskManagement = new RiskManagement();
    savedRiskManagement = new RiskManagement();

    when(mockRiskManagementRepository.findByMonthYear(LocalDate.of(2017,3,4))).thenReturn(list);

    List<RiskManagement> allRiskManagements = resource.getRiskManagements(ID, 2017,3,4);

    verifyZeroInteractions(mockRiskManagementSearchRepository);
    assertThat(allRiskManagements).isNotNull();
  }

  @Test
  public void getData_uploadRiskSuccessful() throws Exception {
    RiskManagement savedRiskManagement = new RiskManagement();
    savedRiskManagement = new RiskManagement();

    when(mockRiskManagementUploadService.uploadRiskManagement(savedRiskManagement)).thenReturn(MESSAGE);

    RiskManagement management = resource.uploadRiskManagement(savedRiskManagement);

    verifyZeroInteractions(mockRiskManagementSearchRepository);
    assertThat(management).isNotNull();
  }

  @Test
  public void updateData_updateRiskSuccessful() throws URISyntaxException {
    RiskManagement savedRiskManagement = new RiskManagement();
    savedRiskManagement = new RiskManagement();
    savedRiskManagement.setId(ID);
    Deal deal = new Deal();
    deal.setId(ID);
    savedRiskManagement.setDeal(deal);
    RiskCriteria riskCriteria = new RiskCriteria();
    riskCriteria.setName(NAME);
    savedRiskManagement.setRiskCriteria(riskCriteria);
    savedRiskManagement.setAmount(new BigDecimal(100.1));
    savedRiskManagement.setConvertedAmount(new BigDecimal(200.1));
    savedRiskManagement.setMonthYear(LocalDate.now());
    Currency currency = new Currency();
    currency.setCode("USD");
    savedRiskManagement.setCurrency(currency);

    AuditRiskManagement auditRiskManagementObject =new AuditRiskManagement();
    auditRiskManagementObject.setCreationUser(CREATE_USER);
    auditRiskManagementObject.setLastUpdate(LAST_DATE);
    auditRiskManagementObject.setCreationTs(CREATED_TS);
    auditRiskManagementObject.setLastUser(LAST_USER);
    auditRiskManagementObject.setRiskManagementId(ID);
    auditRiskManagementObject.setDealId(ID);
    auditRiskManagementObject.setAction(UPDATE_ACTION);
    auditRiskManagementObject.setComments(COMMENT);

    when(mockRiskManagementRepository.save(savedRiskManagement)).thenReturn(riskManagement);
    when(mockAuditRiskManagementRepository.save(auditRiskManagementObject)).thenReturn(auditRiskManagementObject);

    ResponseEntity<RiskManagement> responseEntity = resource.updateRiskManagement(savedRiskManagement);

    verifyZeroInteractions(mockRiskManagementSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateRiskWhereIDIsNullSuccessful() throws URISyntaxException {
    RiskManagement savedRiskManagement = new RiskManagement();
    savedRiskManagement = new RiskManagement();
    savedRiskManagement.setId(null);

    when(mockRiskManagementRepository.save(savedRiskManagement)).thenReturn(riskManagement);

    ResponseEntity<RiskManagement> responseEntity = resource.updateRiskManagement(savedRiskManagement);

    verifyZeroInteractions(mockRiskManagementSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void deleteData_deleteRiskSuccessful() throws URISyntaxException {
    RiskManagement savedRiskManagement = new RiskManagement();
    savedRiskManagement = new RiskManagement();
    savedRiskManagement.setId(null);

    when(mockRiskManagementRepository.getOne(ID)).thenReturn(riskManagement);

    ResponseEntity<Void> responseEntity = resource.deleteRiskManagement(ID);

    verifyZeroInteractions(mockRiskManagementSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchRiskManagement_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockRiskManagementSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<RiskManagement> response = resource.searchRiskManagements(query, pageable);

    assertThat(response).isNotNull();
  }

}
