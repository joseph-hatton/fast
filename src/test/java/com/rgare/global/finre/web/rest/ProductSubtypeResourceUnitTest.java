package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.ProductSubtype;
import com.rgare.global.finre.repository.ProductSubtypeRepository;
import com.rgare.global.finre.repository.search.ProductSubtypeSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductSubtypeResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private ProductSubtypeRepository mockProductSubtypeRepository;

  @Mock
  private ProductSubtypeSearchRepository mockProductSubtypeSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private ProductSubtype productSubtype;

  private ProductSubtype result;

  private ProductSubtypeResource resource;

  private Pageable pageable;
  private PageImpl<ProductSubtype> trackingPage;

  private List<ProductSubtype> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new ProductSubtypeResource(mockProductSubtypeRepository, mockProductSubtypeSearchRepository);
    productSubtype = new ProductSubtype();
    productSubtype.setName(NAME);
    productSubtype.setDescription(DESCRIPTION);

    result = new ProductSubtype();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(productSubtype);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    ProductSubtype savedProductSubtype = new ProductSubtype();
    savedProductSubtype = new ProductSubtype();
    savedProductSubtype.setName(NAME);
    savedProductSubtype.setDescription(DESCRIPTION);

    when(mockProductSubtypeRepository.save(savedProductSubtype)).thenReturn(result);

    ResponseEntity<ProductSubtype> responseEntity = resource.createProductSubtype(savedProductSubtype);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    ProductSubtype savedProductSubtype = new ProductSubtype();
    savedProductSubtype = new ProductSubtype();
    savedProductSubtype.setId(ID);
    savedProductSubtype.setName(NAME);
    savedProductSubtype.setDescription(DESCRIPTION);

    ResponseEntity<ProductSubtype> responseEntity = resource.createProductSubtype(savedProductSubtype);

    verifyZeroInteractions(mockProductSubtypeRepository, mockProductSubtypeSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    ProductSubtype savedProductSubtype = new ProductSubtype();
    savedProductSubtype = new ProductSubtype();
    savedProductSubtype.setId(ID);
    savedProductSubtype.setName(NAME);
    savedProductSubtype.setDescription(DESCRIPTION);

    when(mockProductSubtypeRepository.save(savedProductSubtype)).thenReturn(result);

    ResponseEntity<ProductSubtype> responseEntity = resource.updateProductSubtype(savedProductSubtype);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    ProductSubtype savedProductSubtype = new ProductSubtype();
    savedProductSubtype = new ProductSubtype();
    savedProductSubtype.setId(null);
    savedProductSubtype.setName(NAME);
    savedProductSubtype.setDescription(DESCRIPTION);

    when(mockProductSubtypeRepository.save(savedProductSubtype)).thenReturn(result);

    ResponseEntity<ProductSubtype> responseEntity = resource.updateProductSubtype(savedProductSubtype);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockProductSubtypeRepository.findAll()).thenReturn(list);

    List<ProductSubtype> productSubtypes = resource.getAllProductSubtypes();

    assertThat(productSubtypes).isNotNull();
    assertThat(productSubtypes.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockProductSubtypeRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<ProductSubtype> responseEntity = resource.getProductSubtype(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteProductSubtype(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockProductSubtypeRepository).delete(ID);
    verify(mockProductSubtypeSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockProductSubtypeSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<ProductSubtype> response = resource.searchProductSubtypes(query);

    assertThat(response).isNotNull();
  }



}
