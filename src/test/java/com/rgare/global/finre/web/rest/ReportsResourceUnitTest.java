package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.Reports;
import com.rgare.global.finre.repository.ReportsRepository;
import com.rgare.global.finre.repository.search.ReportsSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ReportsResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private ReportsRepository mockReportsRepository;

  @Mock
  private ReportsSearchRepository mockReportsSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private Reports reports;

  private Reports result;

  private ReportsResource resource;

  private Pageable pageable;
  private PageImpl<Reports> trackingPage;

  private List<Reports> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new ReportsResource(mockReportsRepository, mockReportsSearchRepository);
    reports = new Reports();
    reports.setName(NAME);
    reports.setDescription(DESCRIPTION);

    result = new Reports();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(reports);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    Reports savedReports = new Reports();
    savedReports = new Reports();
    savedReports.setName(NAME);
    savedReports.setDescription(DESCRIPTION);

    when(mockReportsRepository.save(savedReports)).thenReturn(result);

    ResponseEntity<Reports> responseEntity = resource.createReports(savedReports);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    Reports savedReports = new Reports();
    savedReports = new Reports();
    savedReports.setId(ID);
    savedReports.setName(NAME);
    savedReports.setDescription(DESCRIPTION);

    ResponseEntity<Reports> responseEntity = resource.createReports(savedReports);

    verifyZeroInteractions(mockReportsRepository, mockReportsSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    Reports savedReports = new Reports();
    savedReports = new Reports();
    savedReports.setId(ID);
    savedReports.setName(NAME);
    savedReports.setDescription(DESCRIPTION);

    when(mockReportsRepository.save(savedReports)).thenReturn(result);

    ResponseEntity<Reports> responseEntity = resource.updateReports(savedReports);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    Reports savedReports = new Reports();
    savedReports = new Reports();
    savedReports.setId(null);
    savedReports.setName(NAME);
    savedReports.setDescription(DESCRIPTION);

    when(mockReportsRepository.save(savedReports)).thenReturn(result);

    ResponseEntity<Reports> responseEntity = resource.updateReports(savedReports);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockReportsRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<Reports> responseEntity = resource.getReports(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockReportsRepository.findAll(pageable)).thenReturn(trackingPage);

    List<Reports> reports = resource.getAllReports();

  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteReports(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockReportsRepository).delete(ID);
    verify(mockReportsSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockReportsSearchRepository.search(any(QueryBuilder.class), eq(pageable))).thenReturn(trackingPage);

    ResponseEntity<List<Reports>> response = resource.searchReports(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }



}
