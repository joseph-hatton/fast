package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.Currency;
import com.rgare.global.finre.repository.CurrencyRepository;
import com.rgare.global.finre.repository.search.CurrencySearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CurrencyResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String CODE = "CODE";

  @Mock
  private CurrencyRepository mockCurrencyRepository;

  @Mock
  private CurrencySearchRepository mockCurrencySearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private Currency currency;

  private Currency result;

  private CurrencyResource resource;

  private Pageable pageable;
  private PageImpl<Currency> trackingPage;

  private List<Currency> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new CurrencyResource(mockCurrencyRepository, mockCurrencySearchRepository);
    currency = new Currency();
    currency.setName(NAME);
    currency.setCode(CODE);

    result = new Currency();
    result.setId(ID);
    result.setName(NAME);
    result.setCode(CODE);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(currency);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    Currency savedCurrency = new Currency();
    savedCurrency = new Currency();
    savedCurrency.setName(NAME);
    savedCurrency.setCode(CODE);

    when(mockCurrencyRepository.save(savedCurrency)).thenReturn(result);

    ResponseEntity<Currency> responseEntity = resource.createCurrency(savedCurrency);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    Currency savedCurrency = new Currency();
    savedCurrency = new Currency();
    savedCurrency.setId(ID);
    savedCurrency.setName(NAME);
    savedCurrency.setCode(CODE);

    ResponseEntity<Currency> responseEntity = resource.createCurrency(savedCurrency);

    verifyZeroInteractions(mockCurrencyRepository, mockCurrencySearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    Currency savedCurrency = new Currency();
    savedCurrency = new Currency();
    savedCurrency.setId(ID);
    savedCurrency.setName(NAME);
    savedCurrency.setCode(CODE);

    when(mockCurrencyRepository.save(savedCurrency)).thenReturn(result);

    ResponseEntity<Currency> responseEntity = resource.updateCurrency(savedCurrency);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    Currency savedCurrency = new Currency();
    savedCurrency = new Currency();
    savedCurrency.setId(null);
    savedCurrency.setName(NAME);
    savedCurrency.setCode(CODE);

    when(mockCurrencyRepository.save(savedCurrency)).thenReturn(result);

    ResponseEntity<Currency> responseEntity = resource.updateCurrency(savedCurrency);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockCurrencyRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<Currency> responseEntity = resource.getCurrency(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockCurrencyRepository.findAll()).thenReturn(list);

    List<Currency> currencies = resource.getAllCurrencies();

    assertThat(currencies).isNotNull();
    assertThat(currencies.size()).isGreaterThan(0);
  }

  @Test
  public void getData_retreiveByCurrencySuccessful() throws URISyntaxException {

    when(mockCurrencyRepository.findByCodeAndYear("CODE",2017)).thenReturn(currency);

    Currency currencyExpected = resource.getByCurrencyCodeAndYear("CODE", 2017);

    assertThat(currencyExpected).isNotNull();
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteCurrency(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockCurrencyRepository).delete(ID);
    verify(mockCurrencySearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockCurrencySearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<Currency> response = resource.searchCurrencies(query);

    assertThat(response).isNotNull();
  }



}
