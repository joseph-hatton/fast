package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.TreatySubtransactionCash;
import com.rgare.global.finre.repository.TreatySubtransactionCashRepository;
import com.rgare.global.finre.repository.search.TreatySubtransactionCashSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TreatySubtransactionCashResourceUnitTest {

  private static final Long ID = 100L;
  private static final String LAST_USER = "LAST_USER";
  private static final String CREATED_USER = "CREATED_USER";

  @Mock
  private TreatySubtransactionCashRepository mockTreatySubtransactionCashRepository;

  @Mock
  private TreatySubtransactionCashSearchRepository mockTreatySubtransactionCashSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private TreatySubtransactionCash treatySubtransactionCash;

  private TreatySubtransactionCash result;

  private TreatySubtransactionCashResource resource;

  private Pageable pageable;
  private PageImpl<TreatySubtransactionCash> trackingPage;

  private List<TreatySubtransactionCash> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new TreatySubtransactionCashResource(mockTreatySubtransactionCashRepository, mockTreatySubtransactionCashSearchRepository);
    treatySubtransactionCash = new TreatySubtransactionCash();
    treatySubtransactionCash.setLastUser(LAST_USER);
    treatySubtransactionCash.setCreationUser(CREATED_USER);

    result = new TreatySubtransactionCash();
    result.setId(ID);
    result.setLastUser(LAST_USER);
    result.setCreationUser(CREATED_USER);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(treatySubtransactionCash);
    trackingPage = new PageImpl<>(list);

  }

  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    TreatySubtransactionCash savedTreatySubtransactionCash = new TreatySubtransactionCash();
    savedTreatySubtransactionCash = new TreatySubtransactionCash();
    savedTreatySubtransactionCash.setLastUser(LAST_USER);
    savedTreatySubtransactionCash.setCreationUser(CREATED_USER);

    when(mockTreatySubtransactionCashRepository.save(savedTreatySubtransactionCash)).thenReturn(result);

    ResponseEntity<TreatySubtransactionCash> responseEntity = resource.createTreatySubTransaction(savedTreatySubtransactionCash);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    TreatySubtransactionCash savedTreatySubtransactionCash = new TreatySubtransactionCash();
    savedTreatySubtransactionCash = new TreatySubtransactionCash();
    savedTreatySubtransactionCash.setId(ID);
    savedTreatySubtransactionCash.setLastUser(LAST_USER);
    savedTreatySubtransactionCash.setCreationUser(CREATED_USER);

    ResponseEntity<TreatySubtransactionCash> responseEntity = resource.createTreatySubTransaction(savedTreatySubtransactionCash);

    verifyZeroInteractions(mockTreatySubtransactionCashRepository, mockTreatySubtransactionCashSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    TreatySubtransactionCash savedTreatySubtransactionCash = new TreatySubtransactionCash();
    savedTreatySubtransactionCash = new TreatySubtransactionCash();
    savedTreatySubtransactionCash.setId(ID);
    savedTreatySubtransactionCash.setLastUser(LAST_USER);
    savedTreatySubtransactionCash.setCreationUser(CREATED_USER);

    when(mockTreatySubtransactionCashRepository.save(savedTreatySubtransactionCash)).thenReturn(result);

    ResponseEntity<TreatySubtransactionCash> responseEntity = resource.updateTreatySubTransaction(savedTreatySubtransactionCash);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    TreatySubtransactionCash savedTreatySubtransactionCash = new TreatySubtransactionCash();
    savedTreatySubtransactionCash = new TreatySubtransactionCash();
    savedTreatySubtransactionCash.setId(null);
    savedTreatySubtransactionCash.setLastUser(LAST_USER);
    savedTreatySubtransactionCash.setCreationUser(CREATED_USER);

    when(mockTreatySubtransactionCashRepository.save(savedTreatySubtransactionCash)).thenReturn(result);

    ResponseEntity<TreatySubtransactionCash> responseEntity = resource.updateTreatySubTransaction(savedTreatySubtransactionCash);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockTreatySubtransactionCashRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<TreatySubtransactionCash> responseEntity = resource.getTreatySubTransaction(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockTreatySubtransactionCashRepository.findAll(pageable)).thenReturn(trackingPage);

    ResponseEntity<List<TreatySubtransactionCash>> responseEntity = resource.getAllTreatySubTransactions(pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllByTransactionsSuccessful() throws URISyntaxException {

    when(mockTreatySubtransactionCashRepository.findAllByTreatyTransactionCashId(ID,pageable)).thenReturn(trackingPage);

    ResponseEntity<List<TreatySubtransactionCash>> responseEntity = resource.getAllByTreatyTransactionId(ID,pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllByTrackingSuccessful() throws URISyntaxException {

    when(mockTreatySubtransactionCashRepository.findAllByTrackingIdAndIsActive(ID,1)).thenReturn(list);

    List<TreatySubtransactionCash> allByTrackingId = resource.getAllByTrackingId(ID);

    assertThat(allByTrackingId).isNotNull();
  }

  @Test
  public void getAllData_retreiveAllByMDMSuccessful() throws URISyntaxException {

    when(mockTreatySubtransactionCashRepository.findAllByMdmIdAndIsActive(ID,1)).thenReturn(list);

    List<TreatySubtransactionCash> allByTrackingId = resource.getAllByMdmId(ID);

    assertThat(allByTrackingId).isNotNull();
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteTreatySubTransaction(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockTreatySubtransactionCashRepository).delete(ID);
    verify(mockTreatySubtransactionCashSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockTreatySubtransactionCashSearchRepository.search(any(QueryBuilder.class), eq(pageable))).thenReturn(trackingPage);

    ResponseEntity<List<TreatySubtransactionCash>> response = resource.searchTreatySubTransactions(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }



}
