package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.Company;
import com.rgare.global.finre.domain.Contact;
import com.rgare.global.finre.repository.ContactRepository;
import com.rgare.global.finre.repository.search.ContactSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ContactResourceUnitTest {

  private static final Long ID = 100L;
  private static final String COMMENTS = "COMMENTS";
  private static final String EMAIL = "EMAIL";
  private static final String QUERY = "QUERY";

  @Mock
  private ContactRepository mockContractSubtypeRepository;

  @Mock
  private ContactSearchRepository mockContactSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private Contact contact;

  private Contact result;

  private ContactResource resource;

  private Pageable pageable;
  private PageImpl<Contact> trackingPage;

  private List<Contact> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new ContactResource(mockContractSubtypeRepository, mockContactSearchRepository);
    contact = new Contact();
    contact.setComments(COMMENTS);
    contact.setEmail(EMAIL);

    result = new Contact();
    result.setId(ID);
    result.setComments(COMMENTS);
    result.setEmail(EMAIL);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(contact);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    Contact savedContact = new Contact();
    savedContact = new Contact();
    savedContact.setComments(COMMENTS);
    savedContact.setEmail(EMAIL);

    when(mockContractSubtypeRepository.save(savedContact)).thenReturn(result);

    ResponseEntity<Contact> responseEntity = resource.createContact(savedContact);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    Contact savedContact = new Contact();
    savedContact = new Contact();
    savedContact.setId(ID);
    savedContact.setComments(COMMENTS);
    savedContact.setEmail(EMAIL);

    ResponseEntity<Contact> responseEntity = resource.createContact(savedContact);

    verifyZeroInteractions(mockContractSubtypeRepository, mockContactSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    Contact savedContact = new Contact();
    savedContact = new Contact();
    savedContact.setId(ID);
    savedContact.setComments(COMMENTS);
    savedContact.setEmail(EMAIL);

    when(mockContractSubtypeRepository.save(savedContact)).thenReturn(result);

    ResponseEntity<Contact> responseEntity = resource.updateContact(savedContact);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    Contact savedContact = new Contact();
    savedContact = new Contact();
    savedContact.setId(null);
    savedContact.setComments(COMMENTS);
    savedContact.setEmail(EMAIL);

    when(mockContractSubtypeRepository.save(savedContact)).thenReturn(result);

    ResponseEntity<Contact> responseEntity = resource.updateContact(savedContact);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockContractSubtypeRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<Contact> responseEntity = resource.getContact(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockContractSubtypeRepository.findAll(pageable)).thenReturn(trackingPage);

    ResponseEntity<List<Contact>> responseEntity = resource.getAllContacts(pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }


  @Test
  public void searchData_searchByFirstNameSuccessful() throws URISyntaxException {

    when(mockContractSubtypeRepository.findByFirstNameOrLastName(QUERY)).thenReturn(list);

    ResponseEntity<List<Contact>> responseEntity = resource.searchContactsByFirstLastName(QUERY);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchData_searchByFirstNamePageSuccessful() throws URISyntaxException {

    when(mockContractSubtypeRepository.findByFirstNameOrLastNamePag(QUERY, pageable)).thenReturn(trackingPage);

    ResponseEntity<List<Contact>> responseEntity = resource.searchContactsByFirstLastNamePag(QUERY, pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteContact(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockContractSubtypeRepository).delete(ID);
    verify(mockContactSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";

    when(mockContactSearchRepository.search(any(QueryBuilder.class), eq(pageable))).thenReturn(trackingPage);

    ResponseEntity<List<Contact>> response = resource.searchContacts(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }



}
