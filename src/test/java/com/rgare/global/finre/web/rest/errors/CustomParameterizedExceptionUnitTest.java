package com.rgare.global.finre.web.rest.errors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class CustomParameterizedExceptionUnitTest {

  private static final Long ID = 100L;

  private static final String PARAM = "param";

  private final String MESSAGE = "MESSAGE";

  private final Map<String, String> paramMap = new HashMap<>();

  private CustomParameterizedException customParameterizedException;

  @Before
  public void setup() throws URISyntaxException {
    paramMap.put(PARAM,PARAM);
  }


  @Test
  public void getData_getErrorVMSuccessful() throws URISyntaxException {
    customParameterizedException = new CustomParameterizedException(MESSAGE,PARAM,PARAM);
    ParameterizedErrorVM parameterizedErrorVM = customParameterizedException.getErrorVM();
    assertThat(parameterizedErrorVM.getMessage()).isEqualTo(MESSAGE);
  }

  @Test
  public void getData_getMapErrorVMSuccessful() throws URISyntaxException {
    customParameterizedException = new CustomParameterizedException(MESSAGE,paramMap);
    ParameterizedErrorVM parameterizedErrorVM = customParameterizedException.getErrorVM();
    assertThat(parameterizedErrorVM.getMessage()).isEqualTo(MESSAGE);
    assertThat(parameterizedErrorVM.getParams()).isEqualTo(paramMap);
  }

}
