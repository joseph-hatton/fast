package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.AgreementType;
import com.rgare.global.finre.domain.TreatyTracking;
import com.rgare.global.finre.repository.AgreementTypeRepository;
import com.rgare.global.finre.repository.search.AgreementTypeSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AgreementTypeResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private AgreementTypeRepository mockAgreementTypeRepository;

  @Mock
  private AgreementTypeSearchRepository mockAgreementTypeSearchRepository;

  private AgreementType agreementType;

  private AgreementType result;

  private AgreementTypeResource resource;

  private Pageable pageable;
  private PageImpl<AgreementType> trackingPage;

  private List<AgreementType> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new AgreementTypeResource(mockAgreementTypeRepository, mockAgreementTypeSearchRepository);
    agreementType = new AgreementType();
    agreementType.setName(NAME);
    agreementType.setDescription(DESCRIPTION);

    result = new AgreementType();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);


    list = Arrays.asList(agreementType);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    AgreementType savedAgreementType = new AgreementType();
    savedAgreementType = new AgreementType();
    savedAgreementType.setName(NAME);
    savedAgreementType.setDescription(DESCRIPTION);

    when(mockAgreementTypeRepository.save(savedAgreementType)).thenReturn(result);

    ResponseEntity<AgreementType> responseEntity = resource.createAgreementType(savedAgreementType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    AgreementType savedAgreementType = new AgreementType();
    savedAgreementType = new AgreementType();
    savedAgreementType.setId(ID);
    savedAgreementType.setName(NAME);
    savedAgreementType.setDescription(DESCRIPTION);

    ResponseEntity<AgreementType> responseEntity = resource.createAgreementType(savedAgreementType);

    verifyZeroInteractions(mockAgreementTypeRepository, mockAgreementTypeSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    AgreementType savedAgreementType = new AgreementType();
    savedAgreementType = new AgreementType();
    savedAgreementType.setId(ID);
    savedAgreementType.setName(NAME);
    savedAgreementType.setDescription(DESCRIPTION);

    when(mockAgreementTypeRepository.save(savedAgreementType)).thenReturn(result);

    ResponseEntity<AgreementType> responseEntity = resource.updateAgreementType(savedAgreementType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    AgreementType savedAgreementType = new AgreementType();
    savedAgreementType = new AgreementType();
    savedAgreementType.setId(null);
    savedAgreementType.setName(NAME);
    savedAgreementType.setDescription(DESCRIPTION);

    when(mockAgreementTypeRepository.save(savedAgreementType)).thenReturn(result);

    ResponseEntity<AgreementType> responseEntity = resource.updateAgreementType(savedAgreementType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockAgreementTypeRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<AgreementType> responseEntity = resource.getAgreementType(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockAgreementTypeRepository.findAll()).thenReturn(list);

    List<AgreementType> allAgreementTypes = resource.getAllAgreementTypes();

    assertThat(allAgreementTypes).isNotNull();
    assertThat(allAgreementTypes.size()).isGreaterThan(0);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteAgreementType(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockAgreementTypeRepository).delete(ID);
    verify(mockAgreementTypeSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockAgreementTypeSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<AgreementType> response = resource.searchAgreementTypes(query);

    assertThat(response).isNotNull();
  }



}
