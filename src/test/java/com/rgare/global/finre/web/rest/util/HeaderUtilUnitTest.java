package com.rgare.global.finre.web.rest.util;

import com.rgare.global.finre.web.rest.errors.ErrorVM;
import com.rgare.global.finre.web.rest.errors.FieldErrorVM;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class HeaderUtilUnitTest {

  private static final Long ID = 100L;

  private static final String APPLICATION_NAME = "finreApp";

  private static final String MESSAGE = "MESSAGE";
  private static final String MESSAGE_CREATED = "finreApp.MESSAGE.created";
  private static final String MESSAGE_UPDATED = "finreApp.MESSAGE.updated";
  private static final String MESSAGE_DELETED = "finreApp.MESSAGE.deleted";
  private static final String MESSAGE_ERROR = "error.ERROR";
  private static final String PARAM = "PARAM";
  private static final String ERROR = "ERROR";

  private HttpHeaders httpHeaders;

  private HttpHeaders httpHeadersCreated;
  private HttpHeaders httpHeadersUpdated;
  private HttpHeaders httpHeadersDeleted;
  private HttpHeaders httpHeadersError;

  @Before
  public void setup() throws URISyntaxException {
    HeaderUtil headerUtil = new HeaderUtil();
    httpHeaders = new HttpHeaders();
    httpHeaders.add("X-finreApp-alert", MESSAGE);
    httpHeaders.add("X-finreApp-params", PARAM);

    httpHeadersCreated = new HttpHeaders();
    httpHeadersCreated.add("X-finreApp-alert", MESSAGE_CREATED);
    httpHeadersCreated.add("X-finreApp-params", PARAM);

    httpHeadersUpdated = new HttpHeaders();
    httpHeadersUpdated.add("X-finreApp-alert", MESSAGE_UPDATED);
    httpHeadersUpdated.add("X-finreApp-params", PARAM);

    httpHeadersDeleted = new HttpHeaders();
    httpHeadersDeleted.add("X-finreApp-alert", MESSAGE_DELETED);
    httpHeadersDeleted.add("X-finreApp-params", PARAM);

    httpHeadersError = new HttpHeaders();
    httpHeadersError.add("X-finreApp-error", MESSAGE_ERROR);
    httpHeadersError.add("X-finreApp-params", PARAM);
  }

  @Test
  public void createData_createAlertSuccessful() throws URISyntaxException {
    HttpHeaders expected = HeaderUtil.createAlert(MESSAGE,PARAM);
    assertThat(expected).isEqualTo(httpHeaders);
  }

  @Test
  public void createdData_createEntityAlertSuccessful() throws URISyntaxException {
    HttpHeaders expected = HeaderUtil.createEntityCreationAlert(MESSAGE,PARAM);
    assertThat(expected).isEqualTo(httpHeadersCreated);
  }

  @Test
  public void updatedData_updatedEntityAlertSuccessful() throws URISyntaxException {
    HttpHeaders expected = HeaderUtil.createEntityUpdateAlert(MESSAGE,PARAM);
    assertThat(expected).isEqualTo(httpHeadersUpdated);
  }

  @Test
  public void deletedData_deletedEntityAlertSuccessful() throws URISyntaxException {
    HttpHeaders expected = HeaderUtil.createEntityDeletionAlert(MESSAGE,PARAM);
    assertThat(expected).isEqualTo(httpHeadersDeleted);
  }

  @Test
  public void errorData_errorEntityAlertSuccessful() throws URISyntaxException {
    HttpHeaders expected = HeaderUtil.createFailureAlert(PARAM,ERROR,MESSAGE);
    assertThat(expected).isEqualTo(httpHeadersError);
  }
}
