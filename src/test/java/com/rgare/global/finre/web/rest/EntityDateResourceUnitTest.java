package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.EntityDate;
import com.rgare.global.finre.repository.EntityDateRepository;
import com.rgare.global.finre.repository.search.EntityDateSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EntityDateResourceUnitTest {

  private static final Long ID = 100L;
  private static final String TREATY_COMMENTS = "TREATY_COMMENTS";
  private static final String COMMENTS = "COMMENTS";

  @Mock
  private EntityDateRepository mockEntityDateRepository;

  @Mock
  private EntityDateSearchRepository mockEntityDateSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private EntityDate entityDate;

  private EntityDate result;

  private EntityDateResource resource;

  private Pageable pageable;
  private PageImpl<EntityDate> trackingPage;

  private List<EntityDate> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new EntityDateResource(mockEntityDateRepository, mockEntityDateSearchRepository);
    entityDate = new EntityDate();
    entityDate.setTreatyRefComments(TREATY_COMMENTS);
    entityDate.setComments(COMMENTS);

    result = new EntityDate();
    result.setId(ID);
    result.setTreatyRefComments(TREATY_COMMENTS);
    result.setComments(COMMENTS);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(entityDate);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    EntityDate savedEntityDate = new EntityDate();
    savedEntityDate = new EntityDate();
    savedEntityDate.setTreatyRefComments(TREATY_COMMENTS);
    savedEntityDate.setComments(COMMENTS);

    when(mockEntityDateRepository.save(savedEntityDate)).thenReturn(result);

    ResponseEntity<EntityDate> responseEntity = resource.createEntityDate(savedEntityDate);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    EntityDate savedEntityDate = new EntityDate();
    savedEntityDate = new EntityDate();
    savedEntityDate.setId(ID);
    savedEntityDate.setTreatyRefComments(TREATY_COMMENTS);
    savedEntityDate.setComments(COMMENTS);

    ResponseEntity<EntityDate> responseEntity = resource.createEntityDate(savedEntityDate);

    verifyZeroInteractions(mockEntityDateRepository, mockEntityDateSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    EntityDate savedEntityDate = new EntityDate();
    savedEntityDate = new EntityDate();
    savedEntityDate.setId(ID);
    savedEntityDate.setTreatyRefComments(TREATY_COMMENTS);
    savedEntityDate.setComments(COMMENTS);

    when(mockEntityDateRepository.save(savedEntityDate)).thenReturn(result);

    ResponseEntity<EntityDate> responseEntity = resource.updateEntityDate(savedEntityDate);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    EntityDate savedEntityDate = new EntityDate();
    savedEntityDate = new EntityDate();
    savedEntityDate.setId(null);
    savedEntityDate.setTreatyRefComments(TREATY_COMMENTS);
    savedEntityDate.setComments(COMMENTS);

    when(mockEntityDateRepository.save(savedEntityDate)).thenReturn(result);

    ResponseEntity<EntityDate> responseEntity = resource.updateEntityDate(savedEntityDate);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockEntityDateRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<EntityDate> responseEntity = resource.getEntityDate(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockEntityDateRepository.findAll()).thenReturn(list);

    List<EntityDate> allEntityDates = resource.getAllEntityDates();

    assertThat(allEntityDates).isNotNull();
  }

  @Test
  public void getAllData_retreiveAllByEntityDateSuccessful() throws URISyntaxException {

    when(mockEntityDateRepository.findAllByEntityId(ID)).thenReturn(list);

    List<EntityDate> allEntityDates = resource.getAllEntityDatesByEntityId(ID);

    assertThat(allEntityDates).isNotNull();
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteEntityDate(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockEntityDateRepository).delete(ID);
    verify(mockEntityDateSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockEntityDateSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<EntityDate> response = resource.searchEntityDates(query);

    assertThat(response).isNotNull();
  }



}
