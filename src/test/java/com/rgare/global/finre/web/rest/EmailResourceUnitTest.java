package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.TransactionStatus;
import com.rgare.global.finre.domain.Treaty;
import com.rgare.global.finre.domain.TreatyTransactionCash;
import com.rgare.global.finre.repository.TransactionStatusRepository;
import com.rgare.global.finre.repository.search.TransactionStatusSearchRepository;
import com.rgare.global.finre.service.EmailService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EmailResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private EmailService mockEmailService;

  private MailResource resource;

  private Treaty treaty;
  private TreatyTransactionCash treatyTransactionCash;

  private Pageable pageable;
  private PageImpl<TransactionStatus> trackingPage;


  @Before
  public void setup() throws URISyntaxException {
    resource = new MailResource(mockEmailService);
    pageable = new PageRequest(1, 5);

    treaty = new Treaty();
    treaty.setId(ID);

    treatyTransactionCash = new TreatyTransactionCash();
    treatyTransactionCash.setId(ID);
  }

  @Test
  public void sendData_sendEmailSuccessful() throws URISyntaxException {

    ResponseEntity<Treaty> responseEntity = resource.sendEmail(treaty);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void sendData_sendEmailTemplateSuccessful() throws URISyntaxException {

    ResponseEntity<TreatyTransactionCash> responseEntity = resource.sendEmailTemplate(treatyTransactionCash);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void sendData_sendEmailAttachmentSuccessful() throws URISyntaxException {

    ResponseEntity<TreatyTransactionCash> responseEntity = resource.sendEmailAttachmentTemplate(treatyTransactionCash);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

}
