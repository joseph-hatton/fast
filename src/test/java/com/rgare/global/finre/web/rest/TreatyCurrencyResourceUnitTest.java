package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.TreatyCurrency;
import com.rgare.global.finre.repository.TreatyCurrencyRepository;
import com.rgare.global.finre.repository.search.TreatyCurrencySearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TreatyCurrencyResourceUnitTest {

  private static final Long ID = 100L;
  private static final String LAST_USER = "LAST_USER";
  private static final String CREATED_USER = "CREATED_USER";

  @Mock
  private TreatyCurrencyRepository mockTreatyCurrencyRepository;

  @Mock
  private TreatyCurrencySearchRepository mockTreatyCurrencySearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private TreatyCurrency treatyCurrency;

  private TreatyCurrency result;

  private TreatyCurrencyResource resource;

  private Pageable pageable;
  private PageImpl<TreatyCurrency> trackingPage;

  private List<TreatyCurrency> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new TreatyCurrencyResource(mockTreatyCurrencyRepository, mockTreatyCurrencySearchRepository);
    treatyCurrency = new TreatyCurrency();
    treatyCurrency.setLastUser(LAST_USER);
    treatyCurrency.setCreationUser(CREATED_USER);

    result = new TreatyCurrency();
    result.setId(ID);
    result.setLastUser(LAST_USER);
    result.setCreationUser(CREATED_USER);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(treatyCurrency);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    TreatyCurrency savedTreatyCurrency = new TreatyCurrency();
    savedTreatyCurrency = new TreatyCurrency();
    savedTreatyCurrency.setLastUser(LAST_USER);
    savedTreatyCurrency.setCreationUser(CREATED_USER);

    when(mockTreatyCurrencyRepository.save(savedTreatyCurrency)).thenReturn(result);

    ResponseEntity<TreatyCurrency> responseEntity = resource.createTreatyCurrency(savedTreatyCurrency);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    TreatyCurrency savedTreatyCurrency = new TreatyCurrency();
    savedTreatyCurrency = new TreatyCurrency();
    savedTreatyCurrency.setId(ID);
    savedTreatyCurrency.setLastUser(LAST_USER);
    savedTreatyCurrency.setCreationUser(CREATED_USER);

    ResponseEntity<TreatyCurrency> responseEntity = resource.createTreatyCurrency(savedTreatyCurrency);

    verifyZeroInteractions(mockTreatyCurrencyRepository, mockTreatyCurrencySearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    TreatyCurrency savedTreatyCurrency = new TreatyCurrency();
    savedTreatyCurrency = new TreatyCurrency();
    savedTreatyCurrency.setId(ID);
    savedTreatyCurrency.setLastUser(LAST_USER);
    savedTreatyCurrency.setCreationUser(CREATED_USER);

    when(mockTreatyCurrencyRepository.save(savedTreatyCurrency)).thenReturn(result);

    ResponseEntity<TreatyCurrency> responseEntity = resource.updateTreatyCurrency(savedTreatyCurrency);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    TreatyCurrency savedTreatyCurrency = new TreatyCurrency();
    savedTreatyCurrency = new TreatyCurrency();
    savedTreatyCurrency.setId(null);
    savedTreatyCurrency.setLastUser(LAST_USER);
    savedTreatyCurrency.setCreationUser(CREATED_USER);

    when(mockTreatyCurrencyRepository.save(savedTreatyCurrency)).thenReturn(result);

    ResponseEntity<TreatyCurrency> responseEntity = resource.updateTreatyCurrency(savedTreatyCurrency);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockTreatyCurrencyRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<TreatyCurrency> responseEntity = resource.getTreatyCurrency(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockTreatyCurrencyRepository.findAll()).thenReturn(list);

    List<TreatyCurrency> allTreatyCurrencies = resource.getAllTreatyCurrencies();

    assertThat(allTreatyCurrencies).isNotNull();
  }

  @Test
  public void getCountData_currencyCountSuccessful() throws URISyntaxException {

    when(mockTreatyCurrencyRepository.findAllByCurrencyId(ID)).thenReturn(list);

    Integer count = resource.getAllByCurrencyCount(ID);

    assertThat(count).isGreaterThan(0);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteTreatyCurrency(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockTreatyCurrencyRepository).delete(ID);
    verify(mockTreatyCurrencySearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockTreatyCurrencySearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<TreatyCurrency> response = resource.searchTreatyCurrencies(query);

    assertThat(response).isNotNull();
  }



}
