package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.ContactType;
import com.rgare.global.finre.repository.ContactTypeRepository;
import com.rgare.global.finre.repository.search.ContactTypeSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ContactTypeResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private ContactTypeRepository mockContractSubtypeRepository;

  @Mock
  private ContactTypeSearchRepository mockContactTypeSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private ContactType contactType;

  private ContactType result;

  private ContactTypeResource resource;

  private Pageable pageable;
  private PageImpl<ContactType> trackingPage;

  private List<ContactType> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new ContactTypeResource(mockContractSubtypeRepository, mockContactTypeSearchRepository);
    contactType = new ContactType();
    contactType.setName(NAME);
    contactType.setDescription(DESCRIPTION);

    result = new ContactType();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(contactType);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    ContactType savedContactType = new ContactType();
    savedContactType = new ContactType();
    savedContactType.setName(NAME);
    savedContactType.setDescription(DESCRIPTION);

    when(mockContractSubtypeRepository.save(savedContactType)).thenReturn(result);

    ResponseEntity<ContactType> responseEntity = resource.createContactType(savedContactType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    ContactType savedContactType = new ContactType();
    savedContactType = new ContactType();
    savedContactType.setId(ID);
    savedContactType.setName(NAME);
    savedContactType.setDescription(DESCRIPTION);

    ResponseEntity<ContactType> responseEntity = resource.createContactType(savedContactType);

    verifyZeroInteractions(mockContractSubtypeRepository, mockContactTypeSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    ContactType savedContactType = new ContactType();
    savedContactType = new ContactType();
    savedContactType.setId(ID);
    savedContactType.setName(NAME);
    savedContactType.setDescription(DESCRIPTION);

    when(mockContractSubtypeRepository.save(savedContactType)).thenReturn(result);

    ResponseEntity<ContactType> responseEntity = resource.updateContactType(savedContactType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    ContactType savedContactType = new ContactType();
    savedContactType = new ContactType();
    savedContactType.setId(null);
    savedContactType.setName(NAME);
    savedContactType.setDescription(DESCRIPTION);

    when(mockContractSubtypeRepository.save(savedContactType)).thenReturn(result);

    ResponseEntity<ContactType> responseEntity = resource.updateContactType(savedContactType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockContractSubtypeRepository.findAll()).thenReturn(list);

    List<ContactType> contactTypes = resource.getAllContactTypes();

    assertThat(contactTypes).isNotNull();
    assertThat(contactTypes.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockContractSubtypeRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<ContactType> responseEntity = resource.getContactType(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteContactType(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockContractSubtypeRepository).delete(ID);
    verify(mockContactTypeSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockContactTypeSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<ContactType> response = resource.searchContactTypes(query);

    assertThat(response).isNotNull();
  }



}
