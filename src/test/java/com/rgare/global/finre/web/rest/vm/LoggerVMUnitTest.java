package com.rgare.global.finre.web.rest.vm;

import ch.qos.logback.classic.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.LoggerFactory;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * View Model object for storing a Logback logger.
 */
@RunWith(MockitoJUnitRunner.class)
public class LoggerVMUnitTest {

    private String NAME="NAME";

    private String LEVEL = "LEVEL";

    private String result = "LoggerVM{" +
      "name='" + NAME + '\'' +
    ", level='" + LEVEL + '\'' +
    '}';

    private Logger logger;

    @Before
    public void setup() {
      logger = (Logger) LoggerFactory.getLogger(String.class);
      LoggerVM loggerVM = new LoggerVM(logger);
    }

    @Test
    public void getData_testSetters(){
      LoggerVM loggerVM = new LoggerVM(logger);
      loggerVM.setName(NAME);
      loggerVM.setLevel(LEVEL);
      assertThat(loggerVM.getName()).isEqualTo(NAME);
      assertThat(loggerVM.getLevel()).isEqualTo(LEVEL);
    }

    @Test
    public void toStringComparisonTest() {
      LoggerVM loggerVM = new LoggerVM();
      loggerVM.setName(NAME);
      loggerVM.setLevel(LEVEL);

      assertThat(loggerVM.toString()).isEqualTo(result);
    }
}
