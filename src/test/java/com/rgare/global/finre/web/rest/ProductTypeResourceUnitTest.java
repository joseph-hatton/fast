package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.ProductType;
import com.rgare.global.finre.repository.ProductTypeRepository;
import com.rgare.global.finre.repository.search.ProductTypeSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductTypeResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private ProductTypeRepository mockProductTypeRepository;

  @Mock
  private ProductTypeSearchRepository mockProductTypeSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private ProductType productType;

  private ProductType result;

  private ProductTypeResource resource;

  private Pageable pageable;
  private PageImpl<ProductType> trackingPage;

  private List<ProductType> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new ProductTypeResource(mockProductTypeRepository, mockProductTypeSearchRepository);
    productType = new ProductType();
    productType.setName(NAME);
    productType.setDescription(DESCRIPTION);

    result = new ProductType();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(productType);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    ProductType savedProductType = new ProductType();
    savedProductType = new ProductType();
    savedProductType.setName(NAME);
    savedProductType.setDescription(DESCRIPTION);

    when(mockProductTypeRepository.save(savedProductType)).thenReturn(result);

    ResponseEntity<ProductType> responseEntity = resource.createProductType(savedProductType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    ProductType savedProductType = new ProductType();
    savedProductType = new ProductType();
    savedProductType.setId(ID);
    savedProductType.setName(NAME);
    savedProductType.setDescription(DESCRIPTION);

    ResponseEntity<ProductType> responseEntity = resource.createProductType(savedProductType);

    verifyZeroInteractions(mockProductTypeRepository, mockProductTypeSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    ProductType savedProductType = new ProductType();
    savedProductType = new ProductType();
    savedProductType.setId(ID);
    savedProductType.setName(NAME);
    savedProductType.setDescription(DESCRIPTION);

    when(mockProductTypeRepository.save(savedProductType)).thenReturn(result);

    ResponseEntity<ProductType> responseEntity = resource.updateProductType(savedProductType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    ProductType savedProductType = new ProductType();
    savedProductType = new ProductType();
    savedProductType.setId(null);
    savedProductType.setName(NAME);
    savedProductType.setDescription(DESCRIPTION);

    when(mockProductTypeRepository.save(savedProductType)).thenReturn(result);

    ResponseEntity<ProductType> responseEntity = resource.updateProductType(savedProductType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockProductTypeRepository.findAll()).thenReturn(list);

    List<ProductType> productTypes = resource.getAllProductTypes();

    assertThat(productTypes).isNotNull();
    assertThat(productTypes.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockProductTypeRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<ProductType> responseEntity = resource.getProductType(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteProductType(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockProductTypeRepository).delete(ID);
    verify(mockProductTypeSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockProductTypeSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<ProductType> response = resource.searchProductTypes(query);

    assertThat(response).isNotNull();
  }



}
