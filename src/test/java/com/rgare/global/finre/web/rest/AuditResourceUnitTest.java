package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.service.AuditEventService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuditResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private AuditEventService mockAuditEventService;


  private AuditEvent audit;

  private AuditEvent result;

  private AuditResource resource;

  private Pageable pageable;
  private Page<AuditEvent> trackingPage;

  private List<AuditEvent> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new AuditResource(mockAuditEventService);
    audit = new AuditEvent(NAME,DESCRIPTION);

    result = new AuditEvent(NAME,DESCRIPTION);

    list = Arrays.asList(audit);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockAuditEventService.findAll(pageable)).thenReturn(trackingPage);

    ResponseEntity<List<AuditEvent>> responseEntity = resource.getAll(pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getByDates_retreiveByDatesSuccessful() throws URISyntaxException {

    LocalDate fromDate = LocalDate.now();
    LocalDate toDate = LocalDate.now();
    ArgumentCaptor<LocalDateTime> fromTimeArgumentCaptor = ArgumentCaptor.forClass(LocalDateTime.class);
    ArgumentCaptor<LocalDateTime> toTimeArgumentCaptor = ArgumentCaptor.forClass(LocalDateTime.class);

    when(mockAuditEventService.findByDates(fromTimeArgumentCaptor.capture(), toTimeArgumentCaptor.capture(), eq(pageable))).thenReturn(trackingPage);

    ResponseEntity<List<AuditEvent>> responseEntity = resource.getByDates(fromDate,toDate, pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(responseEntity.getBody()).isEqualTo(list);
  }

  @Test
  public void fineOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockAuditEventService.find(ID)).thenReturn(Optional.of(result));

    ResponseEntity<AuditEvent> responseEntity = resource.get(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(responseEntity.getBody()).isEqualTo(result);
  }

}
