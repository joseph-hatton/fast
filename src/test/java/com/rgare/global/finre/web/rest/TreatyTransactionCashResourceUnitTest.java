package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.*;
import com.rgare.global.finre.repository.AuditTransactionsRepository;
import com.rgare.global.finre.repository.TreatySubtransactionCashRepository;
import com.rgare.global.finre.repository.TreatyTransactionCashRepository;
import com.rgare.global.finre.repository.search.TreatyTransactionCashSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TreatyTransactionCashResourceUnitTest {

  private static final Long ID = 100L;
  private static final String LAST_USER = "LAST_USER";
  private static final String CREATED_USER = "CREATED_USER";

  @Mock
  private TreatyTransactionCashRepository mockTreatyTransactionCashRepository;

  @Mock
  private TreatyTransactionCashSearchRepository mockTreatyTransactionCashSearchRepository;

  @Mock
  private AuditTransactionsRepository mockAuditTransactionsRepository;

  @Mock
  private TreatySubtransactionCashRepository mockTreatySubtransactionCashRepository;


  @Mock
  private Dp3LogService mockCDp3LogService;


  private TreatyTransactionCash treatyTransactionCash;

  private TreatyTransactionCash result;

  private TreatyTransactionCashResource resource;

  private Pageable pageable;
  private PageImpl<TreatyTransactionCash> trackingPage;

  private List<TreatyTransactionCash> list;

  private  List<TreatySubtransactionCash> subtransactions;

  @Before
  public void setup() throws URISyntaxException {
    resource = new TreatyTransactionCashResource(mockTreatyTransactionCashRepository,
      mockTreatyTransactionCashSearchRepository,
      mockAuditTransactionsRepository,
      mockTreatySubtransactionCashRepository);
    treatyTransactionCash = new TreatyTransactionCash();
    treatyTransactionCash.setLastUser(LAST_USER);
    treatyTransactionCash.setCreationUser(CREATED_USER);
    TransactionStatus transactionStatus = new TransactionStatus();
    transactionStatus.setName("TEST");
    treatyTransactionCash.setTransactionStatus(new TransactionStatus());

    result = new TreatyTransactionCash();
    result.setId(ID);
    result.setLastUser(LAST_USER);
    result.setCreationUser(CREATED_USER);
    result.setTransactionStatus(transactionStatus);
    Treaty treaty = new Treaty();
    treaty.setTreatyStatus(new TreatyStatus());
    result.setTreaty(treaty);
    result.setMixTransactions(1);
    result.setIsActive(1);


    pageable = new PageRequest(1, 5);


    list = Arrays.asList(treatyTransactionCash);
    trackingPage = new PageImpl<>(list);

    subtransactions = new ArrayList<TreatySubtransactionCash>();
    TreatySubtransactionCash treatySubtransactionCash = new TreatySubtransactionCash();
    treatySubtransactionCash.setTreatyTransactionCash(result);
    subtransactions.add(treatySubtransactionCash);
  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    TreatyTransactionCash savedTreatyTransactionCash = new TreatyTransactionCash();
    savedTreatyTransactionCash = new TreatyTransactionCash();
    savedTreatyTransactionCash.setLastUser(LAST_USER);
    savedTreatyTransactionCash.setCreationUser(CREATED_USER);
    Treaty treaty = new Treaty();
    treaty.setTreatyStatus(new TreatyStatus());
    savedTreatyTransactionCash.setTreaty(treaty);

    when(mockTreatyTransactionCashRepository.save(savedTreatyTransactionCash)).thenReturn(result);

    ResponseEntity<TreatyTransactionCash> responseEntity = resource.createTreatyTransactionCash(savedTreatyTransactionCash);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    TreatyTransactionCash savedTreatyTransactionCash = new TreatyTransactionCash();
    savedTreatyTransactionCash = new TreatyTransactionCash();
    savedTreatyTransactionCash.setId(ID);
    savedTreatyTransactionCash.setLastUser(LAST_USER);
    savedTreatyTransactionCash.setCreationUser(CREATED_USER);

    ResponseEntity<TreatyTransactionCash> responseEntity = resource.createTreatyTransactionCash(savedTreatyTransactionCash);

    verifyZeroInteractions(mockTreatyTransactionCashRepository, mockTreatyTransactionCashSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    TreatyTransactionCash savedTreatyTransactionCash = new TreatyTransactionCash();
    savedTreatyTransactionCash = new TreatyTransactionCash();
    savedTreatyTransactionCash.setId(ID);
    savedTreatyTransactionCash.setLastUser(LAST_USER);
    savedTreatyTransactionCash.setCreationUser(CREATED_USER);
    TransactionStatus transactionStatus = new TransactionStatus();
    transactionStatus.setName("TEST");
    savedTreatyTransactionCash.setTransactionStatus(transactionStatus);

    Treaty treaty = new Treaty();
    treaty.setTreatyStatus(new TreatyStatus());
    savedTreatyTransactionCash.setTreaty(treaty);

    when(mockTreatyTransactionCashRepository.save(savedTreatyTransactionCash)).thenReturn(result);
    when(mockTreatyTransactionCashRepository.findOne(ID)).thenReturn(savedTreatyTransactionCash);

    ResponseEntity<TreatyTransactionCash> responseEntity = resource.updatetreatyTransactionCash(savedTreatyTransactionCash);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockTreatyTransactionCashRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<TreatyTransactionCash> responseEntity = resource.getTreatyTransactionCash(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }


  @Test
  public void getAllData_retreiveAllCashTransactionsSuccessful() throws URISyntaxException {

    when(mockTreatyTransactionCashRepository.findAll(pageable)).thenReturn(trackingPage);

    ResponseEntity<List<TreatyTransactionCash>> responseEntity = resource.getAllTreatyTransactionCashs(pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllByTreatyData_retreiveAllByTreatyIdSuccessful() throws URISyntaxException {

    when(mockTreatyTransactionCashRepository.findAllByTreatyIdAndIsActive(ID,1)).thenReturn(list);

    List<TreatyTransactionCash> treatyTransactionCashes = resource.getAllByTreatyId(ID);

    assertThat(treatyTransactionCashes).isNotNull();
  }

  @Test
  public void getAllByCurrencyCodeData_retreiveAllByCurrencyCodeSuccessful() throws URISyntaxException {

    when(mockTreatyTransactionCashRepository.findAllByTreatyIdAndCurrencyCodeAndIsActive(ID,"CURRENCY_CODE",1)).thenReturn(list);

    List<TreatyTransactionCash> treatyTransactionCashes = resource.getAllByTreatyIdCurrencyCode(ID,"CURRENCY_CODE");

    assertThat(treatyTransactionCashes).isNotNull();
  }

  @Test
  public void getAllByCompanyCountData_retreiveCompanyCountSuccessful() throws URISyntaxException {

    when(mockTreatyTransactionCashRepository.findAllByTreatyIdAndCompanyIdAndIsActive(ID,1L,1)).thenReturn(list);

    Integer allByCompanyIdCount = resource.getAllByCompanyIdCount(ID);

    assertThat(allByCompanyIdCount).isEqualTo(0);
  }

  @Test
  public void getAllByCompanyData_retreiveCompanySuccessful() throws URISyntaxException {

    when(mockTreatyTransactionCashRepository.findAllByTreatyIdAndCompanyIdAndIsActive(ID,1L,1)).thenReturn(list);

    List<TreatyTransactionCash> treatyTransactionCashes = resource.getAllByTreatyIdCompanyId(ID, 1L);

    assertThat(treatyTransactionCashes).isNotNull();
  }

  @Test
  public void getAllMixedIdData_retreiveAllMixedIdSuccessful() throws URISyntaxException {

    when(mockTreatyTransactionCashRepository.findAllByMixTransactionsAndIsActive(1,1)).thenReturn(list);

    ResponseEntity<List<TreatyTransactionCash>> responseEntity = resource.getAllByMixId(1);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllMixedIdAndTreatyData_retreiveAllMixedAndTreatySuccessful() throws URISyntaxException {

    when(mockTreatySubtransactionCashRepository.findAllByTreatyIdAndIsActiveOrderByIdDesc(ID,1)).thenReturn(subtransactions);

    ResponseEntity<List<TreatyTransactionCash>> responseEntity = resource.getAllByMixIdAndTreatyId(1, ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteTreatyTransactionCash(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockTreatyTransactionCashRepository).delete(ID);
    verify(mockTreatyTransactionCashSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockTreatyTransactionCashSearchRepository.search(any(QueryBuilder.class), eq(pageable))).thenReturn(trackingPage);

    ResponseEntity<List<TreatyTransactionCash>> response = resource.searchTreatyTransactionCashs(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }



}
