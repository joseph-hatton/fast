package com.rgare.global.finre.web.rest;

import com.google.common.collect.Sets;
import com.rgare.global.finre.domain.Count;
import com.rgare.global.finre.domain.TreatyTracking;
import com.rgare.global.finre.repository.TreatyTrackingRepository;
import com.rgare.global.finre.repository.search.TreatyTrackingSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TreatyTrackingResourceUnitTest {

  private static final long ID = 42;
  private static final Integer START_YEAR = 2017;
  private static final Integer START_MONTH = 1;
  private static final Integer START_DAY = 2;
  private static final Integer END_YEAR = 2017;
  private static final Integer END_MONTH = 3;
  private static final Integer END_DAY = 4;
  private static final LocalDate START_DATE = LocalDate.of(START_YEAR, START_MONTH, START_DAY);
  private static final LocalDate END_DATE = LocalDate.of(END_YEAR, END_MONTH, END_DAY);
  private static final String X_FINRE_APP_ALERT = "X-finreApp-alert";
  private static final String X_FINRE_APP_PARAMS = "X-finreApp-params";
  private static final String[] ALERT_HEADERS = {X_FINRE_APP_ALERT, X_FINRE_APP_PARAMS};
  private static final String LINK = "Link";
  private static final String X_TOTAL_COUNT = "X-Total-Count";
  private static final String X_FINRE_APP_ERROR = "X-finreApp-error";
  private static final String[] ERROR_HEADERS = {X_FINRE_APP_ERROR, X_FINRE_APP_PARAMS};

  @Mock
  private TreatyTrackingRepository mockTreatyTrackingRepository;

  @Mock
  private TreatyTrackingSearchRepository mockTreatyTrackingSearchRepository;

  private TreatyTracking treatyTrackingInboundWithoutId;
  private TreatyTracking treatyTrackingInboundWithId;
  private TreatyTracking treatyTrackingOutbound;
  private Pageable pageable;

  private TreatyTrackingResource resource;
  private List<TreatyTracking> treatyTrackings;
  private PageImpl<TreatyTracking> trackingPage;
  private ArgumentCaptor<LocalDate> startCaptor;
  private ArgumentCaptor<LocalDate> endCaptor;
  private LocalDate nowLocalDate;

  @Before
  public void setup() {
    resource = new TreatyTrackingResource(mockTreatyTrackingRepository, mockTreatyTrackingSearchRepository);

    treatyTrackingInboundWithoutId = new TreatyTracking();
    treatyTrackingOutbound = new TreatyTracking();
    treatyTrackingOutbound.setId(ID);
    treatyTrackingInboundWithId = new TreatyTracking();
    treatyTrackingInboundWithId.setId(ID);

    pageable = new PageRequest(1, 5);
    treatyTrackings = Arrays.asList(treatyTrackingOutbound);
    trackingPage = new PageImpl<>(treatyTrackings);

    startCaptor = ArgumentCaptor.forClass(LocalDate.class);
    endCaptor = ArgumentCaptor.forClass(LocalDate.class);
    nowLocalDate = LocalDate.now();
  }



  @Test
  public void createTreatyTracking_idAlreadyExists() throws URISyntaxException {
    ResponseEntity<TreatyTracking> response = resource.createTreatyTracking(treatyTrackingInboundWithId);

    verifyZeroInteractions(mockTreatyTrackingRepository, mockTreatyTrackingSearchRepository);
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertHeaders(response.getHeaders(), ERROR_HEADERS, "error.idexists", "treatyTracking");
  }

  @Test
  public void createTreatyTracking_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.save(treatyTrackingInboundWithoutId)).thenReturn(treatyTrackingOutbound);

    ResponseEntity response = resource.createTreatyTracking(treatyTrackingInboundWithoutId);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    verify(mockTreatyTrackingSearchRepository).save(treatyTrackingOutbound);
  }


  @Test
  public void updateTreatyTracking_missingIdExecutesCreateTreatyTracking() throws URISyntaxException {
    when(mockTreatyTrackingRepository.save(treatyTrackingInboundWithoutId)).thenReturn(treatyTrackingOutbound);

    ResponseEntity<TreatyTracking> response = resource.updateTreatyTracking(treatyTrackingInboundWithoutId);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    verify(mockTreatyTrackingSearchRepository).save(treatyTrackingOutbound);
  }

  @Test
  public void updateTreatyTracking_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.save(treatyTrackingInboundWithId)).thenReturn(treatyTrackingOutbound);

    ResponseEntity<TreatyTracking> response = resource.updateTreatyTracking(treatyTrackingInboundWithId);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockTreatyTrackingSearchRepository).save(treatyTrackingOutbound);
    assertThat(response.getHeaders().keySet()).isEqualTo(Sets.newHashSet(X_FINRE_APP_ALERT, X_FINRE_APP_PARAMS));
    assertHeaders(response.getHeaders(), ALERT_HEADERS, "finreApp.treatyTracking.updated", Long.toString(ID));

  }

  @Test
  public void getAllTreatyTracking_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findAll(pageable)).thenReturn(trackingPage);

    ResponseEntity<List<TreatyTracking>> response = resource.getAllTreatyTrackings(pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertHasPaginationHeader(response.getHeaders(), "/api/treaty-trackings?page=0&size=0");
  }

  @Test
  public void getTreatyTracking_notFound() {
    when(mockTreatyTrackingRepository.findOne(ID)).thenReturn(null);

    ResponseEntity<TreatyTracking> response = resource.getTreatyTracking(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
  }

  @Test
  public void getTreatyTracking_successful() {
    when(mockTreatyTrackingRepository.findOne(ID)).thenReturn(treatyTrackingOutbound);

    ResponseEntity<TreatyTracking> response = resource.getTreatyTracking(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackingOutbound);
  }

  @Test
  public void getAllByTreatyId_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findAllByTreatyIdAndIsActive(ID, pageable, 1)).thenReturn(trackingPage);

    ResponseEntity<List<TreatyTracking>> response = resource.getAllByTreatyId(ID, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertHasPaginationHeader(response.getHeaders(), "/api/treaty-trackings/treaty?page=0&size=0");
  }

  @Test
  public void getAllByTreatyIdAndTrackingStatus_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findAllByTreatyIdAndIsActiveAndTrackingStatus(ID, pageable, 1, "Open")).thenReturn(trackingPage);

    ResponseEntity<List<TreatyTracking>> response = resource.getAllByTreatyIdAndTrackingStatus(ID, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertHasPaginationHeader(response.getHeaders(), "/api/treaty-trackings/Open-Tracking?page=0&size=0");
  }

  @Test
  public void countData_getCountSuccessful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.countByIsActiveAndTreatyId(1,ID)).thenReturn(ID);

    ResponseEntity<Count> count = resource.getCountByTreaty(ID);

    assertThat(count.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllByReportingMonthYearDates_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findByTreatyIdAndIsActiveAndReportingMonthYearBetween(ID, 1, START_DATE, END_DATE, pageable)).thenReturn(trackingPage);

    ResponseEntity<List<TreatyTracking>> response = resource.getAllByReportingMonthYearDates(ID, START_YEAR, START_MONTH, START_DAY, END_YEAR, END_MONTH, END_DAY, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertHasPaginationHeader(response.getHeaders(), "/api/treaty-trackings/dates/?page=0&size=0");
  }

  @Test
  public void getAllByCalculatedAssumingGraceDate_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findByIsActiveAndCalculatedAssumingGraceDateBetween(eq(1), startCaptor.capture(), eq(END_DATE))).thenReturn(treatyTrackings);

    ResponseEntity<List<TreatyTracking>> response = resource.getAllByCalculatedAssumingGraceDate(END_YEAR, END_MONTH, END_DAY);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertAreDaysApart(startCaptor, nowLocalDate, 0);
  }

  @Test
  public void getAllByCalculatedReportingDueDate_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findByIsActiveAndTrackingStatusAndCalculatedReportingDueDateBetweenAndReportingReceivedDateIsNullAndReportNotifFlag(eq(1), eq("Open"), startCaptor.capture(), eq(END_DATE), eq(0))).thenReturn(treatyTrackings);

    ResponseEntity<List<TreatyTracking>> response = resource.getAllByCalculatedReportingDueDate(END_YEAR, END_MONTH, END_DAY);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertAreDaysApart(startCaptor, nowLocalDate, 0);

  }

  @Test
  public void getAllBySeriatimDate_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findByIsActiveAndCalculatedSeriatimDueDateBetween(eq(1), startCaptor.capture(), endCaptor.capture())).thenReturn(treatyTrackings);

    ResponseEntity<List<TreatyTracking>> response = resource.getAllBySeriatimDate();

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertAreDaysApart(startCaptor, endCaptor, 7);
  }

  @Test
  public void getAllByCalculateAssumingDateBetween_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findByIsActiveAndTrackingStatusAndCashDueFlagAndCalculatedAssumingDateBetweenAndCashDueTypeAndCashSentRecDateIsNull(eq(1), eq("Open"), eq("Yes"), startCaptor.capture(), endCaptor.capture(), eq("Assuming"))).thenReturn(treatyTrackings);

    ResponseEntity<List<TreatyTracking>> response = resource.getAllByCalculateAssumingDateBetween();

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertAreDaysApart(startCaptor, nowLocalDate, 0);
    assertAreDaysApart(startCaptor, endCaptor, 7);
  }

  @Test
  public void getAllByCalculateAssumingDateEarly_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findByIsActiveAndTrackingStatusAndCashDueFlagAndCalculatedAssumingDateBeforeAndCalculatedAssumingGraceDateAfterAndCashDueTypeAndCashSentRecDateIsNull(eq(1), eq("Open"), eq("Yes"), startCaptor.capture(), endCaptor.capture(), eq("Assuming"))).thenReturn(treatyTrackings);

    ResponseEntity<List<TreatyTracking>> response = resource.getAllByCalculateAssumingDateEarly();

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertAreDaysApart(startCaptor, nowLocalDate, 0);
    assertAreDaysApart(startCaptor, nowLocalDate, 0);
  }

  @Test
  public void getAllByCalculateAssumingDatePastDue_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findByIsActiveAndTrackingStatusAndCashDueFlagAndCalculatedAssumingGraceDateBeforeAndCashDueTypeAndCashSentRecDateIsNull(eq(1), eq("Open"), eq("Yes"), startCaptor.capture(), eq("Assuming"))).thenReturn(treatyTrackings);

    ResponseEntity<List<TreatyTracking>> response = resource.getAllByCalculateAssumingDatePastDue();

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertAreDaysApart(startCaptor, nowLocalDate, -1);
  }

  @Test
  public void getAllBySeriatimDateExpired_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findByIsActiveAndTrackingStatusAndCalculatedSeriatimDueDateBefore(eq(1), eq("Open"), startCaptor.capture())).thenReturn(treatyTrackings);

    ResponseEntity<List<TreatyTracking>> response = resource.getAllBySeriatimDateExpired();

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertAreDaysApart(startCaptor, nowLocalDate, 0);
  }

  @Test
  public void getAllExpiredAssuming_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findByIsActiveAndTrackingStatusAndCalculatedAssumingGraceDateBefore(eq(1), eq("Open"), startCaptor.capture())).thenReturn(treatyTrackings);

    ResponseEntity<List<TreatyTracking>> response = resource.getAllExpiredAssuming();

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertAreDaysApart(startCaptor, nowLocalDate, 0);
  }

  @Test
  public void getAllExpiredCeding_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findByIsActiveAndTrackingStatusAndCalculatedCedingGraceDateBefore(eq(1), eq("Open"), startCaptor.capture())).thenReturn(treatyTrackings);

    ResponseEntity<List<TreatyTracking>> response = resource.getAllExpiredCeding();

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertAreDaysApart(startCaptor, nowLocalDate, 0);
  }

  @Test
  public void getAllExpiredReporting_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findByIsActiveAndTrackingStatusAndCalculatedReportingDueDateBeforeAndCalculatedAssumingGraceDateAfterAndReportingReceivedDateIsNullAndReportNotifFlag(eq(1), eq("Open"), startCaptor.capture(), startCaptor.capture(), eq(0))).thenReturn(treatyTrackings);

    ResponseEntity<List<TreatyTracking>> response = resource.getAllExpiredReporting();

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertAreDaysApart(startCaptor, nowLocalDate, 0);
  }

  @Test
  public void getAllByCalculatedAssumingGraceDate_nonHttp_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findByTreatyIdAndIsActiveAndReportingMonthYear(eq(ID), eq(1), startCaptor.capture())).thenReturn(treatyTrackings);

    List<TreatyTracking> response = resource.getAllByCalculatedAssumingGraceDate(ID, END_YEAR, END_MONTH, END_DAY);

    assertThat(response).isEqualTo(treatyTrackings);
    assertAreDaysApart(startCaptor, END_DATE, 0);
  }

  @Test
  public void getAllByCalculatedCedingGraceDate_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findByIsActiveAndCalculatedCedingGraceDateBetween(eq(1), startCaptor.capture(), endCaptor.capture())).thenReturn(treatyTrackings);

    ResponseEntity<List<TreatyTracking>> response = resource.getAllByCalculatedCedingGraceDate(END_YEAR, END_MONTH, END_DAY);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertAreDaysApart(startCaptor, nowLocalDate, 0);
    assertAreDaysApart(endCaptor, END_DATE, 0);
  }


  @Test
  public void getAllByLastReportingMonthYear_successful() throws URISyntaxException {
    when(mockTreatyTrackingRepository.findByIsActiveAndReportingMonthYearBeforeAndTreatyId(eq(1), endCaptor.capture(), eq(ID))).thenReturn(treatyTrackings);

    ResponseEntity<List<TreatyTracking>> response = resource.getAllByLastReportingMonthYear(END_YEAR, END_MONTH, END_DAY, ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertAreDaysApart(endCaptor, END_DATE, 0);
  }

  @Test
  public void deleteTreatyTracking_successful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteTreatyTracking(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockTreatyTrackingRepository).delete(ID);
    verify(mockTreatyTrackingSearchRepository).delete(ID);
    assertHeaders(response.getHeaders(), ALERT_HEADERS, "finreApp.treatyTracking.deleted", Long.toString(ID));
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockTreatyTrackingSearchRepository.search(any(QueryBuilder.class), eq(pageable))).thenReturn(trackingPage);

    ResponseEntity<List<TreatyTracking>> response = resource.searchTreatyTrackings(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(treatyTrackings);
    assertHasPaginationHeader(response.getHeaders(), "/api/_search/treaty-trackings?page=0&size=0&query=a=b");
  }

  private void assertAreDaysApart(ArgumentCaptor<LocalDate> startCaptor, ArgumentCaptor<LocalDate> endCaptor, int expectedDaysApart) {
    assertThat(startCaptor.getValue().until(endCaptor.getValue(), DAYS)).isEqualTo(expectedDaysApart);
  }

  private void assertAreDaysApart(ArgumentCaptor<LocalDate> startCaptor, LocalDate endDate, int expectedDaysApart) {
    assertThat(startCaptor.getValue().until(endDate, DAYS)).isEqualTo(expectedDaysApart);
  }

  private void assertHeaders(HttpHeaders headers, String[] expectedHeaders, String expectedHeaderOneValue, String expectedHeaderTwoValue) {
    assertThat(headers.keySet()).isEqualTo(Sets.newHashSet(expectedHeaders));
    assertThat(headers.get(expectedHeaders[0]).size()).isEqualTo(1);
    assertThat(headers.get(expectedHeaders[0]).get(0)).isEqualTo(expectedHeaderOneValue);
    assertThat(headers.get(expectedHeaders[1]).size()).isEqualTo(1);
    assertThat(headers.get(expectedHeaders[1]).get(0)).isEqualTo(expectedHeaderTwoValue);
  }


  private void assertHasPaginationHeader(HttpHeaders headers, final String queryUrl) {
    assertThat(headers.keySet()).isEqualTo(Sets.newHashSet(LINK, X_TOTAL_COUNT));
    assertThat(headers.get(LINK).size()).isEqualTo(1);
    assertThat(headers.get(LINK).get(0)).isEqualTo("<" + queryUrl + ">; rel=\"last\",<" + queryUrl + ">; rel=\"first\"");
    assertThat(headers.get(X_TOTAL_COUNT).size()).isEqualTo(1);
    assertThat(headers.get(X_TOTAL_COUNT).get(0)).isEqualTo("1");
  }
}
