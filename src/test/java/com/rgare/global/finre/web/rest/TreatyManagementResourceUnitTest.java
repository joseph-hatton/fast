package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.*;
import com.rgare.global.finre.repository.AuditRiskFeeRepository;
import com.rgare.global.finre.repository.AuditTreatyRepository;
import com.rgare.global.finre.repository.TreatyManagementRepository;
import com.rgare.global.finre.repository.search.TreatyManagementSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TreatyManagementResourceUnitTest {

  private static final Long ID = 100L;
  private static final String COMMENT_YEAR = "COMMENT_YEAR";
  private static final String COMMENT = "COMMENT";

  @Mock
  private TreatyManagementRepository mockTreatyManagementRepository;

  @Mock
  private TreatyManagementSearchRepository mockTreatyManagementSearchRepository;

  @Mock
  private AuditTreatyRepository mockAuditTreatyRepository;

  @Mock
  private AuditRiskFeeRepository mockAuditRiskFeeRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private TreatyManagement treatyManagement;

  private TreatyManagement result;

  private TreatyManagementResource resource;

  private Pageable pageable;
  private PageImpl<TreatyManagement> trackingPage;

  private List<TreatyManagement> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new TreatyManagementResource(mockTreatyManagementRepository,
      mockTreatyManagementSearchRepository,
      mockAuditTreatyRepository,
      mockAuditRiskFeeRepository);
    treatyManagement = new TreatyManagement();
    treatyManagement.setRefundComments(COMMENT_YEAR);
    treatyManagement.setRecaptureComment(COMMENT);

    result = new TreatyManagement();
    result.setId(ID);
    result.setRefundComments(COMMENT_YEAR);
    result.setRecaptureComment(COMMENT);
    Treaty treaty = new Treaty();
    treaty.setTreatyStatus(new TreatyStatus());
    result.setTreaty(treaty);

    Set<RiskFee> riskFees = new HashSet<>();
    RiskFee riskFee = new RiskFee();
    riskFees.add(riskFee);
    result.setRiskFees(riskFees);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(treatyManagement);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    TreatyManagement savedTreatyManagement = new TreatyManagement();
    savedTreatyManagement = new TreatyManagement();
    savedTreatyManagement.setRefundComments(COMMENT_YEAR);
    savedTreatyManagement.setRecaptureComment(COMMENT);
    Treaty treaty = new Treaty();
    treaty.setTreatyStatus(new TreatyStatus());
    savedTreatyManagement.setTreaty(treaty);

    when(mockTreatyManagementRepository.save(savedTreatyManagement)).thenReturn(result);

    ResponseEntity<TreatyManagement> responseEntity = resource.createTreatyManagement(savedTreatyManagement);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    TreatyManagement savedTreatyManagement = new TreatyManagement();
    savedTreatyManagement = new TreatyManagement();
    savedTreatyManagement.setId(ID);
    savedTreatyManagement.setRefundComments(COMMENT_YEAR);
    savedTreatyManagement.setRecaptureComment(COMMENT);

    ResponseEntity<TreatyManagement> responseEntity = resource.createTreatyManagement(savedTreatyManagement);

    verifyZeroInteractions(mockTreatyManagementRepository, mockTreatyManagementSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void saveData_saveWhereIDIsNullSuccessful() throws URISyntaxException {
    TreatyManagement savedTreatyManagement = new TreatyManagement();
    savedTreatyManagement = new TreatyManagement();
    savedTreatyManagement.setId(null);
    savedTreatyManagement.setRefundComments(COMMENT_YEAR);
    savedTreatyManagement.setRecaptureComment(COMMENT);
    savedTreatyManagement.setLastUser("LAST");
    savedTreatyManagement.setInputStatus("INPUT");

    Treaty treaty = new Treaty();
    TreatyStatus treatyStatus = new TreatyStatus();
    treatyStatus.setName("NAME");
    treaty.setTreatyStatus(treatyStatus);
    savedTreatyManagement.setTreaty(treaty);

    when(mockTreatyManagementRepository.save(savedTreatyManagement)).thenReturn(result);

    ResponseEntity<TreatyManagement> responseEntity = resource.createTreatyManagement(savedTreatyManagement);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    TreatyManagement savedTreatyManagement = new TreatyManagement();
    savedTreatyManagement = new TreatyManagement();
    savedTreatyManagement.setId(ID);
    savedTreatyManagement.setRefundComments(COMMENT_YEAR);
    savedTreatyManagement.setRecaptureComment(COMMENT);
    Treaty treaty = new Treaty();
    treaty.setTreatyStatus(new TreatyStatus());
    savedTreatyManagement.setTreaty(treaty);

    when(mockTreatyManagementRepository.save(savedTreatyManagement)).thenReturn(result);
    when(mockTreatyManagementRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<TreatyManagement> responseEntity = resource.updateTreatyManagement(savedTreatyManagement);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    TreatyManagement savedTreatyManagement = new TreatyManagement();
    savedTreatyManagement = new TreatyManagement();
    savedTreatyManagement.setId(null);
    savedTreatyManagement.setRefundComments(COMMENT_YEAR);
    savedTreatyManagement.setRecaptureComment(COMMENT);
    Treaty treaty = new Treaty();
    treaty.setTreatyStatus(new TreatyStatus());
    savedTreatyManagement.setTreaty(treaty);

    when(mockTreatyManagementRepository.save(savedTreatyManagement)).thenReturn(result);
    when(mockTreatyManagementRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<TreatyManagement> responseEntity = resource.updateTreatyManagement(savedTreatyManagement);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockTreatyManagementRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<TreatyManagement> responseEntity = resource.getTreatyManagement(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getOneData_retreiveOneByTreatySuccessful() throws URISyntaxException {

    when(mockTreatyManagementRepository.findOneByTreatyId(ID)).thenReturn(result);

    ResponseEntity<TreatyManagement> responseEntity = resource.getTreatyManagementByTreatyId(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    when(mockTreatyManagementRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<Void> response = resource.deleteTreatyManagement(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockTreatyManagementRepository).delete(ID);
    verify(mockTreatyManagementSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockTreatyManagementSearchRepository.search(any(QueryBuilder.class), eq(pageable))).thenReturn(trackingPage);

    ResponseEntity<List<TreatyManagement>> response = resource.searchTreatyManagements(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }



}
