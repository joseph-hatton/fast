package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.Company;
import com.rgare.global.finre.repository.CompanyRepository;
import com.rgare.global.finre.repository.search.CompanySearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CompanyResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private CompanyRepository mockCompanyRepository;

  @Mock
  private CompanySearchRepository mockCompanySearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private Company company;

  private Company result;

  private CompanyResource resource;

  private Pageable pageable;
  private PageImpl<Company> trackingPage;

  private List<Company> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new CompanyResource(mockCompanyRepository, mockCompanySearchRepository, mockCDp3LogService);
    company = new Company();
    company.setName(NAME);
    company.setLegalName(DESCRIPTION);

    result = new Company();
    result.setId(ID);
    result.setName(NAME);
    result.setLegalName(DESCRIPTION);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(company);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    Company savedCompany = new Company();
    savedCompany = new Company();
    savedCompany.setName(NAME);
    savedCompany.setLegalName(DESCRIPTION);

    when(mockCompanyRepository.save(savedCompany)).thenReturn(result);

    ResponseEntity<Company> responseEntity = resource.createCompany(savedCompany);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    Company savedCompany = new Company();
    savedCompany = new Company();
    savedCompany.setId(ID);
    savedCompany.setName(NAME);
    savedCompany.setLegalName(DESCRIPTION);

    ResponseEntity<Company> responseEntity = resource.createCompany(savedCompany);

    verifyZeroInteractions(mockCompanyRepository, mockCompanySearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    Company savedCompany = new Company();
    savedCompany = new Company();
    savedCompany.setId(ID);
    savedCompany.setName(NAME);
    savedCompany.setLegalName(DESCRIPTION);

    when(mockCompanyRepository.save(savedCompany)).thenReturn(result);

    ResponseEntity<Company> responseEntity = resource.updateCompany(savedCompany);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    Company savedCompany = new Company();
    savedCompany = new Company();
    savedCompany.setId(null);
    savedCompany.setName(NAME);
    savedCompany.setLegalName(DESCRIPTION);

    when(mockCompanyRepository.save(savedCompany)).thenReturn(result);

    ResponseEntity<Company> responseEntity = resource.updateCompany(savedCompany);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockCompanyRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<Company> responseEntity = resource.getCompany(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllCompaniesSuccessful() throws URISyntaxException {

    when(mockCompanyRepository.findAll(pageable)).thenReturn(trackingPage);

    ResponseEntity<List<Company>> response = resource.getAllCompanies(pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllMDMData_retreiveAllMDMCompaniesSuccessful() throws URISyntaxException {

    when(mockCompanyRepository.searchAllMDMCompaniesQuery(pageable)).thenReturn(trackingPage);

    ResponseEntity<List<Company>> response = resource.getAllMDMCompanies(pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }


  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteCompany(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockCompanyRepository).delete(ID);
    verify(mockCompanySearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchCompanies_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockCompanySearchRepository.search(any(QueryBuilder.class), eq(pageable))).thenReturn(trackingPage);

    ResponseEntity<List<Company>> response = resource.searchCompanies(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }

  @Test
  public void searchByCompanyName_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockCompanyRepository.searchCompaniesQuery(query, pageable)).thenReturn(trackingPage);

    ResponseEntity<List<Company>> response = resource.searchCompaniesByName(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }

  @Test
  public void searchByRGACompany_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockCompanyRepository.findByIsRgaCompany(1)).thenReturn(list);

    ResponseEntity<List<Company>> response = resource.getAllRGACompanies();

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }

  @Test
  public void searchClientCompanies_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockCompanyRepository.searchClientCompanyIdIsNotNullQuery(query, pageable)).thenReturn(trackingPage);

    ResponseEntity<List<Company>> response = resource.SearchAllMDMCompanies(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }

  @Test
  public void searchExistingMdmCompanies_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockCompanyRepository.searchExistingMdmCompanyQuery(query, pageable)).thenReturn(trackingPage);

    ResponseEntity<List<Company>> response = resource.searchExistingMdmCompanyQuery(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }

  @Test
  public void getAllMDMData_getAllMDMCompaniesSuccessful() throws URISyntaxException {

    when(mockCompanySearchRepository.findByClientCompanyId(null,pageable)).thenReturn(trackingPage);

    ResponseEntity<List<Company>> response = resource.searchMdmCompanies(pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllNonMDMData_retreiveAllNonMDMCompaniesSuccessful() throws URISyntaxException {

    when(mockCompanyRepository.searchAllNonMDMCompaniesQuery(pageable)).thenReturn(trackingPage);

    ResponseEntity<List<Company>> response = resource.getAllNonMDMCompanies(pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllClientData_retreiveClientCompanyIsNullSuccessful() throws URISyntaxException {
    String query = "a=b";

    when(mockCompanyRepository.searchClientCompanyIdIsNullQuery(query, pageable)).thenReturn(trackingPage);

    ResponseEntity<List<Company>> response = resource.SearchAllNonMDMCompanies(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

}
