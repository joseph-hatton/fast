package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.ProductGroup;
import com.rgare.global.finre.repository.ProductGroupRepository;
import com.rgare.global.finre.repository.search.ProductGroupSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductGroupResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private ProductGroupRepository mockProductGroupRepository;

  @Mock
  private ProductGroupSearchRepository mockProductGroupSearchRepository;

  private ProductGroup productGroup;

  private ProductGroup result;

  private ProductGroupResource resource;

  private Pageable pageable;
  private PageImpl<ProductGroup> trackingPage;

  private List<ProductGroup> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new ProductGroupResource(mockProductGroupRepository, mockProductGroupSearchRepository);
    productGroup = new ProductGroup();
    productGroup.setName(NAME);
    productGroup.setDescription(DESCRIPTION);

    result = new ProductGroup();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);


    list = Arrays.asList(productGroup);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    ProductGroup savedProductGroup = new ProductGroup();
    savedProductGroup = new ProductGroup();
    savedProductGroup.setName(NAME);
    savedProductGroup.setDescription(DESCRIPTION);

    when(mockProductGroupRepository.save(savedProductGroup)).thenReturn(result);

    ResponseEntity<ProductGroup> responseEntity = resource.createProductGroup(savedProductGroup);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    ProductGroup savedProductGroup = new ProductGroup();
    savedProductGroup = new ProductGroup();
    savedProductGroup.setId(ID);
    savedProductGroup.setName(NAME);
    savedProductGroup.setDescription(DESCRIPTION);

    ResponseEntity<ProductGroup> responseEntity = resource.createProductGroup(savedProductGroup);

    verifyZeroInteractions(mockProductGroupRepository, mockProductGroupSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    ProductGroup savedProductGroup = new ProductGroup();
    savedProductGroup = new ProductGroup();
    savedProductGroup.setId(ID);
    savedProductGroup.setName(NAME);
    savedProductGroup.setDescription(DESCRIPTION);

    when(mockProductGroupRepository.save(savedProductGroup)).thenReturn(result);

    ResponseEntity<ProductGroup> responseEntity = resource.updateProductGroup(savedProductGroup);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    ProductGroup savedProductGroup = new ProductGroup();
    savedProductGroup = new ProductGroup();
    savedProductGroup.setId(null);
    savedProductGroup.setName(NAME);
    savedProductGroup.setDescription(DESCRIPTION);

    when(mockProductGroupRepository.save(savedProductGroup)).thenReturn(result);

    ResponseEntity<ProductGroup> responseEntity = resource.updateProductGroup(savedProductGroup);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockProductGroupRepository.findAll()).thenReturn(list);

    List<ProductGroup> productGroups = resource.getAllProductGroups();

    assertThat(productGroups).isNotNull();
    assertThat(productGroups.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockProductGroupRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<ProductGroup> responseEntity = resource.getProductGroup(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteProductGroup(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockProductGroupRepository).delete(ID);
    verify(mockProductGroupSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockProductGroupSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<ProductGroup> response = resource.searchProductGroups(query);

    assertThat(response).isNotNull();
  }



}
