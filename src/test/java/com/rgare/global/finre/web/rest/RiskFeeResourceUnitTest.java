package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.RiskFee;
import com.rgare.global.finre.repository.AuditRiskFeeRepository;
import com.rgare.global.finre.repository.RiskFeeRepository;
import com.rgare.global.finre.repository.search.RiskFeeSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RiskFeeResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private RiskFeeRepository mockRiskFeeRepository;

  @Mock
  private AuditRiskFeeRepository mockAuditRiskFeeRepository;

  @Mock
  private RiskFeeSearchRepository mockRiskFeeSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private RiskFee riskFee;

  private RiskFee result;

  private RiskFeeResource resource;

  private Pageable pageable;
  private PageImpl<RiskFee> trackingPage;

  private List<RiskFee> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new RiskFeeResource(mockRiskFeeRepository, mockRiskFeeSearchRepository, mockAuditRiskFeeRepository);
    riskFee = new RiskFee();
    riskFee.setComments(NAME);
    riskFee.setPeriod(DESCRIPTION);

    result = new RiskFee();
    result.setId(ID);
    result.setComments(NAME);
    result.setPeriod(DESCRIPTION);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(riskFee);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    RiskFee savedRiskFee = new RiskFee();
    savedRiskFee = new RiskFee();
    savedRiskFee.setComments(NAME);
    savedRiskFee.setPeriod(DESCRIPTION);

    when(mockRiskFeeRepository.save(savedRiskFee)).thenReturn(result);

    ResponseEntity<RiskFee> responseEntity = resource.createRiskFee(savedRiskFee);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    RiskFee savedRiskFee = new RiskFee();
    savedRiskFee = new RiskFee();
    savedRiskFee.setId(ID);
    savedRiskFee.setComments(NAME);
    savedRiskFee.setPeriod(DESCRIPTION);

    ResponseEntity<RiskFee> responseEntity = resource.createRiskFee(savedRiskFee);

    verifyZeroInteractions(mockRiskFeeRepository, mockRiskFeeSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    RiskFee savedRiskFee = new RiskFee();
    savedRiskFee = new RiskFee();
    savedRiskFee.setId(ID);
    savedRiskFee.setComments(NAME);
    savedRiskFee.setPeriod(DESCRIPTION);

    when(mockRiskFeeRepository.save(savedRiskFee)).thenReturn(result);

    ResponseEntity<RiskFee> responseEntity = resource.updateRiskFee(savedRiskFee);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    RiskFee savedRiskFee = new RiskFee();
    savedRiskFee = new RiskFee();
    savedRiskFee.setId(null);
    savedRiskFee.setComments(NAME);
    savedRiskFee.setPeriod(DESCRIPTION);

    when(mockRiskFeeRepository.save(savedRiskFee)).thenReturn(result);

    ResponseEntity<RiskFee> responseEntity = resource.updateRiskFee(savedRiskFee);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockRiskFeeRepository.findAll()).thenReturn(list);

    List<RiskFee> riskFees = resource.getAllRiskFees();

    assertThat(riskFees).isNotNull();
    assertThat(riskFees.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockRiskFeeRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<RiskFee> responseEntity = resource.getRiskFee(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteRiskFee(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockRiskFeeRepository).delete(ID);
    verify(mockRiskFeeSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockRiskFeeSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<RiskFee> response = resource.searchRiskFees(query);

    assertThat(response).isNotNull();
  }



}
