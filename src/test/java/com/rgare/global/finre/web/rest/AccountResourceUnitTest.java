package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.PersistentToken;
import com.rgare.global.finre.domain.User;
import com.rgare.global.finre.repository.PersistentTokenRepository;
import com.rgare.global.finre.repository.UserRepository;
import com.rgare.global.finre.security.SecurityUtils;
import com.rgare.global.finre.service.MailService;
import com.rgare.global.finre.service.UserService;
import com.rgare.global.finre.service.dto.UserDTO;
import com.rgare.global.finre.web.rest.vm.KeyAndPasswordVM;
import com.rgare.global.finre.web.rest.vm.ManagedUserVM;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountResourceUnitTest {

  private static final String USER_KEY = "test.user";
  private static final String USER_EMAIL = "test.user@rgare.com";
  private static final String USER_PASSWORD = "secret secret i've got a secret";
  private static final String USER_SERIES = "some series";

  @Mock
  private UserRepository mockUserRepository;

  @Mock
  private UserService mockUserService;

  @Mock
  private MailService mockMailService;

  @Mock
  private PersistentTokenRepository mockPersistentTokenRepository;

  @Mock
  private SecurityUtils mockSecurityUtils;

  @Mock
  private HttpServletRequest mockHttpServletRequest;

  private User user;
  private KeyAndPasswordVM keyAndPasswordVM;
  private PersistentToken persistentToken;
  private ManagedUserVM vm;

  private AccountResource resource;

  @Before
  public void setup() {
    resource = new AccountResource(mockUserRepository, mockUserService, mockMailService, mockPersistentTokenRepository, mockSecurityUtils);
    user = new User();
    user.setId(42l);
    user.setLogin(USER_EMAIL);
    user.setFirstName("test");
    user.setLastName("user");
    user.setEmail(USER_EMAIL);

    keyAndPasswordVM = new KeyAndPasswordVM();
    keyAndPasswordVM.setNewPassword(USER_PASSWORD);
    keyAndPasswordVM.setKey(USER_KEY);

    persistentToken = new PersistentToken();
    persistentToken.setSeries(USER_SERIES);

    vm = new ManagedUserVM(user.getId(), user.getLogin(), user.getPassword(), user.getFirstName(), user.getLastName(), user.getEmail(),true, user.getImageUrl(), user.getLangKey(), user.getCreatedBy(), user.getCreatedDate(), user.getLastModifiedBy(), user.getLastModifiedDate(), null);
    vm.setLogin(user.getLogin());
  }

  @Test
  public void registerAccount_loginAlreadyExists() {
    when(mockUserRepository.findOneByLogin(user.getLogin())).thenReturn(Optional.of(user));

    ResponseEntity responseEntity = resource.registerAccount(vm);

    verifyZeroInteractions(mockUserService, mockMailService);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(responseEntity.getBody()).isEqualTo("login already in use");
  }

  @Test
  public void registerAccount_emailAlreadyExists() {
    when(mockUserRepository.findOneByLogin(user.getLogin())).thenReturn(Optional.empty());
    when(mockUserRepository.findOneByEmail(user.getEmail())).thenReturn(Optional.of(user));

    ResponseEntity responseEntity = resource.registerAccount(vm);

    verifyZeroInteractions(mockUserService, mockMailService);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(responseEntity.getBody()).isEqualTo("email address already in use");
  }

  @Test
  public void registerAccount_successful() {
    when(mockUserRepository.findOneByLogin(user.getLogin())).thenReturn(Optional.empty());
    when(mockUserRepository.findOneByEmail(user.getEmail())).thenReturn(Optional.empty());
    when(mockUserService.createUser(vm.getLogin(), vm.getPassword(), vm.getFirstName(), vm.getLastName(), vm.getEmail().toLowerCase(), vm.getImageUrl(), vm.getLangKey())).thenReturn(user);

    ResponseEntity responseEntity = resource.registerAccount(vm);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    verify(mockMailService).sendActivationEmail(user);
  }

  @Test
  public void activateAccount_notValidUser() {
    when(mockUserService.activateRegistration(USER_KEY)).thenReturn(Optional.empty());

    ResponseEntity<String> responseEntity = resource.activateAccount(USER_KEY);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @Test
  public void activateAccount_successful() {
    when(mockUserService.activateRegistration(USER_KEY)).thenReturn(Optional.of(user));

    ResponseEntity<String> responseEntity = resource.activateAccount(USER_KEY);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void isAuthenticated() {
    when(mockHttpServletRequest.getRemoteUser()).thenReturn(USER_EMAIL);

    String remoteUser = resource.isAuthenticated(mockHttpServletRequest);

    assertThat(remoteUser).isEqualTo(USER_EMAIL);
  }

  @Test
  public void getAccount_notAValidUser() {
    when(mockUserService.getUserWithAuthorities()).thenReturn(null);

    ResponseEntity<UserDTO> responseEntity = resource.getAccount();

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @Test
  public void getAccount_successful() {
    when(mockUserService.getUserWithAuthorities()).thenReturn(user);

    ResponseEntity<UserDTO> responseEntity = resource.getAccount();

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(responseEntity.getBody().getId()).isEqualTo(user.getId());
    assertThat(responseEntity.getBody().getFirstName()).isEqualTo(user.getFirstName());
    assertThat(responseEntity.getBody().getLastName()).isEqualTo(user.getLastName());
  }


  @Test
  @Ignore
  public void saveAccount_emailExists() {
    UserDTO userDto = new UserDTO(user);
    when(mockUserRepository.findOneByEmail(USER_EMAIL)).thenReturn(Optional.of(user));

    ResponseEntity responseEntity = resource.saveAccount(userDto);

    verifyZeroInteractions(mockUserService);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(responseEntity.getBody()).isEqualTo("Email already in use");
  }

  @Test
  public void saveAccount_userDoesntExist() {
    UserDTO userDto = new UserDTO(user);
    when(mockUserRepository.findOneByEmail(USER_EMAIL)).thenReturn(Optional.empty());
    when(mockSecurityUtils.getCurrentUserLogin()).thenReturn(USER_EMAIL);
    when(mockUserRepository.findOneByLogin(user.getLogin())).thenReturn(Optional.empty());

    ResponseEntity responseEntity = resource.saveAccount(userDto);

    verifyZeroInteractions(mockUserService);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @Test
  public void saveAccount_successful() {
    UserDTO userDto = new UserDTO(user);
    when(mockUserRepository.findOneByEmail(USER_EMAIL)).thenReturn(Optional.empty());
    when(mockSecurityUtils.getCurrentUserLogin()).thenReturn(USER_EMAIL);
    when(mockUserRepository.findOneByLogin(user.getLogin())).thenReturn(Optional.of(user));

    ResponseEntity responseEntity = resource.saveAccount(userDto);

    verify(mockUserService).updateUser(user.getFirstName(), user.getLastName(), user.getEmail(), user.getLangKey(), user.getImageUrl());
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void changePassword_invalidLength() {
    ResponseEntity responseEntity = resource.changePassword("hi");

    verifyZeroInteractions(mockUserService);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(responseEntity.getBody()).isEqualTo("Incorrect password");
  }

  @Test
  public void changePassword_successful() {
    final String password = "NEW PASSWORD";

    ResponseEntity responseEntity = resource.changePassword(password);

    verify(mockUserService).changePassword(password);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getCurrentSessions_notLoggedIn() {
    when(mockSecurityUtils.getCurrentUserLogin()).thenReturn(USER_EMAIL);
    when(mockUserRepository.findOneByLogin(USER_EMAIL)).thenReturn(Optional.empty());

    ResponseEntity<List<PersistentToken>> responseEntity = resource.getCurrentSessions();

    verifyZeroInteractions(mockPersistentTokenRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @Test
  public void getCurrentSessions_successful() {
    when(mockSecurityUtils.getCurrentUserLogin()).thenReturn(USER_EMAIL);
    when(mockUserRepository.findOneByLogin(USER_EMAIL)).thenReturn(Optional.of(user));
    when(mockPersistentTokenRepository.findByUser(user)).thenReturn(Arrays.asList(persistentToken));

    ResponseEntity<List<PersistentToken>> responseEntity = resource.getCurrentSessions();

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(responseEntity.getBody()).contains(persistentToken);
  }

  @Test
  public void invalidateSession_notLoggedIn() {
    when(mockSecurityUtils.getCurrentUserLogin()).thenReturn(USER_EMAIL);
    when(mockUserRepository.findOneByLogin(USER_EMAIL)).thenReturn(Optional.empty());
    try {
      resource.invalidateSession(USER_SERIES);
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    verifyZeroInteractions(mockPersistentTokenRepository);
  }

  @Test
  public void invalidateSession_noMatchingSession() {
    final String series = "some series";
    PersistentToken persistentToken = new PersistentToken();
    persistentToken.setSeries("some other series");
    when(mockSecurityUtils.getCurrentUserLogin()).thenReturn(USER_EMAIL);
    when(mockUserRepository.findOneByLogin(USER_EMAIL)).thenReturn(Optional.of(user));
    when(mockPersistentTokenRepository.findByUser(user)).thenReturn(Arrays.asList(persistentToken));
    try {
      resource.invalidateSession(series);
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    verify(mockPersistentTokenRepository, never()).delete(series);
  }

  @Test
  public void invalidateSession_successful() {
    when(mockSecurityUtils.getCurrentUserLogin()).thenReturn(USER_EMAIL);
    when(mockUserRepository.findOneByLogin(USER_EMAIL)).thenReturn(Optional.of(user));
    when(mockPersistentTokenRepository.findByUser(user)).thenReturn(Arrays.asList(persistentToken));
    try {
      resource.invalidateSession(USER_SERIES);
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    verify(mockPersistentTokenRepository).delete(USER_SERIES);
  }

  @Test
  public void requestPasswordReset_emailNotRegistered() {
    when(mockUserService.requestPasswordReset(USER_EMAIL)).thenReturn(Optional.empty());

    ResponseEntity responseEntity = resource.requestPasswordReset(USER_EMAIL);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    verifyZeroInteractions(mockMailService);
  }

  @Test
  public void requestPasswordReset_successful() {
    when(mockUserService.requestPasswordReset(USER_EMAIL)).thenReturn(Optional.of(user));

    ResponseEntity responseEntity = resource.requestPasswordReset(USER_EMAIL);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockMailService).sendPasswordResetMail(user);
  }

  @Test
  public void finishPasswordReset_noOldPassword() {
    KeyAndPasswordVM keyAndPasswordVM = new KeyAndPasswordVM();

    ResponseEntity<String> responseEntity = resource.finishPasswordReset(keyAndPasswordVM);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(responseEntity.getBody()).isEqualTo("Incorrect password");
    verifyZeroInteractions(mockUserService);
  }

  @Test
  public void finishPasswordReset_passwordTooShort() {
    keyAndPasswordVM.setNewPassword("hi");

    ResponseEntity<String> responseEntity = resource.finishPasswordReset(keyAndPasswordVM);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(responseEntity.getBody()).isEqualTo("Incorrect password");
    verifyZeroInteractions(mockUserService);
  }

  @Test
  public void finishPasswordReset_passwordTooLong() {
    keyAndPasswordVM.setNewPassword(String.join("", Collections.nCopies(101, "a")));

    ResponseEntity<String> responseEntity = resource.finishPasswordReset(keyAndPasswordVM);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(responseEntity.getBody()).isEqualTo("Incorrect password");
    verifyZeroInteractions(mockUserService);
  }

  @Test
  public void finishPasswordReset_notAValidUser() {
    when(mockUserService.completePasswordReset(USER_PASSWORD, USER_KEY)).thenReturn(Optional.empty());

    ResponseEntity<String> responseEntity = resource.finishPasswordReset(keyAndPasswordVM);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @Test
  public void finishPasswordReset_successful() {
    when(mockUserService.completePasswordReset(USER_PASSWORD, USER_KEY)).thenReturn(Optional.of(user));

    ResponseEntity<String> responseEntity = resource.finishPasswordReset(keyAndPasswordVM);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

}
