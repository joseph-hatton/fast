package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.AllocationType;
import com.rgare.global.finre.repository.AllocationTypeRepository;
import com.rgare.global.finre.repository.search.AllocationTypeSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AllocationTypeResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private AllocationTypeRepository mockAllocationTypeRepository;

  @Mock
  private AllocationTypeSearchRepository mockAllocationTypeSearchRepository;

  private AllocationType allocationType;

  private AllocationType result;

  private AllocationTypeResource resource;

  private Pageable pageable;
  private PageImpl<AllocationType> trackingPage;

  private List<AllocationType> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new AllocationTypeResource(mockAllocationTypeRepository, mockAllocationTypeSearchRepository);
    allocationType = new AllocationType();
    allocationType.setName(NAME);
    allocationType.setDescription(DESCRIPTION);

    result = new AllocationType();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);


    list = Arrays.asList(allocationType);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    AllocationType savedAgreementType = new AllocationType();
    savedAgreementType = new AllocationType();
    savedAgreementType.setName(NAME);
    savedAgreementType.setDescription(DESCRIPTION);

    when(mockAllocationTypeRepository.save(savedAgreementType)).thenReturn(result);

    ResponseEntity<AllocationType> responseEntity = resource.createAllocationType(savedAgreementType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    AllocationType savedAgreementType = new AllocationType();
    savedAgreementType = new AllocationType();
    savedAgreementType.setId(ID);
    savedAgreementType.setName(NAME);
    savedAgreementType.setDescription(DESCRIPTION);

    ResponseEntity<AllocationType> responseEntity = resource.createAllocationType(savedAgreementType);

    verifyZeroInteractions(mockAllocationTypeRepository, mockAllocationTypeSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    AllocationType savedAgreementType = new AllocationType();
    savedAgreementType = new AllocationType();
    savedAgreementType.setId(ID);
    savedAgreementType.setName(NAME);
    savedAgreementType.setDescription(DESCRIPTION);

    when(mockAllocationTypeRepository.save(savedAgreementType)).thenReturn(result);

    ResponseEntity<AllocationType> responseEntity = resource.updateAllocationType(savedAgreementType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    AllocationType savedAgreementType = new AllocationType();
    savedAgreementType = new AllocationType();
    savedAgreementType.setId(null);
    savedAgreementType.setName(NAME);
    savedAgreementType.setDescription(DESCRIPTION);

    when(mockAllocationTypeRepository.save(savedAgreementType)).thenReturn(result);

    ResponseEntity<AllocationType> responseEntity = resource.updateAllocationType(savedAgreementType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockAllocationTypeRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<AllocationType> responseEntity = resource.getAllocationType(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockAllocationTypeRepository.findAll()).thenReturn(list);

    List<AllocationType> allocationTypes = resource.getAllAllocationTypes();

    assertThat(allocationTypes).isNotNull();
    assertThat(allocationTypes.size()).isGreaterThan(0);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteAllocationType(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockAllocationTypeRepository).delete(ID);
    verify(mockAllocationTypeSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockAllocationTypeSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<AllocationType> response = resource.searchAllocationTypes(query);

    assertThat(response).isNotNull();
  }



}
