package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.NotificationStatus;
import com.rgare.global.finre.repository.NotificationStatusRepository;
import com.rgare.global.finre.repository.search.NotificationStatusSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NotificationStatusResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private NotificationStatusRepository mockNotificationStatusRepository;

  @Mock
  private NotificationStatusSearchRepository mockNotificationStatusSearchRepository;

  private NotificationStatus notificationStatus;

  private NotificationStatus result;

  private NotificationStatusResource resource;

  private Pageable pageable;
  private PageImpl<NotificationStatus> trackingPage;

  private List<NotificationStatus> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new NotificationStatusResource(mockNotificationStatusRepository, mockNotificationStatusSearchRepository);
    notificationStatus = new NotificationStatus();
    notificationStatus.setName(NAME);
    notificationStatus.setDescription(DESCRIPTION);

    result = new NotificationStatus();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);


    list = Arrays.asList(notificationStatus);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    NotificationStatus savedNotificationStatus = new NotificationStatus();
    savedNotificationStatus = new NotificationStatus();
    savedNotificationStatus.setName(NAME);
    savedNotificationStatus.setDescription(DESCRIPTION);

    when(mockNotificationStatusRepository.save(savedNotificationStatus)).thenReturn(result);

    ResponseEntity<NotificationStatus> responseEntity = resource.createNotificationStatus(savedNotificationStatus);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    NotificationStatus savedNotificationStatus = new NotificationStatus();
    savedNotificationStatus = new NotificationStatus();
    savedNotificationStatus.setId(ID);
    savedNotificationStatus.setName(NAME);
    savedNotificationStatus.setDescription(DESCRIPTION);

    ResponseEntity<NotificationStatus> responseEntity = resource.createNotificationStatus(savedNotificationStatus);

    verifyZeroInteractions(mockNotificationStatusRepository, mockNotificationStatusSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    NotificationStatus savedNotificationStatus = new NotificationStatus();
    savedNotificationStatus = new NotificationStatus();
    savedNotificationStatus.setId(ID);
    savedNotificationStatus.setName(NAME);
    savedNotificationStatus.setDescription(DESCRIPTION);

    when(mockNotificationStatusRepository.save(savedNotificationStatus)).thenReturn(result);

    ResponseEntity<NotificationStatus> responseEntity = resource.updateNotificationStatus(savedNotificationStatus);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    NotificationStatus savedNotificationStatus = new NotificationStatus();
    savedNotificationStatus = new NotificationStatus();
    savedNotificationStatus.setId(null);
    savedNotificationStatus.setName(NAME);
    savedNotificationStatus.setDescription(DESCRIPTION);

    when(mockNotificationStatusRepository.save(savedNotificationStatus)).thenReturn(result);

    ResponseEntity<NotificationStatus> responseEntity = resource.updateNotificationStatus(savedNotificationStatus);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockNotificationStatusRepository.findAll()).thenReturn(list);

    List<NotificationStatus> notificationStatuses = resource.getAllNotificationStatuses();

    assertThat(notificationStatuses).isNotNull();
    assertThat(notificationStatuses.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockNotificationStatusRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<NotificationStatus> responseEntity = resource.getNotificationStatus(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteNotificationStatus(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockNotificationStatusRepository).delete(ID);
    verify(mockNotificationStatusSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockNotificationStatusSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<NotificationStatus> response = resource.searchNotificationStatuses(query);

    assertThat(response).isNotNull();
  }



}
