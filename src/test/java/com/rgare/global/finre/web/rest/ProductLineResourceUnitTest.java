package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.ProductLine;
import com.rgare.global.finre.repository.ProductLineRepository;
import com.rgare.global.finre.repository.search.ProductLineSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductLineResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private ProductLineRepository mockProductLineRepository;

  @Mock
  private ProductLineSearchRepository mockProductLineSearchRepository;

  private ProductLine productLine;

  private ProductLine result;

  private ProductLineResource resource;

  private Pageable pageable;
  private PageImpl<ProductLine> trackingPage;

  private List<ProductLine> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new ProductLineResource(mockProductLineRepository, mockProductLineSearchRepository);
    productLine = new ProductLine();
    productLine.setName(NAME);
    productLine.setDescription(DESCRIPTION);

    result = new ProductLine();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);


    list = Arrays.asList(productLine);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    ProductLine savedProductLine = new ProductLine();
    savedProductLine = new ProductLine();
    savedProductLine.setName(NAME);
    savedProductLine.setDescription(DESCRIPTION);

    when(mockProductLineRepository.save(savedProductLine)).thenReturn(result);

    ResponseEntity<ProductLine> responseEntity = resource.createProductLine(savedProductLine);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    ProductLine savedProductLine = new ProductLine();
    savedProductLine = new ProductLine();
    savedProductLine.setId(ID);
    savedProductLine.setName(NAME);
    savedProductLine.setDescription(DESCRIPTION);

    ResponseEntity<ProductLine> responseEntity = resource.createProductLine(savedProductLine);

    verifyZeroInteractions(mockProductLineRepository, mockProductLineSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    ProductLine savedProductLine = new ProductLine();
    savedProductLine = new ProductLine();
    savedProductLine.setId(ID);
    savedProductLine.setName(NAME);
    savedProductLine.setDescription(DESCRIPTION);

    when(mockProductLineRepository.save(savedProductLine)).thenReturn(result);

    ResponseEntity<ProductLine> responseEntity = resource.updateProductLine(savedProductLine);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    ProductLine savedProductLine = new ProductLine();
    savedProductLine = new ProductLine();
    savedProductLine.setId(null);
    savedProductLine.setName(NAME);
    savedProductLine.setDescription(DESCRIPTION);

    when(mockProductLineRepository.save(savedProductLine)).thenReturn(result);

    ResponseEntity<ProductLine> responseEntity = resource.updateProductLine(savedProductLine);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockProductLineRepository.findAll()).thenReturn(list);

    List<ProductLine> productLines = resource.getAllProductLines();

    assertThat(productLines).isNotNull();
    assertThat(productLines.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockProductLineRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<ProductLine> responseEntity = resource.getProductLine(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteProductLine(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockProductLineRepository).delete(ID);
    verify(mockProductLineSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockProductLineSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<ProductLine> response = resource.searchProductLines(query);

    assertThat(response).isNotNull();
  }



}
