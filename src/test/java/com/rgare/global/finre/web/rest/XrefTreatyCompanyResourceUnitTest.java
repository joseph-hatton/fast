package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.XrefTreatyCompany;
import com.rgare.global.finre.repository.XrefTreatyCompanyRepository;
import com.rgare.global.finre.repository.search.XrefTreatyCompanySearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class XrefTreatyCompanyResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private XrefTreatyCompanyRepository mockXrefTreatyCompanyRepository;

  @Mock
  private XrefTreatyCompanySearchRepository mockXrefTreatyCompanySearchRepository;

  private XrefTreatyCompany xrefTreatyCompany;

  private XrefTreatyCompany result;

  private XrefTreatyCompanyResource resource;

  private Pageable pageable;
  private PageImpl<XrefTreatyCompany> trackingPage;

  private List<XrefTreatyCompany> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new XrefTreatyCompanyResource(mockXrefTreatyCompanyRepository, mockXrefTreatyCompanySearchRepository);
    xrefTreatyCompany = new XrefTreatyCompany();
    xrefTreatyCompany.setCompanyType(NAME);
    xrefTreatyCompany.setTreatyId(2L);

    result = new XrefTreatyCompany();
    result.setId(ID);
    result.setCompanyType(NAME);
    result.setTreatyId(2L);


    list = Arrays.asList(xrefTreatyCompany);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    XrefTreatyCompany savedXrefTreatyCompany = new XrefTreatyCompany();
    savedXrefTreatyCompany = new XrefTreatyCompany();
    savedXrefTreatyCompany.setCompanyType(NAME);
    savedXrefTreatyCompany.setTreatyId(2L);

    when(mockXrefTreatyCompanyRepository.save(savedXrefTreatyCompany)).thenReturn(result);

    ResponseEntity<XrefTreatyCompany> responseEntity = resource.createXrefTreatyCompany(savedXrefTreatyCompany);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    XrefTreatyCompany savedXrefTreatyCompany = new XrefTreatyCompany();
    savedXrefTreatyCompany = new XrefTreatyCompany();
    savedXrefTreatyCompany.setId(ID);
    savedXrefTreatyCompany.setCompanyType(NAME);
    savedXrefTreatyCompany.setTreatyId(2L);

    ResponseEntity<XrefTreatyCompany> responseEntity = resource.createXrefTreatyCompany(savedXrefTreatyCompany);

    verifyZeroInteractions(mockXrefTreatyCompanyRepository, mockXrefTreatyCompanySearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    XrefTreatyCompany savedXrefTreatyCompany = new XrefTreatyCompany();
    savedXrefTreatyCompany = new XrefTreatyCompany();
    savedXrefTreatyCompany.setId(ID);
    savedXrefTreatyCompany.setCompanyType(NAME);
    savedXrefTreatyCompany.setTreatyId(2L);

    when(mockXrefTreatyCompanyRepository.save(savedXrefTreatyCompany)).thenReturn(result);

    ResponseEntity<XrefTreatyCompany> responseEntity = resource.updateXrefTreatyCompany(savedXrefTreatyCompany);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }


  @Test
  public void updateData_updateIDIsNullSuccessful() throws URISyntaxException {
    XrefTreatyCompany savedXrefTreatyCompany = new XrefTreatyCompany();
    savedXrefTreatyCompany = new XrefTreatyCompany();
    savedXrefTreatyCompany.setId(null);
    savedXrefTreatyCompany.setCompanyType(NAME);
    savedXrefTreatyCompany.setTreatyId(2L);

    when(mockXrefTreatyCompanyRepository.save(savedXrefTreatyCompany)).thenReturn(result);

    ResponseEntity<XrefTreatyCompany> responseEntity = resource.updateXrefTreatyCompany(savedXrefTreatyCompany);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockXrefTreatyCompanyRepository.findAll()).thenReturn(list);

    List<XrefTreatyCompany> allXrefTreatyCompanies = resource.getAllXrefTreatyCompanies();

    assertThat(allXrefTreatyCompanies).isNotNull();
  }

  @Test
  public void getByTreatyData_retreiveTreatiesSuccessful() throws URISyntaxException {

    when(mockXrefTreatyCompanyRepository.findAllByTreatyId(ID)).thenReturn(list);

    ResponseEntity<List<XrefTreatyCompany>> responseEntity = resource.getAllTreatiesByTreatyId(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getByCompanyData_retreiveCompaniesSuccessful() throws URISyntaxException {

    when(mockXrefTreatyCompanyRepository.findAllByCompanyId(ID)).thenReturn(list);

    ResponseEntity<List<XrefTreatyCompany>> responseEntity = resource.getAllByCompanyId(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getCountData_retreiveCountSuccessful() throws URISyntaxException {

    when(mockXrefTreatyCompanyRepository.findAllByCompanyId(ID)).thenReturn(list);

    Integer count = resource.getAllByCompanyCount(ID);

    assertThat(count).isGreaterThan(0);
  }

  @Test
  public void getCountData_retreiveCountIsZeroSuccessful() throws URISyntaxException {

    when(mockXrefTreatyCompanyRepository.findAllByCompanyId(ID)).thenReturn(null);

    Integer count = resource.getAllByCompanyCount(ID);

    assertThat(count).isEqualTo(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockXrefTreatyCompanyRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<XrefTreatyCompany> responseEntity = resource.getXrefTreatyCompany(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteXrefTreatyCompany(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockXrefTreatyCompanyRepository).delete(ID);
    verify(mockXrefTreatyCompanySearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockXrefTreatyCompanySearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<XrefTreatyCompany> response = resource.searchXrefTreatyCompanies(query);

    assertThat(response).isNotNull();
  }



}
