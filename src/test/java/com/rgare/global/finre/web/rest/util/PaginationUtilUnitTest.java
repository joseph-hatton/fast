package com.rgare.global.finre.web.rest.util;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class PaginationUtilUnitTest {

  private static final Long ID = 100L;

  private static final String APPLICATION_NAME = "finreApp";


  private static final String URI= "http://test.com";
  private static final String QUERY = "A";

  private HttpHeaders httpHeaders;

  private Page page;

  List<String> list;

  private Pageable pageable;

  @Before
  public void setup() throws URISyntaxException {
    httpHeaders = new HttpHeaders();

    list = new ArrayList<String>();

    list.add("TEST1");
    list.add("TEST2");

  }

  @Test
  public void getData_generatePaginationHttpHeadersDefault() throws URISyntaxException {
    page = new PageImpl<>(list);

    String link = "";
    link += "<http://test.com?page=0&size=0>; rel=\"last\",";
    link += "<http://test.com?page=0&size=0>; rel=\"first\"";
    httpHeaders.add("X-Total-Count", Long.toString(page.getTotalElements()));
    httpHeaders.add(HttpHeaders.LINK, link);

    HttpHeaders expected = PaginationUtil.generatePaginationHttpHeaders(page,URI);
    assertThat(expected).isEqualTo(httpHeaders);
  }

  @Test
  public void getData_HttpHeadersPagesGreaterThanZero() throws URISyntaxException {

    pageable = new PageRequest(1, 5);

    page = new PageImpl<>(list,pageable,3);

    String link = "";
    if ((page.getNumber() + 1) < page.getTotalPages()) {
      link = "<http://test.com?page=0&size=0>; rel=\"next\",";
    }
    // prev link
    if ((page.getNumber()) > 0) {
      link += "<http://test.com?page=0&size=5>; rel=\"prev\",";
    }
    // last and first link
    int lastPage = 0;
    if (page.getTotalPages() > 0) {
      lastPage = page.getTotalPages() - 1;
    }
    link += "<http://test.com?page=1&size=5>; rel=\"last\",";
    link += "<http://test.com?page=0&size=5>; rel=\"first\"";
    httpHeaders.add("X-Total-Count", Long.toString(page.getTotalElements()));
    httpHeaders.add(HttpHeaders.LINK, link);

    HttpHeaders expected = PaginationUtil.generatePaginationHttpHeaders(page,URI);
    assertThat(expected).isEqualTo(httpHeaders);
  }

  @Test
  public void getData_HttpHeadersPagesLessThanTotal() throws URISyntaxException {
    pageable = new PageRequest(1, 2);

    page = new PageImpl<>(list,pageable,3);

    String link = "";
    if ((page.getNumber() + 1) < page.getTotalPages()) {
      link = "<http://test.com?page=0&size=2>; rel=\"next\",";
    }
    // prev link
    if ((page.getNumber()) > 0) {
      link += "<http://test.com?page=0&size=2>; rel=\"prev\",";
    }
    // last and first link
    int lastPage = 0;
    if (page.getTotalPages() > 0) {
      lastPage = page.getTotalPages() - 1;
    }
    link += "<http://test.com?page=1&size=2>; rel=\"last\",";
    link += "<http://test.com?page=0&size=2>; rel=\"first\"";
    httpHeaders.add("X-Total-Count", Long.toString(page.getTotalElements()));
    httpHeaders.add(HttpHeaders.LINK, link);

    HttpHeaders expected = PaginationUtil.generatePaginationHttpHeaders(page,URI);
    assertThat(expected).isEqualTo(httpHeaders);
  }

  //-------------------------------
  @Test
  public void getData_generateSearchHttpHeadersDefault() throws URISyntaxException {
    page = new PageImpl<>(list);

    String link = "";
    link += "<http://test.com?page=0&size=0&query=A>; rel=\"last\",";
    link += "<http://test.com?page=0&size=0&query=A>; rel=\"first\"";
    httpHeaders.add("X-Total-Count", Long.toString(page.getTotalElements()));
    httpHeaders.add(HttpHeaders.LINK, link);

    HttpHeaders expected = PaginationUtil.generateSearchPaginationHttpHeaders(QUERY,page,URI);
    assertThat(expected).isEqualTo(httpHeaders);
  }

  @Test
  public void getData_searchHttpHeadersPagesGreaterThanZero() throws URISyntaxException {

    pageable = new PageRequest(1, 5);

    page = new PageImpl<>(list,pageable,3);

    String link = "";
    if ((page.getNumber() + 1) < page.getTotalPages()) {
      link = "<http://test.com?page=0&size=0&query=A>; rel=\"next\",";
    }
    // prev link
    if ((page.getNumber()) > 0) {
      link += "<http://test.com?page=0&size=5&query=A>; rel=\"prev\",";
    }
    // last and first link
    int lastPage = 0;
    if (page.getTotalPages() > 0) {
      lastPage = page.getTotalPages() - 1;
    }
    link += "<http://test.com?page=1&size=5&query=A>; rel=\"last\",";
    link += "<http://test.com?page=0&size=5&query=A>; rel=\"first\"";
    httpHeaders.add("X-Total-Count", Long.toString(page.getTotalElements()));
    httpHeaders.add(HttpHeaders.LINK, link);

    HttpHeaders expected = PaginationUtil.generateSearchPaginationHttpHeaders(QUERY,page,URI);
    assertThat(expected).isEqualTo(httpHeaders);
  }

  @Test
  public void getData_searchHttpHeadersPagesLessThanTotal() throws URISyntaxException {
    pageable = new PageRequest(1, 2);

    page = new PageImpl<>(list,pageable,3);

    String link = "";
    if ((page.getNumber() + 1) < page.getTotalPages()) {
      link = "<http://test.com?page=0&size=2&query=A>; rel=\"next\",";
    }
    // prev link
    if ((page.getNumber()) > 0) {
      link += "<http://test.com?page=0&size=2&query=A>; rel=\"prev\",";
    }
    // last and first link
    int lastPage = 0;
    if (page.getTotalPages() > 0) {
      lastPage = page.getTotalPages() - 1;
    }
    link += "<http://test.com?page=1&size=2&query=A>; rel=\"last\",";
    link += "<http://test.com?page=0&size=2&query=A>; rel=\"first\"";
    httpHeaders.add("X-Total-Count", Long.toString(page.getTotalElements()));
    httpHeaders.add(HttpHeaders.LINK, link);

    HttpHeaders expected = PaginationUtil.generateSearchPaginationHttpHeaders(QUERY,page,URI);
    assertThat(expected).isEqualTo(httpHeaders);
  }

}
