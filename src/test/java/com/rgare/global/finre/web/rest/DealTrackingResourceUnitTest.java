package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.DealTracking;
import com.rgare.global.finre.repository.DealTrackingRepository;
import com.rgare.global.finre.repository.search.DealTrackingSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DealTrackingResourceUnitTest {

  private static final Long ID = 100L;
  private static final String REVISED_FLAG = "REVISED_FLAG";
  private static final String FLAG = "FLAG";

  @Mock
  private DealTrackingRepository mockDealTrackingRepository;

  @Mock
  private DealTrackingSearchRepository mockDealTrackingSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private DealTracking dealTracking;

  private DealTracking result;

  private DealTrackingResource resource;

  private Pageable pageable;
  private PageImpl<DealTracking> trackingPage;

  private List<DealTracking> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new DealTrackingResource(mockDealTrackingRepository, mockDealTrackingSearchRepository);
    dealTracking = new DealTracking();
    dealTracking.setReportingRevisedFlag(REVISED_FLAG);
    dealTracking.setReportingFlag(FLAG);

    result = new DealTracking();
    result.setId(ID);
    result.setReportingRevisedFlag(REVISED_FLAG);
    result.setReportingFlag(FLAG);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(dealTracking);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    DealTracking savedDealTracking = new DealTracking();
    savedDealTracking = new DealTracking();
    savedDealTracking.setReportingRevisedFlag(REVISED_FLAG);
    savedDealTracking.setReportingFlag(FLAG);

    when(mockDealTrackingRepository.save(savedDealTracking)).thenReturn(result);

    ResponseEntity<DealTracking> responseEntity = resource.createDealTracking(savedDealTracking);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    DealTracking savedDealTracking = new DealTracking();
    savedDealTracking = new DealTracking();
    savedDealTracking.setId(ID);
    savedDealTracking.setReportingRevisedFlag(REVISED_FLAG);
    savedDealTracking.setReportingFlag(FLAG);

    ResponseEntity<DealTracking> responseEntity = resource.createDealTracking(savedDealTracking);

    verifyZeroInteractions(mockDealTrackingRepository, mockDealTrackingSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    DealTracking savedDealTracking = new DealTracking();
    savedDealTracking = new DealTracking();
    savedDealTracking.setId(ID);
    savedDealTracking.setReportingRevisedFlag(REVISED_FLAG);
    savedDealTracking.setReportingFlag(FLAG);

    when(mockDealTrackingRepository.save(savedDealTracking)).thenReturn(result);

    ResponseEntity<DealTracking> responseEntity = resource.updateDealTracking(savedDealTracking);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    DealTracking savedDealTracking = new DealTracking();
    savedDealTracking = new DealTracking();
    savedDealTracking.setId(null);
    savedDealTracking.setReportingRevisedFlag(REVISED_FLAG);
    savedDealTracking.setReportingFlag(FLAG);

    when(mockDealTrackingRepository.save(savedDealTracking)).thenReturn(result);

    ResponseEntity<DealTracking> responseEntity = resource.updateDealTracking(savedDealTracking);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockDealTrackingRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<DealTracking> responseEntity = resource.getDealTracking(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockDealTrackingRepository.findAll(pageable)).thenReturn(trackingPage);

    ResponseEntity<List<DealTracking>> responseEntity = resource.getAllDealTrackings(pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllDealsSuccessful() throws URISyntaxException {

    when(mockDealTrackingRepository.findAllByIsActiveAndDealTrackingStatus(1,"OPEN")).thenReturn(list);

    ResponseEntity<List<DealTracking>> responseEntity = resource.getAll();

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllByDealSuccessful() throws URISyntaxException {

    when(mockDealTrackingRepository.findAllByDealIdAndIsActive(ID,pageable,1)).thenReturn(trackingPage);

    ResponseEntity<List<DealTracking>> responseEntity = resource.getAllByDealId(ID,pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getByDatesData_retreiveByDatesSuccessful() throws URISyntaxException {
    when(mockDealTrackingRepository.findAllByDealIdAndIsActiveAndReportingPeriodEndDateBetweenOrderByIdDesc(ID,1, LocalDate.of(2017,2,3),
      LocalDate.of(2017,5,6), pageable)).thenReturn(trackingPage);

    ResponseEntity<List<DealTracking>> responseEntity = resource.getAllByReportingDateDates(ID,2017,2,3,
      2017,5,6,pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getByDatesData_retreiveByReportingDateSuccessful() throws URISyntaxException {
    when(mockDealTrackingRepository.findAllByDealIdAndReportingPeriodEndDateAndIsActive(ID,
      LocalDate.of(2017,5,6),1)).thenReturn(list);

    ResponseEntity<List<DealTracking>> responseEntity = resource.getAllByReportingDate(ID,2017,5,6);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteDealTracking(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockDealTrackingRepository).delete(ID);
    verify(mockDealTrackingSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockDealTrackingSearchRepository.search(any(QueryBuilder.class), eq(pageable))).thenReturn(trackingPage);

    ResponseEntity<List<DealTracking>> response = resource.searchDealTrackings(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }



}
