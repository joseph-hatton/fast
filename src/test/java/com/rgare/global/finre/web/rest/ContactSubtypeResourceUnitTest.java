package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.ContactSubtype;
import com.rgare.global.finre.repository.ContactSubtypeRepository;
import com.rgare.global.finre.repository.search.ContactSubtypeSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ContactSubtypeResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private ContactSubtypeRepository mockContractSubtypeRepository;

  @Mock
  private ContactSubtypeSearchRepository mockContactSubtypeSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private ContactSubtype contactSubtype;

  private ContactSubtype result;

  private ContactSubtypeResource resource;

  private Pageable pageable;
  private PageImpl<ContactSubtype> trackingPage;

  private List<ContactSubtype> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new ContactSubtypeResource(mockContractSubtypeRepository, mockContactSubtypeSearchRepository);
    contactSubtype = new ContactSubtype();
    contactSubtype.setName(NAME);
    contactSubtype.setDescription(DESCRIPTION);

    result = new ContactSubtype();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(contactSubtype);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    ContactSubtype savedContactSubtype = new ContactSubtype();
    savedContactSubtype = new ContactSubtype();
    savedContactSubtype.setName(NAME);
    savedContactSubtype.setDescription(DESCRIPTION);

    when(mockContractSubtypeRepository.save(savedContactSubtype)).thenReturn(result);

    ResponseEntity<ContactSubtype> responseEntity = resource.createContactSubtype(savedContactSubtype);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    ContactSubtype savedContactSubtype = new ContactSubtype();
    savedContactSubtype = new ContactSubtype();
    savedContactSubtype.setId(ID);
    savedContactSubtype.setName(NAME);
    savedContactSubtype.setDescription(DESCRIPTION);

    ResponseEntity<ContactSubtype> responseEntity = resource.createContactSubtype(savedContactSubtype);

    verifyZeroInteractions(mockContractSubtypeRepository, mockContactSubtypeSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    ContactSubtype savedContactSubtype = new ContactSubtype();
    savedContactSubtype = new ContactSubtype();
    savedContactSubtype.setId(ID);
    savedContactSubtype.setName(NAME);
    savedContactSubtype.setDescription(DESCRIPTION);

    when(mockContractSubtypeRepository.save(savedContactSubtype)).thenReturn(result);

    ResponseEntity<ContactSubtype> responseEntity = resource.updateContactSubtype(savedContactSubtype);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    ContactSubtype savedContactSubtype = new ContactSubtype();
    savedContactSubtype = new ContactSubtype();
    savedContactSubtype.setId(null);
    savedContactSubtype.setName(NAME);
    savedContactSubtype.setDescription(DESCRIPTION);

    when(mockContractSubtypeRepository.save(savedContactSubtype)).thenReturn(result);

    ResponseEntity<ContactSubtype> responseEntity = resource.updateContactSubtype(savedContactSubtype);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockContractSubtypeRepository.findAll()).thenReturn(list);

    List<ContactSubtype> contactSubtypes = resource.getAllContactSubtypes();

    assertThat(contactSubtypes).isNotNull();
    assertThat(contactSubtypes.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockContractSubtypeRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<ContactSubtype> responseEntity = resource.getContactSubtype(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteContactSubtype(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockContractSubtypeRepository).delete(ID);
    verify(mockContactSubtypeSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockContactSubtypeSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<ContactSubtype> response = resource.searchContactSubtypes(query);

    assertThat(response).isNotNull();
  }



}
