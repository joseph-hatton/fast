package com.rgare.global.finre.web.rest;

import io.github.jhipster.config.JHipsterProperties;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for the ProfileInfoResource REST controller.
 *
 * @see ProfileInfoResource
 */
@RunWith(MockitoJUnitRunner.class)
public class ProfileInfoResourceUnitTest {

    @Mock
    private Environment mockEnvironment;

    @Mock
    private JHipsterProperties mockJHipsterProperties;

    private ProfileInfoResource resource;

    @Before
    public void setup() {
        resource = new ProfileInfoResource(mockEnvironment,mockJHipsterProperties);
    }

    @Test
    public void getActiveProfiles() throws Exception {
        //ProfileInfoResource.ProfileInfoVM activeProfiles =  resource.getActiveProfiles();

        //assertThat(activeProfiles).isNotNull();
    }

    @Test
    public void getProfileInfoWithoutActiveProfiles() throws Exception {
        String emptyProfile[] = {};
        assertThat(emptyProfile).isEmpty();
    }
}
