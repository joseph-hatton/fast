package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.State;
import com.rgare.global.finre.repository.StateRepository;
import com.rgare.global.finre.repository.search.StateSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StateResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private StateRepository mockStateRepository;

  @Mock
  private StateSearchRepository mockStateSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private State state;

  private State result;

  private StateResource resource;

  private Pageable pageable;
  private PageImpl<State> trackingPage;

  private List<State> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new StateResource(mockStateRepository, mockStateSearchRepository);
    state = new State();
    state.setName(NAME);
    state.setDescription(DESCRIPTION);

    result = new State();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(state);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    State savedState = new State();
    savedState = new State();
    savedState.setName(NAME);
    savedState.setDescription(DESCRIPTION);

    when(mockStateRepository.save(savedState)).thenReturn(result);

    ResponseEntity<State> responseEntity = resource.createState(savedState);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    State savedState = new State();
    savedState = new State();
    savedState.setId(ID);
    savedState.setName(NAME);
    savedState.setDescription(DESCRIPTION);

    ResponseEntity<State> responseEntity = resource.createState(savedState);

    verifyZeroInteractions(mockStateRepository, mockStateSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    State savedState = new State();
    savedState = new State();
    savedState.setId(ID);
    savedState.setName(NAME);
    savedState.setDescription(DESCRIPTION);

    when(mockStateRepository.save(savedState)).thenReturn(result);

    ResponseEntity<State> responseEntity = resource.updateState(savedState);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    State savedState = new State();
    savedState = new State();
    savedState.setId(null);
    savedState.setName(NAME);
    savedState.setDescription(DESCRIPTION);

    when(mockStateRepository.save(savedState)).thenReturn(result);

    ResponseEntity<State> responseEntity = resource.updateState(savedState);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getallData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockStateRepository.findAll()).thenReturn(list);

    List<State> states = resource.getAllStates();

    assertThat(states).isNotNull();
    assertThat(states.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockStateRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<State> responseEntity = resource.getState(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteState(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockStateRepository).delete(ID);
    verify(mockStateSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockStateSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<State> response = resource.searchStates(query);

    assertThat(response).isNotNull();
  }



}
