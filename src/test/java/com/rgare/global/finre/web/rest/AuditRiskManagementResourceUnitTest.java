package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.AuditRiskManagement;
import com.rgare.global.finre.repository.AuditRiskManagementRepository;
import com.rgare.global.finre.repository.search.AuditRiskManagementSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuditRiskManagementResourceUnitTest {

  private static final Long ID = 100L;
  private static final String ACTION = "ACTION";
  private static final String COMMENTS = "COMMENTS";

  @Mock
  private AuditRiskManagementRepository mockAuditRiskManagementRepository;

  @Mock
  private AuditRiskManagementSearchRepository mockAuditRiskManagementSearchRepository;

  private AuditRiskManagement auditRiskManagement;

  private AuditRiskManagement result;

  private AuditRiskManagementResource resource;

  private Pageable pageable;
  private PageImpl<AuditRiskManagement> trackingPage;

  private List<AuditRiskManagement> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new AuditRiskManagementResource(mockAuditRiskManagementRepository, mockAuditRiskManagementSearchRepository);
    auditRiskManagement = new AuditRiskManagement();
    auditRiskManagement.setAction(ACTION);
    auditRiskManagement.setComments(COMMENTS);

    result = new AuditRiskManagement();
    result.setId(ID);
    result.setAction(ACTION);
    result.setComments(COMMENTS);


    list = Arrays.asList(auditRiskManagement);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    AuditRiskManagement savedAgreementType = new AuditRiskManagement();
    savedAgreementType = new AuditRiskManagement();
    savedAgreementType.setAction(ACTION);
    savedAgreementType.setComments(COMMENTS);

    when(mockAuditRiskManagementRepository.save(savedAgreementType)).thenReturn(result);

    ResponseEntity<AuditRiskManagement> responseEntity = resource.createAuditRiskManagement(savedAgreementType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    AuditRiskManagement savedAgreementType = new AuditRiskManagement();
    savedAgreementType = new AuditRiskManagement();
    savedAgreementType.setId(ID);
    savedAgreementType.setAction(ACTION);
    savedAgreementType.setComments(COMMENTS);

    ResponseEntity<AuditRiskManagement> responseEntity = resource.createAuditRiskManagement(savedAgreementType);

    verifyZeroInteractions(mockAuditRiskManagementRepository, mockAuditRiskManagementSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    AuditRiskManagement savedAgreementType = new AuditRiskManagement();
    savedAgreementType = new AuditRiskManagement();
    savedAgreementType.setId(ID);
    savedAgreementType.setAction(ACTION);
    savedAgreementType.setComments(COMMENTS);

    when(mockAuditRiskManagementRepository.save(savedAgreementType)).thenReturn(result);

    ResponseEntity<AuditRiskManagement> responseEntity = resource.updateAuditRiskManagement(savedAgreementType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    AuditRiskManagement savedAgreementType = new AuditRiskManagement();
    savedAgreementType = new AuditRiskManagement();
    savedAgreementType.setId(null);
    savedAgreementType.setAction(ACTION);
    savedAgreementType.setComments(COMMENTS);

    when(mockAuditRiskManagementRepository.save(savedAgreementType)).thenReturn(result);

    ResponseEntity<AuditRiskManagement> responseEntity = resource.updateAuditRiskManagement(savedAgreementType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockAuditRiskManagementRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<AuditRiskManagement> responseEntity = resource.getAuditRiskManagement(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockAuditRiskManagementRepository.findAll()).thenReturn(list);

    List<AuditRiskManagement> allAuditRiskManagements = resource.getAllAuditRiskManagements();

    assertThat(allAuditRiskManagements).isNotNull();
    assertThat(allAuditRiskManagements.size()).isGreaterThan(0);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteAuditRiskManagement(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockAuditRiskManagementRepository).delete(ID);
    verify(mockAuditRiskManagementSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockAuditRiskManagementSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<AuditRiskManagement> response = resource.searchAuditRiskManagements(query);

    assertThat(response).isNotNull();
  }



}
