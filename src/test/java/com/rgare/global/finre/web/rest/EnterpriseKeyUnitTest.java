package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.EnterpriseKey;
import com.rgare.global.finre.repository.EnterpriseKeyRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EnterpriseKeyUnitTest {

  private static final Long ID = 100L;
  private static final String STATUS = "STATUS";
  private static final String COMMENTS = "COMMENTS";

  @Mock
  private EnterpriseKeyRepository mockEnterpriseKeyRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private EnterpriseKey enterpriseKey;

  private EnterpriseKey result;

  private EnterpriseKeyResource resource;

  private Pageable pageable;
  private PageImpl<EnterpriseKey> trackingPage;

  private List<EnterpriseKey> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new EnterpriseKeyResource(mockEnterpriseKeyRepository);
    enterpriseKey = new EnterpriseKey();
    enterpriseKey.setLegalName(STATUS);
    enterpriseKey.setLegalCode(COMMENTS);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(enterpriseKey);
    trackingPage = new PageImpl<>(list);

  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    result = new EnterpriseKey();
    result.setId(ID);
    result.setLegalName(STATUS);
    result.setLegalCode(COMMENTS);
    result.setVersionDate(4L);
    result.setVersionNumber(100L);

    when(mockEnterpriseKeyRepository.findOneByEnterpriseId(ID)).thenReturn(result);

    ResponseEntity<EnterpriseKey> responseEntity = resource.getEnterpriseKey(ID,2L,3L);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getOneData_createDateComparison() throws URISyntaxException {

    result = new EnterpriseKey();
    result.setId(ID);
    result.setLegalName(STATUS);
    result.setLegalCode(COMMENTS);
    result.setVersionDate(4L);
    result.setVersionNumber(100L);

    when(mockEnterpriseKeyRepository.findOneByEnterpriseId(ID)).thenReturn(result);

    ResponseEntity<EnterpriseKey> responseEntity = resource.getEnterpriseKey(ID,4L,3L);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getOneData_versionNumberIsNull() throws URISyntaxException {

    result = new EnterpriseKey();
    result.setId(ID);
    result.setLegalName(STATUS);
    result.setLegalCode(COMMENTS);
    result.setVersionDate(4L);
    result.setVersionNumber(null);

    when(mockEnterpriseKeyRepository.findOneByEnterpriseId(ID)).thenReturn(result);

    ResponseEntity<EnterpriseKey> responseEntity = resource.getEnterpriseKey(ID,4L,3L);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getOneData_versionDateIsNull() throws URISyntaxException {

    result = new EnterpriseKey();
    result.setId(ID);
    result.setLegalName(STATUS);
    result.setLegalCode(COMMENTS);
    result.setVersionDate(null);
    result.setVersionNumber(100L);

    when(mockEnterpriseKeyRepository.findOneByEnterpriseId(ID)).thenReturn(result);

    ResponseEntity<EnterpriseKey> responseEntity = resource.getEnterpriseKey(ID,4L,3L);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockEnterpriseKeyRepository.findOneByEnterpriseId(ID)).thenReturn(result);

    EnterpriseKey responseEntity = resource.getEnterpriseKey(ID);

    assertThat(responseEntity).isNull();
  }

  @Test
  public void getAllKeysData_retreiveAllKeysSuccessful() throws URISyntaxException {

    when(mockEnterpriseKeyRepository.findAll()).thenReturn(list);

    List<EnterpriseKey> responseEntity = resource.getAllEnterpriseKeys();

    assertThat(responseEntity).isNotNull();
  }

}
