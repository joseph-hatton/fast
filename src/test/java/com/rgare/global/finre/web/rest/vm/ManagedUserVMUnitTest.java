package com.rgare.global.finre.web.rest.vm;

import com.rgare.global.finre.service.dto.UserDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.validation.constraints.Size;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * View Model extending the UserDTO, which is meant to be used in the user management UI.
 */
@RunWith(MockitoJUnitRunner.class)
public class ManagedUserVMUnitTest {

    private static final Long ID = 1L;
    private static final String LOGIN = "LOGIN";
    private static final String PASSWORD = "PASSWORD";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String EMAIL = "EMAIL";
    private static final boolean ACTIVATED = true;
    private static final String IMAGE_URL = "IMAGEURL";
    private static final String LANG_KEY = "LANG_KEY";
    private static final String CREATED_BY = "CREATED_BY";
    private static final ZonedDateTime CREATED_DATE = ZonedDateTime.now();
    private static final String LAST_MODIFIED = "LASTMODIFIED";
    private static final ZonedDateTime MODIFIED_DATE = ZonedDateTime.now();
    private static Set<String> AUTHORITIES;

    private static final String result = "ManagedUserVM{} UserDTO{" +
      "login='LOGIN', " +
      "firstName='"+FIRST_NAME+"', " +
      "lastName='"+LAST_NAME+"', " +
      "email='"+EMAIL+"', " +
      "imageUrl='"+IMAGE_URL+"', " +
      "activated="+ACTIVATED+", " +
      "langKey='"+LANG_KEY+"', " +
      "createdBy="+CREATED_BY+", " +
      "createdDate="+CREATED_DATE+", " +
      "lastModifiedBy='"+LAST_MODIFIED+"', " +
      "lastModifiedDate="+MODIFIED_DATE+", " +
      "authorities="+AUTHORITIES
      +"}";

    @Before
    public void setup() {
      AUTHORITIES = new HashSet<>();
      AUTHORITIES.add("ADMIN");
      AUTHORITIES.add("USER");
    }

    @Test
    public void testForParameterizationDefault(){
      ManagedUserVM managedUserVM = new ManagedUserVM();
      assertThat(managedUserVM).isNotNull();
    }

    @Test
    public void testForParameterization(){
      ManagedUserVM managedUserVM = new ManagedUserVM(
        ID, LOGIN, PASSWORD,
        FIRST_NAME, LAST_NAME, EMAIL,
        ACTIVATED, IMAGE_URL, LANG_KEY,
        CREATED_BY, CREATED_DATE, LAST_MODIFIED,
        MODIFIED_DATE,AUTHORITIES );
      assertThat(managedUserVM.getId()).isEqualTo(ID);
      assertThat(managedUserVM.getLogin()).isEqualTo(LOGIN);
      assertThat(managedUserVM.getPassword()).isEqualTo(PASSWORD);
      assertThat(managedUserVM.getFirstName()).isEqualTo(FIRST_NAME);
      assertThat(managedUserVM.getLastName()).isEqualTo(LAST_NAME);
      assertThat(managedUserVM.getEmail()).isEqualTo(EMAIL);
      assertThat(managedUserVM.isActivated()).isEqualTo(ACTIVATED);
      assertThat(managedUserVM.getImageUrl()).isEqualTo(IMAGE_URL);
      assertThat(managedUserVM.getLangKey()).isEqualTo(LANG_KEY);
      assertThat(managedUserVM.getCreatedBy()).isEqualTo(CREATED_BY);
      assertThat(managedUserVM.getCreatedDate()).isEqualTo(CREATED_DATE);
      assertThat(managedUserVM.getLastModifiedBy()).isEqualTo(LAST_MODIFIED);
      assertThat(managedUserVM.getLastModifiedDate()).isEqualTo(MODIFIED_DATE);
      assertThat(managedUserVM.getAuthorities()).isEqualTo(AUTHORITIES);
    }

  @Test
  public void testForEquals(){
    ManagedUserVM managedUserVM = new ManagedUserVM(
      ID, LOGIN, PASSWORD,
      FIRST_NAME, LAST_NAME, EMAIL,
      ACTIVATED, IMAGE_URL, LANG_KEY,
      CREATED_BY, CREATED_DATE, LAST_MODIFIED,
      MODIFIED_DATE,null );
    assertThat(managedUserVM.toString()).isEqualTo(result);
  }
}
