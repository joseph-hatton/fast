package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.Treaty;
import com.rgare.global.finre.domain.TreatyStatus;
import com.rgare.global.finre.repository.AuditTreatyRepository;
import com.rgare.global.finre.repository.TreatyRepository;
import com.rgare.global.finre.repository.search.TreatySearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TreatyResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private TreatyRepository mockTreatyRepository;

  @Mock
  private TreatySearchRepository mockTreatySearchRepository;

  @Mock
  private Dp3LogService mockDp3LogService;

  @Mock
  private AuditTreatyRepository mockAuditTreatyRepository;

  private Treaty treatyTransactionRelief;

  private Treaty result;

  private TreatyResource resource;

  private Pageable pageable;
  private PageImpl<Treaty> trackingPage;

  private List<Treaty> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new TreatyResource(mockTreatyRepository,
      mockTreatySearchRepository,
      mockDp3LogService,
      mockAuditTreatyRepository);
    treatyTransactionRelief = new Treaty();
    treatyTransactionRelief.setLastUser(NAME);
    treatyTransactionRelief.setCreationUser(DESCRIPTION);

    result = new Treaty();
    result.setId(ID);
    result.setLastUser(NAME);
    result.setCreationUser(DESCRIPTION);
    result.setTreatyStatus(new TreatyStatus());


    list = Arrays.asList(treatyTransactionRelief);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    Treaty savedTreaty = new Treaty();
    savedTreaty = new Treaty();
    savedTreaty.setLastUser(NAME);
    savedTreaty.setCreationUser(DESCRIPTION);
    savedTreaty.setTreatyStatus(new TreatyStatus());

    when(mockTreatyRepository.save(savedTreaty)).thenReturn(result);

    ResponseEntity<Treaty> responseEntity = resource.createTreaty(savedTreaty);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    Treaty savedTreaty = new Treaty();
    savedTreaty = new Treaty();
    savedTreaty.setId(ID);
    savedTreaty.setLastUser(NAME);
    savedTreaty.setCreationUser(DESCRIPTION);

    ResponseEntity<Treaty> responseEntity = resource.createTreaty(savedTreaty);

    verifyZeroInteractions(mockTreatyRepository, mockTreatySearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    Treaty savedTreaty = new Treaty();
    savedTreaty = new Treaty();
    savedTreaty.setId(ID);
    savedTreaty.setLastUser(NAME);
    savedTreaty.setCreationUser(DESCRIPTION);
    savedTreaty.setTreatyStatus(new TreatyStatus());

    when(mockTreatyRepository.save(savedTreaty)).thenReturn(result);

    ResponseEntity<Treaty> responseEntity = resource.updateTreaty(savedTreaty);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    Treaty savedTreaty = new Treaty();
    savedTreaty = new Treaty();
    savedTreaty.setId(null);
    savedTreaty.setLastUser(NAME);
    savedTreaty.setCreationUser(DESCRIPTION);
    savedTreaty.setTreatyStatus(new TreatyStatus());

    when(mockTreatyRepository.save(savedTreaty)).thenReturn(result);

    ResponseEntity<Treaty> responseEntity = resource.updateTreaty(savedTreaty);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockTreatyRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<Treaty> responseEntity = resource.getTreaty(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockTreatyRepository.findAllByIsActive(1,pageable)).thenReturn(trackingPage);

    ResponseEntity<List<Treaty>> responseEntity = resource.getAllTreaties(pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllNotificationSuccessful() throws URISyntaxException {

    when(mockTreatyRepository.findAllByNotificationStatus(5)).thenReturn(list);

    List<Treaty> allTreatiesNotification = resource.getAllTreatiesNotification();

    assertThat(allTreatiesNotification).isNotNull();
    assertThat(allTreatiesNotification.size()).isGreaterThan(0);
  }

  @Test
  public void getAllData_retreiveAllActiveSuccessful() throws URISyntaxException {

    when(mockTreatyRepository.findAllByIsActive(1)).thenReturn(list);

    List<Treaty> allTreaties = resource.getAllTreaties();

    assertThat(allTreaties).isNotNull();
    assertThat(allTreaties.size()).isGreaterThan(0);
  }

  @Test
  public void getAllData_retreiveAllByDealActiveSuccessful() throws URISyntaxException {

    when(mockTreatyRepository.findAllByDealIdAndIsActive(ID,1,pageable)).thenReturn(trackingPage);

    ResponseEntity<List<Treaty>> responseEntity = resource.getAllTreatiesByDealId(ID,pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllByTreatyNameSuccessful() throws URISyntaxException {

    when(mockTreatyRepository.findAllByTreatyNameIgnoreCaseAndIsActive(NAME,1)).thenReturn(list);

    List<Treaty> allTreaties = resource.getAllTreatiesByTreatyName(NAME);

    assertThat(allTreaties).isNotNull();
    assertThat(allTreaties.size()).isGreaterThan(0);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    when(mockTreatyRepository.findOne(ID)).thenReturn(result);
    ResponseEntity<Void> response = resource.deleteTreaty(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockTreatyRepository).delete(ID);
    verify(mockTreatySearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockTreatySearchRepository.search(any(QueryBuilder.class), eq(pageable))).thenReturn(trackingPage);

    ResponseEntity<List<Treaty>> response = resource.searchTreaties(query,pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }



}
