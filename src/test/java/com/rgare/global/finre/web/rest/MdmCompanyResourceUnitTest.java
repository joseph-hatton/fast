package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.MdmCompany;
import com.rgare.global.finre.repository.MdmCompanyRepository;
import com.rgare.global.finre.repository.CompanyRepository;
import com.rgare.global.finre.repository.search.MdmCompanySearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MdmCompanyResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String LEGALNAME = "LEGALNAME";

  @Mock
  private MdmCompanyRepository mockMdmCompanyRepository;

  @Mock
  private CompanyRepository mockCompanyRepository;

  @Mock
  private MdmCompanySearchRepository mockMdmCompanySearchRepository;

  private MdmCompany mdmCompany;

  private MdmCompany result;

  private MdmCompanyResource resource;

  private Pageable pageable;
  private Page<MdmCompany> trackingPage;

  private List<MdmCompany> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new MdmCompanyResource(mockMdmCompanyRepository, mockCompanyRepository);
    mdmCompany = new MdmCompany();
    mdmCompany.setCompanyName(NAME);
    mdmCompany.setCountryName(LEGALNAME);

    result = new MdmCompany();
    result.setId(ID);
    result.setCompanyName(NAME);
    result.setCountryName(LEGALNAME);


    list = Arrays.asList(mdmCompany);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockMdmCompanyRepository.searchExistingMdmCompanyQuery(query, pageable)).thenReturn(trackingPage);

    ResponseEntity< List<MdmCompany>> response = resource.SearchExistingMDMCompanies(query,pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }



}
