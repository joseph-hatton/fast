package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.TransactionStatus;
import com.rgare.global.finre.repository.TransactionStatusRepository;
import com.rgare.global.finre.repository.search.TransactionStatusSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TransactionStatusResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private TransactionStatusRepository mockTransactionStatusRepository;

  @Mock
  private TransactionStatusSearchRepository mockTransactionStatusSearchRepository;

  private TransactionStatus transactionStatus;

  private TransactionStatus result;

  private TransactionStatusResource resource;

  private Pageable pageable;
  private PageImpl<TransactionStatus> trackingPage;

  private List<TransactionStatus> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new TransactionStatusResource(mockTransactionStatusRepository, mockTransactionStatusSearchRepository);
    transactionStatus = new TransactionStatus();
    transactionStatus.setName(NAME);
    transactionStatus.setDescription(DESCRIPTION);

    result = new TransactionStatus();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);


    list = Arrays.asList(transactionStatus);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    TransactionStatus savedTransactionStatus = new TransactionStatus();
    savedTransactionStatus = new TransactionStatus();
    savedTransactionStatus.setName(NAME);
    savedTransactionStatus.setDescription(DESCRIPTION);

    when(mockTransactionStatusRepository.save(savedTransactionStatus)).thenReturn(result);

    ResponseEntity<TransactionStatus> responseEntity = resource.createTransactionStatus(savedTransactionStatus);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    TransactionStatus savedTransactionStatus = new TransactionStatus();
    savedTransactionStatus = new TransactionStatus();
    savedTransactionStatus.setId(ID);
    savedTransactionStatus.setName(NAME);
    savedTransactionStatus.setDescription(DESCRIPTION);

    ResponseEntity<TransactionStatus> responseEntity = resource.createTransactionStatus(savedTransactionStatus);

    verifyZeroInteractions(mockTransactionStatusRepository, mockTransactionStatusSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    TransactionStatus savedTransactionStatus = new TransactionStatus();
    savedTransactionStatus = new TransactionStatus();
    savedTransactionStatus.setId(ID);
    savedTransactionStatus.setName(NAME);
    savedTransactionStatus.setDescription(DESCRIPTION);

    when(mockTransactionStatusRepository.save(savedTransactionStatus)).thenReturn(result);

    ResponseEntity<TransactionStatus> responseEntity = resource.updateTransactionStatus(savedTransactionStatus);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    TransactionStatus savedTransactionStatus = new TransactionStatus();
    savedTransactionStatus = new TransactionStatus();
    savedTransactionStatus.setId(null);
    savedTransactionStatus.setName(NAME);
    savedTransactionStatus.setDescription(DESCRIPTION);

    when(mockTransactionStatusRepository.save(savedTransactionStatus)).thenReturn(result);

    ResponseEntity<TransactionStatus> responseEntity = resource.updateTransactionStatus(savedTransactionStatus);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockTransactionStatusRepository.findAll()).thenReturn(list);

    List<TransactionStatus> transactionStatuses = resource.getAllTransactionStatuses();

    assertThat(transactionStatuses).isNotNull();
    assertThat(transactionStatuses.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockTransactionStatusRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<TransactionStatus> responseEntity = resource.getTransactionStatus(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteTransactionStatus(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockTransactionStatusRepository).delete(ID);
    verify(mockTransactionStatusSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockTransactionStatusSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<TransactionStatus> response = resource.searchTransactionStatuses(query);

    assertThat(response).isNotNull();
  }



}
