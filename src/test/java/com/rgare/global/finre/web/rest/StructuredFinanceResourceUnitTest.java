package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.Reports;
import com.rgare.global.finre.domain.StructuredFinance;
import com.rgare.global.finre.repository.StructuredFinanceRepository;
import com.rgare.global.finre.repository.search.StructuredFinanceSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StructuredFinanceResourceUnitTest {

  private static final Long ID = 100L;
  private static final String COMMENT_YEAR = "COMMENT_YEAR";
  private static final String COMMENT = "COMMENT";

  @Mock
  private StructuredFinanceRepository mockStructuredFinanceRepository;

  @Mock
  private StructuredFinanceSearchRepository mockStructuredFinanceSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private StructuredFinance structuredFinance;

  private StructuredFinance result;

  private StructuredFinanceResource resource;

  private Pageable pageable;
  private PageImpl<StructuredFinance> trackingPage;

  private List<StructuredFinance> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new StructuredFinanceResource(mockStructuredFinanceRepository, mockStructuredFinanceSearchRepository);
    structuredFinance = new StructuredFinance();
    structuredFinance.setReportingDueCommentYr(COMMENT_YEAR);
    structuredFinance.setReportingDueComment(COMMENT);

    result = new StructuredFinance();
    result.setId(ID);
    result.setReportingDueCommentYr(COMMENT_YEAR);
    result.setReportingDueComment(COMMENT);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(structuredFinance);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    StructuredFinance savedStructuredFinance = new StructuredFinance();
    savedStructuredFinance = new StructuredFinance();
    savedStructuredFinance.setReportingDueCommentYr(COMMENT_YEAR);
    savedStructuredFinance.setReportingDueComment(COMMENT);

    when(mockStructuredFinanceRepository.save(savedStructuredFinance)).thenReturn(result);

    ResponseEntity<StructuredFinance> responseEntity = resource.createStructuredFinance(savedStructuredFinance);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    StructuredFinance savedStructuredFinance = new StructuredFinance();
    savedStructuredFinance = new StructuredFinance();
    savedStructuredFinance.setId(ID);
    savedStructuredFinance.setReportingDueCommentYr(COMMENT_YEAR);
    savedStructuredFinance.setReportingDueComment(COMMENT);

    ResponseEntity<StructuredFinance> responseEntity = resource.createStructuredFinance(savedStructuredFinance);

    verifyZeroInteractions(mockStructuredFinanceRepository, mockStructuredFinanceSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    StructuredFinance savedStructuredFinance = new StructuredFinance();
    savedStructuredFinance = new StructuredFinance();
    savedStructuredFinance.setId(ID);
    savedStructuredFinance.setReportingDueCommentYr(COMMENT_YEAR);
    savedStructuredFinance.setReportingDueComment(COMMENT);

    when(mockStructuredFinanceRepository.save(savedStructuredFinance)).thenReturn(result);

    ResponseEntity<StructuredFinance> responseEntity = resource.updateStructuredFinance(savedStructuredFinance);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    StructuredFinance savedStructuredFinance = new StructuredFinance();
    savedStructuredFinance = new StructuredFinance();
    savedStructuredFinance.setId(null);
    savedStructuredFinance.setReportingDueCommentYr(COMMENT_YEAR);
    savedStructuredFinance.setReportingDueComment(COMMENT);

    when(mockStructuredFinanceRepository.save(savedStructuredFinance)).thenReturn(result);

    ResponseEntity<StructuredFinance> responseEntity = resource.updateStructuredFinance(savedStructuredFinance);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockStructuredFinanceRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<StructuredFinance> responseEntity = resource.getStructuredFinance(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockStructuredFinanceRepository.findAll(pageable)).thenReturn(trackingPage);

    ResponseEntity<List<StructuredFinance>> responseEntity = resource.getAllStructuredFinances(pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllByDealSuccessful() throws URISyntaxException {

    when(mockStructuredFinanceRepository.findAllByDealIdAndIsActive(ID,pageable,1)).thenReturn(trackingPage);

    ResponseEntity<List<StructuredFinance>> responseEntity = resource.getStructuredFinanceByDealIdActive(ID,pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteStructuredFinance(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockStructuredFinanceRepository).delete(ID);
    verify(mockStructuredFinanceSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockStructuredFinanceSearchRepository.search(any(QueryBuilder.class), eq(pageable))).thenReturn(trackingPage);

    ResponseEntity<List<StructuredFinance>> response = resource.searchStructuredFinances(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }



}
