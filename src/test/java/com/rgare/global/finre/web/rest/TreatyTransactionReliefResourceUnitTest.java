package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.TreatyTransactionRelief;
import com.rgare.global.finre.repository.TreatyTransactionReliefRepository;
import com.rgare.global.finre.repository.TreatySubtransactionReliefRepository;
import com.rgare.global.finre.repository.search.TreatyTransactionReliefSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TreatyTransactionReliefResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private TreatyTransactionReliefRepository mockTreatyTransactionReliefRepository;

  @Mock
  private TreatyTransactionReliefSearchRepository mockTreatyTransactionReliefSearchRepository;

  @Mock
  private TreatySubtransactionReliefRepository mockTreatySubtransactionReliefRepository;

  private TreatyTransactionRelief treatyTransactionRelief;

  private TreatyTransactionRelief result;

  private TreatyTransactionReliefResource resource;

  private Pageable pageable;
  private PageImpl<TreatyTransactionRelief> trackingPage;

  private List<TreatyTransactionRelief> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new TreatyTransactionReliefResource(mockTreatyTransactionReliefRepository, mockTreatyTransactionReliefSearchRepository, mockTreatySubtransactionReliefRepository);
    treatyTransactionRelief = new TreatyTransactionRelief();
    treatyTransactionRelief.setLastUser(NAME);
    treatyTransactionRelief.setCreationUser(DESCRIPTION);

    result = new TreatyTransactionRelief();
    result.setId(ID);
    result.setLastUser(NAME);
    result.setCreationUser(DESCRIPTION);


    list = Arrays.asList(treatyTransactionRelief);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    TreatyTransactionRelief savedTreatyTransactionRelief = new TreatyTransactionRelief();
    savedTreatyTransactionRelief = new TreatyTransactionRelief();
    savedTreatyTransactionRelief.setLastUser(NAME);
    savedTreatyTransactionRelief.setCreationUser(DESCRIPTION);

    when(mockTreatyTransactionReliefRepository.save(savedTreatyTransactionRelief)).thenReturn(result);

    ResponseEntity<TreatyTransactionRelief> responseEntity = resource.createTreatyTransactionRelief(savedTreatyTransactionRelief);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    TreatyTransactionRelief savedTreatyTransactionRelief = new TreatyTransactionRelief();
    savedTreatyTransactionRelief = new TreatyTransactionRelief();
    savedTreatyTransactionRelief.setId(ID);
    savedTreatyTransactionRelief.setLastUser(NAME);
    savedTreatyTransactionRelief.setCreationUser(DESCRIPTION);

    ResponseEntity<TreatyTransactionRelief> responseEntity = resource.createTreatyTransactionRelief(savedTreatyTransactionRelief);

    verifyZeroInteractions(mockTreatyTransactionReliefRepository, mockTreatyTransactionReliefSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    TreatyTransactionRelief savedTreatyTransactionRelief = new TreatyTransactionRelief();
    savedTreatyTransactionRelief = new TreatyTransactionRelief();
    savedTreatyTransactionRelief.setId(ID);
    savedTreatyTransactionRelief.setLastUser(NAME);
    savedTreatyTransactionRelief.setCreationUser(DESCRIPTION);

    when(mockTreatyTransactionReliefRepository.save(savedTreatyTransactionRelief)).thenReturn(result);

    ResponseEntity<TreatyTransactionRelief> responseEntity = resource.updateTreatyTransactionRelief(savedTreatyTransactionRelief);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    TreatyTransactionRelief savedTreatyTransactionRelief = new TreatyTransactionRelief();
    savedTreatyTransactionRelief = new TreatyTransactionRelief();
    savedTreatyTransactionRelief.setId(null);
    savedTreatyTransactionRelief.setLastUser(NAME);
    savedTreatyTransactionRelief.setCreationUser(DESCRIPTION);

    when(mockTreatyTransactionReliefRepository.save(savedTreatyTransactionRelief)).thenReturn(result);

    ResponseEntity<TreatyTransactionRelief> responseEntity = resource.updateTreatyTransactionRelief(savedTreatyTransactionRelief);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockTreatyTransactionReliefRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<TreatyTransactionRelief> responseEntity = resource.getTreatyTransactionRelief(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockTreatyTransactionReliefRepository.findAll(pageable)).thenReturn(trackingPage);

    ResponseEntity<List<TreatyTransactionRelief>> responseEntity = resource.getAllTreatyTransactionReliefs(pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllByDealSuccessful() throws URISyntaxException {

    when(mockTreatyTransactionReliefRepository.findAllByDealIdAndIsActive(ID,1,pageable)).thenReturn(trackingPage);

    ResponseEntity<List<TreatyTransactionRelief>> responseEntity = resource.getAllByDealId(ID, pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteTreatyTransactionRelief(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockTreatyTransactionReliefRepository).delete(ID);
    verify(mockTreatyTransactionReliefSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockTreatyTransactionReliefSearchRepository.search(any(QueryBuilder.class), eq(pageable))).thenReturn(trackingPage);

    ResponseEntity<List<TreatyTransactionRelief>> response = resource.searchTreatyTransactionReliefs(query,pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }

  @Test
  public void getByDatesData_retreiveByDatesSuccessful() throws URISyntaxException {
    when(mockTreatyTransactionReliefRepository.findAllByDealIdAndIsActiveAndReportingDateBetween(ID, 1,LocalDate.of(2017,2,3),
      LocalDate.of(2017,5,6), pageable)).thenReturn(trackingPage);

    ResponseEntity<List<TreatyTransactionRelief>> responseEntity = resource.getAllByReportingDateDates(ID,2017,2,3,
      2017,5,6,pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }



}
