package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.XrefTrackingTransaction;
import com.rgare.global.finre.repository.XrefTrackingTransactionRepository;
import com.rgare.global.finre.repository.search.XrefTrackingTransactionSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class XrefTrackingTransactionResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private XrefTrackingTransactionRepository mockXrefTrackingTransactionRepository;

  @Mock
  private XrefTrackingTransactionSearchRepository mockXrefTrackingTransactionSearchRepository;

  private XrefTrackingTransaction xrefTrackingTransaction;

  private XrefTrackingTransaction result;

  private XrefTrackingTransactionResource resource;

  private Pageable pageable;
  private PageImpl<XrefTrackingTransaction> trackingPage;

  private List<XrefTrackingTransaction> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new XrefTrackingTransactionResource(mockXrefTrackingTransactionRepository, mockXrefTrackingTransactionSearchRepository);
    xrefTrackingTransaction = new XrefTrackingTransaction();
    xrefTrackingTransaction.setTreatyTrackingId(1L);
    xrefTrackingTransaction.setTreatyTransactionCashId(2L);

    result = new XrefTrackingTransaction();
    result.setId(ID);
    result.setTreatyTrackingId(1L);
    result.setTreatyTransactionCashId(2L);


    list = Arrays.asList(xrefTrackingTransaction);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    XrefTrackingTransaction savedXrefTrackingTransaction = new XrefTrackingTransaction();
    savedXrefTrackingTransaction = new XrefTrackingTransaction();
    savedXrefTrackingTransaction.setTreatyTrackingId(1L);
    savedXrefTrackingTransaction.setTreatyTransactionCashId(2L);

    when(mockXrefTrackingTransactionRepository.save(savedXrefTrackingTransaction)).thenReturn(result);

    ResponseEntity<XrefTrackingTransaction> responseEntity = resource.createXrefTrackingTransaction(savedXrefTrackingTransaction);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    XrefTrackingTransaction savedXrefTrackingTransaction = new XrefTrackingTransaction();
    savedXrefTrackingTransaction = new XrefTrackingTransaction();
    savedXrefTrackingTransaction.setId(ID);
    savedXrefTrackingTransaction.setTreatyTrackingId(1L);
    savedXrefTrackingTransaction.setTreatyTransactionCashId(2L);

    ResponseEntity<XrefTrackingTransaction> responseEntity = resource.createXrefTrackingTransaction(savedXrefTrackingTransaction);

    verifyZeroInteractions(mockXrefTrackingTransactionRepository, mockXrefTrackingTransactionSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    XrefTrackingTransaction savedXrefTrackingTransaction = new XrefTrackingTransaction();
    savedXrefTrackingTransaction = new XrefTrackingTransaction();
    savedXrefTrackingTransaction.setId(ID);
    savedXrefTrackingTransaction.setTreatyTrackingId(1L);
    savedXrefTrackingTransaction.setTreatyTransactionCashId(2L);

    when(mockXrefTrackingTransactionRepository.save(savedXrefTrackingTransaction)).thenReturn(result);

    ResponseEntity<XrefTrackingTransaction> responseEntity = resource.updateXrefTrackingTransaction(savedXrefTrackingTransaction);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    XrefTrackingTransaction savedXrefTrackingTransaction = new XrefTrackingTransaction();
    savedXrefTrackingTransaction = new XrefTrackingTransaction();
    savedXrefTrackingTransaction.setId(null);
    savedXrefTrackingTransaction.setTreatyTrackingId(1L);
    savedXrefTrackingTransaction.setTreatyTransactionCashId(2L);

    when(mockXrefTrackingTransactionRepository.save(savedXrefTrackingTransaction)).thenReturn(result);

    ResponseEntity<XrefTrackingTransaction> responseEntity = resource.updateXrefTrackingTransaction(savedXrefTrackingTransaction);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockXrefTrackingTransactionRepository.findAll()).thenReturn(list);

    List<XrefTrackingTransaction> xrefTrackingTransactions = resource.getAllXrefTrackingTransactions();

    assertThat(xrefTrackingTransactions).isNotNull();
    assertThat(xrefTrackingTransactions.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockXrefTrackingTransactionRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<XrefTrackingTransaction> responseEntity = resource.getXrefTrackingTransaction(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteXrefTrackingTransaction(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockXrefTrackingTransactionRepository).delete(ID);
    verify(mockXrefTrackingTransactionSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockXrefTrackingTransactionSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<XrefTrackingTransaction> response = resource.searchXrefTrackingTransactions(query);

    assertThat(response).isNotNull();
  }



}
