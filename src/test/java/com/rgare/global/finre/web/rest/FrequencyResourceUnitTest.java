package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.Frequency;
import com.rgare.global.finre.repository.FrequencyRepository;
import com.rgare.global.finre.repository.search.FrequencySearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FrequencyResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private FrequencyRepository mockFrequencyRepository;

  @Mock
  private FrequencySearchRepository mockFrequencySearchRepository;

  private Frequency frequency;

  private Frequency result;

  private FrequencyResource resource;

  private Pageable pageable;
  private PageImpl<Frequency> trackingPage;

  private List<Frequency> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new FrequencyResource(mockFrequencyRepository, mockFrequencySearchRepository);
    frequency = new Frequency();
    frequency.setName(NAME);
    frequency.setDescription(DESCRIPTION);

    result = new Frequency();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);


    list = Arrays.asList(frequency);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    Frequency savedAgreementType = new Frequency();
    savedAgreementType = new Frequency();
    savedAgreementType.setName(NAME);
    savedAgreementType.setDescription(DESCRIPTION);

    when(mockFrequencyRepository.save(savedAgreementType)).thenReturn(result);

    ResponseEntity<Frequency> responseEntity = resource.createFrequency(savedAgreementType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    Frequency savedAgreementType = new Frequency();
    savedAgreementType = new Frequency();
    savedAgreementType.setId(ID);
    savedAgreementType.setName(NAME);
    savedAgreementType.setDescription(DESCRIPTION);

    ResponseEntity<Frequency> responseEntity = resource.createFrequency(savedAgreementType);

    verifyZeroInteractions(mockFrequencyRepository, mockFrequencySearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    Frequency savedAgreementType = new Frequency();
    savedAgreementType = new Frequency();
    savedAgreementType.setId(ID);
    savedAgreementType.setName(NAME);
    savedAgreementType.setDescription(DESCRIPTION);

    when(mockFrequencyRepository.save(savedAgreementType)).thenReturn(result);

    ResponseEntity<Frequency> responseEntity = resource.updateFrequency(savedAgreementType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    Frequency savedAgreementType = new Frequency();
    savedAgreementType = new Frequency();
    savedAgreementType.setId(null);
    savedAgreementType.setName(NAME);
    savedAgreementType.setDescription(DESCRIPTION);

    when(mockFrequencyRepository.save(savedAgreementType)).thenReturn(result);

    ResponseEntity<Frequency> responseEntity = resource.updateFrequency(savedAgreementType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockFrequencyRepository.findAll()).thenReturn(list);

    List<Frequency> frequencies = resource.getAllFrequencies();

    assertThat(frequencies).isNotNull();
    assertThat(frequencies.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockFrequencyRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<Frequency> responseEntity = resource.getFrequency(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteFrequency(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockFrequencyRepository).delete(ID);
    verify(mockFrequencySearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockFrequencySearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<Frequency> response = resource.searchFrequencies(query);

    assertThat(response).isNotNull();
  }



}
