package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.GfsAssignee;
import com.rgare.global.finre.repository.GfsAssigneeRepository;
import com.rgare.global.finre.repository.search.GfsAssigneeSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GfsAsigneeResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private GfsAssigneeRepository mockGfsAsigneeRepository;

  @Mock
  private GfsAssigneeSearchRepository mockGfsAsigneeSearchRepository;

  private GfsAssignee gfsAssignee;

  private GfsAssignee result;

  private GfsAssigneeResource resource;

  private Pageable pageable;
  private PageImpl<GfsAssignee> trackingPage;

  private List<GfsAssignee> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new GfsAssigneeResource(mockGfsAsigneeRepository, mockGfsAsigneeSearchRepository);
    gfsAssignee = new GfsAssignee();
    gfsAssignee.setFirstName(NAME);
    gfsAssignee.setDescription(DESCRIPTION);

    result = new GfsAssignee();
    result.setId(ID);
    result.setFirstName(NAME);
    result.setDescription(DESCRIPTION);


    list = Arrays.asList(gfsAssignee);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    GfsAssignee savedGfsAssignee = new GfsAssignee();
    savedGfsAssignee = new GfsAssignee();
    savedGfsAssignee.setFirstName(NAME);
    savedGfsAssignee.setDescription(DESCRIPTION);

    when(mockGfsAsigneeRepository.save(savedGfsAssignee)).thenReturn(result);

    ResponseEntity<GfsAssignee> responseEntity = resource.createGfsAssignee(savedGfsAssignee);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    GfsAssignee savedGfsAssignee = new GfsAssignee();
    savedGfsAssignee = new GfsAssignee();
    savedGfsAssignee.setId(ID);
    savedGfsAssignee.setFirstName(NAME);
    savedGfsAssignee.setDescription(DESCRIPTION);

    ResponseEntity<GfsAssignee> responseEntity = resource.createGfsAssignee(savedGfsAssignee);

    verifyZeroInteractions(mockGfsAsigneeRepository, mockGfsAsigneeSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    GfsAssignee savedGfsAssignee = new GfsAssignee();
    savedGfsAssignee = new GfsAssignee();
    savedGfsAssignee.setId(ID);
    savedGfsAssignee.setFirstName(NAME);
    savedGfsAssignee.setDescription(DESCRIPTION);

    when(mockGfsAsigneeRepository.save(savedGfsAssignee)).thenReturn(result);

    ResponseEntity<GfsAssignee> responseEntity = resource.updateGfsAssignee(savedGfsAssignee);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    GfsAssignee savedGfsAssignee = new GfsAssignee();
    savedGfsAssignee = new GfsAssignee();
    savedGfsAssignee.setId(null);
    savedGfsAssignee.setFirstName(NAME);
    savedGfsAssignee.setDescription(DESCRIPTION);

    when(mockGfsAsigneeRepository.save(savedGfsAssignee)).thenReturn(result);

    ResponseEntity<GfsAssignee> responseEntity = resource.updateGfsAssignee(savedGfsAssignee);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockGfsAsigneeRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<GfsAssignee> responseEntity = resource.getGfsAssignee(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteGfsAssignee(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockGfsAsigneeRepository).delete(ID);
    verify(mockGfsAsigneeSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockGfsAsigneeSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<GfsAssignee> response = resource.searchGfsAssignees(query);

    assertThat(response).isNotNull();
  }



}
