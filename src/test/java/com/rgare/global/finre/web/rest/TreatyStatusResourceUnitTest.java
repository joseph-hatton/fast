package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.TreatyStatus;
import com.rgare.global.finre.repository.TreatyStatusRepository;
import com.rgare.global.finre.repository.search.TreatyStatusSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TreatyStatusResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private TreatyStatusRepository mockTreatyStatusRepository;

  @Mock
  private TreatyStatusSearchRepository mockTreatyStatusSearchRepository;

  private TreatyStatus treatyStatus;

  private TreatyStatus result;

  private TreatyStatusResource resource;

  private Pageable pageable;
  private PageImpl<TreatyStatus> trackingPage;

  private List<TreatyStatus> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new TreatyStatusResource(mockTreatyStatusRepository, mockTreatyStatusSearchRepository);
    treatyStatus = new TreatyStatus();
    treatyStatus.setName(NAME);
    treatyStatus.setDescription(DESCRIPTION);

    result = new TreatyStatus();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);


    list = Arrays.asList(treatyStatus);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    TreatyStatus savedTreatyStatus = new TreatyStatus();
    savedTreatyStatus = new TreatyStatus();
    savedTreatyStatus.setName(NAME);
    savedTreatyStatus.setDescription(DESCRIPTION);

    when(mockTreatyStatusRepository.save(savedTreatyStatus)).thenReturn(result);

    ResponseEntity<TreatyStatus> responseEntity = resource.createTreatyStatus(savedTreatyStatus);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    TreatyStatus savedTreatyStatus = new TreatyStatus();
    savedTreatyStatus = new TreatyStatus();
    savedTreatyStatus.setId(ID);
    savedTreatyStatus.setName(NAME);
    savedTreatyStatus.setDescription(DESCRIPTION);

    ResponseEntity<TreatyStatus> responseEntity = resource.createTreatyStatus(savedTreatyStatus);

    verifyZeroInteractions(mockTreatyStatusRepository, mockTreatyStatusSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    TreatyStatus savedTreatyStatus = new TreatyStatus();
    savedTreatyStatus = new TreatyStatus();
    savedTreatyStatus.setId(ID);
    savedTreatyStatus.setName(NAME);
    savedTreatyStatus.setDescription(DESCRIPTION);

    when(mockTreatyStatusRepository.save(savedTreatyStatus)).thenReturn(result);

    ResponseEntity<TreatyStatus> responseEntity = resource.updateTreatyStatus(savedTreatyStatus);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    TreatyStatus savedTreatyStatus = new TreatyStatus();
    savedTreatyStatus = new TreatyStatus();
    savedTreatyStatus.setId(null);
    savedTreatyStatus.setName(NAME);
    savedTreatyStatus.setDescription(DESCRIPTION);

    when(mockTreatyStatusRepository.save(savedTreatyStatus)).thenReturn(result);

    ResponseEntity<TreatyStatus> responseEntity = resource.updateTreatyStatus(savedTreatyStatus);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockTreatyStatusRepository.findAll()).thenReturn(list);

    List<TreatyStatus> treatyStatuses = resource.getAllTreatyStatuses();

    assertThat(treatyStatuses).isNotNull();
    assertThat(treatyStatuses.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockTreatyStatusRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<TreatyStatus> responseEntity = resource.getTreatyStatus(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteTreatyStatus(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockTreatyStatusRepository).delete(ID);
    verify(mockTreatyStatusSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockTreatyStatusSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<TreatyStatus> response = resource.searchTreatyStatuses(query);

    assertThat(response).isNotNull();
  }



}
