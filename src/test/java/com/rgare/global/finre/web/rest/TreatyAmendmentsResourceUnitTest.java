package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.TreatyAmendments;
import com.rgare.global.finre.repository.TreatyAmendmentsRepository;
import com.rgare.global.finre.repository.search.TreatyAmendmentsSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TreatyAmendmentsResourceUnitTest {

  private static final Long ID = 100L;
  private static final String REASON = "REASON";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private TreatyAmendmentsRepository mockTreatyAmendmentsRepository;

  @Mock
  private TreatyAmendmentsSearchRepository mockTreatyAmendmentsSearchRepository;

  private TreatyAmendments treatyAmendments;

  private TreatyAmendments result;

  private TreatyAmendmentsResource resource;

  private Pageable pageable;
  private PageImpl<TreatyAmendments> trackingPage;

  private List<TreatyAmendments> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new TreatyAmendmentsResource(mockTreatyAmendmentsRepository, mockTreatyAmendmentsSearchRepository);
    treatyAmendments = new TreatyAmendments();
    treatyAmendments.setReason(REASON);
    treatyAmendments.setComments(DESCRIPTION);

    result = new TreatyAmendments();
    result.setId(ID);
    result.setReason(REASON);
    result.setComments(DESCRIPTION);


    list = Arrays.asList(treatyAmendments);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    TreatyAmendments savedTreatyAmendments = new TreatyAmendments();
    savedTreatyAmendments = new TreatyAmendments();
    savedTreatyAmendments.setReason(REASON);
    savedTreatyAmendments.setComments(DESCRIPTION);

    when(mockTreatyAmendmentsRepository.save(savedTreatyAmendments)).thenReturn(result);

    ResponseEntity<TreatyAmendments> responseEntity = resource.createTreatyAmendments(savedTreatyAmendments);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    TreatyAmendments savedTreatyAmendments = new TreatyAmendments();
    savedTreatyAmendments = new TreatyAmendments();
    savedTreatyAmendments.setId(ID);
    savedTreatyAmendments.setReason(REASON);
    savedTreatyAmendments.setComments(DESCRIPTION);

    ResponseEntity<TreatyAmendments> responseEntity = resource.createTreatyAmendments(savedTreatyAmendments);

    verifyZeroInteractions(mockTreatyAmendmentsRepository, mockTreatyAmendmentsSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    TreatyAmendments savedTreatyAmendments = new TreatyAmendments();
    savedTreatyAmendments = new TreatyAmendments();
    savedTreatyAmendments.setId(ID);
    savedTreatyAmendments.setReason(REASON);
    savedTreatyAmendments.setComments(DESCRIPTION);

    when(mockTreatyAmendmentsRepository.save(savedTreatyAmendments)).thenReturn(result);

    ResponseEntity<TreatyAmendments> responseEntity = resource.updateTreatyAmendments(savedTreatyAmendments);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    TreatyAmendments savedTreatyAmendments = new TreatyAmendments();
    savedTreatyAmendments = new TreatyAmendments();
    savedTreatyAmendments.setId(null);
    savedTreatyAmendments.setReason(REASON);
    savedTreatyAmendments.setComments(DESCRIPTION);

    when(mockTreatyAmendmentsRepository.save(savedTreatyAmendments)).thenReturn(result);

    ResponseEntity<TreatyAmendments> responseEntity = resource.updateTreatyAmendments(savedTreatyAmendments);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockTreatyAmendmentsRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<TreatyAmendments> responseEntity = resource.getTreatyAmendments(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockTreatyAmendmentsRepository.findAllByIsActive(1,pageable)).thenReturn(trackingPage);

    ResponseEntity<List<TreatyAmendments>> responseEntity = resource.getAllTreatyAmendments(pageable);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllNoPaginationSuccessful() throws URISyntaxException {

    when(mockTreatyAmendmentsRepository.findAllByIsActive(1)).thenReturn(list);

    ResponseEntity<List<TreatyAmendments>> responseEntity = resource.getAllAmendmentsNoPages();

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllNoPageData_retreiveAllNoPaginationSuccessful() throws URISyntaxException {

    when(mockTreatyAmendmentsRepository.findAll()).thenReturn(list);

    ResponseEntity<List<TreatyAmendments>> responseEntity = resource.getAllAmendments();

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllByTreatiesSuccessful() throws URISyntaxException {

    when(mockTreatyAmendmentsRepository.findAllByTreatyIdAndIsActive(ID,1, pageable)).thenReturn(trackingPage);

    ResponseEntity<List<TreatyAmendments>> responseEntity = resource.getAllByTreatyId(ID, pageable.getSort());

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteTreatyAmendments(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockTreatyAmendmentsRepository).delete(ID);
    verify(mockTreatyAmendmentsSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockTreatyAmendmentsSearchRepository.search(any(QueryBuilder.class),eq(pageable))).thenReturn(trackingPage);

    ResponseEntity<List<TreatyAmendments>> response = resource.searchTreatyAmendments(query, pageable);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(list);

    assertThat(response).isNotNull();
  }



}
