package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.MdmAttributes;
import com.rgare.global.finre.repository.MdmAttributesRepository;
import com.rgare.global.finre.repository.search.MdmAttributesSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MdmAttributesResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String LEGALNAME = "LEGALNAME";

  @Mock
  private MdmAttributesRepository mockMdmAttributesRepository;

  @Mock
  private MdmAttributesSearchRepository mockMdmAttributesSearchRepository;

  private MdmAttributes mdmAttributes;

  private MdmAttributes result;

  private MdmAttributesResource resource;

  private Pageable pageable;
  private PageImpl<MdmAttributes> trackingPage;

  private List<MdmAttributes> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new MdmAttributesResource(mockMdmAttributesRepository, mockMdmAttributesSearchRepository);
    mdmAttributes = new MdmAttributes();
    mdmAttributes.setClientCompanyName(NAME);
    mdmAttributes.setLegalName(LEGALNAME);

    result = new MdmAttributes();
    result.setId(ID);
    result.setClientCompanyName(NAME);
    result.setLegalName(LEGALNAME);


    list = Arrays.asList(mdmAttributes);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    MdmAttributes savedMdmAttributes = new MdmAttributes();
    savedMdmAttributes = new MdmAttributes();
    savedMdmAttributes.setClientCompanyName(NAME);
    savedMdmAttributes.setLegalName(LEGALNAME);

    when(mockMdmAttributesRepository.save(savedMdmAttributes)).thenReturn(result);

    ResponseEntity<MdmAttributes> responseEntity = resource.createMdmAttributes(savedMdmAttributes);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    MdmAttributes savedMdmAttributes = new MdmAttributes();
    savedMdmAttributes = new MdmAttributes();
    savedMdmAttributes.setId(ID);
    savedMdmAttributes.setClientCompanyName(NAME);
    savedMdmAttributes.setLegalName(LEGALNAME);

    ResponseEntity<MdmAttributes> responseEntity = resource.createMdmAttributes(savedMdmAttributes);

    verifyZeroInteractions(mockMdmAttributesRepository, mockMdmAttributesSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    MdmAttributes savedMdmAttributes = new MdmAttributes();
    savedMdmAttributes = new MdmAttributes();
    savedMdmAttributes.setId(ID);
    savedMdmAttributes.setClientCompanyName(NAME);
    savedMdmAttributes.setLegalName(LEGALNAME);

    when(mockMdmAttributesRepository.save(savedMdmAttributes)).thenReturn(result);

    ResponseEntity<MdmAttributes> responseEntity = resource.updateMdmAttributes(savedMdmAttributes);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }


  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    MdmAttributes savedMdmAttributes = new MdmAttributes();
    savedMdmAttributes = new MdmAttributes();
    savedMdmAttributes.setId(null);
    savedMdmAttributes.setClientCompanyName(NAME);
    savedMdmAttributes.setLegalName(LEGALNAME);

    when(mockMdmAttributesRepository.save(savedMdmAttributes)).thenReturn(result);

    ResponseEntity<MdmAttributes> responseEntity = resource.updateMdmAttributes(savedMdmAttributes);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockMdmAttributesRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<MdmAttributes> responseEntity = resource.getMdmAttributes(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockMdmAttributesRepository.findAll()).thenReturn(list);

    List<MdmAttributes> allMdmAttributes = resource.getAllMdmAttributes();

    assertThat(allMdmAttributes).isNotNull();
  }

  @Test
  public void getAllData_retreiveAllByEnterpriseKeySuccessful() throws URISyntaxException {

    when(mockMdmAttributesRepository.findAllByEnterpriseId(ID)).thenReturn(list);

    ResponseEntity< List<MdmAttributes>> allMdmAttributes = resource.getAllMdmAttributes(ID);

    assertThat(allMdmAttributes).isNotNull();
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteMdmAttributes(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockMdmAttributesRepository).delete(ID);
    verify(mockMdmAttributesSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockMdmAttributesSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<MdmAttributes> response = resource.searchMdmAttributes(query);

    assertThat(response).isNotNull();
  }



}
