package com.rgare.global.finre.web.rest.errors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class FieldErrorVMUnitTest {

  private static final Long ID = 100L;

  private final String OBJECT_NAME = "OBJECT_NAME";
  private final String MESSAGE = "MESSAGE";
  private final String FIELD = "FIELD";

  private FieldErrorVM fieldErrorVM;
  @Before
  public void setup() throws URISyntaxException {
  }

  @Test
  public void getData_getThreeParameterSuccessful() throws URISyntaxException {
    fieldErrorVM = new FieldErrorVM(OBJECT_NAME,FIELD,MESSAGE);
    assertThat(fieldErrorVM.getMessage()).isEqualTo(MESSAGE);
    assertThat(fieldErrorVM.getField()).isEqualTo(FIELD);
    assertThat(fieldErrorVM.getObjectName()).isEqualTo(OBJECT_NAME);
  }

}
