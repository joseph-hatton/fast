package com.rgare.global.finre.web.rest.vm;

import com.rgare.global.finre.web.rest.errors.FieldErrorVM;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URISyntaxException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class KeyAndPasswordVMUnitTest {

  private static final Long ID = 100L;

  private final String KEY = "KEY";
  private final String PWD = "PWD";

  @Before
  public void setup() throws URISyntaxException {
  }

  @Test
  public void getData_testSetters(){
    KeyAndPasswordVM keyAndPasswordVM = new KeyAndPasswordVM();
    keyAndPasswordVM.setKey(KEY);
    keyAndPasswordVM.setNewPassword(PWD);
    assertThat(keyAndPasswordVM.getKey()).isEqualTo(KEY);
    assertThat(keyAndPasswordVM.getNewPassword()).isEqualTo(PWD);
  }

}
