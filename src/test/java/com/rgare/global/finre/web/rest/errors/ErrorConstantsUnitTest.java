package com.rgare.global.finre.web.rest.errors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ErrorConstantsUnitTest {

  @Test
  public void createConstantsTest(){


    String ERR_CONCURRENCY_FAILURE = "error.concurrencyFailure";
    String ERR_ACCESS_DENIED = "error.accessDenied";
    String ERR_VALIDATION = "error.validation";
    String ERR_METHOD_NOT_SUPPORTED = "error.methodNotSupported";
    String ERR_INTERNAL_SERVER_ERROR = "error.internalServerError";

    ErrorConstants constants = new ErrorConstants();
    assertThat(constants).isNotNull();

    assertThat(constants.ERR_CONCURRENCY_FAILURE).isEqualTo(ERR_CONCURRENCY_FAILURE);
    assertThat(constants.ERR_ACCESS_DENIED).isEqualTo(ERR_ACCESS_DENIED);

  }
}
