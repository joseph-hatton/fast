package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.SettlementType;
import com.rgare.global.finre.repository.SettlementTypeRepository;
import com.rgare.global.finre.repository.search.SettlementTypeSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SettlementTypeResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private SettlementTypeRepository mockSettlementTypeRepository;

  @Mock
  private SettlementTypeSearchRepository mockSettlementTypeSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private SettlementType settlementType;

  private SettlementType result;

  private SettlementTypeResource resource;

  private Pageable pageable;
  private PageImpl<SettlementType> trackingPage;

  private List<SettlementType> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new SettlementTypeResource(mockSettlementTypeRepository, mockSettlementTypeSearchRepository);
    settlementType = new SettlementType();
    settlementType.setName(NAME);
    settlementType.setDescription(DESCRIPTION);

    result = new SettlementType();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(settlementType);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    SettlementType savedSettlementType = new SettlementType();
    savedSettlementType = new SettlementType();
    savedSettlementType.setName(NAME);
    savedSettlementType.setDescription(DESCRIPTION);

    when(mockSettlementTypeRepository.save(savedSettlementType)).thenReturn(result);

    ResponseEntity<SettlementType> responseEntity = resource.createSettlementType(savedSettlementType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    SettlementType savedSettlementType = new SettlementType();
    savedSettlementType = new SettlementType();
    savedSettlementType.setId(ID);
    savedSettlementType.setName(NAME);
    savedSettlementType.setDescription(DESCRIPTION);

    ResponseEntity<SettlementType> responseEntity = resource.createSettlementType(savedSettlementType);

    verifyZeroInteractions(mockSettlementTypeRepository, mockSettlementTypeSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    SettlementType savedSettlementType = new SettlementType();
    savedSettlementType = new SettlementType();
    savedSettlementType.setId(ID);
    savedSettlementType.setName(NAME);
    savedSettlementType.setDescription(DESCRIPTION);

    when(mockSettlementTypeRepository.save(savedSettlementType)).thenReturn(result);

    ResponseEntity<SettlementType> responseEntity = resource.updateSettlementType(savedSettlementType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    SettlementType savedSettlementType = new SettlementType();
    savedSettlementType = new SettlementType();
    savedSettlementType.setId(null);
    savedSettlementType.setName(NAME);
    savedSettlementType.setDescription(DESCRIPTION);

    when(mockSettlementTypeRepository.save(savedSettlementType)).thenReturn(result);

    ResponseEntity<SettlementType> responseEntity = resource.updateSettlementType(savedSettlementType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockSettlementTypeRepository.findAll()).thenReturn(list);

    List<SettlementType> settlementTypes = resource.getAllSettlementTypes();

    assertThat(settlementTypes).isNotNull();
    assertThat(settlementTypes.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockSettlementTypeRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<SettlementType> responseEntity = resource.getSettlementType(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteSettlementType(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockSettlementTypeRepository).delete(ID);
    verify(mockSettlementTypeSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockSettlementTypeSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<SettlementType> response = resource.searchSettlementTypes(query);

    assertThat(response).isNotNull();
  }



}
