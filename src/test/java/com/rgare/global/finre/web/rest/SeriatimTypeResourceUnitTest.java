package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.SeriatimType;
import com.rgare.global.finre.repository.SeriatimTypeRepository;
import com.rgare.global.finre.repository.search.SeriatimTypeSearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SeriatimTypeResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String DESCRIPTION = "DESCRIPTION";

  @Mock
  private SeriatimTypeRepository mockSeriatimTypeRepository;

  @Mock
  private SeriatimTypeSearchRepository mockSeriatimTypeSearchRepository;

  @Mock
  private Dp3LogService mockCDp3LogService;


  private SeriatimType seriatimType;

  private SeriatimType result;

  private SeriatimTypeResource resource;

  private Pageable pageable;
  private PageImpl<SeriatimType> trackingPage;

  private List<SeriatimType> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new SeriatimTypeResource(mockSeriatimTypeRepository, mockSeriatimTypeSearchRepository);
    seriatimType = new SeriatimType();
    seriatimType.setName(NAME);
    seriatimType.setDescription(DESCRIPTION);

    result = new SeriatimType();
    result.setId(ID);
    result.setName(NAME);
    result.setDescription(DESCRIPTION);

    pageable = new PageRequest(1, 5);


    list = Arrays.asList(seriatimType);
    trackingPage = new PageImpl<>(list);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    SeriatimType savedSeriatimType = new SeriatimType();
    savedSeriatimType = new SeriatimType();
    savedSeriatimType.setName(NAME);
    savedSeriatimType.setDescription(DESCRIPTION);

    when(mockSeriatimTypeRepository.save(savedSeriatimType)).thenReturn(result);

    ResponseEntity<SeriatimType> responseEntity = resource.createSeriatimType(savedSeriatimType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    SeriatimType savedSeriatimType = new SeriatimType();
    savedSeriatimType = new SeriatimType();
    savedSeriatimType.setId(ID);
    savedSeriatimType.setName(NAME);
    savedSeriatimType.setDescription(DESCRIPTION);

    ResponseEntity<SeriatimType> responseEntity = resource.createSeriatimType(savedSeriatimType);

    verifyZeroInteractions(mockSeriatimTypeRepository, mockSeriatimTypeSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    SeriatimType savedSeriatimType = new SeriatimType();
    savedSeriatimType = new SeriatimType();
    savedSeriatimType.setId(ID);
    savedSeriatimType.setName(NAME);
    savedSeriatimType.setDescription(DESCRIPTION);

    when(mockSeriatimTypeRepository.save(savedSeriatimType)).thenReturn(result);

    ResponseEntity<SeriatimType> responseEntity = resource.updateSeriatimType(savedSeriatimType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    SeriatimType savedSeriatimType = new SeriatimType();
    savedSeriatimType = new SeriatimType();
    savedSeriatimType.setId(null);
    savedSeriatimType.setName(NAME);
    savedSeriatimType.setDescription(DESCRIPTION);

    when(mockSeriatimTypeRepository.save(savedSeriatimType)).thenReturn(result);

    ResponseEntity<SeriatimType> responseEntity = resource.updateSeriatimType(savedSeriatimType);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockSeriatimTypeRepository.findAll()).thenReturn(list);

    List<SeriatimType> seriatimTypes = resource.getAllSeriatimTypes();

    assertThat(seriatimTypes).isNotNull();
    assertThat(seriatimTypes.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockSeriatimTypeRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<SeriatimType> responseEntity = resource.getSeriatimType(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteSeriatimType(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockSeriatimTypeRepository).delete(ID);
    verify(mockSeriatimTypeSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockSeriatimTypeSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<SeriatimType> response = resource.searchSeriatimTypes(query);

    assertThat(response).isNotNull();
  }



}
