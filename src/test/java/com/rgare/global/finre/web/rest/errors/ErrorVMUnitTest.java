package com.rgare.global.finre.web.rest.errors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ErrorVMUnitTest {

  private static final Long ID = 100L;

  private final String DTO = "DTO";
  private final String MESSAGE = "MESSAGE";
  private final String DESCRIPTION = "DESCRIPTION";

  private List<FieldErrorVM> fieldErrors;

  private ErrorVM errorVM;

  @Before
  public void setup() throws URISyntaxException {
    fieldErrors = new ArrayList<>();
    fieldErrors.add(new FieldErrorVM(DTO,MESSAGE,DESCRIPTION));
  }

  @Test
  public void getData_getOneParameterSuccessful() throws URISyntaxException {
    errorVM = new ErrorVM(MESSAGE);
    assertThat(errorVM.getMessage()).isEqualTo(MESSAGE);
    assertThat(errorVM.getDescription()).isNull();
  }

  @Test
  public void getData_getThreeParameterSuccessful() throws URISyntaxException {
    errorVM = new ErrorVM(MESSAGE, DESCRIPTION,fieldErrors);
    assertThat(errorVM.getMessage()).isEqualTo(MESSAGE);
    assertThat(errorVM.getDescription()).isNotBlank();
    assertThat(errorVM.getFieldErrors()).isEqualTo(fieldErrors);
  }

  @Test
  public void addData_addSuccessful() throws URISyntaxException {
    errorVM = new ErrorVM(MESSAGE, DESCRIPTION,fieldErrors);
    errorVM.add(DTO,DESCRIPTION,MESSAGE);
    assertThat(errorVM.getMessage()).isEqualTo(MESSAGE);
    assertThat(errorVM.getDescription()).isNotBlank();
    assertThat(errorVM.getFieldErrors()).isEqualTo(fieldErrors);
  }

  @Test
  public void addData_addWhereErrorNullSuccessful() throws URISyntaxException {
    errorVM = new ErrorVM(MESSAGE, DESCRIPTION,null);
    errorVM.add(DTO,DESCRIPTION,MESSAGE);
    assertThat(errorVM.getMessage()).isEqualTo(MESSAGE);
    assertThat(errorVM.getDescription()).isNotBlank();
    assertThat(errorVM.getFieldErrors()).isNotNull();
  }
}
