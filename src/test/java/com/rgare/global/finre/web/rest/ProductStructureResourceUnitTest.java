package com.rgare.global.finre.web.rest;

import com.rgare.global.finre.domain.ProductStructure;
import com.rgare.global.finre.repository.ProductStructureRepository;
import com.rgare.global.finre.repository.search.ProductStructureSearchRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductStructureResourceUnitTest {

  private static final Long ID = 100L;
  private static final String NAME = "NAME";
  private static final String CODE = "CODE";

  @Mock
  private ProductStructureRepository mockProductStructureRepository;

  @Mock
  private ProductStructureSearchRepository mockProductStructureSearchRepository;

  private ProductStructure productStructure;

  private ProductStructure result;

  private ProductStructureResource resource;

  private Pageable pageable;
  private PageImpl<ProductStructure> trackingPage;

  private List<ProductStructure> list;

  @Before
  public void setup() throws URISyntaxException {
    resource = new ProductStructureResource(mockProductStructureRepository, mockProductStructureSearchRepository);
    productStructure = new ProductStructure();
    productStructure.setName(NAME);
    productStructure.setStructureCode(CODE);

    result = new ProductStructure();
    result.setId(ID);
    result.setName(NAME);
    result.setStructureCode(CODE);


    list = Arrays.asList(productStructure);
    trackingPage = new PageImpl<>(list);

    pageable = new PageRequest(1, 5);

  }


  @Test
  public void saveData_saveSuccessful() throws URISyntaxException {
    ProductStructure savedProductStructure = new ProductStructure();
    savedProductStructure = new ProductStructure();
    savedProductStructure.setName(NAME);
    savedProductStructure.setStructureCode(CODE);

    when(mockProductStructureRepository.save(savedProductStructure)).thenReturn(result);

    ResponseEntity<ProductStructure> responseEntity = resource.createProductStructure(savedProductStructure);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void saveData_idAlreadyExist() throws URISyntaxException {
    ProductStructure savedProductStructure = new ProductStructure();
    savedProductStructure = new ProductStructure();
    savedProductStructure.setId(ID);
    savedProductStructure.setName(NAME);
    savedProductStructure.setStructureCode(CODE);

    ResponseEntity<ProductStructure> responseEntity = resource.createProductStructure(savedProductStructure);

    verifyZeroInteractions(mockProductStructureRepository, mockProductStructureSearchRepository);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  public void updateData_updateSuccessful() throws URISyntaxException {
    ProductStructure savedProductStructure = new ProductStructure();
    savedProductStructure = new ProductStructure();
    savedProductStructure.setId(ID);
    savedProductStructure.setName(NAME);
    savedProductStructure.setStructureCode(CODE);

    when(mockProductStructureRepository.save(savedProductStructure)).thenReturn(result);

    ResponseEntity<ProductStructure> responseEntity = resource.updateProductStructure(savedProductStructure);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void updateData_updateWhereIDIsNullSuccessful() throws URISyntaxException {
    ProductStructure savedProductStructure = new ProductStructure();
    savedProductStructure = new ProductStructure();
    savedProductStructure.setId(null);
    savedProductStructure.setName(NAME);
    savedProductStructure.setStructureCode(CODE);

    when(mockProductStructureRepository.save(savedProductStructure)).thenReturn(result);

    ResponseEntity<ProductStructure> responseEntity = resource.updateProductStructure(savedProductStructure);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void getAllData_retreiveAllSuccessful() throws URISyntaxException {

    when(mockProductStructureRepository.findAll()).thenReturn(list);

    List<ProductStructure> productStructures = resource.getAllProductStructures();

    assertThat(productStructures).isNotNull();
    assertThat(productStructures.size()).isGreaterThan(0);
  }

  @Test
  public void getOneData_retreiveOneSuccessful() throws URISyntaxException {

    when(mockProductStructureRepository.findOne(ID)).thenReturn(result);

    ResponseEntity<ProductStructure> responseEntity = resource.getProductStructure(ID);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void deleteOneData_deleteOneSuccessful() throws URISyntaxException {
    ResponseEntity<Void> response = resource.deleteProductStructure(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    verify(mockProductStructureRepository).delete(ID);
    verify(mockProductStructureSearchRepository).delete(ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test
  public void searchTreatyTrackings_successful() throws URISyntaxException {
    String query = "a=b";
    when(mockProductStructureSearchRepository.search(any(QueryBuilder.class))).thenReturn(trackingPage);

    List<ProductStructure> response = resource.searchProductStructures(query);

    assertThat(response).isNotNull();
  }



}
