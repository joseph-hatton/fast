package com.rgare.global.finre.web.rest.errors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ParameterizedErrorVMUnitTest {

  private static final Long ID = 100L;

  private final String MESSAGE = "MESSAGE";

  private Map<String, String> paramMap;

  private ParameterizedErrorVM parameterizedErrorVM;

  @Before
  public void setup() throws URISyntaxException {
    paramMap = new HashMap<>();
    paramMap.put(MESSAGE,MESSAGE);
  }

  @Test
  public void getData_getParameterizedSuccessful() throws URISyntaxException {
    parameterizedErrorVM = new ParameterizedErrorVM(MESSAGE,paramMap);
    assertThat(parameterizedErrorVM.getMessage()).isEqualTo(MESSAGE);
    assertThat(parameterizedErrorVM.getParams()).isEqualTo(paramMap);
  }

}
