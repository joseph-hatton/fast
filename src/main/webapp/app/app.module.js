var currentDocumentUpload, currentSelectionUpload;
(function() {
    'use strict';

    angular
        .module('finreApp', [
            'ngAnimate',
            'ui.bootstrap',
            'ui.bootstrap.datetimepicker',
            'ngStorage',
            'ui.utils.masks',
            'tmh.dynamicLocale',
            'pascalprecht.translate',
            'ngResource',
            'ngCookies',
            'ngAria',
            'ngMessages',
            'ngCacheBuster',
            'ui.router',
            'infinite-scroll',
            'ngMaterial',
            'base64',
            // jhipster-needle-angularjs-add-module JHipster will add new module here
            'angular-loading-bar',
            'ngSanitize',
            'textAngular'
        ])
        .config(config)
        .run(run);    
        
    run.$inject = ['stateHandler', 'translationHandler'];

    function run(stateHandler, translationHandler) {
        stateHandler.initialize();
        translationHandler.initialize();
    }

    config.$inject = ['$mdThemingProvider', 'taRegisterTool'];

    function config ($mdThemingProvider, taRegisterTool){
        $mdThemingProvider
        .theme("RGA")
        .primaryPalette('red')
        .accentPalette('grey')
        .warnPalette('deep-orange');

        taRegisterTool('imageUpload', {
            iconclass: 'fa fa-image',
            tooltipText: 'Upload Image',
            action: function(){
                currentDocumentUpload = this.$editor().$parent.$parent.document;
                currentSelectionUpload = window.getSelection();
                angular.element(document.querySelector("#imageUploader")).click();
            }
        });
    }
})();
