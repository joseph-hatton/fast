(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('NavbarController', NavbarController);

  NavbarController.$inject = ['$state', '$window', '$stateParams', '$http', '$timeout', 'Auth', 'Principal', 'ProfileService', 'LoginService', 'CurrentUser', 'InteractionsService', 'AuthServerProvider', '$mdDialog', '$sce'];

  function NavbarController($state, $window, $stateParams, $http, $timeout, Auth, Principal, ProfileService, LoginService, CurrentUser, InteractionsService, AuthServerProvider, $mdDialog, $sce) {
    var vm = this;
    vm.isNavbarCollapsed = true;
    vm.isAuthenticated = Principal.isAuthenticated;
    vm.login = login;
    vm.logout = logout;
    vm.toggleNavbar = toggleNavbar;
    vm.collapseNavbar = collapseNavbar;
    vm.$state = $state;
    vm.dealPage = $window.localStorage.pageDeal;
    vm.authority = null;
    vm.currentUser = null;

    Principal.identity().then(function (account) {
      if (account !== null) {
        angular.forEach(account.authorities, function (authority) {
          if (authority === "ROLE_APP_FINRE_OPS_ADMIN") {
            vm.authority = authority;
          }
        });
        vm.eamTeam = account.authorities.some(function(authority){ return authority === "ROLE_EAM_TEAM"; });
        vm.itSupport = account.authorities.some(function(authority){ return authority === "ROLE_APP_FINRE_IT_SUPPORT"; });
      }
    });

    ProfileService.getProfileInfo().then(function (response) {
      vm.activeProfiles = response.activeProfiles;
      vm.inProduction = response.inProduction;
      vm.swaggerEnabled = response.swaggerEnabled;
    });

    // Refreshes the oauth token!
    function getCurrentUser() {
      vm.repeatInterval = 60 * 60 * 1000;
      CurrentUser.get().$promise
        .then(function (currentUser) {
          vm.currentUser = currentUser;
          if (!vm.popupOpen && vm.currentUser.session.stale) {
            vm.popupOpen = true;
            var dialog = InteractionsService.confirm("You're session is old. Logout now or Cancel and save changes, then logout.", 'body', 'Logout Now', 'Later');
            InteractionsService.showThenDialog(dialog, function () {
              vm.popupOpen = false;
              logout();
            }, function () {
              vm.popupOpen = false;
            });
          }
          $timeout(getCurrentUser, vm.repeatInterval);
        })
        .catch(function (err) {
          if (vm.activeProfiles && vm.activeProfiles.includes('local')) {
            console.log("Expected error getting current user, we are running `local`ly and current user is hard-coded in the app");
          } else {
            if (vm.currentUser) {
              var dialog = InteractionsService.alertDialog("You're session has expired. You've been logged off.", 'body');
              InteractionsService.showThenDialog(dialog, function () {
                logout();
              });
            } else {
              $timeout(getCurrentUser, vm.repeatInterval);
            }
          }
        });
    }

    function login() {
      collapseNavbar();
      LoginService.open();
    }

    function logout() {
      AuthServerProvider.logout()
      .then(function (logoutUrl) {
        vm.logoutUrl = $sce.trustAsResourceUrl(logoutUrl);
        $timeout(function () {
          $window.location.reload();
        }, 1000);
      });
    }

    function toggleNavbar() {
      vm.isNavbarCollapsed = !vm.isNavbarCollapsed;
    }

    function collapseNavbar() {
      vm.isNavbarCollapsed = true;
    }

    getCurrentUser();
  }
})();
