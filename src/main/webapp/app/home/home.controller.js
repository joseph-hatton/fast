(function () {
  'use strict';

  angular
      .module('finreApp')
      .controller('HomeController', HomeController);

  HomeController.$inject = ['$scope', '$window', '$state', 'Principal', 'LoginService', 'TreatyAmendmentsNoPage',
      'TreatySrv', 'TreatyTrackingCustomSvr', 'GfsAssignee', 'TreatyTransactionSrv', 'TreatyTransaction', 'Intro', '$q', 'TreatyManagementCustomSrv', 'InteractionsService', 'TreatyTracking', '$log', '$http'
  ];

  function HomeController($scope, $window, $state, Principal, LoginService, TreatyAmendmentsNoPage,
      TreatySrv, TreatyTrackingCustomSvr, GfsAssignee, TreatyTransactionSrv, TreatyTransaction, Intro, $q, TreatyManagementCustomSrv, InteractionsService, TreatyTracking, $log, $http) {
      var vm = this;

      vm.account = null;
      vm.isAuthenticated = null;
      vm.authority = null;
      vm.login = LoginService.open;
      vm.amedments = [];
      vm.newAmendmentNotify = [];
      vm.AmendmentNotify = [];
      vm.deleteAmendmentNotify = [];
      vm.completeTreatyNotify = [];
      vm.expirationAssumingTracking = [];
      vm.expirationCedingTracking = [];
      vm.expirationSeriatim = [];
      vm.reportingDueDateExpired = [];
      vm.settlementAssumingExpired = [];
      vm.settlementCedingExpired = [];
      vm.seriatimExpired = [];
      vm.completeTreatyNotify = [];
      vm.expirationReportingDue = [];
      vm.clearedReportingNotifications = [];
      vm.deletedTransaction = [];
      vm.assumingDates = [];
      vm.cedingDates = [];
      vm.seriatimDates = [];

      vm.settlementAlmostExpired = [];
      vm.settlementAlmostPastDue = [];
      vm.settlementExpired = [];

      vm.assumingAlmostExpired = [];
      vm.assumingAlmostPastDue = [];
      vm.assumingExpired = [];
      vm.cedingAlmostExpired = [];
      vm.cedingAlmostPastDue = [];
      vm.cedingExpired = [];

      Intro.setupIntroSteps({
          id: 1,
          className: "homeStep"
      })
      .then(function (result) {
          vm.introOptions = {
              steps: result
          };
      });

      $window.localStorage.clear();
      $window.localStorage.localStorage = -1;

      $scope.$on('authenticationSuccess', function () {
          $state.reload();
          getAccount();
      });

      $scope.closeAlert = function (index) {
          vm.newAmendmentNotify.splice(index, 1);
      };



      vm.getTracking = function () {
          GfsAssignee.query(function (result) {
            vm.GFSAssignees = result;
            angular.forEach(result, function (gfs, key) {
                vm.expirationReportingDue[key] = {
                    name: gfs.firstName,
                    dates: [],
                    status: false,
                    datesEX: [],
                    datesExpGracePeriod: [],
                    datesExpCashDueNo: [],
                    datesExpCashDueYesNoGracePeriod: []
                };
                vm.clearedReportingNotifications[key] = {
                    name: gfs.firstName,
                    dates: [],
                    status: false
                };
                vm.assumingDates[key] = {
                    name: gfs.firstName,
                    dates: [],
                    status: false,
                    datesEX: []
                };
                vm.cedingDates[key] = {
                    name: gfs.firstName,
                    dates: [],
                    status: false,
                    datesEX: []
                };
                vm.seriatimDates[key] = {
                    name: gfs.firstName,
                    dates: [],
                    status: false,
                    datesEX: []
                };
                vm.AmendmentNotify[key] = {
                    name: gfs.firstName,
                    amendment: [],
                    amendmentDelete: [],
                    status: false
                };
                vm.completeTreatyNotify[key] = {
                    name: gfs.firstName,
                    treaty: [],
                    status: false
                };
                vm.assumingAlmostPastDue[key] = {
                    name: gfs.firstName,
                    dates: [],
                    status: false,
                    datesEX: []
                };
                vm.assumingAlmostExpired[key] = {
                    name: gfs.firstName,
                    dates: [],
                    status: false,
                    datesEX: []
                };
                vm.assumingExpired[key] = {
                    name: gfs.firstName,
                    dates: [],
                    status: false,
                    datesEX: []
                };
                vm.cedingAlmostPastDue[key] = {
                  name: gfs.firstName,
                  dates: [],
                  status: false,
                  datesEX: []
              };
              vm.cedingAlmostExpired[key] = {
                  name: gfs.firstName,
                  dates: [],
                  status: false,
                  datesEX: []
              };
              vm.cedingExpired[key] = {
                  name: gfs.firstName,
                  dates: [],
                  status: false,
                  datesEX: []
              };
                vm.deletedTransaction[key] = {
                    name: gfs.firstName,
                    transactions: [],
                    status: false
                };
            });

        });

          vm.countTreaty = 0;
          vm.countTransaction = 0;
          vm.countAmendments = 0;

          vm.countSettlementAlmostExpired = 0;
          vm.countSettlementAlmostPastDue = 0;
          vm.countSettlementExpired = 0;

          vm.countAssumingAlmostPastDue = 0;
          vm.countAssumingAlmostExpired = 0;
          vm.countAssumingExpired = 0;
          vm.countCedingAlmostPastDue = 0;
          vm.countCedingAlmostExpired = 0;
          vm.countCedingExpired = 0;

          vm.clearedReportingNotificationsCount = 0;

          vm.countReortingDue = 0;
          vm.countReportingPastGracePeriod = 0;
          vm.countSeriatim = 0;

          var promises = [];
          promises.push(TreatyTrackingCustomSvr.assumingAlmostPastDue.query(onSuccessAlmostPastDueAssuming).$promise);
          promises.push(TreatyTrackingCustomSvr.assumingAlmostExpired.query(onSuccessAlmostExpiredAssuming).$promise);
          promises.push(TreatyTrackingCustomSvr.assumingExpired.query(onSuccessExpiredAssuming).$promise);

          promises.push(TreatyTrackingCustomSvr.cedingAlmostPastDue.query(onSuccessAlmostPastDueCeding).$promise);
          promises.push(TreatyTrackingCustomSvr.cedingAlmostExpired.query(onSuccessAlmostExpiredCeding).$promise);
          promises.push(TreatyTrackingCustomSvr.cedingExpired.query(onSuccessExpiredCeding).$promise);

          $q.all(promises).then(function(){
              vm.settlementAlmostPastDue = vm.assumingAlmostPastDue;
              combineNotifications(vm.settlementAlmostPastDue, vm.cedingAlmostPastDue);
              vm.countSettlementAlmostPastDue = vm.countAssumingAlmostPastDue + vm.countCedingAlmostPastDue;

              vm.settlementAlmostExpired = vm.assumingAlmostExpired;
              combineNotifications(vm.settlementAlmostExpired, vm.cedingAlmostExpired);
              vm.countSettlementAlmostExpired = vm.countAssumingAlmostExpired + vm.countCedingAlmostExpired;

              vm.settlementExpired = vm.assumingExpired;
              combineNotifications(vm.settlementExpired, vm.cedingExpired);
              vm.countSettlementExpired = vm.countAssumingExpired + vm.countCedingExpired;

              function combineNotifications(main, arr){
                main.forEach(function(item){
                  var i = arr.reduce(function(prev, curr){
                      return (item.name === curr.name) ? curr.dates : prev;
                  }, null);

                  item.dates = item.dates.concat(i);
                });
              }
          });

          var dateFuture = dateAddDays(moment(), 7);

          TreatyTrackingCustomSvr.seritatimDueDate.get(onSuccessSeriatim);
          TreatyTrackingCustomSvr.seriatimDueDateExpired.query(onSuccessSeriatimExpired);
          TreatyTrackingCustomSvr.settlementCedingExpired.query(onSuccessSettlementCedingExpired);
          TreatyTrackingCustomSvr.settlementAssumingExpired.query(onSuccessSettlementAssumingExpired);

          TreatyTrackingCustomSvr.clearedReportingNotifications.query(onSuccessClearedReportingNotifications);

          TreatyTrackingCustomSvr.reportingDueDateExpired.query(onSuccessReportingDueDateExpired);
          TreatyTrackingCustomSvr.reportingDueExpired.query(onSuccessReportingDueDateExpiredCashDueNo);
          TreatyTrackingCustomSvr.reportingDueDatePastGracePeriod.query(onSuccessReportingDueDatePastGracePeriod);
          TreatyTrackingCustomSvr.expirationReportingDue.query({
            endYear: moment(dateFuture).get('year'),
            endMonth: moment(dateFuture).get('month') + 1,
            endDay: moment(dateFuture).get('date')
        }, onSuccessDateReportingDue, onError);

          TreatyTransactionSrv.deletedTransaction.query(onSuccessDeletedTransactions);


          TreatyTrackingCustomSvr.expirationAssuming.query({
              endYear: moment(dateFuture).get('year'),
              endMonth: moment(dateFuture).get('month') + 1,
              endDay: moment(dateFuture).get('date')
          }, onSuccessDate, onError);


          TreatyTrackingCustomSvr.expirationCeding.query({
              endYear: moment(dateFuture).get('year'),
              endMonth: moment(dateFuture).get('month') + 1,
              endDay: moment(dateFuture).get('date')
          }, onSuccessDateCeding, onError);

      };

      function accessAndTestFiltering(value) {
          return ((value.treaty != null && value.treaty.treatyName != null && !value.treaty.treatyName.includes('DO NOT USE')) &&
          (value.treaty.deal != null && value.treaty.deal.name != null && !value.treaty.deal.name.includes('DO NOT USE')) &&
          (value.treaty.treatyName.toLowerCase().includes("test") === false && value.treaty.deal.name.toLowerCase().includes("test") === false) &&
          (value.treaty.deal.gfsAssignee.firstName === vm.account.firstName || vm.authority === 'ROLE_APP_FINRE_OPS_ADMIN'));
      }

      function onSuccessDate(result) {
          angular.forEach(result, function (value, key) {
              if (accessAndTestFiltering(value)) {
                  if (value.cashDueFlag === 'Yes' && value.cashSentRecDate === null) {
                      angular.forEach(vm.assumingDates, function (date, key) {
                          if (date.name === value.treaty.deal.gfsAssignee.firstName) {
                              date.dates.push(value);
                              vm.countAsuming = vm.countAsuming + 1;
                          }
                      });
                  }
              }
          });
      }

      vm.getUserFullName = function(login){
        var user = vm.users.filter(function(i){ return i.login === login; });
        return user.firstName + " " + user.lastName;
      };

      function onSuccessDeletedTransactions(result) {
        $http({ method:'GET', url: '/api/jhiUser' })
        .then(function(result){
            vm.users = result.data;
        })
        .then(function(){
            angular.forEach(result, function (Transaction, key) {
                angular.forEach(vm.deletedTransaction, function (Notification, key) {
                    if (Transaction.treaty.deal.gfsAssignee.firstName === Notification.name && vm.authority === 'ROLE_APP_FINRE_OPS_ADMIN') {
                        var user = vm.users.find(function(i){ return i.login === Transaction.lastUser; });
                        Transaction.lastUserFullname = user ? user.firstName + " " + user.lastName : Transaction.lastUser;
                        Notification.transactions.push(Transaction);
                        vm.countTransaction = vm.countTransaction + 1;
                    }
                });
            });
        });
      }


      function onSuccessDateReportingDue(result) {
          angular.forEach(result, function (value, key) {
              if (accessAndTestFiltering(value)) {
                  if (value.reportingReceivedDate === null) {
                      angular.forEach(vm.expirationReportingDue, function (date, key) {
                          if (date.name === value.treaty.deal.gfsAssignee.firstName) {
                              date.datesEX.push(value);
                              vm.countReortingDue = vm.countReortingDue + 1;
                          }
                      });
                  }
              }
          });
      }

      function onSuccessDateCeding(result) {
          angular.forEach(result, function (value, key) {
              if (accessAndTestFiltering(value)) {
                  if (value.cashDueFlag === 'Yes' && value.cashSentRecDate === null) {
                      angular.forEach(vm.cedingDates, function (date, key) {
                          if (date.name === value.treaty.deal.gfsAssignee.firstName) {
                              date.dates.push(value);
                          }
                      });
                  }
              }
          });
      }


      function onSuccessAlmostPastDueAssuming(result) {
      angular.forEach(result, function (value, key) {
          if (accessAndTestFiltering(value)) {
          angular.forEach(vm.assumingAlmostPastDue, function (date, key) {
              if (date.name === value.treaty.deal.gfsAssignee.firstName) {
                value.cashDueType = "ASSUMING";
                date.dates.push(value);
                vm.countAssumingAlmostPastDue++;
              }
          });
          }
      });
      }

      function onSuccessAlmostExpiredAssuming(result) {
      angular.forEach(result, function (value, key) {
          if (accessAndTestFiltering(value)) {
          angular.forEach(vm.assumingAlmostExpired, function (date, key) {
              if (date.name === value.treaty.deal.gfsAssignee.firstName) {
                value.cashDueType = "ASSUMING";
                date.dates.push(value);
                vm.countAssumingAlmostExpired++;
              }
          });
          }
      });
      }

      function onSuccessExpiredAssuming(result) {
      angular.forEach(result, function (value, key) {
          if (accessAndTestFiltering(value)) {
          angular.forEach(vm.assumingExpired, function (date, key) {
              if (date.name === value.treaty.deal.gfsAssignee.firstName) {
                value.cashDueType = "ASSUMING";
                date.dates.push(value);
                vm.countAssumingExpired++;
              }
          });
          }
      });
      }

      function onSuccessAlmostPastDueCeding(result) {
        angular.forEach(result, function (value, key) {
            if (accessAndTestFiltering(value)) {
            angular.forEach(vm.cedingAlmostPastDue, function (date, key) {
                if (date.name === value.treaty.deal.gfsAssignee.firstName) {
                  value.cashDueType = "CEDING";
                  date.dates.push(value);
                  vm.countCedingAlmostPastDue++;
                }
            });
            }
        });
        }

        function onSuccessAlmostExpiredCeding(result) {
        angular.forEach(result, function (value, key) {
            if (accessAndTestFiltering(value)) {
            angular.forEach(vm.cedingAlmostExpired, function (date, key) {
                if (date.name === value.treaty.deal.gfsAssignee.firstName) {
                  value.cashDueType = "CEDING";
                  date.dates.push(value);
                  vm.countCedingAlmostExpired++;
                }
            });
            }
        });
        }

        function onSuccessExpiredCeding(result) {
        angular.forEach(result, function (value, key) {
            if (accessAndTestFiltering(value)) {
            angular.forEach(vm.cedingExpired, function (date, key) {
                if (date.name === value.treaty.deal.gfsAssignee.firstName) {
                  value.cashDueType = "CEDING";
                  date.dates.push(value);
                  vm.countCedingExpired++;
                }
            });
            }
        });
        }

      function onSuccessSeriatim(result) {
      angular.forEach(result, function (value, key) {
          if (accessAndTestFiltering(value)) {
          angular.forEach(vm.seriatimDates, function (date, key) {
              if (date.name === value.treaty.deal.gfsAssignee.firstName && value.seriatimReceivedDate == null) {
              date.dates.push(value);
              vm.countSeriatim = vm.countSeriatim + 1;
              }
          });
          }
      });
      }

      function onSuccessSeriatimExpired(result) {
      angular.forEach(result, function (value, key) {
          if (accessAndTestFiltering(value)) {
          angular.forEach(vm.seriatimDates, function (date, key) {
              if (date.name === value.treaty.deal.gfsAssignee.firstName &&
              value.seriatimReceivedDate == null) {
              date.datesEX.push(value);
              vm.countSeriatim = vm.countSeriatim + 1;
              }
          });
          }
      });
      }

      function onSuccessSettlementCedingExpired(result) {
      angular.forEach(result, function (value, key) {
          if (accessAndTestFiltering(value)) {
          if (value.cashDueFlag === 'Yes' && value.cashSentRecDate === null) {
              angular.forEach(vm.cedingDates, function (date, key) {
              if (date.name === value.treaty.deal.gfsAssignee.firstName) {
                  date.datesEX.push(value);
              }
              });
          }
          }
      });
      }

      function onSuccessSettlementAssumingExpired(result) {
      angular.forEach(result, function (value, key) {
          if (accessAndTestFiltering(value)) {
          if (value.cashDueFlag === 'Yes' && value.cashSentRecDate == null) {
              angular.forEach(vm.assumingDates, function (date, key) {
              if (date.name === value.treaty.deal.gfsAssignee.firstName) {
                  date.datesEX.push(value);
                  vm.countAsuming = vm.countAsuming + 1;
              }
              });
          }
          }
      });
      }

      function onSuccessReportingDueDateExpired(result) {
      angular.forEach(result, function (value, key) {
          if (accessAndTestFiltering(value)) {
          if (value.reportingReceivedDate === null) {
              angular.forEach(vm.expirationReportingDue, function (date, key) {
              if (date.name === value.treaty.deal.gfsAssignee.firstName) {
                  date.dates.push(value);
                  vm.countReortingDue = vm.countReortingDue + 1;
              }
              });
          }
          }
      });
      }

    function onSuccessReportingDueDateExpiredCashDueNo(result) {
      angular.forEach(result, function (value, key) {
        if (accessAndTestFiltering(value)) {
          if (value.reportingReceivedDate === null) {
            angular.forEach(vm.expirationReportingDue, function (date, key) {
              if ((date.name === value.treaty.deal.gfsAssignee.firstName) && value.cashDueFlag==='No') {
                date.datesExpCashDueNo.push(value);
                vm.countReortingDue = vm.countReortingDue + 1;
              }else {
                if ((date.name === value.treaty.deal.gfsAssignee.firstName)
                  && (( value.cashDueType=== 'Assuming' && value.calculatedAssumingGraceDate === null)||
                    ( value.cashDueType=== 'Ceding' && value.calculatedCedingGraceDate === null)
                  )
                ) {
                  date.datesExpCashDueYesNoGracePeriod.push(value);
                  vm.countReortingDue = vm.countReortingDue + 1;
                }
              }
            });
          }
        }
      });
    }



      function onSuccessReportingDueDatePastGracePeriod(result) {
          angular.forEach(result, function (value, key) {
              if (accessAndTestFiltering(value)) {
                  angular.forEach(vm.expirationReportingDue, function (date, key) {
                      if (date.name === value.treaty.deal.gfsAssignee.firstName) {
                          date.datesExpGracePeriod.push(value);
                          vm.countReortingDue++;
                      }
                  });
              }
          });
      }

      function onError() {
      console.log('error');
      }
      getAccount();
      TreatyAmendmentsNoPage.query().$promise.then(function (result) {

      angular.forEach(result, function (value) {
          if ((value.notificationStatus.id === 1 ||
            value.notificationStatus.id === 3) &&
            value.isActive === 1 && value.lastUser !== vm.account.login) {
          if (value.treaty !== null) {
              angular.forEach(vm.AmendmentNotify, function (date, key) {
              if (date.name === value.treaty.deal.gfsAssignee.firstName) {
                  date.amendment.push(value);
                  vm.countAmendments = vm.countAmendments + 1;
              }
              });
          }
          }

      });
      });

      TreatySrv.notificationInputStatus.query(function (result) {
          angular.forEach(result, function (value, key) {
          if (value.treaty.treatyName !== null &&
            !value.treaty.treatyName.includes('DO NOT USE') &&
            !value.treaty.deal.name.includes('DO NOT USE') &&
              (value.treaty.treatyName.toLowerCase().includes("test") === false && value.treaty.deal.name.toLowerCase().includes("test") === false) &&
             value.treaty.lastUser !== vm.account.login &&
            vm.authority === 'ROLE_APP_FINRE_OPS_ADMIN') {
              angular.forEach(vm.completeTreatyNotify, function (gfs, key) {
              if (gfs.name === value.treaty.deal.gfsAssignee.firstName) {
                  gfs.treaty.push(value.treaty);
                  vm.countTreaty = vm.countTreaty + 1;
              }
              });
          }
          });
      });

      function getAccount() {
      Principal.identity().then(function (account) {
          if (account !== null) {
          angular.forEach(account.authorities, function (authority) {
              if (authority === "ROLE_APP_FINRE_OPS_ADMIN") {
              vm.authority = authority;
              }
          });
          vm.account = account;
          vm.isAuthenticated = Principal.isAuthenticated;
          }
      });
      }

      vm.getTracking();
      vm.closeDeleteTransactionNotification = function (transaction, gfAssigne, $index) {
  if(vm.account.login===transaction.lastUser){
    var alert = InteractionsService.alert('You cannot dismiss your own Deleted Transaction notifications.', '#myModal');
    InteractionsService.showThenDialog(alert);
  }else{
      transaction.deleteNotification = 0;
      transaction.hide = true;
      vm.countTransaction = vm.countTransaction - 1;
      gfAssigne.transactions.splice($index, 1);
     TreatyTransaction.update(transaction);
  }
      };

      function onSuccessClearedReportingNotifications(result) {
          angular.forEach(result, function (value, key) {
              if (vm.authority === 'ROLE_APP_FINRE_OPS_ADMIN') {
                  angular.forEach(vm.clearedReportingNotifications, function (date, key) {
                      if (date.name === value.treaty.deal.gfsAssignee.firstName) {
                          date.dates.push(value);
                          vm.clearedReportingNotificationsCount++;
                      }
                  });
              }
          });
      }

      vm.closeReporting = function(notify){
          var prompt = InteractionsService.prompt("Why are you dismissing this notification?", '#myModal');
          InteractionsService.showThenDialog(prompt, function(result){
              if(result){
                  notify.reportNotifFlag = 1;
                  notify.reportNotifReason = result;
                  TreatyTracking.update(notify);
                  var obj = vm.expirationReportingDue.find(function(i){ return i.name === notify.treaty.deal.gfsAssignee.firstName; });
                  var index1 = obj.dates.map(function(i){ return i.id; }).indexOf(notify.id);
                  var index2 = obj.datesExpGracePeriod.map(function(i){ return i.id; }).indexOf(notify.id);

                  if(index1 === -1 && index2 === -1)
                      $log.error('There was a problem...The item isn\'t in the list');
                  else {
                      if(index1 !== -1)
                          obj.dates.splice(index1, 1);
                      if(index2 !== -1)
                          obj.datesExpGracePeriod.splice(index2, 1);
                      vm.countReortingDue--;

                      var clearedList = vm.clearedReportingNotifications.find(function(i){ return i.name === notify.treaty.deal.gfsAssignee.firstName; });
                      clearedList.dates.push(notify);
                      vm.clearedReportingNotificationsCount++;
                  }
              } else {
                  var alert = InteractionsService.alert('You must enter a reason. The notification was not cleared.', '#myModal');
                  InteractionsService.showThenDialog(alert);
              }
          });
      };

      vm.approveReportingNotificationClear = function(notify){
        var confirmAction = InteractionsService.confirm("Do you want to take action on this dismissal?", '#myModal', 'YES', 'NO');

        var confirm = InteractionsService.confirm("Do you want to ACCEPT or DECLINE this dismissal?", '#myModal', 'ACCEPT', 'DECLINE');
          var clearedList = vm.clearedReportingNotifications.find(function(i){ return i.name === notify.treaty.deal.gfsAssignee.firstName; });


        InteractionsService.showThenDialog(confirmAction,function(){

          InteractionsService.showThenDialog(confirm, function(){
              notify.reportNotifFlag = 2;
              TreatyTracking.update(notify);
              var index = clearedList.dates.map(function(i){ return i.id; }).indexOf(notify.id);
              if(index === -1)
                  $log.error("There was a problem...The item isn\'t in the list");
              else {
                  clearedList.dates.splice(index, 1);
                  vm.clearedReportingNotificationsCount--;
              }
          },
            function(){
              notify.reportNotifFlag = 0;
              TreatyTracking.update(notify);
              var obj = vm.expirationReportingDue.find(function(i){ return i.name === notify.treaty.deal.gfsAssignee.firstName; });

              var index = clearedList.dates.map(function(i){ return i.id; }).indexOf(notify.id);

              if(index === -1)
                  $log.error('There was a problem...The item isn\'t in the list');
              else {
                  clearedList.dates.splice(index, 1);
                  vm.clearedReportingNotificationsCount--;
                  vm.countReortingDue++;
                  if(moment().isBefore(notify.calculatedAssumingGraceDate))
                      obj.dates.push(notify);
                  else
                      obj.datesExpGracePeriod.push(notify);
              }
          });
        },function(){})
      };
  }
})();
