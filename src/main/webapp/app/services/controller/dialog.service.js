(function() {
  'use strict';
  angular
    .module('finreApp')
    .factory('DialogService', DialogService);

  DialogService.$inject = [];

  function DialogService () {
    function service($scope, $stateParams, $uibModalInstance, entity, Item, item, controllerName, datePickerItemsRun){
      var vm = this;
      vm.item = item;
      vm.load = function(id) {
        Item.get({id : id}, function(result) {
          vm.item = result;
        });
      };

      var onSaveSuccess = function (result) {
        $scope.$emit('finreApp:' + controllerName + 'Update', result);
        $uibModalInstance.close(result);
        vm.isSaving = false;
      };

      var onSaveError = function () {
        vm.isSaving = false;
      };

      vm.save = function () {
        vm.isSaving = true;
        if (vm.item.id !== null) {
          Item.update(vm.item, onSaveSuccess, onSaveError);
        } else {
          Item.save(vm.item, onSaveSuccess, onSaveError);
        }
      };

      vm.clear = function() {
        $uibModalInstance.dismiss('cancel');
      };

      datePickerItemsRun(vm);

      vm.openCalendar = function(date) {
        vm.datePickerOpenStatus[date] = true;
      };
    }

    return service;
  }
})();
