(function() {
  'use strict';
  angular
    .module('finreApp')
    .factory('DetailService', DetailService);

  DetailService.$inject = [];

  function DetailService () {
    function service($scope, $rootScope, $stateParams, Item, item, controllerName){
      var vm = this;
      vm.item = item;
      vm.load = function (id) {
        Item.get({id: id}, function (result) {
          vm.item = result;
        });
      };
      var unsubscribe = $rootScope.$on('finreApp:' + controllerName + 'Update', function (event, result) {
        vm.item = result;
      });
      $scope.$on('$destroy', unsubscribe);
    }
    return service;
  }
})();
