/**
 * Created by s0045003 on 6/29/2017.
 */
(function() {
  'use strict';
  angular
    .module('finreApp')
    .factory('InteractionsService', InteractionsService);

  InteractionsService.$inject = ['$mdDialog', '$mdTheming'];

  function InteractionsService ($mdDialog, $mdTheming) {
    
    

    return {
      confirmElaborateDialog: function (textContent, okMessage, cancelMessage, parentQuerySelector) {
        return $mdDialog.confirm()
          .textContent(textContent)
          .ok(okMessage)
          .cancel(cancelMessage)
          .theme('RGA')
          .parent(angular.element(document.querySelector(parentQuerySelector)));
      },
      confirmBasicDialog: function (textContent, parentQuerySelector) {
        return $mdDialog.confirm()
          .textContent(textContent)
          .ok("Yes")
          .cancel("No")
          .theme('RGA')
          .parent(angular.element(document.querySelector(parentQuerySelector)));
      },

      confirmDialogWithTitle: function (title, textContent, parentQuerySelector) {
        return $mdDialog.confirm()
          .title(title)
          .textContent(textContent)
          .ok("Yes")
          .cancel("No")
          .theme('RGA')
          .parent(angular.element(document.querySelector(parentQuerySelector)));
      },
      confirmBasicParentNoScrollDialog: function (textContent, parentQuerySelector) {
        return $mdDialog.confirm()
          .textContent(textContent)
          .disableParentScroll(false)
          .ok("Yes")
          .cancel("No")
          .theme('RGA')
          .parent(angular.element(document.querySelector(parentQuerySelector)));
      },
      confirm: function (textContent, parentQuerySelector, yes, no) {
        return $mdDialog.confirm()
          .textContent(textContent)
          .disableParentScroll(false)
          .ok(yes ? yes : "Yes")
          .cancel(no ? no : "No")
          .theme('RGA')
          .parent(angular.element(document.querySelector(parentQuerySelector)));
      },
      alertDialog: function(textContent, parentQuerySelector){
        return $mdDialog.alert()
          .textContent(textContent)
          .ok('OK')
          .theme('RGA')
          .parent(angular.element(document.querySelector(parentQuerySelector)));
      },
      alert: function(textContent, parentQuerySelector){
        return $mdDialog.alert()
          .textContent(textContent)
          .disableParentScroll(false)
          .ok('OK')
          .theme('RGA')
          .parent(angular.element(document.querySelector(parentQuerySelector)));
      },
      alertLarge: function(htmlFile, parentQuerySelector, locals){
        return {
          locals: locals,
          templateUrl: htmlFile,
          parent: angular.element(document.querySelector(parentQuerySelector)),
          clickOutsideToClose:true,
          fullscreen: false // Only for -xs, -sm breakpoints.
        };
      },
      alertParentScrollFalseDialog: function(textContent, parentQuerySelector){
        return $mdDialog.alert()
          .textContent(textContent)
          .disableParentScroll(false)
          .ok('OK')
          .theme('RGA')
          .parent(angular.element(document.querySelector(parentQuerySelector)));
      },
      alertDialogWithTitle: function(title, textContent, parentQuerySelector){
        return $mdDialog.alert()
          .title(title)
          .textContent(textContent)
          .disableParentScroll(false)
          .ok('OK')
          .theme('RGA')
          .parent(angular.element(document.querySelector(parentQuerySelector)));
      },
      prompt: function(textContent, parentQuerySelector){
        return $mdDialog.prompt()
        .textContent(textContent)
        .disableParentScroll(false)
        .theme('RGA')
        .ok('Submit')
        .cancel('Cancel')
        .parent(angular.element(document.querySelector(parentQuerySelector)));
      },
      showThenDialog: function (dialog){
        //$mdDialog.show(dialog).then(function1, function2, function3, function4);
        var args = Array.prototype.slice.call(arguments, 1);
        var showObj = $mdDialog.show(dialog);
        showObj.then.apply(showObj, args);
        return showObj;
      },
      showFinallyDialog: function (dialog){
        //$mdDialog.show(dialog).then(function1, function2, function3, function4);
        var args = Array.prototype.slice.call(arguments, 1);
        var showObj = $mdDialog.show(dialog);
        showObj.finally.apply(showObj, args);
        return showObj;
      }
    };
  }
})();

