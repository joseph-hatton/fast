/* globals $ */
(function() {
    'use strict';

    angular
        .module('finreApp')
        .directive('introButton', introButton);

    function introButton () {
        var directive = {
            restrict: 'E',
            template: '<button class="btn btn-default intro">' + 
            '<span class="glyphicon glyphicon-question-sign"></span>' + 
            '</button>',
            scope: {
                options: '='
            },
            link: start
        };

        function start(scope, elements){
            if(elements.length > 0)
                elements[0].onclick = function(){
                    var i = introJs();
                    i.setOptions(scope.options);        
                    i.start();
                };
        }

        return directive;
    }
})();
