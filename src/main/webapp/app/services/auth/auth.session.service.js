(function() {
    'use strict';

    angular
        .module('finreApp')
        .factory('AuthServerProvider', AuthServerProvider);

    AuthServerProvider.$inject = ['$http', '$localStorage', '$cookies', 'CurrentUser', '$window'];

    function AuthServerProvider ($http, $localStorage, $cookies, CurrentUser, $window) {        
        var service = {
            getToken: getToken,
            hasValidToken: hasValidToken,
            login: login,
            logout: function () {
                return $http({
                    method: 'DELETE',
                    url: '/__auth/current-user'
                })
                .then(function (res) {
                    console.log('res', {res: res});
                    return res.data.logoutUrl;
                });
            }
        };

        return service;

        function getToken () {
            var token = $localStorage.authenticationToken;
            return token;
        }

        function hasValidToken () {
            var token = this.getToken();
            return !!token;
        }

        function login (credentials) {
            var data = 'j_username=' + encodeURIComponent(credentials.username) +
                '&j_password=' + encodeURIComponent(credentials.password) +
                '&remember-me=' + credentials.rememberMe + '&submit=Login';

            return $http.post('api/authentication', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                return response;
            }).catch(function() {
                return null;
            });
        }

        // function logout () {
        //     $http.post('api/logout')
        //     .then(function (res) {
        //         return CurrentUser.del().$promise;
        //     })
        //     .then(function (res) {
        //         delete $localStorage.authenticationToken;

        //         var cookies = $cookies.getAll();

        //         for (var prop in cookies) {
        //         // if (cookies.hasOwnProperty(prop)) {
        //             $cookies.remove(prop);
        //         // }
        //         }
        //         window.location = "/";
        //         return res;
        //     })
        //     .catch(function (err) {
        //         console.error('logout error:', err);
        //         window.location = "/";
        //         return 'log out anyway';
        //     });

        // }
    }
})();
