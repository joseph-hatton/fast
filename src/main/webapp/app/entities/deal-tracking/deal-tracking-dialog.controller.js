(function() {
  'use strict';

  angular
  .module('finreApp')
  .controller('DealTrackingDialogController', DealTrackingDialogController);

  DealTrackingDialogController.$inject = ['$scope','$mdDialog', '$stateParams', '$uibModalInstance', 'entity', 'Principal','DealTracking','Deal'];

  function DealTrackingDialogController ($scope,$mdDialog, $stateParams, $uibModalInstance, entity, Principal, DealTracking,Deal) {
    var vm = this;
    vm.dealTracking = entity;
    vm.dealTracking.financeDateFlag=1;
    vm.dealTracking.autoGenerateFlag=1;
    vm.dealTracking.reportingArmFlag=1;
    vm.dealTracking.reportingRevisedFlag='No';
    vm.dealId=$stateParams.dealId;
    vm.dealTracking.dealTrackingStatus='Open';
    vm.datePickerOpenStatus = {};
    vm.datePickerOpenStatus.financeDate= false;
    vm.datePickerOpenStatus.reportingArmDate= false;
    vm.datePickerOpenStatus.reportingValidationDate= false;
    vm.datePickerOpenStatus.reportingPeriodEndDate= false;
    vm.dealTracking.reportingFlag='No';
    vm.dealTracking.lastUpdate= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
    vm.nextTrackingGeneration={};
    vm.openCalendar = function (date) {
      vm.datePickerOpenStatus[date] = true;
    };
    vm.format = "yyyy-MM";

    vm.dateOptions = {
      datepickerMode: 'month',
      formatMonth: 'MMMM',
      formatYear: 'yyyy',
      formatMonthTitle: 'yyyy',
      minMode: 'month',
      showMonths: true,
      startingDay: 0,
      yearRange: 20,
      minDate: null,
      maxDate: null
    };

    Deal.get({id: $stateParams.dealId}, function (result) {
      vm.deal = result;
      vm.dealTracking.deal=result;
    });

    var onSaveSuccessNextTracking =function(){
      $uibModalInstance.close();
      $scope.$emit('finreApp:treatyTrackingUpdate');
      vm.isSaving = false;

    };
    var onUpdateTrackingAutoGenerate =function(){

    };
    var onSaveSuccess = function (result) {
      if(result.dealTrackingStatus==='Closed' && result.autoGenerateFlag===1){
        result.autoGenerateFlag=2;
        result.lastUpdate= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        DealTracking.update(result, onUpdateTrackingAutoGenerate, onSaveError);
        DealTracking.save(vm.nextTrackingRecord(result), onSaveSuccessNextTracking, onSaveError);
      }else{
        $scope.$emit('finreApp:dealTrackingUpdate', result);
        $uibModalInstance.close(result);
        vm.isSaving = false;
      }
    };

    var onSaveError = function () {
      vm.isSaving = false;
    };

    Principal.identity().then(function (account) {
      vm.lastUser=account.login;
      vm.dealTracking.lastUpdate= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
    });

    vm.dealTracking.lastUser=vm.lastUser;

    vm.save = function (param) {
      vm.isSaving = true;
      if(param===1){
        vm.dealTracking.dealTrackingStatus='Open';
      }
      vm.dealTracking.lastUser=vm.lastUser;
      if (vm.dealTracking.id !== null) {
        vm.dealTracking.lastUpdate= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        DealTracking.update(vm.dealTracking, onSaveSuccess, onSaveError);
      } else {
        vm.dealTracking.isActive=1;
        vm.dealTracking.creationUser=vm.lastUser;
        vm.dealTracking.creationTs= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        DealTracking.save(vm.dealTracking, onSaveSuccess, onSaveError);
      }
    };

    vm.clear = function() {
      $uibModalInstance.dismiss('cancel');
    };
    vm.clearField=function(){
      if(vm.dealTracking.financeDateFlag=== 1){
        vm.dealTracking.financeDate=null;
      }
      if(vm.dealTracking.reportingArmFlag=== 1){
        vm.dealTracking.reportingArmDate=null;
      }
    };

    vm.saveMessage=function(){
      var confirm = $mdDialog.confirm()
      .textContent('Do you want to change this Tracking Status to Closed?')
      .ok('Yes')
      .cancel('No')
      .disableParentScroll(false)
      .parent(angular.element(document.querySelector('#myModal')));

      var confirmChange = $mdDialog.confirm()
      .textContent('Tracking Status has been updated from Open to Closed. Do you want to create the next tracking record?')
      .ok('Generate')
      .cancel('No')
      .disableParentScroll(false)
      .parent(angular.element(document.querySelector('#myModal')));

      if(vm.dealTracking.dealTrackingStatus==='Closed'){
        vm.save(0);
      } else {
        $mdDialog.show(confirm).then(function() {
          vm.dealTracking.dealTrackingStatus='Closed';
          $mdDialog.show(confirmChange).then(function() {
            vm.dealTracking.dealTrackingStatus='Closed';
            vm.dealTracking.autoGenerateFlag=1;
            vm.save(0);
          }, function() {
            vm.dealTracking.trackingStatus='Closed';
            vm.dealTracking.autoGenerateFlag=2;
            vm.save(0);
          });
        }, function() {
          vm.remainInProcess();
        });
      }
    };
    vm.remainInProcess=function() {
      $mdDialog.show($mdDialog.alert()
      .textContent('Tracking Status remains Open')
      .disableParentScroll(false)
      .parent(angular.element(document.querySelector('#myModal')))
      .ok('OK'))
      .finally(function() {
        vm.save(1);
      });
    };


    vm.getNextReportingPeriodDate= function(date){
      date= moment(date).add(3, 'M');
      return date;
    };

    vm.nextTrackingRecord= function (oldTracking){
      vm.nextTrackingGeneration.id=null;
      vm.nextTrackingGeneration.lastUpdate= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
      vm.nextTrackingGeneration.isActive=1;
      vm.nextTrackingGeneration.reportingFlag='No';
      vm.nextTrackingGeneration.deal=oldTracking.deal;
      vm.nextTrackingGeneration.financeDateFlag=oldTracking.financeDateFlag;
      vm.nextTrackingGeneration.reportingArmFlag=oldTracking.reportingArmFlag;
      vm.nextTrackingGeneration.reportingRevisedFlag='No';
      vm.nextTrackingGeneration.autoGenerateFlag=1;
      vm.nextTrackingGeneration.dealTrackingStatus='Open';
      vm.nextTrackingGeneration.reportingPeriodEndDate= vm.getNextReportingPeriodDate(oldTracking.reportingPeriodEndDate);
      return vm.nextTrackingGeneration;
    };

    vm.verifySave = function (editForm) {
      var reportingValid = !editForm.reportingPeriodEndDate.$invalid;
      if(reportingValid){
        var complete = true;

        if((!vm.dealTracking.reportingArmFlag && editForm.reportArmDate.$invalid)
        || (!vm.dealTracking.financeDateFlag && editForm.financeDate.$invalid))
        complete = false;

        if (complete)
          vm.saveMessage();
        else
          vm.save(1);
      }
    }
  }
})();
