(function () {
    'use strict';

    angular
        .module('finreApp')
        .controller('DealTrackingDetailController', DealTrackingDetailController);

    DealTrackingDetailController.$inject = ['$scope', '$filter', '$rootScope', '$stateParams', 'entity', 'DealTracking'];

    function DealTrackingDetailController($scope, $filter, $rootScope, $stateParams, entity, DealTracking) {
        var vm = this;
        vm.dealTracking = entity;
        vm.dealId = $stateParams.dealId;

        vm.deal = vm.dealTracking.deal;
        vm.dealTracking.reportingPeriodEndDate = $filter('date')(vm.dealTracking.reportingPeriodEndDate, 'yyyy-MM');
        vm.dealTracking.reportingArmDate = $filter('date')(vm.dealTracking.reportingArmDate, 'yyyy-MM-dd');
        vm.dealTracking.reportingValidationDate = $filter('date')(vm.dealTracking.reportingValidationDate, 'yyyy-MM-dd');
        vm.dealTracking.financeDate = $filter('date')(vm.dealTracking.financeDate, 'yyyy-MM-dd');

        var unsubscribe = $rootScope.$on('finreApp:dealTrackingUpdate', function (event, result) {
            vm.dealTracking = result;

        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
