(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('DealTrackingController', DealTrackingController);

    DealTrackingController.$inject = ['$scope','$mdDialog', '$stateParams','$state', 'DealTrackingSrv', 'DealTrackingFilterSrv', 'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants','Deal','DealTracking', 'Intro','Principal'];

    function DealTrackingController ($scope,$mdDialog,$stateParams, $state, DealTrackingSrv, DealTrackingFilterSrv, ParseLinks, AlertService, pagingParams, paginationConstants,Deal,DealTracking, Intro,Principal) {
        var vm = this;
        vm.loadAll = loadAll;
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
      // vm.predicate = 'reportingPeriodEndDate';
     //   vm.reverse = pagingParams.descending;
        vm.transition = transition;
        vm.clear = clear;
        vm.search = search;
        vm.searchQuery = pagingParams.search;
        vm.currentSearch = pagingParams.search;
      vm.deleteMultipleTracking=[];
       vm.dealId=$stateParams.dealId;
      vm.searchStartDate = null;
      vm.searchEndDate = null;
      vm.datePickerOpenStatus = {};
      vm.datePickerOpenStatus.searchStartDate = false;
      vm.datePickerOpenStatus.searchEndDate = false;
      vm.format = "yyyy-MM";
      vm.itemsPerPage = paginationConstants.itemsPerPage;
      vm.dateOptions = {
        datepickerMode: 'month',
        formatMonth: 'MMMM',
        formatYear: 'yyyy',
        formatMonthTitle: 'yyyy',
        minMode: 'month',
        showMonths: true,
        startingDay: 0,
        yearRange: 20,
        minDate: null,
        maxDate: null
      };

      Principal.identity().then(function (account) {
        angular.forEach(account.authorities, function (authoritie) {
          if (authoritie === "ROLE_APP_FINRE_OPS_ADMIN") {
            vm.authority = 'Admin';
          } else {
            if (authoritie ==='ROLE_APP_FINRE_VIEW_ONLY' || authoritie ==='ROLE_APP_STRUCTURED_FINANCE_VIEW_ONLY'){
              vm.authority = 'ViewOnly';

            }else{
              if (vm.authority !== 'Admin') {
                vm.authority = 'User';
              }
            }}
        });

      });

      Intro.setupIntroSteps({id:166})
      .then(function(result){
          vm.introOptions = {
              steps: result
          };
      });

      vm.openCalendar = function (date) {
        vm.datePickerOpenStatus[date] = true;
      };

      Deal.get({id: $stateParams.dealId}, function (result) {
        vm.deal = result;
      });

      vm.loadAll();

        function loadAll () {
            if (pagingParams.search) {
              DealTrackingSrv.base.query({
                dealId: $stateParams.dealId,
                    query: pagingParams.search,
                    page: pagingParams.page - 1,
                    size: paginationConstants.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);
            } else {
              if(pagingParams.sort == 'id,asc'){
                vm.predicate = "reportingPeriodEndDate";
                vm.reverse = false;
              }
              DealTrackingSrv.base.query({
                dealId: $stateParams.dealId,
                   page: pagingParams.page - 1,
                   size: paginationConstants.itemsPerPage,
                   sort: sort()
                }, onSuccess, onError);
            }
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
              angular.forEach(data, function (track,key) {
                track.month = calculateMonth(track.reportingPeriodEndDate);
                track.reportingPeriodEndDate = calculateQuarters(track.reportingPeriodEndDate);

              });
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.dealTrackings = data;
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }


        function loadPage (page) {
            vm.page = page;
            vm.transition();
        }

        function transition () {
            $state.transitionTo($state.$current, {
              dealId: $stateParams.dealId,
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }

        function search (searchQuery) {
            if (!searchQuery){
                return vm.clear();
            }
            vm.links = null;
            vm.page = 1;
            vm.predicate = '_score';
            vm.reverse = false;
            vm.currentSearch = searchQuery;
            vm.transition();
        }

        function clear () {
            vm.links = null;
            vm.page = 1;
            vm.predicate = 'id';
            vm.reverse = true;
            vm.currentSearch = null;
            vm.transition();
        }

      vm.filterByReportingPeriod=function() {
        var startDate=vm.searchStartDate;
        var endDate=vm.searchEndDate;

        DealTrackingFilterSrv.reportingReport.get({
          dealId:vm.dealId,
          startYear: moment(startDate).get('year'),
          startMonth: moment(startDate).get('month') + 1,
          startDay: moment(startDate).get('date'),
          endYear: moment(endDate).get('year'),
          endMonth: moment(endDate).get('month') + 1,
          endDay: moment(endDate).get('date')
        }, onSuccessDateFilter, onErrorDateFilter);
      };

      vm.reset = function () {
        vm.searchStartDate = null;
        vm.searchEndDate = null;
        loadAll();
      };
      function onSuccessDateFilter(data,headers){

        angular.forEach(data, function (track) {
          track.month = calculateMonth(track.reportingPeriodEndDate);
          track.reportingPeriodEndDate = calculateQuarters(track.reportingPeriodEndDate);

        });
        vm.links = ParseLinks.parse(headers('link'));
        vm.totalItems = headers('X-Total-Count');
        vm.queryCount = vm.totalItems;
        vm.dealTrackings = data;
        vm.page = pagingParams.page;
      }

      function onErrorDateFilter(){
        console.log('ERROR');
      }

      vm.forDelete=function(tracking){
        if(tracking.checked){
          vm.deleteMultipleTracking.push(tracking);
        }else{
          angular.forEach(vm.deleteMultipleTracking, function (track,index) {
            if(track.id===tracking.id){
              vm.deleteMultipleTracking.splice(index,1);
            }
          });
        }
      };
      vm.deleteMultiple=function(){
        var confirm = $mdDialog.confirm()
          .textContent('You are attempting to delete ' + vm.deleteMultipleTracking.length  + ' tracking records. Click OK to proceed, or CANCEL to abort.')
          .ok('OK')
          .cancel('Cancel')
          .parent(angular.element(document.querySelector('#myModal')));

        $mdDialog.show(confirm).then(function() {
          angular.forEach(vm.deleteMultipleTracking, function (tracking,index) {
            angular.forEach(vm.dealTrackings,function(track,index){
              if(track.id===tracking.id) {
                vm.dealTrackings.splice(index,1);
              }
            });
            DealTracking.delete({id: tracking.id});
          });
        });
      };
      vm.deleteFlagChange= function(){
        if(!vm.deleteFlag){
          vm.deleteMultipleTracking=[];
          angular.forEach(vm.dealTrackings,function(track){
            track.checked=false;
          });
        }

      };


    }
})();
