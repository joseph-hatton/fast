(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('DealTrackingDeleteController',DealTrackingDeleteController);

    DealTrackingDeleteController.$inject = ['$uibModalInstance', 'entity', 'DealTracking'];

    function DealTrackingDeleteController($uibModalInstance, entity, DealTracking) {
        var vm = this;
        vm.dealTracking = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
      var onSaveError= function(){
      };
        vm.confirmDelete = function (id) {
          vm.dealTracking.isActive=0;
          DealTracking.update(vm.dealTracking,   function () {
            $uibModalInstance.close(true);
          }, onSaveError);

        };
    }
})();
