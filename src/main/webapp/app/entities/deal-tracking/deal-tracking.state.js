(function() {
    'use strict';

    angular
        .module('finreApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('deal-tracking', {
            parent: 'entity',
            url: '/deal-tracking/{dealId}?page&sort&search',
            data: {
              authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
                pageTitle: 'finreApp.dealTracking.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/deal-tracking/deal-trackings.html',
                    controller: 'DealTrackingController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dealTracking');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('deal-tracking-detail', {
            parent: 'entity',
            url: '/deal-tracking/{dealId}/detail/{id}',
            data: {
              authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
                pageTitle: 'finreApp.dealTracking.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/deal-tracking/deal-tracking-detail.html',
                    controller: 'DealTrackingDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dealTracking');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'DealTracking', function($stateParams, DealTracking) {
                    return DealTracking.get({id : $stateParams.id}).$promise;
                }]
            }
        })
        .state('deal-tracking.new', {
            parent: 'deal-tracking',
            url: '/new',
            data: {
              authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/deal-tracking/deal-tracking-dialog.html',
                    controller: 'DealTrackingDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('deal-tracking', null, { reload: true });
                }, function() {
                    $state.go('deal-tracking');
                });
            }]
        })
        .state('deal-tracking.edit', {
            parent: 'deal-tracking',
            url: '/edit/{id}',
            data: {
              authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/deal-tracking/deal-tracking-dialog.html',
                    controller: 'DealTrackingDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DealTracking', function(DealTracking) {
                            return DealTracking.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('deal-tracking', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
          .state('deal-tracking-detail.edit', {
            parent: 'deal-tracking-detail',
            url: '/editDetail/{id}',
            data: {
              authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
              $uibModal.open({
                templateUrl: 'app/entities/deal-tracking/deal-tracking-dialog.html',
                controller: 'DealTrackingDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: ['DealTracking', function(DealTracking) {
                    return DealTracking.get({id : $stateParams.id});
                  }]
                }
              }).result.then(function() {
              $state.go('deal-tracking-detail', null, { reload: true });
              }, function() {
             $state.go('deal-tracking-detail', null, { reload: true });
              });
            }]
          })
        .state('deal-tracking.delete', {
            parent: 'deal-tracking',
            url: '/{id}/delete',
            data: {
              authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/deal-tracking/deal-tracking-delete-dialog.html',
                    controller: 'DealTrackingDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DealTracking', function(DealTracking) {
                            return DealTracking.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('deal-tracking', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
