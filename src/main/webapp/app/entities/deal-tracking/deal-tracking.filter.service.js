(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('DealTrackingFilterSrv', DealTrackingFilterSrv);

  DealTrackingFilterSrv.$inject = ['$resource', 'DateUtils'];

  function DealTrackingFilterSrv($resource, DateUtils) {

    var sortReporting='api/deal-trackings/date-range/:dealId/:startYear/:startMonth/:startDay/:endYear/:endMonth/:endDay';
    var sortReportingTracking='api/deal-trackings/tracking-treaty/:dealId/:startYear/:startMonth/:startDay';
    return{
      reportingReport:$resource(sortReporting, {}, {
        'query': { method: 'GET', params: {}, isArray: true },
        'get': {
          method: 'GET',
          transformResponse: function (data) {
            if(data === "" || data === null || data === undefined) return null;
            data = angular.fromJson(data);
            return data;
          },isArray: true
        }
      }),
      reportingReportTracking:$resource(sortReportingTracking, {}, {

        'query': { method: 'GET', params: {}, isArray: true },
        'get': {

          method: 'GET',
          transformResponse: function (data) {
            if(data === "" || data === null || data === undefined) return null;
            data = angular.fromJson(data);
            return data;
          },isArray: true
        }
      })
    }}
})();
