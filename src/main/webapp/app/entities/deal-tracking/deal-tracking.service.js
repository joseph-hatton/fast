(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('DealTracking', DealTracking);

    DealTracking.$inject = ['$resource', 'DateUtils'];

    function DealTracking ($resource, DateUtils) {
        var resourceUrl =  'api/deal-trackings/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                  if (data === '') return null;
                  data = angular.fromJson(data);
                  data.reportingPeriodEndDate = DateUtils.convertLocalDateFromServer(data.reportingPeriodEndDate);
                  data.reportingValidationDate = DateUtils.convertLocalDateFromServer(data.reportingValidationDate);
                  data.reportingArmDate = DateUtils.convertLocalDateFromServer(data.reportingArmDate);
                  data.financeDate = DateUtils.convertLocalDateFromServer(data.financeDate);
                    return data;
                }
            },
            'update': { method:'PUT' },
          transformRequest: function (data) {
            if (data === "" || data === null || data === undefined) return null;
            data.reportingPeriodEndDate = DateUtils.convertLocalDateToServer(data.reportingPeriodEndDate);
            data.reportingValidationDate = DateUtils.convertLocalDateToServer(data.reportingValidationDate);
            data.reportingArmDate = DateUtils.convertLocalDateToServer(data.reportingArmDate);
            data.financeDate = DateUtils.convertLocalDateToServer(data.financeDate);
            return data;
          }
        });
    }
})();
