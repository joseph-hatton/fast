(function() {
    'use strict';

    angular
        .module('finreApp')
        .factory('DealTrackingSearch', DealTrackingSearch);

    DealTrackingSearch.$inject = ['$resource'];

    function DealTrackingSearch($resource) {
        var resourceUrl =  'api/_search/deal-trackings/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
