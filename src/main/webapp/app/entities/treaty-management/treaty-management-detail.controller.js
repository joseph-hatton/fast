(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('TreatyManagementDetailController', TreatyManagementDetailController);

  TreatyManagementDetailController.$inject = ['$scope','$state' ,'$rootScope', '$stateParams', 'entity',
    'Treaty','TreatyManagementCustomSrv'];

  function TreatyManagementDetailController($scope, $state,$rootScope, $stateParams, entity,
                                            Treaty,TreatyManagementCustomSrv) {
    var vm = this;
    vm.treatyManagement = entity;
    vm.treaty=entity;
    vm.treatyId=$stateParams.treatyId;
    vm.dealId = $stateParams.dealId;


    vm.load = function (TreatyManagementCustomSrv, $stateParams) {
      TreatyManagementCustomSrv.get({id: $stateParams.treatyId}, function (result) {
        if (result.id == undefined) {
          $state.go('treaty-management.newMngt', {id: $stateParams.treatyId});
        } else {
          vm.treatyManagement = result;
          vm.treaty = result.treaty;
          vm.riskfeeArray = vm.treatyManagement.riskFees;
          vm.band = vm.riskfeeArray.length;
          for (var i = 0; i < result.entityComments.length; i++) {
            switch (result.entityComments[i].entityType) {
              case 'QuotaShareCommentsMgnt':
                vm.QuotaShareCommentsMgnt = result.entityComments[i];
                vm.quotasharecomment = vm.QuotaShareCommentsMgnt.text;
                break;
              case 'TreatyReferenceSeriatimMgnt':
                vm.TreatyReferenceSeriatimMgnt = result.entityComments[i];
                vm.treatyReferenceSeriatim = vm.TreatyReferenceSeriatimMgnt.text;
                break;
              case 'CommentsSeriatimMgnt':
                vm.CommentsSeriatimMgnt = result.entityComments[i];
                vm.seriatimReportingComment = vm.CommentsSeriatimMgnt.text;
                break;
              case 'TreatyReferenceIntPeMgnt':
                vm.TreatyReferenceIntPeMgnt = result.entityComments[i];
                vm.latePaymentTreatyReference = vm.TreatyReferenceIntPeMgnt.text;
                break;
              case 'CommentsIntPeMgnt':
                vm.CommentsIntPeMgnt = result.entityComments[i];
                vm.commentLatePayment = vm.CommentsIntPeMgnt.text;
                break;
              case 'TreatyReferenceRecapMgnt':
                vm.TreatyReferenceRecapMgnt = result.entityComments[i];
                vm.treatyReferenceRecapture = vm.TreatyReferenceRecapMgnt.text;
                break;
              case 'RecaptureCommentsRecapMgnt':
                vm.RecaptureCommentsRecapMgnt = result.entityComments[i];
                vm.recapturetermComment = vm.RecaptureCommentsRecapMgnt.text;
                break;
              default:
            }
          }
        }
      });
    };

    vm.load(TreatyManagementCustomSrv, $stateParams);

    Treaty.get({id: $stateParams.treatyId}, function (result) {
      vm.treatyManagement.treaty = result;
    });
    var unsubscribe = $rootScope.$on('finreApp:treatyManagementUpdate', function (event, result) {
      vm.treatyManagement = result;
    });
    $scope.$on('$destroy', unsubscribe);

    vm.riskfeeArray=[ ];
    vm.band=0;
  }
})();
