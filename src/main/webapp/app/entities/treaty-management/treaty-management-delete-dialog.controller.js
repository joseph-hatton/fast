(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .controller('TreatyManagementDeleteController', TreatyManagementDeleteController);
  
  TreatyManagementDeleteController.$inject = ['$uibModalInstance', 'entity', 'TreatyManagement'];
  
  function TreatyManagementDeleteController($uibModalInstance, entity, TreatyManagement) {
    var vm = this;
    vm.treatyManagement = entity;
    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };
    vm.confirmDelete = function (id) {
      TreatyManagement.delete({id: id},
        function () {
          $uibModalInstance.close(true);
        });
    };
  }
})();
