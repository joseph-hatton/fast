(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('TreatyManagementDialogController', TreatyManagementDialogController);

  TreatyManagementDialogController.$inject = ['$scope', '$animate', '$timeout', '$mdDialog', '$location', '$stateParams', '$uibModalInstance', 'entity', 'Treaty',
    'TreatyManagementCustomSrv', 'TreatyManagement', 'Frequency', 'Principal', 'InteractionsService'];
  function TreatyManagementDialogController($scope, $animate, $timeout, $mdDialog, $location, $stateParams, $uibModalInstance, entity, Treaty,
                                            TreatyManagementCustomSrv, TreatyManagement, Frequency, Principal, InteractionsService) {
    var vm = this;
    vm.treatyManagement = entity;
    vm.treatyId = $stateParams.treatyId;
    vm.dealId = $stateParams.dealId;
    vm.treatyManagement.riskFees = [];
    vm.band = 0;
    vm.treatyManagement.brokerPeriod = "Risk Fee Rec'd/Settlement Date";
    vm.DayTypeOptions = ['Calendar Days After', 'Business Days After'];
    vm.currencyOptions = [];
    vm.treatyManagement.confirmationRequested = 'No';
    vm.treatyManagement.inputStatus = 'In Process';
    vm.periodOptions = [];
    var msm = 'Input Status on Treaty Management has been updated from In Process to Complete.';
    vm.loadTreatyInfo = function (result) {
      if(result.inputStatus==='Complete'){
        msm = ' Input Status on both Treaty Basic Info and Management Info are now Complete. Admin has been notified.';
      }
      vm.treaty = result;
      angular.forEach(vm.treaty.treatyCurrencies, function (currency) {
        vm.currencyOptions.push(currency.currency);
      });
      if (vm.treatyManagement.refundEliminationFlag == null)
        vm.treatyManagement.refundEliminationFlag = 'No';
      if (vm.treatyManagement.calendarization ==null)
        vm.treatyManagement.calendarization = 'No';
      if(vm.treaty.reason === '' || vm.treaty.reason == null)
        vm.treaty.reason = null;
    };

    //this timeout prevent for no loading the treaty info for the headers
    $timeout(function () { Treaty.get({id: $stateParams.treatyId}, vm.loadTreatyInfo); }, 0);

    $scope.$watch('vm.treaty', function (newVal, oldVal) {
      vm.treaty = newVal;
      if (newVal !== undefined)
        vm.getAllEntityTypes(newVal);
    });


    vm.getAllEntityTypes = function (newVal) {
      vm.treatyManagement.treaty = newVal;
      if (vm.treatyManagement.inputStatus !== 'Complete')
        vm.treatyManagement.inputStatus = 'In Process';
      if (vm.treatyManagement.confirmationRequested !== 'Yes')
        vm.treatyManagement.confirmationRequested = 'No';
      if (vm.treatyManagement.dayTypePosition === undefined)
        vm.treatyManagement.dayTypePosition = '';
      if (vm.treatyManagement.brokerPeriod === undefined)
        vm.treatyManagement.brokerPeriod = '';
    };

    vm.loadAccordionSelected = function($stateParams){
      if ($stateParams.hash != null) {
        $location.hash($stateParams.hash);
        switch ($stateParams.hash) {
          case "generalInfo":
            vm.general = true;
            break;
          case "recaptureInfo":
            vm.recapture = true;
            break;
          case "riskFeeInfo":
            vm.riskFee = true;
            break;
          case "brokerInfo":
            vm.brokerInfo = true;
            break;
          case "refund":
            vm.refund = true;
            break;
        }
      }
    };

    vm.loadAccordionSelected($stateParams);

    if (vm.treatyManagement.id == null) {
      vm.treatyManagement.confirmationRequested = 'No';
      vm.treatyManagement.inputStatus = 'In Process';
    }

    Principal.identity().then(function (account) {
      vm.lastUser = account.login;
      vm.treatyManagement.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
    });

    Frequency.query(function (result) {
      angular.forEach(result, function (Frequency) {
        if (Frequency.name !== 'Occurrence')
          vm.periodOptions.push(Frequency);
      });
    });


    vm.treatyManCustomCallback = function (result) {
      if (result.length === undefined)
        vm.treatyManagement.confirmationRequested = 'No';
      vm.treatyManagement = result;
      vm.treaty = result.treaty;

      if (vm.treatyManagement.riskFees !== undefined)
        vm.band = vm.treatyManagement.riskFees.length;

      if (vm.treatyManagement.calendarization === 'No') {
        vm.treatyManagement.calendarizationTreatyRefer = '';
      }

      if (vm.treatyManagement.confirmationRequested === 'No') {
        vm.treatyManagement.confirmationRequestedComment = '';
      }
    };

    TreatyManagementCustomSrv.get({id: $stateParams.treatyId}, vm.treatyManCustomCallback);

    var onSaveSuccess = function (result) {
      if (vm.treaty.reason != null) {
        vm.treaty.reason = null;
        Treaty.update(vm.treaty, function(){}, onSaveError);
      }
      $scope.$emit('finreApp:treatyManagementUpdate', result);
      $uibModalInstance.close(result);
      vm.isSaving = false;
    };

    var onSaveError = function (error) {
      console.log(error);
      vm.isSaving = false;
    };

    vm.save = function () {
      vm.treatyManagement.lastUser=vm.lastUser;
      vm.isSaving = true;
      vm.treatyManagement.lastUpdate= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';

      if (vm.treatyManagement.id != null) {
        TreatyManagement.update(vm.treatyManagement, onSaveSuccess, onSaveError);
      } else {
        vm.treatyManagement.lastUpdate= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        vm.treatyManagement.creationUser=vm.lastUser;
        vm.treatyManagement.creationTs= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        TreatyManagement.save(vm.treatyManagement, onSaveSuccess, onSaveError);
      }
    };

    vm.clear = function () {
      var confirm = InteractionsService.confirmBasicParentNoScrollDialog('Are you sure you want to exit without saving?', '#myModal');
      InteractionsService.showThenDialog(confirm, function () {
          $uibModalInstance.dismiss('cancel');
        },
        function () { }
      );
    };

    vm.datePickerOpenStatus = {};
    vm.datePickerOpenStatus.summaryDate = false;

    vm.openCalendar = function (date) {
      vm.datePickerOpenStatus[date] = true;
    };

    vm.periodOptionsRiskFee = ['Monthly', 'Quarterly', 'Semi-Annual', 'Annual', 'Initial'];

    vm.addRiskFee = function () {
      var riskfee = {
        minimumRiskFee: vm.minimumRiskFee,
        minimumRiskFeeAmount: vm.minimumRiskFeeAmount,
        rate: vm.riskFeeRate,
        treatyReference: vm.treatyReference,
        period: vm.riskFeeRatePeriod,
        comments: vm.riskfeeComments,
        treatyId: vm.treaty.id,
        riskFeeCurrency: vm.riskFeeCurrency,
        id: null
      };

      if (!riskfee.riskFeeCurrency)
        riskfee.riskFeeCurrency = null;
if(vm.treatyManagement.riskFees == null ) {
  vm.treatyManagement.riskFees = [];

}
      vm.treatyManagement.riskFees.push(riskfee);
      vm.riskFeeRate = '';
      vm.treatyReference = '';
      vm.riskFeeRatePeriod = '';
      vm.riskfeeComments = '';
      vm.minimumRiskFee = '';
      vm.minimumRiskFeeAmount = '';
      vm.riskFeeCurrency = null;

      vm.band = vm.treatyManagement.riskFees.length;
    };

    vm.removeRiskFee = function (index) {
      vm.treatyManagement.riskFees.splice(index, 1);
      vm.band = vm.band - 1;
    };

    $scope.$watch('vm.riskFeeRate', function () {
      vm.treatyReference = '';
      vm.riskFeeRatePeriod = '';
      vm.riskfeeComments = '';
      vm.minimumRiskFee = 0;
      vm.minimumRiskFeeAmount = '';
      vm.riskFeeCurrency = null;
    });

    vm.remainInProcess = function () {
      $animate.enabled(false);
      var dialog = InteractionsService.alertParentScrollFalseDialog("Input Status on Treaty Management remains In-Process", '#myModal');
      InteractionsService.showFinallyDialog(dialog, function () { vm.save(); });
    };

    vm.movedToComplete = function() {
      var confirmChange =  InteractionsService.alertParentScrollFalseDialog(msm, '#myModal');
      vm.treatyManagement.inputStatus = 'Complete';
      InteractionsService.showThenDialog(confirmChange, function () { vm.save(); });
    };

    vm.saveMessage = function () {
      if (vm.treatyManagement.inputStatus !== 'Complete') {
        var message = 'Are you ready to update Input Status on Treaty Management from In Process to Complete?';
        var confirm = InteractionsService.confirmBasicParentNoScrollDialog(message, '#myModal');
        InteractionsService.showThenDialog(confirm, function () {
          vm.movedToComplete();
        }, function () {
          vm.remainInProcess();
        });
      } else vm.save();
    };

    vm.calendarizationChange = function () {
      if (vm.treatyManagement.calendarization === 'No') {
        vm.treatyManagement.calendarizationTreatyRefer = '';
        vm.treatyManagement.calendarizationComments = '';
      }
    };

    vm.refundChange = function () {
      if (vm.treatyManagement.refundEliminationFlag === 'No') {
        vm.treatyManagement.refundEliminationRef = '';
        vm.treatyManagement.refundComments = '';
      }

    };

    // vm.copyTreatyManagement = function () {
    //   angular.forEach(vm.treaty.entityDates, function (entityDate) {
    //     if (entityDate.entityType === 'Settlementdate')
    //       vm.settlementDue = entityDate;
    //   });
    //   vm.treatyManagement.brokerPeriod = "Risk Fee Rec'd/Settlement Date";
    //   vm.treatyManagement.dayTypePosition = 'Calendar Days After';
    //   vm.treatyManagement.brokerFrequency = angular.copy(vm.treaty.settlementFrequency);
    //   vm.treatyManagement.numberOfDays = angular.copy(vm.settlementDue.numberOfDays);
    // };

    // vm.switchRisk = function () {
    //   if (vm.minimumRiskFee === 0) {
    //     vm.riskFeeCurrency = null;
    //     vm.minimumRiskFeeAmount = '';
    //   }
    // };
    vm.accountingPeriodChange = function () {
      if (!vm.treatyManagement.accountingFrequency)
        vm.treatyManagement.accountingTreatyReference = '';
    };
    vm.quotaShareChange = function () {
      if (vm.treatyManagement.quotaShare === undefined)
        vm.treatyManagement.quotaShareComment = '';
    };

    vm.changeRecapDate = function () {
      if (vm.treatyManagement.summaryDate === undefined){
        vm.treatyManagement.treatyReferenceRecapture = '';
        vm.treatyManagement.recaptureComment = '';
      }
    };

    vm.confirmationRqst= function (){
      vm.treatyManagement.confirmationRequestedComment=null;
    }
  }
})();
