(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .factory('TreatyManagementSearch', TreatyManagementSearch);
  
  TreatyManagementSearch.$inject = ['$resource'];
  
  function TreatyManagementSearch($resource) {
    var resourceUrl = 'api/_search/treaty-managements/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true}
    });
  }
})();
