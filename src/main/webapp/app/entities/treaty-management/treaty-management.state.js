(function () {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('treaty-management', {
        parent: 'entity',
        url: '/treaty-management?page&sort&search',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treatyManagement.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty-management/treaty-managements.html',
            controller: 'TreatyManagementController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,asc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
            return {
              page: PaginationUtil.parsePage($stateParams.page),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate($stateParams.sort),
              ascending: PaginationUtil.parseAscending($stateParams.sort),
              search: $stateParams.search
            };
          }],
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treatyManagement');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }]
        }
      })
      .state('treaty-management-detail', {
        parent: 'entity',
        url: '/treaty-management/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treatyManagement.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty-management/treaty-management-detail.html',
            controller: 'TreatyManagementDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treatyManagement');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'TreatyManagement', function ($stateParams, TreatyManagement) {
            return TreatyManagement.get({id: $stateParams.id});
          }]
        }
      })
      .state('treaty-management-detail-treaty', {
        parent: 'entity',
        url: '/treaty-management/treaty/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treatyManagement.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty-management/treaty-management-detail.html',
            controller: 'TreatyManagementDetailController',
            controllerAs: 'vm'
          }
        },
     resolve: {
         translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
        $translatePartialLoader.addPart('treatyManagement');
            return $translate.refresh();
        }],
         entity: ['$stateParams', 'TreatyManagementCustomSrv', function ($stateParams, TreatyManagementCustomSrv) {
         return TreatyManagementCustomSrv.get({id: $stateParams.id});

         }]
       }
      })
      .state('treaty-management-all', {
        parent: 'entity',
        url: '/treaty-management/treaties/{id}?page&sort&search',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treatyManagement.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty-management/treaty-managements.html',
            controller: 'TreatyManagementController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,asc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
            return {
              page: PaginationUtil.parsePage($stateParams.page),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate($stateParams.sort),
              ascending: PaginationUtil.parseAscending($stateParams.sort),
              search: $stateParams.search
            };
          }],
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treatyManagement');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'TreatyManagementCustomSrv', function ($stateParams, TreatyManagementCustomSrv) {
            return TreatyManagementCustomSrv.get({id: $stateParams.id});
          }]
        }
      })
      .state('treaty-management.new', {
        parent: 'treaty-management',
        url: '/new/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-management/treaty-management-dialog.html',
            controller: 'TreatyManagementDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  eucSignificance: null,
                  eucComplexity: null,
                  eucRisk: null,
                  eucUse: null,
                  quotaShare: null,
                  comments: null,
                  gracePeriod: null,
                  latePayment: null,
                  summary: null,
                  summaryDate: null,
                  seriatimType: null,
                  riskFeeRate: null,
                  riskFeeRatePeriod: null,
                  enterpriseId: null,
                  businessUnit: null,
                  currency: null,
                  retrocession: null,
                  recaptureTerm: null,
                  seriatimReporting: null,
                  activeStatus: null,
                  seriatimReportingDueFreq: null,
                  id: null
                };
              }
            }
          }).result.then(function () {
            $state.go('treaty', {id:$stateParams.id}, {reload: true});
          }, function () {
            $state.go('treaty', {id:$stateParams.id}, {reload: true});
          });
        }]
      })
      .state('treaty-mgmt.edit-management', {
        parent: 'treaty-mgmt',
        url: '/edit-management/{dealId}/{treatyId}/{hash}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-management/treaty-management-dialog.html',
            controller: 'TreatyManagementDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['TreatyManagementCustomSrv', function (TreatyManagementCustomSrv) {
                return TreatyManagementCustomSrv.get({id: $stateParams.treatyId});
              }]
            }
          }).result.then(function () {
            $state.go('treaty-mgmt', {dealId: $stateParams.dealId, treatyId: $stateParams.treatyId}, {reload: true});
          }, function () {
            $state.go('treaty-mgmt', {dealId: $stateParams.dealId, treatyId: $stateParams.treatyId}, {reload: true});
          });
        }]
      })
      .state('treaty-detail.edit-management', {
        parent: 'treaty-detail',
        url: '/edit-management/{dealId}/{treatyId}/{hash}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-management/treaty-management-dialog.html',
            controller: 'TreatyManagementDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['TreatyManagementCustomSrv', function (TreatyManagementCustomSrv) {
                return TreatyManagementCustomSrv.get({id: $stateParams.treatyId});
              }]
            }
          }).result.then(function () {
            $state.go('treaty-detail', {dealId: $stateParams.dealId, treatyId: $stateParams.treatyId}, {reload: true});
          }, function () {
            $state.go('treaty-detail', {dealId: $stateParams.dealId, treatyId: $stateParams.treatyId}, {reload: true});
          });
        }]
      })
      .state('treaty-management.delete', {
        parent: 'treaty-management',
        url: '/{id}/delete/{treatyId}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-management/treaty-management-delete-dialog.html',
            controller: 'TreatyManagementDeleteController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['TreatyManagement', function (TreatyManagement) {
                return TreatyManagement.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('treaty-management-all', {id:$stateParams.treatyId}, {reload: true});
          }, function () {
            $state.go('treaty-management-all', {id:$stateParams.treatyId}, {reload: true});
          });
        }]
      })
      .state('treaty-management.edit-detail', {
        parent: 'entity',
        url: '/treaty-management/edit-detail/{id}/{dealId}/{hash}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-management/treaty-management-dialog.html',
            controller: 'TreatyManagementDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
           resolve: {
             translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
               $translatePartialLoader.addPart('treatyManagement');
               $translatePartialLoader.addPart('global');
               return $translate.refresh();
             }],
             entity: ['TreatyManagementCustomSrv', function (TreatyManagementCustomSrv) {
                return TreatyManagementCustomSrv.get({id: $stateParams.id});
              }]
           }
          }).result.then(function () {
            $state.go('treaty-detail', {id:$stateParams.id}, {reload: true});
          }, function () {
            $state.go('treaty-detail', {id:$stateParams.id}, {reload: true});
          });
        }]
      })
      .state('treaty-management.newMngt', {
        parent: 'entity',
        url: '/treaty-management/treaty/{id}/new/',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-management/treaty-management-dialog.html',
            controller: 'TreatyManagementDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {

                   id: null
                };
              }
            }
          }).result.then(function () {
       $state.go('treaty-management-detail-treaty',{id:$stateParams.id}, {reload: true});
          }, function () {
           $state.go('treaty', {id:$stateParams.id}, {reload: true});
          });
        }]
      })


    ;
  }

})();
