(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('TreatyManagement', TreatyManagement);

  TreatyManagement.$inject = ['$resource', 'DateUtils'];

  function TreatyManagement($resource, DateUtils) {
    var resourceUrl = 'api/treaty-managements/:id';

    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          data.summaryDate = DateUtils.convertLocalDateFromServer(data.summaryDate);
          return data;
        }
      },
      'update': {
        method: 'PUT',
        transformRequest: function (data) {
          data.summaryDate = DateUtils.convertLocalDateToServer(data.summaryDate);
          return angular.toJson(data);
        }
      },
      'save': {
        method: 'POST',
        transformRequest: function (data) {
         // data.summaryDate = DateUtils.convertLocalDateToServer(data.summaryDate);
          return angular.toJson(data);
        }
      }
    });
  }
})();
