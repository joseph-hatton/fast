(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('TreatyManagementCustomSrv', TreatyManagementCustomSrv);

  TreatyManagementCustomSrv.$inject = ['$resource', 'DateUtils'];

  function TreatyManagementCustomSrv($resource, DateUtils) {
    var resourceUrl = 'api/treaty-managements/treaty/:id';
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
        if(data === "" || data === null || data === undefined) return null;
          data = angular.fromJson(data);
          data.summaryDate = DateUtils.convertLocalDateFromServer(data.summaryDate);
          return data;
        }
      },
      'update': {
        method: 'PUT',
        transformRequest: function (data) {
          data.summaryDate = DateUtils.convertLocalDateToServer(data.summaryDate);
          return angular.toJson(data);
        }
      },
      'save': {
        method: 'POST',
        transformRequest: function (data) {
          data.summaryDate = DateUtils.convertLocalDateToServer(data.summaryDate);
          return angular.toJson(data);
        }
      }
    });
  }
})();
