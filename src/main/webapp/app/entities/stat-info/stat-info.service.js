(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('StatInfo', StatInfo);

    StatInfo.$inject = ['$resource', 'DateUtils', '$q', '$filter'];

    function StatInfo ($resource, DateUtils, $q, $filter) {
        var url = "api/stat-info";
        var item = {
            "modify": $resource(url, {}, {
                'update': {
                    isArray: false,
                    method: 'PUT',
                    transformRequest: function (data) {
                        return angular.toJson(data);
                    },
                    transformResponse: function (data) {
                        data = angular.fromJson(data);
                        data.lastUpdated = data.lastUpdated ? moment.utc(data.lastUpdated).local() : null;
                        return data;
                    }
                },
                'save': {
                    isArray: false,
                    method: 'POST',
                    transformRequest: function (data) {
                        return angular.toJson(data);
                    },
                    transformResponse: function (data) {
                        data = angular.fromJson(data);
                        data.lastUpdated = data.lastUpdated ? moment.utc(data.lastUpdated).local() : null;
                        return data;
                    }
                }
            }),
            "load": $resource(url + "/:title", { }, {
                'all': {
                    isArray: true,
                    method: 'GET',
                    transformResponse: function (data) {
                        data = angular.fromJson(data);
                        data.lastUpdated = data.lastUpdated ? moment.utc(data.lastUpdated).local() : null;
                        return data;
                    }
                },
                'byTitle': {
                    method: 'GET',
                    isArray: true,
                    interceptor: {
                        response: function(data){
                            data = data.data;
                            if(data.length === 1){
                                data = data[0];
                                data.lastUpdated = data.lastUpdated ? moment.utc(data.lastUpdated).local() : null;
                                return $q.resolve(data);
                            } else {
                                return $q.reject({
                                    message: "Result count is " + data.length + " and shouldn't be.",
                                    result: data
                                });
                            }
                        },
                        responseError: function(result){
                            return result;
                        }
                    }
                }
            })
        };

        return item;
    }
})();
