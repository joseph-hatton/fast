(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .controller('SeriatimTypeDeleteController', SeriatimTypeDeleteController);
  
  SeriatimTypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'SeriatimType'];
  
  function SeriatimTypeDeleteController($uibModalInstance, entity, SeriatimType) {
    var vm = this;
    vm.seriatimType = entity;
    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };
    vm.confirmDelete = function (id) {
      SeriatimType.delete({id: id},
        function () {
          $uibModalInstance.close(true);
        });
    };
  }
})();
