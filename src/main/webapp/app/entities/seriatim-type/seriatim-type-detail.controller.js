(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('SeriatimTypeDetailController', SeriatimTypeDetailController);

  SeriatimTypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'SeriatimType', 'DetailService'];

  function SeriatimTypeDetailController($scope, $rootScope, $stateParams, entity, SeriatimType, DetailService) {
    this.prototype = Object.create(DetailService.prototype);

    this.seriatimType = entity;

    DetailService.call(this, $scope, $rootScope, $stateParams, SeriatimType, this.seriatimType, "SeriatimType");
  }
})();
