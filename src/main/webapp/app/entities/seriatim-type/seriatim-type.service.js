(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('SeriatimType', SeriatimType);
  
  SeriatimType.$inject = ['$resource'];
  
  function SeriatimType($resource) {
    var resourceUrl = 'api/seriatim-types/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          return data;
        }
      },
      'update': {method: 'PUT'}
    });
  }
})();
