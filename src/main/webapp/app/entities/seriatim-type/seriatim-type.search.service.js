(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .factory('SeriatimTypeSearch', SeriatimTypeSearch);
  
  SeriatimTypeSearch.$inject = ['$resource'];
  
  function SeriatimTypeSearch($resource) {
    var resourceUrl = 'api/_search/seriatim-types/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true}
    });
  }
})();
