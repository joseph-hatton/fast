(function () {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('seriatim-type', {
        parent: 'entity',
        url: '/seriatim-type',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.seriatimType.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/seriatim-type/seriatim-types.html',
            controller: 'SeriatimTypeController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('seriatimType');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }]
        }
      })
      .state('seriatim-type-detail', {
        parent: 'entity',
        url: '/seriatim-type/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.seriatimType.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/seriatim-type/seriatim-type-detail.html',
            controller: 'SeriatimTypeDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('seriatimType');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'SeriatimType', function ($stateParams, SeriatimType) {
            return SeriatimType.get({id: $stateParams.id});
          }]
        }
      })
      .state('seriatim-type.new', {
        parent: 'seriatim-type',
        url: '/new',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/seriatim-type/seriatim-type-dialog.html',
            controller: 'SeriatimTypeDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  name: null,
                  description: null,
                  id: null
                };
              }
            }
          }).result.then(function () {
            $state.go('seriatim-type', null, {reload: true});
          }, function () {
            $state.go('seriatim-type');
          });
        }]
      })
      .state('seriatim-type.edit', {
        parent: 'seriatim-type',
        url: '/{id}/edit',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/seriatim-type/seriatim-type-dialog.html',
            controller: 'SeriatimTypeDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['SeriatimType', function (SeriatimType) {
                return SeriatimType.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('seriatim-type', null, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      })
      .state('seriatim-type.delete', {
        parent: 'seriatim-type',
        url: '/{id}/delete',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/seriatim-type/seriatim-type-delete-dialog.html',
            controller: 'SeriatimTypeDeleteController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['SeriatimType', function (SeriatimType) {
                return SeriatimType.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('seriatim-type', null, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      });
  }

})();
