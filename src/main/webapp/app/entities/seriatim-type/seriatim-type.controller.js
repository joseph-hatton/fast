(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .controller('SeriatimTypeController', SeriatimTypeController);
  
  SeriatimTypeController.$inject = ['$scope', '$state', 'SeriatimType', 'SeriatimTypeSearch'];
  
  function SeriatimTypeController($scope, $state, SeriatimType, SeriatimTypeSearch) {
    var vm = this;
    vm.seriatimTypes = [];
    vm.loadAll = function () {
      SeriatimType.query(function (result) {
        vm.seriatimTypes = result;
      });
    };
    
    vm.search = function () {
      if (!vm.searchQuery) {
        return vm.loadAll();
      }
      SeriatimTypeSearch.query({query: vm.searchQuery}, function (result) {
        vm.seriatimTypes = result;
      });
    };
    vm.loadAll();
    
  }
})();
