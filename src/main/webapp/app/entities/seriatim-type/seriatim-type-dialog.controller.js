(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('SeriatimTypeDialogController', SeriatimTypeDialogController);

  SeriatimTypeDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'SeriatimType', 'DialogService'];

  function SeriatimTypeDialogController ($scope, $stateParams, $uibModalInstance, entity, SeriatimType, DialogService) {
    this.prototype = Object.create(DialogService.prototype);

    var datePickerItemsRun = function (vm) {};

    this.seriatimType = entity;

    DialogService.call(this, $scope, $stateParams, $uibModalInstance, entity, SeriatimType, this.seriatimType, "SeriatimType", datePickerItemsRun);
  }
})();

