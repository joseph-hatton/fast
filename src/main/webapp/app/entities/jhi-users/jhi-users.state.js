(function() {
    'use strict';

    angular
        .module('finreApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('users', {
            parent: 'entity',
            url: '/users',
            data: {
                authorities: ['ROLE_EAM_TEAM', 'ROLE_APP_FINRE_IT_SUPPORT']
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/jhi-users/jhi-users.html',
                    controller: 'JhiUsersController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('users.edit', {
            parent: 'users',
            url: '/edit/{id}',
            data: {
                authorities: ['ROLE_EAM_TEAM', 'ROLE_APP_FINRE_IT_SUPPORT']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/jhi-users/jhi-users-dialog.html',
                    controller: 'JhiUsersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['JhiUsers', function(JhiUsers) {
                            return JhiUsers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('users.new', {
            parent: 'users',
            url: '/new',
            data: {
                authorities: ['ROLE_EAM_TEAM', 'ROLE_APP_FINRE_IT_SUPPORT']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/jhi-users/jhi-users-dialog.html',
                    controller: 'JhiUsersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                email: "",
                                login: "",
                                firstName: "",
                                lastName: "",
                                activated: 1,
                                auths: []
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('^', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('authority-descriptions', {
            parent: 'users',
            url: '/authority-descriptions',
            data: {
                authorities: ['ROLE_EAM_TEAM', 'ROLE_APP_FINRE_IT_SUPPORT']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/jhi-users/jhi-authority-popup.html',
                    controller: 'JhiAuthorityPopupController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg'
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
