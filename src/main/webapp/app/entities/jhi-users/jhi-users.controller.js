(function() {
    'use strict';

    angular.module('finreApp').controller('JhiUsersController', JhiUsersController);

    JhiUsersController.$inject = ['$scope', '$state', 'JhiUsers', '$http', 'InteractionsService', 'Principal'];

    function JhiUsersController ($scope, $state, JhiUsers, $http, InteractionsService, Principal) {
        var vm = this;

        vm.loadAll = function() {
            JhiUsers.query().$promise
            .then(function(result) {
                vm.users = result;
                vm.users.forEach(function(user){
                    user.login = user.login.replace('@RGARE.NET', '');
                });
            });
        };

        vm.loadAll();

        vm.removeUser = function(user){
            var confirm = InteractionsService.confirm("Are you sure you want to delete " + user.firstName + " " + user.lastName + " from the system?", '#myModal');
            InteractionsService.showThenDialog(confirm, function(){
                JhiUsers.delete({ id: user.id }).$promise
                .then(function(){
                    vm.users = vm.users.filter(function(i){ return i.id !== user.id; });
                });
            });
        };

        Principal.identity().then(function (account) {
            vm.currentUserId = account.id;
        });
    }
})();
