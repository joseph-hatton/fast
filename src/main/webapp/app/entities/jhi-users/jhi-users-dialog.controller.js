(function () {
    'use strict';

    angular.module('finreApp').controller('JhiUsersDialogController', JhiUsersDialogController);

    JhiUsersDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'JhiUsers', '$q', 'Principal', 'InteractionsService', '$log'];

    function JhiUsersDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, JhiUsers, $q, Principal, InteractionsService, $log) {
        var vm = this;
        
        vm.user = entity;

        vm.authChoices = [
            { name: "ROLE_APP_STRUCTURED_FINANCE_VIEW_ONLY" },
            { name: "ROLE_ADMIN" },
            { name: "ROLE_APP_FINRE_PROCESSOR" },
            { name: "ROLE_APP_FINRE_IT_SUPPORT" },
            { name: "ROLE_APP_FINRE_VIEW_ONLY" },
            { name: "ROLE_APP_FINRE_OPS_ADMIN" },
            { name: "ROLE_APP_STRUCTURED_FINANCE_ADMIN" },
            { name: "ROLE_EAM_TEAM" }
        ];

        $q.resolve(vm.user).then(function(){
            vm.authChoicesShortened = vm.authChoices.map(function(i){ return i.name; });

            vm.user.auths.forEach(function(auth){
                var choice = vm.authChoices.find(function(c){ return c.name === auth.authorityName; });
                if(choice){
                    choice.isChecked = true;
                } else {
                    $log.error("The auths from the user should match the choices.", auth);
                }
            });


            vm.auths = vm.user.auths.map(function(i){ return i.authorityName; });
            vm.user.login = vm.user.login.replace('@RGARE.NET', '');
        });

        Principal.identity().then(function (account) {
            vm.lastUser = account.login;
            vm.currentUserId = account.id;
        });

        vm.clear = function(){
            $uibModalInstance.close();
        };  

        vm.save = function(){
            vm.prepPermissons();
            vm.user.login = vm.user.login + "@RGARE.NET";
            var store = vm.user.id ? 'update' : 'save';
            var message = vm.user.id ? "The information/permissons for " + vm.user.firstName + " " + vm.user.lastName + " have been set successfully." : "The user " + vm.user.firstName + " " + vm.user.lastName + " has been created succesfully and their permissions have been set.";
            
            if(!vm.user.id){
                vm.user.creationTs = moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
                vm.user.creationUser = vm.lastUser;
            }
            vm.user.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
            vm.user.lastUser = vm.lastUser;

            JhiUsers[store](vm.user).$promise
            .then(function(result){
                $uibModalInstance.close();
            })
            .then(function(){
                var alert = InteractionsService.alert(message, "#myModal");
                return InteractionsService.showThenDialog(alert);
            })
            .catch(function(err){
                $log.warn(err);
                $uibModalInstance.close();
                var alert = InteractionsService.alert("There was a problem saving.", "#myModal");
                return InteractionsService.showThenDialog(alert);
            });
        };

        vm.prepPermissons = function(){
            vm.auths = vm.authChoices
            .filter(function(i){
                return i.isChecked;
            })
            .map(function(i){
                return i.name;
            });

            vm.user.auths = vm.user.auths.filter(function(i){ 
                return vm.auths.find(function(j){ return j === i.authorityName; }); 
            });
            vm.auths.forEach(function(auth){
                var authObj = vm.user.auths.find(function(i){ return auth === i.authorityName; });
                if(!authObj){
                    authObj = {
                        authorityName: auth,
                        creationTs: moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z',
                        creationUser: vm.lastUser
                    };
                    vm.user.auths.push(authObj);
                }
                authObj.lastUser = vm.lastUser;
                authObj.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
            });
        };
    }
})();
