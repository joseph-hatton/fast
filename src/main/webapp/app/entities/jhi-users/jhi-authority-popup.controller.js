(function() {
    'use strict';

    angular.module('finreApp').controller('JhiAuthorityPopupController', JhiAuthorityPopupController);
    
    JhiAuthorityPopupController.$inject = ['$scope', '$state', '$http', 'InteractionsService', '$uibModalInstance'];
    
    function JhiAuthorityPopupController ($scope, $state, $http, InteractionsService, $uibModalInstance) {
        var vm = this;

        vm.loadAll = function() {
            $http({ method:'GET', url: '/api/jhi-authority' })
            .then(function(result){
                vm.authorities = result.data;
            });
        };
        
        vm.loadAll();

        vm.close = function(){
            $uibModalInstance.close();
        };
    }
})();
