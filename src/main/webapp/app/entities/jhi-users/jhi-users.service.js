(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('JhiUsers', JhiUsers);

    JhiUsers.$inject = ['$resource'];

    function JhiUsers ($resource) {
        var resourceUrl =  'api/jhiUser/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': { method: 'GET' },
            'update': { method:'PUT' },
            'save': { method:'POST' },
            'delete': { method:'DELETE' }
        });
    }
})();
