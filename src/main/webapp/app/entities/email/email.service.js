(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('Email', Email);

  Email.$inject = ['$resource', 'DateUtils'];

  function Email($resource, DateUtils) {
    var resourceUrl = 'api/email/transactions';
    var resourceAttachUrl = 'api/email/transactions/attachment';

    return {
      send: $resource(resourceUrl, {}, {
        email: {
          method: 'POST',
          transformResponse: function (data) {
            return data;
          }
        }
      }),
      sendAttached: $resource(resourceAttachUrl, {}, {
        email: {
          method: 'POST',
          transformResponse: function (data) {
            return data;
          }
        }
      })
    }
  }
})();

