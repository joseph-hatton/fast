(function () {
    'use strict';

    angular
    .module('finreApp')
    .controller('FastopediaController', FastopediaController);

    FastopediaController.$inject = ['$scope', '$location', '$anchorScroll', 'Fastopedia', '$filter', 'InteractionsService', 'Principal', '$q', '$document', 'taRegisterTool'];

    function FastopediaController($scope, $location, $anchorScroll, Fastopedia, $filter, InteractionsService, Principal, $q, $document) {
        var vm = this;
        vm.editorCount = 0;
        
        vm.toolbar = [
            ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'pre'],
            ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo', 'clear'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent'],
            ['html','insertLink', 'imageUpload', 'wordcount', 'charcount']
        ];

        vm.buildParentChildrenRelationships = function(){
            vm.documents.forEach(function(item) {
                item.subdocuments = $filter('filter')(vm.documents, function(d){
                    return d.parentId === item.id;
                });
            });
        };

        vm.order = function(){
            vm.documents = $filter('filter')(vm.documentsMaster, function(item){
                return item.parentId === 0;
            });

            vm.documents = $filter('orderBy')(vm.documents, 'order');
            vm.documentsMaster.forEach(function(item) {
                item.subdocuments = $filter('orderBy')(item.subdocuments, 'order');
            });
        };

        vm.loadAll = function () {
            return Fastopedia.loadAll().$promise
            .then(processResults, onError);

            function processResults(result) {
                vm.documents = result;
                vm.documentsMaster = result;
                vm.buildParentChildrenRelationships();                              
                vm.order();
                var options = {
                    shouldSort: true,
                    tokenize: true,
                    threshold: 0,
                    location: 0,
                    distance: 0,
                    maxPatternLength: 32,
                    minMatchCharLength: 1,
                    keys: [
                        "title",
                        "content"
                    ]
                };
            }
        };

        vm.loadAll()
        .then(function(){
            vm.documents.forEach(function(item){
                item.update = Fastopedia.update;
            });
        });

        vm.saveContent = function (item) {
            var modify = (item.id == null) ? "save" : "update";
            return Fastopedia[modify](item).$promise
            .then(onSuccess.bind(null, item), onError);
        };

        vm.saveAll = function(){
            var promises = [];
            vm.documentsMaster.forEach(function(item){
                promises.push(vm.saveContent(item));
            });
            return $q.all(promises);
        };

        vm.edit = function(item){
            item.editing = true;
        };

        vm.doneEditing = function(item){
            item.editing = false;
        };

        vm.createNewItem = function(parentId){
            var item = {
                title: "Default",
                content: "Default",
                isActive: 1,
                order: 0,
                parentId: parentId ? parentId : 0,
                subdocuments: [],
                images: []
            };
            vm.saveContent(item)
            .then(function(){
                var docs;
                if(item.parentId !== 0){
                    var parent = vm.documentsMaster.find(function(value){
                        return item.parentId === value.id;
                    });
                    docs = parent.subdocuments;
                    parent.open = true;
                } else {
                    docs = vm.documents;
                }

                docs.forEach(function(item){
                    item.order++;
                });

                docs.unshift(item);
                vm.documentsMaster.push(item);
                vm.saveAll();
            });
        };

        vm.remove = function(arr, index){
            if(arr[index].subdocuments.length === 0){
                var dialog = InteractionsService.confirmBasicParentNoScrollDialog("Are you sure you want to delete item " + arr[index].title + "?", "#myModal");
                InteractionsService.showThenDialog(dialog, function(){
                    arr[index].isActive = 0;
                    for(var i = index + 1; i < arr.length; i++){
                        arr[i].order--;
                    }
                    vm.saveAll()
                    .then(function(){
                        arr.splice(index, 1);
                    });
                });
            }
        };

        Principal.isAdmin().then(function(result){
            vm.admin = result;
        });
        

        vm.moveUp = function(list, index){
            if(index > 0){
                var index1 = index;
                var index2 = index - 1;
                var item1 = list[index1];
                var item2 = list[index2];

                swap(list, index1, index2);

                item1.order--;
                item2.order++;

                vm.saveContent(item1);
                vm.saveContent(item2);
            }
        };

        vm.moveDown = function(list, index){    
            if(index < list.length - 1){                
                var index1 = index;
                var index2 = index + 1;
                var item1 = list[index1];
                var item2 = list[index2];

                swap(list, index1, index2);

                item1.order++;
                item2.order--;

                vm.saveContent(item1);
                vm.saveContent(item2);
            }
        };

        vm.toggleOpen = function (item){
            item.open = !item.open;
        };

        vm.search = function(){
            Fastopedia.search({ searchQuery: vm.searchQuery }).$promise
            .then(function(result){
                vm.searchResults = result;
            });
            vm.searching = true;
        };

        vm.clearSearch = function(){
            vm.searching = false;
            vm.searchQuery = '';
            vm.searchResults = [];
        };

        vm.enterOnSearch = function(keyEvent){
            if(keyEvent.which === 13)
                vm.search();
        };

        vm.setOpen = function(value){
            vm.documentsMaster.forEach(function(item){
                item.open = value;
            });
        };



        // $scope.uploadImage = function(element) {
        //     var file = element.files[0];
        //     var reader = new FileReader();
        //     reader.onloadend = function() {
        //         var img = document.createElement('img');
        //         img.src = reader.result;
        //         if (currentSelectionUpload.getRangeAt && currentSelectionUpload.rangeCount) {
        //             var range = currentSelectionUpload.getRangeAt(0);
        //             range.deleteContents();
        //             range.insertNode(img);
        //             Fastopedia.update(currentDocumentUpload);
        //         }
        //     };
        //     angular.element(document.querySelector("#imageUploader")).val("");
        //     reader.readAsDataURL(file);
        // };

        // vm.readyImageUploader = function(item){
        //     item.vm = vm;
        //     item.Fastopedia = Fastopedia;
        // };

        function swap(list, item1, item2){
            var temp = list[item1];
            list[item1] = list[item2];
            list[item2] = temp;
        }

        function onSuccess(item, value) {
            item.id = value.id;
            vm.contentResponseMsg = "The save was successful!";
        }

        function onError() {
            vm.contentResponseMsg = "There was an error saving.";
        }
    }
})();
