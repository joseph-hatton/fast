(function() {
    'use strict';

    angular
        .module('finreApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('fastopedia', {
            parent: 'app',
            url: '/fastopedia',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY','ROLE_APP_STRUCTURED_FINANCE_ADMIN','ROLE_APP_STRUCTURED_FINANCE_VIEW_ONLY']
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/fastopedia/fastopedia.html',
                    controller: 'FastopediaController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        });
    }

})();
