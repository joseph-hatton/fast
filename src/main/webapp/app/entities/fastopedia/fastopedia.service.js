(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('Fastopedia', Fastopedia);

    Fastopedia.$inject = ['$resource', 'DateUtils'];

    function Fastopedia ($resource) {
        var resourceUrl =  'api/fastopedia/:searchQuery';

        return $resource(resourceUrl, {}, {
            'loadAll': {
                isArray: true,
                method: 'GET',
                transformResponse: function (data) {
                    return angular.fromJson(data);
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    return angular.toJson(data);
                }
            },
            'search': {
                method: 'GET',
                isArray: true
            }
        });
    }
})();
