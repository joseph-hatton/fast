(function() {
  'use strict';
  angular
    .module('finreApp')
    .factory('CurrentUser', CurrentUser);

  CurrentUser.$inject = ['$resource', 'DateUtils'];

  function CurrentUser ($resource, DateUtils) {
    var resourceUrl =  '__auth/current-user';

    return $resource(resourceUrl, {}, {
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          // if the data is not json, the router probably redirected to the secureauth login page (means our refreshtoken and access token are timed out)
          data = angular.fromJson(data);
          return data;
        }
      },
      'del': { method: 'DELETE' }
    });
  }
})();
