(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('ContactSubtypeDialogController', ContactSubtypeDialogController);

  ContactSubtypeDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'ContactSubtype', 'DialogService'];

  function ContactSubtypeDialogController ($scope, $stateParams, $uibModalInstance, entity, ContactSubtype, DialogService) {
    this.prototype = Object.create(DialogService.prototype);

    var datePickerItemsRun = function (vm) { };

    this.contactSubtype = entity;

    DialogService.call(this, $scope, $stateParams, $uibModalInstance, entity, ContactSubtype, this.contactSubtype, "ContactSubtype", datePickerItemsRun);
  }
})();
