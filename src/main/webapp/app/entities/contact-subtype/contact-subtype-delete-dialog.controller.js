(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .controller('ContactSubtypeDeleteController', ContactSubtypeDeleteController);
  
  ContactSubtypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'ContactSubtype'];
  
  function ContactSubtypeDeleteController($uibModalInstance, entity, ContactSubtype) {
    var vm = this;
    vm.contactSubtype = entity;
    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };
    vm.confirmDelete = function (id) {
      ContactSubtype.delete({id: id},
        function () {
          $uibModalInstance.close(true);
        });
    };
  }
})();
