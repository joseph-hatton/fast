(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .controller('ContactSubtypeController', ContactSubtypeController);
  
  ContactSubtypeController.$inject = ['$scope', '$state', 'ContactSubtype', 'ContactSubtypeSearch'];
  
  function ContactSubtypeController($scope, $state, ContactSubtype, ContactSubtypeSearch) {
    var vm = this;
    vm.contactSubtypes = [];
    vm.loadAll = function () {
      ContactSubtype.query(function (result) {
        vm.contactSubtypes = result;
      });
    };
    
    vm.search = function () {
      if (!vm.searchQuery) {
        return vm.loadAll();
      }
      ContactSubtypeSearch.query({query: vm.searchQuery}, function (result) {
        vm.contactSubtypes = result;
      });
    };
    vm.loadAll();
    
  }
})();
