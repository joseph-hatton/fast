(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('ContactSubtype', ContactSubtype);
  
  ContactSubtype.$inject = ['$resource'];
  
  function ContactSubtype($resource) {
    var resourceUrl = 'api/contact-subtypes/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          return data;
        }
      },
      'update': {method: 'PUT'}
    });
  }
})();
