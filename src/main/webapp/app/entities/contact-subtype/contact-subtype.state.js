(function () {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('contact-subtype', {
        parent: 'entity',
        url: '/contact-subtype',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.contactSubtype.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/contact-subtype/contact-subtypes.html',
            controller: 'ContactSubtypeController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('contactSubtype');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }]
        }
      })
      .state('contact-subtype-detail', {
        parent: 'entity',
        url: '/contact-subtype/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.contactSubtype.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/contact-subtype/contact-subtype-detail.html',
            controller: 'ContactSubtypeDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('contactSubtype');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'ContactSubtype', function ($stateParams, ContactSubtype) {
            return ContactSubtype.get({id: $stateParams.id});
          }]
        }
      })
      .state('contact-subtype.new', {
        parent: 'contact-subtype',
        url: '/new',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/contact-subtype/contact-subtype-dialog.html',
            controller: 'ContactSubtypeDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  name: null,
                  description: null,
                  id: null
                };
              }
            }
          }).result.then(function () {
            $state.go('contact-subtype', null, {reload: true});
          }, function () {
            $state.go('contact-subtype');
          });
        }]
      })
      .state('contact-subtype.edit', {
        parent: 'contact-subtype',
        url: '/{id}/edit',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/contact-subtype/contact-subtype-dialog.html',
            controller: 'ContactSubtypeDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['ContactSubtype', function (ContactSubtype) {
                return ContactSubtype.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('contact-subtype', null, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      })
      .state('contact-subtype.delete', {
        parent: 'contact-subtype',
        url: '/{id}/delete',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/contact-subtype/contact-subtype-delete-dialog.html',
            controller: 'ContactSubtypeDeleteController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['ContactSubtype', function (ContactSubtype) {
                return ContactSubtype.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('contact-subtype', null, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      });
  }

})();
