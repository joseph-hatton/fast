(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('ContactSubtypeDetailController', ContactSubtypeDetailController);

  ContactSubtypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'ContactSubtype', 'DetailService'];

  function ContactSubtypeDetailController($scope, $rootScope, $stateParams, entity, ContactSubtype, DetailService) {
    this.prototype = Object.create(DetailService.prototype);

    this.contactSubtype = entity;

    DetailService.call(this, $scope, $rootScope, $stateParams, ContactSubtype, this.contactSubtype, "ContactSubtype");
  }
})();
