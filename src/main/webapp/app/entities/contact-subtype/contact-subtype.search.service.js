(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .factory('ContactSubtypeSearch', ContactSubtypeSearch);
  
  ContactSubtypeSearch.$inject = ['$resource'];
  
  function ContactSubtypeSearch($resource) {
    var resourceUrl = 'api/_search/contact-subtypes/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true}
    });
  }
})();
