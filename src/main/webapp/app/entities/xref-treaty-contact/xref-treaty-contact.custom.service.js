(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('XrefTreatyCustomContact', XrefTreatyCustomContact);

  XrefTreatyCustomContact.$inject = ['$resource', 'DateUtils'];
  var resourceUrl = 'api/xref-treaty-contacts/treaties/:id';
  var xrefContactsUrl = 'api/xref-treaty-contacts/contacts/:id';

  function XrefTreatyCustomContact($resource, DateUtils) {
    return {
      treaties: $resource(resourceUrl, {}, {
        query: { method: 'GET', params: {}, isArray: true },
        'get': {
          method: 'GET',
          transformResponse: function (data) {
            data = angular.fromJson(data);
            return data;
          }
        }
      }),
      contacts: $resource(xrefContactsUrl, {}, {
        query: { method: 'GET', params: {}, isArray: true },
        'get': {
          method: 'GET',
          transformResponse: function (data) {
            data = angular.fromJson(data);
            return data;
          }
        }
      })
    };
  }
})();
