(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('XrefTreatyContact', XrefTreatyContact);

    XrefTreatyContact.$inject = ['$resource', 'DateUtils'];

    function XrefTreatyContact ($resource, DateUtils) {
        var resourceUrl =  'api/xref-treaty-contacts/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    return angular.toJson(data);
                }
            }
        });
    }
})();
