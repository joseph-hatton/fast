(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('RiskCriteriaDeleteController',RiskCriteriaDeleteController);

    RiskCriteriaDeleteController.$inject = ['$uibModalInstance', 'entity', 'RiskCriteria'];

    function RiskCriteriaDeleteController($uibModalInstance, entity, RiskCriteria) {
        var vm = this;

        vm.riskCriteria = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            RiskCriteria.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
