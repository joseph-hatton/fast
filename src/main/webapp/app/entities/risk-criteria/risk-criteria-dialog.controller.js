(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('RiskCriteriaDialogController', RiskCriteriaDialogController);

    RiskCriteriaDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'RiskCriteria'];

    function RiskCriteriaDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, RiskCriteria) {
        var vm = this;

        vm.riskCriteria = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.riskCriteria.id !== null) {
                RiskCriteria.update(vm.riskCriteria, onSaveSuccess, onSaveError);
            } else {
                RiskCriteria.save(vm.riskCriteria, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('finreApp:riskCriteriaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
