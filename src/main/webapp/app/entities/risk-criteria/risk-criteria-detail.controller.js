(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('RiskCriteriaDetailController', RiskCriteriaDetailController);

    RiskCriteriaDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'RiskCriteria'];

    function RiskCriteriaDetailController($scope, $rootScope, $stateParams, previousState, entity, RiskCriteria) {
        var vm = this;

        vm.riskCriteria = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('finreApp:riskCriteriaUpdate', function(event, result) {
            vm.riskCriteria = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
