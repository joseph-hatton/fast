(function() {
    'use strict';

    angular
        .module('finreApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('risk-criteria', {
            parent: 'entity',
            url: '/risk-criteria',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'finreApp.riskCriteria.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/risk-criteria/risk-criteria.html',
                    controller: 'RiskCriteriaController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('riskCriteria');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('risk-criteria-detail', {
            parent: 'risk-criteria',
            url: '/risk-criteria/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'finreApp.riskCriteria.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/risk-criteria/risk-criteria-detail.html',
                    controller: 'RiskCriteriaDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('riskCriteria');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'RiskCriteria', function($stateParams, RiskCriteria) {
                    return RiskCriteria.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'risk-criteria',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('risk-criteria-detail.edit', {
            parent: 'risk-criteria-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/risk-criteria/risk-criteria-dialog.html',
                    controller: 'RiskCriteriaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['RiskCriteria', function(RiskCriteria) {
                            return RiskCriteria.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('risk-criteria.new', {
            parent: 'risk-criteria',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/risk-criteria/risk-criteria-dialog.html',
                    controller: 'RiskCriteriaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('risk-criteria', null, { reload: 'risk-criteria' });
                }, function() {
                    $state.go('risk-criteria');
                });
            }]
        })
        .state('risk-criteria.edit', {
            parent: 'risk-criteria',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/risk-criteria/risk-criteria-dialog.html',
                    controller: 'RiskCriteriaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['RiskCriteria', function(RiskCriteria) {
                            return RiskCriteria.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('risk-criteria', null, { reload: 'risk-criteria' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('risk-criteria.delete', {
            parent: 'risk-criteria',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/risk-criteria/risk-criteria-delete-dialog.html',
                    controller: 'RiskCriteriaDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['RiskCriteria', function(RiskCriteria) {
                            return RiskCriteria.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('risk-criteria', null, { reload: 'risk-criteria' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
