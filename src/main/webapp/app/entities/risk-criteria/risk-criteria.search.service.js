(function() {
    'use strict';

    angular
        .module('finreApp')
        .factory('RiskCriteriaSearch', RiskCriteriaSearch);

    RiskCriteriaSearch.$inject = ['$resource'];

    function RiskCriteriaSearch($resource) {
        var resourceUrl =  'api/_search/risk-criteria/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
