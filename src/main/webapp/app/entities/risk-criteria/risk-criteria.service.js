(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('RiskCriteria', RiskCriteria);

    RiskCriteria.$inject = ['$resource'];

    function RiskCriteria ($resource) {
        var resourceUrl =  'api/risk-criteria/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
