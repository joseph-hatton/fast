(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('RiskCriteriaController', RiskCriteriaController);

    RiskCriteriaController.$inject = ['RiskCriteria', 'RiskCriteriaSearch'];

    function RiskCriteriaController(RiskCriteria, RiskCriteriaSearch) {

        var vm = this;

        vm.riskCriteria = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            RiskCriteria.query(function(result) {
                vm.riskCriteria = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            RiskCriteriaSearch.query({query: vm.searchQuery}, function(result) {
                vm.riskCriteria = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();
