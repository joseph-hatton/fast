(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('ProductStructure', ProductStructure);
  
  ProductStructure.$inject = ['$resource'];
  
  function ProductStructure($resource) {
    var resourceUrl = 'api/product-structures/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          return data;
        }
      },
      'update': {method: 'PUT'}
    });
  }
})();
