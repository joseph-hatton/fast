(function () {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('product-structure', {
        parent: 'entity',
        url: '/product-structure',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.productStructure.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/product-structure/product-structures.html',
            controller: 'ProductStructureController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('productStructure');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }]
        }
      })
      .state('product-structure-detail', {
        parent: 'entity',
        url: '/product-structure/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.productStructure.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/product-structure/product-structure-detail.html',
            controller: 'ProductStructureDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('productStructure');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'ProductStructure', function ($stateParams, ProductStructure) {
            return ProductStructure.get({id: $stateParams.id});
          }]
        }
      })
      .state('product-structure.new', {
        parent: 'product-structure',
        url: '/new',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/product-structure/product-structure-dialog.html',
            controller: 'ProductStructureDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  name: null,
                  description: null,
                  structureCode: null,
                  id: null
                };
              }
            }
          }).result.then(function () {
            $state.go('product-structure', null, {reload: true});
          }, function () {
            $state.go('product-structure');
          });
        }]
      })
      .state('product-structure.edit', {
        parent: 'product-structure',
        url: '/{id}/edit',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/product-structure/product-structure-dialog.html',
            controller: 'ProductStructureDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['ProductStructure', function (ProductStructure) {
                return ProductStructure.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('product-structure', null, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      })
      .state('product-structure.delete', {
        parent: 'product-structure',
        url: '/{id}/delete',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/product-structure/product-structure-delete-dialog.html',
            controller: 'ProductStructureDeleteController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['ProductStructure', function (ProductStructure) {
                return ProductStructure.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('product-structure', null, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      });
  }

})();
