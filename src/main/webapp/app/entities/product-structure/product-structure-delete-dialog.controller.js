(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .controller('ProductStructureDeleteController', ProductStructureDeleteController);
  
  ProductStructureDeleteController.$inject = ['$uibModalInstance', 'entity', 'ProductStructure'];
  
  function ProductStructureDeleteController($uibModalInstance, entity, ProductStructure) {
    var vm = this;
    vm.productStructure = entity;
    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };
    vm.confirmDelete = function (id) {
      ProductStructure.delete({id: id},
        function () {
          $uibModalInstance.close(true);
        });
    };
  }
})();
