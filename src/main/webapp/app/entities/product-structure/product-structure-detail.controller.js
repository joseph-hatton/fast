(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('ProductStructureDetailController', ProductStructureDetailController);

  ProductStructureDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'ProductStructure', 'DetailService'];

  function ProductStructureDetailController($scope, $rootScope, $stateParams, entity, ProductStructure, DetailService) {
    this.prototype = Object.create(DetailService.prototype);

    this.productStructure = entity;

    DetailService.call(this, $scope, $rootScope, $stateParams, ProductStructure, this.productStructure, "ProductStructure");
  }
})();
