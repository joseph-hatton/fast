(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .controller('ProductStructureController', ProductStructureController);
  
  ProductStructureController.$inject = ['$scope', '$state', 'ProductStructure', 'ProductStructureSearch'];
  
  function ProductStructureController($scope, $state, ProductStructure, ProductStructureSearch) {
    var vm = this;
    vm.productStructures = [];
    vm.loadAll = function () {
      ProductStructure.query(function (result) {
        vm.productStructures = result;
      });
    };
    
    vm.search = function () {
      if (!vm.searchQuery) {
        return vm.loadAll();
      }
      ProductStructureSearch.query({query: vm.searchQuery}, function (result) {
        vm.productStructures = result;
      });
    };
    vm.loadAll();
    
  }
})();
