(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('ProductStructureDialogController', ProductStructureDialogController);

  ProductStructureDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'ProductStructure', 'DialogService'];

  function ProductStructureDialogController ($scope, $stateParams, $uibModalInstance, entity, ProductStructure, DialogService) {
    this.prototype = Object.create(DialogService.prototype);

    var datePickerItemsRun = function (vm) {
      vm.datePickerOpenStatus = {};
      vm.datePickerOpenStatus.lastUpdate = false;
      vm.datePickerOpenStatus.creationTS = false;
      vm.datePickerOpenStatus.effBeginTs = false;
      vm.datePickerOpenStatus.effEndTs = false;
    };

    this.productStructure = entity;

    DialogService.call(this, $scope, $stateParams, $uibModalInstance, entity, ProductStructure, this.productStructure, "ProductStructure", datePickerItemsRun);
  }
})();
