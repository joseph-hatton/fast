(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .factory('ProductStructureSearch', ProductStructureSearch);
  
  ProductStructureSearch.$inject = ['$resource'];
  
  function ProductStructureSearch($resource) {
    var resourceUrl = 'api/_search/product-structures/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true}
    });
  }
})();
