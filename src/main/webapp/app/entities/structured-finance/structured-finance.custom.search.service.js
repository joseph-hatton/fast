(function() {
  'use strict';

  angular
    .module('finreApp')
    .factory('StructuredFinanceCustomSrv', StructuredFinanceCustomSrv);

  StructuredFinanceCustomSrv.$inject = ['$resource'];

  function StructuredFinanceCustomSrv($resource) {
    var resourceUrl =  'api/structured-finance-by-deal/:dealId';

    return $resource(resourceUrl, {}, {
      'query': { method: 'GET', isArray: true}
    });
  }
})();
