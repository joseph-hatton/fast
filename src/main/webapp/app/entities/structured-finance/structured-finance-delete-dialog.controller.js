(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('StructuredFinanceDeleteController',StructuredFinanceDeleteController);

    StructuredFinanceDeleteController.$inject = ['$uibModalInstance', 'entity', 'StructuredFinance'];

    function StructuredFinanceDeleteController($uibModalInstance, entity, StructuredFinance) {
        var vm = this;
        vm.structuredFinance = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
          vm.structuredFinance.isActive=0;
            StructuredFinance.update(vm.structuredFinance,
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
