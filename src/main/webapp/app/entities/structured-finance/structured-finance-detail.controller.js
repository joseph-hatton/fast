(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('StructuredFinanceDetailController', StructuredFinanceDetailController);

    StructuredFinanceDetailController.$inject = ['$scope','$filter', '$rootScope', '$stateParams', 'entity', 'StructuredFinance',
    'Deal'];

    function StructuredFinanceDetailController($scope, $filter, $rootScope, $stateParams, entity, StructuredFinance, Deal) {
      var vm = this;
      vm.structuredFinance = entity;
      vm.dealId = $stateParams.dealId;
      vm.id = $stateParams.id;
      vm.requirements = false;
      vm.reporting = false;

      load();

      vm.structuredFinance.rptReqEffDate = $filter('date')(vm.structuredFinance.rptReqEffDate,'yyyy-MM-dd');
      angular.forEach(vm.yearEndMonthOptions, function (Month) {
        if(vm.structuredFinance.deal !== undefined){
          if(Month.id===vm.structuredFinance.deal.yearEndMonth){
            vm.dealEndMonth=Month;
          }}
      });

      function load() {
        Deal.get({id: $stateParams.dealId}, function (result) {
          vm.deal = result;
        });
        vm.yearEndMonthOptions = YEAR_END_MONTH_OPTIONS;

      };
      var unsubscribe = $rootScope.$on('finreApp:structuredFinanceUpdate', function(event, result) {
          vm.structuredFinance = result;
      });
      $scope.$on('$destroy', unsubscribe);

    }
})();
