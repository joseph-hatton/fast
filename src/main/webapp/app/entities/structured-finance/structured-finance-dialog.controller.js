(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('StructuredFinanceDialogController', StructuredFinanceDialogController);

  StructuredFinanceDialogController.$inject = ['$scope', '$timeout', '$mdDialog', '$stateParams', '$uibModalInstance',
    'entity', 'Principal', 'StructuredFinance', 'Deal', 'Frequency','StructuredTrackingCustomSrv'];

  function StructuredFinanceDialogController($scope, $timeout, $mdDialog, $stateParams, $uibModalInstance,
                                             entity, Principal, StructuredFinance, Deal, Frequency,StructuredTrackingCustomSrv) {
    var vm = this;
    vm.structuredFinance = entity;
    // vm.deals = TreatySrv.nopages.query();
    vm.reportingCategory = ['Financial', 'Investment', 'Policy & Experience', 'FinRe Ops', 'Other'];
    vm.preparer = ['Captive', 'Ceding Co.', 'LLC', 'RGA', 'Other '];
    vm.DayTypeOptions = ['Calendar Days After', 'Calendar Days Before ', 'Business Days After', 'Business Days Before', 'On'];
    vm.MonthlyDayTypeOptions = ['Month End', 'Quarter End'];
    vm.QuarterDayTypeOptions = ['Quarter End'];
    vm.SemiAnnualDayTypeOptions = ['Half Year'];
    vm.AnnualDayTypeOptions = ['Year End'];
    vm.structuredFinance.yearEndException = 'No';
    vm.structuredFinance.isActive=1;
    vm.dealEndMonth = '';
    vm.requirements = false;
    vm.reporting = false;
    vm.deal = [];
    vm.dealId = $stateParams.dealId;
    vm.periodOptions=[];

    if(vm.structuredFinance.id != null) {
      StructuredTrackingCustomSrv.structuredTrackingByFinance.query(
        {structuredFinanceId: vm.structuredFinance.id}, function (trackingResults) {
          vm.structuredFinance.tracking = trackingResults.length;
        })
    }else { vm.structuredFinance.tracking=[];
    }



    vm.yearEndMonthOptions = YEAR_END_MONTH_OPTIONS;

    // We need the timeout for the first load deal. note by Lucia Del Pino.
    $timeout(function () {




      Frequency.query(function (result) {
        angular.forEach(result, function (Frequency) {
          if (Frequency.name !== 'Initial' ) {
            vm.periodOptions.push(Frequency);
          }});
        vm.changeReportingFrequency();
      });



      if ($stateParams.dealId !== undefined && $stateParams.dealId !== null) {
        Deal.get({id: $stateParams.dealId}, function (result) {
          vm.structuredFinance.deal = result;
          if (result) {
            angular.forEach(vm.yearEndMonthOptions, function (Month) {
              if (Month.id === result.yearEndMonth) {
                vm.dealEndMonth = Month;
              }
            });
          }
        });
      }
    }, 0);


    vm.changeReportingFrequency = function () {
      if (vm.structuredFinance.reportingFrequency !== null && vm.structuredFinance.reportingFrequency !== undefined) {
        if (vm.structuredFinance.dayType === '') {
          vm.structuredFinance.dayType = vm.DayTypeOptions[0];
        }
        switch (vm.structuredFinance.reportingFrequency.name){
          case 'Monthly':
            vm.structuredFinance.dayType = vm.DayTypeOptions[0];
            vm.DayTypeOptionsRep = vm.MonthlyDayTypeOptions;
            break;
          case 'Quarterly':
            vm.structuredFinance.period = vm.QuarterDayTypeOptions[0];
            vm.DayTypeOptionsRep = vm.QuarterDayTypeOptions;
            vm.structuredFinance.dayType = vm.DayTypeOptions[0];
            break;
          case 'Semi-Annual':
            vm.structuredFinance.period = vm.SemiAnnualDayTypeOptions[0];
            vm.DayTypeOptionsRep = vm.SemiAnnualDayTypeOptions;
            vm.structuredFinance.dayType = vm.DayTypeOptions[0];
            break;
          case 'Annual':
            vm.structuredFinance.yearEndException = 'No';
            vm.changeYearEx();
            vm.structuredFinance.period = vm.AnnualDayTypeOptions[0];
            vm.DayTypeOptionsRep = vm.AnnualDayTypeOptions;
            vm.structuredFinance.dayType = vm.DayTypeOptions[0];
            if (vm.structuredFinance.dayType === '' || vm.structuredFinance.dayType === 'Year End') {
              vm.structuredFinance.numberOfDays = 0;
            }
            break;
          case 'Occurrence':
            vm.structuredFinance.period = null;
            vm.DayTypeOptionsRep = null;
            vm.structuredFinance.dayType = null;
            vm.structuredFinance.numberOfDays = null;
            break;
          default:
        }
      }
    };


    var onSaveSuccess = function (result) {
      $scope.$emit('finreApp:structuredFinanceUpdate', result);
      $uibModalInstance.close(result);
      vm.isSaving = false;
    };

    var onSaveError = function () {
      vm.isSaving = false;
    };

    Principal.identity().then(function (account) {
      vm.lastUser=account.login;
      vm.structuredFinance.lastUpdate= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
    });

    vm.structuredFinance.lastUser=vm.lastUser;

    vm.save = function () {
      vm.isSaving = true;
      if (vm.structuredFinance.id !== null && vm.structuredFinance.id !== undefined) {
        vm.structuredFinance.lastUser=vm.lastUser;
        vm.structuredFinance.lastUpdate= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        StructuredFinance.update(vm.structuredFinance, onSaveSuccess, onSaveError);
      } else {
        vm.structuredFinance.creationUser=vm.lastUser;
        vm.structuredFinance.creationTs= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        vm.structuredFinance.lastUser=vm.lastUser;
        vm.structuredFinance.lastUpdate= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        StructuredFinance.save(vm.structuredFinance, onSaveSuccess, onSaveError);
      }
    };

    vm.clear = function (result) {
//            $uibModalInstance.dismiss('cancel');
      $uibModalInstance.close(result);

    };

    vm.datePickerOpenStatus = {};
    vm.datePickerOpenStatus.RptReqEffDate = false;
    vm.datePickerOpenStatus.feePaymentDate = false;
    vm.datePickerOpenStatus.lastUpdate = false;
    vm.datePickerOpenStatus.creationTS = false;
    vm.datePickerOpenStatus.clnNotificationDate = false;

    vm.openCalendar = function (date) {
      vm.datePickerOpenStatus[date] = true;
    };

    vm.dealChange = function () {
      angular.forEach(vm.yearEndMonthOptions, function (Month) {
        if (vm.structuredFinance.deal !== undefined) {
          if (Month.id === vm.structuredFinance.deal.yearEndMonth) {
            vm.dealEndMonth = Month;
          }
        }
      });

    };
    vm.numberOfDay = function (param) {
      var confirm = $mdDialog.alert()
      //  .title('Are you ready to update Input Status on Treaty Basic Info from In Process to Complete?')
        .textContent('You cannot enter a negative number here.')
        .ok('Ok')
        .parent(angular.element(document.querySelector('#myModal')));

      if (param === 1) {
        if (vm.structuredFinance.numberOfDaysYr === 0) {
          vm.structuredFinance.dayTypeYr = 'On';
        } else {
          if (vm.structuredFinance.numberOfDaysYr < 0) {
            $mdDialog.show(confirm).then(function () {
              vm.structuredFinance.numberOfDaysYr = '';
              vm.structuredFinance.dayTypeYr = '';
            });
          }
        }
      } else {
        if (vm.structuredFinance.numberOfDays === 0) {
          vm.structuredFinance.dayType = 'On';
        } else {
          if (vm.structuredFinance.numberOfDays < 0) {
            $mdDialog.show(confirm).then(function () {
              vm.structuredFinance.numberOfDays = '';
              vm.structuredFinance.dayType = '';
            });
          }
        }
      }
    };
    vm.changeYearEx = function () {
      if (vm.structuredFinance.yearEndException === 'No') {
        vm.structuredFinance.numberOfDaysYr = '';
        vm.structuredFinance.dayTypeYr = '';
        vm.structuredFinance.periodYr = '';
        vm.structuredFinance.reportingDueCommentYr = '';
      }
      else {
        vm.structuredFinance.dayTypeYr = vm.structuredFinance.dayType;
        vm.structuredFinance.periodYr = vm.structuredFinance.period;
      }
    };

  }
})();
