(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('StructuredFinanceController', StructuredFinanceController);

    StructuredFinanceController.$inject = ['$scope', '$state','$stateParams', 'StructuredTrackingCustomSrv', 'ParseLinks',
      'AlertService', 'pagingParams', 'paginationConstants','Deal','StructuredFinanceCustomSrv','StructuredFinanceSearch'];

    function StructuredFinanceController ($scope, $state,$stateParams, StructuredTrackingCustomSrv, ParseLinks,
                                          AlertService, pagingParams, paginationConstants,Deal, StructuredFinanceCustomSrv,StructuredFinanceSearch) {
      var vm = this;
      vm.loadPage = loadPage;
      vm.predicate = pagingParams.predicate;
      vm.reverse = pagingParams.ascending;
      vm.transition = transition;
      vm.clear = clear;
      vm.search = search;
      vm.searchQuery = pagingParams.search;
      vm.currentSearch = pagingParams.search;
      vm.dealId = $stateParams.dealId;
      vm.itemsPerPage = paginationConstants.itemsPerPage;

      Deal.get({id: $stateParams.dealId}, function (result) {
        vm.deal = result;
      });

      loadAll();

      function loadAll () {
        if (pagingParams.search) {
          StructuredFinanceSearch.query({
              dealId: $stateParams.dealId,
                page: pagingParams.page - 1,
                size: paginationConstants.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);
        } else {
          StructuredFinanceCustomSrv.query({
                dealId: $stateParams.dealId,
                page: pagingParams.page - 1,
                size: paginationConstants.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);
        }
        function sort() {
            var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
            if (vm.predicate !== 'id') {
                result.push('id');
            }
            return result;
        }
        function onSuccess(data, headers) {
          vm.links = ParseLinks.parse(headers('link'));
          vm.queryCount = vm.totalItems = headers('X-Total-Count');
          vm.structuredFinances = data;
          vm.page = pagingParams.page;
          angular.forEach (data, function(structuredFinance,key) {
            structuredFinance.trackings= StructuredTrackingCustomSrv.structuredTrackingByFinance.query(
              {structuredFinanceId: structuredFinance.id}, function(trackingResults){
                structuredFinance.trackings=[];

                angular.forEach(trackingResults, function(tracking,key){
                  if(tracking.trackingStatus==='Open'){
                  tracking.quarter = calculateQuarters(tracking.reportingMonthYear);
                  structuredFinance.trackings.push(tracking);
                  }
                });
              }, onError);
          });
        }

        function onError(error) {
            AlertService.error(error.data.message);
        }
      }

      function loadPage (page) {
        vm.page = page;
        vm.transition();
      }

      function transition () {
        $state.transitionTo($state.$current, {
            page: vm.page,
            sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
            search: vm.currentSearch,
            dealId: vm.dealId
        });
      }

      function search (searchQuery) {
          if (!searchQuery){
              return vm.clear();
          }
          vm.links = null;
          vm.page = 1;
          vm.predicate = '_score';
          vm.reverse = false;
          vm.currentSearch = searchQuery;
          vm.transition();
      }
      function clear () {
          vm.links = null;
          vm.page = 1;
          vm.predicate = 'id';
          vm.reverse = true;
          vm.currentSearch = null;
          vm.transition();
      }
    }
})();
