(function() {
    'use strict';

    angular
        .module('finreApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('structured-finance', {
            parent: 'entity',
            url: '/structured-finance/{dealId}?page&sort&search',
            data: {
                authorities: ['ROLE_APP_STRUCTURED_FINANCE_ADMIN','ROLE_APP_STRUCTURED_FINANCE_VIEW_ONLY',],
                pageTitle: 'finreApp.structuredFinance.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/structured-finance/structured-finance.html',
                    controller: 'StructuredFinanceController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                  $translatePartialLoader.addPart('global');
                  $translatePartialLoader.addPart('structuredFinance');
                    return $translate.refresh();
                }]
            }
        })
        .state('structured-finance.detail', {
            parent: 'entity',
            url: '/structured-finance/detail/{dealId}/{id}',
            data: {
                authorities: ['ROLE_APP_STRUCTURED_FINANCE_ADMIN','ROLE_APP_STRUCTURED_FINANCE_VIEW_ONLY'],
                pageTitle: 'finreApp.structuredFinance.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/structured-finance/structured-finance-detail.html',
                    controller: 'StructuredFinanceDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('structuredFinance');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'StructuredFinance', function($stateParams, StructuredFinance) {
                    return StructuredFinance.get({id : $stateParams.id,dealId : $stateParams.dealId}).$promise;
                }]
            }
        })
        .state('structured-finance.new', {
            parent: 'structured-finance',
            url: '/deal/{dealId}/new',
            data: {
              authorities: ['ROLE_APP_STRUCTURED_FINANCE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/structured-finance/structured-finance-dialog.html',
                    controller: 'StructuredFinanceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                              reportingCategory: null,
                              reportingReq: null,
                                id:null

                            };
                        }
                    }
                }).result.then(function() {
                  $state.go('structured-finance.deal',  {dealId: $stateParams.dealId}, { reload: true });
                }, function() {
                 $state.go('structured-finance.deal',  {dealId: $stateParams.dealId}, { reload: true });
                });
            }]
        })
        .state('structured-finance.edit', {
            parent: 'structured-finance',
            url: '/{id}/edit/{dealId}',
            data: {
              authorities: ['ROLE_APP_STRUCTURED_FINANCE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/structured-finance/structured-finance-dialog.html',
                    controller: 'StructuredFinanceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['StructuredFinance', function(StructuredFinance) {
                            return StructuredFinance.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                  $state.go('structured-finance.deal', {dealId:$stateParams.dealId}, { reload: true });
                }, function() {
                  $state.go('structured-finance.deal', {dealId:$stateParams.dealId}, { reload: true });
                });
            }]
        })
          .state('structured-finance.editDetail', {
            parent: 'structured-finance.detail',
            url: '/{id}/edit/detail/{dealId}',
            data: {
              authorities: ['ROLE_APP_STRUCTURED_FINANCE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
              $uibModal.open({
                templateUrl: 'app/entities/structured-finance/structured-finance-dialog.html',
                controller: 'StructuredFinanceDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: ['StructuredFinance', function(StructuredFinance) {
                    return StructuredFinance.get({id : $stateParams.id});
                  }]
                }
              }).result.then(function() {
                $state.go('structured-finance.detail', {id : $stateParams.id,dealId:$stateParams.dealId}, { reload: true });
              }, function() {
                $state.go('structured-finance.detail', {id : $stateParams.id,dealId:$stateParams.dealId}, { reload: true });
              });
            }]
          })
        .state('structured-finance.deal', {
            parent: 'entity',
            url: '/structured-finance/deal/{dealId}?page&sort&search',
            data: {
              authorities: ['ROLE_APP_STRUCTURED_FINANCE_ADMIN','ROLE_APP_STRUCTURED_FINANCE_VIEW_ONLY'],
              pageTitle: 'finreApp.structuredFinance.home.title'
            },
            views: {
              'content@': {
                templateUrl: 'app/entities/structured-finance/structured-finance.html',
                controller: 'StructuredFinanceController',
                controllerAs: 'vm'
              }
            },
            params: {
              page: {
                value: '1',
                squash: true
              },
              sort: {
                value: 'id,asc',
                squash: true
              },
              search: null
            },
            resolve: {
              pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                return {
                  page: PaginationUtil.parsePage($stateParams.page),
                  sort: $stateParams.sort,
                  predicate: PaginationUtil.parsePredicate($stateParams.sort),
                  ascending: PaginationUtil.parseAscending($stateParams.sort),
                  search: $stateParams.search
                };
              }],
              translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                $translatePartialLoader.addPart('structuredFinance');
                $translatePartialLoader.addPart('global');
                return $translate.refresh();
              }]
            }
          })
        .state('structured-finance.delete', {
            parent: 'structured-finance.deal',
            url: '/{id}/delete',
            data: {
              authorities: ['ROLE_APP_STRUCTURED_FINANCE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/structured-finance/structured-finance-delete-dialog.html',
                    controller: 'StructuredFinanceDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['StructuredFinance', function(StructuredFinance) {
                            return StructuredFinance.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                  $state.go('structured-finance.deal', {dealId:$stateParams.dealId}, { reload: true });
                }, function() {
                  $state.go('structured-finance.deal', {dealId:$stateParams.dealId}, { reload: true });
                });
            }]
        });
    }

})();
