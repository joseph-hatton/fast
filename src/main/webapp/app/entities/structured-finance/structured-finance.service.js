(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('StructuredFinance', StructuredFinance);

    StructuredFinance.$inject = ['$resource', 'DateUtils'];

    function StructuredFinance ($resource, DateUtils) {
        var resourceUrl =  'api/structured-finances/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.rptReqEffDate = DateUtils.convertLocalDateFromServer(data.rptReqEffDate);
               //     data.feePaymentDate = DateUtils.convertLocalDateFromServer(data.feePaymentDate);
                //    data.lastUpdate = DateUtils.convertLocalDateFromServer(data.lastUpdate);
                //    data.creationTS = DateUtils.convertLocalDateFromServer(data.creationTS);
                //    data.clnNotificationDate = DateUtils.convertLocalDateFromServer(data.clnNotificationDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                data.rptReqEffDate = DateUtils.convertLocalDateToServer(data.rptReqEffDate);
               //     data.feePaymentDate = DateUtils.convertLocalDateToServer(data.feePaymentDate);
                //    data.lastUpdate = DateUtils.convertLocalDateToServer(data.lastUpdate);
               //     data.creationTS = DateUtils.convertLocalDateToServer(data.creationTS);
                //    data.clnNotificationDate = DateUtils.convertLocalDateToServer(data.clnNotificationDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.rptReqEffDate = DateUtils.convertLocalDateToServer(data.rptReqEffDate);
                 //   data.feePaymentDate = DateUtils.convertLocalDateToServer(data.feePaymentDate);
                  //  data.lastUpdate = DateUtils.convertLocalDateToServer(data.lastUpdate);
                 //   data.creationTS = DateUtils.convertLocalDateToServer(data.creationTS);
                //    data.clnNotificationDate = DateUtils.convertLocalDateToServer(data.clnNotificationDate);
                    return angular.toJson(data);
                }
            }
        });
    }
})();
