(function() {
    'use strict';

    angular
        .module('finreApp')
        .factory('StructuredFinanceSearch', StructuredFinanceSearch);

    StructuredFinanceSearch.$inject = ['$resource'];

    function StructuredFinanceSearch($resource) {
        var resourceUrl =  'api/_search/structured-finances/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
