(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('GfsAssigneeDeleteController',GfsAssigneeDeleteController);

    GfsAssigneeDeleteController.$inject = ['$uibModalInstance', 'entity', 'GfsAssignee'];

    function GfsAssigneeDeleteController($uibModalInstance, entity, GfsAssignee) {
        var vm = this;
        vm.gfsAssignee = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            GfsAssignee.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
