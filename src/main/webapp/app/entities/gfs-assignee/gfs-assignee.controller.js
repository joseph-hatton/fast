(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('GfsAssigneeController', GfsAssigneeController);

    GfsAssigneeController.$inject = ['$scope', '$state', 'GfsAssignee', 'GfsAssigneeSearch'];

    function GfsAssigneeController ($scope, $state, GfsAssignee, GfsAssigneeSearch) {
        var vm = this;
        vm.gfsAssignees = [];
        vm.loadAll = function() {
            GfsAssignee.query(function(result) {
                vm.gfsAssignees = result;
            });
        };

        vm.search = function () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            GfsAssigneeSearch.query({query: vm.searchQuery}, function(result) {
                vm.gfsAssignees = result;
            });
        };
        vm.loadAll();
        
    }
})();
