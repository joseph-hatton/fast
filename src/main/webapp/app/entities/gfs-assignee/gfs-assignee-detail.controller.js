(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('GfsAssigneeDetailController', GfsAssigneeDetailController);

    GfsAssigneeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'GfsAssignee', 'DetailService'];

  function GfsAssigneeDetailController($scope, $rootScope, $stateParams, entity, GfsAssignee, DetailService) {
    this.prototype = Object.create(DetailService.prototype);

    this.gfsAssignee = entity;

    DetailService.call(this, $scope, $rootScope, $stateParams, GfsAssignee, this.gfsAssignee, "GfsAssignee");
  }
})();
