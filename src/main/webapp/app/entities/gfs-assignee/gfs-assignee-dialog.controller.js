(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('GfsAssigneeDialogController', GfsAssigneeDialogController);

    GfsAssigneeDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'GfsAssignee', 'DialogService'];

    function GfsAssigneeDialogController ($scope, $stateParams, $uibModalInstance, entity, GfsAssignee, DialogService) {
      this.prototype = Object.create(DialogService.prototype);

      var datePickerItemsRun = function (vm) {
        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.lastUpdate = false;
        vm.datePickerOpenStatus.creationTS = false;
        vm.datePickerOpenStatus.effBeginTs = false;
        vm.datePickerOpenStatus.effEndTs = false;
      };

      this.gfsAssignee = entity;

      DialogService.call(this, $scope, $stateParams, $uibModalInstance, entity, GfsAssignee, this.gfsAssignee, "GfsAssignee", datePickerItemsRun);
    }
})();
