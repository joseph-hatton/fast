(function() {
    'use strict';

    angular
        .module('finreApp')
        .factory('GfsAssigneeSearch', GfsAssigneeSearch);

    GfsAssigneeSearch.$inject = ['$resource'];

    function GfsAssigneeSearch($resource) {
        var resourceUrl =  'api/_search/gfs-assignees/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
