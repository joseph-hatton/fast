(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('GfsAssignee', GfsAssignee);

    GfsAssignee.$inject = ['$resource', 'DateUtils'];

    function GfsAssignee ($resource, DateUtils) {
        var resourceUrl =  'api/gfs-assignees/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.lastUpdate = DateUtils.convertLocalDateFromServer(data.lastUpdate);
                    data.creationTS = DateUtils.convertLocalDateFromServer(data.creationTS);
                    data.effBeginTs = DateUtils.convertLocalDateFromServer(data.effBeginTs);
                    data.effEndTs = DateUtils.convertLocalDateFromServer(data.effEndTs);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.lastUpdate = DateUtils.convertLocalDateToServer(data.lastUpdate);
                    data.creationTS = DateUtils.convertLocalDateToServer(data.creationTS);
                    data.effBeginTs = DateUtils.convertLocalDateToServer(data.effBeginTs);
                    data.effEndTs = DateUtils.convertLocalDateToServer(data.effEndTs);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.lastUpdate = DateUtils.convertLocalDateToServer(data.lastUpdate);
                    data.creationTS = DateUtils.convertLocalDateToServer(data.creationTS);
                    data.effBeginTs = DateUtils.convertLocalDateToServer(data.effBeginTs);
                    data.effEndTs = DateUtils.convertLocalDateToServer(data.effEndTs);
                    return angular.toJson(data);
                }
            }
        });
    }
})();
