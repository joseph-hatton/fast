(function() {
    'use strict';

    angular
        .module('finreApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('gfs-assignee', {
            parent: 'entity',
            url: '/gfs-assignee',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
                pageTitle: 'finreApp.gfsAssignee.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/gfs-assignee/gfs-assignees.html',
                    controller: 'GfsAssigneeController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('gfsAssignee');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('gfs-assignee-detail', {
            parent: 'entity',
            url: '/gfs-assignee/{id}',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
                pageTitle: 'finreApp.gfsAssignee.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/gfs-assignee/gfs-assignee-detail.html',
                    controller: 'GfsAssigneeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('gfsAssignee');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'GfsAssignee', function($stateParams, GfsAssignee) {
                    return GfsAssignee.get({id : $stateParams.id});
                }]
            }
        })
        .state('gfs-assignee.new', {
            parent: 'gfs-assignee',
            url: '/new',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/gfs-assignee/gfs-assignee-dialog.html',
                    controller: 'GfsAssigneeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                description: null,
                                sortOrder: null,
                                default_flag: null,
                                lastUser: null,
                                lastUpdate: null,
                                creationTS: null,
                                creationUser: null,
                                effBeginTs: null,
                                effEndTs: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('gfs-assignee', null, { reload: true });
                }, function() {
                    $state.go('gfs-assignee');
                });
            }]
        })
        .state('gfs-assignee.edit', {
            parent: 'gfs-assignee',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/gfs-assignee/gfs-assignee-dialog.html',
                    controller: 'GfsAssigneeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['GfsAssignee', function(GfsAssignee) {
                            return GfsAssignee.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('gfs-assignee', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('gfs-assignee.delete', {
            parent: 'gfs-assignee',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/gfs-assignee/gfs-assignee-delete-dialog.html',
                    controller: 'GfsAssigneeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['GfsAssignee', function(GfsAssignee) {
                            return GfsAssignee.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('gfs-assignee', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
