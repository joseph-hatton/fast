(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('EnterpriseKey', EnterpriseKey);

    EnterpriseKey.$inject = ['$resource'];

    function EnterpriseKey ($resource) {
        var resourceUrl =  'api/enterprise-keys/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                  if(data === "" || data === null || data === undefined) return null;
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
