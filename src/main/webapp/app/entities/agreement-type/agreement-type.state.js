(function () {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('agreement-type', {
        parent: 'entity',
        url: '/agreement-type',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.agreementType.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/agreement-type/agreement-types.html',
            controller: 'AgreementTypeController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('agreementType');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }]
        }
      })
      .state('agreement-type-detail', {
        parent: 'entity',
        url: '/agreement-type/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.agreementType.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/agreement-type/agreement-type-detail.html',
            controller: 'AgreementTypeDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('agreementType');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'AgreementType', function ($stateParams, AgreementType) {
            return AgreementType.get({id: $stateParams.id});
          }]
        }
      })
      .state('agreement-type.new', {
        parent: 'agreement-type',
        url: '/new',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/agreement-type/agreement-type-dialog.html',
            controller: 'AgreementTypeDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  name: null,
                  description: null,
                  id: null
                };
              }
            }
          }).result.then(function () {
            $state.go('agreement-type', null, {reload: true});
          }, function () {
            $state.go('agreement-type');
          });
        }]
      })
      .state('agreement-type.edit', {
        parent: 'agreement-type',
        url: '/{id}/edit',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/agreement-type/agreement-type-dialog.html',
            controller: 'AgreementTypeDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['AgreementType', function (AgreementType) {
                return AgreementType.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('agreement-type', null, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      })
      .state('agreement-type.delete', {
        parent: 'agreement-type',
        url: '/{id}/delete',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/agreement-type/agreement-type-delete-dialog.html',
            controller: 'AgreementTypeDeleteController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['AgreementType', function (AgreementType) {
                return AgreementType.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('agreement-type', null, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      });
  }

})();
