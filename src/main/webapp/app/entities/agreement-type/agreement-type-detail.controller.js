(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('AgreementTypeDetailController', AgreementTypeDetailController);

  AgreementTypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'AgreementType', 'DetailService'];

  function AgreementTypeDetailController($scope, $rootScope, $stateParams, entity, AgreementType, DetailService) {
    this.prototype = Object.create(DetailService.prototype);

    this.agreementType = entity;

    DetailService.call(this, $scope, $rootScope, $stateParams, AgreementType, this.agreementType, "AgreementType");
  }
})();
