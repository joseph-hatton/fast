(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .factory('AgreementTypeSearch', AgreementTypeSearch);
  
  AgreementTypeSearch.$inject = ['$resource'];
  
  function AgreementTypeSearch($resource) {
    var resourceUrl = 'api/_search/agreement-types/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true}
    });
  }
})();
