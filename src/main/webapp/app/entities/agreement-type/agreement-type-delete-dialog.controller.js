(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .controller('AgreementTypeDeleteController', AgreementTypeDeleteController);
  
  AgreementTypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'AgreementType'];
  function AgreementTypeDeleteController($uibModalInstance, entity, AgreementType) {
    var vm = this;
    vm.agreementType = entity;
    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };
    vm.confirmDelete = function (id) {
      AgreementType.delete({id: id},
        function () {
          $uibModalInstance.close(true);
        });
    };
  }
})();
