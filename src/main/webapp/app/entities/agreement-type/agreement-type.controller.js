(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .controller('AgreementTypeController', AgreementTypeController);
  
  AgreementTypeController.$inject = ['$scope', '$state', 'AgreementType', 'AgreementTypeSearch'];
  
  function AgreementTypeController($scope, $state, AgreementType, AgreementTypeSearch) {
    var vm = this;
    vm.agreementTypes = [];
    vm.loadAll = function () {
      AgreementType.query(function (result) {
        vm.agreementTypes = result;
      });
    };
    
    vm.search = function () {
      if (!vm.searchQuery) {
        return vm.loadAll();
      }
      AgreementTypeSearch.query({query: vm.searchQuery}, function (result) {
        vm.agreementTypes = result;
      });
    };
    vm.loadAll();
    
  }
})();
