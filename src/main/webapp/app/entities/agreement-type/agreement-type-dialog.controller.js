(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('AgreementTypeDialogController', AgreementTypeDialogController);

  AgreementTypeDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'AgreementType', 'DialogService'];

  function AgreementTypeDialogController ($scope, $stateParams, $uibModalInstance, entity, AgreementType, DialogService) {
    this.prototype = Object.create(DialogService.prototype);

    var datePickerItemsRun = function (vm) {};

    this.agreementType = entity;

    DialogService.call(this, $scope, $stateParams, $uibModalInstance, entity, AgreementType, this.agreementType, "AgreementType", datePickerItemsRun);
  }
})();
