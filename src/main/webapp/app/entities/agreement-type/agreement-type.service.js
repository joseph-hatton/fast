(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('AgreementType', AgreementType);
  
  AgreementType.$inject = ['$resource'];
  
  function AgreementType($resource) {
    var resourceUrl = 'api/agreement-types/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          return data;
        }
      },
      'update': {method: 'PUT'}
    });
  }
})();
