(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('CurrencyCustomSrv', CurrencyCustomSrv);

    CurrencyCustomSrv.$inject = ['$resource', 'DateUtils'];

  function CurrencyCustomSrv($resource, DateUtils) {
    return {
      rate: $resource('api/currencies/rate/:code/:year', {}, {
        'get': {
          method: 'GET',
          transformResponse: function (data) {
            data = angular.fromJson(data);
            return data;
          }
        }
      })
    };
  }
})();
