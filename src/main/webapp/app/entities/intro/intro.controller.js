(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('IntroController', IntroController);

    IntroController.$inject = ['$rootScope', '$scope', '$state', 'Intro', '$q', '$timeout', '$filter', 'InteractionsService', 'Principal'];

    function IntroController ($rootScope, $scope, $state, Intro, $q, $timeout, $filter, InteractionsService, Principal) {
        var vm = this;
        vm.intros = [];
        vm.loadAll = function() {
            return Intro.load.all(function(result) {
                vm.intros = $filter('orderBy')(result, 'order');
            }).$promise;
        };
        vm.loadAll()
        .then(setup);

        vm.save = function(intro){
            var action = (intro.id == null) ? 'save' : 'update';
            return Intro.modify[action](intro).$promise;
        };

        vm.saveAll = function(){
            var introsSavePromises = vm.intros.map(function(intro){
                return vm.save(intro);
            });
            
            return $q.all(introsSavePromises)
            .then(onSuccess)
            .catch(onError);
        };

        vm.moveUp = function(intro, index){
            if(index > 0 && intro.steps[index].inUse === intro.steps[index - 1].inUse){
                var index1 = index;
                var index2 = index - 1;
                var item1 = intro.steps[index1];
                var item2 = intro.steps[index2];

                swap(intro, index1, index2);

                item1.order--;
                item2.order++;

                vm.save(intro)
                .then(function(result){
                    intro.steps = result.steps;
                });
            }
        };

        vm.moveDown = function(intro, index){    
            if(index < intro.steps.length - 1 && intro.steps[index].inUse === intro.steps[index + 1].inUse){                
                var index1 = index;
                var index2 = index + 1;
                var item1 = intro.steps[index1];
                var item2 = intro.steps[index2];

                swap(intro, index1, index2);

                item1.order++;
                item2.order--;

                vm.save(intro)
                .then(function(result){
                    intro.steps = result.steps;
                });
            }
        };

        vm.addStep = function(intro){
            intro.steps.push({
                "content": "default",
                "id": null,
                "inUse": 0,
                "order": intro.steps.length + 1,
                "isActive": 1
            });
            vm.save(intro).then(function(result){
                intro.steps = result.steps;
            });
        };

        vm.editStep = function(intro, index){
            intro.steps[index].contentCache = intro.steps[index].content;
            intro.steps[index].editing = true;
        };

        vm.doneEditingStep = function(intro, index){
            intro.steps[index].editing = false;
        };

        vm.cancelEditingStep = function (intro, index) {
            intro.steps[index].content = intro.steps[index].contentCache;
            vm.save(intro);
            vm.doneEditingStep(intro, index);
        };

        vm.editIntro = function(intro){
            intro.descriptionCache = intro.description;
            intro.titleCache = intro.title;
            intro.editing = true;
        };

        vm.doneEditingIntro = function(intro){
            intro.editing = false;
        };

        vm.cancelEditingIntro = function (intro) {
            intro.description = intro.descriptionCache;
            intro.title = intro.titleCache;
            vm.doneEditingIntro(intro);
        };
        
        vm.removeStep = function(intro, index){
            if(!intro.steps[index].inUse){
                var step = intro.steps[index];
                step.isActive = 0;
                intro.steps.splice(index, 1);
                intro.steps = intro.steps.map(function(item, index){
                    item.order = index + 1;
                    return item;
                });
                intro.steps.push(step);
                vm.save(intro).then(function(result){
                    intro.steps = result.steps;
                });
            }
        };

        vm.removeIntro = function(intro){
            if(vm.activeCount(intro) === 0){
                var dialog = InteractionsService.confirmBasicParentNoScrollDialog("Are you sure you want to delete " + intro.title + "?", "#myModal");
                InteractionsService.showThenDialog(dialog, function(){
                    for(var i = intro.order + 1; i < vm.intros.length; i++){
                        vm.intros[i].order--;
                    }
                    intro.isActive = 0;
                    vm.saveAll();
                });
            }
        };

        vm.createNewIntro = function(){
            vm.adding = true;
            vm.intros.forEach(function(item) {
                item.order++;
            });
            vm.intros.unshift({
                "title": "Default",
                "id": null,
                "description": "",
                "steps": [],
                "isActive": 1,
                "order": 0
            });
            vm.saveAll()
            .then(function(){
                vm.adding = false;
            });
        };

        vm.toggleStepsShowing = function (intro){
            if(intro.expanded){
                intro.expanded = false;
            } else {
                intro.expanded = true;
            }
        };

        vm.inUseCount = function(intro){
            return intro.steps.reduce(function(prev, current){
                if(current.inUse && current.isActive)
                    prev++;
                return prev;
            }, 0);
        };

        vm.NotInUseCount = function(intro){
            return intro.steps.reduce(function(prev, current){
                if(!current.inUse && current.isActive)
                    prev++;
                return prev;
            }, 0);
        };

        vm.activeCount = function(intro){
            return intro.steps.reduce(function(prev, current){
                if(current.isActive)
                    prev++;
                return prev;
            }, 0);
        };

        function setup(){
            Principal.isSupport()
            .then(function(result){
                vm.isSupport = result;
            });
        }

        function swap(intro, item1, item2){
            var temp = intro.steps[item1];
            intro.steps[item1] = intro.steps[item2];
            intro.steps[item2] = temp;
        }

        function onSuccess(result){
            vm.intros = $filter('filter')(result, function(item){
                return item.isActive;
            });
            vm.alert = "Success";
            vm.statusMessage = "Saving was successful!";
            fadeOutStatusMessage();
        }

        function onError(result){
            vm.alert = "Error";
            vm.statusMessage = "There was a problem while saving!";
            fadeOutStatusMessage();
        }

        function fadeOutStatusMessage (){
            $timeout(function(){
                vm.alert = "";
                vm.statusMessage = "";
            }, 3000);
        }
    }
})();
