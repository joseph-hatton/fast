(function() {
    'use strict';

    angular
        .module('finreApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('intro', {
            parent: 'entity',
            url: '/intro',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN']
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/intro/intro.html',
                    controller: 'IntroController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        });
        
    }

})();
