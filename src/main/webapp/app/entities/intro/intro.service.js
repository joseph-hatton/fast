(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('Intro', Intro);

    Intro.$inject = ['$resource', 'DateUtils', '$q', '$filter'];

    function Intro ($resource, DateUtils, $q, $filter) {
        var url = "api/intro";
        var item = {
            "modify": $resource(url, {}, {
                'update': {
                    isArray: false,
                    method: 'PUT',
                    transformRequest: function (data) {
                        return angular.toJson(data);
                    },
                    transformResponse: function (data) {
                        data = angular.fromJson(data);
                        item.steps = $filter('orderBy')(item.steps, "order");    
                        return data;
                    }
                },
                'save': {
                    isArray: false,
                    method: 'POST',
                    transformRequest: function (data) {
                        return angular.toJson(data);
                    }
                }
            }),
            "load": $resource(url + "/:id", { }, {
                'all': {
                    isArray: true,
                    method: 'GET',
                    transformResponse: function (data) {
                        data = angular.fromJson(data);
                        data.forEach(function(item) {
                            item.steps = $filter('orderBy')(item.steps, "order");    
                        });
                        return data;
                    },
                    transformRequest: function (data) {
                        return angular.toJson(data);
                    }
                },
                'byId': {
                    method: 'GET',
                    transformResponse: function (data) {
                        data = angular.fromJson(data);
                        data.steps = $filter('orderBy')(data.steps, "order");    
                        return data;
                    },
                    transformRequest: function (data) {
                        return angular.toJson(data);
                    }
                }
            })
        };

        item.setupIntroSteps = function(input){
            var steps = [];
            return item.load.byId({ id:input.id }).$promise
            .then(function(page){
                page.steps.forEach(function(step){
                    if(step.inUse)
                        steps.push({
                            element:  "." + (input.className ? input.className : "step")  + step.order,
                            intro: step.content
                        });
                });
                return $q.when(steps);
            });
        };

        return item;
    }
})();
