(function() {
    'use strict';

    angular
        .module('finreApp')
        .factory('MdmAttributesSearch', MdmAttributesSearch);

    MdmAttributesSearch.$inject = ['$resource'];

    function MdmAttributesSearch($resource) {
        var resourceUrl =  'api/_search/mdm-attributes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
