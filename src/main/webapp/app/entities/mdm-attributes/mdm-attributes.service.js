(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('MdmAttributes', MdmAttributes);

    MdmAttributes.$inject = ['$resource'];

    function MdmAttributes ($resource) {
        var resourceUrl =  'api/mdm-attributes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
