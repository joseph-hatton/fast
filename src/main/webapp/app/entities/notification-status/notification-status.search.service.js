(function() {
    'use strict';

    angular
        .module('finreApp')
        .factory('NotificationStatusSearch', NotificationStatusSearch);

    NotificationStatusSearch.$inject = ['$resource'];

    function NotificationStatusSearch($resource) {
        var resourceUrl =  'api/_search/notification-statuses/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
