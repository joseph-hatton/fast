(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('NotificationStatusDetailController', NotificationStatusDetailController);

    NotificationStatusDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'NotificationStatus', 'DetailService'];

  function NotificationStatusDetailController($scope, $rootScope, $stateParams, entity, NotificationStatus, DetailService) {
    this.prototype = Object.create(DetailService.prototype);

    this.notificationStatus = entity;

    DetailService.call(this, $scope, $rootScope, $stateParams, NotificationStatus, this.notificationStatus, "NotificationStatus");
  }
})();
