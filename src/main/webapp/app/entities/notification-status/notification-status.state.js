(function() {
    'use strict';

    angular
        .module('finreApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('notification-status', {
            parent: 'entity',
            url: '/notification-status',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
                pageTitle: 'finreApp.notificationStatus.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/notification-status/notification-statuses.html',
                    controller: 'NotificationStatusController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('notificationStatus');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('notification-status-detail', {
            parent: 'entity',
            url: '/notification-status/{id}',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
                pageTitle: 'finreApp.notificationStatus.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/notification-status/notification-status-detail.html',
                    controller: 'NotificationStatusDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('notificationStatus');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'NotificationStatus', function($stateParams, NotificationStatus) {
                    return NotificationStatus.get({id : $stateParams.id});
                }]
            }
        })
        .state('notification-status.new', {
            parent: 'notification-status',
            url: '/new',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/notification-status/notification-status-dialog.html',
                    controller: 'NotificationStatusDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                description: null,
                                sortOrder: null,
                                default_flag: null,
                                lastUser: null,
                                lastUpdate: null,
                                creationTS: null,
                                creationUser: null,
                                effBeginTs: null,
                                effEndTs: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('notification-status', null, { reload: true });
                }, function() {
                    $state.go('notification-status');
                });
            }]
        })
        .state('notification-status.edit', {
            parent: 'notification-status',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/notification-status/notification-status-dialog.html',
                    controller: 'NotificationStatusDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NotificationStatus', function(NotificationStatus) {
                            return NotificationStatus.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('notification-status', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('notification-status.delete', {
            parent: 'notification-status',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/notification-status/notification-status-delete-dialog.html',
                    controller: 'NotificationStatusDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NotificationStatus', function(NotificationStatus) {
                            return NotificationStatus.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('notification-status', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
