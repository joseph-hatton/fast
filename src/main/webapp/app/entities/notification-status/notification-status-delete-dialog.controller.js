(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('NotificationStatusDeleteController',NotificationStatusDeleteController);

    NotificationStatusDeleteController.$inject = ['$uibModalInstance', 'entity', 'NotificationStatus'];

    function NotificationStatusDeleteController($uibModalInstance, entity, NotificationStatus) {
        var vm = this;
        vm.notificationStatus = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            NotificationStatus.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
