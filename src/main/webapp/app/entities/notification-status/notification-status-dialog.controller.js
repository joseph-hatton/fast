(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('NotificationStatusDialogController', NotificationStatusDialogController);

    NotificationStatusDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'NotificationStatus', 'DialogService'];

    function NotificationStatusDialogController ($scope, $stateParams, $uibModalInstance, entity, NotificationStatus, DialogService) {
      this.prototype = Object.create(DialogService.prototype);

      var datePickerItemsRun = function (vm) {
        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.lastUpdate = false;
        vm.datePickerOpenStatus.creationTS = false;
        vm.datePickerOpenStatus.effBeginTs = false;
        vm.datePickerOpenStatus.effEndTs = false;
      };

      this.notificationStatus = entity;

      DialogService.call(this, $scope, $stateParams, $uibModalInstance, entity, NotificationStatus, this.notificationStatus, "NotificationStatus", datePickerItemsRun);
    }
})();
