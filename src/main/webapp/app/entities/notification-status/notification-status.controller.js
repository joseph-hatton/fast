(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('NotificationStatusController', NotificationStatusController);

    NotificationStatusController.$inject = ['$scope', '$state', 'NotificationStatus', 'NotificationStatusSearch'];

    function NotificationStatusController ($scope, $state, NotificationStatus, NotificationStatusSearch) {
        var vm = this;
        vm.notificationStatuses = [];
        vm.loadAll = function() {
            NotificationStatus.query(function(result) {
                vm.notificationStatuses = result;
            });
        };

        vm.search = function () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            NotificationStatusSearch.query({query: vm.searchQuery}, function(result) {
                vm.notificationStatuses = result;
            });
        };
        vm.loadAll();
        
    }
})();
