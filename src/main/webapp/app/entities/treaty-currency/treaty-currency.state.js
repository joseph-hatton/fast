(function() {
    'use strict';

    angular
        .module('finreApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('treaty-currency', {
            parent: 'entity',
            url: '/treaty-currency',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
                pageTitle: 'finreApp.treatyCurrency.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/treaty-currency/treaty-currencies.html',
                    controller: 'TreatyCurrencyController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('treatyCurrency');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('treaty-currency-detail', {
            parent: 'entity',
            url: '/treaty-currency/{id}',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
                pageTitle: 'finreApp.treatyCurrency.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/treaty-currency/treaty-currency-detail.html',
                    controller: 'TreatyCurrencyDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('treatyCurrency');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TreatyCurrency', function($stateParams, TreatyCurrency) {
                    return TreatyCurrency.get({id : $stateParams.id});
                }]
            }
        })
        .state('treaty-currency.new', {
            parent: 'treaty-currency',
            url: '/new',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/treaty-currency/treaty-currency-dialog.html',
                    controller: 'TreatyCurrencyDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                treatyId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('treaty-currency', null, { reload: true });
                }, function() {
                    $state.go('treaty-currency');
                });
            }]
        })
        .state('treaty-currency.edit', {
            parent: 'treaty-currency',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/treaty-currency/treaty-currency-dialog.html',
                    controller: 'TreatyCurrencyDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TreatyCurrency', function(TreatyCurrency) {
                            return TreatyCurrency.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('treaty-currency', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('treaty-currency.delete', {
            parent: 'treaty-currency',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/treaty-currency/treaty-currency-delete-dialog.html',
                    controller: 'TreatyCurrencyDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TreatyCurrency', function(TreatyCurrency) {
                            return TreatyCurrency.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('treaty-currency', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
