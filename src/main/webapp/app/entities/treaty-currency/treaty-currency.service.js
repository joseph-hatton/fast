(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('TreatyCurrency', TreatyCurrency);

    TreatyCurrency.$inject = ['$resource'];

    function TreatyCurrency ($resource) {
        var resourceUrl =  'api/treaty-currencies/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
