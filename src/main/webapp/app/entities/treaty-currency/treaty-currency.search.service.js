(function() {
    'use strict';

    angular
        .module('finreApp')
        .factory('TreatyCurrencySearch', TreatyCurrencySearch);

    TreatyCurrencySearch.$inject = ['$resource'];

    function TreatyCurrencySearch($resource) {
        var resourceUrl =  'api/_search/treaty-currencies/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
