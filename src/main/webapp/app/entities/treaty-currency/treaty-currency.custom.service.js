(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('TreatyCurrencyCustom', TreatyCurrencyCustom);

  TreatyCurrencyCustom.$inject = ['$resource', 'DateUtils'];
  var currencyCountUrl = 'api/treaty-currencies/currency-count/:currencyId';

  function TreatyCurrencyCustom($resource, DateUtils) {
    return {
      currencyCount: $resource(currencyCountUrl, {}, {
        'get': {
          method: 'GET',
          transformResponse: function (data) {
            //data = angular.fromJson(data);
            return data;
          }
        }
      })
    };
  }
})();
