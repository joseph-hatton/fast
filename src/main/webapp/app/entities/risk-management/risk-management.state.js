(function() {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];
    function stateConfig($stateProvider) {
        $stateProvider
        .state('risk-management', {
            parent: 'entity',
            url: '/risk-management?page&sort&search',
            data: {
                  authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
                pageTitle: 'finreApp.riskManagement.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/risk-management/risk-managements.html',
                    controller: 'RiskManagementController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('riskManagement');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('risk-management-detail', {
            parent: 'entity',
            url: '/risk-management/{dealId}',
            data: {
                  authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
                  pageTitle: 'finreApp.riskManagement.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/risk-management/risk-management-detail.html',
                    controller: 'RiskManagementDetailController',
                    controllerAs: 'vm'
                }
            },
          resolve: {
            entity: function () {
              return {
                id: null
              };
            },
            translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
              $translatePartialLoader.addPart('riskManagement');
              $translatePartialLoader.addPart('global');
              return $translate.refresh();
            }]
          }
        })
        .state('deal.risk-management', {
            parent: 'deal',
            url: '/new-risk-management',
            data: {
                  authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/risk-management/risk-management-dialog.html',
                    controller: 'RiskManagementDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('deal', null, { reload: true });
                }, function() {
                    $state.go('deal');
                });
            }]
        })
        .state('risk-management.edit', {
            parent: 'risk-management',
            url: '/{id}/edit',
            data: {
                  authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/risk-management/risk-management-dialog.html',
                    controller: 'RiskManagementDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['RiskManagement', function(RiskManagement) {
                            return RiskManagement.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('risk-management', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('risk-management.delete', {
            parent: 'risk-management',
            url: '/{id}/delete',
            data: {
                  authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/risk-management/risk-management-delete-dialog.html',
                    controller: 'RiskManagementDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['RiskManagement', function(RiskManagement) {
                            return RiskManagement.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('risk-management', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
