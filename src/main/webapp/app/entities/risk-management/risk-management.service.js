(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('RiskManagement', RiskManagement);

    RiskManagement.$inject = ['$resource'];

    function RiskManagement ($resource) {
        var resourceUrl =  'api/risk-managements/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
