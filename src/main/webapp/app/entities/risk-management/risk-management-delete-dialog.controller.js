(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('RiskManagementDeleteController',RiskManagementDeleteController);

    RiskManagementDeleteController.$inject = ['$uibModalInstance', 'entity', 'RiskManagement'];

    function RiskManagementDeleteController($uibModalInstance, entity, RiskManagement) {
        var vm = this;
        vm.riskManagement = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            RiskManagement.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
