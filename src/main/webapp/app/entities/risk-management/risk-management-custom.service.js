(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('RiskCustomSrv', RiskCustomSrv);
  RiskCustomSrv.$inject = ['$resource', 'DateUtils'];
  function RiskCustomSrv($resource, DateUtils) {
    var resourceUrl = 'api/risk-managements/upload';
    var byDae='api/risk-managements/byDate/:startYear/:startMonth/:startDay';
  var byDeal ='api/risk-managements/deal/:dealId/:startYear/:startMonth/:startDay';
    return {
      upload: $resource(resourceUrl, {}, {
          query: {method: 'POST', params: {}, isArray: false
          }
      }),
      getByDate: $resource(byDae, {}, {
        query: {method: 'GET', params: {}, isArray: true}
      }),
      getByDeal: $resource(byDeal, {}, {
        query: {method: 'GET', params: {}, isArray: true}
      })

    }
  }
})();
