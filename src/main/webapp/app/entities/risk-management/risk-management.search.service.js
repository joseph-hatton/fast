(function() {
    'use strict';

    angular
        .module('finreApp')
        .factory('RiskManagementSearch', RiskManagementSearch);

    RiskManagementSearch.$inject = ['$resource'];

    function RiskManagementSearch($resource) {
        var resourceUrl =  'api/_search/risk-managements/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
