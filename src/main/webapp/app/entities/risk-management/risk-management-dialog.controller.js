(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('RiskManagementDialogController', RiskManagementDialogController);

  RiskManagementDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'RiskManagement', 'RiskCustomSrv', 'Principal', 'InteractionsService', '$q'];

  function RiskManagementDialogController($scope, $stateParams, $uibModalInstance, entity, RiskManagement, RiskCustomSrv, Principal, InteractionsService, $q) {
    var vm = this;
    vm.riskManagement = entity;
    vm.riskManagement.monthYear = null;
    vm.year = null;
    vm.warningMessage = null;
    vm.riskManagementDelete = [];
    vm.overwrite = false;

    vm.load = function (id) {
      RiskManagement.get({id: id}, function (result) {
        vm.riskManagement = result;
      });
    };

    vm.format = "yyyy-MM";
    vm.datePickerOpenStatus = {};
    vm.datePickerOpenStatus.monthYear = false;

    vm.openCalendar = function (date) {
      vm.datePickerOpenStatus[date] = true;
    };

    vm.dateOptions = {
      datepickerMode: 'month',
      formatMonth: 'MMMM',
      formatYear: 'yyyy',
      formatMonthTitle: 'yyyy',
      minMode: 'month',
      dateDisabled: displayQuarterly,
      showMonths: true,
      startingDay: 0,
      yearRange: 20,
      minDate: null,
      maxDate: null
    };
    vm.getQuarter = function (date) {
      vm.quarter = moment(date).quarter();
      vm.year = moment(date).year();
    };
    var onSaveSuccess = function (result) {
      var confirmUpload = InteractionsService.alertParentScrollFalseDialog('Data was succesfully  uploaded for ' + vm.year + ' Q' + vm.quarter, '#myModal');
      var ErrorUpload = InteractionsService.alertParentScrollFalseDialog(result.statusMessage, '#myModal');

      if (result.statusMessage === 'Successful!') {
        if (vm.overwrite === true) {
          angular.forEach(vm.riskManagementDelete, function (i, idx) {
            RiskManagement.delete(i);
          });
        }
        InteractionsService.showThenDialog(confirmUpload, function () {
          $scope.$emit('finreApp:riskManagementUpdate');
          $uibModalInstance.close();
          vm.isSaving = false;
        });
      } else {
        InteractionsService.showThenDialog(ErrorUpload, function () {
          vm.isSaving = false;
        });
      }
    };

    var onSaveError = function () {
      vm.isSaving = false;
    };

    Principal.identity().then(function (account) {
      angular.forEach(account.authorities, function (authoritie) {
        if (authoritie === "ROLE_APP_FINRE_OPS_ADMIN") {
          vm.authority = 'Admin';
        } else {
          if (vm.authority !== 'Admin') {
            vm.authority = 'User';
          }
        }
      });
      vm.riskManagement.lastUser = account.login;
      vm.riskManagement.creationUser = account.login;
    });

    vm.save = function () {
      vm.riskManagement.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
      vm.riskManagement.creationTs =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
      var confirm = InteractionsService.confirmBasicParentNoScrollDialog('Are you ready to upload the data for ' + vm.year + '-Q' + vm.quarter + '?', '#myModal');
      InteractionsService.showThenDialog(confirm, function () {
        RiskCustomSrv.upload.query(vm.riskManagement, onSaveSuccess, onSaveError);
      }, function () {
        console.log('NO upload');
      });
    };

    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };

    vm.checkDate = function (date) {
      vm.riskManagement.uploadText = '';
      vm.getQuarter(date);
      RiskCustomSrv.getByDate.query({
        startYear: moment(date).get('year'),
        startMonth: moment(date).get('month') + 1,
        startDay: moment(date).get('date')
      }, function (result) {
        vm.riskManagementDelete = result;
        if (result.length > 0)
          vm.warningMessage = 'You have previously uploaded ' + result.length + ' records for '
            + vm.year + '-Q' + vm.quarter + '. Click the checkbox to proceed to overwrite by uploading new data.';
        else
          vm.warningMessage = null;
      });
    };
  }
})();
