(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('RiskManagementDetailController', RiskManagementDetailController);

  RiskManagementDetailController.$inject = ['$scope', '$mdDialog', 'orderByFilter', '$rootScope', '$stateParams', 'CurrencyCustomSrv', 'RiskCustomSrv', 'Deal', 'RiskManagement', 'InteractionsService','Principal', 'Intro'];

  function RiskManagementDetailController($scope, $mdDialog, orderBy, $rootScope, $stateParams, CurrencyCustomSrv, RiskCustomSrv, Deal, RiskManagement, InteractionsService,Principal,Intro) {
    var vm = this;
    vm.currencyCode = [];
    vm.currencyCodeSelected = [];
    vm.monthYear = null;
    vm.accordion = [{"code": null, "risk": [], "status": false}];
    vm.filter = false;
    vm.riskManagementCopy = {};
    vm.predicate = 'riskCriteria';
    vm.reverse = true;
    vm.flagRecords = false;

    vm.format = "yyyy-MM";
    vm.datePickerOpenStatus = {};
    vm.datePickerOpenStatus.monthYear = false;

    Intro.setupIntroSteps({id:146})
    .then(function(result){
        vm.introOptions = {
            steps: result
        };
    });

    vm.openCalendar = function (date) {
      vm.datePickerOpenStatus[date] = true;
    };

    vm.saveValue = function (riskManagement) {
      angular.copy(riskManagement, vm.riskManagementCopy);
    };

    vm.acceptChanges = function (amount, riskManagement, editform, parent, index) {
      if (amount === null) {
        var confirm = InteractionsService.confirmBasicParentNoScrollDialog('Previous value will be replaced with a blank.', '#myModal');
        InteractionsService.showThenDialog(confirm, function () {
          $scope.status = 'ok';
        }, function () {
          vm.updateCancel(riskManagement, editform, parent, index);
        });
      }
    };

    vm.transition = function transition(array) {
      array.risk = orderBy(array.risk, vm.predicate, vm.reverse);
    };

    vm.loadQuarter = function () {
      vm.currencyCode = [];
      vm.accordion = [{"code": null, "risk": [], "status": false}];
      var year = moment(vm.monthYear).year();
      var month = moment(vm.monthYear).month() + 1;
      var day = 1;
      RiskCustomSrv.getByDeal.query({
        dealId: $stateParams.dealId,
        startYear: year,
        startMonth: month,
        startDay: day
      }, function (result) {
        vm.flagRecords = result.length === 0;
        vm.accordion = [];
        angular.forEach(result, function (risk) {
          var flag = false;
          angular.forEach(vm.accordion, function (RiskAccordion) {
            if (risk.currency.code !== null && RiskAccordion.code === risk.currency.code) {
              RiskAccordion.risk.push(risk);
              flag = true;
            }
          });
          if (!flag) {
            vm.accordion.push({"code": risk.currency.code, "risk": [risk], "status": false});
            vm.currencyCode.push(risk.currency.code);
          }
        });
      });
    };

    vm.updateCancel = function (riskManagement, editform, parent, index) {
      angular.copy(vm.riskManagementCopy, riskManagement);
      var name = 'amount' + '' + parent + '' + index;
      editform[name].$setPristine(true);
      if (vm.checkForm(editform))
        editform.$setPristine(true);
    };

    Principal.identity().then(function (account) {
      angular.forEach(account.authorities, function (authoritie) {
        if (authoritie === "ROLE_APP_FINRE_OPS_ADMIN") {
          vm.authority = 'Admin';
        } else {
          if (authoritie ==='ROLE_APP_FINRE_VIEW_ONLY' || authoritie ==='ROLE_APP_STRUCTURED_FINANCE_VIEW_ONLY'){
            vm.authority = 'ViewOnly';

          }else{
            if (vm.authority !== 'Admin') {
            vm.authority = 'User';
          }
        }}
      });
      vm.lastUser = account.login;
    });
    vm.updateRisk = function (riskManagement, editform, parent, index) {
      console.log(riskManagement.lastUpdate);
      riskManagement.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
      console.log(riskManagement.lastUpdate);
      console.log(riskManagement);
      riskManagement.lastUser =  vm.lastUser;
      var name = 'amount' + '' + parent + '' + index;
      RiskManagement.update(riskManagement, function (result) {
        editform[name].$setPristine(true);
        if (vm.checkForm(editform))
          editform.$setPristine(true);
      });
    };

    vm.checkForm = function (editForm) {
      function checkIfSpecialOrDirty(key) {
        if (key[0] === '$')
          return false;
        return editForm[key].$dirty;
      }

      var keys = Object.keys(editForm);

      var isDirty = keys.some(checkIfSpecialOrDirty);

      return !isDirty;
    };

    vm.changeAmount = function (riskManagement) {
      riskManagement.convertedAmount = (riskManagement.amount * riskManagement.currency.exchangeRate).toFixed(2);
    };
    vm.filterByCode = function () {
      angular.forEach(vm.accordion, function (AccordionPart, key) {
        var flag = false;
        angular.forEach(vm.currencyCodeSelected, function (code) {
          if (code === AccordionPart.code)
            flag = true;
        });
        if(!flag){
          vm.accordion.splice(key, 1);
          vm.currencyCode.splice(key, 1);
        }
      });
      vm.filter = true;
    };
    vm.reset = function () {
      vm.loadQuarter();
      vm.filter = false;
    };
    vm.expand = function () {
      angular.forEach(vm.accordion, function (currency) {
        currency.status = true;
      });
    };
    vm.collapse = function () {
      angular.forEach(vm.accordion, function (currency) {
        currency.status = false;
      });
    };

    // vm.createObject= function(RiskList,UniqueCode){
    //   angular.forEach(UniqueCode, function (code,key) {
    //    vm.accordionFormat[key]={code:code,risk:[],status:false};
    // });
    //  console.log(vm.accordionFormat);
    //  return vm.accordionFormat;

    //  };

    vm.dateOptions = {
      datepickerMode: 'month',
      formatMonth: 'MMMM',
      formatYear: 'yyyy',
      formatMonthTitle: 'yyyy',
      minMode: 'month',
      dateDisabled: displayQuarterly,
      showMonths: true,
      startingDay: 0,
      yearRange: 20,
      minDate: null,
      maxDate: null
    };

    vm.getQuarter = function (date) {
      vm.quarter = moment(date).quarter();
    };

    Deal.get({id: $stateParams.dealId}, function (result) {
      vm.deal = result;
    });
    var unsubscribe = $rootScope.$on('finreApp:riskManagementUpdate', function (event, result) {
      vm.riskManagement = result;
    });
    $scope.$on('$destroy', unsubscribe);
  }
})();
