(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('RiskManagementController', RiskManagementController);

    RiskManagementController.$inject = ['$scope', '$state', 'RiskManagement', 'RiskManagementSearch', 'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants','TreatySrv', 'Intro'];

    function RiskManagementController ($scope, $state, RiskManagement, RiskManagementSearch, ParseLinks, AlertService, pagingParams, paginationConstants,TreatySrv, Intro) {
        var vm = this;
        vm.loadAll = loadAll;
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.clear = clear;
        vm.search = search;
        vm.searchQuery = pagingParams.search;
        vm.currentSearch = pagingParams.search;
        vm.loadAll();
        vm.itemsPerPage = paginationConstants.itemsPerPage;

        

      vm.dealsList=[];
      TreatySrv.nopages.query(function (result) {
        angular.forEach (result, function(deal,key){
            vm.dealsList.push(deal);
        })
      });
      vm.monthYear=null;
      vm.openCalendar = function (date) {
        vm.datePickerOpenStatus[date] = true;
      };
      vm.format = "yyyy-MM";
      vm.datePickerOpenStatus = {};
      function displayQuarterly (data) {
        return data.mode === 'month' &&
          (data.date.getMonth() === 0 || data.date.getMonth() === 1 ||
          data.date.getMonth() === 3 || data.date.getMonth() === 4 ||
          data.date.getMonth() === 6 || data.date.getMonth() === 7 ||
          data.date.getMonth() === 9 || data.date.getMonth() === 10 );
      }
      vm.datePickerOpenStatus.monthYear = false;
      vm.dateOptions = {
        datepickerMode: 'month',
        formatMonth: 'MMMM',
        formatYear: 'yyyy',
        formatMonthTitle: 'yyyy',
        minMode: 'month',
        dateDisabled: displayQuarterly,
        showMonths: true,
        startingDay: 0,
        yearRange: 20,
        minDate: null,
        maxDate: null
      };
      vm.getQuarter= function(date){
        vm.quarter=moment(date).quarter();

      };
        function loadAll () {
            if (pagingParams.search) {
                RiskManagementSearch.query({
                    query: pagingParams.search,
                    page: pagingParams.page - 1,
                    size: paginationConstants.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);
            } else {
                RiskManagement.query({
                    page: pagingParams.page - 1,
                    size: paginationConstants.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);
            }
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.riskManagements = data;
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage (page) {
            vm.page = page;
            vm.transition();
        }

        function transition () {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }

        function search (searchQuery) {
            if (!searchQuery){
                return vm.clear();
            }
            vm.links = null;
            vm.page = 1;
            vm.predicate = '_score';
            vm.reverse = false;
            vm.currentSearch = searchQuery;
            vm.transition();
        }

        function clear () {
            vm.links = null;
            vm.page = 1;
            vm.predicate = 'id';
            vm.reverse = true;
            vm.currentSearch = null;
            vm.transition();
        }

    }
})();
