(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('RiskFeeDialogController', RiskFeeDialogController);

    RiskFeeDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'RiskFee'];

    function RiskFeeDialogController ($scope, $stateParams, $uibModalInstance, entity, RiskFee) {
        var vm = this;
        vm.riskFee = entity;
        vm.load = function(id) {
            RiskFee.get({id : id}, function(result) {
                vm.riskFee = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('finreApp:riskFeeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.riskFee.id !== null) {
                RiskFee.update(vm.riskFee, onSaveSuccess, onSaveError);
            } else {
                RiskFee.save(vm.riskFee, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
