(function() {
    'use strict';

    angular
        .module('finreApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('risk-fee', {
            parent: 'entity',
            url: '/risk-fee',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
                pageTitle: 'finreApp.riskFee.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/risk-fee/risk-fees.html',
                    controller: 'RiskFeeController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('riskFee');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('risk-fee-detail', {
            parent: 'entity',
            url: '/risk-fee/{id}',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
                pageTitle: 'finreApp.riskFee.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/risk-fee/risk-fee-detail.html',
                    controller: 'RiskFeeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('riskFee');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'RiskFee', function($stateParams, RiskFee) {
                    return RiskFee.get({id : $stateParams.id});
                }]
            }
        })
        .state('risk-fee.new', {
            parent: 'risk-fee',
            url: '/new',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/risk-fee/risk-fee-dialog.html',
                    controller: 'RiskFeeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                rate: null,
                                treatyId: null,
                                treatyReference: null,
                                period: null,
                                comments: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('risk-fee', null, { reload: true });
                }, function() {
                    $state.go('risk-fee');
                });
            }]
        })
        .state('risk-fee.edit', {
            parent: 'risk-fee',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/risk-fee/risk-fee-dialog.html',
                    controller: 'RiskFeeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['RiskFee', function(RiskFee) {
                            return RiskFee.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('risk-fee', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('risk-fee.delete', {
            parent: 'risk-fee',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/risk-fee/risk-fee-delete-dialog.html',
                    controller: 'RiskFeeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['RiskFee', function(RiskFee) {
                            return RiskFee.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('risk-fee', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
