(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('RiskFeeController', RiskFeeController);

    RiskFeeController.$inject = ['$scope', '$state', 'RiskFee', 'RiskFeeSearch'];

    function RiskFeeController ($scope, $state, RiskFee, RiskFeeSearch) {
        var vm = this;
        vm.riskFees = [];
        vm.loadAll = function() {
            RiskFee.query(function(result) {
                vm.riskFees = result;
            });
        };

        vm.search = function () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            RiskFeeSearch.query({query: vm.searchQuery}, function(result) {
                vm.riskFees = result;
            });
        };
        vm.loadAll();
        
    }
})();
