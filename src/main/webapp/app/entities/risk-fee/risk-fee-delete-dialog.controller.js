(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('RiskFeeDeleteController',RiskFeeDeleteController);

    RiskFeeDeleteController.$inject = ['$uibModalInstance', 'entity', 'RiskFee'];

    function RiskFeeDeleteController($uibModalInstance, entity, RiskFee) {
        var vm = this;
        vm.riskFee = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            RiskFee.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
