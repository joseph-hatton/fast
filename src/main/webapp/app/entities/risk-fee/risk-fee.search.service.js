(function() {
    'use strict';

    angular
        .module('finreApp')
        .factory('RiskFeeSearch', RiskFeeSearch);

    RiskFeeSearch.$inject = ['$resource'];

    function RiskFeeSearch($resource) {
        var resourceUrl =  'api/_search/risk-fees/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
