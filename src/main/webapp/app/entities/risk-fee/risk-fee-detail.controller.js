(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('RiskFeeDetailController', RiskFeeDetailController);

    RiskFeeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'RiskFee'];

    function RiskFeeDetailController($scope, $rootScope, $stateParams, entity, RiskFee) {
        var vm = this;
        vm.riskFee = entity;
        vm.load = function (id) {
            RiskFee.get({id: id}, function(result) {
                vm.riskFee = result;
            });
        };
        var unsubscribe = $rootScope.$on('finreApp:riskFeeUpdate', function(event, result) {
            vm.riskFee = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
