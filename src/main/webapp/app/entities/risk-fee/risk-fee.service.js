(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('RiskFee', RiskFee);

    RiskFee.$inject = ['$resource'];

    function RiskFee ($resource) {
        var resourceUrl =  'api/risk-fees/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
