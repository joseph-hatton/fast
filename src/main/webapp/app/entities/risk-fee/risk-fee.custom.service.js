(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('RiskFeeSrv', RiskFeeSrv);

  RiskFeeSrv.$inject = ['$resource'];

  function RiskFeeSrv($resource) {
    return {
      riskfee: $resource('api/risk-fees/treaties/:id',{}, {
        query: { method: 'GET', params: {}, isArray: true }
      })
    };
  }
})();
