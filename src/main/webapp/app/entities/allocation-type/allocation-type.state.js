(function() {
    'use strict';

    angular
        .module('finreApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('allocation-type', {
            parent: 'entity',
            url: '/allocation-type',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
                pageTitle: 'finreApp.allocationType.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/allocation-type/allocation-types.html',
                    controller: 'AllocationTypeController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('allocationType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('allocation-type-detail', {
            parent: 'entity',
            url: '/allocation-type/{id}',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
                pageTitle: 'finreApp.allocationType.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/allocation-type/allocation-type-detail.html',
                    controller: 'AllocationTypeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('allocationType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'AllocationType', function($stateParams, AllocationType) {
                    return AllocationType.get({id : $stateParams.id});
                }]
            }
        })
        .state('allocation-type.new', {
            parent: 'allocation-type',
            url: '/new',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/allocation-type/allocation-type-dialog.html',
                    controller: 'AllocationTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                description: null,
                                sortOrder: null,
                                default_flag: null,
                                lastUser: null,
                                lastUpdate: null,
                                creationTS: null,
                                creationUser: null,
                                effBeginTs: null,
                                effEndTs: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('allocation-type', null, { reload: true });
                }, function() {
                    $state.go('allocation-type');
                });
            }]
        })
        .state('allocation-type.edit', {
            parent: 'allocation-type',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/allocation-type/allocation-type-dialog.html',
                    controller: 'AllocationTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AllocationType', function(AllocationType) {
                            return AllocationType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('allocation-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('allocation-type.delete', {
            parent: 'allocation-type',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/allocation-type/allocation-type-delete-dialog.html',
                    controller: 'AllocationTypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AllocationType', function(AllocationType) {
                            return AllocationType.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('allocation-type', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
