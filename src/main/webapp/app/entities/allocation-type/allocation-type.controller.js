(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('AllocationTypeController', AllocationTypeController);

    AllocationTypeController.$inject = ['$scope', '$state', 'AllocationType'];

    function AllocationTypeController ($scope, $state, AllocationType) {
        var vm = this;
        vm.allocationTypes = [];
        vm.loadAll = function() {
            AllocationType.query(function(result) {
                vm.allocationTypes = result;
            });
        };
        vm.loadAll();

    }
})();
