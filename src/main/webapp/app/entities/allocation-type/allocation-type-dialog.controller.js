(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('AllocationTypeDialogController', AllocationTypeDialogController);

    AllocationTypeDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'AllocationType', 'DialogService'];

    function AllocationTypeDialogController ($scope, $stateParams, $uibModalInstance, entity, AllocationType, DialogService) {
      this.prototype = Object.create(DialogService.prototype);

      var datePickerItemsRun = function (vm) {
        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.lastUpdate = false;
        vm.datePickerOpenStatus.creationTS = false;
        vm.datePickerOpenStatus.effBeginTs = false;
        vm.datePickerOpenStatus.effEndTs = false;
      };

      this.allocationType = entity;

      DialogService.call(this, $scope, $stateParams, $uibModalInstance, entity, AllocationType, this.allocationType, "AllocationType", datePickerItemsRun);
    }
})();
