(function() {
    'use strict';

    angular
        .module('finreApp')
        .factory('AllocationTypeSearch', AllocationTypeSearch);

    AllocationTypeSearch.$inject = ['$resource'];

    function AllocationTypeSearch($resource) {
        var resourceUrl =  'api/_search/allocation-types/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
