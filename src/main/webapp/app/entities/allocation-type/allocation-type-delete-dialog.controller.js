(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('AllocationTypeDeleteController',AllocationTypeDeleteController);

    AllocationTypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'AllocationType'];

    function AllocationTypeDeleteController($uibModalInstance, entity, AllocationType) {
        var vm = this;
        vm.allocationType = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            AllocationType.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
