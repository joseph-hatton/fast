(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('AllocationTypeDetailController', AllocationTypeDetailController);

    AllocationTypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'AllocationType', 'DetailService'];

  function AllocationTypeDetailController($scope, $rootScope, $stateParams, entity, AllocationType, DetailService) {
    this.prototype = Object.create(DetailService.prototype);

    this.allocationType = entity;

    DetailService.call(this, $scope, $rootScope, $stateParams, AllocationType, this.allocationType, "AllocationType");
  }
})();
