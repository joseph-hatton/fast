(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('EucUse', EucUse);
  
  EucUse.$inject = ['$resource'];
  
  function EucUse($resource) {
    var resourceUrl = 'api/euc-uses/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          return data;
        }
      },
      'update': {method: 'PUT'}
    });
  }
})();
