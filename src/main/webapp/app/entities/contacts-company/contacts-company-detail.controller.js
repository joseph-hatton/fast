(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('ContactsCompanyDetailController', ContactsCompanyDetailController);

  ContactsCompanyDetailController.$inject = ['$mdDialog', '$scope', '$location', '$filter', '$rootScope', '$stateParams', 'entity',
    'XrefTreatyCompany', 'XrefTreatyCustomCompany', 'CompanyCustomSrv', 'ContactCustomSrv', 'Treaty', 'Principal',
    'ContactSubtype', 'ContactType', 'XrefTreatyContact', 'XrefTreatyCustomContact', 'Contact', 'TreatyTransactionSrv', 'InteractionsService', 'TreatyTrackingCustomSvr', 'Intro'];

  function ContactsCompanyDetailController($mdDialog, $scope, $location, $filter, $rootScope, $stateParams, entity,
                                           XrefTreatyCompany, XrefTreatyCustomCompany, CompanyCustomSrv, ContactCustomSrv, Treaty, Principal,
                                           ContactSubtype, ContactType, XrefTreatyContact, XrefTreatyCustomContact, Contact, TreatyTransactionSrv, InteractionsService, TreatyTrackingCustomSvr, Intro) {
    var vm = this;
    vm.contactsCompany = entity;
    vm.treatyId = $stateParams.id;
    vm.dealId = $stateParams.dealId;

    Intro.setupIntroSteps({id:169})
    .then(function(result){
        vm.introOptions = {
            steps: result
        };
    });

    TreatyTrackingCustomSvr.count.get({treatyId: vm.treatyId}).$promise.then(function (result) {
      vm.trackingCount = result.amount;
    });

    if ($stateParams.contactId !== "") {
      vm.contactId = $stateParams.contactId;
    }
    Principal.identity().then(function (account) {
      angular.forEach(account.authorities, function (authoritie) {
        if (authoritie === "ROLE_APP_FINRE_OPS_ADMIN") {
          vm.authority = 'Admin';
        } else {
          if (authoritie === "ROLE_APP_FINRE_VIEW_ONLY") {
            vm.authority = 'ViewOnly'
          }else{
            if (vm.authority !== 'Admin' && vm.authority !== 'ViewOnly') {
              vm.authority = 'User';
            }
          }
        }
      });
      vm.lastUser = account.login;
    });

    Treaty.get({id: $stateParams.id}, function (result) {
      vm.treaty = result;
      vm.activeTreaty=result.treatyStatus.name!=='Pending';
      ContactType.query(function (res) {
        vm.contactTypes = res;
        angular.forEach(res, function (contact) {
          if (contact.name === 'Broker') {
            vm.selectedContactType = contact;
          }
        });
      });
    });

    vm.selectedCompany = null;
    vm.xrefTreatyCompanies = [];

    // vm.companies = [];
    /*****************************************************
     * COMPANIES
     *****************************************************/
    vm.companyTypeOption = null;
    vm.companyTypes = [
      {name: 'Assuming'},
      {name: 'Ceding'}];
    vm.searchContacts = function () {
      if(vm.selectedContact) {
        ContactCustomSrv.name.query({search: vm.selectedContact}, function (result) {
          vm.contacts = result;
        });
      }
      return vm.contacts;
    };
    if (vm.contactId) {
      Contact.get({id: vm.contactId}, function (result) {
        vm.selectedContact = result;
      });
    } else {
      vm.selectedContact = null;
    }

    // CompanyCustomSrv.companies.query({search: 'rga'}, function (result) {
    //   if (vm.treaty && vm.selectedCompany !== null) {
    //     CompanyCustomSrv.companies.query({search: vm.selectedCompany}, function (result) {
    //       vm.companies = result;
    //     });
    //   }
    // });

    XrefTreatyCustomCompany.treaties.query({id: $stateParams.id}).$promise.then(function (result) {
      vm.xrefTreatyCompanies = result;
    });

    // if ($stateParams.hash != null) {
    //   if ($stateParams.hash === "statusContact") {
    //     vm.statusContact = true;
    //   }
    //   $location.hash($stateParams.hash);
    // }

    $scope.addCompany = function () {
      if (!vm.checkCompany(vm.selectedCompany, vm.companyTypeOption.name)) {
        var confirm = InteractionsService.alertParentScrollFalseDialog('This company has already been added.', '#myModal');
        InteractionsService.showThenDialog(confirm);
      } else {
        vm.xrefTreatyCompanies.push({
          companyType: vm.companyTypeOption.name,
          company: vm.selectedCompany,
          treatyId: $stateParams.id
        });
        vm.selectedCompany = '';
        vm.saveCompanies();
      }
    };

    $scope.removeCompany = function (companySelected,index) {
      ////User Cannot Remove a Treaty Company if its tied to a Transaction
      TreatyTransactionSrv.companyCount.query({
        treatyId: vm.treaty.id,
        companyId: companySelected.company.id
      }, function (result) {
        if (result.length > 0) {
          var confirm = InteractionsService.alertParentScrollFalseDialog('You cannot delete this company because it is being used on one or more transactions.', '#myModal');
          InteractionsService.showThenDialog(confirm);
        } else {
          vm.xrefTreatyCompanies.splice(index, 1);
            XrefTreatyCompany.delete({id: companySelected.id}, function () {
              vm.isSaving = false;
            }, onSaveError);
        }
      });
    };

    vm.searchCompanies = function () {
      if(vm.selectedCompany) {
        CompanyCustomSrv.companies.query({search: vm.selectedCompany}).$promise.then(function (result) {
          vm.companies = result;
        });
        return vm.companies;
      }
    };
    vm.saveCompanies = function () {
      angular.forEach(vm.xrefTreatyCompanies, function (treaty, key) {
        treaty.lastUser = vm.lastUser;
        treaty.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        if (treaty.id == null) {
          treaty.creationUser = vm.lastUser;
          treaty.creationTs =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
          XrefTreatyCompany.save(treaty, function (resultCompanySave) {
            vm.xrefTreatyCompanies[key]=resultCompanySave;
            vm.isSaving = true;
          }, onSaveError);
        } else {
          XrefTreatyCompany.update(treaty, function () {
            vm.isSaving = true;
          }, onSaveError);
        }
      });


    };

    var onSaveError = function () {
      vm.isSaving = false;
    };

    vm.xrefTreatyContacts = [];
    vm.deleteXrefTreatyContacts = [];
    vm.contacts = [];
    vm.selectedContactSubtype = null;
    /*****************************************************
     * CONTACTS
     *****************************************************/
    vm.contactSubtypes = ContactSubtype.query();

    XrefTreatyCustomContact.treaties.query({id: $stateParams.id}).$promise.then( function (result) {
      vm.xrefTreatyContacts = result;
    });

    vm.checkContactExist = function (contact, type) {
      var flag = true;
      angular.forEach(vm.xrefTreatyContacts, function (contacts, key) {
        if (flag && contacts.contact.id === contact.id)
          flag = false;
      });
      return flag;
    };

    vm.checkCompany = function (company, type) {
      var flag = true;
      angular.forEach(vm.xrefTreatyCompanies, function (comnpanyObject, key) {
        if (flag && comnpanyObject.company.id === company.id)
          flag = false;
      });
      return flag;
    };

    $scope.addContact = function () {
      if (!vm.checkContactExist(vm.selectedContact, vm.selectedContactType.name)) {
        var confirm = InteractionsService.alertParentScrollFalseDialog('This contact has already been added.', '#myModal');
        InteractionsService.showThenDialog(confirm);
      } else {
        vm.xrefTreatyContacts.push({
          contactType: vm.selectedContactType,
          contactSubtype: vm.selectedContactSubtype,
          contact: vm.selectedContact,
          treatyId: $stateParams.id,
          creationUser: vm.lastUser,
          creationTs: moment(),
          lastUser: vm.lastUser,
          lastUpdate: moment()
        });

        vm.saveContacts();
        vm.selectedContact = '';
      }
    };
    $scope.removeContact = function (index) {
      vm.deleteXrefTreatyContacts.push(vm.xrefTreatyContacts[index]);
      vm.xrefTreatyContacts.splice(index, 1);
      vm.saveContacts();
    };

    vm.saveContacts = function () {
      angular.forEach(vm.xrefTreatyContacts, function (treaty, key) {
        treaty.lastUser = vm.lastUser;
        treaty.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        if (treaty.id == null) {
          treaty.creationUser = vm.lastUser;
          treaty.creationTs =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
          XrefTreatyContact.save(treaty, onSaveOrUpdateContactSuccess, onSaveError);
        } else {
          treaty.lastUser = vm.lastUser;
          treaty.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
          XrefTreatyContact.update(treaty, onSaveOrUpdateContactSuccess, onSaveError);
        }
      });
      angular.forEach(vm.deleteXrefTreatyContacts, function (deleteTreaty, key) {
        XrefTreatyContact.delete(deleteTreaty, function () {
        }, onSaveError);
      });
      vm.xrefTreatyContacts = [];
      vm.deleteXrefTreatyContacts = [];
    };

    var onSaveOrUpdateContactSuccess = function (result) {
      vm.xrefTreatyContacts.push(result);
    };

    $scope.$watch('vm.selectedContactType', function (newVal, oldVal) {
      if (vm.selectedContactType != null && vm.selectedContactType.name === 'Internal') {
        vm.subtypeDisabled = false;
      } else {
        vm.subtypeDisabled = true;
        vm.selectedContactSubtype = null;
      }
    });

    var unsubscribe = $rootScope.$on('finreApp:contactsCompanyUpdate', function (event, result) {
      vm.treaty = result;
    });
    $scope.$on('$destroy', unsubscribe);
  }
})();
