(function () {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('contacts-company-detail', {
        parent: 'entity',
        url: '/contacts-company/{dealId}/{id}/{contactId}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.contactsCompany.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/contacts-company/contacts-company-detail.html',
            controller: 'ContactsCompanyDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('contactsCompany');
            return $translate.refresh();
          }],
          entity: ['$stateParams', function ($stateParams) {
            return;
          }]
        }
      })
      .state('contacts-company-detail.xnew', {
        parent: 'contacts-company-detail',
        url: '/xnew/{contactId}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/contact/contact-dialog.html',
            controller: 'ContactDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  salutation: null,
                  firstName: null,
                  lastName: null,
                  company: null,
                  mdmCompany: null,
                  contactType: null,
                  address1: null,
                  address2: null,
                  city: null,
                  state: null,
                  postalCode: null,
                  country: null,
                  region: null,
                  phoneNumber: null,
                  faxNumber: null,
                  mobileNumber: null,
                  email: null,
                  treatyType: null,
                  id: null
                };
              }
            }
          }).result.then(function () {
          // we dont use this one because we are calling from the controller
          }, function () {
            $state.go('contacts-company-detail', {id: $stateParams.id,dealId: $stateParams.dealId, contactId:""}, {reload: true});
          });
        }]
      })
      .state('contacts-company-detailContact', {
        parent: 'entity',
        url: '/contacts-company-contact/{dealId}/{id}/{contactId}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.contactsCompany.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/contacts-company/contacts-company-detail.html',
            controller: 'ContactsCompanyDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('contactsCompany');
            return $translate.refresh();
          }],
          entity: ['$stateParams', function ($stateParams) {
            return ;          }]
        }
      })

  }

})();
