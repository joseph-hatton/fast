(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('TreatyTransactionRelief', TreatyTransactionRelief);

    TreatyTransactionRelief.$inject = ['$resource', 'DateUtils'];

    function TreatyTransactionRelief ($resource, DateUtils) {
        var resourceUrl =  'api/treaty-transaction-reliefs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                  data = angular.fromJson(data);
                    angular.forEach(data,function(trans,key){
                      trans.reportingDate = DateUtils.convertLocalDateFromServer(trans.reportingDate);
                    });
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    return angular.toJson(data);
                }
            }
        });
    }
})();
