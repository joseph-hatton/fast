
(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('TreatyTransactionMixSrv', TreatyTransactionMixSrv);

  TreatyTransactionMixSrv.$inject = ['$resource', 'DateUtils'];

  function TreatyTransactionMixSrv($resource, DateUtils) {

    var resourceUrl=    'api/treaty-transaction-cash/mixSpecific/1/:treatyId';
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', params: {}, isArray: true},
      'get': {
        method: 'GET', params: {}, isArray: true,
        transformResponse: function (data) {
          data = angular.fromJson(data);
          angular.forEach(data,function (value,key) {
            value.settlementDate = DateUtils.convertLocalDateFromServer(value.settlementDate);
            value.accountingDate = DateUtils.convertLocalDateFromServer(value.accountingDate);
            angular.forEach(value.subTransactions,function (datesub,key) {
              datesub.reportingDate = DateUtils.convertLocalDateFromServer(datesub.reportingDate);
            })
          });

          return data;
        }
      }
    });
  }
})();
