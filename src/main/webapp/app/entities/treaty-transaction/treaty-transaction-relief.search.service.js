(function() {
    'use strict';

    angular
        .module('finreApp')
        .factory('TreatyTransactionReliefSearch', TreatyTransactionReliefSearch);

    TreatyTransactionReliefSearch.$inject = ['$resource'];

    function TreatyTransactionReliefSearch($resource) {
        var resourceUrl =  'api/_search/treaty-transaction-reliefs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
