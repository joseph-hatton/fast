(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('TreatyTransactionReliefSrv', TreatyTransactionReliefSrv);

  TreatyTransactionReliefSrv.$inject = ['$resource', 'DateUtils'];

  function TreatyTransactionReliefSrv($resource, DateUtils) {
    var resourceUrl = 'api/treaty-transaction-reliefs/ordered-by-latest-subtransaction/deals/:id';
    return {
      'base': $resource('api/treaty-transaction-reliefs/ordered-by-transactions/deals/:id', {}, {
        'query': {method: 'GET', params: {}, isArray: true},
        'get': {
          method: 'GET', params: {}, isArray: true,
          transformResponse: function (data) {
            data = angular.fromJson(data);
            angular.forEach(data, function (trans, key) {
              trans.reportingDate = DateUtils.convertLocalDateFromServer(trans.reportingDate);
            });
            return data;
          }
        }
      }),
      'count': $resource('api/treaty-transaction-reliefs-count/deals/:dealId', {}, {
        'get': {
          method: 'GET',
          isArray: false,
          transformResponse: function (data) {
            data = angular.fromJson(data);
            return data;
          }
        }
      })
    };
  }
})();
