(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .factory('TreatyTransactionSearch', TreatyTransactionSearch);
  
  TreatyTransactionSearch.$inject = ['$resource'];
  
  function TreatyTransactionSearch($resource) {
    var resourceUrl = 'api/_search/treaty-transaction-cash/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true}
    });
  }
})();
