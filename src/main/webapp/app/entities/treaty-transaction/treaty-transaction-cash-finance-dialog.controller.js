(function () {
  'use strict';
  angular
    .module('finreApp')
    .controller('TreatyTransactionCashFinanceDialogController', TreatyTransactionCashFinanceDialogController);

  TreatyTransactionCashFinanceDialogController.$inject = ['$scope', '$timeout', '$uibModalInstance', 'Principal', '$stateParams', 'entity',
    'TreatyTransaction', 'ContactCustomSrv', 'Email', '$http', '$q', '$base64'];

  function TreatyTransactionCashFinanceDialogController($scope, $timeout, $uibModalInstance, Principal, $stateParams, entity,
                                                        TreatyTransaction, ContactCustomSrv, Email, $http, $q, $base64) {
    var vm = this;
    vm.treatyTransaction = entity;
    vm.listOfContacts = [];
    vm.selectedContacts = '';
    vm.selectedContactsInfo = [];
    vm.treatyTransaction.sender = '';
    vm.treatyTransaction.emailComment = '';

    vm.toolbar = [
      ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p'],
      ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo'],
      ['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent']
  ];

    vm.spreadsheet = [];

    Principal.identity().then(function (account) {
      vm.treatyTransaction.lastUser = account.login;
      vm.treatyTransaction.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
      vm.lastUser = account.login;

    });

    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };

    vm.newsearch = function (searchString) {
      if (searchString.length > 1)
        ContactCustomSrv.name.query({
          search: searchString
        }, onSuccess, onError);
    };
    var onSuccess = function (result) {
      vm.listOfContacts = result;
    };
    var onError = function () {
      console.log('ERROR');
    };
    vm.addContact = function (contact) {
      var found = vm.selectedContactsInfo.filter(function (contactINFO) { return contactINFO.id === contact.id; });
      if (found.length === 0)
        vm.selectedContactsInfo.push(contact);
      vm.searchQuery = '';
    };

    vm.removeContact = function ($index) {
      vm.selectedContactsInfo.splice($index, 1);
    };

    vm.sendToFinance = function () {
      angular.forEach(vm.selectedContactsInfo, function (contact) {
        if (vm.selectedContacts !== '') {
          vm.selectedContacts += ',' + contact.email;
        } else {
          vm.selectedContacts = vm.selectedContacts + contact.email;
        }
      });
      vm.treatyTransaction.sender = angular.copy(vm.lastUser);
      vm.treatyTransaction.contacts = vm.selectedContacts;

      //FILE UPLOAD is HAPPENING HERE
      if (vm.fileAttch != null) {
        vm.uploadData(vm.treatyTransaction, vm.fileAttch);
      } else {
        Email.send.email(vm.treatyTransaction, onSendSuccess, onSendError);
      }
    };

    var onSendSuccess = function (result) {
      vm.treatyTransaction.financeNotification = 1;
      TreatyTransaction.update(vm.treatyTransaction, onSuccessUpdate, onErrorUpdate);
    };
    var onSendError = function (result) {
      vm.treatyTransaction.financeNotification = 0;
      TreatyTransaction.update(vm.treatyTransaction, onSuccessUpdate, onErrorUpdate);
    };
    var onSuccessUpdate = function () {
      vm.clear();
    };

    var onErrorUpdate = function () {
      vm.clear();
    };

/////////////////////------------------------------------------------------------------

    vm.fileAttch = null;

    vm.uploadData = function (treatyTransaction, file) {
      var reader = new FileReader();
      reader.readAsBinaryString(file);

      reader.onloadend = function (e) {
        var fileContent = e.target.result;
        //send your binary data via $http or $resource or do anything else with it
        treatyTransaction.fileContent = $base64.encode(fileContent);
        treatyTransaction.fileName = file.name;
        treatyTransaction.fileType = file.type;
        Email.send.email(treatyTransaction, onSendSuccess, onSendError);
      };
    };
  }
})();
