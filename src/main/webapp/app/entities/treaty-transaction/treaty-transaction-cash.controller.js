(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('TreatyTransactionCashController', TreatyTransactionCashController);

  TreatyTransactionCashController.$inject = ['$scope', 'InteractionsService', 'DateUtils', 'orderByFilter', '$state', '$mdDialog', '$stateParams', 'entity', 'TreatyTransaction',
    'Treaty', 'Principal', 'FundingType', 'TreatyTransactionSrv', 'AllocationType', 'XrefTreatyCustomCompany', 'TransactionStatus',
    'CurrencyCustomSrv', 'TreatyTracking', 'TreatyTrackingCustomSvr', 'CompanyCustomSrv', 'Intro'
  ];

  function TreatyTransactionCashController($scope, InteractionsService, DateUtils, orderBy, $state, $mdDialog, $stateParams, entity, TreatyTransaction, Treaty,
    Principal, FundingType, TreatyTransactionSrv, AllocationType, XrefTreatyCustomCompany, TransactionStatus,
    CurrencyCustomSrv, TreatyTracking, TreatyTrackingCustomSvr, CompanyCustomSrv, Intro) {
    var vm = this;
    vm.treatyTransaction = entity;
    vm.treatyId = $stateParams.id;
    vm.dealId = $stateParams.dealId;
    vm.isCollapsed = false;
    vm.legalEntityOptions = [];
    vm.selectedCompany = null;
    vm.companies = [];
    vm.progressBarSaving = 0;
    vm.isCashOut = false;
    vm.flagDirty = 0;
    vm.saveflag = 0;
    vm.fundingTypeOptionsActual = [];
    vm.currencyOptions = [];
    vm.transactionStatusNoExpected = [];
    vm.allocationTypes = [];
    vm.defaultAllocation = '';
    vm.searchStartDate = null;
    vm.searchEndDate = null;
    vm.datePickerOpenStatus = {};
    vm.datePickerOpenStatus.searchStartDate = false;
    vm.datePickerOpenStatus.searchEndDate = false;
    vm.rgaCompaniesCheck = [];
    vm.defaultCompanyObject = null;
    vm.treatyCompaniesCheck = [];
    vm.format = "yyyy-MM";
    vm.expand = 1;
    vm.dateOptions = DATE_MONTH_FORMAT;
    vm.saving = true;
    vm.cashCount = 0;
    vm.listOfTrackingIds = [];
    vm.estimated = 'Estimated';
    vm.actual = 'Actual';
    vm.expected = 'Expected';

    Intro.setupIntroSteps({
      id: 170
    })
    .then(function (result) {
      vm.introOptions = {
        steps: result
      };
    });

    $(document).on('keyup keypress', 'form input[type="text"]', function (e) {
      if (e.keyCode == 13) {
        e.preventDefault();
        return false;
      }
    });


    vm.openCalendar = function (date) {
      vm.datePickerOpenStatus[date] = true;
    };

    Principal.identity().then(function (account) {
      angular.forEach(account.authorities, function (authoritie) {
        if (authoritie === "ROLE_APP_FINRE_OPS_ADMIN") {
          vm.authority = 'Admin';
        } else {
          if (authoritie === "ROLE_APP_FINRE_VIEW_ONLY") {
            vm.authority = 'ViewOnly'
          } else {
            if (vm.authority !== 'Admin' && vm.authority !== 'ViewOnly') {
              vm.authority = 'User';
            }
          }
        }
      });
      vm.treatyTransaction.lastUser = account.login;
      vm.treatyTransaction.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
      vm.lastUser = account.login;
      vm.firstName = account.firstName;
    });

    FundingType.query().$promise.then(function (result) {
      vm.fundingTypeOptions = result;
      angular.forEach(result, function (Type) {
        if (Type.name !== 'N/A') {
          vm.fundingTypeOptionsActual.push(Type);
        } else {
          vm.fundingTypeNa = Type;
        }
      });
    })
    .then(function(){
    //  vm.fundingTypeOptionsActual = [{id: 5, name: "N/A", description: "N/A"}].concat(vm.fundingTypeOptionsActual);
      vm.fundingTypeFilter =  [{id: 5, name: "N/A", description: "N/A"}].concat(vm.fundingTypeOptionsActual);
      vm.fundingTypeOptionsFilter =  [{id: 5, name: "N/A", description: "N/A"}].concat(vm.fundingTypeOptionsActual);
    });

    TreatyTrackingCustomSvr.count.get({
      treatyId: vm.treatyId
    }).$promise.then(function (result) {
      vm.trackingCount = result.amount;
    });


    CompanyCustomSrv.rgaCompanies.query(function (rgaResult) {
      vm.rgaCompaniesCheck = rgaResult;
      angular.forEach(rgaResult, function (rgacompany, key) {
        vm.legalEntityOptions.push(rgacompany);
      });
      XrefTreatyCustomCompany.treaties.query({
        id: vm.treatyId
      }, function (result) {
        vm.treatyCompaniesCheck = result;
        angular.forEach(result, function (company, key) {
          var companyName = "* " + company.company.name;
          company.company.name = companyName;
          vm.legalEntityOptions.push(company.company);
        });

      });
    });

    Treaty.get({
      id: $stateParams.id
    }).$promise.then(function (result) {
      angular.forEach(result.mdmAttributes, function (mdm, key) {
        if (mdm.ekComment === null) {
          mdm.ekComment = '';
        }
      });
      angular.forEach(result.treatyCurrencies, function (currency, key) {
        vm.currencyOptions.push(currency.currency);
      });
      vm.currencyFilter = vm.currencyOptions;
      vm.treatyTransaction.treaty = result;

      AllocationType.query().$promise.then(function (result) {
        angular.forEach(result, function (allocation) {
          if (vm.treatyTransaction.treaty.deal.productLine.name === 'FinRe' && allocation.name === 'FinRe Fee') {
            vm.defaultAllocation = allocation;
          }
          if (vm.treatyTransaction.treaty.deal.productLine.name === 'Alt Fee' && allocation.name === 'Alt Fee') {
            vm.defaultAllocation = allocation;
          }

          if (allocation.name !== "Cash Surplus Relief - Alt Fee" && allocation.name !== "Cash Surplus Relief - FinRe") {
            vm.allocationTypes.push(allocation);
          }
        });

      })
      .then(function(){
        vm.allocationTypeFilter = vm.allocationTypes;
      });
      XrefTreatyCustomCompany.treaties.query({
        id: $stateParams.id
      }).$promise.then(function (result) {
        if (result.length !== null && result.length > 0) {
          if (vm.treatyTransaction.treaty.treatyStatus.name === 'Pending' || vm.treatyTransaction.treaty.mdmAttributes.length === 0) {
            vm.getStatus(0);
          } else {
            if (vm.treatyTransaction.treaty.agreementType.name === 'Retro – Internal') {
              vm.getStatus(2)
            } else {
              vm.getStatus(1);
            }
          }
        } else {
          vm.getStatus(0);
        }
      });



    });



    vm.getStatus = function (param) {
      vm.transactionStatuses = [];
      TransactionStatus.query().$promise.then(function (result) {
        switch (param) {
          case 1:
            vm.transactionStatuses = result;
            break;
          case 0:
            vm.transactionStatuses = [result[2]];
            break;
          case 2:
            angular.forEach(result, function (status) {
              if (status.name !== 'Expected') {
                vm.transactionStatuses.push(status);
              }
            });
            break;
          default:
        }
      })
      .then(function(){
        vm.transactionStatusFilter = vm.transactionStatuses;
      });
    };

    vm.reorder = function () {
      angular.forEach(vm.mainTransactionList, function (transaction, key) {
        transaction.subTransactionsMin = null;
        angular.forEach(transaction.subTransactions, function (subTransaction) {
          subTransaction.isNew = false;
          if (transaction.subTransactionsMin === null || subTransaction.id === null || subTransaction.id > transaction.subTransactionsMin)
            transaction.subTransactionsMin = (subTransaction.id !== null) ? subTransaction.id : Infinity;
        });
      });
      vm.mainTransactionList = orderBy(vm.mainTransactionList, 'subTransactionsMin', true);
    };

    vm.loadAll = function () {
      vm.mainTransactionList = [];
      vm.listOfTrackingIds = [];
      TreatyTransactionSrv.transactions.get({
        treatyId: vm.treatyId
      }, function (result) {
        angular.forEach(result, function (transaction, key) {
          vm.changeStatusFunding(transaction, 5);
          transaction.isCollapsed = true;
          vm.mainTransactionList.push(transaction);
        });
      });
    };

    vm.loadAll();



    var onError = function (result) {};

    var onSaveSuccessDateTracking = function (result) {

    };
    var onSaveSuccess = function (resultTransaction) {

      if (resultTransaction.transactionStatus.name === 'Actual' && resultTransaction.treaty.agreementType.name !== 'Retro – Internal') {
        angular.forEach(resultTransaction.subTransactions, function (subTransactions, key) {
          TreatyTracking.get({
            id: subTransactions.tracking.id
          }).$promise.then(function (resultTracking) {
            var tracking = resultTracking;
            TreatyTransactionSrv.subTransactionTracking.get({
              trackingId: subTransactions.tracking.id
            }).$promise.then(function (result) {

              if (result.length >= 1) {
                var currentSettlement = null;
                angular.forEach(result, function (subByTracking, key) {
                  if (moment(subByTracking.reportingDate).isSame(tracking.reportingMonthYear)) {
                    if (currentSettlement === null) {
                      currentSettlement = subByTracking.settlementDate;
                    } else {
                      if (moment(currentSettlement).isBefore(subByTracking.settlementDate)) {
                        currentSettlement = subByTracking.settlementDate;
                      }
                    }
                  }
                });


                if (currentSettlement === null) {
                  tracking.cashSentRecDate = null;
                  tracking.brokerFeeDueDate = null;
                  if (tracking.cashDueFlag !== 'No') {
                    tracking.trackingStatus = 'Open';
                  }
                } else {
                  if (vm.treatyTransaction.treaty.stmntDueAsmBroker !== null && vm.treatyTransaction.treaty.stmntDueAsmBroker !== undefined) {
                    if (vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays !== null && vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays !== undefined) {
                      tracking.brokerFeeDueDate = (moment(currentSettlement).add(vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays, 'days'));
                    }
                  } else {
                    tracking.brokerFeeDueDate = null;
                  }
                  tracking.cashSentRecDate = currentSettlement;
                }
                TreatyTracking.update(tracking, onSaveSuccessDateTracking, onSaveError);
              }

            });

            vm.cashCount++;

          });
        });
      } else {
        vm.cashCount++;

      }


    };

    var onSaveError = function () {
      vm.isSaving = false;
      vm.saving = true;
    };

    $scope.$watch('vm.cashCount', function () {
      if (vm.mainTransactionList.length === vm.cashCount) {
        vm.saving = true;
        vm.cashCount = 0;

        angular.forEach(vm.listOfTrackingIds, function (track, key) {
          TreatyTracking.get({
            id: track.trackingId
          }).$promise.then(function (resultTracking) {
            var tracking = resultTracking;
            TreatyTransactionSrv.subTransactionTracking.get({
              trackingId: track.trackingId
            }).$promise.then(function (result) {
              if (result.length >= 1) {
                var currentSettlement = null;
                angular.forEach(result, function (subtransaction, key) {
                  if (moment(subtransaction.reportingDate).isSame(tracking.reportingMonthYear)) {
                    if (currentSettlement === null) {
                      currentSettlement = subtransaction.settlementDate;
                    } else {
                      if (moment(currentSettlement).isBefore(subtransaction.settlementDate)) {
                        currentSettlement = subtransaction.settlementDate;
                      }
                    }
                  }
                });

                if (currentSettlement === null) {
                  tracking.cashSentRecDate = null;
                  tracking.brokerFeeDueDate = null;
                  if (tracking.cashDueFlag !== 'No') {
                    tracking.trackingStatus = 'Open';
                  }
                  TreatyTracking.update(tracking, onSaveSuccessDateTracking, onSaveError);
                } else {
                  if (vm.treatyTransaction.treaty.stmntDueAsmBroker !== null && vm.treatyTransaction.treaty.stmntDueAsmBroker !== undefined) {
                    if (vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays !== null && vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays !== undefined) {
                      tracking.brokerFeeDueDate = (moment(currentSettlement).add(vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays, 'days'));
                    }
                  } else {
                    tracking.brokerFeeDueDate = null;
                  }
                  tracking.cashSentRecDate = currentSettlement;

                  TreatyTracking.update(tracking, onSaveSuccessDateTracking, onSaveError);
                }
              }

              if (result.length === 0) {
                tracking.cashSentRecDate = null;
                tracking.brokerFeeDueDate = null;
                if (tracking.cashDueFlag !== 'No') {
                  tracking.trackingStatus = 'Open';
                }

                TreatyTracking.update(tracking, onSaveSuccessDateTracking, onSaveError);

              }
            });

          })
          ///////////////////////////

        })

      }
      if (vm.cashCount === 0) {
        vm.listOfTrackingIds = [];
      }
    });



    vm.save = function (form) {
      vm.saving = false;
      var isEqual = true;
      var isEqualConverted = true;
      //Check to see if transaction equals subtransactions
      angular.forEach(vm.mainTransactionList, function (transaction, key) {
        var total = 0;
        var totalConvert = 0;
        var convertedNetTotal = transaction.convertedNetTotal;
        angular.forEach(transaction.subTransactions, function (subTransaction, key) {
          total = total + subTransaction.amount;
          totalConvert = totalConvert + subTransaction.convertedAmount;

        });
        total = total.toFixed(2);
        totalConvert = totalConvert.toFixed(2);
        convertedNetTotal = convertedNetTotal.toFixed(2);

        if (total * -1 !== transaction.netTotal * -1) {
          isEqual = false;
        }
        if (totalConvert * -1 !== convertedNetTotal * -1) {
          isEqualConverted = false;
        }
      });
      if (isEqual === true && isEqualConverted === true) {

        form.$dirty = false;
        form.$setPristine();
        angular.forEach(vm.mainTransactionList, function (transaction, key) {
          transaction.lastUser = vm.treatyTransaction.lastUser;
          transaction.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
          transaction.isActive = 1;
          if (transaction.originalConvertedAmount.toFixed(2) === transaction.convertedNetTotal.toFixed(2)) {
            transaction.alterConvertedDate = null;
            transaction.alterConveredtUser = null;
          } else {
            transaction.alterConvertedDate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
            transaction.alterConvertedUser = vm.lastUser;
          }


          if (transaction.id != null) {
            transaction.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
            angular.forEach(transaction.subTransactions, function (subTransactions, key) {
              subTransactions.settlementDate = transaction.settlementDate;
              if (subTransactions.id == null) {
                subTransactions.creationTs = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
                subTransactions.creationUser = vm.treatyTransaction.lastUser;
              }
              subTransactions.isActive = 1;
              subTransactions.lastUser = vm.treatyTransaction.lastUser;
              subTransactions.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
            });
            TreatyTransaction.update(transaction, onSaveSuccess, onSaveError);
            transaction.isCollapsed = true;
          } else {
            angular.forEach(transaction.subTransactions, function (subTransactions, key) {
              subTransactions.creationTs = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
              subTransactions.creationUser = vm.treatyTransaction.lastUser;
              subTransactions.lastUser = vm.treatyTransaction.lastUser;
              subTransactions.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
              subTransactions.settlementDate = transaction.settlementDate;
              subTransactions.isActive = 1;
            });
            transaction.creationUser = vm.treatyTransaction.lastUser;
            transaction.creationTs = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
            TreatyTransaction.save(transaction, function (result) {
              onSaveSuccess(result);
              result.accountingDate = DateUtils.convertLocalDateFromServer(result.accountingDate);
              result.settlementDate = DateUtils.convertLocalDateFromServer(result.settlementDate);
              angular.forEach(result.subTransactions, function (subTransactions, key) {
                subTransactions.reportingDate = DateUtils.convertLocalDateFromServer(subTransactions.reportingDate);
              });
              result.isNew = false;
              result.isCollapsed = true;
              vm.mainTransactionList[key] = result;
              vm.mainTransactionList[key].isCollapsed = true;
              vm.changeStatusFunding(vm.mainTransactionList[key], 5);
            }, onSaveError);

          }

        });
      } else {
        $mdDialog
          .show($mdDialog.alert()
            .textContent('You cannot save because the allocations do not add up to the total transaction amount.')
            .disableParentScroll(false)
            .parent(angular.element(document.querySelector('#myModal')))
            .ok('OK'));
      }

      vm.reorder();
    };


    vm.exchangeRate = function (mainTransaction) {
      if (mainTransaction.currency != null && mainTransaction.accountingDate != null) {
        CurrencyCustomSrv.rate.get({
          code: mainTransaction.currency.code,
          year: mainTransaction.accountingDate.getFullYear()
        }).$promise.then(
          function (result) {
            vm.currentExchangeRate = result.exchangeRate;
            mainTransaction.convertedNetTotal = mainTransaction.netTotal * vm.currentExchangeRate;
            mainTransaction.originalConvertedAmount = mainTransaction.netTotal * vm.currentExchangeRate;
            angular.forEach(mainTransaction.subTransactions, function (transaction, key) {
              transaction.convertedAmount = vm.currentExchangeRate * transaction.amount;
            });

          });

        if (mainTransaction.fundingType != null && ( mainTransaction.fundingType.name === "Check-Out")) {
          if (mainTransaction.netTotal >= 0 ) {
            mainTransaction.netTotal = mainTransaction.netTotal * -1;
            mainTransaction.convertedNetTotal = mainTransaction.convertedNetTotal * -1;
          }
        }
      }

    };

    vm.SubTransactionCurrency = function (transaction, mainTransaction) {
      if (mainTransaction.currency != undefined && mainTransaction.accountingDate != undefined) {
        CurrencyCustomSrv.rate.get({
          code: mainTransaction.currency.code,
          year: mainTransaction.accountingDate.getFullYear()
        }).$promise.then(function (result) {
          vm.currentExchangeRate = result.exchangeRate;
          transaction.convertedAmount = vm.currentExchangeRate * transaction.amount;
        });
      }
    };

    vm.openSettlementDate = function (trans) {
      trans.settlementDateOpened = true;
    };

    vm.openAccountingDate = function (trans) {
      trans.accountingDateOpened = true;
    };

    vm.openReportingDate = function (trans) {
      trans.reportingDateOpened = true;
    };

    vm.addSubTransaction = function (Maintransaction) {
      var subtransaction = {
        allocationType: vm.defaultAllocation,
        reportingDate: null,
        amount: null,
        treaty: vm.treatyTransaction.treaty,
        deal: vm.treatyTransaction.treaty.deal,
        isNew: true
      };

      Maintransaction.subTransactions.push(subtransaction);
    };
    vm.removeSubTransaction = function (Maintransaction, $index) {
      Maintransaction.subTransactions.splice($index, 1);
    };


    vm.removeTransaction = function ($index) {
      if (vm.firstName !== vm.treatyTransaction.treaty.deal.gfsAssignee.firstName && vm.authority !== 'Admin') {
        var alert = InteractionsService.alert("I'm sorry " + vm.firstName + ", you are not allowed to delete " + vm.treatyTransaction.treaty.deal.gfsAssignee.firstName + "'s transactions.", '#myModal');
        InteractionsService.showThenDialog(alert);
      } else {
        var cashTransaction = vm.mainTransactionList[$index];
        var confirm = $mdDialog.confirm()
          .textContent('Are you sure you want to delete this transaction?')
          .ok('Yes')
          .cancel('No')
          .parent(angular.element(document.querySelector('#myModal')));

        var confirmChangeActual = $mdDialog.prompt()
          .title('Why are you deleting this transaction? ')
          .textContent('Reason is Required to delete.')
          .ariaLabel('Ny ')
          .ok('Delete.')
          .required(true)
          .cancel('Cancel.')
          .parent(angular.element(document.querySelector('#myModal')));
        var confirmChange = $mdDialog.alert()
          .textContent('Deleted.')
          .ok('OK')
          .parent(angular.element(document.querySelector('#myModal')));

        var trackingid = [];
        if (cashTransaction.id != null) {
          $mdDialog.show(confirm).then(function () {
            cashTransaction.isActive = 0;
            cashTransaction.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
            cashTransaction.lastUser = vm.treatyTransaction.lastUser;
            angular.forEach(cashTransaction.subTransactions, function (subTransactions) {
              if (subTransactions.tracking) {
                var tracking;
                TreatyTracking.get({
                  id: subTransactions.tracking.id
                }).$promise.then(function (result) {
                  tracking = result;
                  TreatyTransactionSrv.subTransactionTracking.get({
                    trackingId: subTransactions.tracking.id
                  }).$promise.then(function (result) {
                    if (result.length >= 1) {
                      var currentSettlement = null;
                      var countTracking = 0;
                      angular.forEach(result, function (subtransaction, key) {
                        if (subTransactions.id !== subtransaction.id && (moment(subtransaction.reportingDate).isSame(tracking.reportingMonthYear))) {
                          if (currentSettlement === null) {
                            currentSettlement = subtransaction.settlementDate;
                          } else {
                            if (subTransactions.id !== subtransaction.id && (moment(currentSettlement).isBefore(subtransaction.settlementDate))) {
                              currentSettlement = subtransaction.settlementDate;
                            }
                          }
                        }
                        countTracking = key;

                      });

                      if (result.length !== 1 && (currentSettlement === null && countTracking === result.length - 1)) {
                        trackingid.push(tracking.id);
                      } else {
                        if (vm.treatyTransaction.treaty.stmntDueAsmBroker !== null && vm.treatyTransaction.treaty.stmntDueAsmBroker !== undefined) {
                          if (vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays !== null && vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays !== undefined) {
                            tracking.brokerFeeDueDate = (moment(currentSettlement).add(vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays, 'days'));
                          }
                        } else {
                          tracking.brokerFeeDueDate = null;
                        }
                        tracking.cashSentRecDate = currentSettlement;
                        TreatyTracking.update(tracking);
                      }
                    }
                  });
                })
              }

              subTransactions.isActive = 0;
              subTransactions.lastUser = vm.treatyTransaction.lastUser;
              subTransactions.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';

            });

            var confirmDeleteChange;
            if (cashTransaction.transactionStatus.name === 'Actual' && cashTransaction.id != null) {
              confirmDeleteChange = confirmChangeActual;
            } else {
              confirmDeleteChange = confirmChange;
            }


            $mdDialog.show(confirmDeleteChange).then(function (result) {
              if (cashTransaction.transactionStatus.name === 'Actual') {
                cashTransaction.deleteReason = result;
                cashTransaction.deleteNotification = 1;
              }
              if (cashTransaction.id === null) {} else {
                TreatyTransaction.update(cashTransaction, function () {
                  angular.forEach(trackingid, function (id, key) {
                    TreatyTracking.get({
                      id: id
                    }).$promise.then(function (result) {
                      if (result.cashDueFlag !== 'No') {
                        result.trackingStatus = 'Open';
                      }
                      result.cashSentRecDate = null;
                      result.brokerFeeDueDate = null;
                      TreatyTracking.update(result);
                    });
                  });


                  angular.forEach(vm.listOfTrackingIds, function (track, key) {
                    TreatyTracking.get({
                      id: track.trackingId
                    }).$promise.then(function (resultTracking) {
                      var tracking = resultTracking;
                      TreatyTransactionSrv.subTransactionTracking.get({
                        trackingId: track.trackingId
                      }).$promise.then(function (result) {
                        if (result.length >= 1) {
                          var currentSettlement = null;
                          angular.forEach(result, function (subtransaction, key) {
                            if (moment(subtransaction.reportingDate).isSame(tracking.reportingMonthYear)) {
                              if (currentSettlement === null) {
                                currentSettlement = subtransaction.settlementDate;
                              } else {
                                if (moment(currentSettlement).isBefore(subtransaction.settlementDate)) {
                                  currentSettlement = subtransaction.settlementDate;
                                }
                              }
                            }
                          });

                          if (currentSettlement === null) {
                            tracking.cashSentRecDate = null;
                            tracking.brokerFeeDueDate = null;
                            if (tracking.cashDueFlag !== 'No') {
                              tracking.trackingStatus = 'Open';
                            }
                            TreatyTracking.update(tracking, onSaveSuccessDateTracking, onSaveError);
                          } else {
                            if (vm.treatyTransaction.treaty.stmntDueAsmBroker !== null && vm.treatyTransaction.treaty.stmntDueAsmBroker !== undefined) {
                              if (vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays !== null && vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays !== undefined) {
                                tracking.brokerFeeDueDate = (moment(currentSettlement).add(vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays, 'days'));
                              }
                            } else {
                              tracking.brokerFeeDueDate = null;
                            }
                            tracking.cashSentRecDate = currentSettlement;

                            TreatyTracking.update(tracking, onSaveSuccessDateTracking, onSaveError);
                          }
                        }

                        if (result.length === 0) {
                          tracking.cashSentRecDate = null;
                          tracking.brokerFeeDueDate = null;
                          if (tracking.cashDueFlag !== 'No') {
                            tracking.trackingStatus = 'Open';
                          }

                          TreatyTracking.update(tracking, onSaveSuccessDateTracking, onSaveError);

                        }
                      });

                    })
                    ///////////////////////////

                  })


                }, onSaveError);
              }
              vm.mainTransactionList.splice($index, 1);
            });
          });
        } else {
          vm.mainTransactionList.splice($index, 1);
        }
      }
    };

    vm.mainTransactionAdd = function () {
      if (vm.treatyTransaction.treaty.treatyStatus.name === 'Pending') {
        var pendingTreaty = $mdDialog.confirm()
          .title('Treaty Status is Pending ')
          .textContent("You can only add Estimates on this treaty.")
          .ok('Ok')
          .cancel('Add Estimates Only')
          .parent(angular.element(document.querySelector('#myModal')));
        $mdDialog.show(pendingTreaty).then(function () {
          console.log('ok');
        }, function () {
          vm.addMain = function () {
            var mainTransactionItem = {
              id: null,
              mixTransactions: 0,
              transactionStatus: {
                id: 2,
                name: "Estimated",
                description: "Estimated"
              },
              treaty: vm.treatyTransaction.treaty,
              company: vm.defaultCompany(),
              legalEntityOptions: vm.legalEntityOptions,
              settlementDate: vm.treatyTransaction.settlementDate,
              accountingDate: vm.treatyTransaction.accountingDate,
              fundingTypeOptions: vm.fundingTypeOptions,
              fundingType: vm.fundingTypeNa,
              currency: vm.treatyTransaction.currency,
              netTotal: vm.treatyTransaction.netTotal,
              subTransactions: [{
                allocationType: vm.defaultAllocation,
                reportingDate: '',
                amount: '',
                treaty: vm.treatyTransaction.treaty,
                deal: vm.treatyTransaction.treaty.deal,
                isNew: true

              }],
              currencyConvertedFlag: 0,
              convertedNetTotal: 0,
              isNew: true
            };

            vm.mainTransactionList.unshift(mainTransactionItem);

          };
          XrefTreatyCustomCompany.treaties.query({
            id: $stateParams.id
          }, function (result) {
            vm.addMain();
          });

        });

      } else {

        vm.addMain = function () {
          var mainTransactionItem = {
            id: null,
            mixTransactions: 0,
            transactionStatus: {
              id: 2,
              name: "Estimated",
              description: "Estimated"
            },
            treaty: vm.treatyTransaction.treaty,
            settlementDate: vm.treatyTransaction.settlementDate,
            company: vm.defaultCompany(),
            legalEntityOptions: vm.legalEntityOptions,
            accountingDate: vm.treatyTransaction.accountingDate,
            fundingTypeOptions: vm.fundingTypeOptions,
            fundingType: vm.fundingTypeNa,
            currency: vm.treatyTransaction.currency,
            netTotal: vm.treatyTransaction.netTotal,
            subTransactions: [{
              allocationType: vm.defaultAllocation,
              reportingDate: '',
              amount: '',
              treaty: vm.treatyTransaction.treaty,
              deal: vm.treatyTransaction.treaty.deal,
              isNew: true
            }],
            currencyConvertedFlag: 0,
            convertedNetTotal: 0,
            isNew: true
          };

          vm.mainTransactionList.unshift(mainTransactionItem);

        };
        XrefTreatyCustomCompany.treaties.query({
          id: $stateParams.id
        }, function (result) {

          if (result.length !== null && result.length > 0) {
            vm.addMain();
          } else {

            var confirm = $mdDialog.confirm()
              .title('Treaty has no Legal Entities ')
              .textContent("You can only add Estimates on this treaty because it has no associated legal entities/companies.")
              .ok('Add Companies To Treaty')
              .cancel('Add Estimates Only')
              .parent(angular.element(document.querySelector('#myModal')));
            $mdDialog.show(confirm).then(function () {
              $state.go('contacts-company-detail', {
                id: $stateParams.id
              })
            }, function () {
              vm.legalEntityNumber = 0;
              vm.addMain();
            });

          }
        });
      }
    };

    vm.copyTransaction = function ($index) {
      vm.copiedTransaction = {
        id: null,
        transactionStatus: 'Estimated',
        mixTransactions: 0,
        treaty: vm.treatyTransaction.treaty,
        settlementDate: vm.treatyTransaction.settlementDate,
        company: null,
        legalEntityOptions: vm.legalEntityOptions,
        accountingDate: vm.treatyTransaction.accountingDate,
        fundingType: {
          description: "N/A",
          id: 3,
          name: "N/A"
        },
        currency: vm.treatyTransaction.currency,
        netTotal: vm.treatyTransaction.netTotal,
        subTransactions: [{
          allocationType: vm.defaultAllocation,
          reportingDate: '',
          amount: '',
          treaty: vm.treatyTransaction.treaty
        }],
        currencyConvertedFlag: 0,
        convertedNetTotal: 0
      };
      angular.copy(vm.mainTransactionList[$index], vm.copiedTransaction);
      //Must set the id to null
      vm.copiedTransaction.id = null;
      vm.copiedTransaction.isNew = true;
      angular.forEach(vm.copiedTransaction.subTransactions, function (subtransaction, key) {
        subtransaction.id = null;
        subtransaction.isNew = true;
      });
      vm.mainTransactionList.unshift(vm.copiedTransaction);
    };

    vm.changeStatusFunding = function (mainTransaction, param) {
      switch (mainTransaction.transactionStatus.name) {
        case 'Estimated':
          mainTransaction.fundingTypeOptions = vm.fundingTypeOptions;
          mainTransaction.fundingType = vm.fundingTypeNa;
          if (param === 1 || param === 0) {
            mainTransaction.settlementDate = null;
          }
          angular.forEach(mainTransaction.subTransactions, function (subTransactions, key) {
            subTransactions.tracking = null;
          });
          break;
        case 'Actual':
          angular.forEach(mainTransaction.subTransactions, function (subTransactions, key) {
            vm.trackingByPeriod(subTransactions, mainTransaction);
          });
          if (mainTransaction.treaty.agreementType.name === 'Retro – Internal' &&
            mainTransaction.treaty.treatyStatus.name !== 'Pending' &&
            mainTransaction.treaty.treatyStatus.name !== 'Terminated') {
            mainTransaction.fundingTypeOptions = vm.fundingTypeOptions;
            mainTransaction.fundingType = vm.fundingTypeNa;

          } else {
            if (param === 1) {
              mainTransaction.fundingType = null;
            }
            mainTransaction.fundingTypeOptions = vm.fundingTypeOptionsActual;
          }

          break;

        case 'Expected':

          if (param === 1) {
            mainTransaction.fundingType = null;
          }
          mainTransaction.fundingTypeOptions = vm.fundingTypeOptionsActual;
          angular.forEach(mainTransaction.subTransactions, function (subTransactions, key) {
            subTransactions.tracking = null;
          });
          break;
        default:
      }


    };

    vm.checkFundingType = function (transaction) {
      if (transaction.fundingType !== null && transaction.fundingType !== undefined) {
        if (transaction.fundingType.name === "Wire-Out" || transaction.fundingType.name === "Check-Out") {
            if (transaction.fundingType.name !== "Wire-Out" ){
              var  msg ='Since Funding Type indicates outbound funds, the Net Total must be less than zero.';
          }else{
            var msg ='Please consider that the Net Total for Wire-Out (outbound funds) should be entered as a negative number.';
          }
          $mdDialog
            .show($mdDialog.alert()
              .textContent(msg)
              .disableParentScroll(false)
              .parent(angular.element(document.querySelector('#myModal')))
              .ok('OK'));

          if (transaction.netTotal > 0 && transaction.fundingType.name !== "Wire-Out" ) {
            transaction.netTotal = transaction.netTotal * -1;
            transaction.convertedNetTotal = transaction.convertedNetTotal * -1;
          }
      
          
        } else {
          if (transaction.fundingType.name === 'Accrual' || transaction.fundingType.name==='Term Adj') {
            transaction.subTransactions.forEach(function (sub) {
              sub.tracking = null;
            })
          }
          if (transaction.netTotal < 0 ) {
            transaction.netTotal = transaction.netTotal * -1;
            transaction.convertedNetTotal = transaction.convertedNetTotal * -1;
          }

        }
      }
    };

    vm.reverse = true;
    vm.sortBy = function (propertyName) {
      vm.reverse = (propertyName !== null) ? !vm.reverse : false;
      vm.propertyName = propertyName;
      vm.mainTransactionList = orderBy(vm.mainTransactionList, propertyName, vm.reverse);
    };

    vm.toggleFilter = function () {
      vm.filterStatus = !vm.filterStatus;
    };

    vm.filter = function (editform) {
      vm.filteredData = true;
      editform.$dirty = false;
      vm.loadAllCash()
      .then(function () {
        vm.mainTransactionList = vm.mainTransactionList
        .filter(function(transaction){ // Reporting Period Filter
          if (vm.searchStartDate && vm.searchEndDate) {
            return transaction.subTransactions.some(function(sub){
              return moment(sub.reportingDate).isBetween(vm.searchStartDate, vm.searchEndDate, null, '[]')
            });
          }
          return true;
        })
        .filter(function(transaction){ // Transaction Status Filter
          return vm.transactionStatusFilter
          .some(function(ts){ return ts.name === transaction.transactionStatus.name; })
        })
        .filter(function(transaction){
          return vm.fundingTypeFilter
          .some(function(ft){ return ft.name === transaction.fundingType.name; })
        })
        .filter(function(transaction){
          return vm.currencyFilter
          .some(function(c){ return c.code === transaction.currency.code; })
        })
        .filter(function(transaction) {
          return vm.allocationTypeFilter
          .some(function(at){
            return transaction.subTransactions.some(function(sub){
              return sub.allocationType.name === at.name;
            })
          })
        });
      });
    };

    vm.loadAllCash = function () {
      vm.mainTransactionList = [];
      vm.listOfTrackingIds = [];
      vm.flagDirty = 0;
      return TreatyTransactionSrv.transactions.get({
        treatyId: $stateParams.id
      }, function (result) {
        angular.forEach(result, function (transaction, key) {
          vm.changeStatusFunding(transaction, 5);
          transaction.isCollapsed = true;
          vm.mainTransactionList.push(transaction);

        });
      }).$promise;
    };

    vm.reset = function (param, editform) {
      vm.filteredData = false;
      vm.listOfTrackingIds = [];
      editform.$dirty = false;
      vm.mainTransactionList = [];
      vm.estimated = 'Estimated';
      vm.actual = 'Actual';
      vm.expected = 'Expected';
      if (param === 1) {
        vm.searchStartDate = null;
        vm.searchEndDate = null;
      }

      TreatyTransactionSrv.transactions.get({
        treatyId: $stateParams.id
      }, function (result) {
        angular.forEach(result, function (transaction, key) {
          vm.changeStatusFunding(transaction, 5);
          transaction.isCollapsed = true;
          vm.mainTransactionList.push(transaction);
        });
      });
    };

    vm.collapse = function (param) {
      if (param === 1) {
        angular.forEach(vm.mainTransactionList, function (transaction) {
          transaction.isCollapsed = false;
        });
        vm.expand = 0;
      } else {
        angular.forEach(vm.mainTransactionList, function (transaction) {
          transaction.isCollapsed = true;
        });
        vm.expand = 1;
      }
    };

    // we will use this function for Validate Selected RGA Entity on Transaction Against Selected EK on Sub-Transaction
    vm.checkCompanyEk = function (mainTransaction, transaction) {
      if (mainTransaction.company != null && transaction.mdm != null) {
        if (mainTransaction.company.legalCode !== transaction.mdm.legalCode) {
          var confirm = $mdDialog.alert()
            .textContent("This EK does not roll up to the selected Legal Entity.")
            .ok('OK')
            .parent(angular.element(document.querySelector('#myModal')));
          $mdDialog.show(confirm).then(function () {});
        }
      }
    };
    vm.updateLegalEntity = function (mainTransaction) {
      angular.forEach(mainTransaction.subTransactions, function (transaction) {
        transaction.mdm = null;
      })
    };
    // default rga company from treaty companies

    vm.defaultCompany = function () {
      angular.forEach(vm.treatyCompaniesCheck, function (treatyCompany) {
        angular.forEach(vm.rgaCompaniesCheck, function (rgaCompany) {
          if (vm.defaultCompanyObject === null && treatyCompany.company.clientCompanyId === rgaCompany.clientCompanyId) {
            vm.defaultCompanyObject = treatyCompany.company;
          }
        })
      });
      return vm.defaultCompanyObject;
    };

    vm.trackingByPeriod = function (transaction, mainTransaction) {
      if (mainTransaction.transactionStatus.name === 'Actual' &&
        mainTransaction.currency != null &&
        mainTransaction.treaty.agreementType.name !== 'Retro – Internal' &&
        transaction.reportingDate != null &&
        transaction.reportingDate !== ""
      ) {
        transaction.trackingList = [];
        TreatyTrackingCustomSvr.TrackingByPeriod.get({
            treatyId: vm.treatyId,
            startYear: moment(transaction.reportingDate).get('year'),
            startMonth: moment(transaction.reportingDate).get('month') + 1,
            startDay: moment(transaction.reportingDate).get('date')
          },
          function (result) {
            angular.forEach(result, function (track) {
              if (track.currency != null) {
                if (track.currency.code === mainTransaction.currency.code) {
                  transaction.trackingList.push(track);
                }
              }
            });
            if (result.length === 0 && !(mainTransaction.transactionStatus && mainTransaction.transactionStatus.name == 'Actual' && mainTransaction.fundingType && (mainTransaction.fundingType.name == 'Accrual' || transaction.fundingType.name==='Term Adj'))) {
              var confirm = $mdDialog.alert()
                .textContent("There is no tracking record for this Reporting Period, therefore, you cannot save an 'Actual' transaction for this period.")
                .ok('OK')
                .disableParentScroll(false)
                .parent(angular.element(document.querySelector('#myModal')));

              $mdDialog.show(confirm).then(function () {});
            }

          }, onError);
      } else {
        transaction.trackingList = [];
      }
    };



    vm.changeSubTracking = function (tracking, id, subId) {
      var flag = false;
      var index;
      angular.forEach(vm.listOfTrackingIds, function (track, key) {
        if (track.trackingId === parseInt(id)) {
          flag = true;
          index = key;
        }
      });
      if (id !== '' && !flag) {
        vm.listOfTrackingIds.push({
          trackingId: parseInt(id),
          subTransactionId: parseInt(subId)
        });
      }
      if (id === '' && flag) {
        vm.listOfTrackingIds.splice(index, 1);
      }

    };




  }
})();
