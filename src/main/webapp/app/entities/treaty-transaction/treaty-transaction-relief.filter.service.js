(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('TreatyTransactionReliefFilter', TreatyTransactionReliefFilter);

  TreatyTransactionReliefFilter.$inject = ['$resource', 'DateUtils'];

  function TreatyTransactionReliefFilter($resource, DateUtils) {

    var sortReporting='api/treaty-transaction-reliefs/ordered-by-id-desc/deals/:dealId/dates/:startYear/:startMonth/:startDay/:endYear/:endMonth/:endDay';
    return{
      reportingReport:$resource(sortReporting, {}, {
        'query': { method: 'GET', params: {}, isArray: true },
        'get': {
          method: 'GET',isArray: true,
          transformResponse: function (data) {
          data = angular.fromJson(data);
            angular.forEach(data,function(trans,key){
              trans.reportingDate = DateUtils.convertLocalDateFromServer(trans.reportingDate);
            });
            return data;
          }
        }
      })
    }}
})();
