(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('TreatyTransaction', TreatyTransaction);

  TreatyTransaction.$inject = ['$resource', 'DateUtils'];

  function TreatyTransaction($resource, DateUtils) {
    var resourceUrl = 'api/treaty-transaction-cash/:id';

    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          data.settlementDate = DateUtils.convertLocalDateFromServer(data.settlementDate);
          data.accountingDate = DateUtils.convertLocalDateFromServer(data.accountingDate);

          return data;
        }
      },
      'update': {
        method: 'PUT',
        transformRequest: function (data) {
     //   data.settlementDate = DateUtils.convertLocalDateToServer(data.settlementDate);
          return angular.toJson(data);
        }
      },
      'save': {
        method: 'POST',
        transformRequest: function (data) {
    //     data.settlementDate = DateUtils.convertLocalDateToServer(data.settlementDate);
          return angular.toJson(data);
        }
      }
    });
  }
})();
