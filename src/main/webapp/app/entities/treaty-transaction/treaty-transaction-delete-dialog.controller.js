(function () {
  'use strict';
  angular
    .module('finreApp')
    .controller('TreatyTransactionDeleteController', TreatyTransactionDeleteController);
  
  TreatyTransactionDeleteController.$inject = ['$uibModalInstance', 'entity', 'TreatyTransaction'];
  
  function TreatyTransactionDeleteController($uibModalInstance, entity, TreatyTransaction) {
    var vm = this;
    vm.treatyTransaction = entity;
    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };
    vm.confirmDelete = function (id) {
      TreatyTransaction.delete({id: id},
        function () {
          $uibModalInstance.close(true);
        });
    };
  }
})();
