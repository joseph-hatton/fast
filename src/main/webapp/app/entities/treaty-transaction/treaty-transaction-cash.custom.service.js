(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('TreatyTransactionSrv', TreatyTransactionSrv);

  TreatyTransactionSrv.$inject = ['$resource', 'DateUtils'];

  function TreatyTransactionSrv($resource, DateUtils) {

    var resourceUrl = 'api/treaty-transaction-cash/mixSpecific/0/:treatyId';
    //var resourceUrl = 'api/treaty-transaction-cash/treaties/:id';
    var currencyCountUrl='api/treaty-transaction-cash/currency/:treatyId/:currencyCode';
    var companyCountUrl='api/treaty-transaction-cash/company/:treatyId/:companyId';
    var subTransactionTracking='api/treaty-subtransaction-cash/tracking/:trackingId';
    var treatyWithActualTransactions='api/treaty-transaction/Actual/treaties/:treatyId';
    var deletedTransaction='api/treaty-transaction-cash/deleted';
    return{
      transactions:$resource(resourceUrl, {}, {
      'query': {method: 'GET', params: {}, isArray: true},
      'get': {
        method: 'GET', params: {}, isArray: true,
        transformResponse: function (data) {
          data = angular.fromJson(data);
          angular.forEach(data,function (value,key) {
           value.settlementDate = DateUtils.convertLocalDateFromServer(value.settlementDate);
           value.accountingDate = DateUtils.convertLocalDateFromServer(value.accountingDate);
           angular.forEach(value.subTransactions,function (datesub,key) {
             datesub.reportingDate = DateUtils.convertLocalDateFromServer(datesub.reportingDate);
           })
         });
        return data;
        }
      }
    }),
      currencyCount: $resource(currencyCountUrl, {}, {
      query: { method: 'GET', params: {}, isArray: true },
      'get': {
        method: 'GET',isArray: true,
        transformResponse: function (data) {
         data = angular.fromJson(data);
          return data;
        }
      }
    }),
      treatyWithActualTransactions: $resource(treatyWithActualTransactions, {}, {
        query: { method: 'GET', params: {}, isArray: true }
      }),

      deletedTransaction: $resource(deletedTransaction, {}, {
        query: { method: 'GET', params: {}, isArray: true }
      }),
      companyCount: $resource(companyCountUrl, {}, {
        query: { method: 'GET', params: {}, isArray: true },
        'get': {
          method: 'GET',isArray: true,
          transformResponse: function (data) {
             data = angular.fromJson(data);
            return data;
          }
        }
      }),
      subTransactionTracking: $resource(subTransactionTracking, {}, {
        query: { method: 'GET', params: {}, isArray: true },
        'get': {
          method: 'GET',isArray: true,
          transformResponse: function (data) {
            data = angular.fromJson(data);
            return data;
          }
        }
      })

  }}
})();

// /treaty-subtransaction-cash/treaty-transactions-tracking/{treatyId}/{trackingId}
