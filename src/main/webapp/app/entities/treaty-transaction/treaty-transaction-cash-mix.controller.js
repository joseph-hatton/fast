(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('TreatyTransactionCashMixController', TreatyTransactionCashMixController);

  TreatyTransactionCashMixController.$inject = ['$scope', 'InteractionsService', 'orderByFilter', '$state', '$mdDialog', '$stateParams', 'entity', 'TreatySrv', 'TreatyTransaction',
    'Treaty', 'TreatyTransactionSrv', 'TreatyTracking', 'Principal', 'FundingType', 'AllocationType', 'Company', 'TransactionStatus', 'DateUtils', 'TreatyTransactionMixSrv', 'CurrencyCustomSrv', 'CompanyCustomSrv', 'TreatyTrackingCustomSvr', 'Intro', 'XrefTreatyCustomCompany'
  ];

  function TreatyTransactionCashMixController($scope, InteractionsService, orderBy, $state, $mdDialog, $stateParams, entity, TreatySrv, TreatyTransaction, Treaty, TreatyTransactionSrv, TreatyTracking,
    Principal, FundingType, AllocationType, Company, TransactionStatus, DateUtils, TreatyTransactionMixSrv, CurrencyCustomSrv, CompanyCustomSrv, TreatyTrackingCustomSvr, Intro, XrefTreatyCustomCompany) {
    var vm = this;
    vm.treatyTransaction = entity;
    vm.treatyId = $stateParams.id;
    vm.dealId = $stateParams.dealId;
    vm.isCollapsed = false;
    vm.legalEntityOptions = [];
    vm.selectedCompany = null;
    vm.companies = [];
    vm.isCashOut = false;
    vm.flagDirty = 0;
    vm.deals = [];
    vm.saveflag = 0;
    vm.cashCount = 0;
    vm.dealsNotreaties = [];
    vm.fundingTypeOptionsActual = [];
    vm.allocationTypes = [];
    vm.searchStartDate = null;
    vm.searchEndDate = null;
    vm.datePickerOpenStatus = {};
    vm.datePickerOpenStatus.searchStartDate = false;
    vm.datePickerOpenStatus.searchEndDate = false;
    vm.rgaCompaniesCheck = [];
    vm.defaultCompanyObject = null;
    vm.treatyCompaniesCheck = [];
    vm.format = "yyyy-MM";
    vm.expand = 1;
    vm.dateOptions = DATE_MONTH_FORMAT;
    vm.estimated = 'Estimated';
    vm.actual = 'Actual';
    vm.expected = 'Expected';
    vm.listOfTrackingIds = [];

    Intro.setupIntroSteps({
        id: 167
      })
      .then(function (result) {
        vm.introOptions = {
          steps: result
        };
      });

    XrefTreatyCustomCompany.treaties.query({
      id: $stateParams.id
    }, function (result) {
      vm.treatyCompaniesCheck = result;
    });

    TreatySrv.nopages.query().$promise.then(function (result) {
      angular.forEach(result, function (deal, key) {
        if (deal.dealStatus === 'Terminated') {
          deal.disabled = true;
          deal.name = deal.name + ' - Deal Terminated'
        } else {
          deal.disabled = false;
        }
        vm.deals.push(deal);
      });
    });

    $(document).on('keyup keypress', 'form input[type="text"]', function (e) {
      if (e.keyCode == 13) {
        e.preventDefault();
        return false;
      }
    });

    vm.openSettlementDate = function (trans) {
      trans.settlementDateOpened = true;
    };

    vm.openAccountingDate = function (trans) {
      trans.accountingDateOpened = true;
    };

    vm.openReportingDate = function (trans) {
      trans.reportingDateOpened = true;
    };

    TreatyTrackingCustomSvr.count.get({
      treatyId: vm.treatyId
    }).$promise.then(function (result) {
      vm.trackingCount = result.amount;
    });

    vm.openCalendar = function (date) {
      vm.datePickerOpenStatus[date] = true;
    };
    vm.currencyOptions = [];

    Principal.identity().then(function (account) {
      angular.forEach(account.authorities, function (authoritie) {
        if (authoritie === "ROLE_APP_FINRE_OPS_ADMIN") {
          vm.authority = 'Admin';
        } else {
          if (authoritie === "ROLE_APP_FINRE_VIEW_ONLY") {
            vm.authority = 'ViewOnly'
          } else {
            if (vm.authority !== 'Admin' && vm.authority !== 'ViewOnly') {
              vm.authority = 'User';
            }
          }
        }
      });
      vm.firstName = account.firstName;
      vm.treatyTransaction.lastUser = account.login;
    });

    Treaty.get({
      id: $stateParams.id
    }).$promise.then(function (result) {
      vm.treatyTransaction.treaty = result;
      vm.companySub(vm.treatyTransaction.treaty.id);
      angular.forEach(result.treatyCurrencies, function (currency, key) {
        vm.currencyOptions.push(currency.currency);
      });
      vm.currencyFilter = vm.currencyOptions;
      if (result.treatyStatus.name === 'Pending') {
        vm.getStatus(0);
      } else {
        if (result.agreementType.name === 'Retro – Internal') {
          vm.getStatus(2);
        } else {
          vm.getStatus(1);
        }
      }

    });

    vm.reorder = function () {
      angular.forEach(vm.mainTransactionList, function (mixTransaction, key) {
        mixTransaction.subTransactionsMin = null;
        angular.forEach(mixTransaction.subTransactions, function (subTransactionMix) {
          subTransactionMix.isNew = false;
          if (mixTransaction.subTransactionsMin == null || subTransactionMix.id == null || subTransactionMix.id > mixTransaction.subTransactionsMin)
            mixTransaction.subTransactionsMin = (subTransactionMix.id != null) ? subTransactionMix.id : Infinity;
        });
      });
      vm.mainTransactionList = orderBy(vm.mainTransactionList, 'subTransactionsMin', true);
    };


    vm.loadAll = function () {
      vm.listOfTrackingIds = [];
      TreatyTransactionMixSrv.get({
        treatyId: $stateParams.id
      }, function (result) {
        vm.mainTransactionList = [];
        angular.forEach(result, function (mixTransaction, key) {
          vm.changeStatusFunding(mixTransaction, 5);
          angular.forEach(mixTransaction.subTransactions, function (subTransactionMix, key) {
            subTransactionMix.deal = subTransactionMix.treaty.deal;
            vm.changeDeal(subTransactionMix);
            vm.trackingByPeriod(subTransactionMix, mixTransaction);

          });
          mixTransaction.isCollapsed = true;
          vm.mainTransactionList.push(mixTransaction);
        });
        //vm.reorder();
      });
    };

    vm.loadAll();

    vm.getStatus = function (param) {
      vm.transactionStatuses = [];
      TransactionStatus.query().$promise.then(function (result) {
        if (param === 1) {
          vm.transactionStatuses = result;
        }
        if (param === 0) {
          vm.transactionStatuses = [result[2]];
        }
        if (param === 2) {
          angular.forEach(result, function (status) {
            if (status.name !== 'Expected') {
              vm.transactionStatuses.push(status);
            }
          });
        }
      })
      .then(function(){
        vm.transactionStatusFilter = vm.transactionStatuses;
      });
    };

    vm.treatyChange = function (transaction) {
      if (vm.treatyTransaction.treaty !== undefined && vm.treatyTransaction.treaty !== null) {
        transaction.legalEntityOptions = [];
        getXrefCompanies(transaction.legalEntityOptions, vm.treatyTransaction.treaty.id);

      }


    };
    vm.companySub = function (id) {
      vm.companySubList = [];
      getXrefCompanies(vm.companySubList, id);
      return vm.companySubList;

    };


    FundingType.query().$promise.then(function (result) {
      vm.fundingTypeOptions = result;
      angular.forEach(result, function (Type) {
        if (Type.name !== 'N/A') {
          vm.fundingTypeOptionsActual.push(Type);

        } else {
          vm.fundingTypeNa = Type;
        }

      });
    })
    .then(function(){
      //vm.fundingTypeOptionsActual = [{id: 5, name: "N/A", description: "N/A"}].concat(vm.fundingTypeOptionsActual);
     // vm.fundingTypeFilter = vm.fundingTypeOptionsActual;
      vm.fundingTypeFilter =  [{id: 5, name: "N/A", description: "N/A"}].concat(vm.fundingTypeOptionsActual);
      vm.fundingTypeOptionsFilter =  [{id: 5, name: "N/A", description: "N/A"}].concat(vm.fundingTypeOptionsActual);
    });

    AllocationType.query().$promise.then(function (result) {
      angular.forEach(result, function (allocation) {
        if (allocation.name !== "Cash Surplus Relief") {
          vm.allocationTypes.push(allocation);
        }
      });
    })
    .then(function(){
      vm.allocationTypeFilter = vm.allocationTypes;
    });

    var onSaveSuccessDateTracking = function () {


    };

    var onSaveSuccess = function (result) {
      if (result.transactionStatus.name === 'Actual') {
        angular.forEach(result.subTransactions, function (subTransactions, key) {
          var tracking = subTransactions.tracking;
          TreatyTransactionSrv.subTransactionTracking.get({
            trackingId: subTransactions.tracking.id
          }, function (resultSub) {
            if (resultSub.length >= 1) {
              var currentSettlement = null;
              angular.forEach(resultSub, function (subtransaction, key) {
                if (moment(subtransaction.reportingDate).isSame(tracking.reportingMonthYear)) {
                  if (currentSettlement === null) {
                    currentSettlement = subtransaction.settlementDate;
                  } else {
                    if (moment(currentSettlement).isBefore(subtransaction.settlementDate)) {
                      currentSettlement = subtransaction.settlementDate;
                    }
                  }
                }
              });

              if (currentSettlement === null) {
                tracking.cashSentRecDate = null;
                tracking.brokerFeeDueDate = null;
                if (tracking.cashDueFlag !== 'No') {
                  tracking.trackingStatus = 'Open';
                }
              } else {
                if (vm.treatyTransaction.treaty.stmntDueAsmBroker !== null && vm.treatyTransaction.treaty.stmntDueAsmBroker !== undefined) {
                  if (vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays !== null && vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays !== undefined) {
                    tracking.brokerFeeDueDate = (moment(currentSettlement).add(vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays, 'days'));
                  }
                } else {
                  tracking.brokerFeeDueDate = null;
                }
                tracking.cashSentRecDate = currentSettlement;
              }
              TreatyTracking.update(tracking, onSaveSuccessDateTracking, onSaveError);
            }
          });


        });
        vm.cashCount++;
      } else {
        vm.cashCount++;
      }


      $scope.$emit('finreApp:treatyTransactionCashUpdate', result);

      $scope.$watch('vm.cashCount', function () {
        if (vm.mainTransactionList.length === vm.cashCount) {
          vm.saving = true;
          vm.cashCount = 0;
          angular.forEach(vm.listOfTrackingIds, function (track, key) {
            TreatyTracking.get({
              id: track.trackingId
            }).$promise.then(function (resultTracking) {
              var tracking = resultTracking;
              TreatyTransactionSrv.subTransactionTracking.get({
                trackingId: track.trackingId
              }).$promise.then(function (result) {
                if (result.length >= 1) {
                  var currentSettlement = null;
                  angular.forEach(result, function (subtransaction, key) {
                    if (moment(subtransaction.reportingDate).isSame(tracking.reportingMonthYear)) {
                      if (currentSettlement === null) {
                        currentSettlement = subtransaction.settlementDate;
                      } else {
                        if (moment(currentSettlement).isBefore(subtransaction.settlementDate)) {
                          currentSettlement = subtransaction.settlementDate;
                        }
                      }
                    }
                  });

                  if (currentSettlement === null) {
                    tracking.cashSentRecDate = null;
                    tracking.brokerFeeDueDate = null;
                    if (tracking.cashDueFlag !== 'No') {
                      tracking.trackingStatus = 'Open';
                    }
                    TreatyTracking.update(tracking, onSaveSuccessDateTracking, onSaveError);
                  } else {
                    if (vm.treatyTransaction.treaty.stmntDueAsmBroker !== null && vm.treatyTransaction.treaty.stmntDueAsmBroker !== undefined) {
                      if (vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays !== null && vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays !== undefined) {
                        tracking.brokerFeeDueDate = (moment(currentSettlement).add(vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays, 'days'));
                      }
                    } else {
                      tracking.brokerFeeDueDate = null;
                    }
                    tracking.cashSentRecDate = currentSettlement;

                    TreatyTracking.update(tracking, onSaveSuccessDateTracking, onSaveError);
                  }
                }

                if (result.length === 0) {
                  tracking.cashSentRecDate = null;
                  tracking.brokerFeeDueDate = null;
                  if (tracking.cashDueFlag !== 'No') {
                    tracking.trackingStatus = 'Open';
                  }

                  TreatyTracking.update(tracking, onSaveSuccessDateTracking, onSaveError);

                }
              });

            })
            ///////////////////////////

          })

        }
      });


      vm.isSaving = false;
      result.accountingDate = DateUtils.convertLocalDateFromServer(result.accountingDate);
      result.settlementDate = DateUtils.convertLocalDateFromServer(result.settlementDate);
      vm.changeStatusFunding(result, 5);
      if (result.mixTransactions === 1) {
        angular.forEach(result.subTransactions, function (subTransactions, key) {
          if (subTransactions.treaty.id !== vm.treatyTransaction.treaty.id) {
            result.treaty = subTransactions.treaty;
            subTransactions.deal = result.treaty.deal;
            vm.changeDeal(subTransactions);
            var currencySaveFlag = true;
            angular.forEach(subTransactions.treaty.treatyCurrencies, function (Currency, key) {
              if (result.currency.id === Currency.currency.id) {
                currencySaveFlag = false;
              }
            });

            if (currencySaveFlag) {
              var objectCurrency = {
                id: null,
                currency: result.currency
              };
              subTransactions.treaty.treatyCurrencies.push(objectCurrency);
              Treaty.update(subTransactions.treaty, onSaveSuccessCurrencyTreaty, onSaveError);
            }
          } else {
            subTransactions.deal = vm.treatyTransaction.treaty.deal;
            vm.changeDeal(subTransactions);
          }
        });

      }
      angular.forEach(result.subTransactions, function (subTransactions, key) {
        subTransactions.reportingDate = DateUtils.convertLocalDateFromServer(subTransactions.reportingDate);
        subTransactions.deals = subTransactions.treaty.deal;

      });

    };

    function onSaveSuccessCurrencyTreaty() {
      console.log('save success new currency in treaty ');
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    vm.save = function (form) {

      var isEqual = true;
      var isEqualConverted = true;
      var idTreaties = true;
      //Check to see if transaction equals subtransactions
      angular.forEach(vm.mainTransactionList, function (transaction, key) {
        var total = 0;
        var totalConvert = 0;
        var convertedNetTotal = transaction.convertedNetTotal;
        var treatySame = true;
        angular.forEach(transaction.subTransactions, function (subTransaction, key) {
          var subTreaty = subTransaction.treaty.id;
          total = total + subTransaction.amount;
          totalConvert = totalConvert + subTransaction.convertedAmount;
          if (parseInt(vm.treatyId) !== parseInt(subTreaty) && treatySame === true) {
            treatySame = false;
          }
        });

        if (treatySame && idTreaties === true) {
          idTreaties = false;
        }
        total = total.toFixed(2);
        totalConvert = totalConvert.toFixed(2);

        convertedNetTotal = convertedNetTotal.toFixed(2);
        if (total * -1 !== transaction.netTotal * -1) {
          isEqual = false;
        }
        if (totalConvert * -1 !== convertedNetTotal * -1) {
          isEqualConverted = false;
        }
      });



      if (isEqual === true && isEqualConverted === true && idTreaties === true) {
        form.$dirty = false;
        form.$setPristine();
        var transactions = vm.mainTransactionList;
        //     vm.mainTransactionList = [];
        angular.forEach(vm.mainTransactionList, function (transaction, key) {
          transaction.lastUser = vm.treatyTransaction.lastUser;
          angular.forEach(transaction.subTransactions, function (subTransactions, key) {
            if (subTransactions.id == null) {
              subTransactions.creationTs = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
              subTransactions.creationUser = vm.treatyTransaction.lastUser;
            }
            subTransactions.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
            subTransactions.lastUser = vm.treatyTransaction.lastUser;
            subTransactions.settlementDate = transaction.settlementDate;
            subTransactions.isActive = 1;
          });
          transaction.mixTransactions = 1;
          if (transaction.id !== null) {
            transaction.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
            transaction.isActive = 1;
            TreatyTransaction.update(transaction, onSaveSuccess, onSaveError);
            transaction.isCollapsed = true;
          } else {
            transaction.isActive = 1;
            transaction.creationUser = vm.treatyTransaction.lastUser;
            transaction.creationTs = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
            TreatyTransaction.save(transaction, function (result) {
              onSaveSuccess(result);
              vm.mainTransactionList[key] = result;
              vm.mainTransactionList[key].isCollapsed = true;
            }, onSaveError);
            transaction.isCollapsed = true;
          }
        });
        vm.reorder();
      } else {

        if (!idTreaties) {

          $mdDialog
            .show($mdDialog.alert()
              .textContent('You cannot save because the combination of selected treaties does not constitute a MIX Transaction. Treaties must be unique in order to save as a MIX.')
              .disableParentScroll(false)
              .parent(angular.element(document.querySelector('#myModal')))
              .ok('OK'));

        } else {
          $mdDialog
            .show($mdDialog.alert()
              .textContent('You cannot save because the allocations do not add up to the total transaction amount.')
              .disableParentScroll(false)
              .parent(angular.element(document.querySelector('#myModal')))
              .ok('OK'));

        }
      }
    };

    vm.exchangeRate = function (mainTransaction, param) {
      if (param === 1) {
        angular.forEach(mainTransaction.subTransactions, function (subTransactions, key) {
          vm.trackingByPeriod(subTransactions, mainTransaction);
        });
      }

      if (mainTransaction.currency != undefined && mainTransaction.accountingDate != undefined) {
        CurrencyCustomSrv.rate.get({
          code: mainTransaction.currency.code,
          year: mainTransaction.accountingDate.getFullYear()
        }).$promise.then(
          function (result) {
            vm.currentExchangeRate = result.exchangeRate;
            mainTransaction.convertedNetTotal = mainTransaction.netTotal * vm.currentExchangeRate;
            angular.forEach(mainTransaction.subTransactions, function (transaction, key) {
              transaction.convertedAmount = vm.currentExchangeRate * transaction.amount;
            });
          });

        if (mainTransaction.fundingType.name === "Check-Out") {
          if (mainTransaction.netTotal >= 0) {
            mainTransaction.netTotal = mainTransaction.netTotal * -1;
            mainTransaction.convertedNetTotal = mainTransaction.convertedNetTotal * -1;
          }
        }
      }
    };

    vm.SubTransactionCurrency = function (transaction, mainTransaction) {
      if (mainTransaction.currency != undefined && mainTransaction.accountingDate != undefined) {
        CurrencyCustomSrv.rate.get({
          code: mainTransaction.currency.code,
          year: mainTransaction.accountingDate.getFullYear()
        }).$promise.then(function (result) {
          vm.currentExchangeRate = result.exchangeRate;
          transaction.convertedAmount = vm.currentExchangeRate * transaction.amount;
        });
      }
    };

    vm.addSubTransaction = function (Maintransaction) {
      var subtransaction = {
        allocationType: '',
        reportingDate: '',
        amount: '',
        company: '',
        treaty: vm.treatyTransaction.treaty,
        deal: vm.treatyTransaction.treaty.deal,
        legalEntityOptions: vm.companySub(vm.treatyTransaction.treaty.id),
        isNew: true
      };
      Maintransaction.subTransactions.push(subtransaction);
      vm.changeDeal(subtransaction);

    };
    vm.removeSubTransaction = function (Maintransaction, $index) {
      Maintransaction.subTransactions.splice($index, 1);
    };

    vm.removeTransaction = function ($index) {
      if (vm.firstName !== vm.treatyTransaction.treaty.deal.gfsAssignee.firstName && vm.authority !== 'Admin') {
        var alert = InteractionsService.alert("I'm sorry " + vm.firstName + ", you are not allowed to delete " + vm.treatyTransaction.treaty.deal.gfsAssignee.firstName + "'s transactions.", '#myModal');
        InteractionsService.showThenDialog(alert);
      } else {
        var mixTransaction = vm.mainTransactionList[$index];
        var confirm = $mdDialog.confirm()
          .textContent('Are you sure you want to delete this transaction?')
          .ok('Yes')
          .cancel('No')
          .parent(angular.element(document.querySelector('#myModal')));

        var confirmChange = $mdDialog.confirm()
          .textContent('Deleted.')
          .ok('OK')
          .parent(angular.element(document.querySelector('#myModal')));

        var confirmChangeActual = $mdDialog.prompt()
          .title('Why are you deleting this transaction? ')
          .textContent('Reason is Required to delete.')
          .ariaLabel('Ny ')
          .ok('Delete.')
          .required(true)
          .cancel('Cancel.')
          .parent(angular.element(document.querySelector('#myModal')));

        var trackingid = [];
        if (mixTransaction.id != null) {
          $mdDialog.show(confirm).then(function () {
            var confirmDeleteChange;
            if (mixTransaction.transactionStatus.name === 'Actual') {
              confirmDeleteChange = confirmChangeActual;
            } else {
              confirmDeleteChange = confirmChange;
            }

            $mdDialog.show(confirmDeleteChange).then(function (result) {
              if (mixTransaction.transactionStatus.name === 'Actual') {
                mixTransaction.deleteReason = result;
                mixTransaction.deleteNotification = 1;
              }
              var flagNext = false;
              mixTransaction.lastUser = vm.treatyTransaction.lastUser;
              mixTransaction.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
              mixTransaction.isActive = 0;
              var trackingList = [];
              angular.forEach(mixTransaction.subTransactions, function (subTransactions, keysUb) {
                if (subTransactions.tracking) {
                  trackingList.push(subTransactions.tracking);

                }
                subTransactions.isActive = 0;
                subTransactions.lastUser = vm.treatyTransaction.lastUser;
                subTransactions.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';

                if (keysUb === mixTransaction.subTransactions.length - 1) {
                  flagNext = true;
                }
              });
              if (flagNext) {
                TreatyTransaction.update(mixTransaction, function (resultDeletedTransaction) {
                  angular.forEach(trackingList, function (tracking, keyTracking) {
                    var tracking = tracking;
                    TreatyTransactionSrv.subTransactionTracking.get({
                      trackingId: tracking.id
                    }).$promise.then(function (result) {
                      if (result.length >= 1) {
                        var currentSettlement = null;
                        var countTracking = 0;
                        angular.forEach(result, function (subTranTracking, key) {
                          if ((moment(subTranTracking.reportingDate).isSame(tracking.reportingMonthYear))) {
                            if (currentSettlement === null) {
                              currentSettlement = subTranTracking.settlementDate;
                            } else {
                              if ((moment(currentSettlement).isBefore(subTranTracking.settlementDate))) {
                                currentSettlement = subTranTracking.settlementDate;
                              }
                            }
                          }
                          countTracking = key;
                        });

                        if (currentSettlement === null && countTracking === result.length - 1) {
                          trackingid.push(tracking.id);
                        } else {
                          if (tracking.treaty.stmntDueAsmBroker !== null && tracking.treaty.stmntDueAsmBroker !== undefined) {
                            if (tracking.treaty.stmntDueAsmBroker.numberOfDays !== null && tracking.treaty.stmntDueAsmBroker.numberOfDays !== undefined) {
                              tracking.brokerFeeDueDate = (moment(currentSettlement).add(tracking.treaty.stmntDueAsmBroker.numberOfDays, 'days'));
                            }
                          } else {
                            tracking.brokerFeeDueDate = null;
                          }
                          tracking.cashSentRecDate = currentSettlement;
                          TreatyTracking.update(tracking);
                        }
                      } else {
                        tracking.cashSentRecDate = null;
                        tracking.brokerFeeDueDate = null;
                        if (tracking.cashDueFlag !== 'No') {
                          tracking.trackingStatus = 'Open';
                        }
                        TreatyTracking.update(tracking);
                      }
                    });
                  });

                  /////////////////
                  angular.forEach(vm.listOfTrackingIds, function (track, key) {
                    TreatyTracking.get({
                      id: track.trackingId
                    }).$promise.then(function (resultTracking) {
                      var tracking = resultTracking;
                      TreatyTransactionSrv.subTransactionTracking.get({
                        trackingId: track.trackingId
                      }).$promise.then(function (result) {
                        if (result.length >= 1) {
                          var currentSettlement = null;
                          angular.forEach(result, function (subtransaction, key) {
                            if (moment(subtransaction.reportingDate).isSame(tracking.reportingMonthYear)) {
                              if (currentSettlement === null) {
                                currentSettlement = subtransaction.settlementDate;
                              } else {
                                if (moment(currentSettlement).isBefore(subtransaction.settlementDate)) {
                                  currentSettlement = subtransaction.settlementDate;
                                }
                              }
                            }
                          });

                          if (currentSettlement === null) {
                            tracking.cashSentRecDate = null;
                            tracking.brokerFeeDueDate = null;
                            if (tracking.cashDueFlag !== 'No') {
                              tracking.trackingStatus = 'Open';
                            }
                            TreatyTracking.update(tracking, onSaveSuccessDateTracking, onSaveError);
                          } else {
                            if (vm.treatyTransaction.treaty.stmntDueAsmBroker !== null && vm.treatyTransaction.treaty.stmntDueAsmBroker !== undefined) {
                              if (vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays !== null && vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays !== undefined) {
                                tracking.brokerFeeDueDate = (moment(currentSettlement).add(vm.treatyTransaction.treaty.stmntDueAsmBroker.numberOfDays, 'days'));
                              }
                            } else {
                              tracking.brokerFeeDueDate = null;
                            }
                            tracking.cashSentRecDate = currentSettlement;

                            TreatyTracking.update(tracking, onSaveSuccessDateTracking, onSaveError);
                          }
                        }

                        if (result.length === 0) {
                          tracking.cashSentRecDate = null;
                          tracking.brokerFeeDueDate = null;
                          if (tracking.cashDueFlag !== 'No') {
                            tracking.trackingStatus = 'Open';
                          }

                          TreatyTracking.update(tracking, onSaveSuccessDateTracking, onSaveError);

                        }
                      });

                    })
                    ///////////////////////////

                  })

                  /////////////////
                }, onSaveError);
                vm.mainTransactionList.splice($index, 1);
              }
            });
          });
        } else {
          vm.mainTransactionList.splice($index, 1);
        }
      }
    };

    vm.mainTransactionAdd = function () {
      vm.addMain = function () {
        var mainTransactionItem = {
          id: null,
          mixTransactions: 0,
          transactionStatus: {
            id: 2,
            name: "Estimated",
            description: "Estimated"
          },
          treaty: vm.treatyTransaction.treaty,
          settlementDate: vm.treatyTransaction.settlementDate,
          accountingDate: vm.treatyTransaction.accountingDate,
          legalEntityOptions: vm.companySub(vm.treatyTransaction.treaty.id),
          company: vm.defaultCompanyObject,
          fundingTypeOptions: vm.fundingTypeOptions,
          fundingType: {
            description: "N/A",
            id: 3,
            name: "N/A"
          },
          currency: vm.treatyTransaction.currency,
          netTotal: vm.treatyTransaction.netTotal,
          subTransactions: [{
            allocationType: '',
            reportingDate: '',
            amount: '',
            treaty: vm.treatyTransaction.treaty,
            deal: vm.treatyTransaction.treaty.deal,
            treaties: vm.treatyByDeal(vm.dealId),
            isNew: true
          }, {
            allocationType: '',
            reportingDate: '',
            amount: '',
            treaty: null,
            deal: vm.treatyTransaction.treaty.deal,
            treaties: vm.treatyByDeal(vm.dealId),
            isNew: true
          }],
          currencyConvertedFlag: 0,
          convertedNetTotal: 0,
          isNew: true
        };
        vm.mainTransactionList.unshift(mainTransactionItem);
      };

      vm.addMain();

    };

    vm.copyTransaction = function ($index) {
      vm.copiedTransaction = {
        id: null,
        transactionStatus: 'Estimated',
        mixTransactions: 0,
        treaty: vm.treatyTransaction.treaty,
        settlementDate: vm.treatyTransaction.settlementDate,
        accountingDate: vm.treatyTransaction.accountingDate,
        company: '',
        legalEntityOptions: vm.companySub(vm.treatyTransaction.treaty.id),
        fundingType: {
          description: "N/A",
          id: 3,
          name: "N/A"
        },
        currency: vm.treatyTransaction.currency,
        netTotal: vm.treatyTransaction.netTotal,
        subTransactions: [{
          allocationType: '',
          reportingDate: '',
          amount: '',
          treaty: vm.treatyTransaction.treaty,
          deal: vm.treatyTransaction.treaty.deal
        }, {
          allocationType: '',
          reportingDate: '',
          amount: '',
          treaty: vm.treatyTransaction.treaty,
          deal: vm.treatyTransaction.treaty.deal
        }],
        currencyConvertedFlag: 0,
        convertedNetTotal: 0
      };
      angular.copy(vm.mainTransactionList[$index], vm.copiedTransaction);
      vm.copiedTransaction.id = null;
      vm.copiedTransaction.isNew = true
      angular.forEach(vm.copiedTransaction.subTransactions, function (subTransaction, key) {
        subTransaction.id = null;
        subTransaction.isNew = true;
      });
      vm.mainTransactionList.unshift(vm.copiedTransaction);
    };

    vm.tracking = function (code, transaction) {

    };

    //////////////////////

    vm.changeStatusFunding = function (mainTransaction, param) {
      switch (mainTransaction.transactionStatus.name) {
        case 'Estimated':
          mainTransaction.fundingTypeOptions = vm.fundingTypeOptions;
          mainTransaction.fundingType = vm.fundingTypeNa;
          if (param === 1 || param === 0) {
            mainTransaction.settlementDate = null;
          }
          angular.forEach(mainTransaction.subTransactions, function (subTransactions, key) {
            subTransactions.tracking = null;
          });
          break;
        case 'Actual':
          angular.forEach(mainTransaction.subTransactions, function (subTransactions, key) {
            vm.trackingByPeriod(subTransactions, mainTransaction);
          });
          if (mainTransaction.treaty.agreementType.name === 'Retro – Internal' &&
            mainTransaction.treaty.treatyStatus.name !== 'Pending' &&
            mainTransaction.treaty.treatyStatus.name !== 'Terminated') {
            mainTransaction.fundingTypeOptions = vm.fundingTypeOptions;
            mainTransaction.fundingType = vm.fundingTypeNa;
            mainTransaction.settlementDate = null;

          } else {
            if (param === 1) {
              mainTransaction.fundingType = null;
            }
            mainTransaction.fundingTypeOptions = vm.fundingTypeOptionsActual;
          }

          break;

        case 'Expected':

          if (param === 1) {
            mainTransaction.fundingType = null;
          }
          mainTransaction.fundingTypeOptions = vm.fundingTypeOptionsActual;
          angular.forEach(mainTransaction.subTransactions, function (subTransactions, key) {
            subTransactions.tracking = null;
          });
          break;
        default:
      }


    };

    // ////////




    vm.checkFundingType = function (transaction) {
      if (transaction.fundingType !== null && transaction.fundingType !== undefined) {
        if (transaction.fundingType.name === "Wire-Out" || transaction.fundingType.name === "Check-Out") {

          if (transaction.fundingType.name !== "Wire-Out" ){
            var  msg ='Since Funding Type indicates outbound funds, the Net Total must be less than zero.';
        }else{
          var msg ='Please consider that the Net Total for Wire-Out (outbound funds) should be entered as a negative number.';
        }
          $mdDialog
            .show($mdDialog.alert()
              .textContent(msg)
              .disableParentScroll(false)
              .parent(angular.element(document.querySelector('#myModal')))
              .ok('OK'));
          
          if (transaction.netTotal > 0 && transaction.fundingType.name !== "Wire-Out" ) 
          {
            transaction.netTotal = transaction.netTotal * -1;
            transaction.convertedNetTotal = transaction.convertedNetTotal * -1;
          }
        } else {
          if (transaction.fundingType.name === 'Accrual'  || transaction.fundingType.name==='Term Adj') {
            transaction.subTransactions.forEach(function (sub) {
              sub.tracking = null;
            })
          }

          if (transaction.netTotal < 0) {
            transaction.netTotal = transaction.netTotal * -1;
            transaction.convertedNetTotal = transaction.convertedNetTotal * -1;
          }

        }
      }
    };

    vm.changeMixTransaction = function (maintransaction) {
      angular.forEach(maintransaction.subTransactions, function (subTransaction, key) {
        subTransaction.deal = angular.copy(subTransaction.treaty.deal);
        vm.changeDeal(subTransaction);

      });
    };


    vm.treatyByDeal = function (id) {
      var treaties = [];
      TreatySrv.deals.query({
        id: id
      }).$promise.then(function (result) {
        angular.forEach(result, function (treaty) {

          if (treaty.treatyStatus.name === 'Terminated') {
            treaty.treatyName = treaty.treatyName + ' - Treaty Terminated';
            treaty.disabled = true;
          } else {
            treaty.disabled = false;
          }
          if (treaty.agreementType && treaty.agreementType.name !== 'Retro – Internal') {
            treaties.push(treaty);
          }

        });

      });
      return treaties;
    };
    vm.changeDeal = function (transaction) {
      if (transaction.deal !== null) {
        transaction.treaties = [];
        transaction.treaties = vm.treatyByDeal(transaction.deal.id);
        transaction.legalEntityOptions = [];
      }
    };

    function getXrefCompanies(transactionList, transactionId) {
      CompanyCustomSrv.rgaCompanies.query(function (rgaResult) {
        vm.rgaCompaniesCheck = rgaResult;
        angular.forEach(rgaResult, function (rgacompany, key) {
          transactionList.push(rgacompany);
        });

        Company.get({
          id: 504521716
        }, function (company) {
          var companyName = "* " + company.name;
          company.name = companyName;
          transactionList.push(company);
          vm.defaultCompany();
          return transactionList;
        });

      });

    }

    vm.reverse = true;
    vm.sortBy = function (propertyName) {
      vm.propertyName = propertyName;
      vm.mainTransactionList = orderBy(vm.mainTransactionList, vm.propertyName, vm.reverse);
    };

    vm.toggleFilter = function () {
      vm.filterStatus = !vm.filterStatus;
    }

    vm.filter = function (editform) {
      vm.filteredData = true;
      editform.$dirty = false;
      vm.loadAllCashMix()
      .then(function () {
        vm.mainTransactionList = vm.mainTransactionList
        .filter(function(transaction){ // Reporting Period Filter
          if (vm.searchStartDate && vm.searchEndDate) {
            return transaction.subTransactions.some(function(sub){
              return moment(sub.reportingDate).isBetween(vm.searchStartDate, vm.searchEndDate, null, '[]')
            });
          }
          return true;
        })
        .filter(function(transaction){ // Transaction Status Filter
          return vm.transactionStatusFilter
          .some(function(ts){ return ts.name === transaction.transactionStatus.name; })
        })
        .filter(function(transaction){
          return vm.fundingTypeFilter
          .some(function(ft){ return ft.name === transaction.fundingType.name; })
        })
        .filter(function(transaction){
          return vm.currencyFilter
          .some(function(c){ return c.code === transaction.currency.code; })
        })
        .filter(function(transaction) {
          return vm.allocationTypeFilter
          .some(function(at){
            return transaction.subTransactions.some(function(sub){
              return sub.allocationType.name === at.name;
            })
          })
        });
      });
    };

    vm.loadAllCashMix = function (_callback) {
      vm.mainTransactionList = [];
      vm.flagDirty = 0;
      vm.listOfTrackingIds = [];
      return TreatyTransactionMixSrv.get({
        treatyId: $stateParams.id
      }, function (result) {
        angular.forEach(result, function (mixTransaction, key) {
          vm.changeStatusFunding(mixTransaction, 5);
          var a = 0;
          angular.forEach(mixTransaction.subTransactions, function (subTransactionMix, key) {
            subTransactionMix.deal = subTransactionMix.treaty.deal;
            vm.changeDeal(subTransactionMix);
            // validation for each treaty in the deal before push !
            if (subTransactionMix.treaty.id === vm.treatyTransaction.treaty.id) {
              a = 1;
            }
          });
          if (a === 1) {
            mixTransaction.isCollapsed = true;
            angular.forEach(mixTransaction.subTransactions, function (subTransactions, key) {
              vm.trackingByPeriod(subTransactions, mixTransaction);
            });
            vm.mainTransactionList.push(mixTransaction);
          }
        });
      }).$promise;
    };

    vm.reset = function (editform) {
      vm.filteredData = false;
      editform.$dirty = false;
      vm.listOfTrackingIds = [];
      vm.mainTransactionList = [];
      vm.searchStartDate = null;
      vm.searchEndDate = null;
      vm.estimated = 'Estimated';
      vm.actual = 'Actual';
      vm.expected = 'Expected';
      TreatyTransactionMixSrv.get({
        treatyId: $stateParams.id
      }, function (result) {
        angular.forEach(result, function (mixTransaction, key) {
          vm.changeStatusFunding(mixTransaction, 5);
          var a = 0;
          angular.forEach(mixTransaction.subTransactions, function (subTransactionMix, key) {
            subTransactionMix.deal = subTransactionMix.treaty.deal;
            vm.changeDeal(subTransactionMix);
            // validation for each treaty in the deal before push !
            if (subTransactionMix.treaty.id === vm.treatyTransaction.treaty.id) {
              a = 1;
            }
          });
          if (a === 1) {
            mixTransaction.isCollapsed = true;
            angular.forEach(mixTransaction.subTransactions, function (subTransactions, key) {
              vm.trackingByPeriod(subTransactions, mixTransaction);
            });
            vm.mainTransactionList.push(mixTransaction);
          }
        });
        //vm.reorder();
      });

      vm.transactionStatusFilter = vm.transactionStatuses;
      vm.fundingTypeFilter = vm.fundingTypeOptions;
      vm.currencyFilter = vm.currencyOptions;
      vm.allocationTypeFilter = vm.allocationTypes;
    };

    var onError = function () {};

    vm.collapse = function (param) {
      if (param === 1) {
        angular.forEach(vm.mainTransactionList, function (transaction) {
          transaction.isCollapsed = false;
        });
      } else {
        angular.forEach(vm.mainTransactionList, function (transaction) {
          transaction.isCollapsed = true;
        });
      }
    };

    vm.defaultCompany = function () {
      angular.forEach(vm.treatyCompaniesCheck, function (treatyCompany) {
        angular.forEach(vm.rgaCompaniesCheck, function (rgaCompany) {
          if (vm.defaultCompanyObject === null && treatyCompany.company.clientCompanyId === rgaCompany.clientCompanyId) {
            vm.defaultCompanyObject = treatyCompany.company;
          }
        });
      });
      return vm.defaultCompanyObject;
    };

    vm.updateLegalEntity = function (mainTransaction) {
      angular.forEach(mainTransaction.subTransactions, function (transaction) {
        transaction.mdm = null;
      });
    };

    vm.checkCompanyEk = function (mainTransaction, transaction) {
      if (transaction.treatyt != null && mainTransaction.company != null && transaction.mdm != null) {
        if (mainTransaction.company.legalCode != transaction.mdm.legalCode) {
          var confirm = $mdDialog.alert()
            // .title('Validate Selected RGA Entity on Transaction Against Selected EK on Sub-Transaction ')
            .textContent("This EK does not roll up to the selected Legal Entity.")
            .ok('OK')
            .parent(angular.element(document.querySelector('#myModal')));
          $mdDialog.show(confirm);

        }
      }
    };

    vm.trackingByPeriod = function (transaction, mainTransaction) {


      if (transaction.treaty != null &&
        mainTransaction.transactionStatus.name === 'Actual' &&
        mainTransaction.currency != null &&
        transaction.reportingDate != null) {

        TreatyTrackingCustomSvr.TrackingByPeriod.get({
            treatyId: transaction.treaty.id,
            startYear: moment(transaction.reportingDate).get('year'),
            startMonth: moment(transaction.reportingDate).get('month') + 1,
            startDay: moment(transaction.reportingDate).get('date')
          },
          function (result) {
            transaction.trackingList = [];
            angular.forEach(result, function (track) {
              if (track.currency.code === mainTransaction.currency.code) {
                transaction.trackingList.push(track);
              }
            });

            if (transaction.trackingList.length === 0 && !(mainTransaction.transactionStatus && mainTransaction.transactionStatus.name == 'Actual' && mainTransaction.fundingType && (mainTransaction.fundingType.name === 'Accrual' || transaction.fundingType.name==='Term Adj'))) {
              var confirm = $mdDialog.alert()
                // .title('Validate Selected RGA Entity on Transaction Against Selected EK on Sub-Transaction ')
                .textContent("There is no tracking record for this Reporting Period, therefore, you cannot save an 'Actual' transaction for this period.")
                .ok('OK')
                .disableParentScroll(false)
                .parent(angular.element(document.querySelector('#myModal')));
              $mdDialog.show(confirm);
            }


          }, onError);
      } else {
        transaction.trackingList = [];
      }
    };


    vm.changeSubTracking = function (tracking, id, subId) {
      var flag = false;
      var index;
      angular.forEach(vm.listOfTrackingIds, function (track, key) {
        if (track.trackingId === parseInt(id)) {
          flag = true;
          index = key;
        }
      });
      if (id !== '' && !flag) {
        vm.listOfTrackingIds.push({
          trackingId: parseInt(id),
          subTransactionId: parseInt(subId)
        });
      }
      if (id === '' && flag) {
        vm.listOfTrackingIds.splice(index, 1);
      }
    }


  }

})();
