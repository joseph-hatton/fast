(function () {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('treaty-transaction-cash', {
        parent: 'entity',
        url: '/treaty-transaction/cash/{id}/{dealId}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treatyTransactionCash.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty-transaction/treaty-transaction-cash.html',
            controller: 'TreatyTransactionCashController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treatyTransactionCash');
            return $translate.refresh();
          }],
          entity: function () {
            return {
              intercompany: null,
              fundingType: null,
              origCurrencyIn: null,
              origCurrencyOut: null,
              usdIn: null,
              usdOut: null,
              transactionStatus: null,
              comments: null,
              settlementType: null,
              clientReportingQuarter: null,
              clientReportingYear: null,
              currencyNotConvertedFlag: null,
              id: null
            };
          }
        }
      })
      .state('treaty-transaction-relief', {
        parent: 'entity',
        url: '/treaty-transaction/relief/{dealId}/{page}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treatyTransactionRelief.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty-transaction/treaty-transaction-relief.html',
            controller: 'TreatyTransactionReliefController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treatyTransactionRelief');
            return $translate.refresh();
          }],
          entity: function () {
            return {
              intercompany: null,
              fundingType: null,
              origCurrencyIn: null,
              origCurrencyOut: null,
              usdIn: null,
              usdOut: null,
              transactionStatus: null,
              comments: null,
              settlementType: null,
              clientReportingQuarter: null,
              clientReportingYear: null,
              currencyNotConvertedFlag: null,
              id: null
            };
          }
        }
      })
      .state('treaty-transaction-cash-mix', {
        parent: 'entity',
        url: '/treaty-transaction/cash/mix/{id}/{dealId}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treatyTransactionCash.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty-transaction/treaty-transaction-cash-mix.html',
            controller: 'TreatyTransactionCashMixController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treatyTransactionCash');
            return $translate.refresh();
          }],
          entity: function () {
            return {
              intercompany: null,
              fundingType: null,
              origCurrencyIn: null,
              origCurrencyOut: null,
              usdIn: null,
              usdOut: null,
              transactionStatus: null,
              comments: null,
              settlementType: null,
              clientReportingQuarter: null,
              clientReportingYear: null,
              currencyNotConvertedFlag: null,
              id: null
            };
          }
        }
      })

      .state('treaty-transaction-relief-tableau', {
        parent: 'entity',
        url: '/treaty-transaction/relief/{dealId}/{page}/{yearEnd}/{monthEnd}/{status}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treatyTransactionRelief.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty-transaction/treaty-transaction-relief.html',
            controller: 'TreatyTransactionReliefController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treatyTransactionRelief');
            return $translate.refresh();
          }],
          entity: function () {
            return {
              intercompany: null,
              fundingType: null,
              origCurrencyIn: null,
              origCurrencyOut: null,
              usdIn: null,
              usdOut: null,
              transactionStatus: null,
              comments: null,
              settlementType: null,
              clientReportingQuarter: null,
              clientReportingYear: null,
              currencyNotConvertedFlag: null,
              id: null
            };
          }
        }
      })


      .state('treaty-transaction-cash.edit-detail', {
        parent: 'treaty-transaction-cash',
        url: '/transaction/{transactionId}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-transaction/treaty-transaction-cash-finance-dialog.html',
            controller: 'TreatyTransactionCashFinanceDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['TreatyTransaction', function (TreatyTransaction) {
                return TreatyTransaction.get({id: $stateParams.transactionId});
              }]
            }
          }).result.then(function () {
            $state.go('treaty-transaction-cash', {id: $stateParams.id, dealId: $stateParams.dealId}, {reload: true});
          }, function () {
            $state.go('treaty-transaction-cash', {id: $stateParams.id, dealId: $stateParams.dealId}, {reload: true});
          });
        }]
      })
      .state('treaty-transaction-cash-mix.finance', {
        parent: 'treaty-transaction-cash-mix',
        url: '/transaction-mix/{transactionId}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-transaction/treaty-transaction-cash-finance-dialog.html',
            controller: 'TreatyTransactionCashFinanceDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['TreatyTransaction', function (TreatyTransaction) {
                return TreatyTransaction.get({id: $stateParams.transactionId});
              }]
            }
          }).result.then(function () {
            $state.go('treaty-transaction-cash-mix', {id: $stateParams.id, dealId: $stateParams.dealId}, {reload: true});
          }, function () {
            $state.go('treaty-transaction-cash-mix', {id: $stateParams.id, dealId: $stateParams.dealId}, {reload: true});
          });
        }]
      })
    ;

  }
})();
