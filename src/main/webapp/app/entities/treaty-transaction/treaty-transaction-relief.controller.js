(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('TreatyTransactionReliefController', TreatyTransactionReliefController);

  TreatyTransactionReliefController.$inject = ['orderByFilter','$scope','$stateParams', 'entity', 'Principal',
    'TreatyTransactionReliefSrv','TreatyTransactionRelief','Currency',
    'Deal', 'CompanyCustomSrv', 'Country', 'DateUtils', '$mdDialog','CurrencyCustomSrv','TreatyTransactionReliefFilter', 'Intro'];
  function TreatyTransactionReliefController(orderBy,$scope, $stateParams, entity,  Principal,
                                             TreatyTransactionReliefSrv,TreatyTransactionRelief,Currency,
                                             Deal, CompanyCustomSrv, Country, DateUtils, $mdDialog,CurrencyCustomSrv,TreatyTransactionReliefFilter, Intro) {
    var vm = this;
    vm.treatyTransaction = entity;
    vm.dealId = $stateParams.dealId;
   // vm.companies = [];
    vm.countries = [];
    vm.selectedCompanyType = null;
    vm.transactionList=[];
    vm.page=$stateParams.page;
    vm.flagDirty=0;
    vm.isSavingCompany=true;
    vm.expand=0;
    vm.datePickerOpenStatus = {};
    vm.datePickerOpenStatus.searchStartDate = false;
    vm.datePickerOpenStatus.searchEndDate = false;
    vm.format = "yyyy-MM";
    vm.actual="Actual";
    vm.estimated='Estimated';
    vm.dateOptions = {
      datepickerMode: 'month',
      formatMonth: 'MMMM',
      formatYear: 'yyyy',
      formatMonthTitle: 'yyyy',
      minMode: 'month',
      showMonths: true,
      startingDay: 0,
      yearRange: 20,
      minDate: null,
      maxDate: null
    };

    vm.saving = true;
    vm.reliefCount = 0;

    Intro.setupIntroSteps({id:173})
    .then(function(result){
        vm.introOptions = {
            steps: result
        };
    });

    if(
      $stateParams.yearEnd !== undefined  &&
      $stateParams.monthEnd !== undefined &&
      $stateParams.status !== undefined){
      vm.tableauFlag=true;
    }


    vm.openCalendar = function (date) {
      vm.datePickerOpenStatus[date] = true;
    };
   /*
    $scope.$on('$stateChangeStart', function( event) {
      if(vm.flagDirty===1){
        var answer = confirm("Are you sure you want to exit without saving?");
        if (!answer) {
          event.preventDefault();
        }}
    });
    */

      CompanyCustomSrv.companies.query({search: 'rga'}, function (result) {
        vm.companies = result;
        angular.forEach(result,function (company) {
          if(company.clientCompanyId===10644  ){
            vm.companyRga=company;
          }
        });
        Country.query(function(result) {
          angular.forEach(result,function (country) {
            if(country.name==='United States'){
              vm.countryDefault=country;
            }
          });
          vm.countries = result;
           if(!vm.tableauFlag) {
            TreatyTransactionReliefSrv.base.get({id: $stateParams.dealId}, function (result) {
              vm.transactionList = result;
              vm.collapse(0);
            });
          }

        });
      });



    $(document).on('keyup keypress', 'form input[type="text"]', function(e) {
      if(e.keyCode === 13) {
        e.preventDefault();
        return false;
      }
    });

    vm.exchangeRate = function (mainTransaction) {
      if(mainTransaction.currency !==undefined && mainTransaction.reportingDate !== undefined){
      CurrencyCustomSrv.rate.get({code:mainTransaction.currency.code ,year:mainTransaction.reportingDate.getFullYear()}).$promise.then(function (result) {
        vm.currentExchangeRate=result.exchangeRate;
        mainTransaction.convertedNetTotal=mainTransaction.netTotal*vm.currentExchangeRate;
        angular.forEach(mainTransaction.subTransactions, function(transaction, key) {
          transaction.convertedAmount=vm.currentExchangeRate * transaction.amount;
        });
      });}
    };
    vm.SubTransactionCurrency=function (transaction,mainTransaction) {
      if(mainTransaction.currency !==undefined && mainTransaction.reportingDate !== undefined) {
        CurrencyCustomSrv.rate.get({
          code: mainTransaction.currency.code,
          year: mainTransaction.reportingDate.getFullYear()
        }).$promise.then(function (result) {
          vm.currentExchangeRate = result.exchangeRate;
          transaction.convertedAmount = vm.currentExchangeRate * transaction.amount;
        });
      }
    };
      vm.companyTypes = [
      { name: 'Assuming'},
      { name: 'Ultimate'},
      { name: 'Interim'},
      { name: 'Internal'},
      { name: 'External Retro'}
      ];


    Principal.identity().then(function (account) {
      angular.forEach(account.authorities, function (authoritie) {
        if (authoritie === "ROLE_APP_FINRE_OPS_ADMIN") {
          vm.authority = 'Admin';
        } else {
          if (authoritie === "ROLE_APP_FINRE_VIEW_ONLY") {
            vm.authority = 'ViewOnly'
          }else{
            if (vm.authority !== 'Admin' && vm.authority !== 'ViewOnly') {
              vm.authority = 'User';
            }
          }
        }
      });
      vm.lastUser=account.login;
      vm.treatyTransaction.lastUpdate= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
    });


    Deal.get({id: $stateParams.dealId}, function (result) {
      vm.deal = result;
    });





    vm.structured= function(transactionSub,transaction){
      if(vm.deal.productSubtype!==null && vm.deal.productSubtype.name!=='')
      if(vm.deal.productSubtype.name==='Structured')
      {
        transactionSub.company=vm.companyRga;
        transaction.country= vm.countryDefault;
      }
    };


    Currency.query(function(result) {
      vm.currencyOptions = result;
    });


    vm.selectedCountry = function (transaction) {
        transaction.region = transaction.country.region;
    };

    vm.testClick = function() {

    };
    vm.selectedCompany = function(subTransac,form,reliefForm) {
      var x = document.getElementById(form);
      $scope.$watch('x', function() {
        var flagCompany=true;
        angular.forEach(vm.transactionList, function (trans, key) {
          angular.forEach(trans.subTransactions, function (sub, key) {
            if (sub.company.id === undefined || sub.company.id === null) {
              flagCompany = false;
            }
          });
        });
        if(!flagCompany){
          vm.setFormInvalid(false);
        }else {
          vm.setFormInvalid(true);
        }

      });
    };
    vm.searchCompanies = function(subtransaction,companyField,reliefForm,mainTransaction){
       CompanyCustomSrv.companies.query({search: subtransaction}, function (result) {
        $scope.$watch('subtransaction.company', function() {
          var flagCompany=true;
          angular.forEach(mainTransaction, function (trans, key) {
            angular.forEach(trans.subTransactions, function (sub, key) {
              if (sub.company.id === undefined || sub.company.id === null) {
                flagCompany = false;
              }
            });
          });
            if(!flagCompany){
              companyField.$invalid = true;
              vm.setFormInvalid(false);
            }else {
              vm.setFormInvalid(true);
            }
        });
        vm.companies = result;
        return vm.companies;
      });
    };

    vm.setFormInvalid=function(info) {
      vm.isSavingCompany=info;
    };

    var onSaveSuccess = function (result) {
      $scope.$emit('finreApp:treatyTransactionUpdate', result);
     // vm.isSaving = false;
      vm.reliefCount++;
    };

    var onSaveError = function (header, data,resp,req) {
     // vm.isSaving = false;
      vm.saving = true;
    };

    $scope.$watch('vm.reliefCount', function() {
      if ( vm.transactionList.length === vm.reliefCount) {
        vm.saving = true;
        vm.reliefCount = 0;
      }
    });

    vm.save = function (form) {
      vm.saving = false;
      form.$dirty=false;
      ////
      var isEqual = true;
      var isEqualConverted = true;
      //Check to see if transaction equals subtransactions
      angular.forEach(vm.transactionList, function(transaction, key) {
        transaction.isNew = false;
        var total = 0;
        var totalConvert=0;
        var convertedNetTotal=transaction.convertedNetTotal;
        angular.forEach(transaction.subTransactions, function (subTransaction, key) {
          subTransaction.isNew = false;
          total = total + subTransaction.amount;
          totalConvert=totalConvert+subTransaction.convertedAmount;
        });
        total=total.toFixed(2);
        totalConvert=totalConvert.toFixed(2);
        convertedNetTotal=convertedNetTotal.toFixed(2);

        if (total*-1 !== transaction.netTotal*-1) {
          isEqual = false;
        }
          if (totalConvert*-1 !==convertedNetTotal*-1) {
          isEqualConverted = false;
        }
      });
        if (isEqual===true && isEqualConverted===true) {
          angular.forEach(vm.transactionList, function(relief, key){
          relief.lastUser = vm.lastUser;
          relief.isActive = 1;
            if (relief.id !== null) {
            angular.forEach(relief.subTransaction, function(sub, key){
              sub.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
              sub.lastUser = vm.lastUser;
            });

            relief.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
            TreatyTransactionRelief.update(relief, onSaveSuccess, onSaveError);
            relief.isCollapsed=true;
          } else {
            angular.forEach(relief.subTransaction, function(sub, key){
              sub.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
              sub.lastUser = vm.lastUser;
              sub.creationTs =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
              sub.creationUser = vm.lastUser;
            });
            relief.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
            relief.lastUser = vm.lastUser;
            relief.creationUser=vm.lastUser;
            relief.creationTs= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
            TreatyTransactionRelief.save(relief, function(result){
              result.reportingDate = DateUtils.convertLocalDateFromServer(result.reportingDate);
              vm.transactionList[key] = result;
              vm.transactionList[key].isCollapsed=true;
              vm.reliefCount++;
              console.log(vm.reliefCount)
            }, onSaveError);
          }
        });
        } else {
        $mdDialog
          .show($mdDialog.alert()
            .textContent('You cannot save because the allocations do not add up to the total transaction amount.')
            .disableParentScroll(false)
            .parent(angular.element(document.querySelector('#myModal')))
            .ok('OK'));

      }
    };

    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };

    vm.USD = function (transaction) {
      transaction.convertedAmount = transaction.currency.exchangeRate * transaction.amount;
    };

    vm.removeTransaction=function($index){
      var reliefTransaction = vm.transactionList[$index];
      var confirm = $mdDialog.confirm()
        .textContent('Are you sure you want to delete this relief transaction?')
        .ok('Yes')
        .cancel('No')
        .parent(angular.element(document.querySelector('#myModal')));

      var confirmChange = $mdDialog.alert()
        .textContent('Deleted.')
        .ok('OK')
        .parent(angular.element(document.querySelector('#myModal')));

      $mdDialog.show(confirm).then(function() {
        $mdDialog.show(confirmChange).then(function() {
          if(reliefTransaction.id !== null && reliefTransaction.id !== undefined) {
            reliefTransaction.isActive = 0;
            TreatyTransactionRelief.update(reliefTransaction, function(){
              //do nothing

            }, onSaveError);
          }
          vm.transactionList.splice($index, 1);
          var flagCompany=true;
          angular.forEach(vm.transactionList, function (trans, key) {
            angular.forEach(trans.subTransactions, function (sub, key) {
              if (sub.company.id === undefined || sub.company.id === null) {
                flagCompany = false;
              }
            });
          });
          if(!flagCompany){
            vm.setFormInvalid(false);
          }else {
            vm.setFormInvalid(true);
          }

        });
      });
    };
    vm.addTransaction = function(){
      var transaction = {
        id: null,
        transactionStatus:'Estimated',
        reportingDate:null,
        country:'',
        region:'',
        deal: vm.deal,
        netTotal:'',
        convertedNetTotal:'',
        subTransactions:[{
          id: null,
          amount:'',
          convertedAmount:'',
          xrefCompanies:[],
          isNew: true,
          internal:0,
          interim:0,
          assuming:0,
          ultimate:0,
          externalRetro:0,
          orderBy:1
        }],
        isNew: true
      };
      vm.structured(transaction.subTransactions[0],transaction);
      vm.transactionList.unshift(transaction);
    };

    vm.addSubTransaction = function(transaction, index){
      console.log( getHighestOrderBy(transaction.subTransactions)+1);
      transaction.subTransactions.push( {
        id: null,
        amount:'',
        convertedAmount:'',
        xrefCompanies:[],
        internal:0,
        interim:0,
        assuming:0,
        ultimate:0,
        externalRetro:0,
        isNew: true,
        orderBy: getHighestOrderBy(transaction.subTransactions)+1
      });
      vm.freshlyAddedSubtransaction = vm.freshlyAddedSubtransaction ? vm.freshlyAddedSubtransaction : [];
      vm.freshlyAddedSubtransaction.push(index);
    };

    vm.removeSubTransaction=function(transaction,$index){
        transaction.subTransactions.splice($index, 1);
    };

    vm.openReportingDate = function(trans) {
      trans.reportingDateOpened = true;
    };

    vm.copyTransaction = function($index){
      vm.copiedTransaction = {
        id: null,
        transactionStatus:'Estimated',
        reportingYear:'',
        reportingMonth:'',
        country: null,
        region:'',
        treaty: vm.deal,
        netTotal:'',
        subTransactions:[{
          id: null,
          currency:'',
          amount:'',
          convertedAmount:'',
          xrefCompanies:[],
          internal:0,
          interim:0,
          assuming:0,
          ultimate:0,
          externalRetro:0,
          orderBy:1
        }]
      };
      angular.copy(vm.transactionList[$index], vm.copiedTransaction);
      //Must set the id to null
      vm.copiedTransaction.id = null;
      vm.copiedTransaction.isNew = true;
      angular.forEach(vm.copiedTransaction.subTransactions,function (subtransaction, key) {
        subtransaction.id = null;
        subtransaction.isNew = true;
      });
      vm.transactionList.unshift(vm.copiedTransaction);
    };

    vm.sortBy = function(propertyName) {
      vm.reverse = (propertyName !== null && vm.propertyName === propertyName) ? !vm.reverse : false;
      vm.propertyName = propertyName;
      vm.transactionList = orderBy(vm.transactionList, vm.propertyName, vm.reverse);
    };

    var onSuccessDateFilter= function (result){
      vm.transactionList=[];
      angular.forEach(result,function(trans){
        trans.isCollapsed=true;
        if(trans.transactionStatus===vm.actual ||trans.transactionStatus===vm.estimated){
          vm.transactionList.push(trans);
        }
      });
    };

    vm.filterByReportingPeriod=function(reliefForm) {
      reliefForm.$dirty=false;
      if(!vm.searchStartDate){
      reliefForm.$setPristine();
      }
      var startDate=vm.searchStartDate;
      var endDate=vm.searchEndDate;
      var transactionFilter=[];
      if(startDate==null || endDate ==null) {
        TreatyTransactionReliefSrv.base.get({id: $stateParams.dealId}, onSuccessDateFilter);
        transactionFilter = vm.transactionList;
        angular.forEach(vm.transactionList,function(trans){
          if(trans.transactionStatus===vm.actual ||trans.transactionStatus===vm.estimated){
            transactionFilter.push(trans);
          }
        });
        vm.transactionList=[];
        vm.transactionList=transactionFilter;

      }else{
        TreatyTransactionReliefFilter.reportingReport.get({
          dealId:vm.dealId,
          startYear: moment(startDate).get('year'),
          startMonth: moment(startDate).get('month') + 1,
          startDay: moment(startDate).get('date'),
          endYear: moment(endDate).get('year'),
          endMonth: moment(endDate).get('month') + 1,
          endDay: moment(endDate).get('date')
        }, onSuccessDateFilter, onErrorDateFilter);
      }
    };

    vm.reset = function (reliefForm) {
      reliefForm.$dirty=false;
      reliefForm.$setPristine();
      vm.searchStartDate = null;
      vm.searchEndDate = null;
      vm.actual="Actual";
      vm.estimated='Estimated';
      TreatyTransactionReliefSrv.base.get({id: $stateParams.dealId}, function (result) {
        vm.transactionList = result;
      });
    };

    vm.collapse= function (param){
      if(param===1){
        angular.forEach(vm.transactionList,function(transaction) {
          transaction.isCollapsed = false;
        });
      }else{
        angular.forEach(vm.transactionList,function(transaction) {
          transaction.isCollapsed = true;
        });
      }
    };

    var onErrorDateFilter= function (result){
      console.log('ERROR');
    };




    if(vm.tableauFlag){
      if($stateParams.status==='Actual'){
        vm.actual="Actual";
      }else{
        vm.actual=null
      }
      if($stateParams.status==='Estimated'){
        vm.estimated='Estimated';
      }else{
        vm.estimated=null
      }



      var monthEnd=parseInt($stateParams.monthEnd)-1;
      var startQuarter=monthEnd;
      vm.searchStartDate=moment().year($stateParams.yearEnd).month(startQuarter).date(1)._d;
      vm.searchEndDate=moment().year($stateParams.yearEnd).month(monthEnd).date(1)._d;

      vm.filterByReportingPeriod(document.getElementById('reliefForm'));
    }


    function getHighestOrderBy(array) {
      var max;
      for (var i = 0; i < array.length; i++) {
        if (array[i].orderBy > (max || 0))
          max = array[i].orderBy;
      }
      return max;
    }


    function swap(list, item1, item2){
      var temp = list[item1];
      list[item1] = list[item2];
      list[item2] = temp;
      var tempOrder = null;
      tempOrder=list[item1].orderBy;
      list[item1].orderBy=list[item2].orderBy;
      list[item2].orderBy=tempOrder
    }

    vm.move = function(list, index,direction){
      var index1,index2;
      if(direction!=='UP') {
         index1 = index;
         index2 = index + 1;
      }else{
         index1 = index;
         index2 = index - 1;
      }
      swap(list,index1,index2);

    };





  }
})();
