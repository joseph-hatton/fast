(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('DealController', DealController);

  DealController.$inject = ['$stateParams', '$state', 'Deal', 'ParseLinks', 'AlertService',
    'pagingParams', 'paginationConstants', 'TreatySrv', 'TreatyTransactionReliefSrv',
    'DealTrackingSrv', 'DealCustomSrv', '$window', 'ProductSubtype', 'GfsAssignee', '$scope', 'Country', '$filter',
    'Intro','DealType', 'ProductLine'
  ];

  function DealController($stateParams, $state, Deal, ParseLinks, AlertService,
    pagingParams, paginationConstants, TreatySrv, TreatyTransactionReliefSrv,
    DealTrackingSrv, DealCustomSrv, $window, ProductSubtype, GfsAssignee, $scope, Country, $filter, Intro,DealType, ProductLine) {
    var vm = this;
    vm.predicate = pagingParams.predicate;
    vm.reverse = pagingParams.ascending;
    vm.searchQuery = pagingParams.search;
    vm.currentSearch = pagingParams.search;
    vm.paginationObject = [];
    vm.paginationObject = $stateParams;
    vm.itemsPerPage = paginationConstants.itemsPerPage;
    vm.productSubtypes = ProductSubtype.query();
    vm.GFSAssigneeOptions = GfsAssignee.query();
    vm.countries = Country.query();
    vm.dealTypes = DealType.query();
    vm.productLines = ProductLine.query();
    vm.usStat = ['Yes', 'No'];
    vm.dealStatus = ['Active', 'Terminated'];
    vm.dealListCopy = [];
    vm.filterDeal = false;

    Intro.setupIntroSteps({id:101})
    .then(function(result){
        vm.introOptions = {
            steps: result
        };
    });

    vm.setup = function (deals, headers) {
      if (headers != null) {
        vm.links = ParseLinks.parse(headers('link'));
        vm.totalItems = headers('X-Total-Count');
      }

      if (vm.allTrackings == null) {
        DealTrackingSrv.all.get().$promise
          .then(reduceTrackingToDealIdArray)
          .then(function () {
            loadSubData(deals)
          });
      } else {
        loadSubData(deals);
      }

      vm.queryCount = vm.totalItems;
      vm.page = $window.localStorage.pageDeal;

      function reduceTrackingToDealIdArray(result) {
        if (vm.allTrackings == null)
          vm.allTrackings = result.reduce(function (r, a) {
            r[a.deal.id] = r[a.deal.id] || [];
            a.quarter = calculateQuarters(a.reportingPeriodEndDate);
            r[a.deal.id].push(a);
            return r;
          }, {});
      }

      function loadSubData(deals) {
        angular.forEach(deals, function (deal, key) {
          if (deal.trackingItem == null) {
            deal.trackingItem = vm.allTrackings[deal.id];

            TreatySrv.treatiesCount.get({
                dealId: deal.id
              }).$promise
              .then(function (result) {
                deal.treatyCount = result.amount;
              });

            TreatyTransactionReliefSrv.count.get({
                dealId: deal.id
              }).$promise
              .then(function (result) {
                deal.reliefCount = result.amount;
              });
          }
        });
      }
    };

    vm.advanceFilter = function () {
      if (vm.filterDeal) {
        TreatySrv.nopages.query().$promise.then(function (result) {
          vm.dealsList = vm.dealSelect = $filter('orderBy')(result, 'name');
          vm.setup(vm.dealsList);
          vm.originalDealsListCopy = angular.merge(vm.dealsList);
          vm.deals = vm.originalDealsListCopy;
          vm.dealStatusSelect = vm.dealStatus;
          vm.usStatSelect = vm.usStat;
          vm.GFSAssigneeSelect = vm.GFSAssigneeOptions;
          vm.productSubtypeSelect = vm.productSubtypes;
          vm.countrySelect = vm.countries;
          vm.dealTypeSelected = vm.dealTypes;
          vm.productLineSelected = vm.productLines;
        });
      } else {
        vm.clear();
        // vm.deals = vm.dealsList;
        // vm.totalItems = vm.queryCount = vm.dealsList.length;
      }
    };

    vm.filterFunc = function (value) {
      return !vm.filterDeal || ((!vm.dealSelect || vm.dealSelect.find(function (obj) {
          return obj.id === value.id;
        }) != null) &&
        (!vm.dealTypeSelected || vm.dealTypeSelected.find(function (obj) {
          return obj.id === value.dealType.id;
        }) != null) &&
        (!vm.productLineSelected || vm.productLineSelected.find(function (obj) {
          return obj.id === value.productLine.id;
        }) != null) &&
        (!vm.countrySelect || vm.countrySelect.find(function (obj) {
          return obj.id === value.country.id;
        }) != null) &&
        (!vm.productSubtypeSelect || vm.productSubtypeSelect.find(function (obj) {
          return obj.id === value.productSubtype.id;
        }) != null) &&
        (!vm.GFSAssigneeSelect || vm.GFSAssigneeSelect.find(function (obj) {
          return obj.id === value.gfsAssignee.id;
        }) != null) &&
        (!vm.usStatSelect || vm.usStatSelect.find(function (obj) {
          return obj === value.usStatRisk;
        }) != null) &&
        (!vm.dealStatusSelect || vm.dealStatusSelect.find(function (obj) {
          return obj === value.dealStatus;
        }) != null));
    };

    $scope.advancedFilterNg = function () {
      if (vm.filterDeal) {
        if (vm.dealsListCopy == null)
          vm.dealsListCopy = vm.originalDealsListCopy;
        vm.deals = $filter('filter')(vm.dealsListCopy, vm.filterFunc);

      }
    };

    vm.search = function (searchString) {
      if (searchString != null && searchString.length>0) {
        vm.currentSearch = $window.localStorage.searchString = searchString = searchString.toUpperCase();
        var page = (vm.filterDeal ? 1 : pagingParams.page - 1);
        var size = (vm.filterDeal ? 1000 : paginationConstants.itemsPerPage);
        if (vm.filterDeal) {
          vm.dealsListCopy = $filter('filter')(vm.originalDealsListCopy, function (value) {
            return value.name.includes(searchString);
          });
          $scope.advancedFilterNg();
        } else {
          DealCustomSrv.searchDeals.query({
            searchString: searchString,
            page: page,
            size: size,
            sort: sort()
          }, function (data, headers) {
            vm.setup(data, headers);
            vm.deals = data;
          }, onError);
        }
      } else {
        if (vm.filterDeal){
          vm.deals = vm.originalDealsListCopy;
     
        }
        else{
          vm.deals = vm.originalDealsCopy;
          vm.clear();
        }
      }
    };


    if ($window.localStorage.pageDeal === undefined) {
      vm.page = 0;
    }
    if ($window.localStorage.searchString != null) {
      vm.search($window.localStorage.searchString);
      vm.searchQuery = $window.localStorage.searchString;
    }

    vm.loadPage = function (page) {
      vm.page = page;
      vm.transition();
    };

    function sort() {
      var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
      if (vm.predicate !== 'id') {
        result.push('id');
      }
      return result;
    }


    function onError(error) {
      AlertService.error(error.data.message);
    }

    vm.transition = function () {
      $state.transitionTo($state.$current, {
        page: vm.page,
        sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
        search: vm.currentSearch
      });
      $window.localStorage.pageDeal = vm.page;
    };

    vm.clear = function () {
      $window.localStorage.clear();
      vm.links = null;
      // vm.page  =-1;
      // vm.predicate = 'name';
      //vm.reverse = true;
      vm.currentSearch = null;
      vm.searchQuery = null;
      vm.dealSelect = vm.dealsList = vm.originalDealsListCopy;
      vm.transition();
      if (vm.filterDeal) {
        vm.advanceFilter();
      } else {
        if (vm.originalDealsCopy == null) {
          vm.loadDeals();
        } else {
          vm.deals = vm.originalDealsCopy;
        }
        vm.queryCount = vm.totalItems = (vm.originalDealsListCopy ? vm.originalDealsListCopy.length : 0);
      }
    };

    vm.onSuccess = function (data, headers) {
      vm.deals = data;
      vm.setup(vm.deals, headers);
    };

    vm.loadDeals = function () {
      Deal.query({
        page: $window.localStorage.pageDeal - 1,
        size: paginationConstants.itemsPerPage,
        sort: sort()
      }, function (data, headers) {
        vm.onSuccess(data, headers);
        vm.originalDealsCopy = angular.merge(vm.deals);
      }, onError);
    };

    vm.loadAll = function () {
      if ($window.localStorage.pageDeal == null) {
        $window.localStorage.pageDeal = 1;
        pagingParams.page = 1;
        vm.page = 1;
      } else {
        vm.page = $window.localStorage.pageDeal;
      }

      if ($window.localStorage.searchString) {
        vm.search($window.localStorage.searchString);
      } else {
        if ($stateParams.dealId == null) {
          vm.loadDeals();
        } else {
          $state.go('deal-detail', {
            dealId: $stateParams.dealId
          }, {
            reload: true
          });
        }
      }
    };

    vm.loadAll();
  }
})();

