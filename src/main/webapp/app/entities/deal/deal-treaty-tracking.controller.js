(function () {
  'use strict';
  angular
    .module('finreApp')
    .controller('DealTreatyTrackingController', DealTreatyTrackingController);
  DealTreatyTrackingController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Deal',
    'TreatySrv', 'TreatyTrackingCustomSvr'];
  function DealTreatyTrackingController($scope, $rootScope, $stateParams, entity, Deal,
                                TreatySrv, TreatyTrackingCustomSvr) {
    var vm = this;
    vm.deal = entity;
    vm.dealId = $stateParams.dealId;
    vm.page=$stateParams.page;

    vm.yearEndMonthOptions = YEAR_END_MONTH_OPTIONS;

    Deal.get({id: $stateParams.dealId}).$promise.then(function (result) {
      vm.deal = result;

      angular.forEach ( vm.yearEndMonthOptions, function(Option,key){

        if(vm.deal.yearEndMonth===Option.id)
        {vm.deal.yearEndMonth=Option.name}
      })

    });

    TreatySrv.deals.query({id: $stateParams.dealId}).$promise.then(function (result) {
      vm.deal.treaties = result;
      angular.forEach (vm.deal.treaties, function(treaty,key){
        TreatyTrackingCustomSvr.tracking.query({treatyId:treaty.id}).$promise.then(function(trackingResults){
          treaty.treatyTrackings = trackingResults;
          angular.forEach(trackingResults, function(tracking,key){
            tracking.quarter = calculateQuarters(tracking.reportingMonthYear);
          });

        });
      })
    });

    var unsubscribe = $rootScope.$on('finreApp:dealUpdate', function (event, result) {
      vm.deal = result;
    });
    $scope.$on('$destroy', unsubscribe);

  }
})();
