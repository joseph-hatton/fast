(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('DealCustomSrv', DealCustomSrv);
  DealCustomSrv.$inject = ['$resource', 'DateUtils'];
  function DealCustomSrv($resource, DateUtils) {
    return {
      searchDeals: $resource('api/deals/existing/search/:searchString', {}, {
        query: { method: 'GET', params: {}, isArray: true }
      }),
      searchDealsExact: $resource('api/deals/existing/searchExactActive', {}, {
        query: { method: 'PUT', params: {}, isArray: true }
      })
    };
  }
})();
