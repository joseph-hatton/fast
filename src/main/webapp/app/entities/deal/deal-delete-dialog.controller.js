(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('DealDeleteController', DealDeleteController);

  DealDeleteController.$inject = ['$uibModalInstance','$stateParams', 'entity', 'Deal','TreatySrv'];

  function DealDeleteController($uibModalInstance, $stateParams,entity, Deal,TreatySrv) {
    var vm = this;
    vm.deal = entity;
    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };

    Deal.get({id: $stateParams.id}, function (result) {
      vm.deal = result;
    });

    TreatySrv.deals.query({id: $stateParams.id}, function (result) {
      vm.band = result.length;
    });


    vm.confirmDelete = function (id) {

      vm.deal.isActive = 0;
      Deal.update(vm.deal, function () {
        $uibModalInstance.close(true);
      });
    };
  }
})();
