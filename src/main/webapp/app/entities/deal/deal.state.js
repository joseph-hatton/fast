(function () {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('deal', {
        parent: 'entity',
        url: '/deal?page&sort&search',
        data: {
          authorities: ['ROLE_ADMIN','ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY','ROLE_APP_STRUCTURED_FINANCE_VIEW_ONLY','ROLE_APP_STRUCTURED_FINANCE_ADMIN'],
          pageTitle: 'finreApp.deal.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/deal/deals.html',
            controller: 'DealController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'name,asc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
            return {
              page: PaginationUtil.parsePage($stateParams.page),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate($stateParams.sort),
              ascending: PaginationUtil.parseAscending($stateParams.sort),
              search: $stateParams.search
            };
          }],
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('deal');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }]
        }
      })
      .state('deal-detail', {
        parent: 'entity',
        url: '/deal/{dealId}/{page}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY','ROLE_APP_STRUCTURED_FINANCE_VIEW_ONLY','ROLE_APP_STRUCTURED_FINANCE_ADMIN'],
          pageTitle: 'finreApp.deal.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/deal/deal-detail.html',
            controller: 'DealDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('deal');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'Deal', function ($stateParams, Deal) {
            return Deal.get({id: $stateParams.dealId}).$promise;
          }]
        }
      })
      .state('deal-treaty-tracking', {
        parent: 'entity',
        url: '/deal/treaty-tracking/{dealId}/{page}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY','ROLE_APP_STRUCTURED_FINANCE_VIEW_ONLY'],
          pageTitle: 'finreApp.deal.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/deal/deal-treaty-tracking.html',
            controller: 'DealTreatyTrackingController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('deal');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'Deal', function ($stateParams, Deal) {
            return Deal.get({id: $stateParams.dealId}).$promise;
          }]
        }
      })
      .state('deal.new', {
        parent: 'deal',
        url: '/new',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/deal/deal-dialog.html',
            controller: 'DealDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  name: null,
                  dealType: null,
                  subtype: null,
                  id: null,
                  usStatRisk: 'No',
                  dealStatus: "Active",
                  reportingRevised: "No"
                };
              }
            }
          }).result.then(function () {
          //  $state.go('^');
          }, function () {
            $state.go('deal');
          });
        }]
      })
      .state('deal.edit', {
        parent: 'deal',
        url: '/{dealId}/edit',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/deal/deal-dialog.html',
            controller: 'DealDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['Deal', function (Deal) {
                return Deal.get({id: $stateParams.dealId}).$promise;
              }]
            }
          }).result.then(function () {
            $state.go('deal-detail', {dealId: $stateParams.dealId}, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      })
      .state('deal.delete', {
        parent: 'deal',
        url: '/{id}/delete',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/deal/deal-delete-dialog.html',
            controller: 'DealDeleteController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['Deal', function (Deal) {
                return Deal.get({id: $stateParams.id}).$promise;
              }]
            }
          }).result.then(function () {
            $state.go('deal', null, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      })
      .state('deal-detail.editDetail', {
        parent: 'deal-detail',
        url: '/{dealId}/editDetail',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/deal/deal-dialog.html',
            controller: 'DealDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['Deal', function (Deal) {
                return Deal.get({id: $stateParams.dealId}).$promise;
              }]
            }
          }).result.then(function () {
            $state.go('deal-detail', {dealId: $stateParams.dealId}, {reload: true});
          }, function () {
            $state.go('deal-detail', {dealId: $stateParams.dealId}, {reload: true});
          });
        }]
      })
    ;
  }

})();
