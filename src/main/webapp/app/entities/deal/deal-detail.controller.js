(function () {
  'use strict';
  angular
    .module('finreApp')
    .controller('DealDetailController', DealDetailController);
  DealDetailController.$inject = ['$scope','$timeout','$filter', '$rootScope', '$stateParams', 'entity', 'Deal', 'Intro'];
  function DealDetailController($scope,$timeout, $filter, $rootScope, $stateParams, entity, Deal, Intro) {
    var vm = this;
    vm.deal = entity;
    vm.dealId = $stateParams.dealId;
    vm.page=$stateParams.page;
    vm.yearEndMonthOptions = YEAR_END_MONTH_OPTIONS;

    Intro.setupIntroSteps({id:186})
    .then(function(result){
        vm.introOptions = {
            steps: result
        };  
    });

    Deal.get({id: $stateParams.dealId}, function (result) {
      vm.deal = result;
      angular.forEach(vm.yearEndMonthOptions, function (Option, key) {
        if (vm.deal.yearEndMonth === Option.id) {
          vm.deal.yearEndMonth = Option.name
        }
      });
      vm.deal.effectiveDate = $filter('date')(vm.deal.effectiveDate, 'MMM dd, yyyy');
      vm.deal.terminatedDate = $filter('date')(vm.deal.terminatedDate, 'MMM dd, yyyy');
      vm.deal.maturityDate = $filter('date')(vm.deal.maturityDate, 'MMM dd, yyyy');
    });

    vm.load = function (id) { };

    var unsubscribe = $rootScope.$on('finreApp:dealUpdate', function (event, result) {
      vm.deal = result;
    });
    $scope.$on('$destroy', unsubscribe);
  }
})();
