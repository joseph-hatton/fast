(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('Deal', Deal);

  Deal.$inject = ['$resource','DateUtils'];

  function Deal($resource,DateUtils) {
    var resourceUrl = 'api/deals/:id';

    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          data.effectiveDate = DateUtils.convertLocalDateFromServer(data.effectiveDate);
          data.terminatedDate = DateUtils.convertLocalDateFromServer(data.terminatedDate);
          data.maturityDate = DateUtils.convertLocalDateFromServer(data.maturityDate);
          return data;
        }
      },
      'update': {method: 'PUT'}
    });
  }
})();
