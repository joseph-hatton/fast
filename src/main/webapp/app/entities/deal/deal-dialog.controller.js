(function () {
    'use strict';

    angular
    .module('finreApp')
    .controller('DealDialogController', DealDialogController);

    DealDialogController.$inject = ['$timeout', 'EucUse', '$state', '$mdDialog', '$stateParams', '$uibModalInstance', 'entity', 'Deal', 'Treaty', '$q',
        'ProductLine', 'ProductSubtype', 'Principal', 'GfsAssignee', 'TreatySrv', 'Country', 'InteractionsService', 'DealCustomSrv', 'TreatyTrackingCustomSvr', 'TreatyTracking', 'DealType'];

    function DealDialogController($timeout, EucUse, $state, $mdDialog, $stateParams, $uibModalInstance, entity, Deal, Treaty, $q,
        ProductLine, ProductSubtype, Principal, GfsAssignee, TreatySrv, Country, InteractionsService, DealCustomSrv, TreatyTrackingCustomSvr, TreatyTracking,DealType) {
        var vm = this;
        var tempTermDate;
        vm.deal = entity;
        vm.productLines = ProductLine.query();
        vm.productSubtypes = ProductSubtype.query();
        vm.dealTypes = DealType.query();
        vm.attTerminate = 0;
        vm.dealId = $stateParams.dealId;
        vm.duplicates = false;
        vm.duplicateMsg = '';
        vm.eucUses = [];
        vm.eucUsesC = [];
        vm.eucUsesR = [];
        vm.eucUsesRisk = [];
        vm.deal.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';

        $q.resolve(vm.deal)
        .then(function(){
            vm.yearEndMonthCache = vm.deal.yearEndMonth;
        });

        Country.query(function (result) {
            vm.Countries = result;
        });


        if ($stateParams.dealId != null) {
            TreatySrv.deals.query({id: $stateParams.dealId}, function (result) {
                vm.treaties = result;
                angular.forEach(result, function (treaty, key) {
                    if (treaty.treatyStatus === null || treaty.treatyStatus.id !== 4) {
                        vm.attTerminate = 1;
                    }
                });
            });
        }

    GfsAssignee.query(function (result) {
      vm.GFSAssigneeOptions = result;
    });

    vm.datePickerOpenStatus = {};
    vm.datePickerOpenStatus.effectiveDate = false;
    vm.datePickerOpenStatus.terminatedDate = false;
    vm.openCalendar = function (date) {
      vm.datePickerOpenStatus[date] = true;
    };

    Principal.identity().then(function (account) {
      angular.forEach(account.authorities, function (authoritie) {
        if (authoritie === "ROLE_APP_FINRE_OPS_ADMIN") {
          vm.authority = 'Admin';
        } else {
          if (vm.authority !== 'Admin') {
            vm.authority = 'User';
          }
        }
      });
      vm.lastUser = account.login;
    });

    var onSaveSuccess = function (result) {
      vm.isSaving = false;
      $timeout(function () {
        $uibModalInstance.close(result);
      }, 60);
      $state.go('deal-detail', {dealId: result.id});
    };

    var onSaveError = function () {
      vm.isSaving = false;
    };

    vm.yearEndMonthOptions = [
      {id: 1, name: 'January'},
      {id: 2, name: 'February'},
      {id: 3, name: 'March'},
      {id: 4, name: 'April'},
      {id: 5, name: 'May'},
      {id: 6, name: 'June'},
      {id: 7, name: 'July'},
      {id: 8, name: 'August'},
      {id: 9, name: 'September'},
      {id: 10, name: 'October'},
      {id: 11, name: 'November'},
      {id: 12, name: 'December'}];
    vm.yearEndDayOptions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];

        vm.save = function (form) {
            vm.isSaving = true;
            vm.duplicates = false;
            vm.deal.isActive = 1;
            vm.deal.lastUser = vm.lastUser;
            checkDuplicateAndSave(vm.deal.name, form);
        };

        function checkDuplicateAndSave(dealName, form) {
            DealCustomSrv.searchDealsExact.query(dealName).$promise
            .then(function (result) {
                if (result.length <= 0) {
                    if (vm.deal.id != null) {
                        vm.deal.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
                        return Deal.update(vm.deal, onSaveSuccess, onSaveError).$promise;
                    } else {
                        vm.deal.creationUser = vm.lastUser;
                        vm.deal.creationTs =  moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
                        return Deal.save(vm.deal, onSaveSuccess, onSaveError).$promise;
                    }
                } else if (result.length === 1 && vm.deal.id != null && vm.deal.id === result[0].id) {
                    vm.deal.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
                    return Deal.update(vm.deal, onSaveSuccess, onSaveError).$promise;
                } else {
                    var dialog = InteractionsService.alertParentScrollFalseDialog('A deal by this name already exists within FAST. Please enter a unique deal name and try again.', '#myModal');
                    InteractionsService.showThenDialog(dialog);
                }
            })
            .then(function(){
                if(vm.deal.yearEndMonth !== vm.yearEndMonthCache){
                    vm.treaties.forEach(function(treaty){
                        TreatyTrackingCustomSvr.openTracking.get({treatyId:treaty.id}).$promise
                        .then(function(result){
                            result.forEach(function(tracking){
                                if(tracking.reportingMonthYear){
                                    var monthYear = moment(tracking.reportingMonthYear).format("YYYY-MM");
                                    var date;
                                    if (treaty.reportingDueQuarterEx && treaty.reportingDueQuarterEx.period != null && treaty.reportingDueQuarterEx.numberOfDays != null && treaty.reportingDueQuarterEx.dayType != null) {
                                        date = treaty.reportingDueQuarterEx;
                                    } else {
                                        date = treaty.reportingDueDate;
                                    }
                                    TreatyTrackingCustomSvr.calculateReportingDue(tracking, treaty.reportingFrequency.name, date, monthYear, treaty.reportingFrequency.name, treaty);
                                }

                                TreatyTracking.update(tracking);
                            });
                        });
                    });
                }
            });
        }

    function onError(error) {
      AlertService.error(error.data.message);
    }

    vm.xclear = function () {
      var confirm = InteractionsService.confirmBasicParentNoScrollDialog('Are you sure you want to exit without saving?', '#myModal');
      InteractionsService.showThenDialog(confirm, function () {
        vm.clear();
      });
    };

    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };

    vm.subTypeChange = function () {
      if (vm.deal.productSubtype !== 'Structured')
        vm.deal.maturityDate = '';
      vm.deal.comments = '';
    };

    vm.dealStatusChange = function () {
      tempTermDate = vm.deal.terminatedDate ? vm.deal.terminatedDate : tempTermDate;
      if (vm.deal.dealStatus === 'Active') {
        vm.deal.terminatedDate = '';
      } else {
        vm.deal.terminatedDate = tempTermDate;
      }
      var confirm = InteractionsService.alertParentScrollFalseDialog('To Terminate this Deal, you must first Terminate all of its Treaties.', '#myModal');
      if (vm.attTerminate === 1) {
        InteractionsService.showThenDialog(confirm, function () {
            $timeout(function () {
              $uibModalInstance.dismiss();
            }, 30);
          }, function () {
            vm.deal.dealStatus = 'Active';
          }
        );
      }
    };

    EucUse.query(function (result) {
      angular.forEach(result, function (EucUses) {
        switch (EucUses.entityType){
          case 'use':
            vm.eucUses.push(EucUses);
            break;
          case 'significance':
            vm.eucUsesR.push(EucUses);
            break;
          case 'complexity':
            vm.eucUsesC.push(EucUses);
            break;
          case 'risk':
            vm.eucUsesRisk.push(EucUses);
            break;
          default:
        }
      });
    });
    vm.eucRules = function () {
      if (vm.deal.eucSignificance != null && vm.deal.eucComplexity != null) {
        var eucLow = null;
        var eucMedium = null;
        var eucHigh = null;
        angular.forEach(vm.eucUsesRisk, function (euc, key) {
          switch (euc.name){
            case 'Low':
              eucLow = euc;
              break;
            case 'Medium':
              eucMedium = euc;
              break;
            case 'High':
              eucHigh = euc;
              break;
            default:
          }
        });

        if (vm.deal.eucSignificance.name === 'Low'
          || (vm.deal.eucSignificance.name === 'Moderate' && vm.deal.eucComplexity.name === 'Low')) {
          vm.deal.eucRisk = eucLow;
        } else if (vm.deal.eucSignificance.name === 'Moderate'
          || (vm.deal.eucSignificance.name === 'Critical' && vm.deal.eucComplexity.name === 'Low')) {
          vm.deal.eucRisk = eucMedium;
        } else if (vm.deal.eucSignificance.name === 'Critical') {
          vm.deal.eucRisk = eucHigh;
        }
      }
    };
  }
})();
