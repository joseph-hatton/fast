(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('ReportsController', ReportsController);

    ReportsController.$inject = ['$scope', '$state', 'Reports', 'ReportsSearch', 'Intro'];

    function ReportsController ($scope, $state, Reports, ReportsSearch, Intro) {
        var vm = this;

        vm.reports = [];
        vm.search = search;

        Intro.setupIntroSteps({id:125})
        .then(function(result){
            vm.introOptions = {
                steps: result
            };
        });

        loadAll();

        function loadAll() {



          function getUniqueValuesOfKey(array, key){
            return array.reduce(function(carry, item){
              if(item[key] && !~carry.indexOf(item[key])) carry.push(item[key]);
              return carry;
            }, []);
          }

          vm.reportBylevel=[];
          Reports.query(function(result) {
            vm.level = getUniqueValuesOfKey(result,'reportLevel');

            angular.forEach(vm.level, function (level, key) {
              var levelR ={level:level, reports:[], status:false};
              vm.reportBylevel.push(levelR);
            });

            angular.forEach(result, function (report, key) {
                angular.forEach(vm.reportBylevel, function (reportByLvL, key) {
                if(report.reportLevel === reportByLvL.level){
                  reportByLvL.reports.push(report);
                }
                });

            });

            vm.reports = result;
          });

        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            ReportsSearch.query({query: vm.searchQuery}, function(result) {
                vm.reports = result;
            });
        }


      vm.loadAll = loadAll;

    }
})();
