(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('ReportsDialogController', ReportsDialogController);

    ReportsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Reports', 'DialogService'];

  function ReportsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Reports, DialogService) {
    this.prototype = Object.create(DialogService.prototype);

    var datePickerItemsRun = function (vm) { };

    this.reports = entity;

    DialogService.call(this, $scope, $stateParams, $uibModalInstance, entity, Reports, this.reports, "Reports", datePickerItemsRun);
  }
})();
