(function() {
    'use strict';

    angular
        .module('finreApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('reports', {
            parent: 'entity',
            url: '/reports',
            data: {
                authorities: ['ROLE_ADMIN','ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY','ROLE_APP_STRUCTURED_FINANCE_VIEW_ONLY','ROLE_APP_STRUCTURED_FINANCE_ADMIN'],
                pageTitle: 'finreApp.reports.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/reports/reports.html',
                    controller: 'ReportsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('reports');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('reports-admin', {
            parent: 'entity',
            url: '/reports/admin',
            data: {
              authorities: ['ROLE_ADMIN','ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY','ROLE_APP_STRUCTURED_FINANCE_VIEW_ONLY','ROLE_APP_STRUCTURED_FINANCE_ADMIN'],
              pageTitle: 'finreApp.reports.home.title'
            },
            views: {
              'content@': {
                templateUrl: 'app/entities/reports/reports-admin.html',
                controller: 'ReportsControllerAdmin',
                controllerAs: 'vm'
              }
            },
            resolve: {
              translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                $translatePartialLoader.addPart('reports');
                $translatePartialLoader.addPart('global');
                return $translate.refresh();
              }]
            }
          })
        .state('reports-detail', {
            parent: 'entity',
            url: '/reports/{id}',
            data: {
              authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
                pageTitle: 'finreApp.reports.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/reports/reports-detail.html',
                    controller: 'ReportsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('reports');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Reports', function($stateParams, Reports) {
                    return Reports.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'reports',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('reports-detail.edit', {
            parent: 'reports-detail',
            url: '/detail/edit',
            data: {
              authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/reports/reports-dialog.html',
                    controller: 'ReportsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Reports', function(Reports) {
                            return Reports.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('reports-admin.new', {
            parent: 'reports-admin',
            url: '/new',
            data: {
              authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/reports/reports-dialog.html',
                    controller: 'ReportsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('reports-admin', null, {reload: true});
                }, function() {
                    $state.go('reports-admin', {reload: true});
                });
            }]
        })
        .state('reports-admin.edit', {
            parent: 'reports-admin',
            url: '/{id}/edit',
            data: {
              authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/reports/reports-dialog.html',
                    controller: 'ReportsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Reports', function(Reports) {
                            return Reports.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('reports-admin', null, { reload: 'reports-admin' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('reports.delete', {
            parent: 'reports',
            url: '/{id}/delete',
            data: {
              authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/reports/reports-delete-dialog.html',
                    controller: 'ReportsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Reports', function(Reports) {
                            return Reports.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('reports', null, { reload: 'reports' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
