(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('ReportsControllerAdmin', ReportsControllerAdmin);

  ReportsControllerAdmin.$inject = ['$scope', '$state', 'Reports', 'ReportsSearch', 'Intro'];

    function ReportsControllerAdmin ($scope, $state, Reports, ReportsSearch, Intro) {
        var vm = this;

        vm.reports = [];

        loadAll();

        function loadAll() {
          angular.forEach(vm.level, function (level, key) {
            var levelR ={level:level, reports:[], status:false};
            vm.reportBylevel.push(levelR);
          });

          Reports.query(function(result) {
            vm.reports = result;
          });

        }


      vm.loadAll = loadAll;

    }
})();
