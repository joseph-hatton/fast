(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('ReportsDetailController', ReportsDetailController);

  ReportsDetailController.$inject = ['$scope', '$sce', '$rootScope', '$stateParams', 'previousState', 'entity', 'Reports'];

  function ReportsDetailController($scope, $sce, $rootScope, $stateParams, previousState, entity, Reports) {
    var vm = this;

    vm.reports = entity;
    if(vm.reports.flagFormat===0){
      vm.flagChange = 'SSRS';
    }else{
      vm.flagChange = 'TABLEAU';

    }


    vm.previousState = previousState.name;
    if (vm.flagChange === 'SSRS') {
      vm.reportLink = $sce.trustAsResourceUrl(vm.reports.reportLink);
    } else {
      vm.reportLink = $sce.trustAsResourceUrl(vm.reports.tableautLink);
    }
    vm.switchChange = function () {
      if (vm.flagChange === 'SSRS') {
        vm.reportLink = $sce.trustAsResourceUrl(vm.reports.reportLink);
      } else {
        vm.reportLink = $sce.trustAsResourceUrl(vm.reports.tableautLink);
      }
    };

    var unsubscribe = $rootScope.$on('finreApp:reportsUpdate', function (event, result) {
      vm.reports = result;
    });
    $scope.$on('$destroy', unsubscribe);

  }
})();
