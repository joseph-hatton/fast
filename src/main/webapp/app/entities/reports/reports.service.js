(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('Reports', Reports);

    Reports.$inject = ['$resource'];

    function Reports ($resource) {
        var resourceUrl =  'api/reports/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
