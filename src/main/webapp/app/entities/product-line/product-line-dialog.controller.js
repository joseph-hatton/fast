(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('ProductLineDialogController', ProductLineDialogController);

  ProductLineDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'ProductLine', 'DialogService'];

  function ProductLineDialogController ($scope, $stateParams, $uibModalInstance, entity, ProductLine, DialogService) {
    this.prototype = Object.create(DialogService.prototype);

    var datePickerItemsRun = function (vm) {};

    this.productLine = entity;

    DialogService.call(this, $scope, $stateParams, $uibModalInstance, entity, ProductLine, this.productLine, "ProductLine", datePickerItemsRun);
  }
})();
