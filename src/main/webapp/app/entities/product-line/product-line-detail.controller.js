(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('ProductLineDetailController', ProductLineDetailController);

  ProductLineDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'ProductLine', 'DetailService'];

  function ProductLineDetailController($scope, $rootScope, $stateParams, entity, ProductLine, DetailService) {
    this.prototype = Object.create(DetailService.prototype);

    this.productLine = entity;

    DetailService.call(this, $scope, $rootScope, $stateParams, ProductLine, this.productLine, "ProductLine");
  }
})();
