(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('ProductLine', ProductLine);
  
  ProductLine.$inject = ['$resource'];
  
  function ProductLine($resource) {
    var resourceUrl = 'api/product-lines/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          return data;
        }
      },
      'update': {method: 'PUT'}
    });
  }
})();
