(function() {
    'use strict';

    angular
        .module('finreApp')
        .factory('StructuredTrackingSearch', StructuredTrackingSearch);

    StructuredTrackingSearch.$inject = ['$resource'];

    function StructuredTrackingSearch($resource) {
        var resourceUrl =  'api/_search/structured-trackings/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
