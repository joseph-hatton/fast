(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('StructuredTrackingDialogController', StructuredTrackingDialogController);

  StructuredTrackingDialogController.$inject = ['$scope', '$filter', '$mdDialog', '$stateParams', '$timeout', 'Deal', 'StructuredFinance', '$uibModalInstance', 'entity', 'Principal', 'StructuredTracking', 'InteractionsService'];

  function StructuredTrackingDialogController($scope, $filter, $mdDialog, $stateParams, $timeout, Deal, StructuredFinance, $uibModalInstance, entity, Principal, StructuredTracking, InteractionsService) {
    var vm = this;
    vm.structuredTracking = entity;
    vm.dealId = $stateParams.dealId;
    vm.requirements = false;
    vm.reporting = false;
    vm.tracking = true;
    vm.dealEndMonth = '';
    vm.structuredTracking.trackingStatus = 'Open';
    vm.structuredTracking.isActive = 1;
    vm.nextTrackingGeneration = {};
    vm.format = "yyyy-MM";
    vm.yearEndMonthOptions = YEAR_END_MONTH_OPTIONS;

    // We need the timeout for the first load deal. note by Lucia Del Pino.
    $timeout(function () {
      if ($stateParams.dealId != null) {
        vm.calculateDate(vm.structuredTracking.clientReportingDate, vm.structuredTracking.structuredFinance.yearEndException, vm.structuredTracking);
        Deal.get({id: $stateParams.dealId}, function (result) {
          vm.structuredTracking.deal = result;
          vm.structuredTracking.structuredFinance.rptReqEffDate = $filter('date')(vm.structuredTracking.structuredFinance.rptReqEffDate, 'yyyy-MM-dd');
          if (result && (result.yearEndMonth > 0 && result.yearEndMonth <= 12)) {
            vm.dealEndMonth = vm.yearEndMonthOptions[result.yearEndMonth - 1];
          }
        });
      }
    }, 0);

    vm.setDateOptions = function (dateOptions, name) {
      switch (name) {
        case "Quarterly":
          dateOptions.dateDisabled = displayQuarterly;
          break;
        case "Annual":
          dateOptions.datepickerMode = 'year';
          dateOptions.minMode = 'year';
          break;
        case "Semi-Annual":
          dateOptions.dateDisabled = displaySemmiAnnual;
          break;
        default:
      }
    };

    if ($stateParams.financeId != undefined) {
      StructuredFinance.get({id: $stateParams.financeId}, function (result) {
        vm.structuredTracking.structuredFinance = result;
        vm.dateOptions = {
          datepickerMode: 'month',
          formatMonth: 'MMMM',
          formatYear: 'yyyy',
          formatMonthTitle: 'yyyy',
          minMode: 'month',
          showMonths: true,
          startingDay: 0,
          yearRange: 20,
          minDate: null,
          maxDate: null
        };
       vm.setDateOptions(vm.dateOptions, result.reportingFrequency.name);
      });
    }

    var onSaveSuccessNextTracking = function () {
      $uibModalInstance.close();
      $scope.$emit('finreApp:treatyTrackingUpdate');
      vm.isSaving = false;
    };
    var onSaveSuccess = function (result) {
      if (result.trackingStatus === 'Closed' && result.autoGenerateFlag === 1) {
        result.autoGenerateFlag = 2;
        StructuredTracking.update(result, function () {
        }, onSaveError);
        StructuredTracking.save(vm.nextTrackingRecord(result), onSaveSuccessNextTracking, onSaveError);
      } else {
        $scope.$emit('finreApp:structuredTrackingUpdate', result);
        $uibModalInstance.close(result);
        vm.isSaving = false;
      }
    };

    var onSaveError = function () {
      vm.isSaving = false;
    };

    Principal.identity().then(function (account) {
      vm.lastUser = account.login;
      vm.structuredTracking.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
    });

    vm.structuredTracking.lastUser = vm.lastUser;

    vm.save = function (flag) {
      if (flag === 1)
        vm.structuredTracking.trackingStatus = 'Open';
      vm.isSaving = true;
      if (vm.structuredTracking.id !== null) {
        vm.structuredTracking.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        StructuredTracking.update(vm.structuredTracking, onSaveSuccess, onSaveError);
      } else {
        vm.structuredTracking.creationUser = vm.lastUser;
        vm.structuredTracking.creationTs =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        vm.structuredTracking.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        vm.structuredTracking.lastUser = vm.lastUser;
        StructuredTracking.save(vm.structuredTracking, onSaveSuccess, onSaveError);
      }
    };

    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };

    vm.datePickerOpenStatus = {};
    vm.datePickerOpenStatus.clientReportingDate = false;
    vm.datePickerOpenStatus.reportingReceiptDate = false;

    vm.openCalendar = function (date) {
      vm.datePickerOpenStatus[date] = true;
    };
    vm.saveMessage = function (param) {
      var confirm = InteractionsService.confirmBasicDialog('Are you ready to change the Tracking Status to Closed?', '#myModal');
      var confirmChange = InteractionsService.confirmElaborateDialog('This Tracking Record has been Closed. Do you want to create the next tracking record?',
        'Generate', 'No', '#myModal');

      var StatusOpenAgain = InteractionsService.alertDialog('This tracking record is now incomplete, and status is changed back to Open.', '#myModal');
      if (param === 2) {
        if (vm.structuredTracking.trackingStatus === 'Closed') {
          vm.save(0);
        } else {
          InteractionsService.showThenDialog(confirm, function () {
            vm.structuredTracking.trackingStatus = 'Closed';
            InteractionsService.showThenDialog(confirmChange, function () {
              vm.structuredTracking.autoGenerateFlag = 1;
              vm.structuredTracking.trackingStatus = 'Closed';
              vm.save(0);
            }, function () {
              vm.structuredTracking.trackingStatus = 'Closed';
              vm.structuredTracking.autoGenerateFlag = 2;
              vm.save(0);
            });
          }, function () {
            vm.remainInProcess();
          });
        }
      } else if (param === 1) {
        if (vm.structuredTracking.trackingStatus !== 'Open') {
          vm.save(0);
        } else {
          InteractionsService.showThenDialog(StatusOpenAgain, function () {
            vm.structuredTracking.trackingStatus = 'Open';
            vm.save(0);
          });
        }
      }
    };
    vm.remainInProcess = function () {
      var dialog = InteractionsService.alertParentScrollFalseDialog('Tracking Status remains Open', '#myModal');
      InteractionsService.showFinallyDialog(dialog, function () {
        vm.save(0);
      });
    };

    vm.calculateDate = function(monthYear, yearEndException, structuredTracking) {
      if (monthYear == null) {
        vm.structuredTracking.calcReportingDueDate = null;
        vm.structuredTracking.calcReportingDueYex = null;
      } else {
        var month = moment(monthYear).month() + 1;
        var frequency = vm.structuredTracking.structuredFinance.reportingFrequency.name;
        var entityDate = {};
        if ((month == 10 || month == 11 || month == 12) && yearEndException === 'Yes') {
          entityDate.numberOfDays = vm.structuredTracking.structuredFinance.numberOfDaysYr;
          entityDate.dayType = vm.structuredTracking.structuredFinance.dayTypeYr;
          entityDate.period = vm.structuredTracking.structuredFinance.periodYr;
          structuredTracking.calcReportingDueDate = null;
          structuredTracking.calcReportingDueYex = calculatingDate(frequency, entityDate, monthYear, frequency);
        } else {
          entityDate.numberOfDays = vm.structuredTracking.structuredFinance.numberOfDays;
          entityDate.dayType = vm.structuredTracking.structuredFinance.dayType;
          entityDate.period = vm.structuredTracking.structuredFinance.period;
          structuredTracking.calcReportingDueDate = calculatingDate(frequency, entityDate, monthYear, frequency);
          structuredTracking.calcReportingDueYex = null;
        }
      }
    };

    vm.getNextReportingPeriodDate = function (date, Frequency) {
      switch (Frequency){
        case 'Monthly':
          date = moment(date).add(1, 'M');
          break;
        case 'Quarterly':
          date = moment(date).add(3, 'M');
          break;
        case 'Semi-Annual':
          date = moment(date).add(6, 'M');
          break;
        case 'Annual':
          date = moment(date).add(12, 'M');
          break;
        default:
      }
      return date;
    };

    vm.nextTrackingRecord = function (oldTracking) {
      vm.nextTrackingGeneration.id = null;
      vm.nextTrackingGeneration.isActive = 1;
      vm.nextTrackingGeneration.deal = oldTracking.deal;
      vm.nextTrackingGeneration.structuredFinance = oldTracking.structuredFinance;
      vm.nextTrackingGeneration.trackingStatus = 'Open';
      vm.nextTrackingGeneration.autoGenerateFlag = 1;
      vm.nextTrackingGeneration.clientReportingDate = vm.getNextReportingPeriodDate(oldTracking.clientReportingDate, oldTracking.structuredFinance.reportingFrequency.name);
      vm.calculateDate(vm.nextTrackingGeneration.clientReportingDate, oldTracking.structuredFinance.yearEndException, vm.nextTrackingGeneration);
      return vm.nextTrackingGeneration;
    };
  }
})();
