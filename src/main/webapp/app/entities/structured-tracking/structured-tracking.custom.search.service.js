(function() {
    'use strict';

    angular
        .module('finreApp')
        .factory('StructuredTrackingCustomSrv', StructuredTrackingCustomSrv);

  StructuredTrackingCustomSrv.$inject = ['$resource'];

    function StructuredTrackingCustomSrv($resource) {


      return {
        structuredTracking: $resource('api/structured-tracking-by-deal/:dealId', {}, {
          'query': {method: 'GET', isArray: true}
        }),
        structuredTrackingByFinance: $resource('api/structured-tracking-by-structured-FinanceID/:structuredFinanceId', {}, {
          'query': {method: 'GET', isArray: true}
        }),
        structuredTrackingByDate: $resource('api/structured-tracking-by-Dates/:structuredFinanceId/dates/:startYear/:startMonth/:startDay/:endYear/:endMonth/:endDay', {}, {
          'query': {method: 'GET', isArray: true},
          'get': {method: 'GET',isArray: true}
        })
    };
    }
})();
