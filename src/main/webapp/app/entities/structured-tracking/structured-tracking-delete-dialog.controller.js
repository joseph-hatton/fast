(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('StructuredTrackingDeleteController',StructuredTrackingDeleteController);

    StructuredTrackingDeleteController.$inject = ['$uibModalInstance', 'entity', 'StructuredTracking'];

    function StructuredTrackingDeleteController($uibModalInstance, entity, StructuredTracking) {
        var vm = this;
        vm.structuredTracking = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
          vm.structuredTracking.isActive=0;
            StructuredTracking.update(vm.structuredTracking,
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
