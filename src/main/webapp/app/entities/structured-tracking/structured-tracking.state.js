(function() {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('structured-tracking', {
        parent: 'entity',
        url: '/structured-tracking/{dealId}/{financeId}?page&sort&search',
        data: {
          authorities: ['ROLE_APP_STRUCTURED_FINANCE_ADMIN','ROLE_APP_STRUCTURED_FINANCE_VIEW_ONLY'],
          pageTitle: 'finreApp.structuredTracking.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/structured-tracking/structured-trackings.html',
            controller: 'StructuredTrackingController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,asc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
            return {

              page: PaginationUtil.parsePage($stateParams.page),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate($stateParams.sort),
              ascending: PaginationUtil.parseAscending($stateParams.sort),
              search: $stateParams.search
            };
          }],
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('structuredTracking');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'StructuredFinance', function($stateParams, StructuredFinance) {
            return StructuredFinance.get({id : $stateParams.financeId}).$promise;
          }]
        }
      })
      .state('structured-tracking-detail', {
        parent: 'entity',
        url: '/structured-tracking/details/{dealId}/{id}',
        data: {
          authorities: ['ROLE_APP_STRUCTURED_FINANCE_ADMIN','ROLE_APP_STRUCTURED_FINANCE_VIEW_ONLY'],
          pageTitle: 'finreApp.structuredTracking.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/structured-tracking/structured-tracking-detail.html',
            controller: 'StructuredTrackingDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('structuredTracking');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'StructuredTracking', function($stateParams, StructuredTracking) {
            return StructuredTracking.get({id : $stateParams.id}).$promise;
          }]
        }
      })
      .state('structured-tracking.new', {
        parent: 'structured-tracking',
        url: '/new/{dealId}/{financeId}',
        data: {
          authorities: ['ROLE_APP_STRUCTURED_FINANCE_ADMIN']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {

          $uibModal.open({
            templateUrl: 'app/entities/structured-tracking/structured-tracking-dialog.html',
            controller: 'StructuredTrackingDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {

              entity: function () {

                return {
                id:null
                };
              }
            }
          }).result.then(function() {
          $state.go('structured-tracking.list',{dealId: $stateParams.dealId,financeId:$stateParams.financeId}, { reload: true });
          }, function() {
          $state.go('structured-tracking.list',{dealId: $stateParams.dealId,financeId:$stateParams.financeId},{ reload: true });
          });
        }]
      })
      .state('structured-tracking.edit', {
        parent: 'structured-tracking',
        url: '/{dealId}/{financeId}/{id}/edit',
        data: {
          authorities: ['ROLE_APP_STRUCTURED_FINANCE_ADMIN']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/structured-tracking/structured-tracking-dialog.html',
            controller: 'StructuredTrackingDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['StructuredTracking', function(StructuredTracking) {
                return StructuredTracking.get({id : $stateParams.id});
              }]
            }
          }).result.then(function() {
            $state.go('structured-tracking.list',{dealId: $stateParams.dealId,financeId:$stateParams.financeId}, {reload: true });
          }, function() {
            $state.go('structured-tracking.list',{dealId: $stateParams.dealId,financeId:$stateParams.financeId}, {reload: true });
          });
        }]
      })

      .state('structured-tracking.list', {
        parent: 'entity',
        url: '/structured-tracking/list/{dealId}/{financeId}?page&sort&search',
        data: {
          authorities: ['ROLE_APP_STRUCTURED_FINANCE_ADMIN','ROLE_APP_STRUCTURED_FINANCE_VIEW_ONLY'],
          pageTitle: 'finreApp.structuredTracking.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/structured-tracking/structured-trackings.html',
            controller: 'StructuredTrackingController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,asc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
            return {
              page: PaginationUtil.parsePage($stateParams.page),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate($stateParams.sort),
              ascending: PaginationUtil.parseAscending($stateParams.sort),
              search: $stateParams.search
            };
          }],
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('global');
            $translatePartialLoader.addPart('structuredTracking');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'StructuredFinance', function($stateParams, StructuredFinance) {
            return StructuredFinance.get({id : $stateParams.financeId}).$promise;
          }]
        }
      })
      .state('structured-tracking.delete', {
        parent: 'structured-tracking.list',
        url: '/{dealId}/{financeId}/{id}/delete',
        data: {
          authorities: ['ROLE_APP_STRUCTURED_FINANCE_ADMIN']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/structured-tracking/structured-tracking-delete-dialog.html',
            controller: 'StructuredTrackingDeleteController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['StructuredTracking', function(StructuredTracking) {
                return StructuredTracking.get({id : $stateParams.id});
              }]
            }
          }).result.then(function() {
            $state.go('structured-tracking.list',{dealId: $stateParams.dealId,financeId:$stateParams.financeId}, {reload: true });
          }, function() {
            $state.go('structured-tracking.list',{dealId: $stateParams.dealId,financeId:$stateParams.financeId}, {reload: true });
          });
        }]
      });
  }
})();
