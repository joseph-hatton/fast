(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('StructuredTracking', StructuredTracking);

    StructuredTracking.$inject = ['$resource', 'DateUtils'];

    function StructuredTracking ($resource, DateUtils) {
        var resourceUrl =  'api/structured-trackings/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.clientReportingDate = DateUtils.convertLocalDateFromServer(data.clientReportingDate);
                    data.reportingReceiptDate = DateUtils.convertLocalDateFromServer(data.reportingReceiptDate);
                  data.calcReportingDueDate = DateUtils.convertLocalDateFromServer(data.calcReportingDueDate);
                  
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.clientReportingDate = DateUtils.convertLocalDateToServer(data.clientReportingDate);
                    data.reportingReceiptDate = DateUtils.convertLocalDateToServer(data.reportingReceiptDate);
                  data.calcReportingDueDate = DateUtils.convertLocalDateToServer(data.calcReportingDueDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.clientReportingDate = DateUtils.convertLocalDateToServer(data.clientReportingDate);
                    data.reportingReceiptDate = DateUtils.convertLocalDateToServer(data.reportingReceiptDate);
                  data.calcReportingDueDate = DateUtils.convertLocalDateToServer(data.calcReportingDueDate);
                    return angular.toJson(data);
                }
            }
        });
    }
})();
