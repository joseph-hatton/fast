(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('StructuredTrackingDetailController', StructuredTrackingDetailController);

    StructuredTrackingDetailController.$inject = ['$scope','$stateParams', '$rootScope','$filter', 'entity'];

    function StructuredTrackingDetailController($scope,$stateParams, $rootScope,$filter, entity) {
        var vm = this;
        vm.requirements = false;
        vm.reporting  = false;
        vm.tracking =true;
        vm.structuredTracking = entity;
        load();

        function load(){
          vm.structuredTracking.calcReportingDueDate = $filter('date')(vm.structuredTracking.calcReportingDueDate,'MMM dd, yyyy');
          vm.structuredTracking.reportingReceiptDate = $filter('date')(vm.structuredTracking.reportingReceiptDate,'MMM dd, yyyy');
          vm.structuredTracking.clientReportingDate  = $filter('date')(vm.structuredTracking.clientReportingDate ,'yyyy-MM');
//          vm.structuredTracking.structuredFinance.rptReqEffDate = $filter('date')(vm.structuredTracking.structuredFinance.rptReqEffDate,'yyyy-MM-dd');
        }

        var unsubscribe = $rootScope.$on('finreApp:structuredTrackingUpdate', function(event, result) {
            vm.structuredTracking = result;
        });

        $scope.$on('$destroy', unsubscribe);

    }
})();
