(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('StructuredTrackingController', StructuredTrackingController);

    StructuredTrackingController.$inject = ['$scope', '$state', '$stateParams', 'StructuredFinance','Deal',
      'StructuredTrackingCustomSrv', 'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants','entity'];

    function StructuredTrackingController ($scope, $state,$stateParams,StructuredFinance,Deal,
                                           StructuredTrackingCustomSrv,  ParseLinks, AlertService, pagingParams,
                                           paginationConstants, entity) {
      var vm = this;
      vm.loadPage = loadPage;
      vm.dealId = $stateParams.dealId;
      vm.financeId = $stateParams.financeId;
      vm.predicate = 'clientReportingDate';
      vm.reverse = pagingParams.descending;
      vm.transition = transition;
      vm.clear = clear;
      vm.search = search;
      vm.searchQuery = pagingParams.search;
      vm.currentSearch = pagingParams.search;
      vm.itemsPerPage = paginationConstants.itemsPerPage;
      vm.structuredFinance= entity;


      vm.searchStartDate = null;
      vm.searchEndDate = null;
    /*Custom Search */
      vm.format = "yyyy-MM";
      vm.datePickerOpenStatus = {};
      vm.datePickerOpenStatus.searchStartDate = false;
      vm.datePickerOpenStatus.searchEndDate = false;

      vm.deal = vm.structuredFinance.deal;

      loadAll();


      function loadAll () {
        StructuredTrackingCustomSrv.structuredTrackingByFinance.query({
          structuredFinanceId: $stateParams.financeId,
          page: pagingParams.page - 1,
          size: paginationConstants.itemsPerPage,
          sort: sort()
        }, onSuccess, onError);
      }

      function sort() {
        var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
        if (vm.predicate !== 'id') {
          result.push('id');
        }
        return result;
      }
      function onSuccess(data, headers) {
        angular.forEach(data, function(structuredTracking,key){
          structuredTracking.quarter = calculateQuarters(structuredTracking.clientReportingDate);
          structuredTracking.month = calculateMonth(structuredTracking.clientReportingDate);
          vm.deal = structuredTracking.deal;
          vm.dateOptions = {
            datepickerMode: 'month',
            formatMonth: 'MMMM',
            formatYear: 'yyyy',
            formatMonthTitle: 'yyyy',
            minMode: 'month',
            showMonths: true,
            startingDay: 0,
            yearRange: 20,
            minDate: null,
            maxDate: null
          };
          switch (structuredTracking.structuredFinance.reportingFrequency.name){
            case "Quarterly":
              vm.dateOptions.dateDisabled = displayQuarterly;
              break;
            case "Annual":
              vm.dateOptions.datepickerMode = 'year';
              vm.dateOptions.minMode = 'year';
              break;
            case "Semi-Annual":
              vm.dateOptions.dateDisabled = displaySemmiAnnual;
              break;
            default:
          }
        });

        vm.links = ParseLinks.parse(headers('link'));
        vm.totalItems = headers('X-Total-Count');
        vm.queryCount = vm.totalItems;
        vm.structuredTrackings = data;
        vm.page = pagingParams.page;
      }

      function onError(error) {
        AlertService.error(error.data.message);
      }

      function loadPage (page) {
          vm.page = page;
          vm.transition();
      }

      function transition () {
          $state.transitionTo($state.$current, {
              page: vm.page,
              sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
              search: vm.currentSearch,
            dealId: vm.dealId,
            financeId:vm.financeId
          });
      }

      function search (searchQuery) {
          if (!searchQuery){
              return vm.clear();
          }
          vm.links = null;
          vm.page = 1;
          vm.predicate = '_score';
          vm.reverse = false;
          vm.currentSearch = searchQuery;
          vm.transition();
      }

      function clear () {
          vm.links = null;
          vm.page = 1;
          vm.predicate = 'id';
          vm.reverse = true;
          vm.currentSearch = null;
          vm.transition();
      }

      vm.searchDates = function () {
        StructuredTrackingCustomSrv.structuredTrackingByDate.get({
          structuredFinanceId: $stateParams.financeId,
          startYear: moment(vm.searchStartDate).get('year'),
          startMonth: moment(vm.searchStartDate).get('month')+1,
          startDay: moment(vm.searchStartDate).get('date'),
          endYear: moment(vm.searchEndDate).get('year'),
          endMonth: moment(vm.searchEndDate).get('month')+1,
          endDay: moment(vm.searchEndDate).get('date'),
          page: pagingParams.page - 1,
          size: paginationConstants.itemsPerPage,
          sort: sort()
        }, onSuccess, onError);
      };
      vm.reset = function () {
        vm.searchStartDate = null;
        vm.searchEndDate = null;
        loadAll();
      };


      // Disable weekend selection
      function displayQuarterly (data) {
        return data.mode === 'month' &&
          (data.date.getMonth() === 0 || data.date.getMonth() === 1 ||
          data.date.getMonth() === 3 || data.date.getMonth() === 4 ||
          data.date.getMonth() === 6 || data.date.getMonth() === 7 ||
          data.date.getMonth() === 9 || data.date.getMonth() === 10 );
      }

      function displaySemmiAnnual (data) {
        return data.mode === 'month' &&
          (data.date.getMonth() === 0 || data.date.getMonth() === 1 || data.date.getMonth() === 2 ||
          data.date.getMonth() === 3 || data.date.getMonth() === 4 || data.date.getMonth() === 6||
          data.date.getMonth() === 7 || data.date.getMonth() === 8||
          data.date.getMonth() === 9 || data.date.getMonth() === 10 );
      }

      vm.openCalendar = function (date) {
        vm.datePickerOpenStatus[date] = true;
      };

    }
})();
