(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('CompanyMdmController', CompanyMdmController);

  CompanyMdmController.$inject = ['$mdDialog', '$window', '$scope', '$state', '$stateParams', 'CompanyCustomSrv', 'CompanySearch', 'ParseLinks', 'AlertService', 'pagingParams',
    'paginationConstants', 'Company', 'Intro', '$timeout', 'StatInfo', '$q', 'Country'
  ];

  function CompanyMdmController($mdDialog, $window, $scope, $state, $stateParams, CompanyCustomSrv, CompanySearch, ParseLinks, AlertService, pagingParams,
    paginationConstants, Company, Intro, $timeout, StatInfo, $q, Country) {
    var vm = this;

    vm.loadAll = loadAll;
    vm.predicate = 'name';
    vm.reverse = pagingParams.ascending;
    vm.transition = transition;
    vm.clear = clear;
    vm.search = search;
    vm.searchQuery = pagingParams.search;
    vm.currentSearch = pagingParams.search;
    vm.loadAll();
    vm.MdmImport = false;
    vm.searchingCompany = false;
    vm.itemsPerPage = paginationConstants.itemsPerPage;

    Intro.setupIntroSteps({
        id: 123
      })
      .then(function (result) {
        vm.introOptions = {
          steps: result
        };
      });

    Country.query(function (result) {
      vm.countries = result;
    });

    vm.statInfoTitle = "mdm_company";

    StatInfo.load.byTitle({
        title: vm.statInfoTitle
      }).$promise
      .then(function (result) {
        vm.statInfo = result;
      })
      .catch(function (reason) {
        if (reason.result.length > 1) {
          var p = [];
          reason.result.forEach(function (item) {
            item.isActive = 0;
            p.push(StatInfo.modify.update(item).$promise);
          });
          $q.all(p)
            .then(vm.refreshAllMdmCompanies);
        } else {
          vm.refreshAllMdmCompanies();
        }
      });

    $scope.$watch(function () {
      return vm.refreshingAllMdm;
    });
    $scope.$watch(function () {
      return vm.refreshCount;
    });

    vm.refreshAllMdmCompanies = function () {
      vm.refreshingAllMdm = true;
      return CompanyCustomSrv.refreshAllMdmCompanies.run().$promise
        .then(function (result) {
          vm.statInfo = vm.statInfo ? vm.statInfo : {};
          vm.statInfo.title = vm.statInfo.title ? vm.statInfo.title : vm.statInfoTitle;
          vm.statInfo.lastUpdatedCount = result.length;
          vm.statInfo.lastUpdated = moment();
          vm.statInfo.isActive = 1;
          StatInfo.modify[vm.statInfo.id ? 'update' : 'save'](vm.statInfo);
          vm.refreshingAllMdm = false;
        })
        .then(vm.loadAll);
    };

    vm.newsearch = function (searchString) {

      $window.localStorage.searchStringMDM = searchString;
      vm.mdmcompanies = [];
      vm.searchingCompany = true;
      vm.currentSearch = searchString;
      if (vm.MdmImport != false) {
        CompanyCustomSrv.searchMdmCompanies.query({
          searchString: $window.localStorage.searchStringMDM,
          page: $window.localStorage.pageMdmCompany - 1,
          size: paginationConstants.itemsPerPage //,
          // sort: sort()
        }, onSuccess, onError);
      } else {
        CompanyCustomSrv.searchMdm.query({
          searchString: $window.localStorage.searchStringMDM,
          page: pagingParams.page - 1,
          size: paginationConstants.itemsPerPage
          //sort: sort(
        }, onSuccess, onError);
      }
    };
    if ($window.localStorage.pageMdmCompany === undefined) {
      vm.page = 0;
    }
    if ($window.localStorage.searchStringMDM != undefined && $window.localStorage.searchStringMDM != null) {
      vm.search($window.localStorage.searchStringMDM);
    }

    function loadAll() {
      if ($window.localStorage.pageMdmCompany === undefined) {
        $window.localStorage.pageMdmCompany = 1;
        pagingParams.page = 1;
        vm.page = 1;
      } else {
        vm.page = $window.localStorage.pageMdmCompany;
      }
      if ($window.localStorage.searchStringMDM && $window.localStorage.searchStringMDM != null) {
        CompanyCustomSrv.companies.query({
          search: $window.localStorage.searchStringMDM,
          page: $window.localStorage.pageMdmCompany - 1,
          size: paginationConstants.itemsPerPage,
          sort: sort()
        }, onSuccess, onError);
      } else {
        CompanyCustomSrv.mdmCompanies.query({
          page: $window.localStorage.pageMdmCompany - 1,
          size: paginationConstants.itemsPerPage,
          sort: sort()
        }, onSuccess, onError);
      }
    }

    function sort() {
      var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
      if (vm.predicate !== 'id') {
        result.push('id');
      }
      return result;
    }

    function onSuccess(data, headers) {


      vm.mdmcompanies = [];
      if (vm.MdmImport === true) {
        angular.forEach(data, function (NewCompany) {
          NewCompany.import = 1;
          NewCompany.name = NewCompany.companyName;
          vm.mdmcompanies.push(NewCompany);
        });
        vm.searchingCompany = false;
      } else {
        angular.forEach(data, function (oldCompany) {
          if (oldCompany.mdmId != null) {
            oldCompany.import = 0;
            vm.mdmcompanies.push(oldCompany);
          } else {
            vm.totalItems = vm.totalItems - 1;
          }
        });

        vm.searchingCompany = false;
      }


      vm.links = ParseLinks.parse(headers('link'));
      vm.totalItems = headers('X-Total-Count');

      vm.queryCount = vm.totalItems;
      vm.page = $window.localStorage.pageMdmCompany;
    }



    function onError(error) {
      AlertService.error(error.data.message);
    }

    function loadPage(page) {
      vm.page = page;
      vm.transition();
    }

    function transition() {

      $state.transitionTo($state.$current, {
        page: vm.page,
        sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
        search: vm.currentSearch
      });
      console.log(vm.page);
      $window.localStorage.pageMdmCompany = vm.page;
    }

    function search(searchQuery) {
      if (!searchQuery) {
        return vm.clear();
      }
      vm.links = null;
      // vm.page = -1;
      vm.predicate = '_score';
      vm.reverse = false;
      vm.currentSearch = searchQuery;
      //  $window.localStorage.searchStringMDM=null;
      vm.transition();


    }

    function clear() {
      $window.localStorage.clear();
      vm.links = null;
      vm.page = 1;
      vm.predicate = 'name';
      vm.searchQuery = null;
      vm.reverse = true;
      vm.currentSearch = null;
      vm.MdmImport = false;
      loadAll();
      vm.transition();
    }

    vm.importMdmCompany = function (company) {
      console.log(company);
      var saveFlag;
      company.id = null;
      company.isActive = 1;
      company.name = company.companyName;
      if (company.legalCode === 'NOVALUE') {
        company.isRgaCompany = 0
      } else {
        company.isRgaCompany = 1
      }
      angular.forEach(vm.countries, function (country, key) {
        if (company.countryName === country.name || company.state === country.state) {
          company.country = country;
        }
      });
      Company.save(company, onImportSuccess, onError);
    };

    var onImportSuccess = function (result) {
      var confirm = $mdDialog.alert()
        .textContent('You have successfully imported ' + result.name)
        .ok('OK')
        .parent(angular.element(document.querySelector('#myModal')));
      $mdDialog.show(confirm).then(function () {
        clear();
      });
    }

  }
})();
