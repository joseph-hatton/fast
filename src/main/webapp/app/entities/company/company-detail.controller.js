(function () {
    'use strict';

    angular
    .module('finreApp')
    .controller('CompanyDetailController', CompanyDetailController);

    CompanyDetailController.$inject = ['$scope','$mdDialog', 'CompanyCustomSrv','$rootScope', '$stateParams', 'entity', 'AlertService', 'Company'];

    function CompanyDetailController($scope, $mdDialog, CompanyCustomSrv, $rootScope, $stateParams, entity, AlertService, Company) {
        var vm = this;
        vm.company = entity;
        vm.searchingCompany=false;

    // Company.get({id: $stateParams.id}, function (result) {
    //   vm.company = result;
    // });


        vm.newsearch = function(searchString) {
            console.log(vm.selectedCompany);
            vm.searchingCompany=true;
            CompanyCustomSrv.searchMdmCompanies.query({
                searchString: searchString
            }, onSuccess, onError);
        };



        vm.mergeMdmCompany= function(company){

          if(vm.selectedCompany.legalCode==='NOVALUE') {
            company.isRgaCompany=0
          }
          else{
            company.isRgaCompany =1
          }
            company.name=vm.selectedCompany.companyName;
            company.clientCompanyId=vm.selectedCompany.clientCompanyId;
            company.legalCode=vm.selectedCompany.legalCode;
            company.legalName=vm.selectedCompany.legalName;
          //  company.city=vm.selectedCompany.city;
            company.affiliateName=vm.selectedCompany.affiliateName;
            company.mdmId=vm.selectedCompany.mdmId;

            //company.state=vm.selectedCompany.state;
          //  company.country=vm.selectedCompany.country;
         Company.update(company, onMergeSuccess, onError);
        };

        var onMergeSuccess = function (result){
            var confirm = $mdDialog.alert()
        .textContent('The company ' + result.name+' Was successfully Merged' )
        .ok('OK')
        .parent(angular.element(document.querySelector('#myModal')));
            $mdDialog.show(confirm).then(function() {
                clear();
            });
        };

        function onSuccess(data, headers) {
            vm.searchingCompany=false;
            vm.companiesAbc=[];
            angular.forEach(data, function(Company,key){
                Company.name=Company.companyName;
                vm.companiesAbc.push(Company);
            });

            vm.totalItems = headers('X-Total-Count');
            vm.queryCount = vm.totalItems;
            vm.companiesAbc = data;
        }

        function onError(error) {
            console.log(error);
   //   AlertService.error(error.data.message);
        }

        function clear() {
            vm.searchQuery=null;
        }





        var unsubscribe = $rootScope.$on('finreApp:companyUpdate', function (event, result) {
            vm.company = result;
        });

        $scope.$on('$destroy', unsubscribe);

    }
})();
