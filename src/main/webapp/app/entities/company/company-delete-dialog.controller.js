(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('CompanyDeleteController', CompanyDeleteController);
  CompanyDeleteController.$inject = ['$uibModalInstance','$stateParams', 'entity', 'Company',
  'XrefTreatyCustomCompany', 'XrefTreatyCompany'];

  function CompanyDeleteController($uibModalInstance,$stateParams, entity, Company,
                                   XrefTreatyCustomCompany, XrefTreatyCompany) {
    var vm = this;
    vm.company = entity;
    vm.treatyList=[];
    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };


    XrefTreatyCustomCompany.companies.query({id: $stateParams.id}, function (result) {

      vm.xrefTreatyCompanies = result;

      angular.forEach(vm.xrefTreatyCompanies, function(xrefCompany,key){
      
        vm.treatyList.push(xrefCompany.treatyId);
      });
    });


    vm.confirmDelete = function (id) {
      angular.forEach(vm.xrefTreatyCompanies, function(xrefCompany,key){
        XrefTreatyCompany.delete({id: xrefCompany.id},
          function () {
          });
      });
      Company.delete({id: id},
        function () {
          $uibModalInstance.close(true);
      });
    };
  }
})();
