(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('CompanyCustomSrv', CompanyCustomSrv);
  CompanyCustomSrv.$inject = ['$resource', 'DateUtils'];
  function CompanyCustomSrv($resource, DateUtils) {
    return {
      companies: $resource('api/companies/search/:search', {}, {
        query: { method: 'GET', params: {}, isArray: true }
      }),
      mdmCompanies: $resource('api/companies/company-mdm', {}, {
        query: { method: 'GET', params: {}, isArray: true }
      }),
      searchMdmCompanies: $resource('api/mdmcompanies/existing-mdm/search/:searchString', {}, {
        query: { method: 'GET', params: {}, isArray: true }
      }),
      rgaCompanies: $resource('api/companies/rga-companies', {}, {
        query: { method: 'GET', params: {}, isArray: true }
      }),
      nonMdmCompanies: $resource('api/companies/non-mdm', {}, {
        query: { method: 'GET', params: {}, isArray: true }
      }),
      searchNonMdmCompanies: $resource('api/companies/non-mdm/search/:searchString', {}, {
        query: { method: 'GET', params: {}, isArray: true }
      }),

      searchMdm: $resource('api/company-mdm/search/new/:searchString', {}, {
        query: { method: 'GET', params: {}, isArray: true }
      }),
      refreshAllMdmCompanies: $resource('api/refresh-existing-mdmcompanies', {}, {
        run: { method: 'GET', params: {}, isArray: true }
      }),
    };
  }
})();
