(function () {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('company', {
        parent: 'entity',
        url: '/company/non-mdm?&page&sort&search',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.company.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/company/companies.html',
            controller: 'CompanyController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,asc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
            return {
              page: PaginationUtil.parsePage($stateParams.page),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate($stateParams.sort),
              ascending: PaginationUtil.parseAscending($stateParams.sort),
              search: $stateParams.search
            };
          }],
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('company');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }]
        }
      })
      .state('company-detail', {
        parent: 'entity',
        url: '/company/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.company.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/company/company-detail.html',
            controller: 'CompanyDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('company');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'Company', function ($stateParams, Company) {
            return Company.get({id: $stateParams.id}).$promise;
          }],
          previousState: ["$state", function ($state) {
            var currentStateData = {
              name: $state.current.name || 'car',
              params: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }]
        }
      })
      .state('company.new', {
        parent: 'company',
        url: '/new',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/company/company-dialog.html',
            controller: 'CompanyDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  name: null,
                  isRgaCompany:0,
                  affiliateName: null,
                  city: null,
                  state: null,
                  postalCode: null,
                  country: null,
                  region: null,
                  isActive: null,
                  id: null
                };
              }
            }
          }).result.then(function () {
            $state.go('company', null, {reload: true});
          }, function () {
            $state.go('company');
          });
        }]
      })
      .state('company.edit', {
        parent: 'company',
        url: '/{id}/edit',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/company/company-dialog.html',
            controller: 'CompanyDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['Company', function (Company) {
                return Company.get({id: $stateParams.id}).$promise;
              }]
            }
          }).result.then(function () {
           $state.go('company', null, {reload: true});
          }, function () {
            $state.go('company', null, {reload: true});
          });
        }]
      })
      .state('company-mdm.edit', {
        parent: 'company-mdm',
        url: '/{id}/edit/mdm',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/company/company-dialog.html',
            controller: 'CompanyDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['Company', function (Company) {
                return Company.get({id: $stateParams.id}).$promise;;
              }]
            }
          }).result.then(function () {
            $state.go('company-mdm', null, {reload: true});
          }, function () {
            $state.go('company-mdm', null, {reload: true});
          });
        }]
      })
      .state('company-detail.edit', {
        parent: 'company-detail',
        url: '/{id}/edit/detail/mdm',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/company/company-dialog.html',
            controller: 'CompanyDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['Company', function (Company) {
                return Company.get({id: $stateParams.id}).$promise;;
              }]
            }
          }).result.then(function () {
            $state.go('company-detail', {id: $stateParams.id}, {reload: true});
          }, function () {
            $state.go('company-detail', {id: $stateParams.id}, {reload: true});
          });
        }]
      })
      .state('company-mdm', {
        parent: 'entity',
        url: '/company/mdm/company-mdm?&page&sort&search',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.company.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/company/companies-mdm.html',
            controller: 'CompanyMdmController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,asc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
            return {
              page: PaginationUtil.parsePage($stateParams.page),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate($stateParams.sort),
              ascending: PaginationUtil.parseAscending($stateParams.sort),
              search: $stateParams.search
            };
          }],
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('company');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }]
        }
      })
      .state('company.delete', {
        parent: 'company',
        url: '/{id}/delete',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/company/company-delete-dialog.html',
            controller: 'CompanyDeleteController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['Company', function (Company) {
                return Company.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('company', null, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      });
  }

})();
