(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('CompanyDialogController', CompanyDialogController);
  CompanyDialogController.$inject = ['$scope','CompanyCustomSrv','$mdDialog', '$stateParams', '$uibModalInstance',
    'entity', 'Company', 'State', 'Country'];
  function CompanyDialogController($scope,CompanyCustomSrv,$mdDialog, $stateParams, $uibModalInstance,
                                   entity, Company, State, Country) {
    var vm = this;
    vm.stateLbl = "State";
    vm.zip = "Zip Code";
    vm.toggleState = true;
    vm.company = entity;
    Country.query().$promise.then(function(result) {
      vm.countries = result;
    });

    vm.selectedCountry = function () {
      if(vm.company.country!==null){
      if(vm.company.country.name === 'United States') {
        vm.stateLbl = "State";
        vm.zip = "Zip Code";
        vm.toggleState = true;
        angular.forEach(vm.states, function(state,key) {
          if (vm.company.state === state.description) {
            vm.company.state = state;
          }
        });
      } else
      {
        vm.stateLbl = "Province";
        vm.zip = "Postal Code";
        vm.toggleState = false;

        if (vm.company.state !==null && vm.company.state.description !== undefined && vm.company.state.description !== null) {
          vm.company.state = vm.company.state.description;
        }
      }
      vm.company.region = vm.company.country.region;
      }
    };

    vm.xclear = function () {
      var confirm = $mdDialog.confirm()
        .textContent('Are you sure you want to exit without saving?')
        .ok('Yes')
        .cancel('No')
        .parent(angular.element(document.querySelector('#myModal')));
      $mdDialog.show(confirm).then(function() {
        $uibModalInstance.dismiss('cancel');
      }, function() {
      });
    };
    State.query(function(result) {
      vm.states = result;
      if($stateParams.id===undefined){}
      else{
      Company.get({id: $stateParams.id}, function (result) {
        vm.company = result;
        if(vm.company.countryName != null && vm.company.country === null){
          angular.forEach(vm.countries, function (country, key) {
            if(vm.company.countryName=== country.name){
              vm.company.country=country;
            }
          });
        }
        vm.selectedCountry();
      });
      }
    });


    var onSaveSuccess = function (result) {
      $scope.$emit('finreApp:companyUpdate', result);
      $uibModalInstance.close(result);
      vm.isSaving = false;
    };

    var onSaveError = function () {
      vm.isSaving = false;
    };

    var sameCompanyName= false;
    var similar = false;

    vm.save = function () {
      if (vm.company.id !== undefined && vm.company.id !== null) {
        vm.saveConfirm();
      } else {
        CompanyCustomSrv.companies.query({search: vm.company.name}, function (result) {
          if (result.length > 0) {
            angular.forEach(result, function (company, key) {
              if (company.name.replace("*", "") === vm.company.name) {
                sameCompanyName = true;
              }
              else {
                similar = true;
              }
            })
          }

          var confirmSimilar = $mdDialog.confirm()
            .textContent('The company you are trying to add is similar to an existing company.')
            .ok('Add Anyway')
            .cancel('Cancel ')
            .parent(angular.element(document.querySelector('#myModal')));

          var SameCompany = $mdDialog.alert()
            .textContent('You cannot add this company because a company with the same name already exists.')
            .ok('OK')
            .parent(angular.element(document.querySelector('#myModal')));
          /*
           $mdDialog.show(confirm).then(function() {
           $uibModalInstance.dismiss('cancel');
           }, function() {
           });*/

          if (sameCompanyName) {
            $mdDialog.show(SameCompany).then(function () {
              $uibModalInstance.dismiss('cancel');
            });
          }
          else {
            if (similar) {
              $mdDialog.show(confirmSimilar).then(function () {
                vm.saveConfirm();
              }, function () {
                $uibModalInstance.dismiss('cancel');
              });
            }
            else {
              vm.saveConfirm();
            }
          }
        });
      }
    };

    vm.saveConfirm=function(){
      vm.isSaving = true;
      vm.company.isActive = 1;
      if (vm.company.state !== null && vm.company.state.description !== undefined) {
        vm.company.state = vm.company.state.description;
      }
      vm.company.name=vm.company.name.replace("*","");
      if (vm.company.id !== null) {

        Company.update(vm.company, onSaveSuccess, onSaveError);
      } else {
        Company.save(vm.company, onSaveSuccess, onSaveError);
      }
    };

    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };

  }
})();
