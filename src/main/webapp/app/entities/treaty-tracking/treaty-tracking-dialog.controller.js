(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('TreatyTrackingDialogController', TreatyTrackingDialogController);

  TreatyTrackingDialogController.$inject = ['$scope', '$mdDialog', '$stateParams', '$uibModalInstance', '$q', 'InteractionsService',
    'entity', 'TreatyTracking', 'Treaty', 'Principal', 'GfsAssignee', 'XrefTreatyCustomCompany', 'TreatyTrackingCustomSvr', 'DealTrackingFilterSrv', '$filter'
  ];

  function TreatyTrackingDialogController($scope, $mdDialog, $stateParams, $uibModalInstance, $q, InteractionsService,
    entity, TreatyTracking, Treaty, Principal, GfsAssignee, XrefTreatyCustomCompany, TreatyTrackingCustomSvr, DealTrackingFilterSrv, $filter) {
    var vm = this;
    vm.treatyId = $stateParams.treatyId;
    vm.dealId = $stateParams.dealId;
    vm.currencyOptions = [];
    vm.datePickerOpenStatus = {};
    vm.datePickerOpenStatus.calculatedReportingDueDate = false;
    vm.datePickerOpenStatus.reportingReceivedDate = false;
    vm.datePickerOpenStatus.cashReceivedDate = false;
    vm.datePickerOpenStatus.cashUploadDate = false;
    vm.datePickerOpenStatus.emailDate = false;
    vm.datePickerOpenStatus.clnDate = false;
    vm.datePickerOpenStatus.confirmationReceivedDate = false;
    vm.datePickerOpenStatus.reportingSentDate = false;
    vm.datePickerOpenStatus.reportingMonthYear = false;
    vm.datePickerOpenStatus.seriatimReceivedDate = false;
    vm.format = "yyyy-MM";
    vm.nextTrackingGeneration = {};
    vm.subTransactionFlag = 1;
    vm.uiFalgEndYearEx = false;
    vm.seriatimDueDate = {
      frequency: null,
      numberOfDays: null,
      dayType: null,
      period: null
    };

    vm.treatyTracking = entity;
    vm.treaty = entity.treaty;




    vm.cashDueTypeOptions = [
        "No Cash Due",
        "Assuming",
        "Ceding"
    ];

    vm.cashDueTypeInit = function(){
        if(vm.treatyTracking.cashDueFlag === 'No'){
            vm.cashDueTypeSelected = "No Cash Due";
        } else if(vm.treatyTracking.cashDueFlag === 'Yes' && vm.treatyTracking.cashDueType){
            vm.cashDueTypeSelected = vm.treatyTracking.cashDueType;
        } else {
            vm.cashDueTypeSelected = '';
        }
    };

    vm.cashDueTypeChange = function(newValue, oldValue){

        if(newValue === "Ceding" && !vm.treaty.stmntDueCed){
            var alert = InteractionsService.alertParentScrollFalseDialog("To select 'Ceding', the ceding information on the treaty must be updated.", "#myModal");
            InteractionsService.showThenDialog(alert, function(){
                vm.cashDueTypeSelected = oldValue;
            });
        } else {
            switch(vm.cashDueTypeSelected){
            case 'No Cash Due':
                  vm.treatyTracking.cashDueFlag = "No";
                  vm.treatyTracking.cashDueType = "";
                 vm.treatyTracking.calculatedAssumingGraceDate='N/A';
                 vm.treatyTracking.calculatedCedingGraceDate='N/A' ;
                break;
            case 'Assuming':
              vm.treatyTracking.cashDueFlag = "Yes";
              vm.treatyTracking.cashDueType = vm.cashDueTypeSelected;
              vm.calculateDates(vm.treatyTracking.reportingMonthYear,1);
              vm.calculateDates(vm.treatyTracking.reportingMonthYear,2);
              vm.calculateDates(vm.treatyTracking.reportingMonthYear,3);
            case 'Ceding':
                vm.treatyTracking.cashDueFlag = "Yes";
                vm.treatyTracking.cashDueType = vm.cashDueTypeSelected;
                vm.calculateDates(vm.treatyTracking.reportingMonthYear,1);
                vm.calculateDates(vm.treatyTracking.reportingMonthYear,2);
                vm.calculateDates(vm.treatyTracking.reportingMonthYear,3);
                break;
            default:
            }
        }
    };

    if (vm.treatyTracking.brokerFeeDueDate) {
      vm.treatyTracking.brokerFeeDueDate = $filter('date')(vm.treatyTracking.brokerFeeDueDate, 'yyyy-MM-dd')
    }


    /// Calcutate settlement date
    vm.reportingPeriodEndDealTracking = function (ReportingDate) {
      DealTrackingFilterSrv.reportingReportTracking.get({
        dealId: $stateParams.dealId,
        startYear: moment(ReportingDate).get('year'),
        startMonth: moment(ReportingDate).get('month') + 1,
        startDay: moment(ReportingDate).get('date')
      }, function (result) {
        if (result.length > 0) {
          vm.financeDate = result[0].financeDate;
          vm.reportingValidationDate = result[0].reportingValidationDate;
          vm.reportArmDate = result[0].reportingArmDate;
          vm.reportingRevisedFlag = result[0].reportingRevisedFlag;
          vm.password = result[0].password;
        } else {
          vm.financeDate = 'No Deal Tracking for this period';
          vm.reportingValidationDate = 'No Deal Tracking for this period';
          vm.reportArmDate = 'No Deal Tracking for this period';
          vm.reportingRevisedFlag = 'No Deal Tracking for this period';
          vm.password = 'No Deal Tracking for this period';
        }

      });

    };




    GfsAssignee.query(function (result) {
      vm.GFSAssigneeOptions = result;
    });
    Principal.identity().then(function (account) {
      vm.lastUser = account.login;
    });

    vm.reportingMonthYearChange = function () {
      vm.reportingPeriodEndDealTracking(vm.treatyTracking.reportingMonthYear);
    };
    vm.load = function () {
      return GetTreatyIfNeeded()
        .then(getCompaniesXref)
        .then(SetupSeriatimsAndCurrencies)
        .then(SetupDefaultValues);

      function getCompaniesXref() {
        var XrefPromise;

        XrefPromise =  XrefTreatyCustomCompany.treaties.query({id:  $stateParams.treatyId}).$promise.then(function (companies) {
          angular.forEach(companies, function (company, key) {
            if(company.companyType.includes('Ceding') && company.company.isRgaCompany===0) {
              vm.reportingConfirmDateFlag=false;
            }else{
              if(company.companyType.includes('Ceding') && company.company.isRgaCompany===1) {
                vm.reportingConfirmDateFlag=true;}
            }
          });
        });

        return XrefPromise ? XrefPromise : $q.resolve(true);
      }
      function GetTreatyIfNeeded() {
        var promise;
        if (vm.treaty == null) {
          promise = Treaty.get({
              id: $stateParams.treatyId
            }).$promise
            .then(function (result) {
              vm.treaty = result;
              vm.treatyTracking.treaty = result;
            });
        }
        return promise ? promise : $q.resolve(true);
      }

      function SetupSeriatimsAndCurrencies() {
        var curr = vm.treaty.treatyCurrencies;
        vm.currencyOptions = curr ? curr.map(function (t) {
          return t.currency;
        }) : [];

        var seriatim = vm.treaty.seriatimTypeTreaties ? vm.treaty.seriatimTypeTreaties.filter(function (t) {
          return t.seriatimDueDateFlag === 'Yes' && t.calculationFlag;
        }) : [];
        if (seriatim.length > 0) {
          seriatim = seriatim[0];
          vm.seriatimDueDate = seriatim.seriatimDueDate;
          vm.seriatimDueDate.frequency = seriatim.seriatimFreq;

        }
        if (!entityDateNotNull(vm.seriatimDueDate) || vm.treatyTracking.reportingMonthYear == null) {
          vm.treatyTracking.calculatedSeriatimDueDate = 'N/A';
          if (vm.treatyTracking.reportingMonthYear == undefined) {
            vm.treatyTracking.calculatedSeriatimDueDate = 'Reporting Period Not Specified';
          }

        }

      }

      function SetupDefaultValues() {

        vm.flagReportingConfirmed=((vm.treaty.stmntDueAsmGracePeriod !==null &&
        vm.treaty.stmntDueAsmGracePeriod.period==='Reporting Confirmed')
        || (vm.treaty.stmntDueCedGracePeriod !== null && vm.treaty.stmntDueCedGracePeriod.period==='Reporting Confirmed'));


        if (vm.treaty.reportingDueYearEx == null ||
          vm.treaty.reportingDueYearEx.period == null ||
          vm.treaty.reportingFrequency == null ||
          vm.treatyTracking.reportingMonthYear == null) {
          vm.treatyTracking.calculatedReportingDueYex = null;
        }
        if (vm.treaty.stmntDueCedGracePeriod == null ||
          vm.treaty.stmntDueCedGracePeriod.period == null ||
          vm.treaty.stmntDueCedGracePeriod.dayType == null) {
          vm.treatyTracking.calculatedCedingGraceDate = 'N/A';
        } else {
          var reportingConfirmDate;
          if(vm.reportingConfirmDateFlag){
            reportingConfirmDate =vm.treatyTracking.confirmationReceivedDate;
          }else{
            reportingConfirmDate =vm.treatyTracking.emailDate;
          }
          if (vm.treatyTracking.cashDueFlag === 'No')
          {
            vm.treatyTracking.calculatedCedingGraceDate='N/A';
            vm.treatyTracking.calculatedAssumingGraceDate='N/A';
          }

          if (vm.treatyTracking.reportingReceivedDate == null && vm.treatyTracking.cashDueFlag === 'Yes' ) {
            if (vm.treaty.stmntDueCedGracePeriod.period === 'Reporting Received')
              vm.treatyTracking.calculatedCedingGraceDate = "Reporting Rec'd/Sent Date Not Specified";
            if (vm.treaty.stmntDueAsmGracePeriod.period === 'Reporting Received')
              vm.treatyTracking.calculatedAssumingGraceDate = "Reporting Rec'd/Sent Date Not Specified";
          }

          if (reportingConfirmDate== null && vm.reportingConfirmDateFlag===true  && vm.treatyTracking.cashDueFlag === 'Yes' ) {
            if (vm.treaty.stmntDueCedGracePeriod.period === 'Reporting Confirmed')
              vm.treatyTracking.calculatedCedingGraceDate = "Conf. Email Rec'd Date Not Specified";
            if (vm.treaty.stmntDueAsmGracePeriod.period === 'Reporting Confirmed')
              vm.treatyTracking.calculatedAssumingGraceDate = "Conf. Email Rec'd Date Not Specified";
          }

          if (reportingConfirmDate == null && vm.reportingConfirmDateFlag===false  && vm.treatyTracking.cashDueFlag === 'Yes' ) {
            if (vm.treaty.stmntDueCedGracePeriod.period === 'Reporting Confirmed')
              vm.treatyTracking.calculatedCedingGraceDate = "Conf. Email Sent Date Not Specified";
            if (vm.treaty.stmntDueAsmGracePeriod.period === 'Reporting Confirmed')
              vm.treatyTracking.calculatedAssumingGraceDate = "Conf. Email Sent Date Not Specified";
          }
        }

        if (vm.treaty.stmntDueAsmBroker == null ||
          vm.treaty.stmntDueAsmBroker.period == null) {
          vm.treatyTracking.brokerFeeDueDate = 'N/A';
        }

        if (vm.treaty.reportingDueDate.period == null ||
          vm.treaty.reportingFrequency == null ||
          vm.treatyTracking.reportingMonthYear == null) {
          vm.treatyTracking.calculatedReportingDueDate = 'Reporting Period Not Specified';
        }
      }
    };

    $q.resolve(vm.treatyTracking)
    .then(vm.load)
    .then(function(){
        vm.treatyTracking.reportingPeriod = vm.treatyTracking.reportingPeriod ? vm.treatyTracking.reportingPeriod : '';
        vm.treatyTracking.reportingPeriod = vm.treatyTracking.reportingPeriod ? vm.treatyTracking.reportingPeriod : '';

        var options = {
            'Monthly': ['', 'Month Ending', 'Month YTD', 'Qtr Ending'],
            'Quarterly': ['Qtr Ending'],
            'Semi-Annual': ['', '6mo Ending', '12mo Ending'],
            'Annual': ['', '12mo Ending', 'Yr Ending']
        };

        if(vm.treaty.reportingFrequency){
            vm.reportingPeriodOptions = options[vm.treaty.reportingFrequency.name];
            vm.treatyTracking.reportingPeriod = vm.treaty.reportingFrequency.name === "Quarterly" ? "Qtr Ending" : vm.treatyTracking.reportingPeriod;
        }
    })
    .then(function(){
        var options = {
            'Monthly': 1,
            'Quarterly': 3,
            'Semi-Annual': 6,
            'Annual': 12
        };

        vm.dateOptions = {
            datepickerMode: 'month',
            formatMonth: 'MMMM',
            formatYear: 'yyyy',
            formatMonthTitle: 'yyyy',
            minMode: 'month',
            showMonths: true,
            dateDisabled: function(obj){
                var button = document.querySelector(".uib-datepicker-current");
                var month = moment().month() + 1;
                if(button && vm.treaty.reportingFrequency && (month % options[vm.treaty.reportingFrequency.name]) !== 0){
                    button.style.opacity = .5;
                    button.style.pointerEvents = "none";
                }


                month = obj.date.getMonth() + 1;
                return vm.treaty.reportingFrequency && (month % options[vm.treaty.reportingFrequency.name]) !== 0;
            }
        };
    });

    vm.calculateDates = function (monthYear, param) {
      calculateReporting(monthYear);
      vm.treatyTracking.calculatedAssumingDate = vm.calculateSettlement(vm.treaty.stmntDueAsm.period, vm.treaty.stmntDueAsm, monthYear, vm.treaty.settlementFrequency.name);
      if(vm.treaty.stmntDueCed && vm.treaty.stmntFrqncCeding)
        vm.treatyTracking.calculatedCedingDate = vm.calculateSettlement(vm.treaty.stmntDueCed.period, vm.treaty.stmntDueCed, monthYear, vm.treaty.stmntFrqncCeding.name);

      if (param === 1) {
        if ((vm.seriatimDueDate.frequency != null || vm.seriatimDueDate.numberOfDays != null) && monthYear != undefined) {
          vm.treatyTracking.calculatedSeriatimDueDate = calculatingDate(vm.seriatimDueDate.period, vm.seriatimDueDate, monthYear, vm.seriatimDueDate.frequency.name);
        }
        else {
          if (monthYear != undefined) {
            vm.treatyTracking.calculatedSeriatimDueDate = 'N/A';
          } else {
            vm.treatyTracking.calculatedSeriatimDueDate = 'Reporting Period Not Specified';
          }
        }

        if (vm.treaty.stmntDueCedGracePeriod !== null && vm.treaty.stmntDueCedGracePeriod.period !== 'Reporting Received' && vm.treaty.stmntDueCedGracePeriod.period !== 'Reporting Confirmed' && vm.treaty.stmntFrqncCeding !== null) {
          if (monthYear == null)
            vm.treatyTracking.calculatedCedingGraceDate = 'Reporting Period Not Specified';
          else
            vm.calculateGracePeriodCedng(vm.treaty.stmntDueCedGracePeriod.period, vm.treaty.stmntDueCedGracePeriod, monthYear, vm.treaty.stmntFrqncCeding.name);
        }

        if (vm.treaty.stmntDueAsmGracePeriod.period !== 'Reporting Received' && vm.treaty.stmntDueAsmGracePeriod.period !== 'Reporting Confirmed' && vm.treaty.settlementFrequency !== null) {
          if (monthYear == null)
            vm.treatyTracking.calculatedAssumingGraceDate = 'Reporting Period Not Specified';
          else
            vm.calculateGracePeriodAssuming(vm.treaty.stmntDueAsmGracePeriod.period, vm.treaty.stmntDueAsmGracePeriod, monthYear, vm.treaty.settlementFrequency.name);
        }
      } else if (param === 2) {
        if ( vm.treatyTracking.cashDueFlag === 'Yes'){
        if ((vm.treaty.stmntDueCedGracePeriod !== null) && (vm.treaty.stmntDueCedGracePeriod.period === 'Reporting Received')) {
          if (vm.treatyTracking.reportingReceivedDate == null  )
            vm.treatyTracking.calculatedCedingGraceDate = "Reporting Rec'd/Sent Date Not Specified";
          else
            vm.calculateGracePeriodCedng(vm.treaty.stmntDueCedGracePeriod.period, vm.treaty.stmntDueCedGracePeriod, vm.treatyTracking.reportingReceivedDate, vm.treaty.stmntFrqncCeding.name);
        }
        if (vm.treaty.stmntDueAsmGracePeriod.period === 'Reporting Received') {
          if (vm.treatyTracking.reportingReceivedDate == null  )
            vm.treatyTracking.calculatedAssumingGraceDate = "Reporting Rec'd/Sent Date Not Specified";
          else
            vm.calculateGracePeriodAssuming(vm.treaty.stmntDueAsmGracePeriod.period, vm.treaty.stmntDueAsmGracePeriod, vm.treatyTracking.reportingReceivedDate, vm.treaty.settlementFrequency.name);
        }}else
        {vm.treatyTracking.calculatedAssumingGraceDate='N/A';
          vm.treatyTracking.calculatedCedingGraceDate='N/A';

        }
      } else if (param === 3) {
        var reportingConfirmDate=null;
        var message=null;
        if ( vm.treatyTracking.cashDueFlag === 'Yes') {
          if (vm.reportingConfirmDateFlag) {
            reportingConfirmDate = vm.treatyTracking.confirmationReceivedDate;
            message = "Conf. Email Rec'd Date Not Specified";
          } else {
            reportingConfirmDate = vm.treatyTracking.emailDate;
            message = "Conf. Email Sent Date Not Specified";
          }
          if (vm.treaty.stmntDueCedGracePeriod !== null && vm.treaty.stmntDueCedGracePeriod.period === 'Reporting Confirmed' && reportingConfirmDate == null) {
            vm.treatyTracking.calculatedCedingGraceDate = message;
          } else {
            if (vm.treaty.stmntDueCedGracePeriod !== null && vm.treaty.stmntDueCedGracePeriod.period === 'Reporting Confirmed' && reportingConfirmDate != null) {
              vm.calculateGracePeriodCedng(vm.treaty.stmntDueCedGracePeriod.period, vm.treaty.stmntDueCedGracePeriod, reportingConfirmDate, vm.treaty.stmntFrqncCeding.name);
            }
          }
          if (vm.treaty.stmntDueAsmGracePeriod.period === 'Reporting Confirmed' && reportingConfirmDate == null) {
            vm.treatyTracking.calculatedAssumingGraceDate = message;
          } else {
            if (vm.treaty.stmntDueAsmGracePeriod.period === 'Reporting Confirmed' && reportingConfirmDate != null) {
              vm.calculateGracePeriodAssuming(vm.treaty.stmntDueAsmGracePeriod.period, vm.treaty.stmntDueAsmGracePeriod, reportingConfirmDate, vm.treaty.settlementFrequency.name);
            }
          }
        }else{
          vm.treatyTracking.calculatedAssumingGraceDate='N/A';
          vm.treatyTracking.calculatedCedingGraceDate='N/A';
        }
      }


      function calculateReporting(monthYear) {
        if (monthYear == null) {
          vm.treatyTracking.calculatedReportingDueDate = 'Reporting Period Not Specified';
        } else {
          var month = moment(monthYear).month();
          if ([2, 5, 8, 11].indexOf(month) > -1) {
            vm.endQuarterFlag = true;
            var date;
            if (vm.treaty.reportingDueQuarterEx != null && vm.treaty.reportingDueQuarterEx.period != null && vm.treaty.reportingDueQuarterEx.numberOfDays != null && vm.treaty.reportingDueQuarterEx.dayType != null) {
              vm.reportingQuarterFlag = true;
              date = vm.treaty.reportingDueQuarterEx;
            } else {
              vm.reportingQuarterFlag = false;
              date = vm.treaty.reportingDueDate;
            }
            TreatyTrackingCustomSvr.calculateReportingDue(vm.treatyTracking, vm.treaty.reportingFrequency.name, date, monthYear, vm.treaty.reportingFrequency.name, vm.treaty, vm);
          } else {
            vm.endQuarterFlag = false;
            vm.assumingFlagQuarter = false;
            vm.cedingFlagQuarter = false;
            vm.reportingQuarterFlag = false;
            TreatyTrackingCustomSvr.calculateReportingDue(vm.treatyTracking, vm.treaty.reportingFrequency.name, vm.treaty.reportingDueDate, monthYear, vm.treaty.reportingFrequency.name, vm.treaty, vm);
          }
        }
      }
    };

    vm.calculateReportingRec = function (dateReportingRec) {
      vm.calculateGracePeriodCedng(vm.treaty.stmntDueCedGracePeriod.period, vm.treaty.stmntDueCedGracePeriod, dateReportingRec, vm.treaty.stmntFrqncCeding.name);
      vm.calculateGracePeriodAssuming(vm.treaty.stmntDueAsmGracePeriod.period, vm.treaty.stmntDueAsmGracePeriod, dateReportingRec, vm.treaty.settlementFrequency.name);
      vm.treatyTracking.calculatedAssumingDate = vm.calculateSettlement(vm.treaty.stmntDueAsm.period, vm.treaty.stmntDueAsm, dateReportingRec, vm.treaty.settlementFrequency.name);
      if(vm.treaty.stmntDueCed)
        vm.treatyTracking.calculatedCedingDate = vm.calculateSettlement(vm.treaty.stmntDueCed.period, vm.treaty.stmntDueCed, dateReportingRec, vm.treaty.stmntFrqncCeding.name);
    };

    vm.calculateGracePeriodCedng = function (frequency, cedingEntity, monthYear, mainfrequency) {
      if (vm.endQuarterFlag && entityDateNotNull(vm.treaty.stmntDueCedQuarterEx)) {
        vm.cedingFlagQuarter = true;
        cedingEntity = vm.treaty.stmntDueCedQuarterEx;
      } else {
        vm.cedingFlagQuarter = false;
      }
      vm.treatyTracking.calculatedCedingGraceDate = calculatingDate(frequency, cedingEntity, monthYear, mainfrequency);
    };

    vm.calculateGracePeriodAssuming = function (frequency, assumingEntity, monthYear, mainfrequency) {
      if (vm.endQuarterFlag && entityDateNotNull(vm.treaty.stmntDueAsmQuarterEx)) {
        vm.assumingFlagQuarter = true;
        assumingEntity = vm.treaty.stmntDueAsmQuarterEx;
      } else {
        vm.assumingFlagQuarter = false;
      }
      vm.treatyTracking.calculatedAssumingGraceDate = calculatingDate(frequency, assumingEntity, monthYear, mainfrequency);

    };

    vm.calculateSettlement = function (frequency, assumingEntity, monthYear, mainfrequency) {
      var item;
      var reportingConfirmDate;
      if(vm.reportingConfirmDateFlag){
        reportingConfirmDate =vm.treatyTracking.confirmationReceivedDate;
      }else{
        reportingConfirmDate =vm.treatyTracking.emailDate;
      }

      switch (assumingEntity.period) {
        case "Reporting Received":
          if (vm.treatyTracking.reportingReceivedDate) {
            item = calculatingDate(frequency, assumingEntity, vm.treatyTracking.reportingReceivedDate, mainfrequency);
          }
          break;
        case "Reporting Confirmed":
          if (reportingConfirmDate) {
            item = calculatingDate(frequency, assumingEntity, reportingConfirmDate, mainfrequency);
          }
          break;
        default:
          item = calculatingDate(frequency, assumingEntity, monthYear, mainfrequency);
      }
      return item ? item : null;
    };

    var onSaveSuccessNextTracking = function () {
      $uibModalInstance.close();
      $scope.$emit('finreApp:treatyTrackingUpdate');
      vm.isSaving = false;
    };

    var onSaveSuccess = function (result) {
      if (result.trackingStatus === 'Closed' && result.autoGenerateFlag === 1) {
        result.autoGenerateFlag = 2;
        result.lastUpdate = moment().format('M/D/YYYY h:m A');
        TreatyTracking.save(TreatyTrackingCustomSvr.generateNextTracking.nextTrackingRecord({
          lastUser: vm.lastUser,
          oldTracking: result,
          nextTrackingGeneration: vm.nextTrackingGeneration,
          treatyTracking: vm.treatyTracking,
          treaty: vm.treaty,
          seriatimDueDate: vm.seriatimDueDate
        }), onSaveSuccessNextTracking, onSaveError);
      } else {
        $uibModalInstance.close(result);
        $scope.$emit('finreApp:treatyTrackingUpdate', result);
        vm.isSaving = false;
      }
    };

    function onSaveError() {
      vm.isSaving = false;
    }
    vm.save = function (flag) {
      vm.treatyTracking.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
      if (vm.treatyTracking.calculatedSeriatimDueDate === 'N/A'|| vm.treatyTracking.calculatedSeriatimDueDate === 'Reporting Period Not Specified') {
        vm.treatyTracking.calculatedSeriatimDueDate = null;
      }
      if (['Reporting Period Not Specified',
          "Reporting Rec'd/Sent Date Not Specified",
          "Conf. Email Rec'd Date Not Specified",
          "Conf. Email Sent Date Not Specified",'N/A'
        ]
        .indexOf(vm.treatyTracking.calculatedCedingGraceDate) > -1 ||
        moment(vm.treatyTracking.calculatedCedingGraceDate).get('year') === undefined) {
        vm.treatyTracking.calculatedCedingGraceDate = null;
      }

      if (['Reporting Period Not Specified',
          "Conf. Email Rec'd Date Not Specified",
          "Conf. Email Sent Date Not Specified",
          "Reporting Rec'd/Sent Date Not Specified",'N/A'
        ]
        .indexOf(vm.treatyTracking.calculatedAssumingGraceDate) > -1) {
        vm.treatyTracking.calculatedAssumingGraceDate = null;
      }
      if (vm.treatyTracking.cashSentRecDate === 'No Actual Settlements Yet' || vm.treatyTracking.cashSentRecDate === null) {
        vm.treatyTracking.cashSentRecDate = null;
        if (vm.treatyTracking.cashDueFlag === 'Yes') {
          vm.treatyTracking.trackingStatus = 'Open';
        }
      }

      if (vm.treatyTracking.brokerFeeDueDate === 'N/A') {
        vm.treatyTracking.brokerFeeDueDate = null;
      }

      vm.treatyTracking.trackingStatus = flag ? 'Open' : vm.treatyTracking.trackingStatus;

      vm.isSaving = true;
      vm.treatyTracking.lastUser = vm.lastUser;

      if (vm.treatyTracking.id !== null) {
        vm.treatyTracking.lastUpdate = moment().format();
        TreatyTracking.update(vm.treatyTracking, onSaveSuccess, onSaveError);
      } else {
        vm.treatyTracking.isActive = 1;
        vm.treatyTracking.creationUser = vm.lastUser;
        vm.treatyTracking.creationTs = moment().format();
        TreatyTracking.save(vm.treatyTracking, onSaveSuccess, onSaveError);
      }
    };
    vm.xclear = function () {
      var confirm = InteractionsService.confirmBasicParentNoScrollDialog('Are you sure you want to exit without saving?', '#myModal');
      InteractionsService.showThenDialog(confirm, function () {
        $uibModalInstance.dismiss('cancel');
      });
    };
    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };
    vm.openCalendar = function (date) {
      vm.datePickerOpenStatus[date] = true;
    };

    vm.futureDate = function (dateForm) {
      var today = new Date();
      if (dateForm != null && today.getTime() < dateForm.getTime()) {
        var confirm = InteractionsService.alertParentScrollFalseDialog('Note: ' + moment(dateForm).format("YYYY-MM-DD") + ' is a future date.', '#myModal');
        InteractionsService.showThenDialog(confirm);
      }
    };

    vm.switchChange = function (param) {
      if((vm.treaty.stmntDueAsmGracePeriod !==null && vm.treaty.stmntDueAsmGracePeriod.period==='Reporting Confirmed' )||
        (vm.treaty.stmntDueCedGracePeriod !==null &&vm.treaty.stmntDueCedGracePeriod.period==='Reporting Confirmed')){
      if(vm.reportingConfirmDateFlag){
        vm.treatyTracking.flagEmailRec=0
      }else{
        vm.treatyTracking.flagEmail=0
      }
      }
      if(param===1 &&  vm.treatyTracking.flagEmailRec!==0 ){
        vm.treatyTracking.confirmationReceivedDate=null;
      }else{
        if(param===2 &&  vm.treatyTracking.flagEmail!==0 ){
        vm.treatyTracking.emailDate=null;
        }
      }

      if(vm.treatyTracking.emailDate == null && vm.reportingConfirmDateFlag===false){
      if(vm.treaty.stmntDueAsmGracePeriod.period==='Reporting Confirmed'){
        vm.treatyTracking.calculatedAssumingGraceDate=null;
      }
      if(vm.treaty.stmntDueAsm.period==='Reporting Confirmed'){
        vm.treatyTracking.calculatedAssumingDate=null;
      }
      if(vm.treaty.stmntDueCedGracePeriod !==null && vm.treaty.stmntDueCedGracePeriod.period==='Reporting Confirmed'){
        vm.treatyTracking.calculatedCedingGraceDate=null;
      }
      if(vm.treaty.stmntDueCed !== null && vm.treaty.stmntDueCed.period==='Reporting Confirmed'){
        vm.treatyTracking.calculatedCedingDate=null;
      }
      }

      if(vm.treatyTracking.confirmationReceivedDate == null && vm.reportingConfirmDateFlag){
        if(vm.treaty.stmntDueAsmGracePeriod.period==='Reporting Confirmed'){
          vm.treatyTracking.calculatedAssumingGraceDate=null;
        }
        if(vm.treaty.stmntDueAsm.period==='Reporting Confirmed'){
          vm.treatyTracking.calculatedAssumingDate=null;
        }
        if(vm.treaty.stmntDueCedGracePeriod !==null && vm.treaty.stmntDueCedGracePeriod.period==='Reporting Confirmed'){
          vm.treatyTracking.calculatedCedingGraceDate=null;
        }
        if(vm.treaty.stmntDueCed !==null && vm.treaty.stmntDueCed.period==='Reporting Confirmed'){
          vm.treatyTracking.calculatedCedingDate=null;
        }

      }

    };
    vm.saveMessage = function () {
      if (vm.treatyTracking.trackingStatus === 'Closed') {
        vm.treatyTracking.autoGenerateFlag = 2;
        vm.save(0);
      } else {
        if (vm.treatyTracking.cashDueFlag === 'Yes' && vm.treatyTracking.cashSentRecDate !== 'No Actual Settlements Yet' && vm.treatyTracking.cashSentRecDate !== null) {
          vm.closeTrackingDialog();
        } else {
          if (vm.treatyTracking.cashDueFlag === 'No') {
            vm.closeTrackingDialog();
          } else {
            vm.save(0);
          }
        }
      }
    };
    vm.closeTrackingDialog = function () {
      var confirm = InteractionsService.confirmBasicParentNoScrollDialog('Are you ready to Close this tracking record?', '#myModal');
      var confirmChange = InteractionsService.confirmBasicParentNoScrollDialog('This Tracking Record has been Closed. Do you want to create the next tracking record?', '#myModal');

      InteractionsService.showThenDialog(confirm, function () {
        vm.treatyTracking.trackingStatus = 'Closed';
        if (vm.treaty.disabledTracking) {
          vm.treatyTracking.autoGenerateFlag = 2;
          vm.save(0);
        } else {
          InteractionsService.showThenDialog(confirmChange, function () {
            vm.treatyTracking.autoGenerateFlag = 1;
            vm.save(0);
          }, function () {
            vm.treatyTracking.autoGenerateFlag = 2;
            vm.save(0);
          });
        }
      }, function () {
        vm.remainInProcess();
      });
    };

    vm.remainInProcess = function () {
      var alert = InteractionsService.alertParentScrollFalseDialog('Tracking Status remains Open', '#myModal');
      InteractionsService.showThenDialog(alert, function () {
        vm.save(0);
      });
    };

    vm.sync = function () {
      if (vm.treatyTracking.reportingMonthYear) {
        vm.reportingPeriodEndDealTracking(vm.treatyTracking.reportingMonthYear);
        var month = moment(vm.treatyTracking.reportingMonthYear).month();
        if (month === (vm.treaty.deal.yearEndMonth-1) && vm.treaty.yearEndException === 'Yes') {
          vm.uiFalgEndYearEx = true;
          vm.reportingQuarterFlag = false;
          vm.treatyTracking.calculatedReportingDueDate = null;
        }
        if ([2, 5, 8, 11].indexOf(month) > -1) {
          vm.endQuarterFlag = true;
          vm.reportingQuarterFlag = vm.endQuarterFlag && entityDateNotNull(vm.treaty.reportingDueQuarterEx);
          vm.assumingFlagQuarter = vm.endQuarterFlag && entityDateNotNull(vm.treaty.stmntDueAsmQuarterEx);
          vm.cedingFlagQuarter = vm.endQuarterFlag && entityDateNotNull(vm.treaty.stmntDueCedQuarterEx);
        } else {
          vm.endQuarterFlag = false;
          vm.assumingFlagQuarter = false;
          vm.cedingFlagQuarter = false;
          vm.reportingQuarterFlag = false;
        }
      }


    };

    vm.sync();

    function entityDateNotNull(date) {
      return date != null && date.period != null && date.dayType != null && date.numberOfDays != null;
    }

    var oldIsDisabled = $scope.isDisabled;



    $scope.isDisabled = function(date){
      var old = oldIsDisabled(date);
      var date;

      if (date === 'today' || date === 'now')
        date = uibDateParser.fromTimezone(new Date(), timezone);

      return old && !vm.dateOptions['dateDisabled'](date);
    }
  }
})();
