(function () {
  'use strict';

  angular
    .module('finreApp')
    .factory('TreatyTrackingSearch', TreatyTrackingSearch);

  TreatyTrackingSearch.$inject = ['$resource'];

  function TreatyTrackingSearch($resource) {
    var resourceUrl = 'api/_search/treaty-trackings/:id';

    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true}
    });
  }
})();
