(function () {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('treaty-tracking', {
        parent: 'entity',
        url: '/treaty-tracking?page&sort&search',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN', 'ROLE_APP_FINRE_PROCESSOR', 'ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treatyTracking.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty-tracking/treaty-trackings.html',
            controller: 'TreatyTrackingController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,asc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
            return {
              page: PaginationUtil.parsePage($stateParams.page),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate($stateParams.sort),
              ascending: PaginationUtil.parseAscending($stateParams.sort),
              search: $stateParams.search
            };
          }],
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treatyTracking');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }]
        }
      })
      .state('treaty-tracking-detail', {
        parent: 'entity',
        url: '/treaty-tracking/treaty/{dealId}/{treatyId}/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN', 'ROLE_APP_FINRE_PROCESSOR', 'ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treatyTracking.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty-tracking/treaty-tracking-detail.html',
            controller: 'TreatyTrackingDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treatyTracking');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'TreatyTracking', function ($stateParams, TreatyTracking) {
            return TreatyTracking.get({
              id: $stateParams.id
            }).$promise;
          }]
        }
      })
      .state('treaty-tracking.newTracking', {
        parent: 'treaty-tracking',
        url: '/new-tracking/{treatyId}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN', 'ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-tracking/treaty-tracking-dialog.html',
            controller: 'TreatyTrackingDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  id: null,
                  treaty: null,
                  flagEmail: 0,
                  flagEmailRec: 0,
                  reportNotifFlag:0,
                  settlementDue: 'Assuming',
                  trackingStatus: 'Open',
                  reportingMonthYear: null,
                  password: 'N/A',
                  autoGenerateFlag: 1,
                  calculatedAssumingGraceDate: 'Reporting Period Not Specified',
                  calculatedCedingGraceDate: 'Reporting Period Not Specified',
                  calculatedReportingDueDate: 'Reporting Period Not Specified'
                };
              }
            }
          }).result.then(function () {
            $state.go('treaty-tracking-all', {
              treatyId: $stateParams.treatyId
            }, {
              reload: true
            });
          }, function () {
            $state.go('treaty-tracking-all', {
              treatyId: $stateParams.treatyId
            }, {
              reload: true
            });
          });
        }]
      })
      .state('treaty-tracking-all.new', {
        parent: 'treaty-tracking-all',
        url: '/new/{dealId}/{treatyId}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN', 'ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-tracking/treaty-tracking-dialog.html',
            controller: 'TreatyTrackingDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  id: null,
                  treaty: null,
                  flagEmail: 0,
                  flagEmailRec: 0,
                  reportNotifFlag:0,
                  reportingMonthYear: null,
                  settlementDue: 'Assuming',
                  trackingStatus: 'Open',
                  password: 'N/A',
                  autoGenerateFlag: 1,
                  calculatedAssumingGraceDate: 'Reporting Period Not Specified',
                  calculatedCedingGraceDate: 'Reporting Period Not Specified',
                  calculatedReportingDueDate: 'Reporting Period Not Specified'
                };
              }
            }
          }).result.then(function () {
            $state.go('treaty-tracking-all', {
              dealId: $stateParams.dealId,
              treatyId: $stateParams.treatyId
            }, {
              reload: true
            });
          }, function () {
            $state.go('treaty-tracking-all', {
              dealId: $stateParams.dealId,
              treatyId: $stateParams.treatyId
            }, {
              reload: true
            });
          });
        }]
      })
      .state('treaty-tracking-all.edit', {
        parent: 'treaty-tracking-all',
        url: '/edit-tracking/{dealId}/{treatyId}/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN', 'ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-tracking/treaty-tracking-dialog.html',
            controller: 'TreatyTrackingDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['TreatyTracking', function (TreatyTracking) {
                return TreatyTracking.get({
                  id: $stateParams.id
                }).$promise;
              }]
            }
          }).result.then(function () {
            $state.go('treaty-tracking-all', {
              dealId: $stateParams.dealId,
              treatyId: $stateParams.treatyId
            }, {
              reload: true
            });
          }, function () {
            $state.go('treaty-tracking-all', {
              dealId: $stateParams.dealId,
              treatyId: $stateParams.treatyId
            }, {
              reload: true
            });
          });
        }]
      })
      .state('treaty-tracking-all', {
        parent: 'entity',
        url: '/treaty-tracking/treaties/{treatyId}/{dealId}?page&sort&search',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN', 'ROLE_APP_FINRE_PROCESSOR', 'ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treatyTracking.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty-tracking/treaty-trackings.html',
            controller: 'TreatyTrackingController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,asc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
            return {
              page: PaginationUtil.parsePage($stateParams.page),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate($stateParams.sort),
              ascending: PaginationUtil.parseAscending($stateParams.sort),
              search: $stateParams.search
            };
          }],
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treatyTracking');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'TreatyTrackingCustomSvr', function ($stateParams, TreatyTrackingCustomSvr) {
            return TreatyTrackingCustomSvr.tracking.query({
              dealId: $stateParams.dealId,
              treatyId: $stateParams.treatyId
            });
          }]
        }
      })
      .state('treaty-tracking-all.delete', {
        parent: 'treaty-tracking-all',
        url: '/delete/{dealId}/{treatyId}/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN', 'ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-tracking/treaty-tracking-delete-dialog.html',
            controller: 'TreatyTrackingDeleteController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['TreatyTracking', function (TreatyTracking) {
                return TreatyTracking.get({
                  id: $stateParams.id
                });
              }]
            }
          }).result.then(function () {
            $state.go('treaty-tracking-all', {
              dealId: $stateParams.dealId,
              treatyId: $stateParams.treatyId
            }, {
              reload: true
            });
          }, function () {
            $state.go('treaty-tracking-all', {
              dealId: $stateParams.dealId,
              treatyId: $stateParams.treatyId
            }, {
              reload: true
            });
          });
        }]
      })
      .state('treaty-tracking-detail.edit-detail', {
        parent: 'treaty-tracking-detail',
        url: '/edit-detail-tracking/{dealId}/{treatyId}/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN', 'ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-tracking/treaty-tracking-dialog.html',
            controller: 'TreatyTrackingDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['TreatyTracking', function (TreatyTracking) {
                return TreatyTracking.get({
                  id: $stateParams.id
                }).$promise;
              }]
            }
          }).result.then(function () {
            $state.go('treaty-tracking-detail', {
              dealId: $stateParams.dealId,
              treatyId: $stateParams.treatyId,
              id: $stateParams.id
            }, {
              reload: true
            });
          }, function () {
            $state.go('treaty-tracking-detail', {
              dealId: $stateParams.dealId,
              treatyId: $stateParams.treatyId,
              id: $stateParams.id
            }, {
              reload: true
            });
          });
        }]
      });
  }

})();
