(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('TreatyTracking', TreatyTracking);

  TreatyTracking.$inject = ['$resource', 'DateUtils'];

  function TreatyTracking($resource, DateUtils) {
    var resourceUrl = 'api/treaty-trackings/:id';

    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          if (data === '') return null;
          data = angular.fromJson(data);
          data.reportingMonthYear = DateUtils.convertLocalDateFromServer(data.reportingMonthYear);
          data.reportingReceivedDate = DateUtils.convertLocalDateFromServer(data.reportingReceivedDate);
          data.cashUploadDate = DateUtils.convertLocalDateFromServer(data.cashUploadDate);
          data.emailDate = DateUtils.convertLocalDateFromServer(data.emailDate);
          data.confirmationReceivedDate = DateUtils.convertLocalDateFromServer(data.confirmationReceivedDate);
          data.reportingSentDate = DateUtils.convertLocalDateFromServer(data.reportingSentDate);
          data.brokerFeeDueDate = DateUtils.convertLocalDateFromServer(data.brokerFeeDueDate);
          data.seriatimReceivedDate = DateUtils.convertLocalDateFromServer(data.seriatimReceivedDate);
          return data;
        }
      },
      'update': {
        method: 'PUT',
        transformRequest: function (data) {
          if (data === "" || data === null || data === undefined) return null;
          data.reportingMonthYear = DateUtils.convertLocalDateToServer(data.reportingMonthYear);
          data.reportingReceivedDate = DateUtils.convertLocalDateToServer(data.reportingReceivedDate);
          data.cashUploadDate = DateUtils.convertLocalDateToServer(data.cashUploadDate);
          data.emailDate = DateUtils.convertLocalDateToServer(data.emailDate);
          data.confirmationReceivedDate = DateUtils.convertLocalDateToServer(data.confirmationReceivedDate);
          data.reportingSentDate = DateUtils.convertLocalDateToServer(data.reportingSentDate);
          data.brokerFeeDueDate = DateUtils.convertLocalDateToServer(data.brokerFeeDueDate);
          data.seriatimReceivedDate = DateUtils.convertLocalDateToServer(data.seriatimReceivedDate);

          return angular.toJson(data);
        }
      },
      'save': {
        method: 'POST',
        transformRequest: function (data) {
          data.reportingMonthYear = DateUtils.convertLocalDateToServer(data.reportingMonthYear);
          data.reportingReceivedDate = DateUtils.convertLocalDateToServer(data.reportingReceivedDate);
          data.cashUploadDate = DateUtils.convertLocalDateToServer(data.cashUploadDate);
          data.financeDate = DateUtils.convertLocalDateToServer(data.financeDate);
          data.emailDate = DateUtils.convertLocalDateToServer(data.emailDate);
          data.confirmationReceivedDate = DateUtils.convertLocalDateToServer(data.confirmationReceivedDate);
          data.reportingSentDate = DateUtils.convertLocalDateToServer(data.reportingSentDate);
          data.brokerFeeDueDate = DateUtils.convertLocalDateToServer(data.brokerFeeDueDate);
          data.seriatimReceivedDate = DateUtils.convertLocalDateToServer(data.seriatimReceivedDate);
          return angular.toJson(data);
        }
      }
    });
  }
})();
