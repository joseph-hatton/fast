(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('TreatyTrackingController', TreatyTrackingController);

  TreatyTrackingController.$inject = ['$state','XrefTreatyCustomCompany', '$stateParams', 'TreatyTracking', 'TreatyTrackingSearch',
    'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants','TreatyTrackingCustomSvr', 'Treaty','TreatyTransactionSrv', 'InteractionsService', 'Principal', '$scope', 'Intro'];
  function TreatyTrackingController( $state,XrefTreatyCustomCompany, $stateParams, TreatyTracking, TreatyTrackingSearch,
                                    ParseLinks, AlertService, pagingParams, paginationConstants,TreatyTrackingCustomSvr, Treaty,TreatyTransactionSrv, InteractionsService, Principal, $scope, Intro) {
    var vm = this;
    vm.treatyId = $stateParams.treatyId;
    vm.dealId = $stateParams.dealId;
    vm.treatyTrackings = [];
    vm.loadAll = loadAll;
    vm.loadPage = loadPage;
    vm.predicate = pagingParams.predicate;
    vm.reverse = pagingParams.ascending;
    vm.transition = transition;
    vm.clear = clear;
    // vm.search = search;
    vm.searchQuery = pagingParams.search;
    vm.currentSearch = pagingParams.search;
    vm.searchStartDate = null;
    vm.searchEndDate = null;
    vm.dupRecords = false;
    vm.trackingYear = null;
    vm.trackingQuarter = null;
    vm.invalidTreatyMsg = '';
    vm.invalidTreaty = true;
    vm.displayFrequency = "Month's End";
    vm.flagReportingDueDate=false;
    vm.deleteMultipleTracking=[];
    vm.itemsPerPage = paginationConstants.itemsPerPage;

    vm.trackingYears = [2015,2016,2017,2018,2019,2020,2021,2022,2023,2024,2025,2026,2027,2028,2029,2030,
      2031,2032,2033,2034,2035,2036,2037,2038,2039,2040];

      Intro.setupIntroSteps({id:168})
      .then(function(result){
          vm.introOptions = {
              steps: result
          };
      });

    vm.go = function (location, obj) {
      $state.go(location, obj);
    };

    XrefTreatyCustomCompany.treaties.query({id:  $stateParams.treatyId}).$promise.then(function (companies) {
      angular.forEach(companies, function (company, key) {
        if(company.companyType.includes('Ceding') && company.company.isRgaCompany===0) {
          vm.reportingConfirmDateFlag=false;
        }else{
          if(company.companyType.includes('Ceding') && company.company.isRgaCompany===1) {
            vm.reportingConfirmDateFlag=true;}
        }
      });
    });

    vm.datePickerOpenStatus = {};
    vm.datePickerOpenStatus.searchStartDate = false;
    vm.datePickerOpenStatus.searchEndDate = false;

    vm.openCalendar = function (date) {
      vm.datePickerOpenStatus[date] = true;
    };


    vm.format = "yyyy-MM";
    vm.dateOptions = {
      datepickerMode: 'month',
      formatMonth: 'MMMM',
      formatYear: 'yyyy',
      formatMonthTitle: 'yyyy',
      minMode: 'month',
      showMonths: true,
      startingDay: 0,
      yearRange: 20,
      minDate: null,
      maxDate: null
    };

    vm.onSuccess = function (data, headers) {
      vm.getTreatyInformation();
      angular.forEach(data, function(tracking,key){
        tracking.quarter = calculateQuarters(tracking.reportingMonthYear);
        tracking.month = calculateMonth(tracking.reportingMonthYear);
      });
      vm.links = ParseLinks.parse(headers('link'));
      vm.totalItems = headers('X-Total-Count');
      vm.queryCount = vm.totalItems;
      vm.treatyTrackings = data;
      vm.page = pagingParams.page;
    };

    function loadAll() {

      // if (pagingParams.search) {
      //   TreatyTrackingSearch.query({
      //     query: pagingParams.search,
      //     page: pagingParams.page - 1,
      //     size: paginationConstants.itemsPerPage,
      //     sort: sort()
      //   }, vm.onSuccess, onError);
      // } else {
        if($stateParams.treatyId != null) {
          TreatyTrackingCustomSvr.tracking.get({
            treatyId: $stateParams.treatyId,
            page: pagingParams.page - 1,
            size: paginationConstants.itemsPerPage,
            sort: sort()
          }, vm.onSuccess, onError);
        }
      // }
    }

    vm.loadAll();

    function sort() {
      if(pagingParams.sort === 'id,asc'){
        vm.predicate = "reportingMonthYear";
        vm.reverse = false;
      }
      var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
      if (vm.predicate !== 'id') {
        result.push('id');
      }
      return result;
    }

    vm.searchDates = function () {
      if(vm.predicate == null || vm.predicate === 'id'){
        vm.predicate = 'reportingMonthYear';
        vm.reverse = false;
      }
      TreatyTrackingCustomSvr.byDates.get({
        treatyId: $stateParams.treatyId,
        startYear: moment(vm.searchStartDate).get('year'),
        startMonth: moment(vm.searchStartDate).get('month')+1,
        startDay: moment(vm.searchStartDate).get('date'),
        endYear: moment(vm.searchEndDate).get('year'),
        endMonth: moment(vm.searchEndDate).get('month')+1,
        endDay: moment(vm.searchEndDate).get('date'),
        page: pagingParams.page - 1,
        size: paginationConstants.itemsPerPage,
        sort: sort()
      }, vm.onSuccess, onError);
    };

    vm.reset = function () {
      vm.searchStartDate = null;
      vm.searchEndDate = null;
      vm.loadAll();
    };

    function onError(error) {
      AlertService.error(error.data.message);
    }

    function loadPage(page) {
      vm.page = page;
      vm.transition();
    }

    vm.getTreatyInformation = function () {

      function checkDate(date) {
        return date != null && date.dayType != null && date.period != null && date.numberOfDays != null;
      }

      Treaty.get({id: $stateParams.treatyId}, function (result) {
        vm.treaty = result;

        vm.flagReportingDueDate = vm.treaty.reportingFrequency != null
          && checkDate(vm.treaty.reportingDueDate)
          && checkDate(vm.treaty.stmntDueAsmGracePeriod);

        vm.invalidTreatyMsg = (vm.treaty.effectiveDate === null ? "Treaty Effective Date is Null. " : "")
          + (vm.treaty.reportingFrequency === null ? "Reporting Frequency is not specified" : "");

        vm.invalidTreaty = (vm.treaty.effectiveDate === null) || (vm.treaty.reportingFrequency === null);

        if(vm.treaty.reportingFrequency !== null)
          vm.checkFrequency(vm.treaty);
      });
    };

    vm.checkFrequency = function(treaty){
      vm.trackingfrequency = [];
      angular.forEach(treaty.entityDates, function (entity,key) {
        if (entity.entityType === 'reportingdate') {
          if (vm.treaty.reportingFrequency.name === 'Monthly' ||
            vm.treaty.reportingFrequency.name === 'Quarterly') {
            vm.trackingfrequency = [
              { frequency: 1, name: "1st Quarter"},
              { frequency: 2, name: "2nd Quarter"},
              { frequency: 3, name: "3rd Quarter"},
              { frequency: 4, name: "4th Quarter"}];
          } else  if (vm.treaty.reportingFrequency.name === 'Semi-Annual') {
            vm.trackingfrequency = [
              {frequency: 5, name: "1st Half"},
              {frequency: 11, name: "Last Half"}];
          } else  if (vm.treaty.reportingFrequency.name === 'Annual') {
            vm.trackingfrequency = [
              {frequency: 11, name: "Year End"}];
          } else {
            vm.trackingfrequency = [
              {frequency: 0, name: "No Information available"}];
          }
          vm.displayFrequency = entity.period;
        }
      });
    };

    function transition() {
      $state.transitionTo($state.$current, {
        treatyId: $stateParams.treatyId,
        dealId: $stateParams.dealId,
        page: vm.page,
        sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
        search: vm.currentSearch
      });
    }

    // function search(searchQuery) {
    //   if (!searchQuery) {
    //     return vm.clear();
    //   }
    //   vm.links = null;
    //   vm.page = 1;
    //   vm.predicate = '_score';
    //   vm.reverse = false;
    //   vm.currentSearch = searchQuery;
    //   vm.transition();
    // }

    function clear() {
      vm.links = null;
      vm.page = 1;
      vm.predicate = 'id';
      vm.reverse = true;
      vm.currentSearch = null;
      vm.transition();
    }

    // var onSaveSuccess = function (result) {
    //   loadAll();
    //   vm.isSaving = false;
    // };

    var onSaveError = function () {
      vm.isSaving = false;
    };

    // vm.createTracking = function () {
    //   var tracks = [];
    //   tracks = vm.createTracks(tracks);
    //   var dups = vm.checkDuplicateTrackingRecords(tracks, vm.trackingYear);
    //   if(dups) {
    //     vm.dupRecords = true;
    //     return;
    //   }
    //   vm.dupRecords = false;
    //
    //   angular.forEach(tracks, function (track,key) {
    //     var treatyTracking = initTracking(track, vm.trackingYear);
    //     TreatyTracking.save(treatyTracking, onSaveSuccess, onSaveError);
    //   });
    //
    // };
    //
    // vm.createTracks = function(tracks){
    //   if (vm.treaty.reportingFrequency.name === 'Monthly') {
    //     if (vm.trackingQuarter.frequency === 1) {
    //       tracks = [0, 1, 2];
    //     } else if (vm.trackingQuarter.frequency === 2) {
    //       tracks = [3, 4, 5];
    //     } else if (vm.trackingQuarter.frequency === 3) {
    //       tracks = [6, 7, 8];
    //     } else {
    //       tracks = [9, 10, 11];
    //     }
    //   } else if (vm.treaty.reportingFrequency.name === 'Quarterly') {
    //     if (vm.trackingQuarter.frequency === 1) {
    //       tracks = [2];
    //     } else if (vm.trackingQuarter.frequency === 2) {
    //       tracks = [5];
    //     } else if (vm.trackingQuarter.frequency === 3) {
    //       tracks = [8];
    //     } else {
    //       tracks = [11];
    //     }
    //   } else  if (vm.treaty.reportingFrequency.name === 'Semi-Annual' || vm.treaty.reportingFrequency.name === 'Annual') {
    //     tracks = [vm.trackingQuarter.frequency];
    //   } else {
    //     tracks = [];
    //   }
    //   return tracks;
    // };

    // function initTracking(month,year) {
    //   vm.treatyTracking = {};
    //   vm.treatyTracking.reportingMonthYear = calculateQuarterDate(month,year);
    //   vm.treatyTracking.reportingRevised = 'No';
    //   vm.treatyTracking.flagEmail=0;
    //   vm.treatyTracking.flagFinance=0;
    //   vm.treatyTracking.flagEmailRec=0;
    //   vm.treatyTracking.flagArm=0;
    //   vm.treatyTracking.settlementDue='Assuming';
    //   vm.treatyTracking.trackingStatus='Open';
    //   vm.treatyTracking.password='N/A';
    //   vm.treatyTracking.treaty = vm.treaty;
    //   return vm.treatyTracking;
    // }
    //
    // vm.checkDuplicateTrackingRecords = function (quarters, year) {
    //   var isDuplicated = false;
    //   var calDate = calculateQuarterDate(quarters[0],year);
    //   angular.forEach(vm.treatyTrackings, function(record,key){
    //     var reportingDate = moment(record.reportingMonthYear).format('YYYY-MM-DD');
    //     if (reportingDate === calDate) {
    //       isDuplicated = true;
    //     }
    //   });
    //   return isDuplicated;
    // };



    vm.forDelete = function (tracking) {
      if(tracking.checked){
        TreatyTransactionSrv.subTransactionTracking.get({trackingId: tracking.id}, function (result) {
          if (result.length > 0) {
            var pendingTreaty = InteractionsService.alertParentScrollFalseDialog("You cannot delete this tracking record because it is linked to one or more transactions.", '#myModal');
            InteractionsService.showThenDialog(pendingTreaty, function () {
              tracking.checked = false;
            });
          } else {
            vm.deleteMultipleTracking.push(tracking);
          }
        });
      } else {
        angular.forEach(vm.deleteMultipleTracking, function (track, index) {
          if (track.id === tracking.id) {
            vm.deleteMultipleTracking.splice(index, 1);
          }
        });
      }
    };
    vm.deleteMultiple=function(){
      var confirm = InteractionsService.confirmBasicParentNoScrollDialog('You are attempting to delete ' + vm.deleteMultipleTracking.length  + ' tracking records. Click OK to proceed, or CANCEL to abort.', '#myModal');
      InteractionsService.showThenDialog(confirm, function() {
        angular.forEach(vm.deleteMultipleTracking, function (tracking) {
          angular.forEach(vm.treatyTrackings,function(track,index){
            if(track.id===tracking.id) {
              vm.treatyTrackings.splice(index,1);
            }
          });
          vm.deleteMultipleTracking
          tracking.isActive=0;
          TreatyTracking.update(tracking, function () {
            clear();
            loadAll();
          }, onSaveError);
        });
        vm.deleteMultipleTracking = [];
      });
    };
    vm.deleteFlagChange= function(){
      if(vm.deleteFlag===false){
        vm.deleteMultipleTracking=[];
        angular.forEach(vm.treatyTrackings,function(track,index){
          track.checked=false;
        });
      }
    };

    Principal.identity().then(function (account) {
      vm.lastUser = account.login;
    });

    vm.closeTracking= function (tracking){
      tracking.trackingStatus='Closed';
      TreatyTracking.update(tracking,  function (result) {
        vm.clear();
        loadAll();
        var genNextTrackingDialog = InteractionsService.confirmBasicParentNoScrollDialog("Do you want to create the next tracking record?", '#myModal');
        InteractionsService.showThenDialog(genNextTrackingDialog, function () {
          vm.nextTrackingGeneration = {};
          result.lastUpdate=moment().format('M/D/YYYY h:m A');
          TreatyTracking.save(TreatyTrackingCustomSvr.generateNextTracking.nextTrackingRecord({
            lastUser: vm.lastUser,
            oldTracking:result,
            nextTrackingGeneration: vm.nextTrackingGeneration,
            treatyTracking: tracking,
            treaty: vm.treaty
          }), function () {
            vm.loadAll();
          });
        });
      }, onSaveError);

    };

    vm.linkToEditPage = function(id){
      if(!vm.treaty.disabledTracking) {
        var info = {
          dealId: vm.dealId,
          treatyId: vm.treatyId,
          id: id
        };
        vm.go("treaty-tracking-all.edit", info);
      }
    };
  }
})();
