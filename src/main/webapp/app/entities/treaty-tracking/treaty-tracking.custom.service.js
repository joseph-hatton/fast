(function () {
  'use strict';
  angular
  .module('finreApp')
  .factory('TreatyTrackingCustomSvr', TreatyTrackingCustomSvr);

  TreatyTrackingCustomSvr.$inject = ['$resource', 'DateUtils'];

  function TreatyTrackingCustomSvr($resource, DateUtils) {
    return {
      'calculateReportingDue': function (tracking, frequency, entityDate, monthYear, mainfrequency, treaty, vm) {
        if (monthYear == null) {
          tracking.calculatedReportingDueDate = 'Reporting Period Not Specified';
        } else {
          var month = moment(monthYear).month() + 1;
          if (treaty.yearEndException === 'Yes' && (month === tracking.treaty.deal.yearEndMonth)) {
            if(vm){
              vm.uiFalgEndYearEx = true;
              vm.reportingQuarterFlag = false;
            }
            tracking.calculatedReportingDueDate = null;

            var date = new Date(moment(monthYear).year(), tracking.treaty.deal.yearEndMonth-1, 0);
            monthYear = date.toISOString().split('T')[0];

            tracking.calculatedReportingDueYex = calculatingDate(frequency, treaty.reportingDueYearEx, monthYear, mainfrequency);
          } else {
            if(vm)
              vm.uiFalgEndYearEx = false;
            tracking.calculatedReportingDueYex = null;
            tracking.calculatedReportingDueDate = calculatingDate(frequency, entityDate, monthYear, mainfrequency);
          }
        }
      },
      'generateNextTracking': {
        nextTrackingRecord: function (inObj) {
          inObj.nextTrackingGeneration.id = null;
          inObj.nextTrackingGeneration.treaty = inObj.oldTracking.treaty;
          inObj.nextTrackingGeneration.currency = inObj.oldTracking.currency;
          inObj.nextTrackingGeneration.cashDueFlag = inObj.oldTracking.cashDueFlag;
          inObj.nextTrackingGeneration.cashDueType = inObj.oldTracking.cashDueType;
          inObj.nextTrackingGeneration.trackingStatus = 'Open';
          inObj.nextTrackingGeneration.flagEmail = inObj.oldTracking.flagEmail;
          inObj.nextTrackingGeneration.flagEmailRec = inObj.oldTracking.flagEmailRec;
          inObj.nextTrackingGeneration.isActive = 1;
          inObj.nextTrackingGeneration.lastUser = inObj.lastUser;
          inObj.nextTrackingGeneration.autoGenerateFlag = 1;
          inObj.nextTrackingGeneration.reportNotifFlag=0;

          inObj.nextTrackingGeneration.reportingMonthYear = getNextReportingPeriodDate(inObj.oldTracking.reportingMonthYear, inObj.treatyTracking.treaty.reportingFrequency.name);
          if(inObj.seriatimDueDate != null && inObj.seriatimDueDate.frequency !== null){
            inObj.nextTrackingGeneration.calculatedSeriatimDueDate = calculatingDate(inObj.seriatimDueDate.period, inObj.seriatimDueDate, inObj.nextTrackingGeneration.reportingMonthYear, inObj.seriatimDueDate.frequency.name);
          }

          var monthQuarter = moment(inObj.nextTrackingGeneration.reportingMonthYear).month();
          var endQuarterFlag;
          if ([2, 5, 8, 11].indexOf(monthQuarter) > -1) {
            endQuarterFlag = true;
          }

          var month = moment(inObj.nextTrackingGeneration.reportingMonthYear).month() + 1;
          if ((month === 12) && inObj.treaty.yearEndException === 'Yes' &&  inObj.treaty.reportingDueYearEx) {
            inObj.nextTrackingGeneration.calculatedReportingDueDate = null;
            var date = new Date(moment(inObj.nextTrackingGeneration.reportingMonthYear).year()+1, inObj.nextTrackingGeneration.treaty.deal.yearEndMonth-1, inObj.nextTrackingGeneration.treaty.deal.yearEndDay);
            var monthYear = date.toISOString().split('T')[0];
            inObj.nextTrackingGeneration.calculatedReportingDueYex = calculatingDate(inObj.treatyTracking.treaty.reportingFrequency.name, inObj.treaty.reportingDueYearEx, monthYear, inObj.treatyTracking.treaty.reportingFrequency.name);
          }
          if (inObj.treaty.reportingDueDate && inObj.nextTrackingGeneration.reportingMonthYear !== null) {
              if(endQuarterFlag && inObj.treatyTracking.treaty.reportingFrequency !== null) {
                if (inObj.treaty.reportingDueQuarterEx && inObj.treaty.reportingDueQuarterEx.period !== undefined ) {
                  inObj.nextTrackingGeneration.calculatedReportingDueDate = calculatingDate(inObj.treatyTracking.treaty.reportingFrequency.name, inObj.treaty.reportingDueQuarterEx, inObj.nextTrackingGeneration.reportingMonthYear, inObj.treatyTracking.treaty.reportingFrequency.name);
                } else {
                  inObj.nextTrackingGeneration.calculatedReportingDueDate = calculatingDate(inObj.treatyTracking.treaty.reportingFrequency.name, inObj.treaty.reportingDueDate, inObj.nextTrackingGeneration.reportingMonthYear, inObj.treatyTracking.treaty.reportingFrequency.name);
                }
              } else {
            if (inObj.nextTrackingGeneration.reportingMonthYear !== null && inObj.treaty.reportingDueDate.period !== undefined && inObj.treatyTracking.treaty.reportingFrequency !== null) {
              inObj.nextTrackingGeneration.calculatedReportingDueDate = calculatingDate(inObj.treatyTracking.treaty.reportingFrequency.name, inObj.treaty.reportingDueDate, inObj.nextTrackingGeneration.reportingMonthYear, inObj.treatyTracking.treaty.reportingFrequency.name);
            }
          }
          }
          if(endQuarterFlag && inObj.treaty.stmntCedMonthFlag==='Yes'){

            if ((inObj.treaty.stmntDueCedQuarterEx)
              && (inObj.treaty.stmntDueCedQuarterEx.period !== null && inObj.treaty.stmntDueCedQuarterEx.dayType !== null)
              && (inObj.treaty.stmntDueCedQuarterEx.period !== 'Reporting Received' && inObj.treaty.stmntDueCedQuarterEx.period !== 'Reporting Confirmed')) {
              if (inObj.nextTrackingGeneration.treaty.stmntFrqncCeding !== undefined && inObj.nextTrackingGeneration.treaty.stmntFrqncCeding !== null) {
                inObj.nextTrackingGeneration.calculatedCedingGraceDate = calculatingDate(inObj.treaty.stmntDueCedQuarterEx.period, inObj.treaty.stmntDueCedQuarterEx, inObj.nextTrackingGeneration.reportingMonthYear, inObj.nextTrackingGeneration.treaty.stmntFrqncCeding.name);
              }
          }}
          else{
          if ((inObj.treaty.stmntDueCedGracePeriod)
          && (inObj.treaty.stmntDueCedGracePeriod.period !== null && inObj.treaty.stmntDueCedGracePeriod.dayType !== null)
          && (inObj.treaty.stmntDueCedGracePeriod.period !== 'Reporting Received' && inObj.treaty.stmntDueCedGracePeriod.period !== 'Reporting Confirmed')) {
            if (inObj.nextTrackingGeneration.treaty.stmntFrqncCeding !== undefined && inObj.nextTrackingGeneration.treaty.stmntFrqncCeding !== null) {
              inObj.nextTrackingGeneration.calculatedCedingGraceDate = calculatingDate(inObj.treaty.stmntDueCedGracePeriod.period, inObj.treaty.stmntDueCedGracePeriod, inObj.nextTrackingGeneration.reportingMonthYear, inObj.nextTrackingGeneration.treaty.stmntFrqncCeding.name);
            }
          }
          }



            if(endQuarterFlag && inObj.treaty.smntAsmMonthFlag==='Yes'){

              if (inObj.treaty.stmntDueAsmQuarterEx) {
                if (inObj.treaty.stmntDueAsmQuarterEx.period !== null && inObj.treaty.stmntDueAsmQuarterEx.dayType !== null) {
                  if (inObj.treaty.stmntDueAsmQuarterEx.period !== 'Reporting Received' && inObj.treaty.stmntDueAsmQuarterEx.period !== 'Reporting Confirmed' && inObj.nextTrackingGeneration.treaty.settlementFrequency !== null) {
                    inObj.nextTrackingGeneration.calculatedAssumingGraceDate = calculatingDate(inObj.treaty.stmntDueAsmQuarterEx.period, inObj.treaty.stmntDueAsmQuarterEx, inObj.nextTrackingGeneration.reportingMonthYear, inObj.nextTrackingGeneration.treaty.settlementFrequency.name);
                  }
                }


              }
            }
          else{
          if (inObj.treaty.stmntDueAsmGracePeriod) {
            if (inObj.treaty.stmntDueAsmGracePeriod.period !== null && inObj.treaty.stmntDueAsmGracePeriod.dayType !== null) {
              if (inObj.treaty.stmntDueAsmGracePeriod.period !== 'Reporting Received' && inObj.treaty.stmntDueAsmGracePeriod.period !== 'Reporting Confirmed' && inObj.nextTrackingGeneration.treaty.settlementFrequency !== null) {
                inObj.nextTrackingGeneration.calculatedAssumingGraceDate = calculatingDate(inObj.treaty.stmntDueAsmGracePeriod.period, inObj.treaty.stmntDueAsmGracePeriod, inObj.nextTrackingGeneration.reportingMonthYear, inObj.nextTrackingGeneration.treaty.settlementFrequency.name);
              }
            }


          }
            }
          return inObj.nextTrackingGeneration;
        }
      },
      tracking: $resource('api/treaty-trackings/treaty/:treatyId', {}, {
        'query': {method: 'GET', isArray: true},
        'get': {
          method: 'GET',
          transformResponse: function (data) {
            if (data === "" || data === null || data === undefined) return null;
            data = angular.fromJson(data);
            data.reportingDate = DateUtils.convertLocalDateFromServer(data.reportingDate);
            data.reportingMonthYear = DateUtils.convertLocalDateFromServer(data.reportingMonthYear);
            data.reportingReceivedDate = DateUtils.convertLocalDateFromServer(data.reportingReceivedDate);
            data.reportingUploadDate = DateUtils.convertLocalDateFromServer(data.reportingUploadDate);
            data.cashReceivedDate = DateUtils.convertLocalDateFromServer(data.cashReceivedDate);
            data.cashUploadDate = DateUtils.convertLocalDateFromServer(data.cashUploadDate);
            data.settlementPaidDate = DateUtils.convertLocalDateFromServer(data.settlementPaidDate);
            data.riskMgmtDate = DateUtils.convertLocalDateFromServer(data.riskMgmtDate);
            data.financeDate = DateUtils.convertLocalDateFromServer(data.financeDate);
            data.emailDate = DateUtils.convertLocalDateFromServer(data.emailDate);
            data.feePaymentDate = DateUtils.convertLocalDateFromServer(data.feePaymentDate);
            data.brokerFeeDueDate = DateUtils.convertLocalDateFromServer(data.brokerFeeDueDate);
            data.acctPeriodDate = DateUtils.convertLocalDateFromServer(data.acctPeriodDate);
            data.cashSentRecDate = DateUtils.convertLocalDateFromServer(data.cashSentRecDate);
            data.confirmationReceivedDate = DateUtils.convertLocalDateFromServer(data.confirmationReceivedDate);
            data.reportingSentDate = DateUtils.convertLocalDateFromServer(data.reportingSentDate);
            data.reportingPeriodDate = DateUtils.convertLocalDateFromServer(data.reportingPeriodDate);
            data.reportArmDate = DateUtils.convertLocalDateFromServer(data.reportArmDate);
            return data;
          }, isArray: true
        }
      }),
      openTracking: $resource('api/treaty-trackings/Open-Tracking/:treatyId', {}, {
        'query': {method: 'GET', isArray: true},
        'get': {
          method: 'GET',
          transformResponse: function (data) {
            if (data === "" || data === null || data === undefined) return null;
            data = angular.fromJson(data);
            data.reportingDate = DateUtils.convertLocalDateFromServer(data.reportingDate);
            data.reportingMonthYear = DateUtils.convertLocalDateFromServer(data.reportingMonthYear);
            data.reportingReceivedDate = DateUtils.convertLocalDateFromServer(data.reportingReceivedDate);
            data.reportingUploadDate = DateUtils.convertLocalDateFromServer(data.reportingUploadDate);
            data.cashReceivedDate = DateUtils.convertLocalDateFromServer(data.cashReceivedDate);
            data.cashUploadDate = DateUtils.convertLocalDateFromServer(data.cashUploadDate);
            data.settlementPaidDate = DateUtils.convertLocalDateFromServer(data.settlementPaidDate);
            data.riskMgmtDate = DateUtils.convertLocalDateFromServer(data.riskMgmtDate);
            data.financeDate = DateUtils.convertLocalDateFromServer(data.financeDate);
            data.emailDate = DateUtils.convertLocalDateFromServer(data.emailDate);
            data.feePaymentDate = DateUtils.convertLocalDateFromServer(data.feePaymentDate);
            data.brokerFeeDueDate = DateUtils.convertLocalDateFromServer(data.brokerFeeDueDate);
            data.acctPeriodDate = DateUtils.convertLocalDateFromServer(data.acctPeriodDate);
            data.cashSentRecDate = DateUtils.convertLocalDateFromServer(data.cashSentRecDate);
            data.confirmationReceivedDate = DateUtils.convertLocalDateFromServer(data.confirmationReceivedDate);
            data.reportingSentDate = DateUtils.convertLocalDateFromServer(data.reportingSentDate);
            data.reportingPeriodDate = DateUtils.convertLocalDateFromServer(data.reportingPeriodDate);
            data.reportArmDate = DateUtils.convertLocalDateFromServer(data.reportArmDate);
            return data;
          }, isArray: true
        }
      }),
      byDates: $resource('api/treaty-trackings/:treatyId/dates/:startYear/:startMonth/:startDay/:endYear/:endMonth/:endDay', {}, {
        'query': {method: 'GET', isArray: true},
        'get': {
          method: 'GET',
          transformResponse: function (data) {
            if (data === "" || data === null || data === undefined) return null;
            data = angular.fromJson(data);
            data.reportingDate = DateUtils.convertLocalDateFromServer(data.reportingDate);
            data.reportingMonthYear = DateUtils.convertLocalDateFromServer(data.reportingMonthYear);
            data.reportingReceivedDate = DateUtils.convertLocalDateFromServer(data.reportingReceivedDate);
            data.reportingUploadDate = DateUtils.convertLocalDateFromServer(data.reportingUploadDate);
            data.cashReceivedDate = DateUtils.convertLocalDateFromServer(data.cashReceivedDate);
            data.cashUploadDate = DateUtils.convertLocalDateFromServer(data.cashUploadDate);
            data.settlementPaidDate = DateUtils.convertLocalDateFromServer(data.settlementPaidDate);
            data.riskMgmtDate = DateUtils.convertLocalDateFromServer(data.riskMgmtDate);
            data.financeDate = DateUtils.convertLocalDateFromServer(data.financeDate);
            data.emailDate = DateUtils.convertLocalDateFromServer(data.emailDate);
            data.feePaymentDate = DateUtils.convertLocalDateFromServer(data.feePaymentDate);
            data.brokerFeeDueDate = DateUtils.convertLocalDateFromServer(data.brokerFeeDueDate);
            data.acctPeriodDate = DateUtils.convertLocalDateFromServer(data.acctPeriodDate);
            data.cashSentRecDate = DateUtils.convertLocalDateFromServer(data.cashSentRecDate);
            data.confirmationReceivedDate = DateUtils.convertLocalDateFromServer(data.confirmationReceivedDate);
            data.reportingSentDate = DateUtils.convertLocalDateFromServer(data.reportingSentDate);
            data.reportingPeriodDate = DateUtils.convertLocalDateFromServer(data.reportingPeriodDate);
            data.reportArmDate = DateUtils.convertLocalDateFromServer(data.reportArmDate);

            return data;
          }, isArray: true
        }
      }),
      expirationAssuming: $resource('api/treaty-trackings/ExpirationDatesAssuming/:endYear/:endMonth/:endDay', {}, {
        'query': {method: 'GET', isArray: true}, 'get': {
          method: 'GET',
          transformResponse: function (data) {
            if (data === "" || data === null || data === undefined) return null;
            data = angular.fromJson(data);
            data.reportingDate = DateUtils.convertLocalDateFromServer(data.reportingDate);
            data.reportingMonthYear = DateUtils.convertLocalDateFromServer(data.reportingMonthYear);
            data.reportingReceivedDate = DateUtils.convertLocalDateFromServer(data.reportingReceivedDate);
            data.reportingUploadDate = DateUtils.convertLocalDateFromServer(data.reportingUploadDate);
            data.cashReceivedDate = DateUtils.convertLocalDateFromServer(data.cashReceivedDate);
            data.cashUploadDate = DateUtils.convertLocalDateFromServer(data.cashUploadDate);
            data.settlementPaidDate = DateUtils.convertLocalDateFromServer(data.settlementPaidDate);
            data.riskMgmtDate = DateUtils.convertLocalDateFromServer(data.riskMgmtDate);
            data.financeDate = DateUtils.convertLocalDateFromServer(data.financeDate);
            data.emailDate = DateUtils.convertLocalDateFromServer(data.emailDate);
            data.feePaymentDate = DateUtils.convertLocalDateFromServer(data.feePaymentDate);
            data.brokerFeeDueDate = DateUtils.convertLocalDateFromServer(data.brokerFeeDueDate);
            data.acctPeriodDate = DateUtils.convertLocalDateFromServer(data.acctPeriodDate);
            data.cashSentRecDate = DateUtils.convertLocalDateFromServer(data.cashSentRecDate);
            data.confirmationReceivedDate = DateUtils.convertLocalDateFromServer(data.confirmationReceivedDate);
            data.reportingSentDate = DateUtils.convertLocalDateFromServer(data.reportingSentDate);
            data.reportingPeriodDate = DateUtils.convertLocalDateFromServer(data.reportingPeriodDate);
            data.reportArmDate = DateUtils.convertLocalDateFromServer(data.reportArmDate);

            return data;
          }, isArray: true
        }
      }),
      expirationCeding: $resource('api/treaty-trackings/ExpirationDatesCeding/:endYear/:endMonth/:endDay', {}, {
        'query': {method: 'GET', isArray: true}, 'get': {
          method: 'GET',
          transformResponse: function (data) {
            if (data === "" || data === null || data === undefined) return null;
            data = angular.fromJson(data);
            data.reportingDate = DateUtils.convertLocalDateFromServer(data.reportingDate);
            data.reportingMonthYear = DateUtils.convertLocalDateFromServer(data.reportingMonthYear);
            data.reportingReceivedDate = DateUtils.convertLocalDateFromServer(data.reportingReceivedDate);
            data.reportingUploadDate = DateUtils.convertLocalDateFromServer(data.reportingUploadDate);
            data.cashReceivedDate = DateUtils.convertLocalDateFromServer(data.cashReceivedDate);
            data.cashUploadDate = DateUtils.convertLocalDateFromServer(data.cashUploadDate);
            data.settlementPaidDate = DateUtils.convertLocalDateFromServer(data.settlementPaidDate);
            data.riskMgmtDate = DateUtils.convertLocalDateFromServer(data.riskMgmtDate);
            data.financeDate = DateUtils.convertLocalDateFromServer(data.financeDate);
            data.emailDate = DateUtils.convertLocalDateFromServer(data.emailDate);
            data.feePaymentDate = DateUtils.convertLocalDateFromServer(data.feePaymentDate);
            data.brokerFeeDueDate = DateUtils.convertLocalDateFromServer(data.brokerFeeDueDate);
            data.acctPeriodDate = DateUtils.convertLocalDateFromServer(data.acctPeriodDate);
            data.cashSentRecDate = DateUtils.convertLocalDateFromServer(data.cashSentRecDate);
            data.confirmationReceivedDate = DateUtils.convertLocalDateFromServer(data.confirmationReceivedDate);
            data.reportingSentDate = DateUtils.convertLocalDateFromServer(data.reportingSentDate);
            data.reportingPeriodDate = DateUtils.convertLocalDateFromServer(data.reportingPeriodDate);
            data.reportArmDate = DateUtils.convertLocalDateFromServer(data.reportArmDate);

            return data;
          }, isArray: true
        }
      }),
      lasReportingMonthYear: $resource('api/treaty-trackings/:treatyId/LastReportingMonthYear/:endYear/:endMonth/:endDay', {}, {
        'query': {method: 'GET', isArray: true},
        'get': {
          method: 'GET',
          transformResponse: function (data) {
            if (data === "" || data === null || data === undefined) return null;
            data = angular.fromJson(data);
            data.reportingDate = DateUtils.convertLocalDateFromServer(data.reportingDate);
            data.reportingMonthYear = DateUtils.convertLocalDateFromServer(data.reportingMonthYear);
            data.reportingReceivedDate = DateUtils.convertLocalDateFromServer(data.reportingReceivedDate);
            data.reportingUploadDate = DateUtils.convertLocalDateFromServer(data.reportingUploadDate);
            data.cashReceivedDate = DateUtils.convertLocalDateFromServer(data.cashReceivedDate);
            data.cashUploadDate = DateUtils.convertLocalDateFromServer(data.cashUploadDate);
            data.settlementPaidDate = DateUtils.convertLocalDateFromServer(data.settlementPaidDate);
            data.riskMgmtDate = DateUtils.convertLocalDateFromServer(data.riskMgmtDate);
            data.financeDate = DateUtils.convertLocalDateFromServer(data.financeDate);
            data.emailDate = DateUtils.convertLocalDateFromServer(data.emailDate);
            data.feePaymentDate = DateUtils.convertLocalDateFromServer(data.feePaymentDate);
            data.brokerFeeDueDate = DateUtils.convertLocalDateFromServer(data.brokerFeeDueDate);
            data.acctPeriodDate = DateUtils.convertLocalDateFromServer(data.acctPeriodDate);
            data.cashSentRecDate = DateUtils.convertLocalDateFromServer(data.cashSentRecDate);
            data.confirmationReceivedDate = DateUtils.convertLocalDateFromServer(data.confirmationReceivedDate);
            data.reportingSentDate = DateUtils.convertLocalDateFromServer(data.reportingSentDate);
            data.reportingPeriodDate = DateUtils.convertLocalDateFromServer(data.reportingPeriodDate);
            data.reportArmDate = DateUtils.convertLocalDateFromServer(data.reportArmDate);

            return data;
          }, isArray: true
        }
      }),
      TrackingByPeriod: $resource('api/treaty-trackings/:treatyId/TrackingPeriod/:startYear/:startMonth/:startDay', {}, {
        'query': {method: 'GET', isArray: true},
        'get': {
          method: 'GET',
          transformResponse: function (data) {
            if (data === "" || data === null || data === undefined) return null;
            data = angular.fromJson(data);
            data.reportingDate = DateUtils.convertLocalDateFromServer(data.reportingDate);
            data.reportingMonthYear = DateUtils.convertLocalDateFromServer(data.reportingMonthYear);
            data.reportingReceivedDate = DateUtils.convertLocalDateFromServer(data.reportingReceivedDate);
            data.reportingUploadDate = DateUtils.convertLocalDateFromServer(data.reportingUploadDate);
            data.cashReceivedDate = DateUtils.convertLocalDateFromServer(data.cashReceivedDate);
            data.cashUploadDate = DateUtils.convertLocalDateFromServer(data.cashUploadDate);
            data.settlementPaidDate = DateUtils.convertLocalDateFromServer(data.settlementPaidDate);
            data.riskMgmtDate = DateUtils.convertLocalDateFromServer(data.riskMgmtDate);
            data.financeDate = DateUtils.convertLocalDateFromServer(data.financeDate);
            data.emailDate = DateUtils.convertLocalDateFromServer(data.emailDate);
            data.feePaymentDate = DateUtils.convertLocalDateFromServer(data.feePaymentDate);
            data.brokerFeeDueDate = DateUtils.convertLocalDateFromServer(data.brokerFeeDueDate);
            data.acctPeriodDate = DateUtils.convertLocalDateFromServer(data.acctPeriodDate);
            data.cashSentRecDate = DateUtils.convertLocalDateFromServer(data.cashSentRecDate);
            data.confirmationReceivedDate = DateUtils.convertLocalDateFromServer(data.confirmationReceivedDate);
            data.reportingSentDate = DateUtils.convertLocalDateFromServer(data.reportingSentDate);
            data.reportingPeriodDate = DateUtils.convertLocalDateFromServer(data.reportingPeriodDate);
            data.reportArmDate = DateUtils.convertLocalDateFromServer(data.reportArmDate);
            return data;
          }, isArray: true
        }
      }),
      seritatimDueDate: $resource('api/treaty-trackings/SeriatimDueDate', {}, {
        'query': {method: 'GET', isArray: true},
        'get': {
          method: 'GET',
          transformResponse: function (data) {
            if (data === "" || data === null || data === undefined) return null;
            data = angular.fromJson(data);
            data.reportingDate = DateUtils.convertLocalDateFromServer(data.reportingDate);
            data.reportingMonthYear = DateUtils.convertLocalDateFromServer(data.reportingMonthYear);
            data.reportingReceivedDate = DateUtils.convertLocalDateFromServer(data.reportingReceivedDate);
            data.reportingUploadDate = DateUtils.convertLocalDateFromServer(data.reportingUploadDate);
            data.cashReceivedDate = DateUtils.convertLocalDateFromServer(data.cashReceivedDate);
            data.cashUploadDate = DateUtils.convertLocalDateFromServer(data.cashUploadDate);
            data.settlementPaidDate = DateUtils.convertLocalDateFromServer(data.settlementPaidDate);
            data.riskMgmtDate = DateUtils.convertLocalDateFromServer(data.riskMgmtDate);
            data.financeDate = DateUtils.convertLocalDateFromServer(data.financeDate);
            data.emailDate = DateUtils.convertLocalDateFromServer(data.emailDate);
            data.feePaymentDate = DateUtils.convertLocalDateFromServer(data.feePaymentDate);
            data.brokerFeeDueDate = DateUtils.convertLocalDateFromServer(data.brokerFeeDueDate);
            data.acctPeriodDate = DateUtils.convertLocalDateFromServer(data.acctPeriodDate);
            data.cashSentRecDate = DateUtils.convertLocalDateFromServer(data.cashSentRecDate);
            data.confirmationReceivedDate = DateUtils.convertLocalDateFromServer(data.confirmationReceivedDate);
            data.reportingSentDate = DateUtils.convertLocalDateFromServer(data.reportingSentDate);
            data.reportingPeriodDate = DateUtils.convertLocalDateFromServer(data.reportingPeriodDate);
            data.reportArmDate = DateUtils.convertLocalDateFromServer(data.reportArmDate);
            return data;
          }, isArray: true
        }
      }),
      reportingDueDateExpired: $resource('api/treaty-trackings/ReportingExpired', {}, {
        'query': {method: 'GET', isArray: true}
      }),
      reportingDueDatePastGracePeriod: $resource('api/treaty-trackings/ReportingPastGracePeriod', {}, {
        'query': {method: 'GET', isArray: true}
      }),
      settlementAssumingExpired: $resource('api/treaty-trackings/SettlementAsummingExpired', {}, {
        'query': {method: 'GET', isArray: true}
      }),
      settlementCedingExpired: $resource('api/treaty-trackings/SettlementCedingExpired', {}, {
        'query': {method: 'GET', isArray: true}
      }),
      seriatimDueDateExpired: $resource('api/treaty-trackings/SeriatimDueDateExpired', {}, {
        'query': {method: 'GET', isArray: true}
      }),
      clearedReportingNotifications: $resource('api/cleared-treaty-tracking-reporting', {}, {
        'query': {
          method: 'GET',
          isArray: true
        }
      }),
      expirationReportingDue: $resource('api/treaty-trackings/ExpirationDatesReporingDue/:endYear/:endMonth/:endDay', {}, {
        'query': {method: 'GET', isArray: true}, 'get': {
          method: 'GET',
          transformResponse: function (data) {
            if (data === "" || data === null || data === undefined) return null;
            data = angular.fromJson(data);
            data.reportingDate = DateUtils.convertLocalDateFromServer(data.reportingDate);
            data.reportingMonthYear = DateUtils.convertLocalDateFromServer(data.reportingMonthYear);
            data.reportingReceivedDate = DateUtils.convertLocalDateFromServer(data.reportingReceivedDate);
            data.reportingUploadDate = DateUtils.convertLocalDateFromServer(data.reportingUploadDate);
            data.cashReceivedDate = DateUtils.convertLocalDateFromServer(data.cashReceivedDate);
            data.cashUploadDate = DateUtils.convertLocalDateFromServer(data.cashUploadDate);
            data.settlementPaidDate = DateUtils.convertLocalDateFromServer(data.settlementPaidDate);
            data.riskMgmtDate = DateUtils.convertLocalDateFromServer(data.riskMgmtDate);
            data.financeDate = DateUtils.convertLocalDateFromServer(data.financeDate);
            data.emailDate = DateUtils.convertLocalDateFromServer(data.emailDate);
            data.feePaymentDate = DateUtils.convertLocalDateFromServer(data.feePaymentDate);
            data.brokerFeeDueDate = DateUtils.convertLocalDateFromServer(data.brokerFeeDueDate);
            data.acctPeriodDate = DateUtils.convertLocalDateFromServer(data.acctPeriodDate);
            data.cashSentRecDate = DateUtils.convertLocalDateFromServer(data.cashSentRecDate);
            data.confirmationReceivedDate = DateUtils.convertLocalDateFromServer(data.confirmationReceivedDate);
            data.reportingSentDate = DateUtils.convertLocalDateFromServer(data.reportingSentDate);
            data.reportingPeriodDate = DateUtils.convertLocalDateFromServer(data.reportingPeriodDate);
            data.reportArmDate = DateUtils.convertLocalDateFromServer(data.reportArmDate);

            return data;
          }, isArray: true
        }
      }),

      assumingAlmostPastDue: $resource('api/treaty-trackings/AssumingDate/AlmostPastDueDate', {}, {
        'query': {method: 'GET', isArray: true}
      }),
      assumingAlmostExpired: $resource('api/treaty-trackings/AssumingDate/AlmostExpired', {}, {
        'query': {method: 'GET', isArray: true}
      }),
      assumingExpired: $resource('api/treaty-trackings/AssumingDate/Expired', {}, {
        'query': {method: 'GET', isArray: true}
      }),

      cedingAlmostPastDue: $resource('api/treaty-trackings/CedingDate/AlmostPastDueDate', {}, {
        'query': {method: 'GET', isArray: true}
      }),
      cedingAlmostExpired: $resource('api/treaty-trackings/CedingDate/AlmostExpired', {}, {
        'query': {method: 'GET', isArray: true}
      }),
      cedingExpired: $resource('api/treaty-trackings/CedingDate/Expired', {}, {
        'query': {method: 'GET', isArray: true}
      }),
      reportingDueExpired: $resource('/api/treaty-trackings/Reporting/noCashDue/Expired', {}, {
        'query': {method: 'GET', isArray: true}
      }),

      count: $resource('api/treaty-trackings/count/:treatyId', {}, {
        'get': {method: 'GET', transformResponse: function (data) {
          data = angular.fromJson(data);
          return data;
        }}
      })
    };
  }
})();
