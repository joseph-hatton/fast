(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('TreatyTrackingDeleteController', TreatyTrackingDeleteController);

  TreatyTrackingDeleteController.$inject = ['$uibModalInstance', '$stateParams', 'entity', 'TreatyTracking','TreatyTransactionSrv'];

  function TreatyTrackingDeleteController($uibModalInstance, $stateParams, entity, TreatyTracking,TreatyTransactionSrv) {
    var vm = this;
    vm.treatyTracking = entity;
    vm.treatyId = $stateParams.treatyId;
    vm.flagTransactions=0;
    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };
    var onSaveError= function (){
      console.log('error updating to delete ')
    };

    TreatyTransactionSrv.subTransactionTracking.get({trackingId:$stateParams.id}).$promise.then(function (result) {
      if(result.length>=1)
      {
        vm.flagTransactions=1;

      }

    vm.confirmDelete = function (id) {

        if(vm.flagTransactions!==1){
          vm.treatyTracking.isActive=0;
          TreatyTracking.update(vm.treatyTracking,  function () {
            $uibModalInstance.close(true);
          }, onSaveError);

        }


    };

    });
  }
})();
