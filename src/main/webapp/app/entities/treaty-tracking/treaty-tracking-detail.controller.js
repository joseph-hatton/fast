var reportingMonthYear;
(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('TreatyTrackingDetailController', TreatyTrackingDetailController);

  TreatyTrackingDetailController.$inject = ['$scope', '$filter', '$rootScope', '$stateParams', 'entity', 'TreatySrv',
    'TreatyTracking', 'Treaty', 'TreatyTransactionSrv', 'DealTrackingFilterSrv', '$state'];
  function TreatyTrackingDetailController($scope, $filter, $rootScope, $stateParams, entity, TreatySrv,
                                          TreatyTracking, Treaty, TreatyTransactionSrv, DealTrackingFilterSrv, $state) {
    var vm = this;
    vm.treatyTracking = entity;
    vm.treatyId = $stateParams.treatyId;
    vm.dealId = $stateParams.dealId;
    vm.seriatimDueDate = {frequency: null, numberOfDays: null, dayType: null, period: null};

    vm.reportingPeriodEndDealTracking = function (ReportingDate) {
      DealTrackingFilterSrv.reportingReportTracking.get({
        dealId: $stateParams.dealId,
        startYear: moment(ReportingDate).get('year'),
        startMonth: moment(ReportingDate).get('month') + 1,
        startDay: moment(ReportingDate).get('date')
      }, function (result) {
        if (result.length > 0) {
          vm.financeDate = result[0].financeDate;
          vm.reportingValidationDate = result[0].reportingValidationDate;
          vm.reportArmDate = result[0].reportingArmDate;
          vm.reportingRevisedFlag = result[0].reportingRevisedFlag;
          vm.password = result[0].password;
        } else {
          vm.financeDate = 'No Deal Tracking For this period';
          vm.reportingValidationDate = 'No Deal Tracking For this period';
          vm.reportArmDate = 'No Deal Tracking For this period';
          vm.reportingRevisedFlag = 'No Deal Tracking For this period';
          vm.password = 'No Deal Tracking For this period';
        }
      });
    };

    function dateCheck(date){
      return date != null && date.period != null && date.numberOfDays != null && date.dayType != null;
    }

    vm.load = function () {
      if (vm.treatyTracking.reportingMonthYear) {
        if (vm.treatyTracking.treaty.seriatimTypeTreaties.length > 0) {
          angular.forEach(vm.treatyTracking.treaty.seriatimTypeTreaties, function (seriatims, key) {
            if (seriatims.seriatimDueDateFlag === 'Yes' && seriatims.calculationFlag) {
              vm.seriatimDueDate.frequency = seriatims.seriatimFreq;
              vm.seriatimDueDate.numberOfDays = seriatims.seriatimDueDate.numberOfDays;
              vm.seriatimDueDate.dayType = seriatims.seriatimDueDate.dayType;
              vm.seriatimDueDate.period = seriatims.seriatimDueDate.period;
            }
          });
        }
        if (moment(vm.treatyTracking.reportingMonthYear).month() === 2 || moment(vm.treatyTracking.reportingMonthYear).month() === 5 || moment(vm.treatyTracking.reportingMonthYear).month() === 8 || moment(vm.treatyTracking.reportingMonthYear).month() === 11) {
          vm.endQuarterFlag = true;

          vm.reportingQuarterFlag = vm.endQuarterFlag && dateCheck(vm.treatyTracking.treaty.reportingDueQuarterEx);
          vm.assumingFlagQuarter = vm.endQuarterFlag && dateCheck(vm.treatyTracking.treaty.stmntDueAsmQuarterEx);
          vm.cedingFlagQuarter = vm.endQuarterFlag && dateCheck(vm.treatyTracking.treaty.stmntDueCedQuarterEx);

          if (moment(vm.treatyTracking.reportingMonthYear).month() === 11) {
            if (vm.treatyTracking.treaty.yearEndException === 'Yes') {
              vm.uiFalgEndYearEx = true;
              vm.reportingQuarterFlag = false;
              vm.treatyTracking.calculatedReportingDueDate = null;
            }
          }
        } else {
          vm.endQuarterFlag = false;
          vm.assumingFlagQuarter = false;
          vm.cedingFlagQuarter = false;
          vm.reportingQuarterFlag = false;
        }
      }



      if (vm.treatyTracking.reportingMonthYear)
        vm.reportingPeriodEndDealTracking(vm.treatyTracking.reportingMonthYear);
        if(vm.treatyTracking.cashDueFlag === 'No')
        {
          vm.treatyTracking.calculatedAssumingGraceDate='N/A';
          vm.treatyTracking.calculatedCedingGraceDate='N/A';
        }
      vm.treatyTracking.reportingMonthYear = $filter('date')(vm.treatyTracking.reportingMonthYear, 'yyyy-MM');
      vm.treatyTracking.reportingReceivedDate = $filter('date')(vm.treatyTracking.reportingReceivedDate, 'yyyy-MM-dd');
      vm.treatyTracking.cashSentRecDate = $filter('date')(vm.treatyTracking.cashSentRecDate, 'yyyy-MM-dd');
      vm.treatyTracking.reportingValidationDate = $filter('date')(vm.treatyTracking.reportingValidationDate, 'yyyy-MM-dd');
      vm.treatyTracking.cashUploadDate = $filter('date')(vm.treatyTracking.cashUploadDate, 'yyyy-MM-dd');
      vm.treatyTracking.riskMgmtDate = $filter('date')(vm.treatyTracking.riskMgmtDate, 'yyyy-MM-dd');
      vm.treatyTracking.confirmationReceivedDate = $filter('date')(vm.treatyTracking.confirmationReceivedDate, 'yyyy-MM-dd');
      vm.treatyTracking.emailDate = $filter('date')(vm.treatyTracking.emailDate, 'yyyy-MM-dd');
      vm.treatyTracking.financeDate = $filter('date')(vm.treatyTracking.financeDate, 'yyyy-MM-dd');
      vm.treatyTracking.reportArmDate = $filter('date')(vm.treatyTracking.reportArmDate, 'yyyy-MM-dd');
      vm.treatyTracking.brokerFeeDueDate = $filter('date')(vm.treatyTracking.brokerFeeDueDate, 'yyyy-MM-dd');
      vm.treatyTracking.seriatimReceivedDate = $filter('date')(vm.treatyTracking.seriatimReceivedDate, 'yyyy-MM-dd');
      reportingMonthYear = vm.treatyTracking.reportingMonthYear;
      getCashTransactions();

      Treaty.get({id: $stateParams.treatyId}, function (result) {
        vm.treaty = result;
      });
    };

    vm.load();

    function getCashTransactions() {
      TreatyTransactionSrv.transactions.get({treatyId: $stateParams.treatyId}, function (result) {
        vm.cashSentTransactions = [];
        angular.forEach(result, function (transaction, key) {
          angular.forEach(transaction.subTransactions, function (subtransaction, key) {
            if (convertYMDate(subtransaction.reportingDate) === convertYMDate(vm.treatyTracking.reportingMonthYear)) {
              vm.cashSentTransactions.push(transaction);
            }
          });
        });
      });
    }

    // function calculateReportingDue(frequency, entityDate, reportingMonthYear) {
    //   vm.treatyTracking.calculatedReportingDueDate = $filter('date')(vm.treatyTracking.calculatedReportingDueDate, 'yyyy-MM-dd');
    //   vm.treatyTracking.reportingMonthYear = $filter('date')(vm.treatyTracking.reportingMonthYear, 'yyyy-MM');
    //   vm.treatyTracking.reportingReceivedDate = $filter('date')(vm.treatyTracking.reportingReceivedDate, 'yyyy-MM-dd');
    //   vm.treatyTracking.cashSentRecDate = $filter('date')(vm.treatyTracking.cashSentRecDate, 'yyyy-MM-dd');
    //   vm.treatyTracking.reportingValidationDate = $filter('date')(vm.treatyTracking.reportingValidationDate, 'yyyy-MM-dd');
    //   vm.treatyTracking.cashUploadDate = $filter('date')(vm.treatyTracking.cashUploadDate, 'yyyy-MM-dd');
    //   vm.treatyTracking.riskMgmtDate = $filter('date')(vm.treatyTracking.riskMgmtDate, 'yyyy-MM-dd');
    //   vm.treatyTracking.confirmationReceivedDate = $filter('date')(vm.treatyTracking.confirmationReceivedDate, 'yyyy-MM-dd');
    //   vm.treatyTracking.emailDate = $filter('date')(vm.treatyTracking.emailDate, 'yyyy-MM-dd');
    //   vm.treatyTracking.financeDate = $filter('date')(vm.treatyTracking.financeDate, 'yyyy-MM-dd');
    //   vm.treatyTracking.reportArmDate = $filter('date')(vm.treatyTracking.reportArmDate, 'yyyy-MM-dd');
    //   vm.treatyTracking.brokerFeeDueDate = $filter('date')(vm.treatyTracking.brokerFeeDueDate, 'yyyy-MM-dd');
    // }

    var unsubscribe = $rootScope.$on('finreApp:treatyTrackingUpdate', function (event, result) {
      vm.treatyTracking = result;
    });

    $scope.$on('$destroy', unsubscribe);

    vm.linkToEditPage = function(){
      if(!vm.treaty.disabledTracking) {
        var info = {
          dealId: vm.dealId,
          treatyId: vm.treatyId,
          id: vm.treatyTracking.id
        };
        $state.go("treaty-tracking-detail.edit-detail", info);
      }
    };
  }
})();
