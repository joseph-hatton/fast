(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('ContactType', ContactType);
  
  ContactType.$inject = ['$resource'];
  
  function ContactType($resource) {
    var resourceUrl = 'api/contact-types/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          return data;
        }
      },
      'update': {method: 'PUT'}
    });
  }
})();
