(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('DealTypeController', DealTypeController);

    DealTypeController.$inject = ['DealType', 'DealTypeSearch'];

    function DealTypeController(DealType, DealTypeSearch) {

        var vm = this;

        vm.dealTypes = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            DealType.query(function(result) {
                vm.dealTypes = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            DealTypeSearch.query({query: vm.searchQuery}, function(result) {
                vm.dealTypes = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();
