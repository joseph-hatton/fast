(function() {
    'use strict';

    angular
        .module('finreApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('deal-type', {
            parent: 'entity',
            url: '/deal-type',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
                pageTitle: 'finreApp.dealType.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/deal-type/deal-types.html',
                    controller: 'DealTypeController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dealType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('deal-type.new', {
            parent: 'deal-type',
            url: '/new',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/deal-type/deal-type-dialog.html',
                    controller: 'DealTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('deal-type', null, { reload: 'deal-type' });
                }, function() {
                    $state.go('deal-type');
                });
            }]
        })
        .state('deal-type.edit', {
            parent: 'deal-type',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/deal-type/deal-type-dialog.html',
                    controller: 'DealTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DealType', function(DealType) {
                            return DealType.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('deal-type', null, { reload: 'deal-type' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('deal-type.delete', {
            parent: 'deal-type',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/deal-type/deal-type-delete-dialog.html',
                    controller: 'DealTypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DealType', function(DealType) {
                            return DealType.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('deal-type', null, { reload: 'deal-type' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
