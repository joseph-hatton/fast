(function() {
    'use strict';
    angular
        .module('finreApp')
        .factory('DealType', DealType);

  DealType.$inject = ['$resource'];

    function DealType ($resource) {
        var resourceUrl =  'api/deal-types/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
