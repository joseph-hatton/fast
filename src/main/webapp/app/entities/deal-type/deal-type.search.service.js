(function() {
    'use strict';

    angular
        .module('finreApp')
        .factory('DealTypeSearch', DealTypeSearch);

    DealTypeSearch.$inject = ['$resource'];

    function DealTypeSearch($resource) {
        var resourceUrl =  'api/_search/deal-types/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
