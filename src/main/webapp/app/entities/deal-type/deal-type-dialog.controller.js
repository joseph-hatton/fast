(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('DealTypeDialogController', DealTypeDialogController);

    DealTypeDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DealType'];

    function DealTypeDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DealType) {
        var vm = this;

        vm.dealType = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.dealType.id !== null) {
                DealType.update(vm.dealType, onSaveSuccess, onSaveError);
            } else {
                DealType.save(vm.dealType, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('finreApp:dealTypeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
