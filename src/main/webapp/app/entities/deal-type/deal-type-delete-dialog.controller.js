(function() {
    'use strict';

    angular
        .module('finreApp')
        .controller('DealTypeDeleteController',DealTypeDeleteController);

    DealTypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'DealType'];

    function DealTypeDeleteController($uibModalInstance, entity, DealType) {
        var vm = this;

        vm.dealType = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DealType.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
