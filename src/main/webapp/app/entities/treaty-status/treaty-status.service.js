(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('TreatyStatus', TreatyStatus);
  
  TreatyStatus.$inject = ['$resource'];
  
  function TreatyStatus($resource) {
    var resourceUrl = 'api/treaty-statuses/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          return data;
        }
      },
      'update': {method: 'PUT'}
    });
  }
})();
