(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('XrefTreatyCustomCompany', XrefTreatyCustomCompany);

  XrefTreatyCustomCompany.$inject = ['$resource', 'DateUtils'];
  var treatiesUrl = 'api/xref-treaty-companies/treaties/:id';
  var dealsUrl = 'api/xref-treaty-companies/deals/:id';
  var companiesUrl = 'api/xref-treaty-companies/companies/:id';
  var companyCountUrl = 'api/xref-treaty-companies/company-count/:id';

  function XrefTreatyCustomCompany($resource, DateUtils) {
    return {
      treaties: $resource(treatiesUrl, {}, {
        query: { method: 'GET', params: {}, isArray: true },
        'get': {
          method: 'GET',
          transformResponse: function (data) {
            data = angular.fromJson(data);
            return data;
          }
        }
      }),
      deals: $resource(dealsUrl, {}, {
        query: { method: 'GET', params: {}, isArray: true },
        'get': {
          method: 'GET',
          transformResponse: function (data) {
            data = angular.fromJson(data);
            return data;
          }
        }
      }),
      companies: $resource(companiesUrl, {}, {
        query: { method: 'GET', params: {}, isArray: true },
        'get': {
          method: 'GET',
          transformResponse: function (data) {
            data = angular.fromJson(data);
            return data;
          }
        }
      }),
      companyCount: $resource(companyCountUrl, {}, {
        query: { method: 'GET', params: {}, isArray: true },
        'get': {
          method: 'GET',
          transformResponse: function (data) {
            data = angular.fromJson(data);
            return data;
          }
        }
      })
    };
  }
})();
