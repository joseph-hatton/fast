(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('XrefTreatyCompany', XrefTreatyCompany);

  XrefTreatyCompany.$inject = ['$resource'];

  function XrefTreatyCompany($resource) {
    var resourceUrl = 'api/xref-treaty-companies/:id';

    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          return data;
        }
      },
      'save': {
        method: 'POST',
        transformRequest: function (data) {
          return angular.toJson(data);
        }
      },
      'update': {method: 'PUT'}
    });
  }
})();
