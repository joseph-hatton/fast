(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('ContactController', ContactController);

  ContactController.$inject = ['$state', 'Contact', 'ContactSearch', 'ParseLinks', 'AlertService',
    'pagingParams', 'paginationConstants','ContactCustomSrv', 'Intro'];
    function ContactController($state, Contact, ContactSearch, ParseLinks, AlertService,
                             pagingParams, paginationConstants,ContactCustomSrv, Intro) {
    var vm = this;
    vm.loadAll = loadAll;
    vm.loadPage = loadPage;
    vm.predicate = pagingParams.predicate;
    vm.reverse = pagingParams.ascending;
    vm.transition = transition;
    vm.clear = clear;
    vm.search = search;
    vm.searchQuery = pagingParams.search;
    vm.currentSearch = pagingParams.search;

    Intro.setupIntroSteps({id:124})
    .then(function(result){
        vm.introOptions = {
            steps: result
        };
    });

    vm.loadAll();
      vm.itemsPerPage = paginationConstants.itemsPerPage;
    function loadAll() {
      if (pagingParams.search) {
        ContactCustomSrv.fullSearch.query({
          search: pagingParams.search,
          page: pagingParams.page - 1,
          sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
          size: paginationConstants.itemsPerPage
        }, onSuccess, onError);
      } else {
        Contact.query({
          page: pagingParams.page - 1,
          size: paginationConstants.itemsPerPage,
          sort: sort()
        }, onSuccess, onError);
      }
      function sort() {
        var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
        if (vm.predicate !== 'id') {
          result.push('id');
        }
        return result;
      }

      function onSuccess(data, headers) {

        console.log(headers);
        vm.links = ParseLinks.parse(headers('link'));
        vm.totalItems = headers('X-Total-Count');
        vm.queryCount = vm.totalItems;
        vm.contacts = data;
        vm.page = pagingParams.page;

      }

      function onError(error) {
        AlertService.error(error.data.message);
      }
    }

    function loadPage(page) {
      vm.page = page;
      vm.transition();
    }

    function transition() {
      $state.transitionTo($state.$current, {
        page: vm.page,
        sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
        search: vm.currentSearch
      });
    }

    function search(searchQuery) {
      if (!searchQuery) {
        return vm.clear();
      }
      vm.links = null;
      vm.page = 1;
    //  vm.predicate = '_score';
      vm.reverse = false;
      vm.currentSearch = searchQuery;
      vm.transition();
    }

    function clear() {
      vm.links = null;
      vm.page = 1;
      vm.predicate = 'id';
      vm.reverse = true;
      vm.currentSearch = null;
      vm.transition();
    }

  }
})();
