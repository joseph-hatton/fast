(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('ContactDeleteController', ContactDeleteController);
  ContactDeleteController.$inject = ['$uibModalInstance','$stateParams', 'entity', 'Contact',
  'XrefTreatyCustomContact', 'XrefTreatyContact'];
  function ContactDeleteController($uibModalInstance,$stateParams, entity, Contact,
                                   XrefTreatyCustomContact, XrefTreatyContact) {
    var vm = this;
    vm.contact = entity;
    vm.treatyList=[];
    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };


    XrefTreatyCustomContact.contacts.query({id: $stateParams.id}, function (result) {
      vm.xrefTreatyContacts = result;
      angular.forEach(vm.xrefTreatyContacts, function (xrefContact, key) {
        vm.treatyList.push(xrefContact.treaty)
      });
    });
    vm.confirmDelete = function (id) {
        angular.forEach(vm.xrefTreatyContacts, function (xrefContact, key) {
          XrefTreatyContact.delete({id: xrefContact.id},
            function () {
            });
        });
        Contact.delete({id: id},
          function () {
            $uibModalInstance.close(true);
          });

    };
  }
})();
