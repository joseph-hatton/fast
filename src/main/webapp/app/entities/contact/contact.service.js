(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('Contact', Contact);

  Contact.$inject = ['$resource', 'DateUtils'];

  function Contact($resource, DateUtils) {
    var resourceUrl = 'api/contacts/:id';

    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          if(data === "" || data === null) return null;
          data = angular.fromJson(data);
          data.updatedDate = DateUtils.convertLocalDateFromServer(data.updatedDate);
          return data;
        }
      },
      'update': {
        method: 'PUT',
        transformRequest: function (data) {
          data.updatedDate = DateUtils.convertLocalDateToServer(data.updatedDate);
          return angular.toJson(data);
        }
      },
      'save': {
        method: 'POST',
        transformRequest: function (data) {
          data.updatedDate = DateUtils.convertLocalDateToServer(data.updatedDate);
          return angular.toJson(data);
        }
      }
    });
  }
})();
