(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('ContactDetailController', ContactDetailController);

  ContactDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Contact'];
  function ContactDetailController($scope, $rootScope, $stateParams, entity, Contact) {
    var vm = this;
    vm.contact = entity;
    vm.treatyId=$stateParams.id;
    vm.load = function (id) {
      Contact.get({id: id}, function (result) {
        vm.contact = result;
      });
    };
    var unsubscribe = $rootScope.$on('finreApp:contactUpdate', function (event, result) {
      vm.contact = result;
    });
    $scope.$on('$destroy', unsubscribe);
  }
})();
