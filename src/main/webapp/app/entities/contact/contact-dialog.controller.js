(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('ContactDialogController', ContactDialogController);

  ContactDialogController.$inject = ['$scope','$timeout','$state','$mdDialog', '$stateParams', '$uibModalInstance', 'entity', 'Contact',
    'State', 'Principal', 'Country'];

  function ContactDialogController($scope,$timeout,$state,$mdDialog ,$stateParams, $uibModalInstance, entity, Contact,
                                   State, Principal,Country) {
    var vm = this;
    vm.stateLbl = "State";
    vm.zipLbl = "Zip Code";
    vm.toggleState = true;
    vm.contact = entity;
    vm.gfsCountries = [];
    vm.treatyId = $stateParams.id;
    vm.dealId = $stateParams.dealId;
    vm.contactId = $stateParams.contactId;
    vm.contact.international = 0;

    State.query(function(result) {
      vm.states = result;
    });

    Country.query(function(result) {
      vm.gfsCountries = result;
    });

    vm.load = function (id) { };

    Principal.identity().then(function (account) {
      vm.lastUser=account.login;
    });

    vm.selectedCountry = function () {
      if(vm.contact.country.name === 'United States') {
        vm.stateLbl = "State";
        vm.zipLbl = "Zip Code";
        vm.toggleState = true;
        vm.contact.international=1;
      } else {
        vm.stateLbl = "Province";
        vm.zipLbl = "Postal Code";
        vm.toggleState = false;
        vm.contact.international=0;
      }
    };
    if(vm.contactId!=undefined && vm.contactId!='') {
      Contact.get({id: $stateParams.id}, function (result) {
        vm.contact = result;
        angular.forEach(vm.salutationOptions, function (salutation, key) {
          if (vm.contact.salutation === salutation.name) {
            vm.contact.salutation = salutation;
          }
        });
        vm.selectedCountry();
      });
    }
    vm.salutationOptions=[
      {name:"Mr."},
      {name:"Mrs."},
      {name:"Ms."},
      {name:"Dr."}
    ];


    var onSaveSuccess = function (result) {
      if($stateParams.contactId===""){
        $state.go('contacts-company-detailContact',{dealId:vm.dealId,id:vm.treatyId ,contactId:result.id});
        $timeout(function() {
          $uibModalInstance.close(result);
        }, 30);
      }
      else{
        $scope.$emit('finreApp:contactUpdate', result);
        $uibModalInstance.close(result);
        vm.isSaving = false;
      }

    };

    var onSaveError = function () {
      vm.isSaving = false;
    };

    vm.save = function () {
      vm.isSaving = true;

      if(vm.contact.country.name === 'United States') {
        vm.contact.province = null;
      } else {
        vm.contact.state = null;
      }

      if(vm.contact.salutation!=null) {
        vm.contact.salutation = vm.contact.salutation.name;
      }
      vm.contact.lastUser=vm.lastUser;
      vm.contact.lastUpdate= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';

      if (vm.contact.id !== null) {
        Contact.update(vm.contact, onSaveSuccess, onSaveError);
      } else {
        vm.contact.creationUser=vm.lastUser;
        vm.contact.creationTs= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        Contact.save(vm.contact, onSaveSuccess, onSaveError);
      }
    };

    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };

    vm.clearx = function () {
        var confirm = $mdDialog.confirm()
          .title('Are you sure you want to exit without saving?')
          .ok('Yes')
          .cancel('No')
          .parent(angular.element(document.querySelector('#myModal')));

        $mdDialog.show(confirm).then(function() {
          $uibModalInstance.dismiss('cancel');
        }, function() {

        });
    };


    vm.datePickerOpenStatus = {};
    vm.datePickerOpenStatus.updatedDate = false;

    vm.openCalendar = function (date) {
      vm.datePickerOpenStatus[date] = true;
    };
  }
})();
