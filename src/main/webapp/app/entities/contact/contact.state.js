(function () {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('contact', {
        parent: 'entity',
        url: '/contact?page&sort&search',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.contact.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/contact/contacts.html',
            controller: 'ContactController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,asc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
            return {
              page: PaginationUtil.parsePage($stateParams.page),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate($stateParams.sort),
              ascending: PaginationUtil.parseAscending($stateParams.sort),
              search: $stateParams.search
            };
          }],
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('contact');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }]
        }
      })
      .state('contact-detail', {
        parent: 'entity',
        url: '/contact/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.contact.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/contact/contact-detail.html',
            controller: 'ContactDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('contact');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'Contact', function ($stateParams, Contact) {
            return Contact.get({id: $stateParams.id});
          }]
        }
      })
      .state('contact.new', {
        parent: 'contact',
        url: '/new',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/contact/contact-dialog.html',
            controller: 'ContactDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  salutation: null,
                  firstName: null,
                  lastName: null,
                  company: null,
                  mdmCompany: null,
                  contactType: null,
                  address1: null,
                  address2: null,
                  city: null,
                  state: null,
                  postalCode: null,
                  country: null,
                  region: null,
                  phoneNumber: null,
                  faxNumber: null,
                  mobileNumber: null,
                  email: null,
                  treatyType: null,
                  id: null
                };
              }
            }
          }).result.then(function () {
            $state.go('contact', null, {reload: true});
          }, function () {
            $state.go('contact');
          });
        }]
      })
      .state('contact.xnew', {
        parent: 'contact',
        url: '/xnew/{id}/{contactId}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/contact/contact-dialog.html',
            controller: 'ContactDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  salutation: null,
                  firstName: null,
                  lastName: null,
                  company: null,
                  mdmCompany: null,
                  contactType: null,
                  address1: null,
                  address2: null,
                  city: null,
                  state: null,
                  postalCode: null,
                  country: null,
                  region: null,
                  phoneNumber: null,
                  faxNumber: null,
                  mobileNumber: null,
                  email: null,
                  treatyType: null,
                  id: null
                };
              }
            }
          }).result.then(function () {
//            $state.go('contacts-company-detailContact', {id: $stateParams.id,contactId: $stateParams.contactId}, {reload: true});
          }, function () {
//            $state.go('contacts-company-detailContact', {id: $stateParams.id,contactId: $stateParams.contactId}, {reload: true});
          });
        }]
      })
      .state('contact.edit', {
        parent: 'contact',
        url: '/{id}/edit',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/contact/contact-dialog.html',
            controller: 'ContactDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['Contact', function (Contact) {
                return Contact.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('contact', null, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      })
      .state('contact-detail.edit', {
        parent: 'contact-detail',
        url: '/{id}/edit/detail',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/contact/contact-dialog.html',
            controller: 'ContactDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['Contact', function (Contact) {
                return Contact.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('contact-detail', {id: $stateParams.id}, {reload: true});
          }, function () {
            $state.go('contact-detail', {id: $stateParams.id}, {reload: true});
          });
        }]
      })
      .state('contact.delete', {
        parent: 'contact',
        url: '/{id}/delete',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/contact/contact-delete-dialog.html',
            controller: 'ContactDeleteController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['Contact', function (Contact) {
                return Contact.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('contact', null, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      });
  }

})();
