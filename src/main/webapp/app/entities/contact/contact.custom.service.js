(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('ContactCustomSrv', ContactCustomSrv);
  ContactCustomSrv.$inject = ['$resource', 'DateUtils'];
  function ContactCustomSrv($resource, DateUtils) {

    return {
      name: $resource('api/contacts/name/search/:search', {}, {
        query: { method: 'GET', params: {}, isArray: true }
      }),
      fullSearch: $resource('api/contacts/name/pagination/search/:search', {}, {
        query: { method: 'GET', params: {}, isArray: true }
      })
    };
  }
})();
