(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('FundingTypeDetailController', FundingTypeDetailController);

  FundingTypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'FundingType', 'DetailService'];

  function FundingTypeDetailController($scope, $rootScope, $stateParams, entity, FundingType, DetailService) {
    this.prototype = Object.create(DetailService.prototype);

    this.fundingType = entity;

    DetailService.call(this, $scope, $rootScope, $stateParams, FundingType, this.fundingType, "FundingType");
  }
})();
