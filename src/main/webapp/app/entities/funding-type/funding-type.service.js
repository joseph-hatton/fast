(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('FundingType', FundingType);
  
  FundingType.$inject = ['$resource'];
  
  function FundingType($resource) {
    var resourceUrl = 'api/funding-types/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          return data;
        }
      },
      'update': {method: 'PUT'}
    });
  }
})();
