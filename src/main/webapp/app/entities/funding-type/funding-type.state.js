(function () {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('funding-type', {
        parent: 'entity',
        url: '/funding-type',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.fundingType.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/funding-type/funding-types.html',
            controller: 'FundingTypeController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('fundingType');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }]
        }
      })
      .state('funding-type-detail', {
        parent: 'entity',
        url: '/funding-type/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.fundingType.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/funding-type/funding-type-detail.html',
            controller: 'FundingTypeDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('fundingType');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'FundingType', function ($stateParams, FundingType) {
            return FundingType.get({id: $stateParams.id});
          }]
        }
      })
      .state('funding-type.new', {
        parent: 'funding-type',
        url: '/new',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/funding-type/funding-type-dialog.html',
            controller: 'FundingTypeDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  name: null,
                  description: null,
                  id: null
                };
              }
            }
          }).result.then(function () {
            $state.go('funding-type', null, {reload: true});
          }, function () {
            $state.go('funding-type');
          });
        }]
      })
      .state('funding-type.edit', {
        parent: 'funding-type',
        url: '/{id}/edit',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/funding-type/funding-type-dialog.html',
            controller: 'FundingTypeDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['FundingType', function (FundingType) {
                return FundingType.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('funding-type', null, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      })
      .state('funding-type.delete', {
        parent: 'funding-type',
        url: '/{id}/delete',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/funding-type/funding-type-delete-dialog.html',
            controller: 'FundingTypeDeleteController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['FundingType', function (FundingType) {
                return FundingType.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('funding-type', null, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      });
  }

})();
