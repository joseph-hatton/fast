(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .factory('FundingTypeSearch', FundingTypeSearch);
  
  FundingTypeSearch.$inject = ['$resource'];
  
  function FundingTypeSearch($resource) {
    var resourceUrl = 'api/_search/funding-types/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true}
    });
  }
})();
