(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .controller('FundingTypeController', FundingTypeController);
  
  FundingTypeController.$inject = ['$scope', '$state', 'FundingType', 'FundingTypeSearch'];
  
  function FundingTypeController($scope, $state, FundingType, FundingTypeSearch) {
    var vm = this;
    vm.fundingTypes = [];
    vm.loadAll = function () {
      FundingType.query(function (result) {
        vm.fundingTypes = result;
      });
    };
    
    vm.search = function () {
      if (!vm.searchQuery) {
        return vm.loadAll();
      }
      FundingTypeSearch.query({query: vm.searchQuery}, function (result) {
        vm.fundingTypes = result;
      });
    };
    vm.loadAll();
    
  }
})();
