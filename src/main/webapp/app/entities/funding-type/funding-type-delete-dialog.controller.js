(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .controller('FundingTypeDeleteController', FundingTypeDeleteController);
  
  FundingTypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'FundingType'];
  
  function FundingTypeDeleteController($uibModalInstance, entity, FundingType) {
    var vm = this;
    vm.fundingType = entity;
    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };
    vm.confirmDelete = function (id) {
      FundingType.delete({id: id},
        function () {
          $uibModalInstance.close(true);
        });
    };
  }
})();
