(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('FundingTypeDialogController', FundingTypeDialogController);

  FundingTypeDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'FundingType', 'DialogService'];

  function FundingTypeDialogController ($scope, $stateParams, $uibModalInstance, entity, FundingType, DialogService) {
    this.prototype = Object.create(DialogService.prototype);

    var datePickerItemsRun = function (vm) {};

    this.fundingType = entity;

    DialogService.call(this, $scope, $stateParams, $uibModalInstance, entity, FundingType, this.fundingType, "FundingType", datePickerItemsRun);
  }
})();
