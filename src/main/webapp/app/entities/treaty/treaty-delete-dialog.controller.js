(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('TreatyDeleteController', TreatyDeleteController);

  TreatyDeleteController.$inject = ['$uibModalInstance', '$stateParams', 'entity', 'Treaty', 'TreatyTransactionSrv'];

  function TreatyDeleteController($uibModalInstance, $stateParams,entity, Treaty,TreatyTransactionSrv) {
    var vm = this;
    vm.treaty = entity;
    vm.amendmentsList=[];
    vm.dealId = $stateParams.dealId;

    //vm.transactionLength=0;
    // for delete all the treaty we need delete entity dates. casade delete ( seriatim, 139002
    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };
      TreatyTransactionSrv.transactions.get({treatyId: $stateParams.id}, function (result) {
        vm.transactionLength=result.length;
      });

    vm.confirmDelete = function (id) {
      vm.treaty.isActive = 0;
      Treaty.update(vm.treaty, function () {
      $uibModalInstance.close(true);
     });
   };
  }
})();
