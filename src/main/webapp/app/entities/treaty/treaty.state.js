(function () {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('treaty', {
        parent: 'entity',
        url: '/treaty?page&sort&search',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treaty.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty/treaties.html',
            controller: 'TreatyController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,asc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
            return {
              page: PaginationUtil.parsePage($stateParams.page),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate($stateParams.sort),
              ascending: PaginationUtil.parseAscending($stateParams.sort),
              search: $stateParams.search
            };
          }],
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treaty');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }]
        }
      })
      .state('treaty-deal', {
        parent: 'entity',
        url: '/treaty/deal/{dealId}?page&sort&search',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treaty.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty/treaties.html',
            controller: 'TreatyController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,asc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
            return {
              page: PaginationUtil.parsePage($stateParams.page),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate($stateParams.sort),
              ascending: PaginationUtil.parseAscending($stateParams.sort),
              search: $stateParams.search
            };
          }],
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treaty');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'TreatySrv', function ($stateParams, TreatySrv) {
            return TreatySrv.deals.query({id: $stateParams.dealId});
          }]
        }
      })
      .state('treaty-detail', {
        parent: 'entity',
        url: '/treaty/detail/{id}/{dealId}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treaty.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty/treaty-detail.html',
            controller: 'TreatyDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treaty');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'Treaty', function ($stateParams, Treaty) {
            return Treaty.get({id: $stateParams.id});
          }]
        }
      })
      .state('treaty-deal.new', {
        parent: 'treaty-deal',
        url: '/new',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty/treaty-dialog.html',
            controller: 'TreatyDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                treatyCurrencies:[],
               cedingFlag:'No',
               treatyStatus:{id:1 ,name:"Pending",treatyStatusCode:"PEND"},
               yearEndException:'No',
               stmntCedMonthFlag:'No',
               smntAsmMonthFlag:'No',
               rptDueMonthFlag:'No',
                  reportingDueDate: {numberOfDays:null,dayType:null,period:null},
               mdmAttributes:[],
                  period: null,
                  broker: null,
                  effectiveDate: null,
                  isActive: 1,
                  agreementType: null,
                  region: null,
                  country: null,
                  settlementFrequency: null,
                  reportingFrequency: null,

                  reportingDueYearEx:null,
                reportingDueQuarterEx:null,
                stmntDueAsm:null,
                stmntDueAsmGracePeriod:null,
                stmntDueAsmQuarterEx:null,
                stmntDueAsmBroker:null,
                stmntDueCedQuarterEx:null,
                stmntDueCedGracePeriod:null,
                stmntDueCed:null

                };
              }
            }
          }).result.then(function () {
            $state.go('treaty-deal', {id: $stateParams.id, dealId: $stateParams.dealId}, {reload: true});
          }, function () {
            $state.go('treaty-deal', {id: $stateParams.id, dealId: $stateParams.dealId}, {reload: true});
          });
        }]
      })
      .state('deal-treaty-tracking.new', {
        parent: 'deal-treaty-tracking',
        url: '/new',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty/treaty-dialog.html',
            controller: 'TreatyDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  period: null,
                  broker: null,
                  effectiveDate: null,
                  //dueDate: null,
                  isActive: 1,
                  agreementType: null,
                  region: null,
                  country: null,
                  settlementFrequency: null,
                  reportingFrequency: null
                };
              }
            }
          }).result.then(function () {
            $state.go('deal-treaty-tracking', {dealId: $stateParams.dealId}, {reload: true});
          }, function () {
            $state.go('deal-treaty-tracking', {dealId: $stateParams.dealId}, {reload: true});
          });
        }]
      })
      .state('treaty-deal.delete', {
        parent: 'treaty-deal',
        url: '/delete/{dealId}/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty/treaty-delete-dialog.html',
            controller: 'TreatyDeleteController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['Treaty', function (Treaty) {
                return Treaty.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('treaty-deal', {id: $stateParams.id, dealId: $stateParams.dealId}, {reload: true});
          }, function () {
            $state.go('treaty-deal', {id: $stateParams.id, dealId: $stateParams.dealId}, {reload: true});
          });
        }]
      })
      .state('treaty-detail.edit-detail', {
        parent: 'treaty-detail',
        url: '/{id}/{dealId }/edit/{hash}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty/treaty-dialog.html',
            controller: 'TreatyDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['Treaty', function (Treaty) {
                return  Treaty.get({id: $stateParams.id}).$promise;
              }]
            }
          }).result.then(function () {
            $state.go('treaty-detail', {id: $stateParams.id, dealId: $stateParams.dealId}, {reload: true});
          }, function () {
            $state.go('treaty-detail', {id: $stateParams.id, dealId: $stateParams.dealId}, {reload: true});
          });
        }]
      })
      .state('treaty-mgmt', {
        parent: 'entity',
        url: '/treaty/mgmt/{dealId}/{treatyId}/{hash}?page&sort&search',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treaty.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty/treaties.html',
            controller: 'TreatyController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,asc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
            return {
              page: PaginationUtil.parsePage($stateParams.page),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate($stateParams.sort),
              ascending: PaginationUtil.parseAscending($stateParams.sort),
              search: $stateParams.search
            };
          }],
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treaty');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'TreatySrv', function ($stateParams, TreatySrv) {
            return TreatySrv.deals.query({id: $stateParams.dealId});
          }]
        }
      });
  }
})();
