(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .factory('TreatySearch', TreatySearch);
  
  TreatySearch.$inject = ['$resource'];
  
  function TreatySearch($resource) {
    var resourceUrl = 'api/_search/treaties/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true}
    });
  }
})();
