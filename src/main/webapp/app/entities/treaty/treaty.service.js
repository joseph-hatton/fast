(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('Treaty', Treaty);

  Treaty.$inject = ['$resource', 'DateUtils'];

  function Treaty($resource, DateUtils) {
    var resourceUrl = 'api/treaties/:id';

    return $resource(resourceUrl, {}, {
      get: {
        method: 'GET',
        isArray: false,
        transformResponse: function (data) {

          if (data === '') return null;
          data = angular.fromJson(data);
          data.effectiveDate = DateUtils.convertLocalDateFromServer(data.effectiveDate);
          data.terminationDate = DateUtils.convertLocalDateFromServer(data.terminationDate);
          return data;
        }
      },
      query: { method: 'GET', params: {}, isArray: true },
      update: {
        method: 'PUT',
        transformRequest: function (data) {
          data.effectiveDate = DateUtils.convertLocalDateToServer(data.effectiveDate);
          data.terminationDate = DateUtils.convertLocalDateToServer(data.terminationDate);
          return angular.toJson(data);
        }
      },
      save: {
        method: 'POST',
        transformRequest: function (data) {
          data.effectiveDate = DateUtils.convertLocalDateToServer(data.effectiveDate);
          data.terminationDate = DateUtils.convertLocalDateToServer(data.terminationDate);
        //   data.dueDate = DateUtils.convertLocalDateToServer(data.dueDate);
          return angular.toJson(data);
        }
      }

    });
  }
})();
