(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('TreatySrv', TreatySrv);

  TreatySrv.$inject = ['$resource', 'DateUtils'];

  function TreatySrv($resource, DateUtils) {
    return {

    transactionMdm: $resource('api/treaty-subtransaction-cash/mdm/:mdmId', {}, {
      query: { method: 'GET', params: {}, isArray: true }
    }),
      treaties: $resource('api/treaties/nopage', {}, {
        query: { method: 'GET', params: {}, isArray: true }
      }),
      deals: $resource('api/treaties/deals/:id', {}, {
        query: { method: 'GET', params: {}, isArray: true }
      }),
      treatiesCount: $resource('api/treatiesCount/deals/:dealId', {}, {
        get: { method: 'GET', params: {}, isArray: false, transformResponse: function (data) {
          data = angular.fromJson(data);
          return data;
        } }
      }),
      entityDates: $resource('api/entity-dates/treaties/:id', {}, {
        query: { method: 'GET', isArray: true }
      }),
      companies: $resource('api/client-companies/search/:search', {}, {
        query: { method: 'GET', params: {}, isArray: true }
      }),
      nopages: $resource('api/deals/nopage/:id', {}, {
        query: { method: 'GET', params: {
          sort:"name,asc"
        }, isArray: true }

      }),
      mdmTreaty: $resource('api/mdm-attributes/enterpriseId/:enterpriseId', {}, {
      query: { method: 'GET', isArray: true }

    }),

      notification: $resource('api/treaties/notification', {}, {
        query: { method: 'GET', isArray: true }
      }),
      notificationInputStatus: $resource('api/treaty-managements/treatyComplete', {}, {
        query: { method: 'GET', isArray: true }
      }),

      uniqueTreatyName: $resource('api/treaty/uniqueTreatyName', {}, {
        query: { method: 'PUT', params: {}, isArray: true }

      }),
      enterpriseKeyVersion: $resource('api/enterprise-keys/:id/create/:hbCreatedVID/change/:hbChangedVID', {}, {
        query: { method: 'GET', isArray: false }
      })

    };
  }
})();
