(function () {
  'use strict';
  angular
    .module('finreApp')
    .controller('TreatyDialogController', TreatyDialogController);

  TreatyDialogController.$inject = ['$scope', 'TreatyTrackingCustomSvr', 'TreatyTracking', '$mdDialog', '$location', '$stateParams', '$uibModalInstance', 'entity', 'Deal', 'Treaty',
    'ProductStructure', 'TreatyStatus', 'Frequency', 'AgreementType', 'TreatyManagementCustomSrv', 'Principal', 'SeriatimType', 'Currency', 'EnterpriseKey', 'TreatyTransactionSrv', 'TreatySrv', 'XrefTreatyCustomCompany', '$q'
  ];

  function TreatyDialogController($scope, TreatyTrackingCustomSvr, TreatyTracking, $mdDialog, $location, $stateParams, $uibModalInstance, entity, Deal, Treaty, ProductStructure, TreatyStatus, Frequency,
    AgreementType, TreatyManagementCustomSrv, Principal, SeriatimType, Currency, EnterpriseKey, TreatyTransactionSrv, TreatySrv, XrefTreatyCustomCompany, $q) {
    var vm = this;
    vm.treaty = entity;
    vm.EKusStat=false;
    vm.EKproductLine=false;
    vm.EKproductStructure=false;
    vm.EKdealName=false;
    var flagTracking = 0;
    var msm = ' Input Status on Treaty Basic Info has been changed from In Process to Complete. Remember to input Treaty Management Info.';
    var msnInproces ='Input Status on Treaty Basic Info remains In Process. Remember to input Treaty Management Info.';
    if($stateParams.id){
    TreatyManagementCustomSrv.get({id: $stateParams.id}).$promise
    .then(function (result) {
      if(result.inputStatus==='Complete'){
       msm = 'Input Status on both Treaty Basic Info and Management Info are now Complete. Admin has been notified.';
       msnInproces= 'Input Status on Management Info remains In Process.';
      }
    });
  }

    vm.load = function () {
      angular.forEach(vm.treaty.mdmAttributes, function (mdm, key) {
        if(mdm.usStatRisk!==vm.treaty.deal.usStatRisk) {
          vm.EKusStat=true;
        }
        if(mdm.deal.toLowerCase()!==vm.treaty.deal.name.toLowerCase()) {
          vm.EKdealName=true;
        }
        if(mdm.productLineName.toLowerCase()!==vm.treaty.deal.productLine.name.toLowerCase()) {
          vm.EKproductLine=true;
        }
        if(!mdm.productStructure.name.toLowerCase().includes('other') && mdm.productStructure.name.toLowerCase()!==vm.treaty.productStructure.name.toLowerCase()
        || (mdm.productStructure.name.toLowerCase().includes('other') && !vm.treaty.productStructure.name.toLowerCase().includes(mdm.productStructure.name.toLowerCase()))
        ) {
          vm.EKproductStructure=true;
        }
      });

      vm.seriatimDueDate = {
        numberOfDays: null,
        dayType: null,
        period: null,
        comments: null,
        treatyRefComments: null
      };
      vm.yearEndRegulatoryDueDate = {
        numberOfDays: null,
        dayType: null,
        period: null,
        comments: null,
        treatyRefComments: null
      };
      vm.dealId = $stateParams.dealId;
      vm.treatyId = $stateParams.treatyId;
      vm.statusOptions = TreatyStatus.query();
      vm.commentlist = [];
      vm.treatyParam = 0;
      vm.saveJustTreaty = 0;
      vm.treaty.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
      vm.currencyList = [];
      vm.currencyOptionBroker = [];
      vm.activated = false;
      vm.general = true;
      vm.ekExist = false;
      vm.ekData = null;
      vm.activeStatus = {
        id: 2,
        name: "Active",
        treatyStatusCode: "ACTIV"
      };
      vm.businessUnitView = '';
      vm.DayTypeOptionsBroker = ['Calendar Days After', 'Business Days After'];
      vm.yearEndRegulatoryTypes = ['Annual Statement', 'DAC Tax', 'FIT Tax'];
      vm.yearEndRegulatoryDueDateFlag = 'No';
      vm.mdmAttribute = null;
      vm.buttonEdit = false;
      vm.seriatimDueDateFlag = 'No';
      vm.periodOptions = [];
      vm.ekBrokered = {};
      vm.paramTrackingUpdate = 0;
      vm.calculationSeriatim = false;
      vm.quarterFreq = {};
      vm.flagActualTransaction = false;


      if ($stateParams.id) {
        XrefTreatyCustomCompany.treaties.query({
          id: $stateParams.id
        }).$promise.then(function (companies) {
          angular.forEach(companies, function (company, key) {
            if (company.companyType.includes('Ceding') && company.company.isRgaCompany === 0) {
              vm.reportingConfirmDateFlag = false;
            } else {
              if (company.companyType.includes('Ceding') && company.company.isRgaCompany === 1) {
                vm.reportingConfirmDateFlag = true;
              }
            }
          });
        });
      }

      Deal.get({
        id: $stateParams.dealId
      }).$promise.then(function (result) {
        vm.treaty.deal = result;
        vm.yearEndDate = YEAR_END_MONTH_OPTIONS.find(function (item) {
          return item.id === result.yearEndMonth;
        }).name + " " + result.yearEndDay;

      ProductStructure.query().$promise.then(function (result) {
        vm.structureOptions =result;
        if (vm.treaty.deal.productSubtype.name === 'Structured') {
          angular.forEach(result, function (structureOption) {
            if (structureOption.name === 'Other - Structured Finance')
              vm.treaty.productStructure = structureOption;
          });
        }

      });

       AgreementType.query().$promise.then(function (result) {
         vm.agreementTypes= result;
          if (vm.treaty.deal.productSubtype.name === 'Brokered') {
            if (vm.treaty.id === undefined) {
              EnterpriseKey.get({
                id: 121892
              }, function (result) {
                result.id = null;
                vm.ekData = result;
                vm.mdmOn = 0;
                vm.importEK();
              }, onSaveEKError);
            }
            angular.forEach(result, function (agreementType) {
              if (agreementType.name === 'Brokered') {
                vm.treaty.agreementType = agreementType;
              }
            });
          }

      });
      });

      //SECTION for HASHING and opening up Accordions
      if ($stateParams.hash !== null &&
        $stateParams.hash !== undefined) {
        if ($stateParams.hash === "generalInfo") {
          vm.general = true;
        } else if ($stateParams.hash === "currency") {
          vm.currencySection = true;
          vm.general = false;
        } else if ($stateParams.hash === "mdmAttr") {
          vm.mdm = true;
          vm.general = false;
        } else if ($stateParams.hash === "reportingDue") {
          vm.reporting = true;
          vm.general = false;
        } else if ($stateParams.hash === "seriatimInfo") {
          vm.seriatim = true;
          vm.general = false;
        } else if ($stateParams.hash === "settlementAssuming") {
          vm.assuming = true;
          vm.general = false;
        } else if ($stateParams.hash === "settlementCeding") {
          vm.ceding = true;
          vm.general = false;
        } else if ($stateParams.hash === "yearEndRegulatory") {
          vm.yearEndRegulatory = true;
          vm.general = false;

        } else {
          vm.penalty = true;
          vm.general = false;
        }
        $location.hash($stateParams.hash);
      }

      Currency.query(function (result) {
        vm.currencyOptions = result;
        vm.defaultCurrency = vm.currencyOptions[85];
        vm.disableAlreadyAddedCurrencies();
      });

      Principal.identity().then(function (account) {
        angular.forEach(account.authorities, function (authoritie) {
          if (authoritie === "ROLE_APP_FINRE_OPS_ADMIN") {
            vm.authority = 'Admin';
          } else {
            if (vm.authority !== 'Admin') {
              vm.authority = 'User';
            }
          }
        });
        vm.lastUser = account.login;
      });


      vm.DayTypeOptions = ['Calendar Days After', 'Calendar Days Before ', 'Business Days After', 'Business Days Before', 'On'];
      vm.MonthlyDayTypeOptions = ['Month End', 'Quarter End'];
      vm.QuarterDayTypeOptions = ['Quarter End'];
      vm.SemiAnnualDayTypeOptions = ['Half Year'];
      vm.AnnualDayTypeOptions = ['Year End'];
    };

    vm.load();

    vm.checkMDM=function(){
      vm.EKproductStructure=false;
      angular.forEach(vm.treaty.mdmAttributes, function (mdm, key) {
      if(mdm.productStructure.name.toLowerCase()!==vm.treaty.productStructure.name.toLowerCase()) {
        vm.EKproductStructure=true;
      }})
    };


    vm.checkValue=function(val1,val2){

      return (val1===val2)
    };



    vm.selectFirstItem = function () {
      var flag = false;
      var first;
      angular.forEach(vm.treaty.seriatimTypeTreaties, function (seriatim, index) {
        if (seriatim.seriatimDueDateFlag == "Yes")
          if (seriatim.calculationFlag) {
            flag = true;
            vm.dueDateSelected = index;
          } else if (first == null)
          first = index;
      });
      if (first != null && !flag) {
        vm.treaty.seriatimTypeTreaties[first].calculationFlag = true;
        vm.dueDateSelected = first;
      }
    };

    vm.selectFirstItem();

    Frequency.query(function (result) {
      angular.forEach(result, function (Frequency) {
        if (Frequency.name !== 'Occurrence' && Frequency.name !== 'Initial') {
          vm.periodOptions.push(Frequency);
        }
        if (Frequency.name === 'Quarterly') {
          vm.quarterFreq = Frequency;
        }
      });
      if ($stateParams.id != null && vm.treaty.$promise) {
        vm.latepaymentFn();
        vm.changeReportingFrequency(1);
        vm.changeFrequencyDueAssuming(1);
        vm.changeFrequencyDueCeding(1);
        angular.forEach(vm.treaty.mdmAttributes, function (mdm, key) {
          if (mdm.ekComment == null) {
            mdm.ekComment = '';
          }


        });
        if (vm.treaty.businessUnitId !== undefined || vm.treaty.businessUnitId !== null) {
          vm.businessUnitView = '' + vm.treaty.businessUnitId + '' + vm.treaty.businessUnitName;
        }
        angular.forEach(vm.treaty.treatyCurrencies, function (currency) {
          vm.currencyOptionBroker.push(currency.currency);
        });
        if (vm.treaty.brokerFeeSwitchFlag === null) {
          vm.treaty.brokerFeeSwitchFlag = 'No';
        }
      }
    });

    vm.cleanImport = function () {
      vm.ekExist = null;
    };


    vm.changeLegalDocId= function (mdm){
      mdm.lastUser=vm.lastUser;
    };
    vm.importEK = function () {
      vm.ekData.creationUser = 'ImportMDM';
      vm.ekData.creationTs = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
      vm.ekData.lastUser = 'ImportMDM';
      vm.ekData.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
      if (vm.ekData.usStatRisk === 'B' || vm.ekData.usStatRisk === 'S') {
        vm.ekData.usStatRisk = 'Yes';
      } else {
        vm.ekData.usStatRisk = 'No';
      }
      if (vm.ekData.effectiveStatusCd === 'A') {
        vm.ekData.effectiveStatusCd = 'Active';
      } else {
        vm.ekData.effectiveStatusCd = 'Inactive';
      }
      var codeCurrencyEK = vm.ekData.businessUnitName.substr(vm.ekData.businessUnitName.indexOf("-") + 1, 3).toUpperCase();
      if (vm.mdmOn === 0) {
        if (vm.treaty.deal.productSubtype.name !== 'Brokered') {
          TreatySrv.mdmTreaty.query({
            enterpriseId: vm.ekData.enterpriseId
          }, function (result) {
            if (result.length > 0) {
              var confirm = $mdDialog.alert()
                .textContent('You cannot add this EK because it is already linked to Treaty')
                .ok('OK')
                .disableParentScroll(false)
                .parent(angular.element(document.querySelector('#myModal')));

              $mdDialog.show(confirm).then(function () {});
            } else {
              if (vm.ekData.productStructure.name != vm.treaty.productStructure.name) {
                var confirmChangeStruture = $mdDialog.confirm()
                  .title('Existing Structure Value vs. MDM Structure Value.')
                  .textContent(vm.ekData.productStructure.name + ' - vs - ' + vm.treaty.productStructure.name)
                  .ok('OVERWRITE')
                  .cancel('DONT OVERWRITE')
                  .disableParentScroll(false)
                  .parent(angular.element(document.querySelector('#myModal')));

                $mdDialog.show(confirmChangeStruture).then(function () {
                  vm.treaty.mdmAttributes.push((vm.ekData));
                  vm.searchCurrecyByCode(codeCurrencyEK);
                  vm.treaty.productStructure = vm.ekData.productStructure;
                  vm.mdmOn = 1;
                }, function () {
                  vm.treaty.mdmAttributes.push((vm.ekData));
                  vm.searchCurrecyByCode(codeCurrencyEK);
                  vm.mdmOn = 1;
                });

              } else {
                vm.treaty.mdmAttributes.push((vm.ekData));
                vm.searchCurrecyByCode(codeCurrencyEK);
                vm.mdmOn = 1;
              }

            }
          });
        } else {
          vm.treaty.mdmAttributes.push((vm.ekData));
          vm.searchCurrecyByCode(codeCurrencyEK);
          vm.mdmOn = 1;
        }
      }
    };



    vm.disableAlreadyAddedCurrencies = function () {
      var alreadyAddedText = " - Already Added";
      angular.forEach(vm.currencyOptions, function (currency) {
        var flag = false;
        if (currency.name.includes(alreadyAddedText))
          flag = true;
        if (vm.treaty.treatyCurrencies.filter(function (c) {
            return c.currency != null ? c.currency.code === currency.code : false;
          }).length > 0) {
          if (!flag)
            currency.name += alreadyAddedText;
          currency.disabled = true;
        } else {
          if (flag)
            currency.name.replace(alreadyAddedText, "");
          currency.disabled = false;
        }
      });
    };



    vm.changeReportingFrequency = function (param) {
      if (vm.treaty.reportingDueDate === null) {
        vm.treaty.reportingDueDate = {
          numberOfDays: null,
          dayType: null,
          period: null,
          comments: null,
          treatyRefComments: null
        };
      }
      if (vm.treaty.reportingFrequency !== null && vm.treaty.reportingFrequency !== undefined) {
        if (vm.treaty.reportingDueDate.dayType === null || vm.treaty.reportingDueDate.dayType === '') {
          vm.treaty.reportingDueDate.dayType = vm.DayTypeOptions[0];
        }
        if (vm.treaty.reportingFrequency.name === 'Monthly') {
          vm.DayTypeOptionsRep = vm.MonthlyDayTypeOptions;
          if (vm.treaty.reportingDueDate.period === null || param === 0) {
            vm.treaty.reportingDueDate.period = vm.DayTypeOptionsRep[0];
          }
          if (vm.treaty.reportingDueDate.dayType === null || param === 0) {
            vm.treaty.reportingDueDate.dayType = vm.DayTypeOptions[0];
          }
        }
        if (vm.treaty.reportingFrequency.name === 'Quarterly') {
          vm.DayTypeOptionsRep = vm.QuarterDayTypeOptions;
          if (vm.treaty.reportingDueDate.period === null || param === 0) {
            vm.treaty.reportingDueDate.period = vm.DayTypeOptionsRep[0];
          }
          if (vm.treaty.reportingDueDate.dayType === null || param === 0) {
            vm.treaty.reportingDueDate.dayType = vm.DayTypeOptions[0];
          }

          vm.treaty.rptDueMonthFlag = 'No';
          vm.changeFlagDate(vm.treaty.rptDueMonthFlag, vm.treaty.reportingDueQuarterEx);

        }

        if (vm.treaty.reportingFrequency.name === 'Semi-Annual') {
          vm.DayTypeOptionsRep = vm.SemiAnnualDayTypeOptions;
          if (vm.treaty.reportingDueDate.period === null || param === 0) {
            vm.treaty.reportingDueDate.period = vm.DayTypeOptionsRep[0];
          }
          if (vm.treaty.reportingDueDate.dayType === null || param === 0) {
            vm.treaty.reportingDueDate.dayType = vm.DayTypeOptions[0];
          }
          vm.treaty.rptDueMonthFlag = 'No';
          vm.changeFlagDate(vm.treaty.rptDueMonthFlag, vm.treaty.reportingDueQuarterEx);


        }
        if (vm.treaty.reportingFrequency.name === 'Annual') {
          vm.treaty.yearEndException = 'No';
          vm.changYearExceptFlag();
          vm.treaty.rptDueMonthFlag = 'No';
          vm.changeFlagDate(vm.treaty.rptDueMonthFlag, vm.treaty.reportingDueQuarterEx);
          vm.DayTypeOptionsRep = vm.AnnualDayTypeOptions;
          if (vm.treaty.reportingDueDate.period === null || param === 0) {
            vm.treaty.reportingDueDate.period = vm.DayTypeOptionsRep[0];
          }
          if (vm.treaty.reportingDueDate.dayType === null || param === 0) {
            vm.treaty.reportingDueDate.dayType = vm.DayTypeOptions[0];
          }


          if (vm.treaty.reportingDueDate.dayType === '' || vm.treaty.reportingDueDate.dayType === 'Year End') {
            vm.treaty.reportingDueDate.numberOfDays = 0;
          }
        }
        if (vm.treaty.reportingDueDate.dayType === 'Year End' || vm.treaty.reportingDueDate.dayType === 'Half Year' || vm.treaty.reportingDueDate.dayType === 'Month End') {
          vm.treaty.reportingDueDate.numberOfDays = 0;
        }
      } else {
        vm.treaty.reportingDueDate = null;
        vm.treaty.reportingDueYearEx = null;
        vm.treaty.yearEndException = 'No';

      }
    };

    vm.MonthlyTypeOptions = ['Month End', 'Quarter End', 'Reporting Received', 'Reporting Confirmed'];
    vm.QuarterTypeOptions = ['Quarter End', 'Reporting Received', 'Reporting Confirmed'];
    vm.SemiAnnualTypeOptions = ['Half Year', 'Reporting Received', 'Reporting Confirmed'];
    vm.AnnualTypeOptions = ['Year End', 'Reporting Received', 'Reporting Confirmed'];

    vm.changeFrequencyDueAssuming = function (param) {

      if (vm.treaty.stmntDueAsm === null) {
        vm.treaty.stmntDueAsm = {
          numberOfDays: null,
          dayType: null,
          period: null,
          comments: null,
          treatyRefComments: null
        };
      }
      if (vm.treaty.stmntDueAsmGracePeriod === null) {
        vm.treaty.stmntDueAsmGracePeriod = {
          numberOfDays: null,
          dayType: null,
          period: null,
          comments: null,
          treatyRefComments: null
        };
      }

      if (vm.treaty.settlementFrequency !== null && vm.treaty.settlementFrequency !== undefined) {
        if (vm.treaty.stmntDueAsm.dayType === null) {
          vm.treaty.stmntDueAsm.dayType = vm.DayTypeOptions[0];
          if (vm.treaty.stmntDueAsmGracePeriod.period === null || param === 0) {
            vm.treaty.stmntDueAsmGracePeriod.dayType = vm.DayTypeOptions[0];
          }

        }
        if (vm.treaty.settlementFrequency.name === 'Monthly') {
          vm.periodOptionDue = vm.MonthlyTypeOptions;
          if (vm.treaty.stmntDueAsm.period === null || param === 0) {
            vm.treaty.stmntDueAsm.period = vm.periodOptionDue[0];
          }
          if (vm.treaty.stmntDueAsm.dayType === null || param === 0) {
            vm.treaty.stmntDueAsm.dayType = vm.DayTypeOptions[0];
          }
          vm.periodOptionPastDue = vm.MonthlyTypeOptions;
          if (vm.treaty.stmntDueAsmGracePeriod.period === null || param === 0) {
            vm.treaty.stmntDueAsmGracePeriod.period = vm.periodOptionDue[0];
          }
          if (vm.treaty.stmntDueAsmGracePeriod.dayType === null || param === 0) {
            vm.treaty.stmntDueAsmGracePeriod.dayType = vm.DayTypeOptions[0];
          }
        }
        if (vm.treaty.settlementFrequency.name === 'Quarterly') {
          vm.periodOptionDue = vm.QuarterTypeOptions;
          if (vm.treaty.stmntDueAsm.period === null || param === 0) {
            vm.treaty.stmntDueAsm.period = vm.periodOptionDue[0];
          }
          if (vm.treaty.stmntDueAsm.dayType === null || param === 0) {
            vm.treaty.stmntDueAsm.dayType = vm.DayTypeOptions[0];
          }
          vm.periodOptionPastDue = vm.QuarterTypeOptions;
          if (vm.treaty.stmntDueAsmGracePeriod.period === null || param === 0) {
            vm.treaty.stmntDueAsmGracePeriod.period = vm.periodOptionDue[0];
          }

          if (vm.treaty.stmntDueAsmGracePeriod.dayType === null || param === 0) {
            vm.treaty.stmntDueAsmGracePeriod.dayType = vm.DayTypeOptions[0];
          }
          vm.treaty.smntAsmMonthFlag = 'No';
          vm.changeFlagDate(vm.treaty.smntAsmMonthFlag, vm.treaty.stmntDueAsmQuarterEx);
        }
        if (vm.treaty.settlementFrequency.name === 'Semi-Annual') {
          vm.periodOptionDue = vm.SemiAnnualTypeOptions;
          if (vm.treaty.stmntDueAsm.period === null || param === 0) {
            vm.treaty.stmntDueAsm.period = vm.periodOptionDue[0];
          }
          if (vm.treaty.stmntDueAsm.dayType === null || param === 0) {
            vm.treaty.stmntDueAsm.dayType = vm.DayTypeOptions[0];
          }
          vm.periodOptionPastDue = vm.SemiAnnualTypeOptions;
          if (vm.treaty.stmntDueAsmGracePeriod.period === null || param === 0) {
            vm.treaty.stmntDueAsmGracePeriod.period = vm.periodOptionDue[0];
          }
          if (vm.treaty.stmntDueAsmGracePeriod.dayType === null || param === 0) {
            vm.treaty.stmntDueAsmGracePeriod.dayType = vm.DayTypeOptions[0];
          }
          vm.treaty.smntAsmMonthFlag = 'No';
          vm.changeFlagDate(vm.treaty.smntAsmMonthFlag, vm.treaty.stmntDueAsmQuarterEx);
        }
        if (vm.treaty.settlementFrequency.name === 'Annual') {
          vm.periodOptionDue = vm.AnnualTypeOptions;
          if (vm.treaty.stmntDueAsm.period === null || param === 0) {
            vm.treaty.stmntDueAsm.period = vm.periodOptionDue[0];
          }
          if (vm.treaty.stmntDueAsm.dayType === null || param === 0) {
            vm.treaty.stmntDueAsm.dayType = vm.DayTypeOptions[0];
          }
          vm.periodOptionPastDue = vm.AnnualTypeOptions;
          if (vm.treaty.stmntDueAsmGracePeriod.period === null || param === 0) {
            vm.treaty.stmntDueAsmGracePeriod.period = vm.periodOptionDue[0];
          }
          if (vm.treaty.stmntDueAsmGracePeriod.dayType === null || param === 0) {
            vm.treaty.stmntDueAsmGracePeriod.dayType = vm.DayTypeOptions[0];
          }
          vm.treaty.smntAsmMonthFlag = 'No';
          vm.changeFlagDate(vm.treaty.smntAsmMonthFlag, vm.treaty.stmntDueAsmQuarterEx);
        }
      } else {
        vm.treaty.stmntDueAsm = null;
        vm.treaty.stmntDueAsmGracePeriod = null;
      }
    };
    vm.changeFrequencyDueCeding = function (param) {
      if (vm.treaty.stmntDueCed === null) {
        vm.treaty.stmntDueCed = {
          numberOfDays: null,
          dayType: null,
          period: null,
          comments: null,
          treatyRefComments: null
        };
      }
      if (vm.treaty.stmntDueCedGracePeriod === null) {
        vm.treaty.stmntDueCedGracePeriod = {
          numberOfDays: null,
          dayType: null,
          period: null,
          comments: null,
          treatyRefComments: null
        };
      }
      if (vm.treaty.stmntFrqncCeding !== null &&
        vm.treaty.stmntFrqncCeding !== '' &&
        vm.treaty.stmntFrqncCeding !== undefined) {
        if (vm.treaty.stmntDueCed === undefined ||
          vm.treaty.stmntDueCed.dayType === null ||
          vm.treaty.stmntDueCed.dayType === undefined) {

          if (vm.treaty.stmntDueCed.dayType === null || param == 0) {
            vm.treaty.stmntDueCed.dayType = vm.DayTypeOptions[0];
          }
          if (vm.treaty.stmntDueCedGracePeriod.dayType === null || param === 0) {
            vm.treaty.stmntDueCedGracePeriod.dayType = vm.DayTypeOptions[0];
          }
        }

        if (vm.treaty.stmntFrqncCeding.name === 'Monthly') {
          vm.periodOptionDueCeding = vm.MonthlyTypeOptions;
          vm.periodOptionPastDueCeding = vm.MonthlyTypeOptions;
          if (vm.treaty.stmntDueCed.period === null || param == 0) {
            vm.treaty.stmntDueCed.period = vm.periodOptionDueCeding[0];
          }
          if (vm.treaty.stmntDueCed.dayType === null || param == 0) {
            vm.treaty.stmntDueCed.dayType = vm.DayTypeOptions[0];
          }
          if (vm.treaty.stmntDueCedGracePeriod.dayType === null || param === 0) {
            vm.treaty.stmntDueCedGracePeriod.dayType = vm.DayTypeOptions[0];
          }
          if (vm.treaty.stmntDueCedGracePeriod.period === null || param === 0) {
            vm.treaty.stmntDueCedGracePeriod.period = vm.periodOptionDueCeding[0];
          }
        }
        if (vm.treaty.stmntFrqncCeding.name === 'Quarterly') {
          vm.periodOptionDueCeding = vm.QuarterTypeOptions;
          vm.periodOptionPastDueCeding = vm.QuarterTypeOptions;
          if (vm.treaty.stmntDueCed.period === null || param == 0) {
            vm.treaty.stmntDueCed.period = vm.periodOptionDueCeding[0];
          }
          if (vm.treaty.stmntDueCed.dayType === null || param == 0) {
            vm.treaty.stmntDueCed.dayType = vm.DayTypeOptions[0];
          }
          if (vm.treaty.stmntDueCedGracePeriod.dayType === null || param === 0) {
            vm.treaty.stmntDueCedGracePeriod.dayType = vm.DayTypeOptions[0];
          }
          if (vm.treaty.stmntDueCedGracePeriod.period === null || param === 0) {
            vm.treaty.stmntDueCedGracePeriod.period = vm.periodOptionDueCeding[0];
          }
          vm.treaty.stmntCedMonthFlag = 'No';
          vm.changeFlagDate(vm.treaty.stmntCedMonthFlag, vm.treaty.stmntDueCedQuarterEx);
        }
        if (vm.treaty.stmntFrqncCeding.name === 'Semi-Annual') {
          vm.periodOptionDueCeding = vm.SemiAnnualTypeOptions;
          vm.periodOptionPastDueCeding = vm.SemiAnnualTypeOptions;
          if (vm.treaty.stmntDueCed.period === null || param == 0) {
            vm.treaty.stmntDueCed.period = vm.periodOptionDueCeding[0];
          }
          if (vm.treaty.stmntDueCed.dayType === null || param == 0) {
            vm.treaty.stmntDueCed.dayType = vm.DayTypeOptions[0];
          }
          if (vm.treaty.stmntDueCedGracePeriod.dayType === null || param === 0) {
            vm.treaty.stmntDueCedGracePeriod.dayType = vm.DayTypeOptions[0];
          }
          if (vm.treaty.stmntDueCedGracePeriod.period === null || param === 0) {
            vm.treaty.stmntDueCedGracePeriod.period = vm.periodOptionDueCeding[0];
          }
          vm.treaty.stmntCedMonthFlag = 'No';
          vm.changeFlagDate(vm.treaty.stmntCedMonthFlag, vm.treaty.stmntDueCedQuarterEx);
        }
        if (vm.treaty.stmntFrqncCeding.name === 'Annual') {
          vm.periodOptionDueCeding = vm.AnnualTypeOptions;
          vm.periodOptionPastDueCeding = vm.AnnualTypeOptions;
          if (vm.treaty.stmntDueCed.period === null || param == 0) {
            vm.treaty.stmntDueCed.period = vm.periodOptionDueCeding[0];
          }
          if (vm.treaty.stmntDueCed.dayType === null || param == 0) {
            vm.treaty.stmntDueCed.dayType = vm.DayTypeOptions[0];
          }
          if (vm.treaty.stmntDueCedGracePeriod.dayType === null || param === 0) {
            vm.treaty.stmntDueCedGracePeriod.dayType = vm.DayTypeOptions[0];
          }
          if (vm.treaty.stmntDueCedGracePeriod.period === null || param === 0) {
            vm.treaty.stmntDueCedGracePeriod.period = vm.periodOptionDueCeding[0];
          }
          vm.treaty.stmntCedMonthFlag = 'No';
          vm.changeFlagDate(vm.treaty.stmntCedMonthFlag, vm.treaty.stmntDueCedQuarterEx);
        }
      } else {
        vm.treaty.stmntDueCed = null;
        vm.treaty.stmntDueCedGracePeriod = null;
      }
    };
    vm.changeFrequencySeriatim = function () {
      if (vm.seriatimFreq !== null && vm.seriatimFreq !== undefined) {
        if (vm.seriatimDueDate === null || vm.seriatimDueDate.numberOfDays === null) {
          vm.seriatimDueDate = {
            numberOfDays: null,
            dayType: null,
            period: null,
            comments: null,
            treatyRefComments: null
          };
        }
        vm.seriatimDueDate.dayType = vm.DayTypeOptions[0];
        if (vm.seriatimFreq.name === 'Monthly') {
          vm.DayTypeOptionsSer = vm.MonthlyDayTypeOptions;
          vm.seriatimDueDate.period = vm.MonthlyDayTypeOptions[0];

        }
        if (vm.seriatimFreq.name === 'Quarterly') {
          vm.DayTypeOptionsSer = vm.QuarterDayTypeOptions;
          vm.seriatimDueDate.period = vm.QuarterDayTypeOptions[0];

        }

        if (vm.seriatimFreq.name === 'Semi-Annual') {
          vm.DayTypeOptionsSer = vm.SemiAnnualDayTypeOptions;
          vm.seriatimDueDate.period = vm.SemiAnnualDayTypeOptions[0];

        }
        if (vm.seriatimFreq.name === 'Annual') {
          vm.DayTypeOptionsSer = vm.AnnualDayTypeOptions;
          vm.seriatimDueDate.period = vm.AnnualDayTypeOptions[0];

          if (vm.seriatimDueDate.dayType === '' || vm.seriatimDueDate.dayType === 'Year End') {
            vm.seriatimDueDate.numberOfDays = 0;
          }
        }
        if (vm.seriatimDueDate.dayType === 'Year End' || vm.seriatimDueDate.dayType === 'Half Year' || vm.seriatimDueDate.dayType === 'Month End') {
          vm.seriatimDueDate.numberOfDays = 0;
        }
      } else {
        if (vm.seriatimDueDate === null) {} else {
          vm.seriatimDueDate.numberOfDays = null;
          vm.seriatimDueDate.dayType = null;
          vm.seriatimDueDate.period = null;
        }
      }
    };

    vm.changeStatus = function () {
      if (vm.treaty.treatyStatus.name !== 'Terminated')
        vm.treaty.terminationDate = '';
    };


    /////////Tracking Function /////////////


    var onSuccessTrackingList = function (result) {
      vm.paramTrackingUpdate = result.length;
      if (vm.paramTrackingUpdate === 0) {
        $uibModalInstance.close();
        vm.isSaving = false;
      } else {
        angular.forEach(result, function (tracking, key) {
          if ((moment(tracking.reportingMonthYear).month() === 2 || moment(tracking.reportingMonthYear).month() === 5 || moment(tracking.reportingMonthYear).month() === 8 || moment(tracking.reportingMonthYear).month() === 11)) {
            vm.endQuarterFlag = true;
          } else {
            vm.endQuarterFlag = false;
          }

          var reportingConfirmDate = null;
          if (vm.reportingConfirmDateFlag) {
            reportingConfirmDate = tracking.confirmationReceivedDate;
          } else {
            reportingConfirmDate = tracking.emailDate;
          }
          switch (vm.newTreaty.stmntDueAsm.period) {
            case "Reporting Received":
              if (tracking.reportingReceivedDate) {
                tracking.calculatedAssumingDate = calculatingDate(vm.treaty.stmntDueAsm.period, vm.treaty.stmntDueAsm, tracking.reportingReceivedDate, tracking.treaty.settlementFrequency.name);
              } else {
                tracking.calculatedAssumingDate = null;
              }
              break;
            case "Reporting Confirmed":
              if (reportingConfirmDate !== null) {
                tracking.calculatedAssumingDate = calculatingDate(tracking.treaty.stmntDueAsm.period, tracking.treaty.stmntDueAsm, reportingConfirmDate, tracking.treaty.settlementFrequency.name);
              } else {
                tracking.calculatedAssumingDate = null;
              }
              break;
            default:
              if (tracking.reportingMonthYear != null) {
                tracking.calculatedAssumingDate = calculatingDate(vm.treaty.stmntDueAsm.period, vm.treaty.stmntDueAsm, tracking.reportingMonthYear, tracking.treaty.settlementFrequency.name);
              } else {
                tracking.calculatedAssumingDate = null;
              }
          }

if(vm.newTreaty.stmntDueCed != null){
          switch (vm.newTreaty.stmntDueCed.period) {
            case "Reporting Received":
              if (tracking.reportingReceivedDate) {
                tracking.calculatedCedingDate = calculatingDate(vm.treaty.stmntDueCed.period, vm.treaty.stmntDueCed, tracking.reportingReceivedDate, tracking.treaty.stmntFrqncCeding.name);
              } else {
                tracking.calculatedCedingDate = null;
              }
              break;
            case "Reporting Confirmed":
              if (reportingConfirmDate !== null) {
                tracking.calculatedCedingDate = calculatingDate(tracking.treaty.stmntDueCed.period, tracking.treaty.stmntDueCed, reportingConfirmDate, tracking.treaty.stmntFrqncCeding.name);
              } else {
                tracking.calculatedCedingDate = null;
              }
              break;
            default:
              if (tracking.reportingMonthYear != null && vm.treaty.stmntFrqncCeding) {
                tracking.calculatedCedingDate = calculatingDate(vm.treaty.stmntDueCed.period, vm.treaty.stmntDueCed, tracking.reportingMonthYear, tracking.treaty.stmntFrqncCeding.name);
              } else {
                tracking.calculatedCedingDate = null;
              }
          }

          }else{
  tracking.calculatedCedingDate = null;
}

          if (tracking.reportingMonthYear != null && vm.newTreaty.reportingDueDate.period !== undefined && vm.newTreaty.reportingFrequency !== null) {
            if (vm.endQuarterFlag && vm.treaty.reportingDueQuarterEx != null && vm.treaty.reportingDueQuarterEx.period != null && vm.treaty.reportingDueQuarterEx.numberOfDays != null && vm.treaty.reportingDueQuarterEx.dayType != null) {
              tracking.calculatedReportingDueDate = calculatingDate(vm.newTreaty.reportingFrequency.name, vm.treaty.reportingDueQuarterEx, tracking.reportingMonthYear, tracking.treaty.reportingFrequency.name);
            } else {
              tracking.calculatedReportingDueDate = calculatingDate(vm.newTreaty.reportingFrequency.name, vm.newTreaty.reportingDueDate, tracking.reportingMonthYear, tracking.treaty.reportingFrequency.name);
            }
          }
          if (tracking.treaty.cedingFlag !== 'No'  && tracking.cashDueFlag !== 'No' && (vm.newTreaty.stmntDueCedGracePeriod.period !== null &&
              vm.newTreaty.stmntDueCedGracePeriod.dayType !== null)) {
            if (vm.newTreaty.stmntDueCedGracePeriod.period === 'Reporting Received') {
              if (tracking.reportingReceivedDate !== undefined && tracking.reportingReceivedDate !== null) {
                tracking.calculatedCedingGraceDate = calculatingDate(vm.newTreaty.stmntDueCedGracePeriod.period, vm.newTreaty.stmntDueCedGracePeriod, tracking.reportingReceivedDate, tracking.treaty.stmntFrqncCeding.name);
              } else {
                tracking.calculatedCedingGraceDate = null;
                tracking.trackingStatus = "Open";
              }
            }
            if (vm.newTreaty.stmntDueCedGracePeriod.period === 'Reporting Confirmed') {
              if (reportingConfirmDate != null) {
                tracking.calculatedCedingGraceDate = calculatingDate(vm.newTreaty.stmntDueCedGracePeriod.period, vm.newTreaty.stmntDueCedGracePeriod, reportingConfirmDate, tracking.treaty.stmntFrqncCeding.name);
              } else {
                tracking.calculatedCedingGraceDate = null;
                tracking.flagEmailRec = 0;
                tracking.trackingStatus = "Open";
              }
            }
            if (vm.newTreaty.stmntDueCedGracePeriod.period !== 'Reporting Received' &&
              vm.newTreaty.stmntDueCedGracePeriod.period !== 'Reporting Confirmed') {
              tracking.calculatedCedingGraceDate = calculatingDate(vm.newTreaty.stmntDueCedGracePeriod.period, vm.newTreaty.stmntDueCedGracePeriod, tracking.reportingMonthYear, vm.newTreaty.stmntFrqncCeding.name);
            }
          } else {
            tracking.calculatedCedingGraceDate = null;
          }
          if (tracking.cashDueFlag !== 'No' &&  vm.newTreaty.stmntDueAsmGracePeriod.period !== null && vm.newTreaty.stmntDueAsmGracePeriod.dayType !== null) {
            if (vm.newTreaty.stmntDueAsmGracePeriod.period === 'Reporting Received') {
              if (tracking.reportingReceivedDate !== undefined && tracking.reportingReceivedDate !== null) {

                tracking.calculatedAssumingGraceDate = calculatingDate(vm.newTreaty.stmntDueAsmGracePeriod.period, vm.newTreaty.stmntDueAsmGracePeriod, tracking.reportingReceivedDate, tracking.treaty.settlementFrequency.name);
              } else {

                tracking.calculatedAssumingGraceDate = null;
              }
            }
            if (vm.newTreaty.stmntDueAsmGracePeriod.period === 'Reporting Confirmed' && reportingConfirmDate !== null) {
              if (reportingConfirmDate != null) {
                tracking.calculatedAssumingGraceDate = calculatingDate(vm.newTreaty.stmntDueAsmGracePeriod.period, vm.newTreaty.stmntDueAsmGracePeriod, reportingConfirmDate, tracking.treaty.settlementFrequency.name);
              } else {
                tracking.calculatedAssumingGraceDate = null;
              }
            } else {
              if (vm.newTreaty.stmntDueAsmGracePeriod.period === 'Reporting Confirmed' && reportingConfirmDate === null) {
                tracking.calculatedAssumingGraceDate = null;
              }
            }

            if (vm.newTreaty.stmntDueAsmGracePeriod.period !== 'Reporting Received' && vm.newTreaty.stmntDueAsmGracePeriod.period !== 'Reporting Confirmed' && tracking.treaty.settlementFrequency !== null) {

              tracking.calculatedAssumingGraceDate = calculatingDate(vm.newTreaty.stmntDueAsmGracePeriod.period, vm.newTreaty.stmntDueAsmGracePeriod, tracking.reportingMonthYear, tracking.treaty.settlementFrequency.name);
            }
          }else {
            tracking.calculatedAssumingGraceDate = null;
          }
          if (vm.newTreaty.stmntDueAsmBroker !== null) {
            if (tracking.cashSentRecDate && vm.newTreaty.stmntDueAsmBroker.numberOfDays !== null && vm.newTreaty.stmntDueAsmBroker.numberOfDays !== undefined) {
              tracking.brokerFeeDueDate = (moment(tracking.cashSentRecDate).add(vm.newTreaty.stmntDueAsmBroker.numberOfDays, 'days'));
            }
          } else {
            tracking.brokerFeeDueDate = null;
          }
          if (vm.endQuarterFlag && vm.newTreaty.stmntDueAsmQuarterEx && vm.newTreaty.stmntDueAsmQuarterEx.period != null &&
            vm.newTreaty.stmntDueAsmQuarterEx.numberOfDays != null && vm.newTreaty.stmntDueAsmQuarterEx.dayType != null &&
            vm.newTreaty.stmntDueAsmQuarterEx.period !== 'Reporting Received' && vm.newTreaty.stmntDueAsmQuarterEx.period !== 'Reporting Confirmed') {
            tracking.calculatedAssumingGraceDate = calculatingDate(vm.newTreaty.stmntDueAsmGracePeriod.period, vm.newTreaty.stmntDueAsmGracePeriod, tracking.reportingMonthYear, tracking.treaty.settlementFrequency.name);
          }
          if (vm.endQuarterFlag && vm.newTreaty.stmntDueCedQuarterEx && vm.newTreaty.stmntDueCedQuarterEx.period != null &&
            vm.newTreaty.stmntDueCedQuarterEx.numberOfDays != null && vm.newTreaty.stmntDueCedQuarterEx.dayType != null &&
            vm.newTreaty.stmntDueCedQuarterEx.period !== 'Reporting Received' && vm.newTreaty.stmntDueCedQuarterEx.period !== 'Reporting Confirmed'
          ) {
            tracking.calculatedCedingGraceDate = calculatingDate(vm.newTreaty.stmntDueCedQuarterEx.period, vm.newTreaty.stmntDueCedQuarterEx, tracking.reportingMonthYear, tracking.treaty.stmntFrqncCeding.name);
          }



          if (vm.newTreaty.seriatimTypeTreaties.length > 0) {
            if(vm.newTreaty.hasCalculationSeriatim) {
              angular.forEach(vm.newTreaty.seriatimTypeTreaties, function (seriatims, key) {
                if (seriatims.seriatimDueDateFlag === 'Yes' && seriatims.calculationFlag === true) {
                  tracking.calculatedSeriatimDueDate = calculatingDate(seriatims.seriatimDueDate.period, seriatims.seriatimDueDate, tracking.reportingMonthYear, seriatims.seriatimFreq.name);
                }
              });
            }else{
              tracking.calculatedSeriatimDueDate=null;
            }
          }else{
            tracking.calculatedSeriatimDueDate=null;
          }
          tracking.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
          TreatyTracking.update(tracking, onSaveSuccessDateTracking, onSaveErrorDateTracking);
        });
      }
    };
    var onSaveSuccessDateTracking = function (result) {
      flagTracking = flagTracking + 1;
      if (vm.paramTrackingUpdate === flagTracking) {
        $uibModalInstance.close();
        $scope.$emit('finreApp:treatyUpdate', vm.newTreaty);
        vm.isSaving = false;
      }
    };
    var onSaveErrorDateTracking = function () {
      console.log('Error');
    };
    var onError = function (result) {};
    var onSaveSuccess = function (result) {
    var hasCalculationSeriatim = false;
      angular.forEach(result.seriatimTypeTreaties, function (seriatims, key) {
        if (hasCalculationSeriatim===false && seriatims.calculationFlag === true) {
          hasCalculationSeriatim = true ;
        }
      });
      ///treaty Tracking Update

      vm.newTreaty = result;
      vm.newTreaty.hasCalculationSeriatim=hasCalculationSeriatim;
      TreatyTrackingCustomSvr.tracking.get({
        treatyId: result.id
      }, onSuccessTrackingList, onError);
      ////////

    };

    var onSaveError = function () {
      vm.isSaving = false;
    };

    vm.clear = function () {

      var confirm = $mdDialog.confirm()
        .textContent('Are you sure you want to exit without saving?')
        .ok('Yes')
        .cancel('No')
        .disableParentScroll(false)
        .parent(angular.element(document.querySelector('#myModal')));
      $mdDialog.show(confirm).then(function () {
        $uibModalInstance.dismiss('cancel');
      }, function () {});

    };
    $scope.datePickerOpenStatus = {};
    $scope.datePickerOpenStatus.effectiveDate = false;
    $scope.datePickerOpenStatus.terminationDate = false;

    $scope.datePickerOpenStatus.dueDate = false;

    $scope.openCalendar = function (date) {
      $scope.datePickerOpenStatus[date] = true;
      $scope.datePickerOpenStatus[date] = true;
    };

    $scope.datePickerForReportingRecDate = {
      options: {
        showWeeks: false
      },
      status: {
        opened: false
      },
      open: function () {
        $scope.datePickerForReportingRecDate.status.opened = true;
      }
    };

    $scope.datePickerForSettlementRecDate = {
      options: {
        showWeeks: false
      },
      status: {
        opened: false
      },
      open: function () {
        $scope.datePickerForSettlementRecDate.status.opened = true;
      }
    };

    // we need declare the Entity date objects the first time to assign the EntityType ''

    SeriatimType.query(function (result) {
      angular.forEach(result, function (seriatim) {
        if (seriatim.name == 'N/A') {
          vm.seriatimType = seriatim;
          vm.seriatimDefault = angular.copy(seriatim);
        }
      });
      vm.seriatimTypes = result;
      if (vm.treaty.id == null) {
        vm.treaty.inputStatus = 'In Process';
        vm.treaty.minBrokerFeeFlag = 'No';
        vm.treaty.brokerFeeSwitchFlag = 'No';
        vm.treaty.latePayment = 'No';
        vm.commentlist = [];
        vm.latepaymentFn();
      } else {}

    });

    vm.changeFrequencyYearEndRegulatory = function () {
      if (vm.yearEndRegulatoryFreq !== null) {
        if (vm.yearEndRegulatoryDueDate === null) {

          vm.yearEndRegulatoryDueDate = {
            numberOfDays: null,
            dayType: null,
            period: null,
            comments: null,
            treatyRefComments: null
          };
        }
        if (vm.yearEndRegulatoryFreq.name === 'Monthly') {
          vm.periodRegulatoryOptions = ['Month End'];
          vm.yearEndRegulatoryDueDate.period = 'Month End';
          vm.yearEndRegulatoryDueDate.dayType = 'Calendar Days After';
        }
        if (vm.yearEndRegulatoryFreq.name === 'Quarterly') {
          vm.periodRegulatoryOptions = ['Quarter End'];
          vm.yearEndRegulatoryDueDate.period = 'Quarter End';
          vm.yearEndRegulatoryDueDate.dayType = 'Calendar Days After';
        }
        if (vm.yearEndRegulatoryFreq.name === 'Semi-Annual') {
          vm.periodRegulatoryOptions = ['Half Year'];
          vm.yearEndRegulatoryDueDate.period = 'Half Year';
          vm.yearEndRegulatoryDueDate.dayType = 'Calendar Days After';
        }
        if (vm.yearEndRegulatoryFreq.name === 'Annual') {
          vm.periodRegulatoryOptions = ['Year End'];
          vm.yearEndRegulatoryDueDate.period = 'Year End';
          vm.yearEndRegulatoryDueDate.dayType = 'Calendar Days After';
        }
      } else {
        vm.yearEndRegulatoryDueDate = null;

      }
    };

    vm.addYearEndRegulatory = function () {
      if (vm.treaty.yearEndRegulatoryList == undefined || vm.treaty.yearEndRegulatoryList == null) {
        vm.treaty.yearEndRegulatoryList = [];
      }
      var yearEndRegulatory = {
        yearEndRegulatoryType: vm.yearEndRegulatoryType,
        regulatoryDueDateFlag: vm.yearEndRegulatoryDueDateFlag,
        regulatoryDueDate: vm.yearEndRegulatoryDueDate,
        regulatoryFrequency: vm.yearEndRegulatoryFreq
      };
      vm.treaty.yearEndRegulatoryList.push(yearEndRegulatory);

      vm.yearEndRegulatoryType = null;
      vm.yearEndRegulatoryDueDateFlag = 'No';
      vm.yearEndRegulatoryDueDate = {
        numberOfDays: null,
        dayType: null,
        period: null,
        comments: null,
        treatyRefComments: null
      };
      vm.yearEndRegulatoryFreq = null;

    };


    vm.removeYearEndRegulatory = function (index) {
      vm.treaty.yearEndRegulatoryList.splice(index, 1);
      if (vm.treaty.yearEndRegulatoryList.length === 0) {
        vm.yearEndRegulatoryType = null;
        vm.yearEndRegulatoryDueDateFlag = 'No';
        vm.yearEndRegulatoryDueDate = null;
        vm.yearEndRegulatoryFreq = null;
      }
    };


    vm.addSeriamType = function (seriatimType) {
      if (vm.treaty.seriatimTypeTreaties == undefined) {
        vm.treaty.seriatimTypeTreaties = [];
      }
      var seriatimTypeTreaty = {
        seriatimType: seriatimType,
        seriatimDueDateFlag: vm.seriatimDueDateFlag,
        seriatimDueDate: vm.seriatimDueDate,
        seriatimFreq: vm.seriatimFreq,
        treatySeriatimComment: vm.treatySeriatimComment,
        treatySeriatimReference: vm.treatySeriatimReference,
        calculationFlag: false

      };

      vm.treaty.seriatimTypeTreaties.push(seriatimTypeTreaty);
      vm.seriatimType = vm.seriatimDefault;
      vm.seriatimFreq = null;
      vm.seriatimDueDateFlag = 'No';
      vm.seriatimDueDate = {
        numberOfDays: null,
        dayType: null,
        period: null,
        comments: null,
        treatyRefComments: null
      };
      vm.treatySeriatimComment = null;
      vm.treatySeriatimReference = null;
      vm.selectFirstItem();
    };

    vm.editSeritatimType = function (seriatimTypeObject, index) {
      vm.seriatimType = seriatimTypeObject.seriatimType;
      vm.seriatimFreq = seriatimTypeObject.seriatimFreq;
      vm.changeFrequencySeriatim();
      vm.seriatimDueDateFlag = seriatimTypeObject.seriatimDueDateFlag;
      vm.seriatimDueDate = seriatimTypeObject.seriatimDueDate;
      vm.treatySeriatimComment = seriatimTypeObject.treatySeriatimComment;
      vm.treatySeriatimReference = seriatimTypeObject.treatySeriatimReference;
      vm.calculationSeriatim = seriatimTypeObject.calculationFlag;
      vm.treaty.seriatimTypeTreaties.splice(index, 1);
      vm.selectFirstItem();
    };

    vm.updateSelection = function (position) {
      angular.forEach(vm.treaty.seriatimTypeTreaties, function (subscription, index) {
        subscription.calculationFlag = position === index;
      });
    };
    $scope.removeSeriamType = function (index) {
      vm.treaty.seriatimTypeTreaties.splice(index, 1);
      if (vm.treaty.seriatimTypeTreaties.length === 0) {
        vm.seriatimFreq = null;
        vm.treatySeriatimComment = null;
        vm.seriatimType = vm.seriatimDefault;
        vm.seriatimDueDateFlag = 'No';
        vm.changeFlagDate(vm.seriatimDueDateFlag, vm.seriatimDueDate);
      }
      vm.selectFirstItem();
    };

    vm.latepaymentFn = function () {
      if (vm.treaty.latePayment !== 'Yes') {
        vm.treaty.intPenaltyComment = '';
        vm.treaty.treatyRefIntPltyComment = '';
      }
    };
    vm.changeSeriatim = function () {
      vm.seriatimDueDateFlag = 'No';
      vm.treatySeriatimComment = '';
      vm.seriatimFreq = null;
      if (vm.seriatimType === undefined) {
        if (vm.seriatimType.name === 'N/A') {
          vm.seriatimDueDateFlag = 'No';
          vm.treatySeriatimComment = null;
          vm.seriatimFreq = null;
        }
      }
    };
    vm.changeCedingFlag = function (editForm) {
      if (vm.treaty.cedingFlag === 'Yes') {
        vm.treaty.stmntDueCed = {
          numberOfDays: null,
          dayType: null,
          period: null,
          comments: null,
          treatyRefComments: null
        };
        vm.treaty.stmntDueCedGracePeriod = {
          numberOfDays: null,
          dayType: null,
          period: null,
          comments: null,
          treatyRefComments: null
        };
        vm.treaty.stmntDueCedQuarterEx =null;
        vm.treaty.stmntCedMonthFlag="No";
        vm.changeFlagDate(vm.treaty.stmntCedMonthFlag,vm.treaty.stmntDueCedQuarterEx);
        vm.treaty.notification = '';
        vm.treaty.stmntFrqncCeding = null;
        vm.treaty.gpTrCeding = '';
        vm.treaty.gpCmmtsCeding = '';
        vm.treaty.notification = '';
      } else {
        if (vm.treaty.cedingFlag === 'No') {
          editForm.stmntFrqncCeding.$dirty = false;
          editForm.settlementduedaysCeding.$dirty = false;
          editForm.settlementduedaytype.$dirty = false;
          editForm.settlementdueperiod.$dirty = false;
          editForm.cedingComments.$dirty = false;
          editForm.settlementduedayspastCeding.$dirty = false;
          editForm.settlementduedaytypepast.$dirty = false;
          editForm.settlementdueperiodpast.$dirty = false;
          editForm.gpTrCeding.$dirty = false;
          editForm.gpCmmtsCeding.$dirty = false;
          vm.copyAssuming();
          vm.treaty.notification = ' - This information was copied from Settlement Due - Assuming.';

        }

      }
    };

    vm.copyAssuming = function () {
      if (vm.treaty.stmntDueCed === null) {
        vm.treaty.stmntDueCed = {
          numberOfDays: null,
          dayType: null,
          period: null,
          comments: null,
          treatyRefComments: null
        };
      }
      if (vm.treaty.stmntDueCed === null) {
        vm.treaty.stmntDueCedGracePeriod = {
          numberOfDays: null,
          dayType: null,
          period: null,
          comments: null,
          treatyRefComments: null
        };
      }
      if (vm.treaty.settlementFrequency !== null) {
        vm.treaty.stmntFrqncCeding = angular.copy(vm.treaty.settlementFrequency);
        vm.changeFrequencyDueCeding(0);
        vm.treaty.stmntDueCed.dayType = angular.copy(vm.treaty.stmntDueAsm.dayType);
        vm.treaty.stmntDueCed.numberOfDays = vm.treaty.stmntDueAsm.numberOfDays;
        vm.treaty.stmntDueCed.period = vm.treaty.stmntDueAsm.period;
        vm.treaty.stmntDueCedGracePeriod.numberOfDays = vm.treaty.stmntDueAsmGracePeriod.numberOfDays;
        vm.treaty.stmntDueCedGracePeriod.dayType = vm.treaty.stmntDueAsmGracePeriod.dayType;
        vm.treaty.stmntDueCedGracePeriod.period = vm.treaty.stmntDueAsmGracePeriod.period;
        vm.treaty.stmntDueCed.comments = vm.treaty.stmntDueAsm.comments;
        vm.treaty.stmntDueCedGracePeriod.comments = vm.treaty.stmntDueAsmGracePeriod.comments;
        vm.treaty.stmntDueCedGracePeriod.treatyRefComments = vm.treaty.stmntDueAsmGracePeriod.treatyRefComments;
      } else {
        vm.treaty.stmntDueCed = null;
        vm.treaty.stmntDueCedGracePeriod = null;
      }
    };
    vm.changYearExceptFlag = function () {
      if (vm.treaty.yearEndException === 'Yes') {
        vm.treaty.reportingDueYearEx = {
          numberOfDays: null,
          dayType: null,
          period: "Year End",
          comments: null,
          treatyRefComments: null
        };
      }
    };
    vm.changQuarterExceptFlag = function () {
      if (vm.treaty.rptDueMonthFlag === 'Yes') {
        vm.treaty.reportingDueQuarterEx = {
          numberOfDays: null,
          dayType: null,
          period: "Quarter End",
          comments: null,
          treatyRefComments: null
        };
      } else {
        vm.treaty.reportingDueQuarterEx = null;
      }
    };
    
    vm.saveMessage = function () {

      var confirm = $mdDialog.confirm()
        .textContent('Are you ready to update Input Status on Treaty Basic Info from In Process to Complete?')
        .disableParentScroll(false)
        .ok('Yes')
        .cancel('No')
        .parent(angular.element(document.querySelector('#myModal')));

      var confirmChange = $mdDialog.alert()
        .textContent(msm)
        .disableParentScroll(false)
        .ok('OK')
        .parent(angular.element(document.querySelector('#myModal')));

      if (vm.treaty.inputStatus === 'Complete') {
        vm.save();
      } else {
        $mdDialog.show(confirm).then(function () {
          vm.treaty.inputStatus = 'Complete';
          $mdDialog.show(confirmChange).then(function () {
            vm.treaty.inputStatus = 'Complete';
            vm.save();
          });
        }, function () {
          vm.remainInProcess();
        });
      }
    };

    vm.save = function () {
      vm.isSaving = true;
      vm.treaty.lastUser = vm.lastUser;
      vm.treaty.isActive = 1;

      // status complete notification 5  read notification 6
      if (vm.treaty.inputStatus === 'Complete' && vm.treaty.notificationStatus != 6) {
        vm.treaty.notificationStatus = 5;
      }
      if (vm.treaty.id !== undefined) {
        vm.treaty.lastUpdate = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
        vm.treaty.reason = '';
        Treaty.update(vm.treaty, onSaveSuccess, onSaveError);
      } else {
        vm.treaty.creationUser = vm.lastUser;
        vm.treaty.creationTs = moment().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
      if(vm.treaty.agreementType && vm.treaty.agreementType.name!=='Retro – Internal'){
        vm.treaty.disabledTracking = 0;
      }else{
            vm.treaty.disabledTracking = 1;
      }
        Treaty.save(vm.treaty, onSaveSuccess, onSaveError);
      }

    };

    vm.remainInProcess = function () {
      $mdDialog.show($mdDialog.alert()
          .textContent(msnInproces)
          .disableParentScroll(false)
          .parent(angular.element(document.querySelector('#myModal')))
          .ok('OK'))
        .finally(function () {
          vm.save();
        });
    };


    vm.saveTreaty = function (param) {
      var uniqueTreatyName = $mdDialog.alert()
        .textContent('A Treaty by this name already exists within FAST. Please enter a unique Treaty name and try again.')
        .disableParentScroll(false)
        .ok('Try Again')
        .parent(angular.element(document.querySelector('#myModal')));

      var emptyTreatyName = $mdDialog.alert()
        .textContent('Each treaty must have a name. Please enter a unique Treaty Name and try again.')
        .disableParentScroll(false)
        .ok('Try Again')
        .parent(angular.element(document.querySelector('#myModal')));


      var confirm = $mdDialog.confirm()
        //  .title('Are you ready to update Input Status on Treaty Basic Info from In Process to Complete?')
        .textContent('Are you ready to update Input Status on '+ vm.treaty.treatyName+' from In Process to Complete?')
        .disableParentScroll(false)
        .ok('Yes')
        .cancel('No')
        .parent(angular.element(document.querySelector('#myModal')));

      var confirmChange = $mdDialog.alert()
        //  .title('Are you ready to update Input Status on Treaty Basic Info from In Process to Complete?')
        .textContent(msm)
        .disableParentScroll(false)
        .ok('OK')
        .parent(angular.element(document.querySelector('#myModal')));

      var confirmAdminCloseStatus = $mdDialog.confirm()
        .textContent('Do you want to change Treaty Status from Pending to Active?')
        .disableParentScroll(false)
        .ok('OK')
        .cancel('NO')
        .parent(angular.element(document.querySelector('#myModal')));


      var terminationDateRequired = $mdDialog.alert()
        .textContent('This treaty is Terminated, so in order to Save, you must first input Termination Date, or change the treaty status to something other than Terminated.')
        .disableParentScroll(false)
        .ok('Input Termination Date')
        .parent(angular.element(document.querySelector('#myModal')));


      if (vm.treaty.treatyStatus.name === 'Terminated' && vm.treaty.terminationDate == null) {
        $mdDialog.show(terminationDateRequired);

      } else {

        if (vm.treaty.treatyName == null || vm.treaty.treatyName === '') {

          $mdDialog.show(emptyTreatyName);
        } else {
          TreatySrv.uniqueTreatyName.query(vm.treaty.treatyName, function (result) {
            if ((result.$promise && result.length === 1 &&
                vm.treaty.id != null && result[0].id === vm.treaty.id) ||
              (result.$promise && result.length === 0) || (vm.treaty.treatyName === 'DO NOT USE')) {
              vm.saveJustTreaty = 1;
              if (vm.treaty.inputStatus === 'Complete') {


                if (param === 3) {

                  vm.treaty.inputStatus = 'In Process';
                  vm.save();
                } else {
                  var IsSameOrNew = vm.lastUser === vm.treaty.lastUser;
                  if (vm.authority === 'Admin' && vm.treaty.treatyStatus.name === 'Pending' && !IsSameOrNew) {
                    ///
                    $mdDialog.show(confirmAdminCloseStatus).then(function () {
                      vm.treaty.treatyStatus = vm.activeStatus;
                      vm.save();
                    }, function () {
                      vm.save();
                    });

                    ////
                  } else {
                    vm.save();

                  }

                }
              } else {
                if (param === 1) {
                  $mdDialog.show(confirm).then(function () {
                    vm.treaty.inputStatus = 'Complete';
                    $mdDialog.show(confirmChange).then(function () {
                      vm.treaty.inputStatus = 'Complete';
                      vm.save();
                    });
                  }, function () {
                    vm.remainInProcess();
                  });
                } else {
                  vm.save();
                }
              }

            } else {
              $mdDialog.show(uniqueTreatyName);

            }
          });
        }
      }
    };
    vm.agreementTypeChange = function (treaty) {
      if (treaty.agreementType != null && treaty.agreementType.name === 'Retro – Internal') {
        //enabled Tracking
        treaty.disabledTracking = '1';
      } else {
        //Disable Tracking
        treaty.disabledTracking = '0';
      }
    };

    vm.changeON = function (type) {
      if (type.dayType === 'On') {
        type.numberOfDays = 0;
      } else {
        if (type.numberOfDays > 0) {

        } else {
          type.numberOfDays = null;
        }

      }
    };

    vm.numberOfDay = function (element) {
      if (element.numberOfDays === 0) {
        element.dayType = 'On';
      }
    };

    vm.currencyAdd = function () {
      vm.curencyCopy = angular.copy(vm.currency);
      vm.currencyOptionBroker.push(vm.curencyCopy);
      var currecyObjectAdd = {
        currency: vm.curencyCopy,
        creationUser: vm.lastUser,
        creationTs: moment(),
        lastUser: vm.lastUser,
        lastUpdate: moment()
      };

      vm.treaty.treatyCurrencies.push(currecyObjectAdd);
      vm.currency.code = '';
      vm.disableAlreadyAddedCurrencies();
    };

    vm.searchEK = function (enterpriseKey) {

      vm.activated = true;
      EnterpriseKey.get({
        id: vm.enterpriseKey
      }, onSaveEKSuccess, onSaveEKError);
      vm.mdmOn = 0;
      angular.forEach(vm.treaty.mdmAttributes, function (mdm, key) {

        if (mdm.enterpriseId == enterpriseKey) {
          vm.mdmOn = 1;
        }
      });
    };

    var onSaveEKSuccess = function (result) {
      if (result.id === undefined) {
        vm.activated = false;
        vm.ekData = null;
      } else {
        vm.activated = false;
        if (result != null) {
          vm.ekExist = true;
          result.id = null;
          vm.ekData = result;
          if (vm.treaty.deal.productLine.id == result.productLineId) {
            vm.treaty.productLineMatched = true;
          } else {
            vm.treaty.productLineMatched = false;
          }
        }
      }
    };
    var onSaveEKError = function (result) {
      vm.ekExist = false;
      vm.activated = false;
    };


    vm.removeCurrency = function (index) {
      var brokerCurrencyCode = '';
      var currencyCode = vm.treaty.treatyCurrencies[index].currency.code;
      var riskFeeCurrency = 0;
      var confirm = $mdDialog.alert()
        .textContent('You cannot delete this Currency because it is being used.')
        .disableParentScroll(false)
        .ok('Ok')
        .parent(angular.element(document.querySelector('#myModal')));
      TreatyTransactionSrv.currencyCount.query({
        treatyId: vm.treaty.id,
        currencyCode: vm.treaty.treatyCurrencies[index].currency.code
      }, function (result) {
        if (vm.treaty.id) {
          TreatyManagementCustomSrv.get({
            id: vm.treaty.id
          }, function (result) {
            angular.forEach(result.riskFees, function (riskFee, key) {
              if (riskFee.riskFeeCurrency != null && riskFee.riskFeeCurrency.code === currencyCode) {
                riskFeeCurrency = 1;
              }
            });
            if (riskFeeCurrency === 1) {
              $mdDialog.show(confirm).then(function () {});
            } else {
              if (vm.treaty.brokerCurrency != undefined && vm.treaty.brokerCurrency !== null && vm.treaty.brokerCurrency !== '') {
                brokerCurrencyCode = vm.treaty.brokerCurrency.code;
              }

              if (result.length > 0 || brokerCurrencyCode === currencyCode) {

                $mdDialog.show(confirm).then(function () {});
              } else {
                vm.treaty.treatyCurrencies.splice(index, 1);
              }

            }
          });
        }
      });
      vm.disableAlreadyAddedCurrencies();
    };

    vm.brokerSwitchChange = function () {
      if (vm.treaty.brokerFeeSwitchFlag === 'Yes') {
        if (vm.treaty.stmntDueAsmBroker === null || vm.treaty.stmntDueAsmBroker === undefined) {
          vm.treaty.stmntDueAsmBroker = {
            numberOfDays: null,
            dayType: null,
            period: null,
            comments: null,
            treatyRefComments: null
          };
        }
        vm.treaty.stmntDueAsmBroker.numberOfDays = angular.copy(vm.treaty.stmntDueAsm.numberOfDays);
        vm.treaty.stmntDueAsmBroker.dayType = 'Calendar Days After';
        vm.treaty.stmntDueAsmBroker.period = "Risk Fee Rec'd/Settlement Date";
        if (vm.treaty.deal.productSubtype !== undefined && vm.treaty.deal.productSubtype !== null) {
          if (vm.treaty.deal.productSubtype.name === 'Brokered') {
            vm.treaty.broker = 'FOR ALLOCATION PURPOSES ONLY';
          } else {
            // Elissa ask to remove this on friday sep 16 but maybe ask back
            //  if(vm.treaty.deal.productSubtype.name==='Structured') {
            //     vm.treaty.broker='N/A';
            //   }
          }
        }
      } else {
        vm.treaty.stmntDueAsmBroker.numberOfDays = null;
        vm.treaty.stmntDueAsmBroker.dayType = null;
        vm.treaty.stmntDueAsmBroker.period = null;
        vm.treaty.stmntDueAsmBroker.period = null;
        vm.treaty.stmntDueAsmBroker.comments = null;
        vm.treaty.broker = '';
        vm.treaty.brokerFeePercentage = '';
        vm.treaty.minBrokerFeeFlag = 'No';
        vm.treaty.brokerCurrency = null;
        vm.treaty.brokerFeeAmount = '';
      }
    };
    vm.minBrokerFeeChange = function () {
      if (vm.treaty.minBrokerFeeFlag === 'No') {
        vm.treaty.brokerCurrency = null;
        vm.treaty.brokerFeeAmount = '';
      }
    };
    vm.removeMdm = function () {
      var mdmWithTransaction = $mdDialog.alert()
        .title('You cannot delete this EK because it is associated with at least one Cash Transaction.')
        .textContent('To delete the EK, you must first delete the transaction to which the EK is linked. Afterward, you can come back to MDM Attributes and proceed to remove the EK.')
        .disableParentScroll(false)
        .ok('OK')
        .parent(angular.element(document.querySelector('#myModal')));

      var mdmConfirm = $mdDialog.confirm()
        //  .title('Are you ready to update Input Status on Treaty Basic Info from In Process to Complete?')
        .textContent('Are you sure you want to delete this EK?')
        .disableParentScroll(false)
        .ok('Remove EK')
        .cancel('Cancel')
        .parent(angular.element(document.querySelector('#myModal')));


      TreatySrv.transactionMdm.query({
        mdmId: vm.mdmAttribute[0].id
      }, function (result) {
        if (result.length === 0) {
          $mdDialog.show(mdmConfirm).then(function () {
            var yourSelect = document.getElementById("mdmId");
            vm.treaty.mdmAttributes.splice(yourSelect.selectedIndex, 1);
            vm.mdmAttribute = null;
          });
        } else {
          $mdDialog.show(mdmWithTransaction).then(function () {

          });

        }

      });


    };


    vm.showLegalDoc = function () {
      vm.index = document.getElementById("mySelect").selectedIndex;
      if (!isNaN(vm.treaty.mdmAttributes[vm.index].legalDoc) && (vm.treaty.mdmAttributes[vm.index].lastUser === null ||
          !vm.treaty.mdmAttributes[vm.index].lastUser.toLowerCase().includes('@rgare.net'))
        ) {
        vm.buttonEdit = false;
      } else {
        vm.buttonEdit = true;
      }
    };
    vm.editLegalDoc = function () {
      if (vm.editLegalDocId === true) {
        vm.editLegalDocId = false;
      } else {
        vm.editLegalDocId = true;
      }

    };

    vm.editCommentEk = function () {
      vm.index = document.getElementById("mySelect").selectedIndex;
      if (vm.editCommentEkFlag === true) {
        vm.editCommentEkFlag = false;
      } else {
        vm.editCommentEkFlag = true;
      }
    };

    $scope.$watch(
      "vm.mdmAttribute",
      function handleFooChange() {
        if (vm.mdmAttribute != null) {
          vm.currentEkId = vm.mdmAttribute[0].enterpriseId;
        }
      }
    );

    vm.searchCurrecyByCode = function (code) {
      var flag = 0;
      var currencyObject = {
        currency: null
      };
      angular.forEach(vm.currencyOptions, function (currency, key) {
        if (flag === 0 && code === currency.code) {
          currencyObject.currency = currency;
          flag = 1;
          angular.forEach(vm.treaty.treatyCurrencies, function (Treatycurrency, key) {
            if (currencyObject.currency.code === Treatycurrency.currency.code) {
              {
                flag = 3;
              }
            }
          });

        }

      });
      if (flag === 1) {
        vm.treaty.treatyCurrencies.push(currencyObject);
      }
    };
    vm.changeFlagDate = function (flag, element) {
      if (flag === 'No' && element !== null) {
        element = null;
      }
      if (vm.seriatimDueDateFlag === 'No') {
        vm.seriatimDueDate = null;
        vm.seriatimFreq = null;
        vm.changeFrequencySeriatim();
      }
      if (vm.seriatimDueDateFlag === 'Yes') {
        vm.seriatimFreq = vm.quarterFreq;
        if (element) {
          element.numberOfDays = null;
        }
        vm.changeFrequencySeriatim();
      }
      if (vm.yearEndRegulatoryDueDateFlag === 'Yes') {
        vm.yearEndRegulatoryFreq = vm.quarterFreq;

        if (element) {
          element.numberOfDays = null;
        }
        vm.periodRegulatoryOptions = ['Year End'];
        vm.yearEndRegulatoryDueDate = vm.yearEndRegulatoryDueDate ? vm.yearEndRegulatoryDueDate : {};
        vm.yearEndRegulatoryFreq = vm.periodOptions.find(function(item){ return item.name === "Annual"; });
        vm.yearEndRegulatoryDueDate.period = 'Year End';
        vm.yearEndRegulatoryDueDate.dayType = 'Calendar Days After';
      }
      if (vm.yearEndRegulatoryDueDateFlag === 'No') {
        vm.yearEndRegulatoryDueDate = null;
        vm.yearEndRegulatoryFreq = null;
      }
      if (vm.treaty.smntAsmMonthFlag === "Yes" && !vm.treaty.stmntDueAsmQuarterEx) {
        vm.treaty.stmntDueAsmQuarterEx = {
          period: "Quarter End"
        }
      } else if (vm.treaty.smntAsmMonthFlag === "No") {
        vm.treaty.stmntDueAsmQuarterEx = null;
      }
      if (vm.treaty.stmntCedMonthFlag === "Yes" && !vm.treaty.stmntDueCedQuarterEx) {
        vm.treaty.stmntDueCedQuarterEx = {
          period: "Quarter End"
        }
      } else if (vm.treaty.stmntCedMonthFlag === "No") {
        vm.treaty.stmntDueCedQuarterEx = null;
      }
    };

    if (vm.treaty.id != null) {
      TreatyTransactionSrv.treatyWithActualTransactions.query({
        treatyId: vm.treaty.id
      }).$promise.then(function (result) {
        if (result.length !== 0) {
          vm.flagActualTransaction = true;
        }

      })
    }
  }
})();
