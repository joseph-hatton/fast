(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('TreatyDetailController', TreatyDetailController);
  TreatyDetailController.$inject = ['$scope', '$mdDialog', '$filter', 'Principal', '$state', '$rootScope', '$stateParams', 'entity', 'Treaty',
    'TreatyManagementCustomSrv', 'TreatyManagement', 'TreatySrv', 'TreatyTrackingCustomSvr', 'InteractionsService', '$q', 'Intro'];

  function TreatyDetailController($scope, $mdDialog, $filter, Principal, $state, $rootScope, $stateParams, entity, Treaty,
                                  TreatyManagementCustomSrv, TreatyManagement, TreatySrv, TreatyTrackingCustomSvr, InteractionsService, $q, Intro) {
    var vm = this;
    vm.treaty = entity;
    vm.treatyCcpy = entity;
    vm.treatyId = $stateParams.id;
    vm.dealId = $stateParams.dealId;
    vm.managementCopy = entity;
    vm.settlementAssuming = null;
    vm.settlementPastAssuming = null;
    vm.settlementCeding = null;
    vm.spinner = false;
    vm.settlementPastCeding = null;
    vm.treatyManagement = {};



    Principal.identity().then(function (account) {
      vm.lastUser=account.login;
    });

    Intro.setupIntroSteps({id:172})
    .then(function(result){
        vm.introOptions = {
            steps: result
        };
    });

    vm.load = function (id) {
    };
    TreatyTrackingCustomSvr.count.get({treatyId: vm.treatyId}).$promise.then(function (result) {
      vm.trackingCount = result.amount;
    });

    var unsubscribe = $rootScope.$on('finreApp:treatyUpdate', function (event, result) {
      vm.treaty = result;

    });
    $scope.$on('$destroy', unsubscribe);
    vm.loadAll = function () {
      return TreatyManagementCustomSrv.get({id: $stateParams.id}).$promise
      .then(function (result) {
        angular.forEach(vm.treaty.mdmAttributes, function (mdm, key) {
          if(mdm.usStatRisk!==vm.treaty.deal.usStatRisk) {
            vm.EKusStat=true;
          }
          if(mdm.deal.toLowerCase()!==vm.treaty.deal.name.toLowerCase()) {
            vm.EKdealName=true;
          }
          if(mdm.productLineName.toLowerCase()!==vm.treaty.deal.productLine.name.toLowerCase()) {
            vm.EKproductLine=true;
          }
          if(!mdm.productStructure.name.toLowerCase().includes('other') && mdm.productStructure.name.toLowerCase()!==vm.treaty.productStructure.name.toLowerCase()
            || (mdm.productStructure.name.toLowerCase().includes('other') && !vm.treaty.productStructure.name.toLowerCase().includes(mdm.productStructure.name.toLowerCase()))
          ) {
            vm.EKproductStructure=true;
          }
        });
        if (result.id == null) {
          vm.treatyManagement.inputStatus = 'In Process';
          //  $state.go('treaty-management.newMngt',{id:$stateParams.id});
        } else {
          vm.treatyManagement = result;
          vm.treaty = result.treaty;
          vm.riskfeeArray = vm.treatyManagement.riskFees;
          vm.treatyManagement.summaryDate = $filter('date')(vm.treatyManagement.summaryDate, 'MMM dd, yyyy');
          vm.band = vm.riskfeeArray.length;
          vm.managementCopy = angular.copy(result);
        }
        vm.yearEndDate = YEAR_END_MONTH_OPTIONS.find(function(item){ return item.id === vm.treaty.deal.yearEndMonth; }).name + " " + vm.treaty.deal.yearEndDay;
      })
      .then(function () {
        vm.treatyMdmUpdating = angular.copy(vm.treaty);
        vm.treaty.effectiveDate = $filter('date')(vm.treaty.effectiveDate, 'MMM dd, yyyy');
        vm.treaty.terminationDate = $filter('date')(vm.treaty.terminationDate, 'MMM dd, yyyy');
        vm.updateMdmOn = true;
        angular.forEach(vm.treaty.mdmAttributes, function (mdm, key) {
          if (mdm.ekComment === null) {
            mdm.ekComment = '';
          }
          TreatySrv.enterpriseKeyVersion.query({
            id: mdm.enterpriseId,
            hbCreatedVID: mdm.versionDate,
            hbChangedVID: mdm.versionNumber
          }, function (result) {
            if (result.id != null) {
              vm.updateMdmOn = false;
            }

          });
        });
      });
    };

    vm.loadAll();

    vm.riskfeeArray = [];
    vm.band = 0;

    vm.copyBasicInfo = function (treaty, reason) {
      var treatyCopy = angular.copy(treaty);
      treatyCopy.id = null;
      treatyCopy.mdmAttributes = null;
      treatyCopy.mdmId = null;
      treatyCopy.notificationStatus = null;
      treatyCopy.treatyStatus = {description: 'Pending', id: '1', name: 'Pending', treatyStatusCode: 'PEND'};
      treatyCopy.effectiveDate = null;
      treatyCopy.reason = reason ? reason : null;
      treatyCopy.lastUser=vm.lastUser;
      treatyCopy.creationUser=vm.lastUser;

      resetId(treatyCopy.reportingDueYearEx);
      resetId(treatyCopy.reportingDueDate);
      resetId(treatyCopy.reportingDueQuarterEx);
      resetId(treatyCopy.stmntDueAsm);
      resetId(treatyCopy.stmntDueAsmBroker);
      resetId(treatyCopy.stmntDueAsmGracePeriod);
      resetId(treatyCopy.stmntDueAsmQuarterEx);
      resetId(treatyCopy.stmntDueCed);
      resetId(treatyCopy.stmntDueCedGracePeriod);
      resetId(treatyCopy.stmntDueCedQuarterEx);

      treatyCopy.seriatimTypeTreaties = null;
      treatyCopy.yearEndRegulatoryList = null;
      treatyCopy.treatyTrackings = null;

      angular.forEach(treatyCopy.treatyCurrencies, function (currency) {
        currency.id = null;
      });

      treatyCopy.treatyName = null;
      treatyCopy.copyTreatyMsg = treaty.deal.name + ' - ' + treaty.agreementType.name + ' - ' + treaty.productStructure.name + ' - ' + treaty.treatyName;
      treatyCopy.inputStatus = 'In Process';

      treatyCopy.deal = vm.treaty.deal;

      treatyCopy.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
      return Treaty.save(treatyCopy).$promise;

      function resetId(item) {
        if (item != null) {
          item.id = null;
        }
      }
    };

    vm.copyTreatyManagement = function (oldTreatyManagement, newTreaty) {
      var treatyManagement = oldTreatyManagement;
      treatyManagement.id = null;
      angular.forEach(treatyManagement.riskFees, function (riskFee) {
        riskFee.id = null;
      });
      treatyManagement.treaty = newTreaty;
      treatyManagement.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
      return TreatyManagement.save(treatyManagement).$promise;
    };

    vm.copyTreaty = function (treaty) {
      var content = treaty.deal.name + ' - ' + treaty.agreementType.name + ' - ' + treaty.productStructure.name + ' - ' + (treaty.treatyName ? treaty.treatyName : '');
      var confirm = InteractionsService.confirmDialogWithTitle('Are you sure you want to create a new treaty by copying Basic Info and Management info from this treaty?', content, '#myTreatyList');
      InteractionsService.showThenDialog(confirm, function () {
        var newTreaty;
        vm.copyBasicInfo(treaty).then(function (result) {
          newTreaty = result;
          return TreatyManagementCustomSrv.get({id: treaty.id}).$promise;
        })
          .then(function (result) {
            return vm.copyTreatyManagement(result, newTreaty);
          })
          .then(function () {
            var confirm = InteractionsService.alertDialogWithTitle('You have successfully created a new treaty by copying.', content, '#myTreatyList');
            return InteractionsService.showThenDialog(confirm, function () {
              $state.go('treaty-detail.edit-detail', {
                id: newTreaty.id,
                dealId: $stateParams.dealId,
                hash: 'generalInfo'
              });
            });
          })
          .catch(function () {
            InteractionsService.showThenDialog(InteractionsService.alertParentScrollFalseDialog('Sorry, an error occurred. Try again.'));
          });
      });
    };

    vm.updateMdmAttributes = function () {
      vm.spinner = true;
      vm.updateMdmFlag = 0;
      vm.flagPromise = 0;
      var queryArray = [];
      angular.forEach(vm.treatyMdmUpdating.mdmAttributes, function (mdm, key) {
        console.log(vm.updateMdmFlag, vm.treatyMdmUpdating.mdmAttributes.length);
        var params = {id: mdm.enterpriseId, hbCreatedVID: mdm.versionDate, hbChangedVID: mdm.versionNumber};
        var queryPromise = TreatySrv.enterpriseKeyVersion.query(params).$promise
          .then(processResults.bind(this, mdm, key));
        queryArray.push(queryPromise);
      });

      $q.all(queryArray)
        .then(function () {
          Treaty.update(vm.treatyMdmUpdating, onUpdateMdmSucces, onError);
        });

      function processResults(mdm, key, result) {
        vm.updateMdmFlag++;
        if (result.id) {
          result.id = mdm.id;
          result.ekComment = mdm.ekComment ? mdm.ekComment : '';
          result.usStatRisk = mdm.usStatRisk;
         if(result.legalDoc.toLowerCase().includes('n/a') ||
           result.legalDoc.toLowerCase().includes('na')||
           result.legalDoc.toLowerCase().includes('nondet')){
           result.legalDoc=mdm.legalDoc;
         }

          if (result.effectiveStatusCd === 'A') {
            result.effectiveStatusCd = 'Active';
          }
          else {
            result.effectiveStatusCd = 'Inactive';
          }
          vm.treatyMdmUpdating.mdmAttributes[key] = result;
        }

      }
    };

    var onError = function () {
      console.log('error updating mdm');
    };
    var onUpdateMdmSucces = function () {
      vm.updateMdmOn = true;
      vm.spinner = false;
      console.log('OK');

    };
  }
})();
