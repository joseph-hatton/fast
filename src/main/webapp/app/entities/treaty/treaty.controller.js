(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('TreatyController', TreatyController);

  TreatyController.$inject = ['$scope','$stateParams','$mdDialog', '$state', 'Treaty', 'TreatySearch',
    'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants',
    'TreatySrv','TreatyManagementCustomSrv','TreatyManagement','Deal','TreatyTrackingCustomSvr','Principal', 'InteractionsService', 'Intro' ];
  function TreatyController($scope,$stateParams, $mdDialog, $state, Treaty, TreatySearch,
                            ParseLinks, AlertService, pagingParams, paginationConstants,
                              TreatySrv,TreatyManagementCustomSrv,TreatyManagement,Deal,TreatyTrackingCustomSvr,Principal, InteractionsService, Intro) {
    var vm = this;
    vm.loadAll = loadAll;
    vm.loadPage = loadPage;
    vm.predicate = pagingParams.predicate;
    vm.reverse = pagingParams.ascending;
    vm.transition = transition;
    vm.clear = clear;
    vm.search = search;
    vm.dealId = $stateParams.dealId;
    vm.searchQuery = pagingParams.search;
    vm.currentSearch = pagingParams.search;
    vm.treatyForReplicate=[];
    vm.countReplication=0;
    vm.replicateFlag=false;
    vm.itemsPerPage = paginationConstants.itemsPerPage;

    Intro.setupIntroSteps({id:174})
    .then(function(result){
        vm.introOptions = {
            steps: result
        };
    });

    Principal.identity().then(function (account) {
      angular.forEach(account.authorities, function (authoritie) {
        if (authoritie === "ROLE_APP_FINRE_OPS_ADMIN") {
          vm.authority = 'Admin';
        } else {
          if (authoritie === "ROLE_APP_FINRE_VIEW_ONLY") {
            vm.authority = 'ViewOnly'
          }else{
            if (vm.authority !== 'Admin' && vm.authority !== 'ViewOnly') {
              vm.authority = 'User';
            }
          }
        }
      });
      vm.lastUser=account.login;
    });

    TreatySrv.nopages.query(function (result) {
      vm.dealsList=[];
      angular.forEach (result, function(deal,key){
        if(deal.dealStatus !='Terminated'){
          vm.dealsList.push(deal);
        }
      })
    });

    vm.changeReplicateFlag= function(){
      if(vm.deal.dealStatus === "Terminated"){
        vm.replicateFlag = false;
      } else {
        vm.treatyList=[];
        vm.dealTreaty=null;
        vm.treatyForReplicate=[];
      }
    };
    vm.findTreaties= function(id){
      TreatySrv.deals.query({
        id:vm.dealTreaty.id},function(result){
        vm.treatyList=result;
        angular.forEach(vm.treatyList, function(item){
          item.oldTreatyName = item.treatyName;
        });
      });
    };
    vm.replicate= function (){
      vm.countReplication=vm.treatyForReplicate.length;
      var confirm = InteractionsService.confirmBasicParentNoScrollDialog(
        'Are you sure you want replicate ' + vm.treatyForReplicate.length + ' treat' + (vm.treatyForReplicate.length > 1 ? 'ies' : 'y') + ' from ' + vm.dealTreaty.name + ' to  ' + vm.deal.name + ' ?', '#myModal');

      InteractionsService.showThenDialog(confirm, function () {
        var promises = [];
        var newTreaty;
        angular.forEach (vm.treatyForReplicate, function(treaty){
          var nestedCopies = vm.copyBasicInfo(treaty, 'Replicated')
          .then(function (result) {
            newTreaty = result;
            return TreatyManagementCustomSrv.get({id: treaty.id}).$promise;
          })
          .then(function (oldTreatyManagement) {
            return vm.copyTreatyManagement(oldTreatyManagement, newTreaty);
          });
          promises.push(nestedCopies);
        });

        Promise.all(promises).then(function () {
          var confirm = InteractionsService.alertDialog('You have successfully replicated the treaties.', '#myTreatyList');
          InteractionsService.showThenDialog(confirm, function () {
            clear();
            loadAll();
            vm.replicateFlag=false;
            vm.treatyList=[];
            vm.dealTreaty=null;
            vm.treatyForReplicate=[];
          });
        });
      });
    };

    if ($stateParams.dealId !== undefined && $stateParams.dealId !== null) {
      loadAll();

      Deal.get({id: $stateParams.dealId}, function (result) {
        vm.deal = result;
      });
    }

    function loadAll() {
      if (pagingParams.search) {
        TreatySearch.query({
          id: $stateParams.dealId,
          query: pagingParams.search,
          page: pagingParams.page - 1,
          size: paginationConstants.itemsPerPage,
          sort: sort()
        }, onSuccess, onError);
      }else {
        TreatySrv.deals.query({
          id: $stateParams.dealId,
          page: pagingParams.page - 1,
          size: paginationConstants.itemsPerPage,
          sort: sort()
        }, function (result, headers) {
          vm.links = ParseLinks.parse(headers('link'));
          vm.totalItems = headers('X-Total-Count');
          vm.queryCount = vm.totalItems;
          vm.treaties = result;
          angular.forEach (vm.treaties, function(treaty,key){
            treaty = hookToSharePointDocs(treaty);
            if(treaty.isActive === 0) {
              vm.treaties.splice(key,1);
              vm.totalItems = vm.totalItems -1;
            }

            TreatyTrackingCustomSvr.tracking.query({treatyId:treaty.id},function(trackingResults){
              treaty.treatyTrackings = [];
              angular.forEach(trackingResults, function(tracking,key){
                if(tracking.trackingStatus==='Open'){
                tracking.quarter = calculateQuarters(tracking.reportingMonthYear);
                treaty.treatyTrackings.push(tracking);
                }
              });
              if(treaty.agreementType.name === 'Retro – Internal' || (treaty.treatyName != null && treaty.treatyName.includes("DO NOT USE"))) {
                treaty.disabledTracking = 1;
                Treaty.update(treaty);
              } else if(treaty.treatyTrackings.length > 0) {
                treaty.disabledTracking = 0;
                Treaty.update(treaty);
              }
            });
          })
        }, onError);
        vm.page = pagingParams.page;

      }
      function hookToSharePointDocs(treaty) {
        var legalDocs = getLegalDocs(treaty);
        treaty.docsUrl = legalDocs;
        return treaty;
      }

      function getLegalDocs(treaty){
        var legalDocs = [];
        if (treaty === undefined ||
          treaty.mdmAttributes === null ||
          treaty.mdmAttributes === undefined)  return legalDocs;

        var url = 'http://image.rgare.com/sites/USdivtreaties/default.aspx?uid=1&rid=1&BaseDocumentNumbertext=';

        angular.forEach (treaty.mdmAttributes, function(attribute,key) {
          if (attribute.legalDoc !== null &&
            attribute.legalDoc !== undefined &&
            attribute.legalDoc !== 'N/A' &&
            attribute.legalDoc !== 'NA' &&
            !attribute.legalDoc.toLowerCase().includes('n/a')
            &&
            !attribute.legalDoc.toLowerCase().includes('na')
          ){
            legalDocs.push(url + attribute.legalDoc);
          }
        });
        return legalDocs;
      }

      function sort() {
        var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
        if (vm.predicate !== 'id') {
          result.push('id');
        } else {
          result = ['treatyName', 'productStructure.name', 'agreementType.name'];
        }
        return result;

      }
    }

    function onSuccess(data, headers) {
      vm.links = ParseLinks.parse(headers('link'));
      vm.totalItems = headers('X-Total-Count');
      vm.treaties = data;
      angular.forEach (vm.treaties, function(treaty,key){
        if(treaty.isActive === 0) {
          vm.treaties.splice(key,1);
          vm.totalItems = vm.totalItems -1;
        }

        TreatyTrackingCustomSvr.tracking.query({treatyId:treaty.id},function(trackingResults){
          vm.tracking = [];
          if(vm.tracking.$resolved) {
            angular.forEach(trackingResults, function (track, key) {
              if(track.trackingStatus==='Open'){
              track.quarter = calculateQuarters(track.reportingMonthYear);
              treaty.treatyTrackings.push(track);
              }
            });
          }
        });
      });
      vm.queryCount = vm.totalItems;
      vm.page = pagingParams.page;
    }

    function onError(error) {
      AlertService.error(error.data.message);
    }

    function loadPage(page) {
      vm.page = page;
      vm.transition();
    }

    function transition() {
      $state.transitionTo($state.$current, {
        dealId: $stateParams.dealId,
        page: vm.page,
        sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
        search: vm.currentSearch
      });
    }

    function search(searchQuery) {
      if (!searchQuery) {
        return vm.clear();
      }
      vm.dealId= $stateParams.dealId;
      vm.links = null;
      vm.page = 1;
      vm.predicate = '_score';
      vm.reverse = false;
      vm.currentSearch = searchQuery;
      vm.transition();
    }

    function clear() {
      vm.dealId= $stateParams.dealId;
      vm.links = null;
      vm.page = 1;
      vm.predicate = 'id';
      vm.reverse = true;
      vm.currentSearch = null;
      vm.transition();
    }

    vm.toggleTracking =  function (treaty){
      if((treaty.treatyName != null && treaty.treatyName.includes('DO NOT USE')) || (treaty.agreementType.name === 'Retro – Internal')){ //Tracking is ALWAYS disabled for "Retro-Internal" and "DO NOT USE"
        treaty.disabledTracking = 1;
      } else {
        if(treaty.treatyTrackings.length > 0){ //Invalid Tracking Records Exist
          treaty.disabledTracking = 0;
        } else { //No Tracking Records, Good to toggle Tracking
          if(treaty.disabledTracking === 1){
            var dialog = InteractionsService.confirmBasicParentNoScrollDialog((treaty.treatyName != null ? treaty.treatyName : 'This') + ' is a valid treaty (and is not a Retro – Internal). Are you sure you want to disable tracking?', '#myModal');
            InteractionsService.showThenDialog(dialog,
              function () { Treaty.update(treaty, onSuccess, onError); },
              function () { treaty.disabledTracking = 0; });
          } else
            Treaty.update(treaty, onSuccess, onError);
        }
      }
    };

    vm.copyBasicInfo = function (treaty, reason) {
      var treatyCopy = angular.copy(treaty);
      treatyCopy.id = null;
      treatyCopy.mdmAttributes = null;
      treatyCopy.mdmId = null;
      treatyCopy.notificationStatus = null;
      treatyCopy.creationTs =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
      treatyCopy.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
      treatyCopy.lastUser=vm.lastUser;
      treatyCopy.creationUser=vm.lastUser;
      treatyCopy.treatyStatus = {description: 'Pending', id: '1', name: 'Pending', treatyStatusCode: 'PEND'};
      treatyCopy.effectiveDate = null;
      treatyCopy.reason = reason ? reason : null;

      resetId(treatyCopy.reportingDueYearEx);
      resetId(treatyCopy.reportingDueDate);
      resetId(treatyCopy.reportingDueQuarterEx);
      resetId(treatyCopy.stmntDueAsm);
      resetId(treatyCopy.stmntDueAsmBroker);
      resetId(treatyCopy.stmntDueAsmGracePeriod);
      resetId(treatyCopy.stmntDueAsmQuarterEx);
      resetId(treatyCopy.stmntDueCed);
      resetId(treatyCopy.stmntDueCedGracePeriod);
      resetId(treatyCopy.stmntDueCedQuarterEx);

      treatyCopy.seriatimTypeTreaties = null;
      treatyCopy.yearEndRegulatoryList = null;
      treatyCopy.treatyTrackings = null;

      angular.forEach(treatyCopy.treatyCurrencies, function (currency) {
        currency.id = null;
      });

      treatyCopy.treatyName = null;
      treatyCopy.copyTreatyMsg = treaty.deal.name + ' - ' + treaty.agreementType.name + ' - ' + treaty.productStructure.name + ' - ' + treaty.treatyName;
      treatyCopy.inputStatus = 'In Process';

      treatyCopy.deal=vm.deal;

      treatyCopy.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
      return Treaty.save(treatyCopy).$promise;

      function resetId(item) {
        if (item != null) {
          item.id = null;
        }
      }
    };

    vm.copyTreatyManagement = function (oldTreatyManagement, newTreaty) {
      var treatyManagement = oldTreatyManagement;
      treatyManagement.id = null;
      angular.forEach(treatyManagement.riskFees, function (riskFee) {
        riskFee.id = null;
      });
      treatyManagement.treaty = newTreaty;
      treatyManagement.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
      return TreatyManagement.save(treatyManagement).$promise;
    };

    vm.copyTreaty = function (treaty) {
      var content = treaty.deal.name + ' - ' + treaty.agreementType.name + ' - ' + treaty.productStructure.name + ' - ' + (treaty.treatyName ? treaty.treatyName : '');
      var confirm = InteractionsService.confirmDialogWithTitle('Are you sure you want to create a new treaty by copying Basic Info and Management info from this treaty?', content, '#myTreatyList');
      InteractionsService.showThenDialog(confirm, function () {
        var newTreaty;
        vm.copyBasicInfo(treaty).then(function (result) {
          newTreaty = result;
          return TreatyManagementCustomSrv.get({id: treaty.id}).$promise;
        })
        .then(function (result) {
          return vm.copyTreatyManagement(result, newTreaty);
        })
        .then(function () {
          var confirm = InteractionsService.alertDialogWithTitle('You have successfully created a new treaty by copying.', content, '#myTreatyList');
          return InteractionsService.showThenDialog(confirm, function () {
            $state.go('treaty-detail.edit-detail', {
              id: newTreaty.id,
              dealId: $stateParams.dealId,
              hash: 'generalInfo'
            });
          });
        })
        .catch(function () {
          InteractionsService.showThenDialog(InteractionsService.alertParentScrollFalseDialog('Sorry, an error occurred. Try again.'));
        });
      });
    };

    vm.deleteTreaty = function(id){
      if(vm.deal.dealStatus !== "Terminated"){
        var info = {
          id:id,
          dealId:vm.dealId
        };
        $state.go("treaty-deal.delete", info);
      }
    };
}})();
