(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('TreatyAmendments', TreatyAmendments);
  
  TreatyAmendments.$inject = ['$resource', 'DateUtils'];
  
  function TreatyAmendments($resource, DateUtils) {
    var resourceUrl = 'api/treaty-amendments/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          data.effectiveDate = DateUtils.convertLocalDateFromServer(data.effectiveDate);
          return data;
        }
      },
      'update': {
        method: 'PUT',
        transformRequest: function (data) {
          data.effectiveDate = DateUtils.convertLocalDateToServer(data.effectiveDate);
          return angular.toJson(data);
        }
      },
      'save': {
        method: 'POST',
        transformRequest: function (data) {
          data.effectiveDate = DateUtils.convertLocalDateToServer(data.effectiveDate);
          return angular.toJson(data);
        }
      }
    });
  }
})();
