(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .factory('TreatyAmendmentsSearch', TreatyAmendmentsSearch);
  
  TreatyAmendmentsSearch.$inject = ['$resource'];
  
  function TreatyAmendmentsSearch($resource) {
    var resourceUrl = 'api/_search/treaty-amendments/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true}
    });
  }
})();
