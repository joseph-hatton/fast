(function () {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('treaty-amendments', {
        parent: 'entity',
        url: '/treaty-amendments?page&sort&search',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treatyAmendments.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty-amendments/treaty-amendments.html',
            controller: 'TreatyAmendmentsController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,asc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
            return {
              page: PaginationUtil.parsePage($stateParams.page),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate($stateParams.sort),
              ascending: PaginationUtil.parseAscending($stateParams.sort),
              search: $stateParams.search
            };
          }],
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treatyAmendments');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }]
        }
      })
      .state('treaty-amendments-all', {
        parent: 'entity',
        url: '/treaty-amendments/treaties/{treatyId}/{dealId}?page&sort&search',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treatyAmendments.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty-amendments/treaty-amendments.html',
            controller: 'TreatyAmendmentsController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,asc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
            return {
              page: PaginationUtil.parsePage($stateParams.page),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate($stateParams.sort),
              ascending: PaginationUtil.parseAscending($stateParams.sort),
              search: $stateParams.search
            };
          }],
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treatyAmendments');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'TreatyAmendmentsAll', function ($stateParams, TreatyAmendmentsAll) {
            return TreatyAmendmentsAll.query({id: $stateParams.treatyId});
          }]

        }
      })
      .state('treaty-amendments-detail', {
        parent: 'entity',
        url: '/treaty-amendments/detail/{dealId}/{treatyId}/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.treatyAmendments.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/treaty-amendments/treaty-amendments-detail.html',
            controller: 'TreatyAmendmentsDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('treatyAmendments');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'TreatyAmendments', function ($stateParams, TreatyAmendments) {
           return TreatyAmendments.get({id: $stateParams.id}).$promise;
          }]
        }
      })
      .state('treaty-amendments-all.new', {
        parent: 'treaty-amendments-all',
        url: '/new/{dealId}/{treatyId}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-amendments/treaty-amendments-dialog.html',
            controller: 'TreatyAmendmentsDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  amendmentName: null,
                  effectiveDate: null,
                  comments: null,
                  reason: null,
                  activeStatus: null,
                  id: null
                };
              }
            }
          }).result.then(function () {
          $state.go('treaty-amendments-all', {dealId: $stateParams.dealId,treatyId: $stateParams.treatyId}, {reload: true});
          }, function () {
            $state.go('treaty-amendments-all', {dealId: $stateParams.dealId,treatyId: $stateParams.treatyId}, {reload: true});
          });
        }]
      })
      .state('treaty-amendments-all.edit', {
        parent: 'treaty-amendments-all',
        url: '/edit/{dealId}/{treatyId}/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-amendments/treaty-amendments-dialog.html',
            controller: 'TreatyAmendmentsDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['TreatyAmendments', function (TreatyAmendments) {
                return TreatyAmendments.get({id: $stateParams.id}).$promise;
              }]
            }
          }).result.then(function () {
            $state.go('treaty-amendments-all', {treatyId:$stateParams.treatyId,dealId:$stateParams.dealId}, {reload: true});
          }, function () {
            $state.go('treaty-amendments-all',{treatyId:$stateParams.treatyId,dealId:$stateParams.dealId}, {reload: true});
          });
        }]
      })
      .state('treaty-amendments-detail.edit', {
        parent: 'treaty-amendments',
        url: '/edit/detail/{dealId}/{treatyId}/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-amendments/treaty-amendments-dialog.html',
            controller: 'TreatyAmendmentsDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['TreatyAmendments', function (TreatyAmendments) {
                return TreatyAmendments.get({id: $stateParams.id}).$promise;
              }]
            }
          }).result.then(function () {
            $state.go('treaty-amendments-detail',{treatyId:$stateParams.treatyId,dealId:$stateParams.dealId,id:$stateParams.id}, {reload: true});
          }, function () {
            $state.go('treaty-amendments-detail', {treatyId:$stateParams.treatyId,dealId:$stateParams.dealId,id:$stateParams.id}, {reload: true});
          });
        }]
      })
      .state('treaty-amendments-all.delete', {
        parent: 'treaty-amendments-all',
        url: '/delete/{dealId}/{treatyId}/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/treaty-amendments/treaty-amendments-delete-dialog.html',
            controller: 'TreatyAmendmentsDeleteController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['TreatyAmendments', function (TreatyAmendments) {
                return TreatyAmendments.get({id: $stateParams.id}.$promise);
              }]
            }
          }).result.then(function () {
            $state.go('treaty-amendments-all', {treatyId:$stateParams.treatyId,dealId:$stateParams.dealId}, {reload: true});
          }, function () {
            $state.go('treaty-amendments-all', {treatyId:$stateParams.treatyId,dealId:$stateParams.dealId}, {reload: true});
          });
        }]
      });
  }

})();
