(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('TreatyAmendmentsNoPage', TreatyAmendmentsNoPage);

  TreatyAmendmentsNoPage.$inject = ['$resource', 'DateUtils'];
  ///treaty-amendments/nopage/all
  function TreatyAmendmentsNoPage($resource, DateUtils) {
    var resourceUrl = 'api/treaty-amendments/nopage/all';

    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          data.reportingDueDate = DateUtils.convertLocalDateFromServer(data.reportingDueDate);
          data.paymentDueDate = DateUtils.convertLocalDateFromServer(data.paymentDueDate);
          data.summaryDate = DateUtils.convertLocalDateFromServer(data.summaryDate);
          return data;
        }
      }
    });
  }
})();
