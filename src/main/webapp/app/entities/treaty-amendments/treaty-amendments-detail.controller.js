(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('TreatyAmendmentsDetailController', TreatyAmendmentsDetailController);

  TreatyAmendmentsDetailController.$inject = ['$scope','$filter', '$rootScope', '$stateParams', 'entity'];

  function TreatyAmendmentsDetailController($scope, $filter, $rootScope, $stateParams, entity) {
    var vm = this;
    vm.treatyAmendments = entity;
    vm.treatyId = $stateParams.treatyId;
    vm.dealId = $stateParams.dealId;

    vm.treatyAmendments.effectiveDate = $filter('date')(vm.treatyAmendments.effectiveDate,'MMM dd, yyyy');

    var unsubscribe = $rootScope.$on('finreApp:treatyAmendmentsUpdate', function (event, result) {
      vm.treatyAmendments = result;
    });

    $scope.$on('$destroy', unsubscribe);

  }
})();
