(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('TreatyAmendmentsController', TreatyAmendmentsController);

  TreatyAmendmentsController.$inject = ['$stateParams', '$state', '$location',
    'TreatyAmendmentsAll', 'TreatyAmendmentsSearch', 'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants', 'Treaty', 'Principal', 'TreatyTrackingCustomSvr', '$scope', '$filter', 'Intro'];

  function TreatyAmendmentsController($stateParams, $state, $location, TreatyAmendmentsAll,
                                      TreatyAmendmentsSearch, ParseLinks, AlertService, pagingParams, paginationConstants, Treaty, Principal, TreatyTrackingCustomSvr, $scope, $filter, Intro) {
    var vm = this;
    vm.predicate = pagingParams.predicate;
    vm.reverse = pagingParams.ascending;
    vm.treatyAmendments = null;
    vm.searchQuery = pagingParams.search;
    vm.currentSearch = pagingParams.search;
    vm.treatyId = $stateParams.treatyId;
    vm.dealId = $stateParams.dealId;
    vm.itemsPerPage = paginationConstants.itemsPerPage;

    Intro.setupIntroSteps({id:171})
    .then(function(result){
        vm.introOptions = {
            steps: result
        };  
    });

    $scope.amendmentNumberSort = function () {
      vm.amendmentSort = vm.amendmentSort == null ? vm.locationSearch.amendmentSort : !vm.amendmentSort;

      if(vm.locationSearch.sort != null || vm.locationSearch.amendmentSort == null) {
        $location.search('sort', null);
        $location.search('amendmentSort', true);
      }
      vm.predicate = null;
      vm.reverse = null;
      vm.treatyAmendments = $filter('orderBy')(vm.treatyAmendments, 'amendmentName', !vm.amendmentSort, looseComparator);
      function looseComparator(a, b) {
        return a.value.localeCompare(b.value, undefined, {numeric: true, sensitivity: 'base'});
      }
    };

    vm.loadAll = function () {
      vm.locationSearch = $location.search();

      // if (pagingParams.search) {
      //   TreatyAmendmentsSearch.query({
      //     query: pagingParams.search,
      //     page: pagingParams.page - 1,
      //     size: paginationConstants.itemsPerPage,
      //     sort: sort()
      //   }, onSuccess, onError);
      // } else
      if (vm.treatyId !== undefined) {
        if(vm.predicate === 'id'){
          vm.predicate = 'id';
          vm.reverse = false;
        }
        TreatyAmendmentsAll.query({
          id: $stateParams.treatyId,
          sort: sort()
        }, onSuccess, onError);

      }

      TreatyTrackingCustomSvr.count.get({treatyId: vm.treatyId}).$promise.then(function (result) {
        vm.trackingCount = result.amount;
      });

      Treaty.get({id: $stateParams.treatyId}, function (result) {
        vm.treaty = result;
      });

      $scope.$on('$locationChangeSuccess', callAmendmentSort);

      function callAmendmentSort() {
        if(vm.locationSearch.amendmentSort != null)
          $scope.amendmentNumberSort();
      }

      function sort() {
        var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
        if (vm.predicate !== 'id') {
          result.push('id');
        }
        return result;
      }

      function onSuccess(data, headers) {
        // vm.links = ParseLinks.parse(headers('link'));
        // vm.totalItems = headers('X-Total-Count');
        // vm.queryCount = vm.totalItems;
        vm.treatyAmendments = data;
        callAmendmentSort();
        // vm.page = pagingParams.page;
      }

      function onError(error) {
        AlertService.error(error.data.message);
      }
    };

    vm.loadAll();

    vm.transition = function () {
      $state.transitionTo($state.$current, {
        treatyId: $stateParams.treatyId,
        dealId: $stateParams.dealId,
        // page: vm.page,
        sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')
        // search: vm.currentSearch
      });
    };

    vm.loadPage = function (page) {
      // vm.page = page;
      vm.transition();
    };

    vm.clear = function () {
      // vm.links = null;
      // vm.page = 1;
      vm.predicate = 'id';
      vm.reverse = true;
      // vm.currentSearch = null;
      vm.transition();
    };

    vm.search = function (searchQuery) {
      if (!searchQuery) {
        return vm.clear();
      }
      vm.links = null;
      vm.page = 1;
      vm.predicate = '_score';
      vm.reverse = false;
      vm.currentSearch = searchQuery;
      vm.transition();
    };

    Principal.identity().then(function (account) {
      angular.forEach(account.authorities, function (authoritie) {
        if (authoritie === "ROLE_APP_FINRE_OPS_ADMIN")
          vm.actualUser = 'Admin';
        else if (vm.actualUser !== 'Admin')
          vm.actualUser = 'User';
      });
      vm.lastUser = account.login;
    });
    vm.deleteAmendment = function(id){
      if(vm.treaty.treatyStatus.id !== 4 && vm.actualUser === 'Admin'){
        var info = {
          dealId: vm.dealId,
          treatyId: vm.treatyId,
          id: id
        };
        $state.go("treaty-amendments-all.delete", info);
      }
    };
  }
})();
