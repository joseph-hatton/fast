(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('TreatyAmendmentsDeleteController', TreatyAmendmentsDeleteController);

  TreatyAmendmentsDeleteController.$inject = ['$uibModalInstance','NotificationStatus','$stateParams', 'entity', 'TreatyAmendments','TreatyAmendmentsAll','Treaty'];

  function TreatyAmendmentsDeleteController($uibModalInstance,NotificationStatus, $stateParams, entity, TreatyAmendments,TreatyAmendmentsAll,Treaty) {
    var vm = this;
    vm.treatyAmendments = entity;
    vm.treatyId=$stateParams.treatyId;
    vm.treatyAmendments.reason='';
    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };

    NotificationStatus.query(function(result) {
      angular.forEach(result, function (notification, key) {
        if(notification.name==='Modified'){
        vm.ModifieddNotify=notification;
        }
      });

    });

    var onError= function (){
      console.log('error');
    };

    vm.confirmDelete = function (id) {
     vm.treatyAmendments.isActive = 0;
      vm.treatyAmendments.notificationStatus= vm.ModifieddNotify;
      TreatyAmendments.update(vm.treatyAmendments).$promise.then(onSuccess,onError);
    };
//Change Amended to Active
    function onSuccess() {
          TreatyAmendmentsAll.query({id: vm.treatyId}).$promise.then(function (result) {
          vm.treatyAmendmentAfterUpdate = result;
          if(vm.treatyAmendmentAfterUpdate.length===0 && vm.treatyAmendments.treaty.treatyStatus.name==='Amended')  {
                vm.treatyAmendments.treaty.treatyStatus = {id: 2, name: "Active", description: "Active", treatyStatusCode: "ACTIV"};
                Treaty.update(vm.treatyAmendments.treaty, function () {
                  $uibModalInstance.close(true);
                });
          }else{
            $uibModalInstance.close(true);
          }
        });
    }
  }
})();
