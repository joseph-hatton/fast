(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('TreatyAmendmentsDialogController', TreatyAmendmentsDialogController);
  TreatyAmendmentsDialogController.$inject = ['$scope','$mdDialog', '$stateParams', '$uibModalInstance', 'entity',
    'TreatyAmendments','Principal', 'Treaty','NotificationStatus', 'InteractionsService'];
  function TreatyAmendmentsDialogController($scope,$mdDialog, $stateParams, $uibModalInstance, entity,
                                            TreatyAmendments,Principal, Treaty,NotificationStatus, InteractionsService) {

    var vm = this;
    vm.treatyAmendments = entity;
    vm.treatyId = $stateParams.treatyId;
    vm.treatyAmendments.lastUpdate= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
    vm.dealId = $stateParams.dealId;


    Principal.identity().then(function (account) {
      var IsSameOrNew = account.login === vm.treatyAmendments.lastUser;
      angular.forEach(account.authorities, function (authoritie) {
        if(vm.treatyAmendments.id && (authoritie === "ROLE_APP_FINRE_OPS_ADMIN" && !IsSameOrNew)) {
           vm.actualUser = 'Admin';
        }else{
          if(vm.actualUser !== 'Admin'){
            vm.actualUser = 'User';
          }
        }
      });
      vm.lastUser=account.login;
    });


    NotificationStatus.query(function(result) {
      vm.notificationStatuses = result;
      angular.forEach(result, function (notification, key) {
        if(notification.name==='Notified'){
          vm.treatyAmendments.notificationStatus=notification;
          vm.notificationNotified=notification;
        }
        if(notification.name==='Confirmed')
          vm.notificationConfirmed=notification;
      })
    });

    if($stateParams.id == null)
      Treaty.get({id: $stateParams.treatyId}, function (result) { vm.treatyAmendments.treaty = result; });
    else
      TreatyAmendments.get({id: $stateParams.id}, function (result) { vm.treatyAmendments = result; });

    vm.load = function (id) { };

    var onSaveSuccess = function (result) {
      $scope.$emit('finreApp:treatyAmendmentsUpdate', result);

      if (vm.treatyAmendments.treaty.treatyStatus.name === 'Active') {
        vm.treatyAmendments.treaty.treatyStatus = {
          id: 3,
          name: "Amended",
          description: "Amended",
          treatyStatusCode: "AMEND"
        };
        vm.treatyAmendments.lastUpdate =  moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        Treaty.update(vm.treatyAmendments.treaty).$promise.then( function(){ $uibModalInstance.close(result)});
      }else
      {
        $uibModalInstance.close(result);
      }
      vm.isSaving = false;
    };

    var onSaveError = function () {
      vm.isSaving = false;
    };

    vm.save = function () {
      vm.isSaving = true;
      vm.treatyAmendments.lastUser=vm.lastUser;
      vm.treatyAmendments.isActive = 1;
      vm.treatyAmendments.notificationStatus = (vm.actualUser==='Admin') ? vm.notificationConfirmed : vm.notificationNotified;
      if (vm.treatyAmendments.id !== null) {
        vm.treatyAmendments.lastUpdate= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        TreatyAmendments.update(vm.treatyAmendments, onSaveSuccess, onSaveError);
      } else {
        vm.treatyAmendments.creationUser=vm.lastUser;
        vm.treatyAmendments.creationTs= moment().format('YYYY-MM-DDTHH:mm:ss')+ 'Z';
        TreatyAmendments.save(vm.treatyAmendments, onSaveSuccess, onSaveError);
      }
    };

    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };

    vm.clearx = function () {
      var confirm = InteractionsService.confirmBasicDialog('Are you sure you want to exit without saving?', '#myModal');
      InteractionsService.showThenDialog(confirm, function() { $uibModalInstance.dismiss('cancel'); });
    };

    vm.datePickerOpenStatus = {};
    vm.datePickerOpenStatus.effectiveDate = false;

    vm.openCalendar = function (date) {
      vm.datePickerOpenStatus[date] = true;
    };

  }
})();
