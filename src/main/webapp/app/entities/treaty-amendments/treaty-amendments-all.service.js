(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('TreatyAmendmentsAll', TreatyAmendmentsAll);
  
  TreatyAmendmentsAll.$inject = ['$resource', 'DateUtils'];
  
  function TreatyAmendmentsAll($resource, DateUtils) {
    var resourceUrl = 'api/treaty-amendments/treaties/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          data.reportingDueDate = DateUtils.convertLocalDateFromServer(data.reportingDueDate);
          data.paymentDueDate = DateUtils.convertLocalDateFromServer(data.paymentDueDate);
          data.summaryDate = DateUtils.convertLocalDateFromServer(data.summaryDate);
          return data;
        }
      }
    });
  }
})();
