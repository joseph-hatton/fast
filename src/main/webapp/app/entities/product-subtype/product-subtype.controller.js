(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .controller('ProductSubtypeController', ProductSubtypeController);
  
  ProductSubtypeController.$inject = ['$scope', '$state', 'ProductSubtype', 'ProductSubtypeSearch'];
  
  function ProductSubtypeController($scope, $state, ProductSubtype, ProductSubtypeSearch) {
    var vm = this;
    vm.productSubtypes = [];
    vm.loadAll = function () {
      ProductSubtype.query(function (result) {
        vm.productSubtypes = result;
      });
    };
    
    vm.search = function () {
      if (!vm.searchQuery) {
        return vm.loadAll();
      }
      ProductSubtypeSearch.query({query: vm.searchQuery}, function (result) {
        vm.productSubtypes = result;
      });
    };
    vm.loadAll();
    
  }
})();
