(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .controller('ProductSubtypeDeleteController', ProductSubtypeDeleteController);
  
  ProductSubtypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'ProductSubtype'];
  
  function ProductSubtypeDeleteController($uibModalInstance, entity, ProductSubtype) {
    var vm = this;
    vm.productSubtype = entity;
    vm.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };
    vm.confirmDelete = function (id) {
      ProductSubtype.delete({id: id},
        function () {
          $uibModalInstance.close(true);
        });
    };
  }
})();
