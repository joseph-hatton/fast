(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('ProductSubtypeDetailController', ProductSubtypeDetailController);

  ProductSubtypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'ProductSubtype', 'DetailService'];

  function ProductSubtypeDetailController($scope, $rootScope, $stateParams, entity, ProductSubtype, DetailService) {
    this.prototype = Object.create(DetailService.prototype);

    this.productSubtype = entity;

    DetailService.call(this, $scope, $rootScope, $stateParams, ProductSubtype, this.productSubtype, "ProductSubtype");
  }
})();
