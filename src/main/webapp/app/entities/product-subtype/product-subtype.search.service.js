(function () {
  'use strict';
  
  angular
    .module('finreApp')
    .factory('ProductSubtypeSearch', ProductSubtypeSearch);
  
  ProductSubtypeSearch.$inject = ['$resource'];
  
  function ProductSubtypeSearch($resource) {
    var resourceUrl = 'api/_search/product-subtypes/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true}
    });
  }
})();
