(function () {
  'use strict';

  angular
    .module('finreApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('product-subtype', {
        parent: 'entity',
        url: '/product-subtype',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.productSubtype.home.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/product-subtype/product-subtypes.html',
            controller: 'ProductSubtypeController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('productSubtype');
            $translatePartialLoader.addPart('global');
            return $translate.refresh();
          }]
        }
      })
      .state('product-subtype-detail', {
        parent: 'entity',
        url: '/product-subtype/{id}',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR','ROLE_APP_FINRE_VIEW_ONLY'],
          pageTitle: 'finreApp.productSubtype.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'app/entities/product-subtype/product-subtype-detail.html',
            controller: 'ProductSubtypeDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('productSubtype');
            return $translate.refresh();
          }],
          entity: ['$stateParams', 'ProductSubtype', function ($stateParams, ProductSubtype) {
            return ProductSubtype.get({id: $stateParams.id});
          }]
        }
      })
      .state('product-subtype.new', {
        parent: 'product-subtype',
        url: '/new',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/product-subtype/product-subtype-dialog.html',
            controller: 'ProductSubtypeDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  name: null,
                  description: null,
                  id: null
                };
              }
            }
          }).result.then(function () {
            $state.go('product-subtype', null, {reload: true});
          }, function () {
            $state.go('product-subtype');
          });
        }]
      })
      .state('product-subtype.edit', {
        parent: 'product-subtype',
        url: '/{id}/edit',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/product-subtype/product-subtype-dialog.html',
            controller: 'ProductSubtypeDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['ProductSubtype', function (ProductSubtype) {
                return ProductSubtype.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('product-subtype', null, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      })
      .state('product-subtype.delete', {
        parent: 'product-subtype',
        url: '/{id}/delete',
        data: {
          authorities: ['ROLE_APP_FINRE_OPS_ADMIN','ROLE_APP_FINRE_PROCESSOR']
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/entities/product-subtype/product-subtype-delete-dialog.html',
            controller: 'ProductSubtypeDeleteController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['ProductSubtype', function (ProductSubtype) {
                return ProductSubtype.get({id: $stateParams.id});
              }]
            }
          }).result.then(function () {
            $state.go('product-subtype', null, {reload: true});
          }, function () {
            $state.go('^');
          });
        }]
      });
  }

})();
