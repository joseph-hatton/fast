(function () {
  'use strict';
  angular
    .module('finreApp')
    .factory('ProductSubtype', ProductSubtype);
  
  ProductSubtype.$inject = ['$resource'];
  
  function ProductSubtype($resource) {
    var resourceUrl = 'api/product-subtypes/:id';
    
    return $resource(resourceUrl, {}, {
      'query': {method: 'GET', isArray: true},
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          return data;
        }
      },
      'update': {method: 'PUT'}
    });
  }
})();
