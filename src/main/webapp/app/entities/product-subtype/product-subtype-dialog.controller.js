(function () {
  'use strict';

  angular
    .module('finreApp')
    .controller('ProductSubtypeDialogController', ProductSubtypeDialogController);

  ProductSubtypeDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'ProductSubtype', 'DialogService'];

  function ProductSubtypeDialogController ($scope, $stateParams, $uibModalInstance, entity, ProductSubtype, DialogService) {
    this.prototype = Object.create(DialogService.prototype);

    var datePickerItemsRun = function (vm) {};

    this.productSubtype = entity;

    DialogService.call(this, $scope, $stateParams, $uibModalInstance, entity, ProductSubtype, this.productSubtype, "ProductSubtype", datePickerItemsRun);
  }
})();
