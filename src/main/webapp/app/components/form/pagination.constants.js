(function() {
    'use strict';

    angular
        .module('finreApp')
        .constant('paginationConstants', {
            'itemsPerPage': 10
        });
})();
