(function() {
  'use strict';

  angular
    .module('finreApp')
    .directive('fastUploader', fastUploader);

  fastUploader.$inject = ['$parse'];

  function fastUploader($parse) {
    var directive = {
      restrict: 'A',
      link: linkFunc
    };
    return directive;

    function linkFunc(scope, element, attrs) {
      var model = $parse(attrs.fastUploader);
      var modelAssign = model.assign;
      element.bind('change', function () {
        scope.$apply(function () {
          modelAssign(scope, element[0].files[0]);
        });
      });
    }
  }
})();
