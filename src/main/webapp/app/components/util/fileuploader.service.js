(function() {
  angular
    .module('finreApp')
    .factory("UploadService",UploadService);

      UploadService.$inject = ['$http', '$q'];

      function UploadService ($http, $q) {

        return ({
          upload: upload
        });

        function upload(file) {
          var upl = $http({
            method: 'POST',
            url: 'http://localhost:8080/api/file-upload', // /api/upload
            headers: {
              'Content-Type': 'multipart/form-data'
            },
            data: {
              upload: file
            },
            transformRequest: function (data, headersGetter) {
              var formData = new FormData();
              angular.forEach(data, function (value, key) {
                formData.append(key, value);
              });

              var headers = headersGetter();
              delete headers['Content-Type'];

              return formData;
            }
          });
          return upl.then(handleSuccess, handleError);

        } // End upload function

        // ---
        // PRIVATE METHODS.
        // ---

        function handleError(response, data) {
          if (!angular.isObject(response.data) || !response.data.message) {
            return ($q.reject("An unknown error occurred."));
          }

          return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
          return (response);
        }
      }
})();
