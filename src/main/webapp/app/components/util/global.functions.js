function uploadImage(element, update) {
  var file = element.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
      var img = document.createElement('img');
      img.src = reader.result;
      if (currentSelectionUpload.getRangeAt && currentSelectionUpload.rangeCount) {
          var range = currentSelectionUpload.getRangeAt(0);
          range.deleteContents();
          range.insertNode(img);
          update(currentDocumentUpload);
      }
  };
  angular.element(document.querySelector("#imageUploader")).val("");
  reader.readAsDataURL(file);
};

//Below 'find' polyfill is from mdn resources: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find
if (!Array.prototype.find) {
  Object.defineProperty(Array.prototype, 'find', {
    value: function(predicate) {
     // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If IsCallable(predicate) is false, throw a TypeError exception.
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }

      // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
      var thisArg = arguments[1];

      // 5. Let k be 0.
      var k = 0;

      // 6. Repeat, while k < len
      while (k < len) {
        // a. Let Pk be ! ToString(k).
        // b. Let kValue be ? Get(O, Pk).
        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
        // d. If testResult is true, return kValue.
        var kValue = o[k];
        if (predicate.call(thisArg, kValue, k, o)) {
          return kValue;
        }
        // e. Increase k by 1.
        k++;
      }

      // 7. Return undefined.
      return undefined;
    }
  });
}

var YEAR_END_MONTH_OPTIONS=[
  {id:1, name:'January'},
  {id:2, name:'February'},
  {id:3, name:'March'},
  {id:4, name:'April'},
  {id:5, name:'May'},
  {id:6, name:'June'},
  {id:7, name:'July'},
  {id:8, name:'August'},
  {id:9, name:'September'},
  {id:10, name:'October'},
  {id:11, name:'November'},
  {id:12, name:'December'}];

var DATE_MONTH_FORMAT = {
  datepickerMode: 'month',
  formatMonth: 'MMMM',
  formatYear: 'yyyy',
  formatMonthTitle: 'yyyy',
  minMode: 'month',
  showMonths: true,
  startingDay: 0,
  yearRange: 20,
  minDate: null,
  maxDate: null
};

function getNextReportingPeriodDate(date, Frequency) {
  if (Frequency === 'Monthly') {
    date = moment(date).add(1, 'M');
  }
  if (Frequency === 'Quarterly') {
    date = moment(date).add(3, 'M');
  }
  if (Frequency === 'Semi-Annual') {
    date = moment(date).add(6, 'M');
  }
  if (Frequency === 'Annual') {
    date = moment(date).add(12, 'M');
  }
  return date;
}

String.prototype.includes = function (str) {
  return this.indexOf(str) >= 0;
};

function displayQuarterly(data) {
  return data.mode === 'month' &&
    (data.date.getMonth() === 0 || data.date.getMonth() === 1 ||
      data.date.getMonth() === 3 || data.date.getMonth() === 4 ||
      data.date.getMonth() === 6 || data.date.getMonth() === 7 ||
      data.date.getMonth() === 9 || data.date.getMonth() === 10 );
}

function displaySemmiAnnual(data) {
  return data.mode === 'month' &&
    (data.date.getMonth() === 0 || data.date.getMonth() === 1 || data.date.getMonth() === 2 ||
      data.date.getMonth() === 3 || data.date.getMonth() === 4 || data.date.getMonth() === 6 ||
      data.date.getMonth() === 7 || data.date.getMonth() === 8 ||
      data.date.getMonth() === 9 || data.date.getMonth() === 10 );
}

function calculatingDate(frequency, entityDate, reportingMonthYear, mainfrequency) {
  if (frequency === null) return;
  if (frequency.includes('Month')) {
    if (entityDate.period.includes('Quarter')) {
      return calculateQuarterly(entityDate, reportingMonthYear);
    } else {
      return calculateMonthly(entityDate, reportingMonthYear);
    }
  } else if (frequency.includes('Quarter')) {
    return calculateQuarterly(entityDate, reportingMonthYear);
  } else if (frequency.includes('Semi-Annual')) {
    return calculateSemiAnnually(entityDate, reportingMonthYear);
  } else if (frequency.includes('Year') || frequency === 'Annual') {
    return calculateAnnually(entityDate, reportingMonthYear);
  } else if (frequency.includes('Reporting Received') || frequency.includes('Reporting Confirmed')) {
    if (mainfrequency === 'Monthly') {
      return calculateMonthlyReportRec(entityDate, reportingMonthYear);
    }
    if (mainfrequency === 'Quarterly') {
      return calculateQuarterlyReportRec(entityDate, reportingMonthYear);
    }
    if (mainfrequency === 'Annual') {
      return calculateAnnuallyReportRec(entityDate, reportingMonthYear);
    }
    if (mainfrequency === 'Semi-Annual') {
      return calculateSemiAnnuallyReportRec(entityDate, reportingMonthYear);
    }
  }
}

function calculateAnnuallyReportRec(entityDate, reportingMonthYear) {
  return calculateCalendarOrBusiness(moment(reportingMonthYear), entityDate).format('YYYY-MM-DD');
  //theses are 0-based months
}

function calculateMonthlyReportRec(entityDate, reportingMonthYear) {
  return calculateCalendarOrBusiness(moment(reportingMonthYear), entityDate).format('YYYY-MM-DD');
}

function calculateQuarterlyReportRec(entityDate, reportingMonthYear) {
  return calculateCalendarOrBusiness(moment(reportingMonthYear), entityDate).format('YYYY-MM-DD');
}

function calculateMonthly(entityDate, reportingMonthYear) {
  return calculateCalendarOrBusiness(calculateEndOfMonth(reportingMonthYear), entityDate).format('YYYY-MM-DD');
}

function calculateMonthlyOn(entityDate, reportingMonthYear) {
  return calculateCalendarOrBusiness(calculateEndOfMonth(reportingMonthYear), entityDate).format('YYYY-MM-DD');
}

function calculateQuarterly(entityDate, reportingMonthYear) {
  return calculateCalendarOrBusiness(calculateEndOfQuarter(reportingMonthYear), entityDate).format('YYYY-MM-DD');
}

function calculateQuarterlyOn(entityDate, reportingMonthYear) {
  return calculateCalendarOrBusiness(calculateEndOfQuarter(reportingMonthYear), entityDate).format('YYYY-MM-DD');
}

function calculateEndOfMonth(reportingMonthYear) {
  return moment(reportingMonthYear).endOf('month');
}

function calculateEndOfQuarter(reportingMonthYear) {
  //theses are 0-based months
  var firstQuarter = 2;
  var secondQuarter = 5;
  var thirdQuarter = 8;
  var forthQuarter = 11;
  if (moment(reportingMonthYear).get('month') <= firstQuarter) {
    //1st Quarter - Set Quarter Month
    return moment().set({'year': moment(reportingMonthYear).get('year'), 'month': firstQuarter}).endOf('month');
  } else if (moment(reportingMonthYear).get('month') > firstQuarter &&
    moment(reportingMonthYear).get('month') <= secondQuarter) {
    //2nd Quarter  - Set Quarter Month
    return moment().set({'year': moment(reportingMonthYear).get('year'), 'month': secondQuarter}).endOf('month');
  } else if (moment(reportingMonthYear).get('month') > secondQuarter &&
    moment(reportingMonthYear).get('month') <= thirdQuarter) {
    //3rd Quarter  - Set Quarter Month
    return moment(reportingMonthYear).set({
      'year': moment(reportingMonthYear).get('year'),
      'month': thirdQuarter
    }).endOf('month');
  } else {
    //4th Quarter  - Set Quarter Month
    return moment().set({'year': moment(reportingMonthYear).get('year'), 'month': forthQuarter}).endOf('month');
  }
}

function calculateCalendarOrBusiness(dueDate, entityDate) {

  if (entityDate.dayType.includes('Calendar')) {
    if (entityDate.dayType.includes('After')) {
      return calendarDaysAfter(dueDate, entityDate.numberOfDays);
    } else {
      return calendarDaysBefore(dueDate, entityDate.numberOfDays);
    }
  } else {
    if (entityDate.dayType.includes('After')) {
      return businessDaysAfter(dueDate, entityDate.numberOfDays);
    } else {
      return businessDaysBefore(dueDate, entityDate.numberOfDays);
    }
  }
}

function calculateSemiAnnually(entityDate, reportingMonthYear) {
  return calculateCalendarOrBusiness(calculateSemiAnnualMonth(reportingMonthYear), entityDate).format('YYYY-MM-DD');
}

function calculateSemiAnnuallyReportRec(entityDate, reportingMonthYear) {
  return calculateCalendarOrBusiness(calculateSemiAnnualMonth(reportingMonthYear), entityDate).format('YYYY-MM-DD');
}

function calculateSemiAnnualMonth(reportingMonthYear) {
  //theses are 0-based months
  var halfYear = 5;
  var endYearMonth = 11;
  if (moment(reportingMonthYear).get('month') <= halfYear) {
    //Half Year - Set Half Year
    return moment().set({'year': moment(reportingMonthYear).get('year'), 'month': halfYear}).endOf('month');
  } else {
    //End of the Year - Set Year End Month
    return moment().set({'year': moment(reportingMonthYear).get('year'), 'month': endYearMonth}).endOf('month');
  }
}

function calculateAnnually(entityDate, reportingMonthYear) {
  return calculateCalendarOrBusiness(calculateAnnualMonth(reportingMonthYear), entityDate).format('YYYY-MM-DD');
  //theses are 0-based months
}

function calculateAnnualMonth(reportingMonthYear) {
  //theses are 0-based months
  var endYearMonth = 11;
  //End of the Year - Set Year End Month
  return moment().set({'year': moment(reportingMonthYear).get('year'), 'month': endYearMonth}).endOf('month');
}

function businessDaysAfter(date, days) {
  for (var i = 0; i < days;) {
    date = date.add(1, 'days');
    // decrease "days" only if it's a weekday.
    if (date.isoWeekday() !== 6 && date.isoWeekday() !== 7) {
      i++;
    }
  }
  return date;
}

function businessDaysBefore(date, days) {
  for (var i = 0; i < days;) {
    date = date.subtract(1, 'days');
    // decrease "days" only if it's a weekday.
    if (date.isoWeekday() !== 6 && date.isoWeekday() !== 7) {
      i++;
    }
  }
  return date;
}

function calendarDaysAfter(date, days) {
  for (var i = 0; i < days; i++) {
    date = date.add(1, 'days');
  }
  return date;
}

function calendarDaysBefore(date, days) {

  if (date !== null) {
    for (var i = 0; i < days; i++) {
      date = date.subtract(1, 'days');
    }
  }

  return date;
}

function calculateQuarters(reportingMonthYear) {
  //theses are 0-based months
  var firstQuarter = 2;
  var secondQuarter = 5;
  var thirdQuarter = 8;
  var forthQuarter = 11;
  var quarterly = ["Q1", "Q2", "Q3", "Q4"];

  if (moment(reportingMonthYear).get('month') <= firstQuarter) {
    //1st Quarter - Set Quarter Month
    return moment(reportingMonthYear).get('year') + " " + quarterly[0];
  } else if (moment(reportingMonthYear).get('month') > firstQuarter &&
    moment(reportingMonthYear).get('month') <= secondQuarter) {
    //2nd Quarter  - Set Quarter Month
    return moment(reportingMonthYear).get('year') + " " + quarterly[1];
  } else if (moment(reportingMonthYear).get('month') > secondQuarter &&
    moment(reportingMonthYear).get('month') <= thirdQuarter) {
    //3rd Quarter  - Set Quarter Month
    return moment(reportingMonthYear).get('year') + " " + quarterly[2];
  } else {
    //4th Quarter  - Set Quarter Month
    return moment(reportingMonthYear).get('year') + " " + quarterly[3];
  }
}

function calculateMonth(reportingMonthYear) {
  return moment(reportingMonthYear).format('MMMM');
}

function dateMask(val) {
  if (val.match(/^\d{4}$/) !== null || val.match(/^\d{4}\-\d{2}$/) !== null) {
    val += '-';
  }
  return val;
}

function dateMaskYM(val) {
  if (val.match(/^\d{4}$/) !== null) {
    val += '-';
  }
  return val;
}

function calculateQuarterDate(month, year) {
  return moment().set({'year': year, 'month': month}).endOf('month').format('YYYY-MM-DD');
}

function convertYMDate(date) {
  var thisDate = moment(date);
  return moment().set({
    'year': thisDate.get('year'),
    'month': thisDate.get('month'),
    'date': thisDate.get('date')
  }).format('YYYY-MM-DD');
}

function dateAddDays(date, days) {
  if (date !== null) {
    for (var i = 0; i < days; i++) {
      date = date.add(1, 'days');
    }
  }
  return date;
}

(function () {
  'use strict';
  angular
    .module('finreApp')
    .filter('unique', function () {
      return function (collection, keyname) {
        var output = [],
          keys = [];

        angular.forEach(collection, function (item) {
          var key = item[keyname];
          if (keys.indexOf(key) === -1) {
            keys.push(key);
            output.push(item);
          }
        });

        return output;
      };
    });
})();
