
  ${AnsiColor.GREEN} ████████   ████████   ██         ██  ████████    ████████
  ${AnsiColor.GREEN} ██            ██      ██  ██    ██   ██    ██    ██
  ${AnsiColor.GREEN} ████          ██      ██   ██   ██   ████████    ████
  ${AnsiColor.GREEN} ██            ██      ██     ██ ██   ██   ██     ██
  ${AnsiColor.GREEN} ██         ████████   ██        ██   ██     ██   ████████

${AnsiColor.BRIGHT_BLUE}:: FinRe 🤓  :: Running Spring Boot ${spring-boot.version} ::
:: http://connect.rgare.net ::${AnsiColor.DEFAULT}
