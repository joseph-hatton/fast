package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A MdmAttributes.
 */
@Entity
@Table(name = "mdm_attributes")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "mdmattributes")
public class MdmAttributes extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "enterprise_id")
    private Long enterpriseId;

    @Column(name = "affiliate_type")
    private String affiliateType;

    @Column(name = "ek_comment")
    private String ekComment;

    @Column(name = "deal")
    private String deal;

    @Column(name = "us_stat_risk")
    private String usStatRisk;

    @Column(name = "effective_status_cd")
    private String effectiveStatusCd;


   @Column(name = "business_unit_id")
    private Long businessUnitId;

    @Column(name = "business_unit_name")
    private String businessUnitName;

    @Column(name = "corporate_indicator")
    private String corporateIndicator;

    @Column(name = "legal_doc")
    private String legalDoc;

    @ManyToOne
    @JoinColumn(name="product_structure_id", columnDefinition = "varchar2")
    private ProductStructure productStructure;

    @Column(name = "reporting_product_id")
    private String reportingProductId;

    @Column(name = "reporting_product")
    private String reportingProduct;

    @Column(name = "deal_id")
    private Long dealId;

    @Column(name = "client_company_id")
    private Long clientCompanyId;

    @Column(name = "product_line_id")
    private String productLineId;

    @Column(name = "product_line_name")
    private String productLineName;

    @Column(name = "client_company_name")
    private String clientCompanyName;

    @Column(name = "form_of_reinsurance")
    private String formOfReinsurance;

    @Column(name = "product_group_id")
    private long productGroupId;

    @Column(name = "product_group_name")
    private String productGroupName;

    @Column(name = "product_type_id")
    private Long productTypeId;

    @Column(name = "product_type_name")
    private String productTypeName;

    @Column(name = "risk_transfer_gaap")
    private String riskTransferGaap;

    @Column(name = "risk_transfer_local")
    private String riskTransferLocal;

    @Column(name = "risk_transfer_state")
    private String riskTransferState;

    @Column(name = "legal_code")
    private String legalCode;

    @Column(name = "legal_name")
    private String legalName;

  /**HB_CHANGED_VID**/
    @Column(name = "VERSION_NUMBER")
    private Long versionNumber;

    /**HB_CREATED_VID**/
    @Column(name = "VERSION_DATE")
    private Long versionDate;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JsonBackReference("mdmTreaty")
    private Treaty mdmTreaty;

    public String getUsStatRisk() {
      return usStatRisk;
    }

    public void setUsStatRisk(String usStatRisk) {
      this.usStatRisk = usStatRisk;
    }

    public String getDeal() {
      return deal;
    }

    public void setDeal(String deal) {
      this.deal = deal;
    }

    public String getEffectiveStatusCd() {
      return effectiveStatusCd;
    }

    public void setEffectiveStatusCd(String effectiveStatusCd) {
      this.effectiveStatusCd = effectiveStatusCd;
    }

    public Long getVersionNumber() {
        return versionNumber;
      }

    public void setVersionNumber(Long versionNumber) {
      this.versionNumber = versionNumber;
    }

    public Long getVersionDate() {
      return versionDate;
    }

    public void setVersionDate(Long versionDate) {
      this.versionDate = versionDate;
    }

    public String getEkComment() {
      return ekComment;
    }

    public void setEkComment(String ekComment) {
      this.ekComment = ekComment;
    }

    public Long getId() {
          return id;
      }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEnterpriseId() {
        return enterpriseId;
    }

    public void setEnterpriseId(Long enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    public String getAffiliateType() {
        return affiliateType;
    }

    public void setAffiliateType(String affiliateType) {
        this.affiliateType = affiliateType;
    }

    public Long getBusinessUnitId() {
        return businessUnitId;
    }

    public void setBusinessUnitId(Long businessUnitId) {
        this.businessUnitId = businessUnitId;
    }

    public String getBusinessUnitName() {
        return businessUnitName;
    }

    public void setBusinessUnitName(String businessUnitName) {
        this.businessUnitName = businessUnitName;
    }

    public String getCorporateIndicator() {
        return corporateIndicator;
    }

    public String getLegalCode() {
        return legalCode;
      }

    public void setLegalCode(String legalCode) {
        this.legalCode = legalCode;
      }

    public String getLegalName() {
    return legalName;
  }

    public void setLegalName(String legalName) {
    this.legalName = legalName;
  }

    public void setCorporateIndicator(String corporateIndicator) {
        this.corporateIndicator = corporateIndicator;
    }

    public String getLegalDoc() {
        return legalDoc;
    }

    public void setLegalDoc(String legalDoc) {
        this.legalDoc = legalDoc;
    }

    public String getReportingProduct() {
        return reportingProduct;
    }

    public void setReportingProduct(String reportingProduct) {
        this.reportingProduct = reportingProduct;
    }

    public ProductStructure getProductStructure() {
      return productStructure;
    }

    public void setProductStructure(ProductStructure productStructure) {
      this.productStructure = productStructure;
    }

    public String getReportingProductId() {
      return reportingProductId;
    }

    public void setReportingProductId(String reportingProductId) {
      this.reportingProductId = reportingProductId;
    }

    public Long getDealId() {
      return dealId;
    }

    public void setDealId(Long dealId) {
      this.dealId = dealId;
    }


    public String getProductLineId() {
    return productLineId;
  }

    public void setProductLineId(String productLineId) {
    this.productLineId = productLineId;
  }

    public String getProductLineName() {
    return productLineName;
  }

    public void setProductLineName(String productLineName) {
    this.productLineName = productLineName;
  }

    public Long getClientCompanyId() {
      return clientCompanyId;
    }

    public void setClientCompanyId(Long clientCompanyId) {
      this.clientCompanyId = clientCompanyId;
    }

    public String getClientCompanyName() {
      return clientCompanyName;
    }

    public void setClientCompanyName(String clientCompanyName) {
      this.clientCompanyName = clientCompanyName;
    }

    public String getFormOfReinsurance() {
      return formOfReinsurance;
    }

    public void setFormOfReinsurance(String formOfReinsurance) {
      this.formOfReinsurance = formOfReinsurance;
    }

    public long getProductGroupId() {
      return productGroupId;
    }

    public void setProductGroupId(long productGroupId) {
      this.productGroupId = productGroupId;
    }

    public String getProductGroupName() {
      return productGroupName;
    }

    public void setProductGroupName(String productGroupName) {
      this.productGroupName = productGroupName;
    }

    public Long getProductTypeId() {
      return productTypeId;
    }

    public void setProductTypeId(Long productTypeId) {
      this.productTypeId = productTypeId;
    }

    public String getProductTypeName() {
      return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
      this.productTypeName = productTypeName;
    }

    public String getRiskTransferGaap() {
      return riskTransferGaap;
    }

    public void setRiskTransferGaap(String riskTransferGaap) {
      this.riskTransferGaap = riskTransferGaap;
    }

    public String getRiskTransferLocal() {
      return riskTransferLocal;
    }

    public void setRiskTransferLocal(String riskTransferLocal) {
      this.riskTransferLocal = riskTransferLocal;
    }

    public String getRiskTransferState() {
      return riskTransferState;
    }

    public void setRiskTransferState(String riskTransferState) {
      this.riskTransferState = riskTransferState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MdmAttributes mdmAttributes = (MdmAttributes) o;
        if(mdmAttributes.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, mdmAttributes.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MdmAttributes{" +
            "id=" + id +
            ", enterpriseId='" + enterpriseId + "'" +
            ", affiliateType='" + affiliateType + "'" +
            ", businessUnitId='" + businessUnitId + "'" +
            ", businessUnitName='" + businessUnitName + "'" +
            ", corporateIndicator='" + corporateIndicator + "'" +
            ", legalDoc='" + legalDoc + "'" +
            ", reportingProduct='" + reportingProduct + "'" +
            ", formOfReinsurance='" + formOfReinsurance + "'" +
            ", productGroupId='" + productGroupId + "'" +
            ", productGroupName='" + productGroupName + "'" +
            ", productTypeId='" + productTypeId + "'" +
            ", productTypeName='" + productTypeName + "'" +
            ", riskTransferGaap='" + riskTransferGaap + "'" +
            ", riskTransferLocal='" + riskTransferLocal + "'" +
            ", riskTransferState='" + riskTransferState + "'" +
            '}';
    }
}
