package com.rgare.global.finre.domain;

//import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;


/**
 * Created by s0044514 on 1/12/2017.
 */

@Entity
@Table(name = "audit_treaty")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "audittreaty")
public class AuditTreaty implements Serializable {

  private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "treaty_id")
    private Long treatyId;

    @Column(name = "action")
    private String action;

    @Column(name = "name")
    private String name;

    @Column(name = "comments")
    private String comments;

    @Column(name = "creation_user")
    String creationUser;

    @Column(name = "creation_ts")
    ZonedDateTime creationTs;

    public Long getId() {
    return id;
    }

    public void setId(Long id) {
    this.id = id;
    }

    public Long getTreatyId() {
    return treatyId;
    }

    public void setTreatyId(Long treatyId) {
    this.treatyId = treatyId;
    }

    public String getAction() {
    return action;
    }

    public void setAction(String action) {
    this.action = action;
    }

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

    public String getComments() {
    return comments;
    }

    public void setComments(String comments) {
    this.comments = comments;
    }

    public String getCreationUser() {
    return creationUser;
    }

    public void setCreationUser(String creationUser) {
    this.creationUser = creationUser;
    }

    public ZonedDateTime getCreationTs() {
    return creationTs;
    }

    public void setCreationTs(ZonedDateTime creationTs) {
    this.creationTs = creationTs;
    }
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AuditTreaty auditTreaty = (AuditTreaty) o;
    if(auditTreaty.id == null || id == null) {
      return false;
    }
    return Objects.equals(id, auditTreaty.id);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id);
  }

  @Override
  public String toString() {
    return "AuditTreaty{" +
      "id=" + id +
      ", treatyId='" + treatyId + "'" +
      ", action='" + action + "'" +
      ", comments='" + comments + "'" +
      ", name='" + name + "'" +
      ", creationUser='" + creationUser + "'" +
      '}';
  }
}

