package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A TreatyManagement.
 */
@Entity
@Table(name = "treaty_management")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "treatymanagement")
public class TreatyManagement extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "quota_share")
    private BigDecimal quotaShare;

    @Column(name = "summary_date")
    private LocalDate summaryDate;

    @Column(name = "active_status")
    private Integer isActive;

    @Column(name = "input_status")
    private String inputStatus;

    @Column(name = "general_comment_refund")
    private String generalCommentRefund;

    @Column(name = "calendarization")
    private String calendarization;

    @Column(name = "calendarization_treaty_refer")
    private String calendarizationTreatyRefer;

    @Column(name = "confirmation_requested")
    private String  confirmationRequested;

    @Column(name = "confirmation_requested_comment")
    private String confirmationRequestedComment;

    @Column(name = "accounting_treaty_reference")
    private String accountingTreatyReference;

    @Column(name = "quota_share_comment")
    private String quotaShareComment;

    @Column(name = "treaty_reference_recapture")
    private String treatyReferenceRecapture;

    @Column(name = "recapture_comment")
    private String recaptureComment;

    @Column(name = "calendarization_comments")
    private String calendarizationComments;

    @Column(name = "refund_elimination_flag")
    private String refundEliminationFlag;

    @Column(name = "refund_elimination_ref")
    private String refundEliminationRef;

    @Column(name = "refund_comments")
    private String refundComments;

    @ManyToOne
    private Treaty treaty;



    @ManyToOne
    private Frequency accountingFrequency;


    @OneToMany(orphanRemoval=true, fetch = FetchType.EAGER, mappedBy = "riskFee", cascade = { CascadeType.ALL})
    @JsonManagedReference("riskFee")
    private Set<RiskFee> riskFees = new HashSet<>();


    public Long getId() {
          return id;
      }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getQuotaShare() {
        return quotaShare;
    }

    public void setQuotaShare(BigDecimal quotaShare) {
        this.quotaShare = quotaShare;
    }

    public LocalDate getSummaryDate() {
        return summaryDate;
    }

    public void setSummaryDate(LocalDate summaryDate) {
        this.summaryDate = summaryDate;
    }

    public Integer getIsActive() {
        return isActive;
      }

    public void setIsActive(Integer isActive) {
      this.isActive = isActive;
    }

    public String getInputStatus() {
      return inputStatus;
    }

    public void setInputStatus(String inputStatus) {
      this.inputStatus = inputStatus;
    }



  public Treaty getTreaty() {
          return treaty;
      }

    public void setTreaty(Treaty treaty) {
        this.treaty = treaty;
    }

    public Frequency getAccountingFrequency() {
      return accountingFrequency;
    }

    public void setAccountingFrequency(Frequency accountingFrequency) {
      this.accountingFrequency = accountingFrequency;
    }
    public String getCalendarization() {
      return calendarization;
    }

    public void setCalendarization(String calendarization){
      this.calendarization= calendarization;
    }

    public String getCalendarizationTreatyRefer() {
      return calendarizationTreatyRefer;
    }

    public void setCalendarizationTreatyRefer(String calendarizationTreatyRefer) {
      this.calendarizationTreatyRefer = calendarizationTreatyRefer;
    }

    public String getConfirmationRequested() {
      return confirmationRequested;
    }

    public void setConfirmationRequested(String confirmationRequested){
      this.confirmationRequested = confirmationRequested;
    }

    public String getConfirmationRequestedComment() {
      return confirmationRequestedComment;
    }

    public void setConfirmationRequestedComment(String confirmationRequestedComment) {
      this.confirmationRequestedComment = confirmationRequestedComment;
    }

    public String getAccountingTreatyReference() {
      return accountingTreatyReference;
    }

    public void setAccountingTreatyReference(String accountingTreatyReference) {
      this.accountingTreatyReference = accountingTreatyReference;
    }

  public Set<RiskFee> getRiskFees() {
      return riskFees;
    }

    public void setRiskFees(Set<RiskFee> riskFees) {
      this.riskFees = riskFees;
    }

    public String getQuotaShareComment() {
      return quotaShareComment;
    }

    public void setQuotaShareComment(String quotaShareComment) {
      this.quotaShareComment = quotaShareComment;
    }

    public String getTreatyReferenceRecapture() {
      return treatyReferenceRecapture;
    }

    public void setTreatyReferenceRecapture(String treatyReferenceRecapture) {
      this.treatyReferenceRecapture = treatyReferenceRecapture;
    }

    public String getRecaptureComment() {
      return recaptureComment;
    }

    public void setRecaptureComment(String recaptureComment) {
      this.recaptureComment = recaptureComment;
    }

  public String getCalendarizationComments() {
    return calendarizationComments;
  }

  public void setCalendarizationComments(String calendarizationComments) {
    this.calendarizationComments = calendarizationComments;
  }

  public String getRefundEliminationFlag() {
    return refundEliminationFlag;
  }

  public void setRefundEliminationFlag(String refundEliminationFlag) {
    this.refundEliminationFlag = refundEliminationFlag;
  }

  public String getRefundEliminationRef() {
    return refundEliminationRef;
  }

  public void setRefundEliminationRef(String refundEliminationRef) {
    this.refundEliminationRef = refundEliminationRef;
  }

  public String getRefundComments() {
    return refundComments;
  }

  public void setRefundComments(String refundComments) {
    this.refundComments = refundComments;
  }

  public String getGeneralCommentRefund() {
    return generalCommentRefund;
  }

  public void setGeneralCommentRefund(String generalCommentRefund) {
    this.generalCommentRefund = generalCommentRefund;
  }

  @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TreatyManagement treatyManagement = (TreatyManagement) o;
        if(treatyManagement.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, treatyManagement.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TreatyManagement{" +
            "id=" + id +
            ", quotaShare='" + quotaShare + "'" +
            ", summaryDate='" + summaryDate + "'" +
            ", isActive='" + isActive + "'" +
            ", accountingFrequency='" + accountingFrequency  + "'" +
            ", calendarization='" + calendarization  + "'" +
            ", calendarizationTreatyRefer='" + calendarizationTreatyRefer  + "'" +
            ", confirmationRequested='" + confirmationRequested  + "'" +
            ", confirmationRequestedComment='" + confirmationRequestedComment  + "'" +
            ", accountingTreatyReference='" + accountingTreatyReference  + "'" +
            '}';


    }
}
