package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Deal.
 */
@Entity
@Table(name = "deal")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "deal")
public class Deal extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "deal_status")
    private String dealStatus;

    @Column(name = "client_deal_name")
    private String clientDealName;

    @Column(name = "terminated_date")
    private LocalDate terminatedDate;

    @Column(name = "effective_date")
    private LocalDate effectiveDate;

    @Column(name = "year_end_month")
    private Integer yearEndMonth;

    @Column(name = "year_end_day")
    private Integer yearEndDay;

    @Column(name = "maturity_date")
    private LocalDate maturityDate;

    @Column(name = "comments")
    private String comments;

    @Column(name = "active_status")
    private Integer isActive;

    @Column(name = "us_stat_risk")
    private String usStatRisk;

    @Column(name = "mdm_id")
    private Long mdmId;

    @Column(name = "FORM_OF_REINSURANCE")
    private String formOfReinsurance;

    @Column(name = "RISK_TRANSFER_GAAP")
    private String riskTransferGaap;

    @Column(name = "RISK_TRANSFER_LOCAL")
    private String riskTransferLocal;

    @Column(name = "RISK_TRANSFER_STATE")
    private String riskTransferState;

    @OneToOne
    private DealType dealType;

    @ManyToOne
    private EucUse eucUse;

    @ManyToOne
    private EucUse eucComplexity;

    @ManyToOne
    private EucUse eucRisk;

    @ManyToOne
    private EucUse eucSignificance;

    @ManyToOne
    private ProductLine productLine;

    @ManyToOne
    private ProductSubtype productSubtype;

    @ManyToOne
    private Country country;

    @ManyToOne
    private GfsAssignee gfsAssignee;

    public Long getId() {
          return id;
      }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
      String upperCaseName = name.toUpperCase();
      return upperCaseName;
    }

    public void setName(String name) {
        if(name != null) {
          String upperCaseName = name.toUpperCase();
          this.name = upperCaseName;
        }
    }

    public ProductLine getProductLine() {
        return productLine;
      }

    public void setProductLine(ProductLine productLine) {
      this.productLine = productLine;
    }

    public ProductSubtype getProductSubtype() {
      return productSubtype;
    }

    public void setProductSubtype(ProductSubtype productSubtype) {
      this.productSubtype = productSubtype;
    }

    public Integer getIsActive() {
      return isActive;
    }

    public void setIsActive(Integer isActive) {
      this.isActive = isActive;
    }

    public String getClientDealName() {
      return clientDealName;
    }

    public void setClientDealName(String clientDealName) {
      this.clientDealName = clientDealName;
    }

    public String getUsStatRisk() {
      return usStatRisk;
    }

    public void setUsStatRisk(String usStatRisk) {
      this.usStatRisk = usStatRisk;
    }

    public LocalDate getTerminatedDate() {
      return terminatedDate;
    }

    public void setTerminatedDate(LocalDate terminatedDate) {
      this.terminatedDate = terminatedDate;
    }

    public LocalDate getEffectiveDate() {
      return effectiveDate;
    }

    public void setEffectiveDate(LocalDate effectiveDate) {
      this.effectiveDate = effectiveDate;
    }

    public String getDealStatus() {
      return dealStatus;
    }

    public void setDealStatus(String dealStatus) {
      this.dealStatus = dealStatus;
    }

    public Integer getYearEndMonth() {
      return yearEndMonth;
    }

    public void setYearEndMonth(Integer yearEndMonth) {
      this.yearEndMonth = yearEndMonth;
    }

    public Integer getYearEndDay() {
      return yearEndDay;
    }

    public void setYearEndDay(Integer yearEndDay) {
      this.yearEndDay = yearEndDay;
    }

    public LocalDate getMaturityDate() {
        return maturityDate;
      }

    public void setMaturityDate(LocalDate maturityDate) {
      this.maturityDate = maturityDate;
    }

    public String getComments() {
      return comments;
    }

    public void setComments(String comments) {
      this.comments = comments;
    }

    public Long getMdmId() {
      return mdmId;
    }

    public void setMdmId(Long mdmId) {
      this.mdmId = mdmId;
    }

    public GfsAssignee getGfsAssignee() {
      return gfsAssignee;
    }

    public void setGfsAssignee(GfsAssignee gfsAssignee) {
      this.gfsAssignee = gfsAssignee;
    }

  public EucUse getEucUse() {
    return eucUse;
  }

  public Country getCountry() {
    return country;
  }

  public void setCountry(Country country) {
    this.country = country;
  }

  public void setEucUse(EucUse eucUse) {
    this.eucUse = eucUse;
  }

  public EucUse getEucComplexity() {
    return eucComplexity;
  }

  public void setEucComplexity(EucUse eucComplexity) {
    this.eucComplexity = eucComplexity;
  }

  public EucUse getEucRisk() {
    return eucRisk;
  }

  public void setEucRisk(EucUse eucRisk) {
    this.eucRisk = eucRisk;
  }

  public EucUse getEucSignificance() {
    return eucSignificance;
  }

  public void setEucSignificance(EucUse eucSignificance) {
    this.eucSignificance = eucSignificance;
  }

  public DealType getDealType() {
    return dealType;
  }

  public void setDealType(DealType dealType) {
    this.dealType = dealType;
  }

  public String getFormOfReinsurance() {
    return formOfReinsurance;
  }

  public void setFormOfReinsurance(String formOfReinsurance) {
    this.formOfReinsurance = formOfReinsurance;
  }

  public String getRiskTransferGaap() {
    return riskTransferGaap;
  }

  public void setRiskTransferGaap(String riskTransferGaap) {
    this.riskTransferGaap = riskTransferGaap;
  }

  public String getRiskTransferLocal() {
    return riskTransferLocal;
  }

  public void setRiskTransferLocal(String riskTransferLocal) {
    this.riskTransferLocal = riskTransferLocal;
  }

  public String getRiskTransferState() {
    return riskTransferState;
  }

  public void setRiskTransferState(String riskTransferState) {
    this.riskTransferState = riskTransferState;
  }

  @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Deal deal = (Deal) o;
        if(deal.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, deal.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Deal{" +
                "id=" + id +
                ", name='" + name + "'" +
                ", usStatRisk='" + usStatRisk + "'" +
                ", clientDealName='" + clientDealName + "'" +
                '}';
    }
}
