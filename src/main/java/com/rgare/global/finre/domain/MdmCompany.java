package com.rgare.global.finre.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;


/**
 * Created by s0044514 on 12/6/2016.
 */
@Entity
@Table(name = "mdm_company")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "mdmcompany")
public class MdmCompany implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  private Long id;

  @Column(name = "mdm_id")
  private Long mdmId;

  @Column(name = "client_company_id")
  private Long clientCompanyId;

  @Column(name = "affiliate_name")
  private String affiliatename;

  @Column(name = "country")
  private String countryName;

  @Column(name = "state")
  private String state;

  @Column(name = "city")
  private String city;

  @Column(name = "legal_name")
  private String legalName;

  @Column(name = "legal_code")
  private String legalCode;

  @Column(name = "company_name")
  private String companyName;

  @Column(name = "version_date")
  private Long versionDate;

  @Column(name = "version_number")
  private Long versionNumber;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getMdmId() {
    return mdmId;
  }

  public void setMdmId(Long mdmId) {
    this.mdmId = mdmId;
  }

  public Long getClientCompanyId() {
    return clientCompanyId;
  }

  public void setClientCompanyId(Long clientCompanyId) {
    this.clientCompanyId = clientCompanyId;
  }

  public String getAffiliatename() {
    return affiliatename;
  }

  public void setAffiliatename(String affiliatename) {
    this.affiliatename = affiliatename;
  }

  public String getCountryName() {
    return countryName;
  }

  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getLegalName() {
    return legalName;
  }

  public void setLegalName(String legalName) {
    this.legalName = legalName;
  }

  public String getLegalCode() {
    return legalCode;
  }

  public void setLegalCode(String legalCode) {
    this.legalCode = legalCode;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public Long getVersionDate() {
    return versionDate;
  }

  public void setVersionDate(Long versionDate) {
    this.versionDate = versionDate;
  }

  public Long getVersionNumber() {
    return versionNumber;
  }

  public void setVersionNumber(Long versionNumber) {
    this.versionNumber = versionNumber;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    MdmCompany that = (MdmCompany) o;

    if (!id.equals(that.id)) return false;
    if (!clientCompanyId.equals(that.clientCompanyId)) return false;
    if (!legalName.equals(that.legalName)) return false;
    return companyName.equals(that.companyName);

  }

  @Override
  public int hashCode() {
    int result = id.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "MdmCompany{" +
      "id=" + id +
      ", mdmIid=" + mdmId +
      ", clientCompanyId='" + clientCompanyId + '\'' +
      ", affiliatename='" + affiliatename + '\'' +
      ", countryName='" + countryName + '\'' +
      ", state='" + state + '\'' +
      ", city='" + city + '\'' +
      ", legalName='" + legalName + '\'' +
      ", legalCode='" + legalCode + '\'' +
      ", companyName='" + companyName + '\'' +
      '}';
  }
}
