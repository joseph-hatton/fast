package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A StructuredFinance.
 */
@Entity
@Table(name = "structured_finance")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "structuredfinance")
public class StructuredFinance extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "active_status")
    private Integer isActive;

    @Column(name = "reporting_category")
    private String reportingCategory;

    @Column(name = "reporting_req")
    private String reportingReq;

    @Column(name = "preparer")
    private String preparer;

    @Column(name = "reporting_ref_doc")
    private String rptRefDoc;

    @Column(name = "note_increase_doc")
    private String noteIncreaseDoc;

    @Column(name = "reporting_req_eff_date")
    private LocalDate rptReqEffDate;

    @Column(name = "comments")
    private String comments;

    @Column(name = "content_specifications")
    private String contentSpecifications;


    @Column(name = "file_name")
    private String fileName;

    @Column(name = "number_of_days")
    private Integer numberOfDays;

    @Column(name = "day_type")
    private String dayType;

    @Column(name = "period")
    private String period;

    @Column(name = "reporting_due_comment")
    private String reportingDueComment;

    @Column(name = "year_end_exception")
    private String yearEndException;

    @Column(name = "number_of_days_yr")
    private Integer numberOfDaysYr;

    @Column(name = "day_type_yr")
    private String dayTypeYr;

    @Column(name = "period_yr")
    private String periodYr;

    @Column(name = "reporting_due_comment_yr")
    private String reportingDueCommentYr;

    @ManyToOne
    private Deal deal;

    @ManyToOne
    private Frequency reportingFrequency;

    public Integer getIsActive() {
      return isActive;
    }

    public void setIsActive(Integer isActive) {
      this.isActive = isActive;
    }

    public Long getId() {
          return id;
      }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReportingCategory() {
      return reportingCategory;
    }

    public void setReportingCategory(String reportingCategory) {
      this.reportingCategory = reportingCategory;
    }

    public String getReportingReq() {
      return reportingReq;
    }

    public void setReportingReq(String reportingReq) {
      this.reportingReq = reportingReq;
    }

    public String getPreparer() {
      return preparer;
    }

    public void setPreparer(String preparer) {
      this.preparer = preparer;
    }

    public String getRptRefDoc() {
      return rptRefDoc;
    }

    public void setRptRefDoc(String rptRefDoc) {
      this.rptRefDoc = rptRefDoc;
    }

    public String getNoteIncreaseDoc() {
      return noteIncreaseDoc;
    }

    public void setNoteIncreaseDoc(String noteIncreaseDoc) {
      this.noteIncreaseDoc = noteIncreaseDoc;
    }

    public LocalDate getRptReqEffDate() {
      return rptReqEffDate;
    }

    public void setRptReqEffDate(LocalDate rptReqEffDate) {
      this.rptReqEffDate = rptReqEffDate;
    }

    public String getComments() {
      return comments;
    }

    public void setComments(String comments) {
      this.comments = comments;
    }

    public String getContentSpecifications() {
      return contentSpecifications;
    }

    public void setContentSpecifications(String contentSpecifications) {
      this.contentSpecifications = contentSpecifications;
    }

    public String getFileName() {
      return fileName;
    }

    public void setFileName(String fileName) {
      this.fileName = fileName;
    }

    public Integer getNumberOfDays() {
      return numberOfDays;
    }

    public void setNumberOfDays(Integer numberOfDays) {
      this.numberOfDays = numberOfDays;
    }

    public String getDayType() {
      return dayType;
    }

    public void setDayType(String dayType) {
      this.dayType = dayType;
    }

    public String getPeriod() {
      return period;
    }

    public void setPeriod(String period) {
      this.period = period;
    }

    public String getReportingDueComment() {
      return reportingDueComment;
    }

    public void setReportingDueComment(String reportingDueComment) {
      this.reportingDueComment = reportingDueComment;
    }

    public String getYearEndException() {
      return yearEndException;
    }

    public void setYearEndException(String yearEndException) {
      this.yearEndException = yearEndException;
    }

    public Integer getNumberOfDaysYr() {
      return numberOfDaysYr;
    }

    public void setNumberOfDaysYr(Integer numberOfDaysYr) {
      this.numberOfDaysYr = numberOfDaysYr;
    }

    public String getDayTypeYr() {
      return dayTypeYr;
    }

    public void setDayTypeYr(String dayTypeYr) {
      this.dayTypeYr = dayTypeYr;
    }

    public String getPeriodYr() {
      return periodYr;
    }

    public void setPeriodYr(String periodYr) {
      this.periodYr = periodYr;
    }

    public String getReportingDueCommentYr() {
      return reportingDueCommentYr;
    }

    public void setReportingDueCommentYr(String reportingDueCommentYr) {
      this.reportingDueCommentYr = reportingDueCommentYr;
    }

    public Deal getDeal() {
      return deal;
    }

    public void setDeal(Deal deal) {
      this.deal = deal;
    }

    public Frequency getReportingFrequency() {
      return reportingFrequency;
    }

    public void setReportingFrequency(Frequency reportingFrequency) {
      this.reportingFrequency = reportingFrequency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StructuredFinance structuredFinance = (StructuredFinance) o;
        if(structuredFinance.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, structuredFinance.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "StructuredFinance{" +
            "id=" + id +

            '}';
    }
}
