package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A RiskFee.
 */
@Entity
@Table(name = "risk_fee")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "riskfee")
public class RiskFee implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

    @Column(name = "rate")
    private BigDecimal rate;

    @Column(name = "treaty_id")
    private Long treatyId;

    @Column(name = "treaty_reference")
    private String treatyReference;

    @Column(name = "fee_period")
    private String period;

    @Column(name = "comments")
    private String comments;

    @Column(name = "minimum_risk_fee")
    private Integer minimumRiskFee;

    @Column(name = "minimum_risk_fee_amount")
    private BigDecimal minimumRiskFeeAmount;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JsonBackReference("riskFee")
    private TreatyManagement riskFee;

    @ManyToOne
    private Currency riskFeeCurrency;

    public Long getId() {
      return id;
    }

    public void setId(Long id) {
      this.id = id;
    }

  public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public Long getTreatyId() {
        return treatyId;
    }

    public void setTreatyId(Long treatyId) {
        this.treatyId = treatyId;
    }

    public String getTreatyReference() {
        return treatyReference;
    }

    public void setTreatyReference(String treatyReference) {
        this.treatyReference = treatyReference;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getMinimumRiskFee() {
      return minimumRiskFee;
    }

    public void setMinimumRiskFee( Integer  minimumRiskFee) {
      this.minimumRiskFee = minimumRiskFee;
    }

    public BigDecimal getMinimumRiskFeeAmount() {
      return minimumRiskFeeAmount;
    }

    public void setMinimumRiskFeeAmount(BigDecimal minimumRiskFeeAmount) {
      this.minimumRiskFeeAmount = minimumRiskFeeAmount;
    }

  public Currency getRiskFeeCurrency() {
    return riskFeeCurrency;
  }

  public void setRiskFeeCurrency(Currency riskFeeCurrency) {
    this.riskFeeCurrency = riskFeeCurrency;
  }

  @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RiskFee riskFee = (RiskFee) o;
        if(riskFee.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, riskFee.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }


  //using for reporting
    @Override
    public String toString() {
        return "RiskFee{" +
            "id=" + id +
            ", rate='" + rate + "'" +
            ", treatyId='" + treatyId + "'" +
            ", period='" + period + "'" +
            '}';
    }
}
