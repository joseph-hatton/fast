package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A SettlementType.
 */
@Entity
@Table(name = "settlement_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "settlementtype")
public class SettlementType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "sort_order")
    private Integer sortOrder;

    @Column(name = "default_flag")
    private String default_flag;

    @Column(name = "last_user")
    private String lastUser;

    @Column(name = "last_update")
    private LocalDate lastUpdate;

    @Column(name = "creation_ts")
    private LocalDate creationTS;

    @Column(name = "creation_user")
    private String creationUser;

    @Column(name = "eff_begin_ts")
    private LocalDate effBeginTs;

    @Column(name = "eff_end_ts")
    private LocalDate effEndTs;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getDefault_flag() {
        return default_flag;
    }

    public void setDefault_flag(String default_flag) {
        this.default_flag = default_flag;
    }

    public String getLastUser() {
        return lastUser;
    }

    public void setLastUser(String lastUser) {
        this.lastUser = lastUser;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public LocalDate getCreationTS() {
        return creationTS;
    }

    public void setCreationTS(LocalDate creationTS) {
        this.creationTS = creationTS;
    }

    public String getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    public LocalDate getEffBeginTs() {
        return effBeginTs;
    }

    public void setEffBeginTs(LocalDate effBeginTs) {
        this.effBeginTs = effBeginTs;
    }

    public LocalDate getEffEndTs() {
        return effEndTs;
    }

    public void setEffEndTs(LocalDate effEndTs) {
        this.effEndTs = effEndTs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SettlementType settlementType = (SettlementType) o;
        if(settlementType.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, settlementType.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SettlementType{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", description='" + description + "'" +
            ", sortOrder='" + sortOrder + "'" +
            ", default_flag='" + default_flag + "'" +
            ", lastUser='" + lastUser + "'" +
            ", lastUpdate='" + lastUpdate + "'" +
            ", creationTS='" + creationTS + "'" +
            ", creationUser='" + creationUser + "'" +
            ", effBeginTs='" + effBeginTs + "'" +
            ", effEndTs='" + effEndTs + "'" +
            '}';
    }
}
