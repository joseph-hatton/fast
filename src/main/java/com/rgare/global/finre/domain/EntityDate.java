package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A EntityDate.
 */
@Entity
@Table(name = "entity_date")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "entitydate")
public class EntityDate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "entity_type")
    private String entityType;

    @Column(name = "number_of_days")
    private Integer numberOfDays;

    @Column(name = "day_type")
    private String dayType;


    @Column(name = "period")
    private String period;

    @Column(name = "year_type")
    private String yearType;

    @Column(name = "comments")
    private String comments;

    @Column(name = "treaty_ref_comments")
    private String treatyRefComments;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JsonBackReference("entity")
    private Treaty entity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public Integer getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(Integer numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public String getDayType() {
        return dayType;
    }

    public void setDayType(String dayType) {
        this.dayType = dayType;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getYearType() {
        return yearType;
    }

    public void setYearType(String yearType) {
        this.yearType = yearType;
    }

    public String getComments() {
      return comments;
    }

    public void setComments(String comments) {
      this.comments = comments;
    }

    public String getTreatyRefComments() {
      return treatyRefComments;
    }

    public void setTreatyRefComments(String treatyRefComments) {
      this.treatyRefComments = treatyRefComments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EntityDate entityDate = (EntityDate) o;
        if(entityDate.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, entityDate.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }


    //using for reporting
    @Override
    public String toString() {
      try {
        return numberOfDays+" "+dayType+" "+period+" ";
      }catch(NullPointerException e1) {
       return "null";
      }
    }
}
