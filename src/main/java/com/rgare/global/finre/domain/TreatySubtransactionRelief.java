package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A TreatySubTransaction.
 */
@Entity
@Table(name = "treaty_relief_subtransaction")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "treatyreliefsubtransaction")
public class TreatySubtransactionRelief extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amount", precision=10, scale=2)
    private BigDecimal amount;

    @Column(name = "converted_amount", precision=10, scale=2)
    private BigDecimal convertedAmount;

    @Column(name = "assuming_type")
    private Integer assuming;

    @Column(name = "ultimate_type")
    private Integer ultimate;

    @Column(name = "interim_type")
    private Integer interim;

    @Column(name = "internal_type")
  private Integer internal;
    @Column(name = "order_by")
    private Integer orderBy;

  @Column(name = "external_retro_type")
  private Integer externalRetro;



    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JsonBackReference("treatyTransactionRelief")
    private TreatyTransactionRelief treatyTransactionRelief;

    @ManyToOne
    private Company company;

    public Long getId() {
          return id;
      }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getConvertedAmount() {
        return convertedAmount;
    }

    public void setConvertedAmount(BigDecimal convertedAmount) {
        this.convertedAmount = convertedAmount;
    }

    public Integer getAssuming() {
      return assuming;
    }

    public void setAssuming(Integer assuming) {
      this.assuming = assuming;
    }

    public Integer getUltimate() {
      return ultimate;
    }

    public void setUltimate(Integer ultimate) {
      this.ultimate = ultimate;
    }

    public Integer getInterim() {
      return interim;
    }

    public void setInterim(Integer interim) {
      this.interim = interim;
    }

    public TreatyTransactionRelief getTreatyTransactionRelief() {
          return treatyTransactionRelief;
        }

    public void setTreatyTransactionRelief(TreatyTransactionRelief treatyTransactionRelief) {
      this.treatyTransactionRelief = treatyTransactionRelief;
    }

    public Integer getExternalRetro() {
      return externalRetro;
    }

    public void setExternalRetro(Integer externalRetro) {
      this.externalRetro = externalRetro;
    }

    public Company getCompany() {
        return company;
      }

    public Integer getOrderBy() {
      return orderBy;
    }

    public void setOrderBy(Integer orderBy) {
      this.orderBy = orderBy;
    }

      public void setCompany(Company company) {
      this.company = company;
    }

      public Integer getInternal() {
        return internal;
      }

      public void setInternal(Integer internal) {
        this.internal = internal;
      }

  @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TreatySubtransactionRelief treatySubTransaction = (TreatySubtransactionRelief) o;
        if(treatySubTransaction.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, treatySubTransaction.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TreatySubtransactionRelief{" +
            "id=" + id +
            ", amount='" + amount + "'" +
            ", convertedAmount='" + convertedAmount + "'" +
            '}';
    }
}
