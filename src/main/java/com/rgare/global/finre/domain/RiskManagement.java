package com.rgare.global.finre.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A RiskManagement.
 */
@Entity
@Table(name = "risk_management")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "riskmanagement")
public class RiskManagement extends BaseUser  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    @Transient
    @JsonSerialize
    @JsonDeserialize
    private String uploadText;

    @Transient
    @JsonSerialize
    @JsonDeserialize
    private String statusMessage;

    @Column(name = "amount", precision=10, scale=2)
    private BigDecimal amount;

    @Column(name = "convertedAmount", precision=10, scale=2)
    private BigDecimal convertedAmount;

    @ManyToOne
    private Country country;

    @ManyToOne
    private Currency currency;

    @ManyToOne
    private RiskCriteria riskCriteria;

    @ManyToOne
    private Deal deal;

    @Column(name = "month_year")
    private LocalDate monthYear;


  public String getStatusMessage() {
    return statusMessage;
  }

  public void setStatusMessage(String statusMessage) {
    this.statusMessage = statusMessage;
  }

  public Currency getCurrency() {
    return currency;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }

  public void setId(Long id) {
        this.id = id;
    }

    public String getUploadText() {
    return uploadText;
    }

    public void setUploadText(String uploadText) {
    this.uploadText = uploadText;
    }

    public BigDecimal getAmount() {
    return amount;
    }

    public void setAmount(BigDecimal amount) {
    this.amount = amount;
    }

    public Country getCountry() {
    return country;
    }

    public void setCountry(Country country) {
    this.country = country;
    }

    public Deal getDeal() {
    return deal;
    }

    public void setDeal(Deal deal) {
    this.deal = deal;
    }

    public LocalDate getMonthYear() {
    return monthYear;
    }

    public void setMonthYear(LocalDate monthYear) {
    this.monthYear = monthYear;
    }

    public RiskCriteria getRiskCriteria() {
      return riskCriteria;
    }

    public void setRiskCriteria(RiskCriteria riskCriteria) {
      this.riskCriteria = riskCriteria;
    }

    public BigDecimal getConvertedAmount() {
      return convertedAmount;
    }

    public void setConvertedAmount(BigDecimal convertedAmount) {
      this.convertedAmount = convertedAmount;
    }

  @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RiskManagement riskManagement = (RiskManagement) o;
        if(riskManagement.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, riskManagement.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "RiskManagement{" +
            "id=" + id +
            '}';
    }
}
