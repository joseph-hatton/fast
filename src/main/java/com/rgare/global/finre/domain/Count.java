package com.rgare.global.finre.domain;

import java.io.Serializable;

public class Count implements Serializable {
  private Long amount;

  public Count(Long amount) {
    this.amount = amount;
  }

  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }
}
