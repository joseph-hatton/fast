package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A TreatyTransactionRelief.
 */
@Entity
@Table(name = "treaty_transaction_relief")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "treatytransactionrelief")
public class TreatyTransactionRelief extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "reporting_date")
    private LocalDate reportingDate;

    @Column(name = "transaction_status")
    private String transactionStatus;

    @Column(name = "net_total", precision=10, scale=2)
    private BigDecimal netTotal;

    @Column(name = "converted_net_total", precision=10, scale=2)
    private BigDecimal convertedNetTotal;

    @Column(name = "active_status")
    private Integer isActive;

    @OneToMany(orphanRemoval=true, fetch = FetchType.EAGER, mappedBy = "treatyTransactionRelief", cascade = { CascadeType.PERSIST, CascadeType.REMOVE,CascadeType.DETACH ,CascadeType.MERGE, CascadeType.REFRESH })
    @OrderBy("orderBy asc")
    @JsonManagedReference("treatyTransactionRelief")
    private List<TreatySubtransactionRelief> subTransactions = new ArrayList<>();

    @ManyToOne
    private Deal deal;

    @ManyToOne
    private Currency currency;

    @ManyToOne
    private Country country;

    public BigDecimal getConvertedNetTotal() {
      return convertedNetTotal;
    }

    public void setConvertedNetTotal(BigDecimal convertedNetTotal) {
      this.convertedNetTotal = convertedNetTotal;
    }

    public Long getId() {
          return id;
      }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getReportingDate() {
      return reportingDate;
    }

    public void setReportingDate(LocalDate reportingDate) {
      this.reportingDate = reportingDate;
    }

    public Country getCountry() {
      return country;
    }

    public void setCountry(Country country) {
      this.country = country;
    }

    public List<TreatySubtransactionRelief> getSubTransactions() {
        return subTransactions;
      }

    public void setSubTransactions(List<TreatySubtransactionRelief> subTransactions) {
      this.subTransactions = subTransactions;
    }

    public BigDecimal getNetTotal() {
      return netTotal;
    }

    public void setNetTotal(BigDecimal netTotal) {
      this.netTotal = netTotal;
    }

    public Currency getCurrency() {
      return currency;
    }

    public void setCurrency(Currency currency) {
      this.currency = currency;
    }

    public String getTransactionStatus() {
      return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
      this.transactionStatus = transactionStatus;
    }

    public Deal getDeal() {
      return deal;
    }

    public void setDeal(Deal deal) {
      this.deal = deal;
    }

    public Integer getIsActive() {
      return isActive;
    }

    public void setIsActive(Integer isActive) {
      this.isActive = isActive;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TreatyTransactionRelief treatyTransactionRelief = (TreatyTransactionRelief) o;
        if(treatyTransactionRelief.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, treatyTransactionRelief.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TreatyTransactionRelief{" +
            "id=" + id +
            ", reportingDate='" + reportingDate + "'" +
            ", country='" + country + "'" +
            ", netTotal='" + netTotal + "'" +
            ", convertedNetTotal='" + convertedNetTotal + "'" +
            ", isActive='" + isActive + "'" +
            '}';
    }
}
