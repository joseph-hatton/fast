package com.rgare.global.finre.domain;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDate;
import java.time.ZonedDateTime;

/**
 * Created by s0044514 on 12/22/2016.
 */
@MappedSuperclass
public class BaseUser {

  @Column(name = "creation_user")
  String creationUser;

  @Column(name = "last_user")
  String lastUser;

  @Column(name = "last_update")
  ZonedDateTime lastUpdate;

  @Column(name = "creation_ts")
  ZonedDateTime creationTs;

  public String getCreationUser() {
    return creationUser;
  }

  public void setCreationUser(String creationUser) {
    this.creationUser = creationUser;
  }

  public String getLastUser() {
    return lastUser;
  }

  public void setLastUser(String lastUser) {
    this.lastUser = lastUser;
  }

  public ZonedDateTime getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(ZonedDateTime lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  public ZonedDateTime getCreationTs() {
    return creationTs;
  }

  public void setCreationTs(ZonedDateTime creationTs) {
    this.creationTs = creationTs;
  }
}
