package com.rgare.global.finre.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Reports.
 */
@Entity
@Table(name = "reports")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "reports")
public class Reports implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "report_link")
    private String reportLink;

    @Column(name = "report_level")
    private String reportLevel;

    @Column(name = "tableau_link")
    private String tableautLink;

    @Column(name = "flag_format")
    private Integer flagFormat;

    @Column(name = "description")
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Reports name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReportLink() {
        return reportLink;
    }

    public Reports reportLink(String reportLink) {
        this.reportLink = reportLink;
        return this;
    }

  public String getTableautLink() {
    return tableautLink;
  }

  public void setTableautLink(String tableautLink) {
    this.tableautLink = tableautLink;
  }

  public Integer getFlagFormat() {
    return flagFormat;
  }

  public void setFlagFormat(Integer flagFormat) {
    this.flagFormat = flagFormat;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setReportLink(String reportLink) {
        this.reportLink = reportLink;
    }

  public String getReportLevel() {
    return reportLevel;
  }

  public void setReportLevel(String reportLevel) {
    this.reportLevel = reportLevel;
  }

  @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Reports reports = (Reports) o;
        if(reports.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, reports.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Reports{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", reportLink='" + reportLink + "'" +
            '}';
    }
}
