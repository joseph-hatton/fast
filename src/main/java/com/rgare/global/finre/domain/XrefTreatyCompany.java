package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A XrefTreatyCompany.
 */
@Entity
@Table(name = "xref_treaty_company")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "xreftreatycompany")
public class XrefTreatyCompany extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "treaty_id")
    private Long treatyId;

    @Column(name = "company_type")
    private String companyType;

    @ManyToOne
    private Company company;

    public Long getId() {
          return id;
      }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTreatyId() {
        return treatyId;
    }

    public void setTreatyId(Long treatyId) {
        this.treatyId = treatyId;
    }

    public String getCompanyType() {
      return companyType;
    }

    public void setCompanyType(String companyType) {
      this.companyType = companyType;
    }

    public Company getCompany() {
      return company;
    }

    public void setCompany(Company company) {
      this.company = company;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        XrefTreatyCompany xrefTreatyCompany = (XrefTreatyCompany) o;
        if(xrefTreatyCompany.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, xrefTreatyCompany.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "XrefTreatyCompany{" +
            "id=" + id +
            ", treatyId='" + treatyId + "'" +
            '}';
    }
}
