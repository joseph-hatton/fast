package com.rgare.global.finre.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A TreatyTracking.
 */
@Entity
@Table(name = "treaty_tracking")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "treatytracking")
public class TreatyTracking extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "calculated_reporting_due_date")
    private LocalDate calculatedReportingDueDate;

    @Column(name = "calculated_seriatim_due_date")
    private LocalDate calculatedSeriatimDueDate;

    @Column(name = "calculated_reporting_due_yex")
    private LocalDate calculatedReportingDueYex;

    @Column(name = "calculated_assuming_grace_date")
    private LocalDate calculatedAssumingGraceDate;

    @Column(name = "calculated_assuming_date")
    private LocalDate calculatedAssumingDate;

    @Column(name = "calculated_ceding_date")
    private LocalDate calculatedCedingDate;

    @Column(name = "calculated_ceding_grace_date")
    private LocalDate calculatedCedingGraceDate;

    @Column(name = "reporting_received_date")
    private LocalDate reportingReceivedDate;

    @Column(name = "cash_upload_date")
    private LocalDate cashUploadDate;

    @Column(name = "active_status")
    private Integer isActive;

    @Column(name = "flag_email_rec")
    private Integer flagEmailRec;

    @Column(name = "flag_email")
    private Integer flagEmail;

    @Column(name = "reporting_month_year")
    private LocalDate reportingMonthYear;

    @Column(name = "comments")
    private String comments;

    @Column(name = "settlement_due")
    private String settlementDue;

    @Column(name = "tracking_status")
    private String trackingStatus;

    @Column(name = "risk_mgmt_date")
    private LocalDate riskMgmtDate;

    @Column(name = "email_date")
    private LocalDate emailDate;

    @Column(name = "broker_fee_due_date")
    private LocalDate brokerFeeDueDate;

    @Column(name = "confirmation_received_date")
    private LocalDate confirmationReceivedDate;

    @Column(name = "cash_sent_rec_date")
    private LocalDate cashSentRecDate;

    @Column(name = "reporting_period")
    private String reportingPeriod;

    @Column(name = "auto_generate_flag")
    private Integer autoGenerateFlag;

    @Column(name = "cash_due_flag")
    private String cashDueFlag;

    @Column(name = "cash_due_type")
    private String cashDueType;

    @Column(name = "report_notif_flag")
    private Integer reportNotifFlag;

    @Column(name = "report_notif_reason")
    private String reportNotifReason;



  @Column(name = "seriatim_received_date")
    private LocalDate seriatimReceivedDate;

    @ManyToOne
    private Currency currency;

    @ManyToOne
    private Treaty treaty;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getFlagEmailRec() {
      return flagEmailRec;
    }

    public void setFlagEmailRec(Integer flagEmailRec) {
      this.flagEmailRec = flagEmailRec;
    }

    public LocalDate getCalculatedReportingDueDate() {
      return calculatedReportingDueDate;
    }

    public void setCalculatedReportingDueDate(LocalDate calculatedReportingDueDate) {
      this.calculatedReportingDueDate = calculatedReportingDueDate;
    }

    public LocalDate getCalculatedAssumingGraceDate() {
      return calculatedAssumingGraceDate;
    }

    public void setCalculatedAssumingGraceDate(LocalDate calculatedAssumingGraceDate) {
      this.calculatedAssumingGraceDate = calculatedAssumingGraceDate;
    }

    public LocalDate getCalculatedCedingGraceDate() {
      return calculatedCedingGraceDate;
    }

    public void setCalculatedCedingGraceDate(LocalDate calculatedCedingGraceDate) {
      this.calculatedCedingGraceDate = calculatedCedingGraceDate;
    }

    public LocalDate getCalculatedSeriatimDueDate() {
      return calculatedSeriatimDueDate;
    }

    public void setCalculatedSeriatimDueDate(LocalDate calculatedSeriatimDueDate) {
      this.calculatedSeriatimDueDate = calculatedSeriatimDueDate;
    }

    public LocalDate getReportingReceivedDate() {
            return reportingReceivedDate;
        }

    public void setReportingReceivedDate(LocalDate reportingReceivedDate) {
        this.reportingReceivedDate = reportingReceivedDate;
    }

    public LocalDate getCalculatedReportingDueYex() {
      return calculatedReportingDueYex;
    }

    public void setCalculatedReportingDueYex(LocalDate calculatedReportingDueYex) {
      this.calculatedReportingDueYex = calculatedReportingDueYex;
    }

    public Integer getAutoGenerateFlag() {
      return autoGenerateFlag;
    }

    public void setAutoGenerateFlag(Integer autoGenerateFlag) {
      this.autoGenerateFlag = autoGenerateFlag;
    }

    public LocalDate getCashUploadDate() {
        return cashUploadDate;
    }

    public void setCashUploadDate(LocalDate cashUploadDate) {
        this.cashUploadDate = cashUploadDate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getReportingPeriod() {
      return reportingPeriod;
    }

    public void setReportingPeriod(String reportingPeriod) {
      this.reportingPeriod = reportingPeriod;
    }

    public LocalDate getRiskMgmtDate() {
          return riskMgmtDate;
      }

    public void setRiskMgmtDate(LocalDate riskMgmtDate) {
        this.riskMgmtDate = riskMgmtDate;
    }

    public LocalDate getEmailDate() {
        return emailDate;
    }

    public void setEmailDate(LocalDate emailDate) {
        this.emailDate = emailDate;
    }
    public Treaty getTreaty() {
        return treaty;
    }

    public void setTreaty(Treaty treaty) {
        this.treaty = treaty;
    }

    public Currency getCurrency() {
      return currency;
    }

    public void setCurrency(Currency currency) {
      this.currency = currency;
    }

    public LocalDate getConfirmationReceivedDate() {
      return confirmationReceivedDate;
    }

    public void setConfirmationReceivedDate(LocalDate confirmationReceivedDate) {
      this.confirmationReceivedDate = confirmationReceivedDate;
    }

    public String getTrackingStatus() {
      return trackingStatus;
    }

    public void setTrackingStatus(String trackingStatus) {
      this.trackingStatus = trackingStatus;
    }

    public LocalDate getCashSentRecDate() {
      return cashSentRecDate;
    }

    public void setCashSentRecDate(LocalDate cashSentRecDate) {
      this.cashSentRecDate = cashSentRecDate;
    }

    public LocalDate getReportingMonthYear() {
      return reportingMonthYear;
    }

    public void setReportingMonthYear(LocalDate reportingMonthYear) {
      this.reportingMonthYear = reportingMonthYear;
    }

    public String getSettlementDue() {
      return settlementDue;
    }

    public void setSettlementDue(String settlementDue) {
      this.settlementDue = settlementDue;
    }

    public Integer getFlagEmail() {
      return flagEmail;
    }

    public void setFlagEmail(Integer flagEmail) {
      this.flagEmail = flagEmail;
    }


    public LocalDate getBrokerFeeDueDate() {
      return brokerFeeDueDate;
    }

    public void setBrokerFeeDueDate(LocalDate brokerFeeDueDate) {
      this.brokerFeeDueDate = brokerFeeDueDate;
    }

    public Integer getIsActive() {
      return isActive;
    }

    public void setIsActive(Integer isActive) {
      this.isActive = isActive;
    }

    public String getCashDueFlag() {
      return cashDueFlag;
    }

    public void setCashDueFlag(String cashDueFlag) {
      this.cashDueFlag = cashDueFlag;
    }

    public String getCashDueType() {
      return cashDueType;
    }

    public void setCashDueType(String cashDueType) {
      this.cashDueType = cashDueType;
    }

    public LocalDate getSeriatimReceivedDate() {
      return seriatimReceivedDate;
    }

    public void setSeriatimReceivedDate(LocalDate seriatimReceivedDate) {
      this.seriatimReceivedDate = seriatimReceivedDate;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TreatyTracking treatyTracking = (TreatyTracking) o;
        if(treatyTracking.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, treatyTracking.id);
    }

    public LocalDate getCalculatedAssumingDate() {
      return calculatedAssumingDate;
    }

    public void setCalculatedAssumingDate(LocalDate calculatedAssumingDate) {
      this.calculatedAssumingDate = calculatedAssumingDate;
    }

    public Integer getReportNotifFlag() {
      return reportNotifFlag;
    }

    public void setReportNotifFlag(Integer reportNotifFlag) {
      this.reportNotifFlag = reportNotifFlag;
    }
    
    public String getReportNotifReason() {
      return reportNotifReason;
    }

    public void setReportNotifReason(String reportNotifReason) {
      this.reportNotifReason = reportNotifReason;
    }

  @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TreatyTracking{" +
            "id=" + id +
            ", calculatedReportingDueDate='" + calculatedReportingDueDate + "'" +
            ", reportingReceivedDate='" + reportingReceivedDate + "'" +
            ", cashUploadDate='" + cashUploadDate + "'" +
            ", comments='" + comments + "'" +
            ", riskMgmtDate='" + riskMgmtDate + "'" +
            ", emailDate='" + emailDate + "'" +
            ", confirmationReceivedDate='" + confirmationReceivedDate + "'" +
            ", cashSentRecDate='" + cashSentRecDate + "'" +
            ", trackingStatus='" + trackingStatus + "'" +
              '}';
    }
}
