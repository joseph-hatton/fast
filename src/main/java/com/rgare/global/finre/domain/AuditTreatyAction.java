package com.rgare.global.finre.domain;

/**
 * Created by s0044514 on 1/17/2017.
 */
public enum AuditTreatyAction {
    UPDATE,
    DELETE,
    SAVE;
}
