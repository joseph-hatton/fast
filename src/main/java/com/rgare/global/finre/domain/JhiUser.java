package com.rgare.global.finre.domain;

import com.rgare.global.finre.domain.JhiUserAuthority;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.time.LocalDateTime;

/**
 * A jhi_user.
 */
@Entity
@Table(name = "jhi_user")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class JhiUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "login")
    private String login;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "activated")
    private Integer activated;
    
    @Column(name = "lang_key")
    private String langKey;
    
    @Column(name = "last_user")
    private String lastUser;

    @Column(name = "last_update")
    private LocalDateTime lastUpdate;

    @Column(name = "creation_ts")
    private LocalDateTime creationTs;

    @Column(name = "creation_user")
    private String creationUser;

    @OneToMany(orphanRemoval=true, fetch = FetchType.EAGER, mappedBy = "jhiUser", cascade = { CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE, CascadeType.REFRESH })
    @JsonManagedReference
    // @ElementCollection
    // @CollectionTable(name="jhi_user_authority", joinColumns=@JoinColumn(name="user_id"))
    // @Column(name="authority_name")
    private List<JhiUserAuthority> auths;

    public List<JhiUserAuthority> getAuths() {
        return auths;
    }

    public void setAuths(List<JhiUserAuthority> auths) {
        this.auths = auths;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setlogin(String login) {
        this.login = login;
    }
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public Integer getActivated() {
        return activated;
    }

    public void setActivated(Integer activated) {
        this.activated = activated;
    }
    
    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public String getLastUser() {
        return lastUser;
    }
    
    public void setLastUser(String lastUser) {
        this.lastUser = lastUser;
    }
    
    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }
    
    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    
    public LocalDateTime getCreationTs() {
        return creationTs;
    }
    
    public void setCreationTs(LocalDateTime creationTs) {
        this.creationTs = creationTs;
    }
    
    public String getCreationUser() {
        return creationUser;
    }
    
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }
}
