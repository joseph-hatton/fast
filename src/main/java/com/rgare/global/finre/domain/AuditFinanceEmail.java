package com.rgare.global.finre.domain;

//import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;


/**
 * Created by s0044514 on 1/12/2017.
 */

@Entity
@Table(name = "audit_finance_email")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "auditFinanceEmail")

public class AuditFinanceEmail implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "comments")
    private String comments;

    @Column(name = "creation_user")
    String creationUser;

    @Column(name = "creation_ts")
    ZonedDateTime creationTs;

    public Long getId() {
    return id;
    }

    public void setId(Long id) {
    this.id = id;
    }

    public String getComments() {
    return comments;
    }

    public void setComments(String comments) {
    this.comments = comments;
    }

    public String getCreationUser() {
    return creationUser;
    }

    public void setCreationUser(String creationUser) {
    this.creationUser = creationUser;
    }

    public ZonedDateTime getCreationTs() {
    return creationTs;
    }

    public void setCreationTs(ZonedDateTime creationTs) {
    this.creationTs = creationTs;
    }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AuditFinanceEmail auditFinanceEmail = (AuditFinanceEmail) o;
    if(auditFinanceEmail.id == null || id == null) {
      return false;
    }
    return Objects.equals(id, auditFinanceEmail.id);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id);
  }

  @Override
  public String toString() {
    return "AuditFinanceEmail{" +
      "id=" + id +
      ", comments='" + comments + "'" +
      ", creationUser='" + creationUser + "'" +
      ", creationTs='" + creationTs + "'" +
      '}';
  }
}

