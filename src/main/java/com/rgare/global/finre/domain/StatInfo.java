package com.rgare.global.finre.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * A statInfo.
 */
@Entity
@Table(name = "stat_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "statinfo")
public class StatInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "page_title")
    private String title;

    @Column(name = "last_updated")
    private LocalDateTime lastUpdated;

    @Column(name = "last_updated_count")
    private Integer lastUpdatedCount;

    @Column(name = "active_status")
    private Integer isActive;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Integer getLastUpdatedCount() {
        return lastUpdatedCount;
    }

    public void setLastUpdatedCount(Integer lastUpdatedCount) {
        this.lastUpdatedCount = lastUpdatedCount;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }
}
