package com.rgare.global.finre.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.ManyToAny;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A StructuredTracking.
 */
@Entity
@Table(name = "structured_tracking")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "structuredtracking")
public class StructuredTracking extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "active_status")
    private Integer isActive;


    @Column(name = "client_reporting_date")
    private LocalDate clientReportingDate;

    @Column(name = "calc_reporting_due_date")
    private LocalDate calcReportingDueDate;

    @Column(name = "calc_reporting_due_yex")
    private LocalDate calcReportingDueYex;

    @Column(name = "reporting_receipt_date")
    private LocalDate reportingReceiptDate;

    @Column(name = "auto_generate_flag")
    private Integer autoGenerateFlag;

    @Column(name = "comments")
    private String comments;

    @Column(name = "tracking_status")
    private String trackingStatus;

    @ManyToOne
    private Deal deal;

    @ManyToOne
    private StructuredFinance structuredFinance;

    public Integer getIsActive() {
      return isActive;
    }

    public void setIsActive(Integer isActive) {
      this.isActive = isActive;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getClientReportingDate() {
        return clientReportingDate;
    }

    public void setClientReportingDate(LocalDate clientReportingDate) {
        this.clientReportingDate = clientReportingDate;
    }

    public LocalDate getReportingReceiptDate() {
        return reportingReceiptDate;
    }

    public void setReportingReceiptDate(LocalDate reportingReceiptDate) {
        this.reportingReceiptDate = reportingReceiptDate;
    }

    public Integer getAutoGenerateFlag() {
      return autoGenerateFlag;
    }

    public void setAutoGenerateFlag(Integer autoGenerateFlag) {
      this.autoGenerateFlag = autoGenerateFlag;
    }

    public String getComments() {
          return comments;
      }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Deal getDeal() {
      return deal;
    }

    public void setDeal(Deal deal) {
      this.deal = deal;
    }

    public StructuredFinance getStructuredFinance() {
      return structuredFinance;
    }

    public void setStructuredFinance(StructuredFinance structuredFinance) {
      this.structuredFinance = structuredFinance;
    }

    public String getTrackingStatus() {
      return trackingStatus;
    }

    public void setTrackingStatus(String trackingStatus) {
      this.trackingStatus = trackingStatus;
    }

    public LocalDate getCalcReportingDueDate() {
      return calcReportingDueDate;
    }

    public void setCalcReportingDueDate(LocalDate calcReportingDueDate) {
      this.calcReportingDueDate = calcReportingDueDate;
    }

    public LocalDate getCalcReportingDueYex() {
      return calcReportingDueYex;
    }

    public void setCalcReportingDueYex(LocalDate calcReportingDueYex) {
      this.calcReportingDueYex = calcReportingDueYex;
    }

  @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StructuredTracking structuredTracking = (StructuredTracking) o;
        if(structuredTracking.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, structuredTracking.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "StructuredTracking{" +
            "id=" + id +
            ", clientReportingDate='" + clientReportingDate + "'" +
            ", reportingReceiptDate='" + reportingReceiptDate + "'" +
            ", trackingStatus='" + trackingStatus + "'" +
            ", comments='" + comments + "'" +
            '}';
    }
}
