package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A TreatyTransaction.
 */
@Entity
@Table(name = "treaty_transaction_cash")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "treatytransactioncash")
public class TreatyTransactionCash extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

    @Column(name = "settlement_date")
    private LocalDate settlementDate;

    @Column(name = "net_total", precision=10, scale=2)
    private BigDecimal netTotal;

    @Column(name = "converted_net_total", precision=10, scale=2)
    private BigDecimal convertedNetTotal;

    @Column(name = "active_status")
    private Integer isActive;

    @Column(name = "finance_notification")
    private Integer financeNotification;

    @Column(name = "mix_transactions")
    private Integer mixTransactions;

    @Column(name = "accounting_date")
    private LocalDate accountingDate;

    @Column(name = "delete_reason")
    private String deleteReason;

    @Column(name = "delete_notification")
    private Integer deleteNotification;

    @Column(name = "alter_converted_user")
    private String alterConvertedUser;

    @Column(name = "alter_converted_date")
    private LocalDate alterConvertedDate;

    @Column(name = "currency_converted_flag")
    private Integer currencyConvertedFlag;

    @Column(name = "original_converted_amount", precision=10, scale=2)
    private BigDecimal originalConvertedAmount;

    @Column(name = "contacts")
    private String contacts;


    @Transient
    @JsonSerialize
    @JsonDeserialize
    private String sender;


    @Transient
    @JsonSerialize
    @JsonDeserialize
    private String subject;

    @Transient
    @JsonSerialize
    @JsonDeserialize
    private String emailComment;

    @Transient
    @JsonSerialize
    @JsonDeserialize
    private String fileContent;

    @Transient
    @JsonSerialize
    @JsonDeserialize
    private String fileName;

    @Transient
    @JsonSerialize
    @JsonDeserialize
    private String fileType;

    @OneToMany(orphanRemoval=true, fetch = FetchType.EAGER, mappedBy = "treatyTransactionCash", cascade = { CascadeType.PERSIST, CascadeType.REMOVE,CascadeType.DETACH ,CascadeType.MERGE, CascadeType.REFRESH })
    @JsonManagedReference("treatyTransactionCash")
    private Set<TreatySubtransactionCash> subTransactions = new HashSet<>();

    @ManyToOne
    private Treaty treaty;

    @ManyToOne
    private Currency currency;

    @ManyToOne
    private FundingType fundingType;

    @ManyToOne
    private Company company;

    @ManyToOne
    private TransactionStatus transactionStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(LocalDate settlementDate) {
        this.settlementDate = settlementDate;
    }

    public BigDecimal getNetTotal() {
        return netTotal;
    }

    public void setNetTotal(BigDecimal netTotal) {
        this.netTotal = netTotal;
    }

    public BigDecimal getConvertedNetTotal() {
        return convertedNetTotal;
    }

    public void setConvertedNetTotal(BigDecimal convertedNetTotal) {
        this.convertedNetTotal = convertedNetTotal;
    }

    public Treaty getTreaty() {
        return treaty;
    }

    public void setTreaty(Treaty treaty) {
        this.treaty = treaty;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public FundingType getFundingType() {
        return fundingType;
    }

    public void setFundingType(FundingType fundingType) {
        this.fundingType = fundingType;
    }

    public Set<TreatySubtransactionCash> getSubTransactions() {
        return subTransactions;
    }

    public void setSubTransactions(Set<TreatySubtransactionCash> subTransactions) {
        this.subTransactions = subTransactions;
    }

    public Integer getCurrencyConvertedFlag() {
        return currencyConvertedFlag;
    }

    public void setCurrencyConvertedFlag(Integer currencyConvertedFlag) {
        this.currencyConvertedFlag = currencyConvertedFlag;
    }

    public Integer getFinanceNotification() {
        return financeNotification;
    }

    public void setFinanceNotification(Integer financeNotification) {
        this.financeNotification = financeNotification;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public TransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public LocalDate getAccountingDate() {
        return accountingDate;
    }

    public void setAccountingDate(LocalDate accountingDate) {
        this.accountingDate = accountingDate;
    }

    public Integer getMixTransactions() {
        return mixTransactions;
    }

    public void setMixTransactions(Integer mixTransactions) {
        this.mixTransactions = mixTransactions;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getAlterConvertedUser() {
        return alterConvertedUser;
    }

    public void setAlterConvertedUser(String alterConvertedUser) {
        this.alterConvertedUser = alterConvertedUser;
    }

    public LocalDate getAlterConvertedDate() {
        return alterConvertedDate;
    }

    public void setAlterConvertedDate(LocalDate alterConvertedDate) {
        this.alterConvertedDate = alterConvertedDate;
    }

    public BigDecimal getOriginalConvertedAmount() {
        return originalConvertedAmount;
    }

    public void setOriginalConvertedAmount(BigDecimal originalConvertedAmount) {
        this.originalConvertedAmount = originalConvertedAmount;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getEmailComment() {
        return emailComment;
    }

    public void setEmailComment(String emailComment) {
        this.emailComment = emailComment;
    }

    public String getFileContent() {
      return fileContent;
    }

    public void setFileContent(String fileContent) {
      this.fileContent = fileContent;
    }

    public String getFileName() {
      return fileName;
    }

    public void setFileName(String fileName) {
      this.fileName = fileName;
    }

    public String getFileType() {
      return fileType;
    }

    public void setFileType(String fileType) {
      this.fileType = fileType;
    }

    public String getDeleteReason() {
      return deleteReason;
    }

    public void setDeleteReason(String deleteReason) {
      this.deleteReason = deleteReason;
    }

    public Integer getDeleteNotification() {
      return deleteNotification;
    }

    public void setDeleteNotification(Integer deleteNotification) {
    this.deleteNotification = deleteNotification;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TreatyTransactionCash treatyTransaction = (TreatyTransactionCash) o;
        if(treatyTransaction.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, treatyTransaction.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
  //using for reporting
    @Override
    public String toString() {
        return "TreatyTransactionCash{" +
            "id=" + id +
            ", originalConvertedAmount='" + originalConvertedAmount + "'" +
            ", mixTransactions='" + mixTransactions + "'" +
            ", convertedNetTotal='" + convertedNetTotal + "'" +
            ", isActive='" + isActive + "'" +
            ", lastUser='" + lastUser + "'" +
            ", lastUpdate='" + lastUpdate + "'" +
            ", transactionStatus='" + transactionStatus.getName() + "'" +
            ", contacts='" + contacts + "'" +
            '}';
    }
}
