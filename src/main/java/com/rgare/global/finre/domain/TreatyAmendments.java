package com.rgare.global.finre.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A TreatyAmendments.
 */
@Entity
@Table(name = "treaty_amendments")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "treatyamendments")
public class TreatyAmendments extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amendment_name")
    private String amendmentName;

    @Column(name = "effective_date")
    private LocalDate effectiveDate;

    @Column(name = "comments")
    private String comments;

    @Column(name = "reason")
    private String reason;

    @Column(name = "active_status")
    private Integer isActive;

    @ManyToOne
    private Treaty treaty;

    @ManyToOne
    private NotificationStatus notificationStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAmendmentName() {
        return amendmentName;
    }

    public void setAmendmentName(String amendmentName) {
        this.amendmentName = amendmentName;
    }

    public LocalDate getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(LocalDate effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Treaty getTreaty() {
        return treaty;
    }

    public void setTreaty(Treaty treaty) {
        this.treaty = treaty;
    }

    public NotificationStatus getNotificationStatus() {
    return notificationStatus;
  }

    public void setNotificationStatus(NotificationStatus notificationStatus) {
      this.notificationStatus = notificationStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TreatyAmendments treatyAmendments = (TreatyAmendments) o;
        if(treatyAmendments.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, treatyAmendments.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TreatyAmendments{" +
            "id=" + id +
            ", amendmentName='" + amendmentName + "'" +
            ", effectiveDate='" + effectiveDate + "'" +
            ", comments='" + comments + "'" +
            ", reason='" + reason + "'" +
            ", isActive='" + isActive + "'" +
            '}';
    }
}
