package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Treaty.
 */
@Entity
@Table(name = "treaty")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "treaty")
public class Treaty extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "period")
    private String period;

    @Column(name = "broker")
    private String broker;

    @Column(name = "effective_date")
    private LocalDate effectiveDate;

    @Column(name = "termination_date")
    private LocalDate terminationDate;

    @Column(name = "reason")
    private String reason;

    @Column(name = "copy_treaty_msg")
    private String copyTreatyMsg;

    @Column(name = "stmnt_ced_month_flag")
    private String stmntCedMonthFlag;

    @Column(name = "stmnt_asm_month_flag")
    private String smntAsmMonthFlag;

    @Column(name = "rpt_due_month_flag")
    private String rptDueMonthFlag;

    @Column(name = "notification")
    private String notification;

    @Column(name = "broker_fee_amount", precision=10, scale=2)
    private BigDecimal brokerFeeAmount;

    @Column(name = "broker_fee_percentage")
    private BigDecimal brokerFeePercentage;

    @Column(name = "active_status")
    private Integer isActive;

    @Column(name = "ek_notification_flag")
    private Integer ekNotificationFlag;

    @Column(name = "notification_status")
    private Integer notificationStatus;

    @Column(name = "comments")
    private String comments;

    @Column(name = "treaty_reference")
    private String treatyReference;

    @Column(name = "treaty_name")
    private String treatyName;

    @Column(name = "grace_period_comments")
    private String gracePeriodComments;

    @Column(name = "late_payment")
    private String latePayment;

    @Column(name = "ceding_flag")
    private String cedingFlag;

    @Column(name = "input_status")
    private String inputStatus;

    @Column(name = "broker_fee_switch_flag")
    private String brokerFeeSwitchFlag;

    @Column(name = "min_broker_fee_flag")
    private String minBrokerFeeFlag;

   @Column(name = "gp_tr_ceding")
    private String gpTrCeding;

    @Column(name = "gp_cmmts_ceding")
    private String gpCmmtsCeding;

    @Column(name = "treaty_ref_int_plty_comment")
    private String treatyRefIntPltyComment;

    @Column(name = "int_penalty_comment")
    private String intPenaltyComment;

    @Column(name = "currency_comments")
    private String currencyComments;

    @Column(name = "reporting_product_comment")
    private String reportingProductCmmnt;

    @Column(name = "year_end_exception")
    private String yearEndException;

    @Column(name = "mdm_id")
    private Long mdmId;

    @Column(name = "tracking_disabled_flag")
    private Integer disabledTracking;

    @OneToMany(orphanRemoval=true, fetch = FetchType.EAGER, mappedBy = "treatyCurrency", cascade = { CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE, CascadeType.REFRESH })
    @JsonManagedReference("treatyCurrency")
    private Set<TreatyCurrency> treatyCurrencies = new HashSet<>();

    @OneToMany(orphanRemoval=true, fetch = FetchType.EAGER, mappedBy = "mdmTreaty", cascade = { CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE, CascadeType.REFRESH })
    @JsonManagedReference("mdmTreaty")
    private Set<MdmAttributes> mdmAttributes = new HashSet<>();

    @ManyToOne
    private Currency brokerCurrency;


    @ManyToOne
    private Deal deal;

    @ManyToOne
    private ProductStructure productStructure;

    @ManyToOne
    private AgreementType agreementType;

    @ManyToOne
    private Frequency settlementFrequency;

    @ManyToOne
    private Frequency reportingFrequency;

    @ManyToOne
    private Frequency stmntFrqncCeding;

    @ManyToOne
    private Frequency stmntPastdueFrqncCeding;

    @ManyToOne
    private TreatyStatus treatyStatus;

    @ManyToOne
    private Frequency pastDueFrequency;

    @ManyToOne
    private Frequency reportingFrequencyMgmt;

    @OneToMany(orphanRemoval=true, fetch = FetchType.EAGER, mappedBy = "treaty", cascade = { CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE, CascadeType.REFRESH })
    @JsonManagedReference("treaty")
    private Set<TreatySeriatimType> seriatimTypeTreaties = new HashSet<>();

    public Integer getEkNotificationFlag() {
      return ekNotificationFlag;
    }

    public void setEkNotificationFlag(Integer ekNotificationFlag) {
      this.ekNotificationFlag = ekNotificationFlag;
    }

  @OneToMany(orphanRemoval=true, fetch = FetchType.EAGER, mappedBy = "treaty", cascade = { CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE, CascadeType.REFRESH })
    @JsonManagedReference("treaty")
    private Set<YearEndRegulatory> yearEndRegulatoryList = new HashSet<>();

    @ManyToOne
    private TreatySeriatim treatySeriatim;

    @OneToOne(orphanRemoval=true,cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    private EntityDate reportingDueDate;

    @OneToOne(orphanRemoval=true,cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    private EntityDate reportingDueYearEx;

    @OneToOne(orphanRemoval=true,cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    private EntityDate reportingDueQuarterEx;

    @OneToOne(orphanRemoval=true,cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    private EntityDate stmntDueCedQuarterEx;

    @OneToOne(orphanRemoval=true,cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    private EntityDate stmntDueCedGracePeriod;

    @OneToOne(orphanRemoval=true,cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    private EntityDate stmntDueCed;

    @OneToOne(orphanRemoval=true,cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    private EntityDate stmntDueAsmQuarterEx;

    @OneToOne(orphanRemoval=true,cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    private EntityDate stmntDueAsmGracePeriod;

    @OneToOne(orphanRemoval=true,cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    private EntityDate stmntDueAsm;

    @OneToOne(orphanRemoval=true,cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    private EntityDate stmntDueAsmBroker;

    public Long getId() {
          return id;
      }

    public void setId(Long id) {
        this.id = id;
    }

    public Currency getBrokerCurrency() {
      return brokerCurrency;
    }

    public void setBrokerCurrency(Currency brokerCurrency) {
      this.brokerCurrency = brokerCurrency;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getBroker() {
        return broker;
    }

    public void setBroker(String broker) {
        this.broker = broker;
    }

    public LocalDate getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(LocalDate effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public AgreementType getAgreementType() {
      return agreementType;
    }

    public void setAgreementType(AgreementType agreementType) {
      this.agreementType = agreementType;
    }

    public String getStmntCedMonthFlag() {
      return stmntCedMonthFlag;
    }

    public void setStmntCedMonthFlag(String stmntCedMonthFlag) {
      this.stmntCedMonthFlag = stmntCedMonthFlag;
    }

    public String getSmntAsmMonthFlag() {
      return smntAsmMonthFlag;
    }

    public void setSmntAsmMonthFlag(String smntAsmMonthFlag) {
      this.smntAsmMonthFlag = smntAsmMonthFlag;
    }

    public String getRptDueMonthFlag() {
      return rptDueMonthFlag;
    }

    public void setRptDueMonthFlag(String rptDueMonthFlag) {
      this.rptDueMonthFlag = rptDueMonthFlag;
    }

    public static long getSerialVersionUID() {
          return serialVersionUID;
      }

    public Frequency getSettlementFrequency() {
      return settlementFrequency;
    }

    public void setSettlementFrequency(Frequency settlementFrequency) {
      this.settlementFrequency = settlementFrequency;
    }

    public Frequency getReportingFrequency() {
      return reportingFrequency;
    }

    public void setReportingFrequency(Frequency reportingFrequency) {
      this.reportingFrequency = reportingFrequency;
    }

    public Deal getDeal() {
        return deal;
    }

    public void setDeal(Deal deal) {
        this.deal = deal;
    }

    public ProductStructure getProductStructure() {
      return productStructure;
    }

    public void setProductStructure(ProductStructure productStructure) {
      this.productStructure = productStructure;
    }

    public TreatyStatus getTreatyStatus() {
      return treatyStatus;
    }

    public void setTreatyStatus(TreatyStatus treatyStatus) {
      this.treatyStatus = treatyStatus;
    }

    public Frequency getPastDueFrequency() {
      return pastDueFrequency;
    }

    public void setPastDueFrequency(Frequency pastDueFrequency) {
      this.pastDueFrequency = pastDueFrequency;
    }

    public String getComments() {
      return comments;
    }

    public void setComments(String comments) {
      this.comments = comments;
    }

    public String getTreatyReference() {
      return treatyReference;
    }

    public void setTreatyReference(String treatyReference) {
      this.treatyReference = treatyReference;
    }

    public String getGracePeriodComments() {
      return gracePeriodComments;
    }

    public void setGracePeriodComments(String gracePeriodComments) {
      this.gracePeriodComments = gracePeriodComments;
    }

    public Frequency getReportingFrequencyMgmt() {
      return reportingFrequencyMgmt;
    }

    public void setReportingFrequencyMgmt(Frequency reportingFrequencyMgmt) {
      this.reportingFrequencyMgmt = reportingFrequencyMgmt;
    }

    public String getLatePayment() {
      return latePayment;
    }

    public void setLatePayment(String latePayment) {
      this.latePayment = latePayment;
    }


    public String getInputStatus() {
      return inputStatus;
    }

    public void setInputStatus(String inputStatus) {
      this.inputStatus = inputStatus;
    }

    public String getTreatyRefIntPltyComment() {
      return treatyRefIntPltyComment;
    }

    public void setTreatyRefIntPltyComment(String treatyRefIntPltyComment) {
      this.treatyRefIntPltyComment = treatyRefIntPltyComment;
    }

    public String getIntPenaltyComment() {
      return intPenaltyComment;
    }

    public void setIntPenaltyComment(String intPenaltyComment) {
      this.intPenaltyComment = intPenaltyComment;
    }

    public String getCedingFlag() {
      return cedingFlag;
    }

    public void setCedingFlag(String cedingFlag) {
      this.cedingFlag = cedingFlag;
    }

    public String getGpTrCeding() {
      return gpTrCeding;
    }

    public void setGpTrCeding(String gpTrCeding) {
      this.gpTrCeding = gpTrCeding;
    }


    public String getGpCmmtsCeding() {
      return gpCmmtsCeding;
    }

    public void setGpCmmtsCeding(String gpCmmtsCeding) {
      this.gpCmmtsCeding = gpCmmtsCeding;
    }

    public Frequency getStmntFrqncCeding() {
      return stmntFrqncCeding;
    }

    public void setStmntFrqncCeding(Frequency stmntFrqncCeding) {
      this.stmntFrqncCeding = stmntFrqncCeding;
    }

    public Frequency getStmntPastdueFrqncCeding() {
      return stmntPastdueFrqncCeding;
    }

    public void setStmntPastdueFrqncCeding(Frequency stmntPastdueFrqncCeding) {
      this.stmntPastdueFrqncCeding = stmntPastdueFrqncCeding;
    }

    public String getTreatyName() {
      return treatyName;
    }

    public void setTreatyName(String treatyName) {
      this.treatyName = treatyName;
    }


    public String getReportingProductCmmnt() {
      return reportingProductCmmnt;
    }

    public void setReportingProductCmmnt(String reportingProductCmmnt) {
      this.reportingProductCmmnt = reportingProductCmmnt;
    }

    public LocalDate getTerminationDate() {
      return terminationDate;
    }

    public void setTerminationDate(LocalDate terminationDate) {
      this.terminationDate = terminationDate;
    }

    public String getYearEndException() {
      return yearEndException;
    }

    public void setYearEndException(String yearEndException) {
      this.yearEndException = yearEndException;
    }

    public Set<TreatyCurrency> getTreatyCurrencies() {
      return treatyCurrencies;
    }

    public void setTreatyCurrencies(Set<TreatyCurrency> treatyCurrencies) {
      this.treatyCurrencies = treatyCurrencies;
    }

    public Long getMdmId() {
      return mdmId;
    }

    public void setMdmId(Long mdmId) {
      this.mdmId = mdmId;
    }

    public String getNotification() {
      return notification;
    }

    public void setNotification(String notification) {
      this.notification = notification;
    }

    public BigDecimal getBrokerFeeAmount() {
    return brokerFeeAmount;
    }

    public void setBrokerFeeAmount(BigDecimal brokerFeeAmount) {
      this.brokerFeeAmount = brokerFeeAmount;
    }

    public BigDecimal getBrokerFeePercentage() {
      return brokerFeePercentage;
    }

    public void setBrokerFeePercentage(BigDecimal brokerFeePercentage) {
      this.brokerFeePercentage = brokerFeePercentage;
    }

    public String getBrokerFeeSwitchFlag() {
      return brokerFeeSwitchFlag;
    }

    public void setBrokerFeeSwitchFlag(String brokerFeeSwitchFlag) {
      this.brokerFeeSwitchFlag = brokerFeeSwitchFlag;
    }

    public String getMinBrokerFeeFlag() {
      return minBrokerFeeFlag;
    }

    public void setMinBrokerFeeFlag(String minBrokerFeeFlag) {
      this.minBrokerFeeFlag = minBrokerFeeFlag;
    }

    public String getCurrencyComments() {
      return currencyComments;
    }

    public void setCurrencyComments(String currencyComments) {
      this.currencyComments = currencyComments;
    }

    public Set<MdmAttributes> getMdmAttributes() {
      return mdmAttributes;
    }

    public void setMdmAttributes(Set<MdmAttributes> mdmAttributes) {
      this.mdmAttributes = mdmAttributes;
    }

    public String getCopyTreatyMsg() {
      return copyTreatyMsg;
    }

    public void setCopyTreatyMsg(String copyTreatyMsg) {
      this.copyTreatyMsg = copyTreatyMsg;
    }

    public Integer getNotificationStatus() {
      return notificationStatus;
    }

    public void setNotificationStatus(Integer notificationStatus) {
      this.notificationStatus = notificationStatus;
    }

    public Integer getDisabledTracking() {
      return disabledTracking;
    }

    public void setDisabledTracking(Integer disabledTracking) {
      this.disabledTracking = disabledTracking;
    }

    public EntityDate getStmntDueAsmBroker() {
      return stmntDueAsmBroker;
    }

    public void setStmntDueAsmBroker(EntityDate stmntDueAsmBroker) {
      this.stmntDueAsmBroker = stmntDueAsmBroker;
    }

    public EntityDate getStmntDueAsm() {
      return stmntDueAsm;
    }

    public void setStmntDueAsm(EntityDate stmntDueAsm) {
      this.stmntDueAsm = stmntDueAsm;
    }

    public EntityDate getStmntDueCed() {
      return stmntDueCed;
    }

    public void setStmntDueCed(EntityDate stmntDueCed) {
      this.stmntDueCed = stmntDueCed;
    }

    public EntityDate getStmntDueCedGracePeriod() {
      return stmntDueCedGracePeriod;
    }

    public void setStmntDueCedGracePeriod(EntityDate stmntDueCedGracePeriod) {
      this.stmntDueCedGracePeriod = stmntDueCedGracePeriod;
    }

    public EntityDate getStmntDueCedQuarterEx() {
      return stmntDueCedQuarterEx;
    }

    public void setStmntDueCedQuarterEx(EntityDate stmntDueCedQuarterEx) {
      this.stmntDueCedQuarterEx = stmntDueCedQuarterEx;
    }

    public EntityDate getReportingDueQuarterEx() {
      return reportingDueQuarterEx;
    }

    public void setReportingDueQuarterEx(EntityDate reportingDueQuarterEx) {
      this.reportingDueQuarterEx = reportingDueQuarterEx;
    }

    public EntityDate getReportingDueYearEx() {
      return reportingDueYearEx;
    }

    public void setReportingDueYearEx(EntityDate reportingDueYearEx) {
      this.reportingDueYearEx = reportingDueYearEx;
    }

    public EntityDate getReportingDueDate() {
      return reportingDueDate;
    }

    public void setReportingDueDate(EntityDate reportingDueDate) {
      this.reportingDueDate = reportingDueDate;
    }

    public Set<TreatySeriatimType> getSeriatimTypeTreaties() {
      return seriatimTypeTreaties;
    }

    public void setSeriatimTypeTreaties(Set<TreatySeriatimType> seriatimTypeTreaties) {
      this.seriatimTypeTreaties = seriatimTypeTreaties;
    }

    public EntityDate getStmntDueAsmQuarterEx() {
      return stmntDueAsmQuarterEx;
    }

    public void setStmntDueAsmQuarterEx(EntityDate stmntDueAsmQuarterEx) {
      this.stmntDueAsmQuarterEx = stmntDueAsmQuarterEx;
    }

    public EntityDate getStmntDueAsmGracePeriod() {
      return stmntDueAsmGracePeriod;
    }

    public void setStmntDueAsmGracePeriod(EntityDate stmntDueAsmGracePeriod) {
      this.stmntDueAsmGracePeriod = stmntDueAsmGracePeriod;
    }

    public Set<YearEndRegulatory> getYearEndRegulatoryList() {
    return yearEndRegulatoryList;
    }

    public void setYearEndRegulatoryList(Set<YearEndRegulatory> yearEndRegulatoryList) {
    this.yearEndRegulatoryList = yearEndRegulatoryList;
    }

    public TreatySeriatim getTreatySeriatim() {
        return treatySeriatim;
    }

    public void setTreatySeriatim(TreatySeriatim treatySeriatim) {
        this.treatySeriatim = treatySeriatim;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Treaty treaty = (Treaty) o;
        if(treaty.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, treaty.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }



  //using for reporting
    @Override
    public String toString() {
        String reportingFrequencyStr = "";
        String stmntDueAsmGracePeriodName = "";
        String stmntDueCedGracePeriodName = "";
        if (reportingFrequency == null) {
          reportingFrequencyStr = "";
        } else {
          reportingFrequencyStr = reportingFrequency.getName();
        }
        if (settlementFrequency == null) {
          stmntDueAsmGracePeriodName = "";
        } else {
          stmntDueAsmGracePeriodName = settlementFrequency.getName();
        }

        if (stmntFrqncCeding == null) {
          stmntDueCedGracePeriodName = "";
        } else {
          stmntDueCedGracePeriodName = stmntFrqncCeding.getName();
        }
        return "Treaty{" +
              " treatyStatus='" + treatyStatus.getName() + "'" +
              ", effectiveDate='" + effectiveDate + "'" +
              ", reportingDueDate='" + reportingFrequencyStr + " " + reportingDueDate + "'" +
              ", stmntDueAsmGracePeriod='" +stmntDueAsmGracePeriodName + " " +  stmntDueAsmGracePeriod + "'" +
              ", stmntDueCedGracePeriod='" +stmntDueCedGracePeriodName + " " +  stmntDueCedGracePeriod + "'" +
              ", InputStatus='" + inputStatus + "'" +
            '}';
    }
}
;
