package com.rgare.global.finre.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A XrefTrackingTransaction.
 */
@Entity
@Table(name = "xref_tracking_transaction")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "xreftrackingtransaction")
public class XrefTrackingTransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "treaty_transaction_cash_id")
    private Long treatyTransactionCashId;

    @Column(name = "treaty_cash_subtransaction_id")
    private Long treatyCashSubtransactionId;

    @Column(name = "treaty_tracking_id")
    private Long treatyTrackingId;

    @Column(name = "currency_id")
    private Long currencyId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTreatyTransactionCashId() {
        return treatyTransactionCashId;
    }

    public void setTreatyTransactionCashId(Long treatyTransactionCashId) {
        this.treatyTransactionCashId = treatyTransactionCashId;
    }

    public Long getTreatyCashSubtransactionId() {
      return treatyCashSubtransactionId;
    }

    public void setTreatyCashSubtransactionId(Long treatyCashSubtransactionId) {
      this.treatyCashSubtransactionId = treatyCashSubtransactionId;
    }

    public Long getTreatyTrackingId() {
      return treatyTrackingId;
    }

    public void setTreatyTrackingId(Long treatyTrackingId) {
      this.treatyTrackingId = treatyTrackingId;
    }

    public Long getCurrencyId() {
      return currencyId;
    }

    public void setCurrencyId(Long currencyId) {
      this.currencyId = currencyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        XrefTrackingTransaction xrefTrackingTransaction = (XrefTrackingTransaction) o;
        if(xrefTrackingTransaction.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, xrefTrackingTransaction.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "XrefTrackingTransaction{" +
            "id=" + id +
            ", treatyTransactionCashId='" + treatyTransactionCashId + "'" +
          ", treatyCashSubtransactionId='" + treatyCashSubtransactionId + "'" +
          ", treatyTrackingId='" + treatyTrackingId + "'" +
          ", currencyId='" + currencyId + "'" +
            '}';
    }
}
