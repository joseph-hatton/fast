package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TreatySeriatimType.
 */
@Entity
@Table(name = "year_end_regulatory")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "yearEndRegulatory")
public class YearEndRegulatory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "regulatory_due_date_flag")
    private String regulatoryDueDateFlag;

    @Column(name = "year_end_regulatory_type")
    private String yearEndRegulatoryType;

    @OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
    private EntityDate regulatoryDueDate;

    @ManyToOne
    private Frequency regulatoryFrequency;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JsonBackReference("treaty")
    private Treaty treaty;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Frequency getRegulatoryFrequency() {
      return regulatoryFrequency;
    }

    public void setRegulatoryFrequency(Frequency regulatoryFrequency) {
      this.regulatoryFrequency = regulatoryFrequency;
    }

    public EntityDate getRegulatoryDueDate() {
      return regulatoryDueDate;
    }

    public void setRegulatoryDueDate(EntityDate regulatoryDueDate) {
      this.regulatoryDueDate = regulatoryDueDate;
    }

    public String getYearEndRegulatoryType() {
      return yearEndRegulatoryType;
    }

    public void setYearEndRegulatoryType(String yearEndRegulatoryType) {
      this.yearEndRegulatoryType = yearEndRegulatoryType;
    }

    public String getRegulatoryDueDateFlag() {
      return regulatoryDueDateFlag;
    }

    public void setRegulatoryDueDateFlag(String regulatoryDueDateFlag) {
      this.regulatoryDueDateFlag = regulatoryDueDateFlag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        YearEndRegulatory yearEndRegulatory = (YearEndRegulatory) o;
        if(yearEndRegulatory.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, yearEndRegulatory.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "YearEndRegulatory{" +
            "id=" + id +
            '}';
    }
}
