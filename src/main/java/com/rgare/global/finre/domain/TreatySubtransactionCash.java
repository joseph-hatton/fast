package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A TreatySubTransaction.
 */
@Entity
@Table(name = "treaty_cash_subtransaction")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "treatycashsubtransaction")
public class TreatySubtransactionCash extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

    @Column(name = "amount", precision=10, scale=2)
    private BigDecimal amount;

    @Column(name = "converted_amount", precision=10, scale=2)
    private BigDecimal convertedAmount;

    @Column(name = "reporting_date")
    private LocalDate reportingDate;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JsonBackReference("treatyTransactionCash")
    private TreatyTransactionCash treatyTransactionCash;


    @Column(name = "active_status")
    private Integer isActive;

    @ManyToOne
    private AllocationType allocationType;

    @Column(name = "settlement_date")
    private LocalDate settlementDate;

    @ManyToOne
    private MdmAttributes mdm;

    @ManyToOne
    private Treaty treaty;

    @ManyToOne
    private TreatyTracking tracking;

    public Long getId() {
          return id;
      }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getConvertedAmount() {
        return convertedAmount;
    }

    public void setConvertedAmount(BigDecimal convertedAmount) {
        this.convertedAmount = convertedAmount;
    }

    public TreatyTransactionCash getTreatyTransactionCash() {
      return treatyTransactionCash;
    }

    public void setTreatyTransactionCash(TreatyTransactionCash treatyTransactionCash) {
      this.treatyTransactionCash = treatyTransactionCash;
    }
    public LocalDate getSettlementDate() {
    return settlementDate;
  }

    public void setSettlementDate(LocalDate settlementDate) {
    this.settlementDate = settlementDate;
  }

    public AllocationType getAllocationType() {
          return allocationType;
      }

    public void setAllocationType(AllocationType allocationType) {
        this.allocationType = allocationType;
    }

    public LocalDate getReportingDate() {
      return reportingDate;
    }

    public void setReportingDate(LocalDate reportingDate) {
      this.reportingDate = reportingDate;
    }

    public Treaty getTreaty() {
      return treaty;
    }

    public void setTreaty(Treaty treaty) {
      this.treaty = treaty;
    }

    public MdmAttributes getMdm() {
      return mdm;
    }

    public void setMdm(MdmAttributes mdm) {
      this.mdm = mdm;
    }

    public TreatyTracking getTracking() {
    return tracking;
  }

    public void setTracking(TreatyTracking tracking) {
    this.tracking = tracking;
  }

    public Integer getIsActive() {
    return isActive;
    }

    public void setIsActive(Integer isActive) {
    this.isActive = isActive;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TreatySubtransactionCash treatySubTransaction = (TreatySubtransactionCash) o;
        if(treatySubTransaction.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, treatySubTransaction.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TreatySubtransactionCash{" +
            "id=" + id +
            ", amount='" + amount + "'" +
            ", convertedAmount='" + convertedAmount + "'" +
            ", reportingDate='" + reportingDate + "'" +
            '}';
    }
}
