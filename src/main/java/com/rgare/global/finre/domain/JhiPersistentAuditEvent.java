package com.rgare.global.finre.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * A jhi_persistent_audit_event.
 */
@Entity
@Table(name = "jhi_persistent_audit_event")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class JhiPersistentAuditEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name="principal")
    public String principal;
    
    @Column(name="event_date")
    public LocalDateTime eventDate;
    
    @Column(name="event_type")
    public String eventType;

    @Column(name = "last_user")
    private String lastUser;

    @Column(name = "last_update")
    private LocalDateTime lastUpdate;

    @Column(name = "creation_ts")
    private LocalDateTime creationTs;

    @Column(name = "creation_user")
    private String creationUser;

    public JhiPersistentAuditEvent(String principal, LocalDateTime date, String lastUser, String eventType) {
        this.principal = principal;
        this.eventDate = this.lastUpdate = this.creationTs = date;
        this.lastUser = this.creationUser = lastUser;
        this.eventType = eventType;
    }

    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getPrincipal() {
        return principal;
    }
    
    public void setPrincipal(String principal) {
        this.principal = principal;
    }
    
    public LocalDateTime getEventDate() {
        return eventDate;
    }
    
    public void setEventDate(LocalDateTime eventDate) {
        this.eventDate = eventDate;
    }
    
    public String getEventType() {
        return eventType;
    }
    
    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getLastUser() {
        return lastUser;
    }
    
    public void setLastUser(String lastUser) {
        this.lastUser = lastUser;
    }
    
    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }
    
    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    
    public LocalDateTime getCreationTs() {
        return creationTs;
    }
    
    public void setCreationTs(LocalDateTime creationTs) {
        this.creationTs = creationTs;
    }
    
    public String getCreationUser() {
        return creationUser;
    }
    
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }
}
