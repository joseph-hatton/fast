package com.rgare.global.finre.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A EnterpriseKey.
 */
@Entity
@Table(name = "enterprise_key")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "enterprisekey")
public class EnterpriseKey implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "deal")
    private String deal;

    @Column(name = "effective_status_cd")
    private String effectiveStatusCd;

    @Column(name = "enterprise_id")
    private Long enterpriseId;

    @Column(name = "affiliate_type")
    private String affiliateType;

    @Column(name = "corporate_indicator")
    private String corporateIndicator;

    @Column(name = "business_unit_id")
    private String businessUnitId;

    @Column(name = "business_unit_name")
    private String businessUnitName;

    @Column(name = "legal_doc")
    private String legalDoc;

    @Column(name = "reporting_product_id")
    private String reportingProductId;

    @Column(name = "reporting_product")
    private String reportingProduct;

    @Column(name = "product_line_id")
    private Long productLineId;

    @Column(name = "product_line_name")
    private String productLineName;

    @Column(name = "client_company_id")
    private String clientCompanyId;

    @Column(name = "client_company_name")
    private String clientCompanyName;

    @Column(name = "us_stat_risk")
    private String usStatRisk;

    /**HB_CHANGED_VID**/
    @Column(name = "version_number")
    private Long versionNumber;

    /**HB_CREATED_VID**/
    @Column(name = "version_date")
    private Long versionDate;

    @Column(name = "legal_code")
    private String legalCode;

    @Column(name = "legal_name")
    private String legalName;

    public String getDeal() {
    return deal;
  }

    public void setDeal(String deal) {
    this.deal = deal;
  }

    @ManyToOne
    @JoinColumn(name="product_structure_id", columnDefinition = "number")
    private ProductStructure productStructure;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getEnterpriseId() {
      return enterpriseId;
    }

    public void setEnterpriseId(Long enterpriseId) {
      this.enterpriseId = enterpriseId;
    }

    public String getAffiliateType() {
        return affiliateType;
      }

    public void setAffiliateType(String affiliateType) {
      this.affiliateType = affiliateType;
    }

    public String getCorporateIndicator() {
      return corporateIndicator;
    }

    public void setCorporateIndicator(String corporateIndicator) {
      this.corporateIndicator = corporateIndicator;
    }

    public String getEffectiveStatusCd() {
      return effectiveStatusCd;
    }

    public void setEffectiveStatusCd(String effectiveStatusCd) {
      this.effectiveStatusCd = effectiveStatusCd;
    }

  public String getBusinessUnitId() {
      return businessUnitId;
    }

    public void setBusinessUnitId(String businessUnitId) {
      this.businessUnitId = businessUnitId;
    }

    public String getBusinessUnitName() {
      return businessUnitName;
    }

    public void setBusinessUnitName(String businessUnitName) {
      this.businessUnitName = businessUnitName;
    }


    public ProductStructure getProductStructure() {
      return productStructure;
    }

    public void setProductStructure(ProductStructure productStructure) {
      this.productStructure = productStructure;
    }

    public String getLegalDoc() {
        return legalDoc;
      }

    public void setLegalDoc(String legalDoc) {
      this.legalDoc = legalDoc;
    }

    public String getReportingProductId() {
      return reportingProductId;
    }

    public void setReportingProductId(String reportingProductId) {
      this.reportingProductId = reportingProductId;
    }

    public String getReportingProduct() {
      return reportingProduct;
    }

    public void setReportingProduct(String reportingProduct) {
      this.reportingProduct = reportingProduct;
    }

    public Long getProductLineId() {
      return productLineId;
    }

    public void setProductLineId(Long productLineId) {
      this.productLineId = productLineId;
    }

    public String getProductLineName() {
      return productLineName;
    }

    public void setProductLineName(String productLineName) {
      this.productLineName = productLineName;
    }

    public String getClientCompanyId() {
      return clientCompanyId;
    }

    public void setClientCompanyId(String clientCompanyId) {
      this.clientCompanyId = clientCompanyId;
    }

    public String getClientCompanyName() {
      return clientCompanyName;
    }

    public void setClientCompanyName(String clientCompanyName) {
      this.clientCompanyName = clientCompanyName;
    }

    public Long getVersionNumber() {
      return versionNumber;
    }

    public void setVersionNumber(Long versionNumber) {
      this.versionNumber = versionNumber;
    }

    public Long getVersionDate() {
      return versionDate;
    }

    public void setVersionDate(Long versionDate) {
      this.versionDate = versionDate;
    }

    public String getLegalCode() {
      return legalCode;
    }

    public void setLegalCode(String legalCode) {
      this.legalCode = legalCode;
    }

    public String getLegalName() {
      return legalName;
    }

    public void setLegalName(String legalName) {
      this.legalName = legalName;
    }

    public String getUsStatRisk() {
      return usStatRisk;
    }

    public void setUsStatRisk(String usStatRisk) {
      this.usStatRisk = usStatRisk;
    }

  @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EnterpriseKey enterpriseKey = (EnterpriseKey) o;
        if(enterpriseKey.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, enterpriseKey.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "EnterpriseKey{" +
            "id=" + id +
            ", name='" + name + "'" +
            '}';
    }
}
