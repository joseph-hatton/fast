package com.rgare.global.finre.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A XrefTreatyContact.
 */
@Entity
@Table(name = "xref_treaty_contact")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "xreftreatycontact")
public class XrefTreatyContact extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "treaty_id", nullable = false)
    private Long treatyId;

    @ManyToOne
    private Contact contact;

    @ManyToOne
    private ContactSubtype contactSubtype;

    @ManyToOne
    private ContactType contactType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTreatyId() {
        return treatyId;
    }

    public void setTreatyId(Long treatyId) {
        this.treatyId = treatyId;
    }

    public Contact getContact() {
      return contact;
    }

    public void setContact(Contact contact) {
      this.contact = contact;
    }

    public ContactSubtype getContactSubtype() {
      return contactSubtype;
    }

    public void setContactSubtype(ContactSubtype contactSubtype) {
      this.contactSubtype = contactSubtype;
    }

    public ContactType getContactType() {
      return contactType;
    }

    public void setContactType(ContactType contactType) {
      this.contactType = contactType;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        XrefTreatyContact xrefTreatyContact = (XrefTreatyContact) o;
        if(xrefTreatyContact.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, xrefTreatyContact.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "XrefTreatyContact{" +
            "id=" + id +
            ", treatyId='" + treatyId + "'" +
            ", contact='" + contact + "'" +
            ", lastUser='" + lastUser + "'" +
            '}';
    }
}
