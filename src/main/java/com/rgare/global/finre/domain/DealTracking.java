package com.rgare.global.finre.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DealTracking.
 */
@Entity
@Table(name = "deal_tracking")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dealtracking")
public class DealTracking extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "finance_date")
    private LocalDate financeDate;

    @Column(name = "reporting_arm_date")
    private LocalDate reportingArmDate;

    @Column(name = "reporting_validation_date")
    private LocalDate reportingValidationDate;

    @Column(name = "reporting_period_end_date")
    private LocalDate reportingPeriodEndDate;

    @Column(name = "password")
    private String password;

    @Column(name = "auto_generate_flag")
    private Integer autoGenerateFlag;

    @Column(name = "active_status")
    private Integer isActive;

    @Column(name = "deal_tracking_status")
    private String dealTrackingStatus;

    @Column(name = "finance_date_flag")
    private Integer financeDateFlag;

    @Column(name = "reporting_arm_flag")
    private Integer reportingArmFlag;

    @Column(name = "reporting_flag")
    private String reportingFlag;

    @Column(name = "reporting_revised_flag")
    private String reportingRevisedFlag;

    @ManyToOne
    private Deal deal;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getFinanceDate() {
      return financeDate;
    }

    public void setFinanceDate(LocalDate financeDate) {
      this.financeDate = financeDate;
    }

    public LocalDate getReportingArmDate() {
      return reportingArmDate;
    }

    public void setReportingArmDate(LocalDate reportingArmDate) {
      this.reportingArmDate = reportingArmDate;
    }

  public LocalDate getReportingValidationDate() {
    return reportingValidationDate;
  }

  public void setReportingValidationDate(LocalDate reportingValidationDate) {
    this.reportingValidationDate = reportingValidationDate;
  }

  public Integer getAutoGenerateFlag() {
    return autoGenerateFlag;
  }

  public void setAutoGenerateFlag(Integer autoGenerateFlag) {
    this.autoGenerateFlag = autoGenerateFlag;
  }

  public LocalDate getReportingPeriodEndDate() {
      return reportingPeriodEndDate;
    }

    public void setReportingPeriodEndDate(LocalDate reportingPeriodEndDate) {
      this.reportingPeriodEndDate = reportingPeriodEndDate;
    }

    public String getDealTrackingStatus() {
      return dealTrackingStatus;
    }

    public void setDealTrackingStatus(String dealTrackingStatus) {
      this.dealTrackingStatus = dealTrackingStatus;
    }

     public String getPassword() {
      return password;
    }

    public void setPassword(String password) {
      this.password = password;
    }

    public Integer getFinanceDateFlag() {
      return financeDateFlag;
    }

    public void setFinanceDateFlag(Integer financeDateFlag) {
      this.financeDateFlag = financeDateFlag;
    }

    public Integer getReportingArmFlag() {
      return reportingArmFlag;
    }

    public void setReportingArmFlag(Integer reportingArmFlag) {
      this.reportingArmFlag = reportingArmFlag;
    }

  public String getReportingRevisedFlag() {
    return reportingRevisedFlag;
  }

  public void setReportingRevisedFlag(String reportingRevisedFlag) {
    this.reportingRevisedFlag = reportingRevisedFlag;
  }

  public Deal getDeal() {
      return deal;
    }

    public void setDeal(Deal deal) {
      this.deal = deal;
    }

  public String getReportingFlag() {
    return reportingFlag;
  }

  public void setReportingFlag(String reportingFlag) {
    this.reportingFlag = reportingFlag;
  }

  public Integer getIsActive() {
    return isActive;
  }

  public void setIsActive(Integer isActive) {
    this.isActive = isActive;
  }

  @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DealTracking dealTracking = (DealTracking) o;
        if(dealTracking.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, dealTracking.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DealTracking{" +
            "id=" + id +
            '}';
    }
}
