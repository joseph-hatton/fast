package com.rgare.global.finre.domain;

//import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


/**
 * Created by s0044514 on 1/12/2017.
 */

@Entity
@Table(name = "audit_risk_fee")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "auditriskfee")

public class AuditRiskFee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "treaty_id")
    private Long treatyId;

    @Column(name = "risk_fee_id")
    private Long riskFeeId;

    @Column(name = "treaty_status")
    private String treatyStatus;

    @Column(name = "action")
    private String action;


    @Column(name = "comments")
    private String comments;

    @Column(name = "creation_user")
    String creationUser;

    @Column(name = "management_input_status")
    private String managementInputStatus;


    public Long getId() {
    return id;
    }

    public void setId(Long id) {
    this.id = id;
    }

    public Long getTreatyId() {
    return treatyId;
    }

    public void setTreatyId(Long treatyId) {
    this.treatyId = treatyId;
    }

    public String getAction() {
    return action;
    }

    public void setAction(String action) {
    this.action = action;
    }

    public String getComments() {
    return comments;
    }

    public void setComments(String comments) {
    this.comments = comments;
    }

    public String getCreationUser() {
    return creationUser;
    }

    public Long getRiskFeeId() {
    return riskFeeId;
    }

    public void setRiskFeeId(Long riskFeeId) {
    this.riskFeeId = riskFeeId;
    }

    public String getTreatyStatus() {
    return treatyStatus;
    }

    public void setTreatyStatus(String treatyStatus) {
    this.treatyStatus = treatyStatus;
    }

    public void setCreationUser(String creationUser) {
    this.creationUser = creationUser;
    }

    public String getManagementInputStatus() {
    return managementInputStatus;
    }

    public void setManagementInputStatus(String managementInputStatus) {
       this.managementInputStatus = managementInputStatus;
    }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AuditRiskFee auditRiskFee = (AuditRiskFee) o;
    if(auditRiskFee.id == null || id == null) {
      return false;
    }
    return Objects.equals(id, auditRiskFee.id);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id);
  }

  @Override
  public String toString() {
    return "AuditRiskFee{" +
      "id=" + id +
      ", treatyId='" + treatyId + "'" +
      ", riskFeeId='" + riskFeeId + "'" +
      ", treatyStatus='" + treatyStatus + "'" +
      ", action='" + action + "'" +
      ", comments='" + comments + "'" +
      ", creationUser='" + creationUser + "'" +
      ", managementInputStatus='" + managementInputStatus + "'" +
      '}';
  }
}

