package com.rgare.global.finre.domain;

import com.rgare.global.finre.domain.JhiUser;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * A jhi_user_authority.
 */
@Entity
@Table(name = "jhi_user_authority")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class JhiUserAuthority implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JsonBackReference
    @JoinColumn(name="user_id")
    private JhiUser jhiUser;

    @Column(name = "authority_name")
    private String authorityName;

    @Column(name = "last_user")
    private String lastUser;

    @Column(name = "last_update")
    private LocalDateTime lastUpdate;

    @Column(name = "creation_ts")
    private LocalDateTime creationTs;

    @Column(name = "creation_user")
    private String creationUser;
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getAuthorityName() {
        return authorityName;
    }
    
    public void setAuthorityName(String authorityName) {
        this.authorityName = authorityName;
    }
    
    public String getLastUser() {
        return lastUser;
    }
    
    public void setLastUser(String lastUser) {
        this.lastUser = lastUser;
    }
    
    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }
    
    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    
    public LocalDateTime getCreationTs() {
        return creationTs;
    }
    
    public void setCreationTs(LocalDateTime creationTs) {
        this.creationTs = creationTs;
    }
    
    public String getCreationUser() {
        return creationUser;
    }
    
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    
}
