package com.rgare.global.finre.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A AuditRiskManagement.
 */
@Entity
@Table(name = "audit_risk_management")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "auditriskmanagement")
public class AuditRiskManagement extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "deal_id")
    private Long dealId;

    @Column(name = "risk_management_id")
    private Long riskManagementId;

    @Column(name = "action")
    private String action;

    @Column(name = "comments")
    private String comments;

    public Long getDealId() {
      return dealId;
    }

    public void setDealId(Long dealId) {
      this.dealId = dealId;
    }

    public String getAction() {
      return action;
    }

    public void setAction(String action) {
      this.action = action;
    }

    public String getComments() {
      return comments;
    }

    public void setComments(String comments) {
      this.comments = comments;
    }

    public Long getRiskManagementId() {
      return riskManagementId;
    }

    public void setRiskManagementId(Long riskManagementId) {
      this.riskManagementId = riskManagementId;
    }

    public Long getId() {
          return id;
      }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AuditRiskManagement auditRiskManagement = (AuditRiskManagement) o;
        if (auditRiskManagement.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, auditRiskManagement.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "AuditRiskManagement{" +
            "id=" + id +
            '}';
    }
}
