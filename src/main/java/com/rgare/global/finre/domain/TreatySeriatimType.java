package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TreatySeriatimType.
 */
@Entity
@Table(name = "treaty_seriatim_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "treatyseriatimtype")
public class TreatySeriatimType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "seriatim_due_date_flag")
    private String seriatimDueDateFlag;

    @Column(name = "treaty_seriatim_comment")
    private String treatySeriatimComment;

    @Column(name = "treaty_seriatim_reference")
    private String treatySeriatimReference;

    @Column(name = "calculation_flag")
    private Boolean calculationFlag;

    @OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
    private EntityDate seriatimDueDate;

    @ManyToOne
    private Frequency seriatimFreq;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JsonBackReference("treaty")
    private Treaty treaty;

    @ManyToOne
    private SeriatimType seriatimType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SeriatimType getSeriatimType() {
      return seriatimType;
    }

    public void setSeriatimType(SeriatimType seriatimType) {
      this.seriatimType = seriatimType;
    }

    public String getSeriatimDueDateFlag() {
        return seriatimDueDateFlag;
      }

    public void setSeriatimDueDateFlag(String seriatimDueDateFlag) {
      this.seriatimDueDateFlag = seriatimDueDateFlag;
    }

    public EntityDate getSeriatimDueDate() {
      return seriatimDueDate;
    }

    public void setSeriatimDueDate(EntityDate seriatimDueDate) {
      this.seriatimDueDate = seriatimDueDate;
    }

    public Frequency getSeriatimFreq() {
      return seriatimFreq;
    }

    public void setSeriatimFreq(Frequency seriatimFreq) {
      this.seriatimFreq = seriatimFreq;
    }

    public String getTreatySeriatimComment() {
      return treatySeriatimComment;
    }

    public void setTreatySeriatimComment(String treatySeriatimComment) {
      this.treatySeriatimComment = treatySeriatimComment;
    }


  public Boolean getCalculationFlag() {
    return calculationFlag;
  }

  public void setCalculationFlag(Boolean calculationFlag) {
    this.calculationFlag = calculationFlag;
  }

  public String getTreatySeriatimReference() {
      return treatySeriatimReference;
    }

    public void setTreatySeriatimReference(String treatySeriatimReference) {
      this.treatySeriatimReference = treatySeriatimReference;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TreatySeriatimType treatySeriatimType = (TreatySeriatimType) o;
        if(treatySeriatimType.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, treatySeriatimType.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TreatySeriatimType{" +
            "id=" + id +
            '}';
    }
}
