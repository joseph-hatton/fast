package com.rgare.global.finre.domain;

import java.util.List;
import java.util.stream.Collectors;
import org.json.JSONObject;


public class Reason {
    public String action;
    public List<JhiUserAuthority> auths;
    
    public Reason(String action, List<JhiUserAuthority> auths){
        this.action = action;
        this.auths = auths;
    }

    public String toString() {
        JSONObject res = new JSONObject();
        
        try {
            res.put("action", this.action);
            res.put("auths", this.auths.stream().map(x -> x.getAuthorityName()).collect(Collectors.toList()));
        } catch (Exception e) {
            
        }
        

        return res.toString();
    }
}  