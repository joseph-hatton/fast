package com.rgare.global.finre.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Company.
 */
@Entity
@Table(name = "company")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "company")
public class Company implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "company_name", nullable = false)
    private String name;

    @Column(name = "affiliate_name")
    private String affiliateName;

    @Column(name = "company_type")
    private String companyType;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "postal_code")
    private String postalCode;

    @Column(name = "region")
    private String region;

    @Column(name = "country")
    private String countryName;

    @Column(name = "client_company_id")
    private Long clientCompanyId;

    @Column(name = "mdm_id")
    private Long mdmId;

    @Column(name = "active_status")
    private Integer isActive;

    @Column(name = "legal_code")
    private String legalCode;

    @Column(name = "legal_name")
    private String legalName;

    @Column(name = "is_rga_company")
    private Integer isRgaCompany;

    @Column(name = "version_date")
    private Long versionDate;

    @Column(name = "version_number")
    private Long versionNumber;

    @ManyToOne
    private Country country;

    public Long getId() {
              return id;
          }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        if(clientCompanyId==null){
          return name.concat("*");
        }
      return name;
    }

    public void setName(String name) {
      if(name != null) {
        this.name = name.toUpperCase();
      }
    }

    public String getAffiliateName() {
        return affiliateName;
    }

    public void setAffiliateName(String affiliateName) {
      if(affiliateName != null) {
        this.affiliateName = affiliateName.toUpperCase();
      }
    }

    public String getLegalCode() {
        return legalCode;
      }

    public void setLegalCode(String legalCode) {
        this.legalCode = legalCode;
      }

    public String getLegalName() {
        return legalName;
      }

    public void setLegalName(String legalName) {
    this.legalName = legalName;
  }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Integer getIsActive() {
      return isActive;
    }

    public void setIsActive(Integer isActive) {
      this.isActive = isActive;
    }

    public String getCompanyType() {
      return companyType;
    }

    public void setCompanyType(String companyType) {
      this.companyType = companyType;
    }

    public Long getClientCompanyId() {
      return clientCompanyId;
    }

    public void setClientCompanyId(Long clientCompanyId) {
      this.clientCompanyId = clientCompanyId;
    }

    public String getRegion() {
      return region;
    }

    public void setRegion(String region) {
      this.region = region;
    }

    public Country getCountry() {
      return country;
    }

    public void setCountry(Country country) {
      this.country = country;
    }

    public String getCountryName() {
      return countryName;
    }

    public void setCountryName(String countryName) {
      this.countryName = countryName;
    }

    public Long getMdmId() {
      return mdmId;
    }

    public void setMdmId(Long mdmId) {
      this.mdmId = mdmId;
    }

    public Integer getIsRgaCompany() {
      return isRgaCompany;
    }

    public void setIsRgaCompany(Integer isRgaCompany) {
      this.isRgaCompany = isRgaCompany;
    }

    public Long getVersionDate() {
      return versionDate;
    }

    public void setVersionDate(Long versionDate) {
      this.versionDate = versionDate;
    }

    public Long getVersionNumber() {
      return versionNumber;
    }

    public void setVersionNumber(Long versionNumber) {
      this.versionNumber = versionNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Company company = (Company) o;
        if(company.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, company.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Company{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", affiliateName='" + affiliateName + "'" +
            ", companyType='" + companyType + "'" +
            ", city='" + city + "'" +
            ", state='" + state + "'" +
            ", postalCode='" + postalCode + "'" +
            ", countryName='" + countryName + "'" +
            ", region='" + region + "'" +
            ", isActive='" + isActive + "'" +
            ", clientCompanyId='" + clientCompanyId + "'" +
            ", mdmId='" + mdmId + "'" +
            ", legalCode='" + legalCode + "'" +
            ", legalName='" + legalName + "'" +
          '}';
    }
}
