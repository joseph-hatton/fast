package com.rgare.global.finre.config;

import com.rgare.global.finre.domain.DealType;
import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.rgare.global.finre.domain.JhiPersistentAuditEvent.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.JhiAuthority.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.JhiUserAuthority.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.JhiUser.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.StatInfo.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.Fastopedia.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.IntroPage.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.IntroStep.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.PersistentToken.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.User.class.getName() + ".persistentTokens", jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.AgreementType.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.AllocationType.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.AuditFinanceEmail.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.AuditRiskFee.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.AuditTransactions.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.AuditTreaty.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.Company.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.Contact.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.ContactSubtype.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.ContactType.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.Country.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.Currency.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.Deal.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.DealTracking.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.EnterpriseKey.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.EntityDate.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.EucUse.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.Frequency.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.FundingType.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.GfsAssignee.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.MdmAttributes.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.MdmCompany.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.NotificationStatus.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.ProductLine.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.ProductStructure.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.ProductSubtype.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.ProductGroup.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.ProductType.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.Reports.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.RiskFee.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.RiskManagement.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.SeriatimType.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.SettlementType.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.State.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.StructuredFinance.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.StructuredTracking.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.TransactionStatus.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.Treaty.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.Treaty.class.getName() + ".treatyCurrency", jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.Treaty.class.getName() + ".mdmAttributes", jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.TreatyAmendments.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.TreatyCurrency.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.TreatyManagement.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.TreatySeriatim.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.TreatySeriatimType.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.TreatyStatus.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.TreatySubtransactionCash.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.TreatySubtransactionRelief.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.TreatyTracking.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.TreatyTransactionCash.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.TreatyTransactionRelief.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.XrefTrackingTransaction.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.XrefTreatyCompany.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.XrefTreatyContact.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.YearEndRegulatory.class.getName(), jcacheConfiguration);
            //            cm.createCache(com.rgare.global.finre.domain.AgreementType.class.getName() + ".cars", jcacheConfiguration);

            cm.createCache(com.rgare.global.finre.domain.RiskCriteria.class.getName(), jcacheConfiguration);
            cm.createCache(com.rgare.global.finre.domain.AuditRiskManagement.class.getName(), jcacheConfiguration);
            cm.createCache(DealType.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
