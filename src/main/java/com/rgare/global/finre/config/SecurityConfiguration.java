package com.rgare.global.finre.config;

import com.rgare.global.finre.security.*;

import io.github.jhipster.config.JHipsterProperties;
import io.github.jhipster.security.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.filter.CorsFilter;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  private final Logger log = LoggerFactory.getLogger(SecurityConfiguration.class);

  private final AuthenticationManagerBuilder authenticationManagerBuilder;

  private final Environment env;

  private final JHipsterProperties jHipsterProperties;

  private final CorsFilter corsFilter;

  private UserDetailsLoader userDetailsLoader;

  public SecurityConfiguration(AuthenticationManagerBuilder authenticationManagerBuilder,
                               Environment env,
                               JHipsterProperties jHipsterProperties,
                               CorsFilter corsFilter) {

    this.authenticationManagerBuilder = authenticationManagerBuilder;
    this.corsFilter = corsFilter;
    this.env = env;
    this.jHipsterProperties = jHipsterProperties;
  }

  @PostConstruct
  public void init() {
    try {
      this.configure(authenticationManagerBuilder);
    } catch (Exception e) {
      throw new BeanInitializationException("Security configuration failed", e);
    }
  }


  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(getPreAuthenticatedAuthenticationProvider());
  }

  @Bean
  public Http401UnauthorizedEntryPoint http401UnauthorizedEntryPoint() {
    return new Http401UnauthorizedEntryPoint();
  }

  @Bean
  public AjaxLogoutSuccessHandler ajaxLogoutSuccessHandler() {
    return new AjaxLogoutSuccessHandler();
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring()
      .antMatchers(HttpMethod.OPTIONS, "/**")
      .antMatchers("/app/**/*.{js,html}")
      .antMatchers("/bower_components/**")
      .antMatchers("/i18n/**")
      .antMatchers("/content/**")
      .antMatchers("/swagger-ui/index.html")
      .antMatchers("/test/**");
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
      .sessionManagement()
      .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
      .and()
      .csrf().disable()
      .exceptionHandling()
      .and()
      .logout()
      .logoutUrl("/api/logout")
      .deleteCookies("JSESSIONID")
      .invalidateHttpSession(true)
      .logoutSuccessHandler(ajaxLogoutSuccessHandler())
      .permitAll()
      .and()
      .headers()
      .frameOptions()
      .disable()
      .and()
      .authorizeRequests()
      .antMatchers("/api/authenticate").authenticated()
      .antMatchers("/api/profile-info").authenticated()
      .antMatchers("/api/**").authenticated()
      .antMatchers("/management/health").permitAll()
      .antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN)
      .antMatchers("/v2/api-docs/**").permitAll()
      .antMatchers("/swagger-resources/configuration/ui").permitAll()
      .antMatchers("/swagger-ui/index.html").hasAuthority(AuthoritiesConstants.ADMIN)
      .and()
      .addFilterBefore(new DebugFilter(), BasicAuthenticationFilter.class)
      .addFilterBefore(getFilter(), BasicAuthenticationFilter.class);
  }

  @Bean
  public SecurityEvaluationContextExtension securityEvaluationContextExtension() {
    return new SecurityEvaluationContextExtension();
  }

  @Lazy
  @Bean
  public PreAuthenticatedAuthenticationProvider getPreAuthenticatedAuthenticationProvider() {
    PreAuthenticatedAuthenticationProvider provider = new PreAuthenticatedAuthenticationProvider();
    provider.setPreAuthenticatedUserDetailsService(userDetailsLoader);
    return provider;
  }

  public AbstractPreAuthenticatedProcessingFilter getFilter() throws Exception {
    List<String> activeProfiles = Arrays.asList(DefaultProfileUtil.getActiveProfiles(env));
    log.debug("which profiles are active? " + activeProfiles);
    AbstractPreAuthenticatedProcessingFilter filter;
    if (activeProfiles.contains("local")) {
      filter = new SystemUserAuthenticationFilter();
    } else {
      RequestHeaderAuthenticationFilter headerFilter = new RequestHeaderAuthenticationFilter();
      headerFilter.setExceptionIfHeaderMissing(false);
      headerFilter.setPrincipalRequestHeader("user-id");
      filter = headerFilter;
    }
    filter.setAuthenticationManager(authenticationManager());
    return filter;
  }

  @Autowired
  public void setUserDetailsLoader(UserDetailsLoader userDetailsLoader) {
    this.userDetailsLoader = userDetailsLoader;
  }
}
