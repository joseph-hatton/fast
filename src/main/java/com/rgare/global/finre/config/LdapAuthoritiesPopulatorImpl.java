package com.rgare.global.finre.config;

import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.NameAwareAttribute;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import java.util.Collection;
import java.util.HashSet;


/**
 * Created by s0044277 on 10/5/2016.
 */
public class LdapAuthoritiesPopulatorImpl implements LdapAuthoritiesPopulator {

  private String searchAttribute="memberof";


  @Override
    public Collection<? extends GrantedAuthority> getGrantedAuthorities(DirContextOperations userData, String username) {

        Collection<GrantedAuthority> authorities = new HashSet<>();

        NameAwareAttribute awareAttribute=((NameAwareAttribute)userData.getAttributes().get(searchAttribute));
        if(awareAttribute!=null) {
            NamingEnumeration<?> namingEnumeration = awareAttribute.getAll();
            try {
                while (namingEnumeration.hasMore()) {
                    String roleString = (String) namingEnumeration.next();
                    String role = roleString.substring(3, roleString.indexOf(','));
                    authorities.add(new GrantedAuthority() {
                        @Override
                        public String getAuthority() {
                            return role;
                        }
                    });
                }
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }


        return authorities;
    }

  public String getSearchAttribute() {
    return searchAttribute;
  }

  public void setSearchAttribute(String searchAttribute) {
    this.searchAttribute = searchAttribute;
  }
}
