package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.*;
import com.rgare.global.finre.repository.AuditTreatyRepository;
import com.rgare.global.finre.repository.TreatyRepository;
import com.rgare.global.finre.repository.search.TreatySearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing Treaty.
 */
@RestController
@RequestMapping("/api")
public class TreatyResource {

    private final Logger log = LoggerFactory.getLogger(TreatyResource.class);

    private static final String ENTITY_NAME = "treaty";

    private TreatyRepository treatyRepository;

    private TreatySearchRepository treatySearchRepository;

    private Dp3LogService dp3LogService;

    private AuditTreatyRepository auditTreatyRepository;

    public TreatyResource(TreatyRepository treatyRepository, TreatySearchRepository treatySearchRepository,
                          Dp3LogService dp3LogService, AuditTreatyRepository auditTreatyRepository) {
        this.treatyRepository = treatyRepository;
        this.treatySearchRepository = treatySearchRepository;
        this.dp3LogService = dp3LogService;
        this.auditTreatyRepository = auditTreatyRepository;
    }
    /**
     * POST  /treaties : Create a new treaty.
     *
     * @param treaty the treaty to create
     * @return the ResponseEntity with status 201 (Created) and with body the new treaty, or with status 400 (Bad Request) if the treaty has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaties",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Treaty> createTreaty(@Valid @RequestBody Treaty treaty) throws URISyntaxException {
        log.debug("REST request to save Treaty : {}", treaty);
        if (treaty.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("treaty", "idexists", "A new treaty cannot already have an ID")).body(null);
        }
        Treaty result = treatyRepository.save(treaty);
        treatySearchRepository.save(result);
        auditTreatyRepository.save(getAuditTreaty(treaty,AuditTreatyAction.SAVE.name()));
      //  dp3LogService.postDp3log("SAVING TREATY","/api/treaties/", "Save Treaty Id:" + result.getId());
        return ResponseEntity.created(new URI("/api/treaties/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("treaty", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /treaties : Updates an existing treaty.
     *
     * @param treaty the treaty to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated treaty,
     * or with status 400 (Bad Request) if the treaty is not valid,
     * or with status 500 (Internal Server Error) if the treaty couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaties",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Treaty> updateTreaty(@Valid @RequestBody Treaty treaty) throws URISyntaxException {
        log.debug("REST request to update Treaty : {}", treaty);
        if (treaty.getId() == null) {
            return createTreaty(treaty);
        }

        Treaty result = treatyRepository.save(treaty);
        treatySearchRepository.save(result);
        auditTreatyRepository.save(getAuditTreaty(treaty,AuditTreatyAction.UPDATE.name()));
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("treaty", treaty.getId().toString()))
            .body(result);
    }

    /**
     * GET  /treaties : get all the treaties.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of treaties in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/treaties",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Treaty>> getAllTreaties(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Treaties");
        Page<Treaty> page = treatyRepository.findAllByIsActive(1,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treaties");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }



  /**
   * GET  /treaties : get all the treaties.
   *
   *
   * @return the ResponseEntity with status 200 (OK) and the list of treaties in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/treaties/notification",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public List<Treaty> getAllTreatiesNotification()
    throws URISyntaxException {
    log.debug("REST request to get all");
    List<Treaty> Treaties = treatyRepository.findAllByNotificationStatus(5);
    return Treaties;
  }


  /**
   * GET  /treaties/nopage : get all the treaties. No Pagination
   *
   * @return the ResponseEntity with status 200 (OK) and the list of treaties in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/treaties/nopage",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public List<Treaty> getAllTreaties() {
    log.debug("REST request to get all Treaties");
    List<Treaty> Treaties = treatyRepository.findAllByIsActive(1);
    return Treaties;
  }
  /**
     * GET  /treaties/:id : get the "id" treaty.
     *
     * @param id the id of the treaty to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the treaty, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/treaties/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Treaty> getTreaty(@PathVariable Long id) {
        log.debug("REST request to get Treaty : {}", id);
        Treaty treaty = treatyRepository.findOne(id);
        return Optional.ofNullable(treaty)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /treaties/deals/ -> get all the deals.
     */
    @RequestMapping(value = "/treaties/deals/{dealId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Treaty>> getAllTreatiesByDealId(@PathVariable Long dealId,Pageable pageable) throws URISyntaxException {
        log.debug("REST request to get all Treaties by Deal");

      Page<Treaty> treaties = treatyRepository.findAllByDealIdAndIsActive(dealId,1,pageable);

      HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(treaties, "/api/treaties/deals");
      return new ResponseEntity<>(treaties.getContent(), headers, HttpStatus.OK);

    }

  /**
   * GET  /treatiesCount/deals/{dealId} -> get all the deals.
   */
  @RequestMapping(value = "/treatiesCount/deals/{dealId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Count> getTreatiesCountByDealId(@PathVariable Long dealId) throws URISyntaxException {
    log.debug("REST request to get all Treaties by Deal");

    Long treatiesCount = treatyRepository.countByDealIdAndIsActive(dealId,1);

    return new ResponseEntity<>(new Count(treatiesCount), HttpStatus.OK);
  }


  /**
   * GET  /treaties/deals/ -> get all the deals.
   */
  @RequestMapping(value = "/treaty/uniqueTreatyName",
    method = RequestMethod.PUT,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public  List <Treaty>  getAllTreatiesByTreatyName(@Valid @RequestBody  String treatyName) throws URISyntaxException {
    log.debug("REST request to get all Treaties by Deal"+treatyName);

    List <Treaty> treaties = treatyRepository.findAllByTreatyNameIgnoreCaseAndIsActive(treatyName,1);
    return treaties;

  }

    /**
     * DELETE  /treaties/:id : delete the "id" treaty.
     *
     * @param id the id of the treaty to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/treaties/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTreaty(@PathVariable Long id) {
        log.debug("REST request to delete Treaty : {}", id);
        auditTreatyRepository.save(getAuditTreaty(treatyRepository.findOne(id), AuditTreatyAction.DELETE.name()));
        treatyRepository.delete(id);
        treatySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("treaty", id.toString())).build();
    }

    /**
     * SEARCH  /_search/treaties?query=:query : search for the treaty corresponding
     * to the query.
     *
     * @param query the query of the treaty search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/treaties",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Treaty>> searchTreaties(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Treaties for query {}", query);
        Page<Treaty> page = treatySearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/treaties");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    private AuditTreaty getAuditTreaty(@RequestBody Treaty treaty, String auditTreatyActionType) {
      AuditTreaty auditTreaty = new AuditTreaty();
      auditTreaty.setAction(auditTreatyActionType);
      auditTreaty.setTreatyId(treaty.getId());
      auditTreaty.setCreationTs(ZonedDateTime.now());
      auditTreaty.setCreationUser(treaty.getLastUser());
      auditTreaty.setName(AuditTreatyName.TREATY.name());
      auditTreaty.setComments(treaty.toString());
      System.out.println(treaty.toString());
      return auditTreaty;
    }
}
