package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.AllocationType;
import com.rgare.global.finre.repository.AllocationTypeRepository;
import com.rgare.global.finre.repository.search.AllocationTypeSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AllocationType.
 */
@RestController
@RequestMapping("/api")
public class AllocationTypeResource {

    private final Logger log = LoggerFactory.getLogger(AllocationTypeResource.class);

    private static final String ENTITY_NAME = "allocationType";

    private AllocationTypeRepository allocationTypeRepository;

    private AllocationTypeSearchRepository allocationTypeSearchRepository;

    public AllocationTypeResource(AllocationTypeRepository allocationTypeRepository, AllocationTypeSearchRepository allocationTypeSearchRepository) {
        this.allocationTypeRepository = allocationTypeRepository;
        this.allocationTypeSearchRepository = allocationTypeSearchRepository;
    }

    /**
     * POST  /allocation-types : Create a new allocationType.
     *
     * @param allocationType the allocationType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new allocationType, or with status 400 (Bad Request) if the allocationType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/allocation-types",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AllocationType> createAllocationType(@RequestBody AllocationType allocationType) throws URISyntaxException {
        log.debug("REST request to save AllocationType : {}", allocationType);
        if (allocationType.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new allocationType cannot already have an ID")).body(null);
        }
        AllocationType result = allocationTypeRepository.save(allocationType);
        allocationTypeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/allocation-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /allocation-types : Updates an existing allocationType.
     *
     * @param allocationType the allocationType to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated allocationType,
     * or with status 400 (Bad Request) if the allocationType is not valid,
     * or with status 500 (Internal Server Error) if the allocationType couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/allocation-types",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AllocationType> updateAllocationType(@RequestBody AllocationType allocationType) throws URISyntaxException {
        log.debug("REST request to update AllocationType : {}", allocationType);
        if (allocationType.getId() == null) {
            return createAllocationType(allocationType);
        }
        AllocationType result = allocationTypeRepository.save(allocationType);
        allocationTypeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, allocationType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /allocation-types : get all the allocationTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of allocationTypes in body
     */
    @RequestMapping(value = "/allocation-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<AllocationType> getAllAllocationTypes() {
        log.debug("REST request to get all AllocationTypes");
        List<AllocationType> allocationTypes = allocationTypeRepository.findAll();
        return allocationTypes;
    }

    /**
     * GET  /allocation-types/:id : get the "id" allocationType.
     *
     * @param id the id of the allocationType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the allocationType, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/allocation-types/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AllocationType> getAllocationType(@PathVariable Long id) {
        log.debug("REST request to get AllocationType : {}", id);
        AllocationType allocationType = allocationTypeRepository.findOne(id);
        return Optional.ofNullable(allocationType)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /allocation-types/:id : delete the "id" allocationType.
     *
     * @param id the id of the allocationType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/allocation-types/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAllocationType(@PathVariable Long id) {
        log.debug("REST request to delete AllocationType : {}", id);
        allocationTypeRepository.delete(id);
        allocationTypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/allocation-types?query=:query : search for the allocationType corresponding
     * to the query.
     *
     * @param query the query of the allocationType search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/allocation-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<AllocationType> searchAllocationTypes(@RequestParam String query) {
        log.debug("REST request to search AllocationTypes for query {}", query);
        return StreamSupport
            .stream(allocationTypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
