package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.Company;
import com.rgare.global.finre.repository.CompanyRepository;
import com.rgare.global.finre.repository.search.CompanySearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing Company.
 */
@RestController
@RequestMapping("/api")
public class   CompanyResource {

    private final Logger log = LoggerFactory.getLogger(CompanyResource.class);

    private static final String ENTITY_NAME = "company";

    private static final String REST = "REST request to get a page of Companies";

    private CompanyRepository companyRepository;

    private CompanySearchRepository companySearchRepository;

    private Dp3LogService dp3LogService;

    public CompanyResource(CompanyRepository companyRepository, CompanySearchRepository companySearchRepository,
                           Dp3LogService dp3LogService) {
        this.companyRepository = companyRepository;
        this.companySearchRepository = companySearchRepository;
        this.dp3LogService = dp3LogService;
    }

    /**
     * POST  /companies : Create a new company.
     *
     * @param company the company to create
     * @return the ResponseEntity with status 201 (Created) and with body the new company, or with status 400 (Bad Request) if the company has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/companies",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Company> createCompany(@Valid @RequestBody Company company) throws URISyntaxException {
        log.debug("REST request to save Company : {}", company);
        if (company.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new company cannot already have an ID")).body(null);
        }
     //  dp3LogService.postDp3log("SAVING COMPANY", "/api/companies/", "Save Company Name:" + company.getName());
        Company result = companyRepository.save(company);
        companySearchRepository.save(result);

        return ResponseEntity.created(new URI("/api/companies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /companies : Updates an existing company.
     *
     * @param company the company to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated company,
     * or with status 400 (Bad Request) if the company is not valid,
     * or with status 500 (Internal Server Error) if the company couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/companies",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Company> updateCompany(@Valid @RequestBody Company company) throws URISyntaxException {
        log.debug("REST request to update Company : {}", company);
        if (company.getId() == null) {
            return createCompany(company);
        }
        Company result = companyRepository.save(company);
        companySearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, company.getId().toString()))
            .body(result);
    }

    /**
     * GET  /companies : get all the companies.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of companies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/companies",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Company>> getAllCompanies(Pageable pageable)
        throws URISyntaxException {
        log.debug(REST);
        Page<Company> page = companyRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/companies");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

  /**
   * GET  /companies : get all the companies.
   *
   * @param pageable the pagination information
   * @return the ResponseEntity with status 200 (OK) and the list of companies in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
    @RequestMapping(value = "/companies/company-mdm",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Company>> getAllMDMCompanies(Pageable pageable)
      throws URISyntaxException {
      log.debug(REST);
      Page<Company> page = companyRepository.searchAllMDMCompaniesQuery(pageable);
      HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/companies/company-mdm");
      return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

  /**
   * GET  /companies : get all the companies.
   *
   * @param pageable the pagination information
   * @return the ResponseEntity with status 200 (OK) and the list of companies in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/companies/company-mdm/search/{searchString}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<Company>> SearchAllMDMCompanies(@PathVariable String searchString, Pageable pageable)
    throws URISyntaxException {
    log.debug(REST);
    Page<Company> page = companyRepository.searchClientCompanyIdIsNotNullQuery(searchString, pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/companies/company-mdm");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
  }




  /**
   * GET  /companies : get all the companies.
   *
   * @param pageable the pagination information
   * @return the ResponseEntity with status 200 (OK) and the list of companies in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/company-mdm/search/new/{searchString}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<Company>> searchExistingMdmCompanyQuery(@PathVariable String searchString, Pageable pageable)
    throws URISyntaxException {
    log.debug(REST);
    Page<Company> page = companyRepository.searchExistingMdmCompanyQuery(searchString, pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/company-mdm/search/new");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
  }

    /**
     * GET  /companies : get all the companies.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of companies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/companies/non-mdm",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Company>> getAllNonMDMCompanies(Pageable pageable)
      throws URISyntaxException {
      log.debug(REST);
      Page<Company> page = companyRepository.searchAllNonMDMCompaniesQuery(pageable);
      HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/companies/non-mdm");
      return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

  /**
   * GET  /companies : get all the companies.
   *
   * @param pageable the pagination information
   * @return the ResponseEntity with status 200 (OK) and the list of companies in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/companies/non-mdm/search/{searchString}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<Company>> SearchAllNonMDMCompanies(@PathVariable String searchString,Pageable pageable)
    throws URISyntaxException {
    log.debug("REST request to Search all Non MDM Companies");
    Page<Company> page = companyRepository.searchClientCompanyIdIsNullQuery(searchString, pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/companies/non-mdm/search");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
  }

    /**
     * GET  /companies/:id : get the "id" company.
     *
     * @param id the id of the company to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the company, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/companies/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Company> getCompany(@PathVariable Long id) {
        log.debug("REST request to get Company : {}", id);
        Company company = companyRepository.findOne(id);
        return Optional.ofNullable(company)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /companies/search : get all the Companies.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of clientCompanies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/companies/search/{searchString}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Company>> searchCompaniesByName(@PathVariable String searchString,  Pageable pageable)
      throws URISyntaxException {
      log.debug("REST request to get a page of Company searches");
      Page<Company> page = companyRepository.searchCompaniesQuery(searchString,pageable);
      HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/companies/search");
      return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /companies/rga-companies : get all by RGA Companies.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of deals in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/companies/rga-companies",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Company>> getAllRGACompanies()
      throws URISyntaxException {
      log.debug("REST request to get a All RGA Companies");
      List<Company> page = companyRepository.findByIsRgaCompany(1);
      return Optional.ofNullable(page)
        .map(result -> new ResponseEntity<>(
          result,
          HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    /**
     * DELETE  /companies/:id : delete the "id" company.
     *
     * @param id the id of the company to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/companies/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteCompany(@PathVariable Long id) {
        log.debug("REST request to delete Company : {}", id);
        companyRepository.delete(id);
        companySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/companies?query=:query : search for the company corresponding
     * to the query.
     *
     * @param query the query of the company search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/companies",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Company>> searchCompanies(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Companies for query {}", query);
        Page<Company> page = companySearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/companies");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

  /**
   * SEARCH  /_search/companies?query=:query : search for the company corresponding
   * to the query.
   *
   * @param pageable the query of the company search
   * @return the result of the search
   */
  @RequestMapping(value = "/_search/companies/mdm",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<Company>> searchMdmCompanies(Pageable pageable)
    throws URISyntaxException {
    log.debug("REST request to search for a page of MDM Companies for query {}");
    Page<Company> page = companySearchRepository.findByClientCompanyId(null, pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/_search/companies/mdm");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
  }

}
