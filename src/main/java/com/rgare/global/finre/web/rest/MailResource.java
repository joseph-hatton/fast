package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.Deal;
import com.rgare.global.finre.domain.Treaty;
import com.rgare.global.finre.domain.TreatyTransactionCash;
import com.rgare.global.finre.repository.DealRepository;
import com.rgare.global.finre.repository.search.DealSearchRepository;
//import com.rgare.global.finre.service.EmailService;
import com.rgare.global.finre.service.EmailService;
import com.rgare.global.finre.service.MailService;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing Deal.
 */
@RestController
@RequestMapping("/api")
public class MailResource {

    private final Logger log = LoggerFactory.getLogger(MailResource.class);

    private EmailService emailService;

    public MailResource(EmailService emailService) {
        this.emailService = emailService;
    }

    /**
     * POST  /email  : Create a new email message.
     *
     * @param treaty the treaty to pass
     * @return the ResponseEntity with status 201 (Created) and with body the treaty, or with status 400 (Bad Request) if the treaty is null
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/email",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Treaty> sendEmail(@RequestBody Treaty treaty) throws URISyntaxException {
        log.debug("REST request to save Deal : {}", treaty);
//        emailService.sendEmail("jhatton@rgare.com","Subject Test","<html><strong>test Test</strong></html>",true,false);
        return ResponseEntity.created(new URI("/api/email/" + treaty.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("email", treaty.getId().toString()))
            .body(treaty);
    }

    /**
     * POST  /email  : Create a new email message.
     *
     * @param treatyTransactionCash the treatyTransactionCash to pass
     * @return the ResponseEntity with status 201 (Created) and with body the treaty, or with status 400 (Bad Request) if the treaty is null
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/email/transactions",
      method = RequestMethod.POST,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyTransactionCash> sendEmailTemplate(@RequestBody TreatyTransactionCash treatyTransactionCash) throws URISyntaxException {
      log.debug("REST request to send Cash Transaction to Fianace : {}", treatyTransactionCash);

      //USE TEMPLATEID TO BE USED IN FORMULATING THE EMAIL!

      emailService.sendEmailCashTransaction(treatyTransactionCash);
      return ResponseEntity.created(new URI("/api/email/transactions/" + treatyTransactionCash.getId()))
        .headers(HeaderUtil.createEntityCreationAlert("email", treatyTransactionCash.getId().toString()))
        .body(treatyTransactionCash);
    }

  /**
   * POST  /email  : Create a new email message.
   *
   * @param treatyTransactionCash the treatyTransactionCash to pass
   * @return the ResponseEntity with status 201 (Created) and with body the treaty, or with status 400 (Bad Request) if the treaty is null
   * @throws URISyntaxException if the Location URI syntax is incorrect
   */
  @RequestMapping(value = "/email/transactions/attachment",
    method = RequestMethod.POST,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<TreatyTransactionCash> sendEmailAttachmentTemplate(
    @RequestBody TreatyTransactionCash treatyTransactionCash)
    throws URISyntaxException {
    log.debug("REST request to send Cash Transaction to Fianace : {}", treatyTransactionCash);

    //USE TEMPLATEID TO BE USED IN FORMULATING THE EMAIL!

    emailService.sendEmailTransactionWithAttachment(treatyTransactionCash);
    return ResponseEntity.created(new URI("/api/email/transactions/attachment/" + treatyTransactionCash.getId()))
      .headers(HeaderUtil.createEntityCreationAlert("email", treatyTransactionCash.getId().toString()))
      .body(treatyTransactionCash);
  }
}
