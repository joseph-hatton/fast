package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.ContactType;
import com.rgare.global.finre.repository.ContactTypeRepository;
import com.rgare.global.finre.repository.search.ContactTypeSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ContactType.
 */
@RestController
@RequestMapping("/api")
public class ContactTypeResource {

    private final Logger log = LoggerFactory.getLogger(ContactTypeResource.class);

    private static final String ENTITY_NAME = "contactType";

    private ContactTypeRepository contactTypeRepository;

    private ContactTypeSearchRepository contactTypeSearchRepository;

    public ContactTypeResource(ContactTypeRepository contactTypeRepository, ContactTypeSearchRepository contactTypeSearchRepository) {
        this.contactTypeRepository = contactTypeRepository;
        this.contactTypeSearchRepository = contactTypeSearchRepository;
    }

    /**
     * POST  /contact-types : Create a new contactType.
     *
     * @param contactType the contactType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new contactType, or with status 400 (Bad Request) if the contactType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/contact-types",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ContactType> createContactType(@Valid @RequestBody ContactType contactType) throws URISyntaxException {
        log.debug("REST request to save ContactType : {}", contactType);
        if (contactType.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new contactType cannot already have an ID")).body(null);
        }
        ContactType result = contactTypeRepository.save(contactType);
        contactTypeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/contact-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /contact-types : Updates an existing contactType.
     *
     * @param contactType the contactType to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated contactType,
     * or with status 400 (Bad Request) if the contactType is not valid,
     * or with status 500 (Internal Server Error) if the contactType couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/contact-types",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ContactType> updateContactType(@Valid @RequestBody ContactType contactType) throws URISyntaxException {
        log.debug("REST request to update ContactType : {}", contactType);
        if (contactType.getId() == null) {
            return createContactType(contactType);
        }
        ContactType result = contactTypeRepository.save(contactType);
        contactTypeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, contactType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /contact-types : get all the contactTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of contactTypes in body
     */
    @RequestMapping(value = "/contact-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ContactType> getAllContactTypes() {
        log.debug("REST request to get all ContactTypes");
        List<ContactType> contactTypes = contactTypeRepository.findAll();
        return contactTypes;
    }

    /**
     * GET  /contact-types/:id : get the "id" contactType.
     *
     * @param id the id of the contactType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the contactType, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/contact-types/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ContactType> getContactType(@PathVariable Long id) {
        log.debug("REST request to get ContactType : {}", id);
        ContactType contactType = contactTypeRepository.findOne(id);
        return Optional.ofNullable(contactType)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /contact-types/:id : delete the "id" contactType.
     *
     * @param id the id of the contactType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/contact-types/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteContactType(@PathVariable Long id) {
        log.debug("REST request to delete ContactType : {}", id);
        contactTypeRepository.delete(id);
        contactTypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/contact-types?query=:query : search for the contactType corresponding
     * to the query.
     *
     * @param query the query of the contactType search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/contact-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ContactType> searchContactTypes(@RequestParam String query) {
        log.debug("REST request to search ContactTypes for query {}", query);
        return StreamSupport
            .stream(contactTypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
