package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.AgreementType;
import com.rgare.global.finre.repository.AgreementTypeRepository;
import com.rgare.global.finre.repository.search.AgreementTypeSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AgreementType.
 */
@RestController
@RequestMapping("/api")
public class AgreementTypeResource {

    private final Logger log = LoggerFactory.getLogger(AgreementTypeResource.class);

    private static final String ENTITY_NAME = "agreementType";

    private AgreementTypeRepository agreementTypeRepository;

    private AgreementTypeSearchRepository agreementTypeSearchRepository;

    public AgreementTypeResource(AgreementTypeRepository agreementTypeRepository, AgreementTypeSearchRepository agreementTypeSearchRepository) {
        this.agreementTypeRepository = agreementTypeRepository;
        this.agreementTypeSearchRepository = agreementTypeSearchRepository;
    }

    /**
     * POST  /agreement-types : Create a new agreementType.
     *
     * @param agreementType the agreementType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new agreementType, or with status 400 (Bad Request) if the agreementType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/agreement-types",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AgreementType> createAgreementType(@Valid @RequestBody AgreementType agreementType) throws URISyntaxException {
        log.debug("REST request to save AgreementType : {}", agreementType);
        if (agreementType.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new agreementType cannot already have an ID")).body(null);
        }
        AgreementType result = agreementTypeRepository.save(agreementType);
        agreementTypeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/agreement-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /agreement-types : Updates an existing agreementType.
     *
     * @param agreementType the agreementType to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated agreementType,
     * or with status 400 (Bad Request) if the agreementType is not valid,
     * or with status 500 (Internal Server Error) if the agreementType couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/agreement-types",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AgreementType> updateAgreementType(@Valid @RequestBody AgreementType agreementType) throws URISyntaxException {
        log.debug("REST request to update AgreementType : {}", agreementType);
        if (agreementType.getId() == null) {
            return createAgreementType(agreementType);
        }
        AgreementType result = agreementTypeRepository.save(agreementType);
        agreementTypeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, agreementType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /agreement-types : get all the agreementTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of agreementTypes in body
     */
    @RequestMapping(value = "/agreement-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<AgreementType> getAllAgreementTypes() {
        log.debug("REST request to get all AgreementTypes");
        List<AgreementType> agreementTypes = agreementTypeRepository.findAll();
        return agreementTypes;
    }

    /**
     * GET  /agreement-types/:id : get the "id" agreementType.
     *
     * @param id the id of the agreementType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the agreementType, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/agreement-types/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AgreementType> getAgreementType(@PathVariable Long id) {
        log.debug("REST request to get AgreementType : {}", id);
        AgreementType agreementType = agreementTypeRepository.findOne(id);
        return Optional.ofNullable(agreementType)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /agreement-types/:id : delete the "id" agreementType.
     *
     * @param id the id of the agreementType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/agreement-types/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAgreementType(@PathVariable Long id) {
        log.debug("REST request to delete AgreementType : {}", id);
        agreementTypeRepository.delete(id);
        agreementTypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/agreement-types?query=:query : search for the agreementType corresponding
     * to the query.
     *
     * @param query the query of the agreementType search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/agreement-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<AgreementType> searchAgreementTypes(@RequestParam String query) {
        log.debug("REST request to search AgreementTypes for query {}", query);
        return StreamSupport
            .stream(agreementTypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
