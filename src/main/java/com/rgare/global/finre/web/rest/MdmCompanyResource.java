package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.Company;
import com.rgare.global.finre.domain.MdmCompany;
import com.rgare.global.finre.repository.CompanyRepository;
import com.rgare.global.finre.repository.MdmCompanyRepository;
import com.rgare.global.finre.repository.search.CompanySearchRepository;
import com.rgare.global.finre.service.Dp3LogService;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static net.logstash.logback.marker.Markers.append;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for existing MDMCompany Search.
 */
@RestController
@RequestMapping("/api")
public class MdmCompanyResource {

    private final Logger log = LoggerFactory.getLogger(MdmCompanyResource.class);

    private static final String ENTITY_NAME = "mdmCompany";

    private MdmCompanyRepository mdmCompanyRepository;

    private CompanyRepository companyRepository;

    public MdmCompanyResource(MdmCompanyRepository mdmCompanyRepository, CompanyRepository companyRepository) {
        this.mdmCompanyRepository = mdmCompanyRepository;
        this.companyRepository = companyRepository;
    }

  /**
   * GET  /companies : get all existing mdmcompanies.
   *
   * @param pageable the pagination information
   * @return the ResponseEntity with status 200 (OK) and the list of existing mdmcompanies in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/mdmcompanies/existing-mdm/search/{searchString}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<MdmCompany>> SearchExistingMDMCompanies(@PathVariable String searchString, Pageable pageable)
    throws URISyntaxException {
    log.debug(append("searchString", searchString)
      .and(append("pageable", pageable)), "REST request to get a page of Companies");
    Page<MdmCompany> page = mdmCompanyRepository.searchExistingMdmCompanyQuery(searchString, pageable);
    log.debug(append("searchString", searchString)
      .and(append("pageable", pageable))
      .and(append("mdmCompany", page.getContent())),"got mdm companies");
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/companies/existing-mdm");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
  }

  /**
   * GET  /refresh-existing-mdmcompanies : refresh all existing mdmcompanies.
   *
   * @return the ResponseEntity with status 200 (OK) and the list of updated mdmcompanies in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/refresh-existing-mdmcompanies",
  method = RequestMethod.GET,
  produces = MediaType.APPLICATION_JSON_VALUE)
@Timed
public ResponseEntity<List<Company>> refreshExistingMdmCompanies()
  throws URISyntaxException {
  log.debug("Request to update all mdm companies");

  List<Company> oldCompanies = companyRepository.getAllMDMCompanies();
  Map<Long, MdmCompany> newCompanies = mdmCompanyRepository.findAll().stream().collect(Collectors.toMap(MdmCompany::getMdmId, c -> c));

  List<Company> updated = new ArrayList<>();

  for (Company company : oldCompanies){
    MdmCompany newCompany = newCompanies.get(company.getMdmId());
    if(newCompany != null && 
    ((company.getVersionDate() == null || !company.getVersionDate().equals(newCompany.getVersionDate())) || 
    (company.getVersionNumber() == null || !company.getVersionNumber().equals(newCompany.getVersionNumber())))){
      company.setName(newCompany.getCompanyName());
      company.setClientCompanyId(newCompany.getClientCompanyId());
      company.setLegalCode(newCompany.getLegalCode());
      company.setLegalName(newCompany.getLegalName());
      company.setAffiliateName(newCompany.getAffiliatename());
      company.setVersionDate(newCompany.getVersionDate());
      company.setVersionNumber(newCompany.getVersionNumber());

      companyRepository.save(company);

      updated.add(company);
    }
  }

  return new ResponseEntity<>(updated, HttpStatus.OK);
}
 }
