package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.*;
import com.rgare.global.finre.repository.StatInfoRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing StatInfo.
 */
@RestController
@RequestMapping("/api")
public class StatInfoResource {
    private StatInfoRepository statInfoRepository;

    public StatInfoResource(StatInfoRepository statInfoRepository) {
        this.statInfoRepository = statInfoRepository;
    }
    /**
     * POST  /intro : Create a new StatInfo.
     *
     * @param statInfo the StatInfo to create
     * @return the ResponseEntity with status 201 (Created) and with body the new treaty, or with status 400 (Bad Request) if the treaty has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/stat-info",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<StatInfo> createStatInfo(@Valid @RequestBody StatInfo statInfo) throws URISyntaxException {
        if (statInfo.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("statInfo", "idexists", "A new StatInfo cannot already have an ID")).body(null);
        }
        
        StatInfo result = statInfoRepository.save(statInfo);

        return ResponseEntity.created(new URI("/api/stat-info/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("stat-info", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /stat-info : Updates an existing treaty.
     *
     * @param statInfo the StatInfo to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated treaty,
     * or with status 400 (Bad Request) if the treaty is not valid,
     * or with status 500 (Internal Server Error) if the treaty couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/stat-info",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<StatInfo> updateStatInfo(@Valid @RequestBody StatInfo statInfo) throws URISyntaxException {
        if (statInfo.getId() == null) {
            return createStatInfo(statInfo);
        }

        StatInfo result = statInfoRepository.save(statInfo);
    
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("statInfo", statInfo.getId().toString()))
            .body(result);
    }


  /**
   * GET  /stat-info : get all the StatInfos. No Pagination
   *
   * @return the ResponseEntity with status 200 (OK) and the list of treaties in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/stat-info",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<StatInfo>> getAllStatInfo() {
    List<StatInfo> pages = statInfoRepository.findAll();

    return new ResponseEntity<>(pages, null, HttpStatus.OK);
  }

  /**
   * GET  /stat-info/:title : get the StatInfo for this title
   *
   * @return the ResponseEntity with status 200 (OK) and the list of treaties in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/stat-info/{title}",
  method = RequestMethod.GET,
  produces = MediaType.APPLICATION_JSON_VALUE)
@Timed
public ResponseEntity<List<StatInfo>> getStatInfo(@PathVariable String title) {
    List<StatInfo> page = statInfoRepository.findAllByTitleLikeAndIsActive(title, 1);

    return new ResponseEntity<>(page, null, HttpStatus.OK);
}

}
