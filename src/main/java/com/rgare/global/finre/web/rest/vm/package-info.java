/**
 * View Models used by Spring MVC REST controllers.
 */
package com.rgare.global.finre.web.rest.vm;
