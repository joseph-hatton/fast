package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.Frequency;
import com.rgare.global.finre.repository.FrequencyRepository;
import com.rgare.global.finre.repository.search.FrequencySearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Frequency.
 */
@RestController
@RequestMapping("/api")
public class FrequencyResource {

    private final Logger log = LoggerFactory.getLogger(FrequencyResource.class);

    private static final String ENTITY_NAME = "frequency";

    private FrequencyRepository frequencyRepository;

    private FrequencySearchRepository frequencySearchRepository;

    public FrequencyResource(FrequencyRepository frequencyRepository, FrequencySearchRepository frequencySearchRepository) {
        this.frequencyRepository = frequencyRepository;
        this.frequencySearchRepository = frequencySearchRepository;
    }

    /**
     * POST  /frequencies : Create a new frequency.
     *
     * @param frequency the frequency to create
     * @return the ResponseEntity with status 201 (Created) and with body the new frequency, or with status 400 (Bad Request) if the frequency has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/frequencies",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Frequency> createFrequency(@Valid @RequestBody Frequency frequency) throws URISyntaxException {
        log.debug("REST request to save Frequency : {}", frequency);
        if (frequency.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("frequency", "idexists", "A new frequency cannot already have an ID")).body(null);
        }
        Frequency result = frequencyRepository.save(frequency);
        frequencySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/frequencies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("frequency", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /frequencies : Updates an existing frequency.
     *
     * @param frequency the frequency to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated frequency,
     * or with status 400 (Bad Request) if the frequency is not valid,
     * or with status 500 (Internal Server Error) if the frequency couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/frequencies",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Frequency> updateFrequency(@Valid @RequestBody Frequency frequency) throws URISyntaxException {
        log.debug("REST request to update Frequency : {}", frequency);
        if (frequency.getId() == null) {
            return createFrequency(frequency);
        }
        Frequency result = frequencyRepository.save(frequency);
        frequencySearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("frequency", frequency.getId().toString()))
            .body(result);
    }

    /**
     * GET  /frequencies : get all the frequencies.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of frequencies in body
     */
    @RequestMapping(value = "/frequencies",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Frequency> getAllFrequencies() {
        log.debug("REST request to get all Frequencies");
        List<Frequency> frequencies = frequencyRepository.findAll();
        return frequencies;
    }

    /**
     * GET  /frequencies/:id : get the "id" frequency.
     *
     * @param id the id of the frequency to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the frequency, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/frequencies/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Frequency> getFrequency(@PathVariable Long id) {
        log.debug("REST request to get Frequency : {}", id);
        Frequency frequency = frequencyRepository.findOne(id);
        return Optional.ofNullable(frequency)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /frequencies/:id : delete the "id" frequency.
     *
     * @param id the id of the frequency to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/frequencies/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteFrequency(@PathVariable Long id) {
        log.debug("REST request to delete Frequency : {}", id);
        frequencyRepository.delete(id);
        frequencySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("frequency", id.toString())).build();
    }

    /**
     * SEARCH  /_search/frequencies?query=:query : search for the frequency corresponding
     * to the query.
     *
     * @param query the query of the frequency search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/frequencies",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Frequency> searchFrequencies(@RequestParam String query) {
        log.debug("REST request to search Frequencies for query {}", query);
        return StreamSupport
            .stream(frequencySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
