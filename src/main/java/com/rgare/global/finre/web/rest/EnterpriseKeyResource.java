package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.EnterpriseKey;
import com.rgare.global.finre.repository.EnterpriseKeyRepository;
import com.rgare.global.finre.repository.MdmAttributesRepository;
import com.rgare.global.finre.repository.search.MdmAttributesSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing EnterpriseKey.
 */
@RestController
@RequestMapping("/api")
public class EnterpriseKeyResource {

    private final Logger log = LoggerFactory.getLogger(EnterpriseKeyResource.class);

    private static final String ENTITY_NAME = "enterpriseKey";

    private EnterpriseKeyRepository enterpriseKeyRepository;

    public EnterpriseKeyResource(EnterpriseKeyRepository enterpriseKeyRepository) {
        this.enterpriseKeyRepository = enterpriseKeyRepository;
    }

    /**
     * GET  /enterprise-keys : get all the enterpriseKeys.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of enterpriseKeys in body
     */
    @RequestMapping(value = "/enterprise-keys",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<EnterpriseKey> getAllEnterpriseKeys() {
        log.debug("REST request to get all EnterpriseKeys");
        List<EnterpriseKey> enterpriseKeys = enterpriseKeyRepository.findAll();
        return enterpriseKeys;
    }

    /**
   * GET  /enterprise-keys/:id : get the "id" enterpriseKey.
   *
   * @param id the id of the enterpriseKey to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the enterpriseKey, or with status 404 (Not Found)
   */
  @RequestMapping(value = "/enterprise-keys/{id}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public EnterpriseKey getEnterpriseKey(@PathVariable Long id) {
    log.debug("REST request to get EnterpriseKey : {}", id);
    EnterpriseKey enterpriseKey = enterpriseKeyRepository.findOneByEnterpriseId(id);
    return enterpriseKey;
  }

  /**
   * GET  /enterprise-keys/:id : get the "id" enterpriseKey.
   *
   * @param id the id of the enterpriseKey to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the enterpriseKey, or with status 404 (Not Found)
   */
  @RequestMapping(value = "/enterprise-keys/{id}/create/{hbcreatedvid}/change/{hbchangedvid}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<EnterpriseKey> getEnterpriseKey(@PathVariable Long id, @PathVariable Long hbcreatedvid, @PathVariable Long hbchangedvid) {
    log.debug("REST request to get EnterpriseKey : {}", id);
    EnterpriseKey enterpriseKey = enterpriseKeyRepository.findOneByEnterpriseId(id);

    if(enterpriseKey.getVersionNumber()!=null){
      Long ekChangedVid = enterpriseKey.getVersionNumber();
      if(ekChangedVid.equals(hbchangedvid)){
        if(enterpriseKey.getVersionDate()!=null){
          Long ekCreatedVid = enterpriseKey.getVersionDate();
          if(ekCreatedVid.equals(hbcreatedvid)){
            return new ResponseEntity<>(HttpStatus.OK);
          }else{
            return Optional.ofNullable(enterpriseKey)
              .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
              .orElse(new ResponseEntity<>(HttpStatus.OK));
          }
        }else{
          return new ResponseEntity<>(HttpStatus.OK);
        }
      } else{
        return Optional.ofNullable(enterpriseKey)
          .map(result -> new ResponseEntity<>(
            result,
            HttpStatus.OK))
          .orElse(new ResponseEntity<>(HttpStatus.OK));
      }
    }

    return Optional.ofNullable(enterpriseKey)
      .map(result -> new ResponseEntity<>(
        result,
        HttpStatus.OK))
    .orElse(new ResponseEntity<>(HttpStatus.OK));
  }
}
