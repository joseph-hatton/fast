package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.*;
import com.rgare.global.finre.repository.AuditRiskFeeRepository;
import com.rgare.global.finre.repository.AuditTreatyRepository;
import com.rgare.global.finre.repository.TreatyManagementRepository;
import com.rgare.global.finre.repository.search.TreatyManagementSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TreatyManagement.
 */
@RestController
@RequestMapping("/api")
public class TreatyManagementResource {

    private final Logger log = LoggerFactory.getLogger(TreatyManagementResource.class);

    private static final String ENTITY_NAME = "treatyManagement";

    private TreatyManagementRepository treatyManagementRepository;

    private TreatyManagementSearchRepository treatyManagementSearchRepository;

    private AuditTreatyRepository auditTreatyRepository;

    private AuditRiskFeeRepository auditRiskFeeRepository;

    public TreatyManagementResource(TreatyManagementRepository treatyManagementRepository,
                                    TreatyManagementSearchRepository treatyManagementSearchRepository,
                                    AuditTreatyRepository auditTreatyRepository,
                                    AuditRiskFeeRepository auditRiskFeeRepository) {
        this.treatyManagementRepository = treatyManagementRepository;
        this.treatyManagementSearchRepository = treatyManagementSearchRepository;
        this.auditTreatyRepository = auditTreatyRepository;
        this.auditRiskFeeRepository = auditRiskFeeRepository;
    }

    /**
     * POST  /treaty-managements : Create a new treatyManagement.
     *
     * @param treatyManagement the treatyManagement to create
     * @return the ResponseEntity with status 201 (Created) and with body the new treatyManagement, or with status 400 (Bad Request) if the treatyManagement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaty-managements",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyManagement> createTreatyManagement(@RequestBody TreatyManagement treatyManagement) throws URISyntaxException {
        log.debug("REST request to save TreatyManagement : {}", treatyManagement);
        if (treatyManagement.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("treatyManagement", "idexists", "A new treatyManagement cannot already have an ID")).body(null);
        }
        TreatyManagement result = treatyManagementRepository.save(treatyManagement);
        treatyManagementSearchRepository.save(result);
        auditTreatyRepository.save(getAuditTreaty(treatyManagement,AuditTreatyAction.SAVE.name()));
        saveRiskFee(result.getRiskFees(),treatyManagement.getTreaty().getTreatyStatus().getName(),treatyManagement.getLastUser() ,treatyManagement.getInputStatus());
        return ResponseEntity.created(new URI("/api/treaty-managements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("treatyManagement", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /treaty-managements : Updates an existing treatyManagement.
     *
     * @param treatyManagement the treatyManagement to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated treatyManagement,
     * or with status 400 (Bad Request) if the treatyManagement is not valid,
     * or with status 500 (Internal Server Error) if the treatyManagement couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaty-managements",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyManagement> updateTreatyManagement(@RequestBody TreatyManagement treatyManagement) throws URISyntaxException {
        log.debug("REST request to update TreatyManagement : {}", treatyManagement);
        if (treatyManagement.getId() == null) {
            return createTreatyManagement(treatyManagement);
        }
        TreatyManagement result = treatyManagementRepository.save(treatyManagement);
        saveRiskFee(result.getRiskFees(),treatyManagement.getTreaty().getTreatyStatus().getName(),treatyManagement.getLastUser(),treatyManagement.getInputStatus());
        treatyManagementSearchRepository.save(result);
        auditTreatyRepository.save(getAuditTreaty(treatyManagement,AuditTreatyAction.UPDATE.name()));
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("treatyManagement", treatyManagement.getId().toString()))
            .body(result);
    }

  /**
     * GET  /treaty-managements/:id : get the "id" treatyManagement.
     *
     * @param id the id of the treatyManagement to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the treatyManagement, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/treaty-managements/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyManagement> getTreatyManagement(@PathVariable Long id) {
        log.debug("REST request to get TreatyManagement : {}", id);
        TreatyManagement treatyManagement = treatyManagementRepository.findOne(id);
        return Optional.ofNullable(treatyManagement)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

  /**
   * GET  /treaty-managements/treaty/:id : get the "treatyId" treatyManagement.
   *
   * @param treatyId the id of the treatyManagement to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the treatyManagement, or with status 404 (Not Found)
   */
  @RequestMapping(value = "/treaty-managements/treaty/{treatyId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<TreatyManagement> getTreatyManagementByTreatyId(@PathVariable Long treatyId) {
    log.debug("REST request to get TreatyManagement by Treaty: {}", treatyId);
    TreatyManagement treatyManagement = treatyManagementRepository.findOneByTreatyId(treatyId);
    return Optional.ofNullable(treatyManagement)
      .map(result -> new ResponseEntity<>(
        result,
        HttpStatus.OK))
  .orElse(new ResponseEntity<>(HttpStatus.OK));
  }

  /**
   * GET  /treaty-managements/treaty/:id : get the "treatyId" treatyManagement.
   * @return the ResponseEntity with status 200 (OK) and with body the treatyManagement, or with status 404 (Not Found)
   */
  @RequestMapping(value = "/treaty-managements/treatyComplete",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public List<TreatyManagement> completeNotification() {
    log.debug("REST request to get TreatyManagement by Treaty: {}");
    List<TreatyManagement> treatyManagement = treatyManagementRepository.completeNotification();
    return treatyManagement;
  }



    /**
     * DELETE  /treaty-managements/:id : delete the "id" treatyManagement.
     *
     * @param id the id of the treatyManagement to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/treaty-managements/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTreatyManagement(@PathVariable Long id) {
        log.debug("REST request to delete TreatyManagement : {}", id);
        TreatyManagement treatyManagement = treatyManagementRepository.findOne(id);
        auditTreatyRepository.save(getAuditTreaty(treatyManagement,AuditTreatyAction.DELETE.name()));
        treatyManagementRepository.delete(id);
        treatyManagementSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("treatyManagement", id.toString())).build();
    }

    /**
     * SEARCH  /_search/treaty-managements?query=:query : search for the treatyManagement corresponding
     * to the query.
     *
     * @param query the query of the treatyManagement search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/treaty-managements",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TreatyManagement>> searchTreatyManagements(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of TreatyManagements for query {}", query);
        Page<TreatyManagement> page = treatyManagementSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/treaty-managements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    private void saveRiskFee(Set<RiskFee> riskFees ,String treatyStatus , String lastUser ,String managementStatus) {
      for (RiskFee riskFee : riskFees) {
          auditRiskFeeRepository.save(getAuditRiskFee(riskFee,AuditTreatyAction.SAVE.name(),treatyStatus, lastUser, managementStatus));

      }
    }
    private AuditTreaty getAuditTreaty(@RequestBody TreatyManagement treatyManagement, String auditTreatyActionType) {
      AuditTreaty auditTreaty = new AuditTreaty();
      auditTreaty.setAction(auditTreatyActionType);
      auditTreaty.setTreatyId(treatyManagement.getTreaty().getId());
      auditTreaty.setCreationTs(ZonedDateTime.now());
      auditTreaty.setCreationUser(treatyManagement.getTreaty().getCreationUser());
      auditTreaty.setName(AuditTreatyName.TREATY_MANAGEMENT.name());
      auditTreaty.setComments(treatyManagement.toString());
      return auditTreaty;
    }

    private AuditRiskFee getAuditRiskFee(@RequestBody RiskFee riskFee, String auditTreatyActionType, String treatyStatus, String lastUser, String managementStatus) {
      AuditRiskFee auditRiskFee = new AuditRiskFee();
      auditRiskFee.setAction(auditTreatyActionType);
      auditRiskFee.setRiskFeeId(riskFee.getId());
      auditRiskFee.setTreatyId(riskFee.getTreatyId());
      auditRiskFee.setTreatyStatus(treatyStatus);
      auditRiskFee.setManagementInputStatus (managementStatus);
      auditRiskFee.setCreationUser(lastUser);
      auditRiskFee.setComments(riskFee.toString());
      return auditRiskFee;
    }
}
