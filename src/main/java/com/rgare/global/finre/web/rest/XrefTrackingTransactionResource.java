package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.XrefTrackingTransaction;
import com.rgare.global.finre.repository.XrefTrackingTransactionRepository;
import com.rgare.global.finre.repository.search.XrefTrackingTransactionSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing XrefTrackingTransaction.
 */
@RestController
@RequestMapping("/api")
public class XrefTrackingTransactionResource {

    private final Logger log = LoggerFactory.getLogger(XrefTrackingTransactionResource.class);

    private static final String ENTITY_NAME = "xrefTrackingTransaction";

    private XrefTrackingTransactionRepository xrefTrackingTransactionRepository;

    private XrefTrackingTransactionSearchRepository xrefTrackingTransactionSearchRepository;

    public XrefTrackingTransactionResource(XrefTrackingTransactionRepository xrefTrackingTransactionRepository, XrefTrackingTransactionSearchRepository xrefTrackingTransactionSearchRepository) {
        this.xrefTrackingTransactionRepository = xrefTrackingTransactionRepository;
        this.xrefTrackingTransactionSearchRepository = xrefTrackingTransactionSearchRepository;
    }

    /**
     * POST  /xref-tracking-transactions : Create a new xrefTrackingTransaction.
     *
     * @param xrefTrackingTransaction the xrefTrackingTransaction to create
     * @return the ResponseEntity with status 201 (Created) and with body the new xrefTrackingTransaction, or with status 400 (Bad Request) if the xrefTrackingTransaction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/xref-tracking-transactions",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<XrefTrackingTransaction> createXrefTrackingTransaction(@RequestBody XrefTrackingTransaction xrefTrackingTransaction) throws URISyntaxException {
        log.debug("REST request to save XrefTrackingTransaction : {}", xrefTrackingTransaction);
        if (xrefTrackingTransaction.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("xrefTrackingTransaction", "idexists", "A new xrefTrackingTransaction cannot already have an ID")).body(null);
        }
        XrefTrackingTransaction result = xrefTrackingTransactionRepository.save(xrefTrackingTransaction);
        xrefTrackingTransactionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/xref-tracking-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("xrefTrackingTransaction", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /xref-tracking-transactions : Updates an existing xrefTrackingTransaction.
     *
     * @param xrefTrackingTransaction the xrefTrackingTransaction to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated xrefTrackingTransaction,
     * or with status 400 (Bad Request) if the xrefTrackingTransaction is not valid,
     * or with status 500 (Internal Server Error) if the xrefTrackingTransaction couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/xref-tracking-transactions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<XrefTrackingTransaction> updateXrefTrackingTransaction(@RequestBody XrefTrackingTransaction xrefTrackingTransaction) throws URISyntaxException {
        log.debug("REST request to update XrefTrackingTransaction : {}", xrefTrackingTransaction);
        if (xrefTrackingTransaction.getId() == null) {
            return createXrefTrackingTransaction(xrefTrackingTransaction);
        }
        XrefTrackingTransaction result = xrefTrackingTransactionRepository.save(xrefTrackingTransaction);
        xrefTrackingTransactionSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("xrefTrackingTransaction", xrefTrackingTransaction.getId().toString()))
            .body(result);
    }

    /**
     * GET  /xref-tracking-transactions : get all the xrefTrackingTransactions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of xrefTrackingTransactions in body
     */
    @RequestMapping(value = "/xref-tracking-transactions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<XrefTrackingTransaction> getAllXrefTrackingTransactions() {
        log.debug("REST request to get all XrefTrackingTransactions");
        List<XrefTrackingTransaction> xrefTrackingTransactions = xrefTrackingTransactionRepository.findAll();
        return xrefTrackingTransactions;
    }

    /**
     * GET  /xref-tracking-transactions/:id : get the "id" xrefTrackingTransaction.
     *
     * @param id the id of the xrefTrackingTransaction to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the xrefTrackingTransaction, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/xref-tracking-transactions/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<XrefTrackingTransaction> getXrefTrackingTransaction(@PathVariable Long id) {
        log.debug("REST request to get XrefTrackingTransaction : {}", id);
        XrefTrackingTransaction xrefTrackingTransaction = xrefTrackingTransactionRepository.findOne(id);
        return Optional.ofNullable(xrefTrackingTransaction)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /xref-tracking-transactions/:id : delete the "id" xrefTrackingTransaction.
     *
     * @param id the id of the xrefTrackingTransaction to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/xref-tracking-transactions/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteXrefTrackingTransaction(@PathVariable Long id) {
        log.debug("REST request to delete XrefTrackingTransaction : {}", id);
        xrefTrackingTransactionRepository.delete(id);
        xrefTrackingTransactionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("xrefTrackingTransaction", id.toString())).build();
    }

    /**
     * SEARCH  /_search/xref-tracking-transactions?query=:query : search for the xrefTrackingTransaction corresponding
     * to the query.
     *
     * @param query the query of the xrefTrackingTransaction search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/xref-tracking-transactions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<XrefTrackingTransaction> searchXrefTrackingTransactions(@RequestParam String query) {
        log.debug("REST request to search XrefTrackingTransactions for query {}", query);
        return StreamSupport
            .stream(xrefTrackingTransactionSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
