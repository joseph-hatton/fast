package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.ProductGroup;
import com.rgare.global.finre.repository.ProductGroupRepository;
import com.rgare.global.finre.repository.search.ProductGroupSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing ProductGroup.
 */
@RestController
@RequestMapping("/api")
public class ProductGroupResource {

    private final Logger log = LoggerFactory.getLogger(ProductGroupResource.class);

    private static final String ENTITY_NAME = "productGroup";

    private ProductGroupRepository productGroupRepository;

    private ProductGroupSearchRepository productGroupSearchRepository;

    public ProductGroupResource(ProductGroupRepository productGroupRepository, ProductGroupSearchRepository productGroupSearchRepository) {
        this.productGroupRepository = productGroupRepository;
        this.productGroupSearchRepository = productGroupSearchRepository;
    }
    /**
     * POST  /product-groups : Create a new productGroup.
     *
     * @param productGroup the productGroup to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productGroup, or with status 400 (Bad Request) if the productGroup has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/product-groups",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductGroup> createProductGroup(@Valid @RequestBody ProductGroup productGroup) throws URISyntaxException {
        log.debug("REST request to save ProductGroup : {}", productGroup);
        if (productGroup.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("productGroup", "idexists", "A new productGroup cannot already have an ID")).body(null);
        }
        ProductGroup result = productGroupRepository.save(productGroup);
        productGroupSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/product-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("productGroup", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /product-groups : Updates an existing productGroup.
     *
     * @param productGroup the productGroup to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated productGroup,
     * or with status 400 (Bad Request) if the productGroup is not valid,
     * or with status 500 (Internal Server Error) if the productGroup couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/product-groups",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductGroup> updateProductGroup(@Valid @RequestBody ProductGroup productGroup) throws URISyntaxException {
        log.debug("REST request to update ProductGroup : {}", productGroup);
        if (productGroup.getId() == null) {
            return createProductGroup(productGroup);
        }
        ProductGroup result = productGroupRepository.save(productGroup);
        productGroupSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("productGroup", productGroup.getId().toString()))
            .body(result);
    }

    /**
     * GET  /product-groups : get all the productGroups.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of productGroups in body
     */
    @RequestMapping(value = "/product-groups",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ProductGroup> getAllProductGroups() {
        log.debug("REST request to get all ProductGroups");
        List<ProductGroup> productGroups = productGroupRepository.findAll();
        return productGroups;
    }

    /**
     * GET  /product-groups/:id : get the "id" productGroup.
     *
     * @param id the id of the productGroup to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the productGroup, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/product-groups/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductGroup> getProductGroup(@PathVariable Long id) {
        log.debug("REST request to get ProductGroup : {}", id);
        ProductGroup productGroup = productGroupRepository.findOne(id);
        return Optional.ofNullable(productGroup)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /product-groups/:id : delete the "id" productGroup.
     *
     * @param id the id of the productGroup to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/product-groups/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteProductGroup(@PathVariable Long id) {
        log.debug("REST request to delete ProductGroup : {}", id);
        productGroupRepository.delete(id);
        productGroupSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("productGroup", id.toString())).build();
    }

    /**
     * SEARCH  /_search/product-groups?query=:query : search for the productGroup corresponding
     * to the query.
     *
     * @param query the query of the productGroup search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/product-groups",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ProductGroup> searchProductGroups(@RequestParam String query) {
        log.debug("REST request to search ProductGroups for query {}", query);
        return StreamSupport
            .stream(productGroupSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
