package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.*;
import com.rgare.global.finre.repository.JhiUserRepository;
import com.rgare.global.finre.repository.JhiPersistentAuditEventRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing Treaty.
 */
@RestController
@RequestMapping("/api")
public class JhiUserResource {

    private JhiUserRepository jhiUserRepository;
    private JhiPersistentAuditEventRepository jhiPersistentAuditEventRepository;

    public JhiUserResource(JhiUserRepository jhiUserRepository, JhiPersistentAuditEventRepository jhiPersistentAuditEventRepository) {
        this.jhiUserRepository = jhiUserRepository;
        this.jhiPersistentAuditEventRepository = jhiPersistentAuditEventRepository;
    }
    /**
     * POST  /jhiUser : Create a new jhiUser.
     *
     * @param jhiUser the jhiUser to create
     * @return the ResponseEntity with status 201 (Created) and with body the new treaty, or with status 400 (Bad Request) if the treaty has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/jhiUser",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<JhiUser> createjhiUser(@Valid @RequestBody JhiUser jhiUser) throws URISyntaxException {
        if (jhiUser.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("jhiUser", "idexists", "A new jhiUser cannot already have an ID")).body(null);
        }
        JhiUser result = jhiUserRepository.save(jhiUser);
        saveAuditInfo(jhiUser, "create");

        return ResponseEntity.created(new URI("/jhiUser/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("jhiUser", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /jhiUser : Updates an existing treaty.
     *
     * @param jhiUser the jhiUser to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated treaty,
     * or with status 400 (Bad Request) if the treaty is not valid,
     * or with status 500 (Internal Server Error) if the treaty couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/jhiUser",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<JhiUser> updateIntroPage(@Valid @RequestBody JhiUser jhiUser) throws URISyntaxException {
        if (jhiUser.getId() == null) {
            return createjhiUser(jhiUser);
        }

        JhiUser result = jhiUserRepository.save(jhiUser);
        saveAuditInfo(jhiUser, "update");
        
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("jhiUser", jhiUser.getId().toString()))
            .body(result);
    }


  /**
   * GET  /jhiUsers : get all the intros. No Pagination
   *
   * @return the ResponseEntity with status 200 (OK) and the list of treaties in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/jhiUser",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<JhiUser>> getAlljhiUsers() {
    List<JhiUser> jhiUsers = jhiUserRepository.findAll();

    return new ResponseEntity<>(jhiUsers, null, HttpStatus.OK);
  }

  /**
   * GET  /jhiUser/:id : get the jhiUser for this id
   *
   * @return the ResponseEntity with status 200 (OK) and the list of treaties in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/jhiUser/{id}",
  method = RequestMethod.GET,
  produces = MediaType.APPLICATION_JSON_VALUE)
@Timed
public ResponseEntity<JhiUser> getjhiUser(@PathVariable Long id) {
    JhiUser jhiUser = jhiUserRepository.findOneById(id);

  return new ResponseEntity<>(jhiUser, null, HttpStatus.OK);
}
/**
   * DELETE  /jhiUser/:id : delete the jhiUser for this id
   *
   * @return the ResponseEntity with status 200 (OK) and the list of treaties in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/jhiUser/{id}",
  method = RequestMethod.DELETE,
  produces = MediaType.APPLICATION_JSON_VALUE)
@Timed
public ResponseEntity<JhiUser> deletejhiUser(@PathVariable Long id) {
    JhiUser u = jhiUserRepository.findOneById(id);
    jhiUserRepository.delete(u);
    saveAuditInfo(u, "delete");

  return new ResponseEntity<>(null, null, HttpStatus.OK);
}

      

    private void saveAuditInfo (JhiUser jhiUser, String reason) {
        Reason r = new Reason(reason, jhiUser.getAuths());
        JhiPersistentAuditEvent e = new JhiPersistentAuditEvent(jhiUser.getLogin(), jhiUser.getLastUpdate(), jhiUser.getLastUser(), r.toString());
        jhiPersistentAuditEventRepository.save(e);
    }
}
