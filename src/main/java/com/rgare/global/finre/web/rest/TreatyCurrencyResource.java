package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.TreatyCurrency;
import com.rgare.global.finre.repository.TreatyCurrencyRepository;
import com.rgare.global.finre.repository.search.TreatyCurrencySearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TreatyCurrency.
 */
@RestController
@RequestMapping("/api")
public class TreatyCurrencyResource {

    private final Logger log = LoggerFactory.getLogger(TreatyCurrencyResource.class);

    private static final String ENTITY_NAME = "treatyCurrency";

    private TreatyCurrencyRepository treatyCurrencyRepository;

    private TreatyCurrencySearchRepository treatyCurrencySearchRepository;

    public TreatyCurrencyResource(TreatyCurrencyRepository treatyCurrencyRepository, TreatyCurrencySearchRepository treatyCurrencySearchRepository) {
        this.treatyCurrencyRepository = treatyCurrencyRepository;
        this.treatyCurrencySearchRepository = treatyCurrencySearchRepository;
    }

    /**
     * POST  /treaty-currencies : Create a new treatyCurrency.
     *
     * @param treatyCurrency the treatyCurrency to create
     * @return the ResponseEntity with status 201 (Created) and with body the new treatyCurrency, or with status 400 (Bad Request) if the treatyCurrency has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaty-currencies",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyCurrency> createTreatyCurrency(@RequestBody TreatyCurrency treatyCurrency) throws URISyntaxException {
        log.debug("REST request to save TreatyCurrency : {}", treatyCurrency);
        if (treatyCurrency.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("treatyCurrency", "idexists", "A new treatyCurrency cannot already have an ID")).body(null);
        }
        TreatyCurrency result = treatyCurrencyRepository.save(treatyCurrency);
        treatyCurrencySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/treaty-currencies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("treatyCurrency", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /treaty-currencies : Updates an existing treatyCurrency.
     *
     * @param treatyCurrency the treatyCurrency to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated treatyCurrency,
     * or with status 400 (Bad Request) if the treatyCurrency is not valid,
     * or with status 500 (Internal Server Error) if the treatyCurrency couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaty-currencies",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyCurrency> updateTreatyCurrency(@RequestBody TreatyCurrency treatyCurrency) throws URISyntaxException {
        log.debug("REST request to update TreatyCurrency : {}", treatyCurrency);
        if (treatyCurrency.getId() == null) {
            return createTreatyCurrency(treatyCurrency);
        }
        TreatyCurrency result = treatyCurrencyRepository.save(treatyCurrency);
        treatyCurrencySearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("treatyCurrency", treatyCurrency.getId().toString()))
            .body(result);
    }

    /**
     * GET  /treaty-currencies : get all the treatyCurrencies.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of treatyCurrencies in body
     */
    @RequestMapping(value = "/treaty-currencies",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TreatyCurrency> getAllTreatyCurrencies() {
        log.debug("REST request to get all TreatyCurrencies");
        List<TreatyCurrency> treatyCurrencies = treatyCurrencyRepository.findAll();
        return treatyCurrencies;
    }

    /**
     * GET  /treaty-currencies : get all the treatyCurrencies.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of treatyCurrencies in body
     */
    @RequestMapping(value = "/treaty-currencies/currency-count/{currencyId}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Integer  getAllByCurrencyCount(@PathVariable Long currencyId) {
      log.debug("REST request to get all TreatyCurrencies");
      List<TreatyCurrency> treatyCurrencies = treatyCurrencyRepository.findAllByCurrencyId(currencyId);
      if (treatyCurrencies == null)
        return 0;
      return treatyCurrencies.size();
    }

    /**
     * GET  /treaty-currencies/:id : get the "id" treatyCurrency.
     *
     * @param id the id of the treatyCurrency to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the treatyCurrency, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/treaty-currencies/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyCurrency> getTreatyCurrency(@PathVariable Long id) {
        log.debug("REST request to get TreatyCurrency : {}", id);
        TreatyCurrency treatyCurrency = treatyCurrencyRepository.findOne(id);
        return Optional.ofNullable(treatyCurrency)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /treaty-currencies/:id : delete the "id" treatyCurrency.
     *
     * @param id the id of the treatyCurrency to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/treaty-currencies/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTreatyCurrency(@PathVariable Long id) {
        log.debug("REST request to delete TreatyCurrency : {}", id);
        treatyCurrencyRepository.delete(id);
        treatyCurrencySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("treatyCurrency", id.toString())).build();
    }

    /**
     * SEARCH  /_search/treaty-currencies?query=:query : search for the treatyCurrency corresponding
     * to the query.
     *
     * @param query the query of the treatyCurrency search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/treaty-currencies",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TreatyCurrency> searchTreatyCurrencies(@RequestParam String query) {
        log.debug("REST request to search TreatyCurrencies for query {}", query);
        return StreamSupport
            .stream(treatyCurrencySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
