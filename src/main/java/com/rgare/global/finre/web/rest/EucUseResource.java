package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.EucUse;
import com.rgare.global.finre.repository.EucUseRepository;
import com.rgare.global.finre.repository.search.EucUseSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing EucUse.
 */
@RestController
@RequestMapping("/api")
public class EucUseResource {

    private final Logger log = LoggerFactory.getLogger(EucUseResource.class);

    private static final String ENTITY_NAME = "eucUse";

    private EucUseRepository eucUseRepository;

    private EucUseSearchRepository eucUseSearchRepository;

    public EucUseResource(EucUseRepository eucUseRepository, EucUseSearchRepository eucUseSearchRepository) {
        this.eucUseRepository = eucUseRepository;
        this.eucUseSearchRepository = eucUseSearchRepository;
    }

    /**
     * POST  /euc-uses : Create a new eucUse.
     *
     * @param eucUse the eucUse to create
     * @return the ResponseEntity with status 201 (Created) and with body the new eucUse, or with status 400 (Bad Request) if the eucUse has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/euc-uses",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EucUse> createEucUse(@Valid @RequestBody EucUse eucUse) throws URISyntaxException {
        log.debug("REST request to save EucUse : {}", eucUse);
        if (eucUse.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new eucUse cannot already have an ID")).body(null);
        }
        EucUse result = eucUseRepository.save(eucUse);
        eucUseSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/euc-uses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /euc-uses : Updates an existing eucUse.
     *
     * @param eucUse the eucUse to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated eucUse,
     * or with status 400 (Bad Request) if the eucUse is not valid,
     * or with status 500 (Internal Server Error) if the eucUse couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/euc-uses",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EucUse> updateEucUse(@Valid @RequestBody EucUse eucUse) throws URISyntaxException {
        log.debug("REST request to update EucUse : {}", eucUse);
        if (eucUse.getId() == null) {
            return createEucUse(eucUse);
        }
        EucUse result = eucUseRepository.save(eucUse);
        eucUseSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, eucUse.getId().toString()))
            .body(result);
    }

    /**
     * GET  /euc-uses : get all the eucUses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of eucUses in body
     */
    @RequestMapping(value = "/euc-uses",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<EucUse> getAllEucUses() {
        log.debug("REST request to get all EucUses");
        List<EucUse> eucUses = eucUseRepository.findAll();
        return eucUses;
    }

    /**
     * GET  /euc-uses/:id : get the "id" eucUse.
     *
     * @param id the id of the eucUse to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the eucUse, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/euc-uses/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EucUse> getEucUse(@PathVariable Long id) {
        log.debug("REST request to get EucUse : {}", id);
        EucUse eucUse = eucUseRepository.findOne(id);
        return Optional.ofNullable(eucUse)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /euc-uses/:id : delete the "id" eucUse.
     *
     * @param id the id of the eucUse to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/euc-uses/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteEucUse(@PathVariable Long id) {
        log.debug("REST request to delete EucUse : {}", id);
        eucUseRepository.delete(id);
        eucUseSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/euc-uses?query=:query : search for the eucUse corresponding
     * to the query.
     *
     * @param query the query of the eucUse search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/euc-uses",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<EucUse> searchEucUses(@RequestParam String query) {
        log.debug("REST request to search EucUses for query {}", query);
        return StreamSupport
            .stream(eucUseSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
