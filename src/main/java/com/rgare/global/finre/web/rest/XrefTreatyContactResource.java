package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.XrefTreatyCompany;
import com.rgare.global.finre.domain.XrefTreatyContact;
import com.rgare.global.finre.repository.XrefTreatyContactRepository;
import com.rgare.global.finre.repository.search.XrefTreatyContactSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing XrefTreatyContact.
 */
@RestController
@RequestMapping("/api")
public class XrefTreatyContactResource {

    private final Logger log = LoggerFactory.getLogger(XrefTreatyContactResource.class);

    private static final String ENTITY_NAME = "xrefTreatyContact";

    private XrefTreatyContactRepository xrefTreatyContactRepository;

    private XrefTreatyContactSearchRepository xrefTreatyContactSearchRepository;

    public XrefTreatyContactResource(XrefTreatyContactRepository xrefTreatyContactRepository, XrefTreatyContactSearchRepository xrefTreatyContactSearchRepository) {
        this.xrefTreatyContactRepository = xrefTreatyContactRepository;
        this.xrefTreatyContactSearchRepository = xrefTreatyContactSearchRepository;
    }
    /**
     * POST  /xref-treaty-contacts : Create a new xrefTreatyContact.
     *
     * @param xrefTreatyContact the xrefTreatyContact to create
     * @return the ResponseEntity with status 201 (Created) and with body the new xrefTreatyContact, or with status 400 (Bad Request) if the xrefTreatyContact has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/xref-treaty-contacts",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<XrefTreatyContact> createXrefTreatyContact(@Valid @RequestBody XrefTreatyContact xrefTreatyContact) throws URISyntaxException {
        log.debug("REST request to save XrefTreatyContact : {}", xrefTreatyContact);
        if (xrefTreatyContact.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("xrefTreatyContact", "idexists", "A new xrefTreatyContact cannot already have an ID")).body(null);
        }
        XrefTreatyContact result = xrefTreatyContactRepository.save(xrefTreatyContact);
        xrefTreatyContactSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/xref-treaty-contacts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("xrefTreatyContact", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /xref-treaty-contacts : Updates an existing xrefTreatyContact.
     *
     * @param xrefTreatyContact the xrefTreatyContact to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated xrefTreatyContact,
     * or with status 400 (Bad Request) if the xrefTreatyContact is not valid,
     * or with status 500 (Internal Server Error) if the xrefTreatyContact couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/xref-treaty-contacts",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<XrefTreatyContact> updateXrefTreatyContact(@Valid @RequestBody XrefTreatyContact xrefTreatyContact) throws URISyntaxException {
        log.debug("REST request to update XrefTreatyContact : {}", xrefTreatyContact);
        if (xrefTreatyContact.getId() == null) {
            return createXrefTreatyContact(xrefTreatyContact);
        }
        XrefTreatyContact result = xrefTreatyContactRepository.save(xrefTreatyContact);
        xrefTreatyContactSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("xrefTreatyContact", xrefTreatyContact.getId().toString()))
            .body(result);
    }

    /**
     * GET  /xref-treaty-contacts : get all the xrefTreatyContacts.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of xrefTreatyContacts in body
     */
    @RequestMapping(value = "/xref-treaty-contacts",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<XrefTreatyContact> getAllXrefTreatyContacts() {
        log.debug("REST request to get all XrefTreatyContacts");
        List<XrefTreatyContact> xrefTreatyContacts = xrefTreatyContactRepository.findAll();
        return xrefTreatyContacts;
    }

    /**
     * GET  /xref-treaty-contacts/:id : get the "id" xrefTreatyContact.
     *
     * @param id the id of the xrefTreatyContact to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the xrefTreatyContact, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/xref-treaty-contacts/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<XrefTreatyContact> getXrefTreatyContact(@PathVariable Long id) {
        log.debug("REST request to get XrefTreatyContact : {}", id);
        XrefTreatyContact xrefTreatyContact = xrefTreatyContactRepository.findOne(id);
        return Optional.ofNullable(xrefTreatyContact)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /xref-treaty-contacts/treaties -> get all xref treaty-contacts by treatyId.
     */
    @RequestMapping(value = "/xref-treaty-contacts/treaties/{treatyId}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<XrefTreatyContact>> getAllTreatiesByTreatyId(@PathVariable Long treatyId) {
      log.debug("REST request to get all XrefTreatyContact by TreatyId");

      List<XrefTreatyContact> xrefTreatyContacts = xrefTreatyContactRepository.findAllByTreatyId(treatyId);
      return Optional.ofNullable(xrefTreatyContacts)
        .map(result -> new ResponseEntity<>(
          result,
          HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /xref-treaty-contacts/contacts -> get all xref treaty-contacts by contactId.
     */
    @RequestMapping(value = "/xref-treaty-contacts/contacts/{contactId}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<XrefTreatyContact>> getAllByContactId(@PathVariable Long contactId) {
      log.debug("REST request to get all XrefTreatyContact by contactId");

      List<XrefTreatyContact> xrefTreatyContacts = xrefTreatyContactRepository.findAllByContactId(contactId);
      return Optional.ofNullable(xrefTreatyContacts)
        .map(result -> new ResponseEntity<>(
          result,
          HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /xref-treaty-contacts/:id : delete the "id" xrefTreatyContact.
     *
     * @param id the id of the xrefTreatyContact to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/xref-treaty-contacts/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteXrefTreatyContact(@PathVariable Long id) {
        log.debug("REST request to delete XrefTreatyContact : {}", id);
        xrefTreatyContactRepository.delete(id);
        xrefTreatyContactSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("xrefTreatyContact", id.toString())).build();
    }

    /**
     * SEARCH  /_search/xref-treaty-contacts?query=:query : search for the xrefTreatyContact corresponding
     * to the query.
     *
     * @param query the query of the xrefTreatyContact search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/xref-treaty-contacts",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<XrefTreatyContact> searchXrefTreatyContacts(@RequestParam String query) {
        log.debug("REST request to search XrefTreatyContacts for query {}", query);
        return StreamSupport
            .stream(xrefTreatyContactSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
