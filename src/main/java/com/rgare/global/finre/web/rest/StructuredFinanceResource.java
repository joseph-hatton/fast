package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.StructuredFinance;
import com.rgare.global.finre.domain.StructuredTracking;
import com.rgare.global.finre.repository.StructuredFinanceRepository;
import com.rgare.global.finre.repository.search.StructuredFinanceSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing StructuredFinance.
 */
@RestController
@RequestMapping("/api")
public class StructuredFinanceResource {

    private final Logger log = LoggerFactory.getLogger(StructuredFinanceResource.class);

    private static final String ENTITY_NAME = "structuredFinance";

    private StructuredFinanceRepository structuredFinanceRepository;

    private StructuredFinanceSearchRepository structuredFinanceSearchRepository;

    public StructuredFinanceResource(StructuredFinanceRepository structuredFinanceRepository, StructuredFinanceSearchRepository structuredFinanceSearchRepository) {
        this.structuredFinanceRepository = structuredFinanceRepository;
        this.structuredFinanceSearchRepository = structuredFinanceSearchRepository;
    }
    /**
     * POST  /structured-finances : Create a new structuredFinance.
     *
     * @param structuredFinance the structuredFinance to create
     * @return the ResponseEntity with status 201 (Created) and with body the new structuredFinance, or with status 400 (Bad Request) if the structuredFinance has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/structured-finances",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<StructuredFinance> createStructuredFinance(@RequestBody StructuredFinance structuredFinance) throws URISyntaxException {
        log.debug("REST request to save StructuredFinance : {}", structuredFinance);
        if (structuredFinance.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("structuredFinance", "idexists", "A new structuredFinance cannot already have an ID")).body(null);
        }
        StructuredFinance result = structuredFinanceRepository.save(structuredFinance);
        structuredFinanceSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/structured-finances/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("structuredFinance", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /structured-finances : Updates an existing structuredFinance.
     *
     * @param structuredFinance the structuredFinance to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated structuredFinance,
     * or with status 400 (Bad Request) if the structuredFinance is not valid,
     * or with status 500 (Internal Server Error) if the structuredFinance couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/structured-finances",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<StructuredFinance> updateStructuredFinance(@RequestBody StructuredFinance structuredFinance) throws URISyntaxException {
        log.debug("REST request to update StructuredFinance : {}", structuredFinance);
        if (structuredFinance.getId() == null) {
            return createStructuredFinance(structuredFinance);
        }
        StructuredFinance result = structuredFinanceRepository.save(structuredFinance);
        structuredFinanceSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("structuredFinance", structuredFinance.getId().toString()))
            .body(result);
    }

    /**
     * GET  /structured-finances : get all the structuredFinances.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of structuredFinances in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/structured-finances",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<StructuredFinance>> getAllStructuredFinances(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of StructuredFinances");
        Page<StructuredFinance> page = structuredFinanceRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/structured-finances");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /structured-finances/:id : get the "id" structuredFinance.
     *
     * @param id the id of the structuredFinance to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the structuredFinance, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/structured-finances/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<StructuredFinance> getStructuredFinance(@PathVariable Long id) {
        log.debug("REST request to get StructuredFinance : {}", id);
        StructuredFinance structuredFinance = structuredFinanceRepository.findOne(id);
        return Optional.ofNullable(structuredFinance)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }




  /**
   * GET  /structured-trackings/:id : get the "id" structuredTracking.
   *  @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   * @param dealId the id of the structuredTracking to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the structuredTracking, or with status 404 (Not Found)
   */
  @RequestMapping(value = "/structured-finance-by-deal/{dealId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<StructuredFinance>> getStructuredFinanceByDealIdActive(@PathVariable Long dealId,Pageable pageable)  throws URISyntaxException {
    {
      log.debug("REST request a page of StructuredFinance : {}", dealId);
      Page<StructuredFinance> page = structuredFinanceRepository.findAllByDealIdAndIsActive(dealId,pageable,1);
      HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/structured-finances");
      return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
  }


    /**
     * DELETE  /structured-finances/:id : delete the "id" structuredFinance.
     *
     * @param id the id of the structuredFinance to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/structured-finances/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteStructuredFinance(@PathVariable Long id) {
        log.debug("REST request to delete StructuredFinance : {}", id);
        structuredFinanceRepository.delete(id);
        structuredFinanceSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("structuredFinance", id.toString())).build();
    }

    /**
     * SEARCH  /_search/structured-finances?query=:query : search for the structuredFinance corresponding
     * to the query.
     *
     * @param query the query of the structuredFinance search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/structured-finances",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<StructuredFinance>> searchStructuredFinances(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of StructuredFinances for query {}", query);
        Page<StructuredFinance> page = structuredFinanceSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/structured-finances");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
