package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.Deal;
import com.rgare.global.finre.repository.DealRepository;
import com.rgare.global.finre.repository.search.DealSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing Deal.
 */
@RestController
@RequestMapping("/api")
public class DealResource {

    private final Logger log = LoggerFactory.getLogger(DealResource.class);

    private static final String ENTITY_NAME = "deal";

    private DealRepository dealRepository;

    private DealSearchRepository dealSearchRepository;

    public DealResource(DealRepository dealRepository, DealSearchRepository dealSearchRepository) {
        this.dealRepository = dealRepository;
        this.dealSearchRepository = dealSearchRepository;
    }

    /**
     * POST  /deals : Create a new deal.
     *
     * @param deal the deal to create
     * @return the ResponseEntity with status 201 (Created) and with body the new deal, or with status 400 (Bad Request) if the deal has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/deals",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Deal> createDeal(@Valid @RequestBody Deal deal) throws URISyntaxException {
        log.debug("REST request to save Deal : {}", deal);
        if (deal.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new deal cannot already have an ID")).body(null);
        }
        Deal result = dealRepository.save(deal);
        dealSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/deals/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /deals : Updates an existing deal.
     *
     * @param deal the deal to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated deal,
     * or with status 400 (Bad Request) if the deal is not valid,
     * or with status 500 (Internal Server Error) if the deal couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/deals",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Deal> updateDeal(@Valid @RequestBody Deal deal) throws URISyntaxException {
        log.debug("REST request to update Deal : {}", deal);
        if (deal.getId() == null) {
            return createDeal(deal);
        }
        Deal result = dealRepository.save(deal);
        dealSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, deal.getId().toString()))
            .body(result);
    }

    /**
     * GET  /deals : get all the deals.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of deals in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/deals")
    @Timed
    public ResponseEntity<List<Deal>> getAllDeals(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Deals");
       Page<Deal> page = dealRepository.findDealsByIsActive(1,pageable);
       // Page<Deal> page = dealRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/deals");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /deals : get all the deals.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of deals in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/deals/nopage",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Deal>> getAllDealsNoPages()
      throws URISyntaxException {
      log.debug("REST request to get a All Deals");
      List<Deal> page = dealRepository.findDealsByIsActive(1);
      return Optional.ofNullable(page)
        .map(result -> new ResponseEntity<>(
          result,
          HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /deals/:id : get the "id" deal.
     *
     * @param id the id of the deal to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the deal, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/deals/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Deal> getDeal(@PathVariable Long id) {
        log.debug("REST request to get Deal : {}", id);
        Deal deal = dealRepository.findOne(id);
        return Optional.ofNullable(deal)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /deals/existing/search : get all existing deals.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of existing mdmcompanies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/deals/existing/search/{searchString}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Deal>> searchExistingDeals(@PathVariable String searchString, Pageable pageable)
      throws URISyntaxException {
      log.debug("REST request to get all Deals Search");
      Page<Deal> page = dealRepository.searchExistingDealQuery(searchString, pageable);
      HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/deals/existing/search");
      return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /deals/existing/search : get all existing deals.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of existing mdmcompanies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/deals/existing/searchExactActive",
      method = RequestMethod.PUT,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Deal> searchExactActiveExistingDeals(@Valid @RequestBody String searchString)
      throws URISyntaxException {
      log.debug("REST request to get all Deals Search");
      List<Deal> deals = dealRepository.findAllByNameIgnoreCaseAndIsActiveAndDealStatus(searchString,1,"Active");
      return deals;
    }

    /**
     * DELETE  /deals/:id : delete the "id" deal.
     *
     * @param id the id of the deal to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/deals/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteDeal(@PathVariable Long id) {
        log.debug("REST request to delete Deal : {}", id);
        dealRepository.delete(id);
        dealSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/deals?query=:query : search for the deal corresponding
     * to the query.
     *
     * @param query the query of the deal search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/deals",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Deal>> searchDeals(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Deals for query {}", query);
        Page<Deal> page = dealSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/deals");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
