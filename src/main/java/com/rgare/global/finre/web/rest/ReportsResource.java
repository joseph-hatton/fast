package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.Reports;
import com.rgare.global.finre.repository.ReportsRepository;
import com.rgare.global.finre.repository.search.ReportsSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing Reports.
 */
@RestController
@RequestMapping("/api")
public class ReportsResource {

    private final Logger log = LoggerFactory.getLogger(ReportsResource.class);

    private static final String ENTITY_NAME = "reports";

    private ReportsRepository reportsRepository;

    private ReportsSearchRepository reportsSearchRepository;

    public ReportsResource(ReportsRepository reportsRepository, ReportsSearchRepository reportsSearchRepository) {
        this.reportsRepository = reportsRepository;
        this.reportsSearchRepository = reportsSearchRepository;
    }

    /**
     * POST  /reports : Create a new reports.
     *
     * @param reports the reports to create
     * @return the ResponseEntity with status 201 (Created) and with body the new reports, or with status 400 (Bad Request) if the reports has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/reports",
      method = RequestMethod.POST,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Reports> createReports(@RequestBody Reports reports) throws URISyntaxException {
        log.debug("REST request to save Reports : {}", reports);
        if (reports.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("reports", "idexists", "A new reports cannot already have an ID")).body(null);
        }
        Reports result = reportsRepository.save(reports);
        reportsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/reports/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("reports", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /reports : Updates an existing reports.
     *
     * @param reports the reports to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated reports,
     * or with status 400 (Bad Request) if the reports is not valid,
     * or with status 500 (Internal Server Error) if the reports couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/reports",
      method = RequestMethod.PUT,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Reports> updateReports(@RequestBody Reports reports) throws URISyntaxException {
        log.debug("REST request to update Reports : {}", reports);
        if (reports.getId() == null) {
            return createReports(reports);
        }
        Reports result = reportsRepository.save(reports);
        reportsSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("reports", reports.getId().toString()))
            .body(result);
    }

    /**
     * GET  /reports : get all the reports.
     * @return the ResponseEntity with status 200 (OK) and the list of reports in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/reports",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Reports> getAllReports()
        throws URISyntaxException {
       // log.debug("REST request to get a page of Reports");
       // Page<Reports> page = reportsRepository.findAll(pageable);
       // HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/reports");
        List<Reports> reports= reportsRepository.findAll();
        return reports ; //new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /reports/:id : get the "id" reports.
     *
     * @param id the id of the reports to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the reports, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/reports/{id}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Reports> getReports(@PathVariable Long id) {
        log.debug("REST request to get Reports : {}", id);
        Reports reports = reportsRepository.findOne(id);
        return Optional.ofNullable(reports)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /reports/:id : delete the "id" reports.
     *
     * @param id the id of the reports to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/reports/{id}",
      method = RequestMethod.DELETE,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteReports(@PathVariable Long id) {
        log.debug("REST request to delete Reports : {}", id);
        reportsRepository.delete(id);
        reportsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("reports", id.toString())).build();
    }

    /**
     * SEARCH  /_search/reports?query=:query : search for the reports corresponding
     * to the query.
     *
     * @param query the query of the reports search
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/reports",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Reports>> searchReports(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Reports for query {}", query);
        Page<Reports> page = reportsSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/reports");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
