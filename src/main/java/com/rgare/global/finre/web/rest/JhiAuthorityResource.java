package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.*;
import com.rgare.global.finre.repository.JhiAuthorityRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing Treaty.
 */
@RestController
@RequestMapping("/api")
public class JhiAuthorityResource {

    private JhiAuthorityRepository jhiAuthorityRepository;

    public JhiAuthorityResource(JhiAuthorityRepository jhiAuthorityRepository) {
        this.jhiAuthorityRepository = jhiAuthorityRepository;
    }
    
  /**
   * GET  /jhiAuthority : get all the auths. No Pagination
   *
   * @return the ResponseEntity with status 200 (OK) and the list of treaties in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/jhi-authority",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<JhiAuthority>> getAlljhiAuthorities() {
    List<JhiAuthority> jhiAuths = jhiAuthorityRepository.findAll();

    return new ResponseEntity<>(jhiAuths, null, HttpStatus.OK);
  }
}
