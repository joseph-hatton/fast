package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.DealType;

import com.rgare.global.finre.repository.DealTypeRepository;
import com.rgare.global.finre.repository.search.DealTypeSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing DealType.
 */
@RestController
@RequestMapping("/api")
public class DealTypeResource {

    private final Logger log = LoggerFactory.getLogger(DealTypeResource.class);

    private static final String ENTITY_NAME = "deal_type";

    private final DealTypeRepository deal_typeRepository;

    private final DealTypeSearchRepository deal_typeSearchRepository;

    public DealTypeResource(DealTypeRepository deal_typeRepository, DealTypeSearchRepository deal_typeSearchRepository) {
        this.deal_typeRepository = deal_typeRepository;
        this.deal_typeSearchRepository = deal_typeSearchRepository;
    }

    /**
     * POST  /deal-types : Create a new deal_type.
     *
     * @param deal_type the deal_type to create
     * @return the ResponseEntity with status 201 (Created) and with body the new deal_type, or with status 400 (Bad Request) if the deal_type has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/deal-types")
    @Timed
    public ResponseEntity<DealType> createDeal_type(@RequestBody DealType deal_type) throws URISyntaxException {
        log.debug("REST request to save DealType : {}", deal_type);
        if (deal_type.getId() != null) {
          return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new deal Type cannot already have an ID")).body(null);
        }
        DealType result = deal_typeRepository.save(deal_type);
        deal_typeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/deal-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /deal-types : Updates an existing deal_type.
     *
     * @param deal_type the deal_type to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated deal_type,
     * or with status 400 (Bad Request) if the deal_type is not valid,
     * or with status 500 (Internal Server Error) if the deal_type couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/deal-types")
    @Timed
    public ResponseEntity<DealType> updateDeal_type(@RequestBody DealType deal_type) throws URISyntaxException {
        log.debug("REST request to update DealType : {}", deal_type);
        if (deal_type.getId() == null) {
            return createDeal_type(deal_type);
        }
        DealType result = deal_typeRepository.save(deal_type);
        deal_typeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, deal_type.getId().toString()))
            .body(result);
    }

    /**
     * GET  /deal-types : get all the deal_types.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of deal_types in body
     */
    @GetMapping("/deal-types")
    @Timed
    public List<DealType> getAllDeal_types() {
        log.debug("REST request to get all Deal_types");
        return deal_typeRepository.findAll();
        }

    /**
     * GET  /deal-types/:id : get the "id" deal_type.
     *
     * @param id the id of the deal_type to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the deal_type, or with status 404 (Not Found)
     */
    @GetMapping("/deal-types/{id}")
    @Timed
    public ResponseEntity<DealType> getDeal_type(@PathVariable Long id) {
        log.debug("REST request to get DealType : {}", id);
        DealType deal_type = deal_typeRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(deal_type));
    }

    /**
     * DELETE  /deal-types/:id : delete the "id" deal_type.
     *
     * @param id the id of the deal_type to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/deal-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteDeal_type(@PathVariable Long id) {
        log.debug("REST request to delete DealType : {}", id);
        deal_typeRepository.delete(id);
        deal_typeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/deal-types?query=:query : search for the deal_type corresponding
     * to the query.
     *
     * @param query the query of the deal_type search
     * @return the result of the search
     */
    @GetMapping("/_search/deal-types")
    @Timed
    public List<DealType> searchDeal_types(@RequestParam String query) {
        log.debug("REST request to search Deal_types for query {}", query);
        return StreamSupport
            .stream(deal_typeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
