package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.AuditRiskManagement;
import com.rgare.global.finre.domain.RiskManagement;
import com.rgare.global.finre.repository.AuditRiskManagementRepository;
import com.rgare.global.finre.repository.RiskManagementRepository;
import com.rgare.global.finre.repository.search.RiskManagementSearchRepository;
import com.rgare.global.finre.service.RiskManagementUploadService;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing RiskManagement.
 */
@RestController
@RequestMapping("/api")
public class RiskManagementResource {

    private final Logger log = LoggerFactory.getLogger(RiskManagementResource.class);

    private static final String ENTITY_NAME = "riskManagement";

    private RiskManagementRepository riskManagementRepository;
    private AuditRiskManagementRepository auditRiskManagementRepository;
    private RiskManagementSearchRepository riskManagementSearchRepository;

    private RiskManagementUploadService riskManagementUploadService;

    public RiskManagementResource(RiskManagementRepository riskManagementRepository,
                                  AuditRiskManagementRepository auditRiskManagementRepository,
                                  RiskManagementSearchRepository riskManagementSearchRepository,
                                  RiskManagementUploadService riskManagementUploadService) {
        this.riskManagementRepository = riskManagementRepository;
        this.auditRiskManagementRepository=auditRiskManagementRepository;
        this.riskManagementSearchRepository = riskManagementSearchRepository;
        this.riskManagementUploadService = riskManagementUploadService;
    }

    /**
     * POST  /risk-managements : Create a new riskManagement.
     *
     * @param riskManagement the riskManagement to create
     * @return the ResponseEntity with status 201 (Created) and with body the new riskManagement, or with status 400 (Bad Request) if the riskManagement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/risk-managements",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<RiskManagement> createRiskManagement(@RequestBody RiskManagement riskManagement) throws URISyntaxException {
        log.debug("REST request to save RiskManagement : {}", riskManagement);
        if (riskManagement.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("riskManagement", "idexists", "A new riskManagement cannot already have an ID")).body(null);
        }
        RiskManagement result = riskManagementRepository.save(riskManagement);
        return ResponseEntity.created(new URI("/api/risk-managements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("riskManagement", result.getId().toString()))
            .body(result);
    }

    /**
     * POST  /risk-managements : Create a new riskManagement.
     *
     * @param riskManagement the riskManagement to create
     * @return the ResponseEntity with status 201 (Created) and with body the new riskManagement, or with status 400 (Bad Request) if the riskManagement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */

    @RequestMapping(value = "/risk-managements/upload",
      method = RequestMethod.POST,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public RiskManagement uploadRiskManagement(@RequestBody RiskManagement riskManagement) throws Exception {
      log.debug("REST request to upload RiskManagement : {}", riskManagement);
      String msg = riskManagementUploadService.uploadRiskManagement(riskManagement);
      riskManagement.setStatusMessage(msg);
      return riskManagement;
    }



  /**
     * PUT  /risk-managements : Updates an existing riskManagement.
     *
     * @param riskManagement the riskManagement to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated riskManagement,
     * or with status 400 (Bad Request) if the riskManagement is not valid,
     * or with status 500 (Internal Server Error) if the riskManagement couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/risk-managements",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<RiskManagement> updateRiskManagement(@RequestBody RiskManagement riskManagement) throws URISyntaxException {
        log.debug("REST request to update RiskManagement : {}", riskManagement);
        if (riskManagement.getId() == null) {
            return createRiskManagement(riskManagement);
        }
        RiskManagement result = riskManagementRepository.save(riskManagement);

      AuditRiskManagement auditRiskManagementObject =new AuditRiskManagement();
      auditRiskManagementObject.setCreationUser(result.getLastUser());
      auditRiskManagementObject.setLastUpdate(riskManagement.getLastUpdate());
      auditRiskManagementObject.setCreationTs(riskManagement.getLastUpdate());
      auditRiskManagementObject.setLastUser(result.getLastUser());
      auditRiskManagementObject.setRiskManagementId(result.getId());
      auditRiskManagementObject.setDealId(riskManagement.getDeal().getId());
      auditRiskManagementObject.setAction("UPDATE");
      auditRiskManagementObject.setComments("'"+riskManagement.getRiskCriteria().getName()+"' , '"+riskManagement.getAmount().toString()+"' , '"+riskManagement.getConvertedAmount().toString()+"' , '"+riskManagement.getMonthYear()+"' , '"+riskManagement.getCurrency().getCode()+"'");
      auditRiskManagementRepository.save(auditRiskManagementObject);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("riskManagement", riskManagement.getId().toString()))
            .body(result);
    }

    /**
     * GET  /risk-managements : get all the riskManagements.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of riskManagements in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/risk-managements",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<RiskManagement>> getAllRiskManagements(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of RiskManagements");
        Page<RiskManagement> page = riskManagementRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/risk-managements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /risk-managements/:id : get the "id" riskManagement.
     *
     * @param id the id of the riskManagement to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the riskManagement, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/risk-managements/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<RiskManagement> getRiskManagement(@PathVariable Long id) {
        log.debug("REST request to get RiskManagement : {}", id);
        RiskManagement riskManagement = riskManagementRepository.findOne(id);
        return Optional.ofNullable(riskManagement)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

  /**
   * GET  /risk-managements/deal/:dealId/:code : get the "id" riskManagement.
   *
   * @param dealId the id of the riskManagement to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the riskManagement, or with status 404 (Not Found)
   */
  @RequestMapping(value = "/risk-managements/deal/{dealId}/{startYear}/{startMonth}/{startDay}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public List<RiskManagement> getRiskManagements(@PathVariable Long dealId,   @PathVariable Integer startYear,
                                                 @PathVariable Integer startMonth,
                                                 @PathVariable Integer startDay) {
    log.debug("REST request to get RiskManagement : {}", dealId);
    List<RiskManagement> List = riskManagementRepository.findAllByDealIdAndMonthYear(dealId,
      LocalDate.of(startYear.intValue(),startMonth.intValue(),startDay.intValue()));
    return List;
  }

  /**
   * GET  /risk-managements/:id : get the "id" riskManagement.
   *

   * @return the ResponseEntity with status 200 (OK) and with body the riskManagement, or with status 404 (Not Found)
   */
  @RequestMapping(value = "/risk-managements/byDate/{startYear}/{startMonth}/{startDay}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public List<RiskManagement> getRiskManagement(  @PathVariable Integer startYear,
                                                  @PathVariable Integer startMonth,
                                                  @PathVariable Integer startDay) {
    List<RiskManagement> riskManagements = riskManagementRepository.findByMonthYear(LocalDate.of(startYear,startMonth,startDay));
    return riskManagements;
  }

    /**
     * DELETE  /risk-managements/:id : delete the "id" riskManagement.
     *
     * @param id the id of the riskManagement to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/risk-managements/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteRiskManagement(@PathVariable Long id) {
        log.debug("REST request to delete RiskManagement : {}", id);
      RiskManagement Deleted =riskManagementRepository.getOne(id);
    //  AuditRiskManagement auditRiskManagementObject =new AuditRiskManagement();
      //auditRiskManagementObject.setCreationUser(Deleted.getLastUser());
      //auditRiskManagementObject.setLastUpdate(Deleted.getLastUpdate());
      //auditRiskManagementObject.setCreationTs(Deleted.getLastUpdate());
      //auditRiskManagementObject.setLastUser(Deleted.getLastUser());
      //auditRiskManagementObject.setRiskManagementId(Deleted.getId());
      //auditRiskManagementObject.setDealId(Deleted.getDeal().getId());
      //auditRiskManagementObject.setAction("DELETE");
      //auditRiskManagementObject.setComments("'"+Deleted.getRiskCriteria().getName()+"' , '"+Deleted.getAmount()+"' , '"+Deleted.getConvertedAmount()+"' , '"+Deleted.getMonthYear()+"' , '"+Deleted.getCurrency().getCode()+"'");
      //auditRiskManagementRepository.save(auditRiskManagementObject);

      riskManagementRepository.delete(id);
      return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("riskManagement", id.toString())).build();
    }

    /**
     * SEARCH  /_search/risk-managements?query=:query : search for the riskManagement corresponding
     * to the query.
     *
     * @param query the query of the riskManagement search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/risk-managements",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<RiskManagement> searchRiskManagements(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of RiskManagements for query {}", query);
        return StreamSupport
            .stream(riskManagementSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
