package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.TreatyStatus;
import com.rgare.global.finre.repository.TreatyStatusRepository;
import com.rgare.global.finre.repository.search.TreatyStatusSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TreatyStatus.
 */
@RestController
@RequestMapping("/api")
public class TreatyStatusResource {

    private final Logger log = LoggerFactory.getLogger(TreatyStatusResource.class);

    private static final String ENTITY_NAME = "treatyStatus";

    private TreatyStatusRepository treatyStatusRepository;

    private TreatyStatusSearchRepository treatyStatusSearchRepository;

    public TreatyStatusResource(TreatyStatusRepository treatyStatusRepository, TreatyStatusSearchRepository treatyStatusSearchRepository) {
        this.treatyStatusRepository = treatyStatusRepository;
        this.treatyStatusSearchRepository = treatyStatusSearchRepository;
    }
    /**
     * POST  /treaty-statuses : Create a new treatyStatus.
     *
     * @param treatyStatus the treatyStatus to create
     * @return the ResponseEntity with status 201 (Created) and with body the new treatyStatus, or with status 400 (Bad Request) if the treatyStatus has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaty-statuses",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyStatus> createTreatyStatus(@Valid @RequestBody TreatyStatus treatyStatus) throws URISyntaxException {
        log.debug("REST request to save TreatyStatus : {}", treatyStatus);
        if (treatyStatus.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("treatyStatus", "idexists", "A new treatyStatus cannot already have an ID")).body(null);
        }
        TreatyStatus result = treatyStatusRepository.save(treatyStatus);
        treatyStatusSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/treaty-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("treatyStatus", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /treaty-statuses : Updates an existing treatyStatus.
     *
     * @param treatyStatus the treatyStatus to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated treatyStatus,
     * or with status 400 (Bad Request) if the treatyStatus is not valid,
     * or with status 500 (Internal Server Error) if the treatyStatus couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaty-statuses",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyStatus> updateTreatyStatus(@Valid @RequestBody TreatyStatus treatyStatus) throws URISyntaxException {
        log.debug("REST request to update TreatyStatus : {}", treatyStatus);
        if (treatyStatus.getId() == null) {
            return createTreatyStatus(treatyStatus);
        }
        TreatyStatus result = treatyStatusRepository.save(treatyStatus);
        treatyStatusSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("treatyStatus", treatyStatus.getId().toString()))
            .body(result);
    }

    /**
     * GET  /treaty-statuses : get all the treatyStatuses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of treatyStatuses in body
     */
    @RequestMapping(value = "/treaty-statuses",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TreatyStatus> getAllTreatyStatuses() {
        log.debug("REST request to get all TreatyStatuses");
        List<TreatyStatus> treatyStatuses = treatyStatusRepository.findAll();
        return treatyStatuses;
    }

    /**
     * GET  /treaty-statuses/:id : get the "id" treatyStatus.
     *
     * @param id the id of the treatyStatus to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the treatyStatus, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/treaty-statuses/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyStatus> getTreatyStatus(@PathVariable Long id) {
        log.debug("REST request to get TreatyStatus : {}", id);
        TreatyStatus treatyStatus = treatyStatusRepository.findOne(id);
        return Optional.ofNullable(treatyStatus)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /treaty-statuses/:id : delete the "id" treatyStatus.
     *
     * @param id the id of the treatyStatus to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/treaty-statuses/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTreatyStatus(@PathVariable Long id) {
        log.debug("REST request to delete TreatyStatus : {}", id);
        treatyStatusRepository.delete(id);
        treatyStatusSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("treatyStatus", id.toString())).build();
    }

    /**
     * SEARCH  /_search/treaty-statuses?query=:query : search for the treatyStatus corresponding
     * to the query.
     *
     * @param query the query of the treatyStatus search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/treaty-statuses",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TreatyStatus> searchTreatyStatuses(@RequestParam String query) {
        log.debug("REST request to search TreatyStatuses for query {}", query);
        return StreamSupport
            .stream(treatyStatusSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
