package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.RiskCriteria;
import com.rgare.global.finre.service.RiskCriteriaService;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RiskCriteria.
 */
@RestController
@RequestMapping("/api")
public class RiskCriteriaResource {

    private final Logger log = LoggerFactory.getLogger(RiskCriteriaResource.class);

    private static final String ENTITY_NAME = "riskCriteria";
        
    private final RiskCriteriaService riskCriteriaService;

    public RiskCriteriaResource(RiskCriteriaService riskCriteriaService) {
        this.riskCriteriaService = riskCriteriaService;
    }

    /**
     * POST  /risk-criteria : Create a new riskCriteria.
     *
     * @param riskCriteria the riskCriteria to create
     * @return the ResponseEntity with status 201 (Created) and with body the new riskCriteria, or with status 400 (Bad Request) if the riskCriteria has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/risk-criteria")
    @Timed
    public ResponseEntity<RiskCriteria> createRiskCriteria(@RequestBody RiskCriteria riskCriteria) throws URISyntaxException {
        log.debug("REST request to save RiskCriteria : {}", riskCriteria);
        if (riskCriteria.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new riskCriteria cannot already have an ID")).body(null);
        }
        RiskCriteria result = riskCriteriaService.save(riskCriteria);
        return ResponseEntity.created(new URI("/api/risk-criteria/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /risk-criteria : Updates an existing riskCriteria.
     *
     * @param riskCriteria the riskCriteria to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated riskCriteria,
     * or with status 400 (Bad Request) if the riskCriteria is not valid,
     * or with status 500 (Internal Server Error) if the riskCriteria couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/risk-criteria")
    @Timed
    public ResponseEntity<RiskCriteria> updateRiskCriteria(@RequestBody RiskCriteria riskCriteria) throws URISyntaxException {
        log.debug("REST request to update RiskCriteria : {}", riskCriteria);
        if (riskCriteria.getId() == null) {
            return createRiskCriteria(riskCriteria);
        }
        RiskCriteria result = riskCriteriaService.save(riskCriteria);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, riskCriteria.getId().toString()))
            .body(result);
    }

    /**
     * GET  /risk-criteria : get all the riskCriteria.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of riskCriteria in body
     */
    @GetMapping("/risk-criteria")
    @Timed
    public List<RiskCriteria> getAllRiskCriteria() {
        log.debug("REST request to get all RiskCriteria");
        return riskCriteriaService.findAll();
    }

    /**
     * GET  /risk-criteria/:id : get the "id" riskCriteria.
     *
     * @param id the id of the riskCriteria to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the riskCriteria, or with status 404 (Not Found)
     */
    @GetMapping("/risk-criteria/{id}")
    @Timed
    public ResponseEntity<RiskCriteria> getRiskCriteria(@PathVariable Long id) {
        log.debug("REST request to get RiskCriteria : {}", id);
        RiskCriteria riskCriteria = riskCriteriaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(riskCriteria));
    }

    /**
     * DELETE  /risk-criteria/:id : delete the "id" riskCriteria.
     *
     * @param id the id of the riskCriteria to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/risk-criteria/{id}")
    @Timed
    public ResponseEntity<Void> deleteRiskCriteria(@PathVariable Long id) {
        log.debug("REST request to delete RiskCriteria : {}", id);
        riskCriteriaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/risk-criteria?query=:query : search for the riskCriteria corresponding
     * to the query.
     *
     * @param query the query of the riskCriteria search 
     * @return the result of the search
     */
    @GetMapping("/_search/risk-criteria")
    @Timed
    public List<RiskCriteria> searchRiskCriteria(@RequestParam String query) {
        log.debug("REST request to search RiskCriteria for query {}", query);
        return riskCriteriaService.search(query);
    }


}
