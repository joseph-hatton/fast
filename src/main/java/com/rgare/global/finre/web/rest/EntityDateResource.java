package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.EntityDate;
import com.rgare.global.finre.repository.EntityDateRepository;
import com.rgare.global.finre.repository.search.EntityDateSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing EntityDate.
 */
@RestController
@RequestMapping("/api")
public class EntityDateResource {

    private final Logger log = LoggerFactory.getLogger(EntityDateResource.class);

    private static final String ENTITY_NAME = "entityDate";

    private EntityDateRepository entityDateRepository;

    private EntityDateSearchRepository entityDateSearchRepository;

    public EntityDateResource(EntityDateRepository entityDateRepository, EntityDateSearchRepository entityDateSearchRepository) {
        this.entityDateRepository = entityDateRepository;
        this.entityDateSearchRepository = entityDateSearchRepository;
    }

    /**
     * POST  /entity-dates : Create a new entityDate.
     *
     * @param entityDate the entityDate to create
     * @return the ResponseEntity with status 201 (Created) and with body the new entityDate, or with status 400 (Bad Request) if the entityDate has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/entity-dates",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EntityDate> createEntityDate(@Valid @RequestBody EntityDate entityDate) throws URISyntaxException {
        log.debug("REST request to save EntityDate : {}", entityDate);
        if (entityDate.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new entityDate cannot already have an ID")).body(null);
        }
        EntityDate result = entityDateRepository.save(entityDate);
        entityDateSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/entity-dates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /entity-dates : Updates an existing entityDate.
     *
     * @param entityDate the entityDate to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated entityDate,
     * or with status 400 (Bad Request) if the entityDate is not valid,
     * or with status 500 (Internal Server Error) if the entityDate couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/entity-dates",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EntityDate> updateEntityDate(@Valid @RequestBody EntityDate entityDate) throws URISyntaxException {
        log.debug("REST request to update EntityDate : {}", entityDate);
        if (entityDate.getId() == null) {
            return createEntityDate(entityDate);
        }
        EntityDate result = entityDateRepository.save(entityDate);
        entityDateSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, entityDate.getId().toString()))
            .body(result);
    }

    /**
     * GET  /entity-dates : get all the entityDates.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of entityDates in body
     */
    @RequestMapping(value = "/entity-dates",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<EntityDate> getAllEntityDates() {
        log.debug("REST request to get all EntityDates");
        List<EntityDate> entityDates = entityDateRepository.findAll();
        return entityDates;
    }

    /**
     * GET  /entity-dates/:id : get the "id" entityDate.
     *
     * @param id the id of the entityDate to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the entityDate, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/entity-dates/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EntityDate> getEntityDate(@PathVariable Long id) {
        log.debug("REST request to get EntityDate : {}", id);
        EntityDate entityDate = entityDateRepository.findOne(id);
        return Optional.ofNullable(entityDate)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

  /**
   * GET  /entity-dates : get all the entityDates.
   *
   * @return the ResponseEntity with status 200 (OK) and the list of entityDates in body
   */
  @RequestMapping(value = "/entity-dates/treaties/{entityId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public List<EntityDate> getAllEntityDatesByEntityId(@PathVariable Long entityId) {
    log.debug("REST request to get all EntityDates", entityId);
    List<EntityDate> entityDates = entityDateRepository.findAllByEntityId(entityId);
    return entityDates;
  }

    /**
     * DELETE  /entity-dates/:id : delete the "id" entityDate.
     *
     * @param id the id of the entityDate to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/entity-dates/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteEntityDate(@PathVariable Long id) {
        log.debug("REST request to delete EntityDate : {}", id);
        entityDateRepository.delete(id);
        entityDateSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/entity-dates?query=:query : search for the entityDate corresponding
     * to the query.
     *
     * @param query the query of the entityDate search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/entity-dates",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<EntityDate> searchEntityDates(@RequestParam String query) {
        log.debug("REST request to search EntityDates for query {}", query);
        return StreamSupport
            .stream(entityDateSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
