package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.SeriatimType;
import com.rgare.global.finre.repository.SeriatimTypeRepository;
import com.rgare.global.finre.repository.search.SeriatimTypeSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SeriatimType.
 */
@RestController
@RequestMapping("/api")
public class SeriatimTypeResource {

    private final Logger log = LoggerFactory.getLogger(SeriatimTypeResource.class);

    private static final String ENTITY_NAME = "seriatimType";

    private SeriatimTypeRepository seriatimTypeRepository;

    private SeriatimTypeSearchRepository seriatimTypeSearchRepository;

    public SeriatimTypeResource(SeriatimTypeRepository seriatimTypeRepository, SeriatimTypeSearchRepository seriatimTypeSearchRepository) {
        this.seriatimTypeRepository = seriatimTypeRepository;
        this.seriatimTypeSearchRepository = seriatimTypeSearchRepository;
    }

    /**
     * POST  /seriatim-types : Create a new seriatimType.
     *
     * @param seriatimType the seriatimType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new seriatimType, or with status 400 (Bad Request) if the seriatimType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/seriatim-types",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SeriatimType> createSeriatimType(@Valid @RequestBody SeriatimType seriatimType) throws URISyntaxException {
        log.debug("REST request to save SeriatimType : {}", seriatimType);
        if (seriatimType.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("seriatimType", "idexists", "A new seriatimType cannot already have an ID")).body(null);
        }
        SeriatimType result = seriatimTypeRepository.save(seriatimType);
        seriatimTypeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/seriatim-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("seriatimType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /seriatim-types : Updates an existing seriatimType.
     *
     * @param seriatimType the seriatimType to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated seriatimType,
     * or with status 400 (Bad Request) if the seriatimType is not valid,
     * or with status 500 (Internal Server Error) if the seriatimType couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/seriatim-types",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SeriatimType> updateSeriatimType(@Valid @RequestBody SeriatimType seriatimType) throws URISyntaxException {
        log.debug("REST request to update SeriatimType : {}", seriatimType);
        if (seriatimType.getId() == null) {
            return createSeriatimType(seriatimType);
        }
        SeriatimType result = seriatimTypeRepository.save(seriatimType);
        seriatimTypeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("seriatimType", seriatimType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /seriatim-types : get all the seriatimTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of seriatimTypes in body
     */
    @RequestMapping(value = "/seriatim-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SeriatimType> getAllSeriatimTypes() {
        log.debug("REST request to get all SeriatimTypes");
        List<SeriatimType> seriatimTypes = seriatimTypeRepository.findAll();
        return seriatimTypes;
    }

    /**
     * GET  /seriatim-types/:id : get the "id" seriatimType.
     *
     * @param id the id of the seriatimType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the seriatimType, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/seriatim-types/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SeriatimType> getSeriatimType(@PathVariable Long id) {
        log.debug("REST request to get SeriatimType : {}", id);
        SeriatimType seriatimType = seriatimTypeRepository.findOne(id);
        return Optional.ofNullable(seriatimType)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /seriatim-types/:id : delete the "id" seriatimType.
     *
     * @param id the id of the seriatimType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/seriatim-types/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSeriatimType(@PathVariable Long id) {
        log.debug("REST request to delete SeriatimType : {}", id);
        seriatimTypeRepository.delete(id);
        seriatimTypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("seriatimType", id.toString())).build();
    }

    /**
     * SEARCH  /_search/seriatim-types?query=:query : search for the seriatimType corresponding
     * to the query.
     *
     * @param query the query of the seriatimType search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/seriatim-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SeriatimType> searchSeriatimTypes(@RequestParam String query) {
        log.debug("REST request to search SeriatimTypes for query {}", query);
        return StreamSupport
            .stream(seriatimTypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
