package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.google.common.collect.Maps;
import com.rgare.global.finre.domain.Count;
import com.rgare.global.finre.domain.TreatyTracking;
import com.rgare.global.finre.repository.TreatyTrackingRepository;
import com.rgare.global.finre.repository.search.TreatyTrackingSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static net.logstash.logback.marker.Markers.append;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing TreatyTracking.
 */
@RestController
@RequestMapping("/api")
public class TreatyTrackingResource {

  private static final String PAGEABLE = "pageable";
  private static final String TREATY_ID = "treatyId";
  private static final String START_YEAR = "startYear";
  private static final String START_MONTH = "startMonth";
  private static final String START_DAY = "startDay";
  private static final String END_YEAR = "endYear";
  private static final String END_MONTH = "endMonth";
  private static final String END_DAY = "endDay";
  private static final String QUERY = "query";
  private final Logger log = LoggerFactory.getLogger(TreatyTrackingResource.class);
  private static final String OPEN = "Open";
  private static final String TREATY_TRACKING_ID = "treatyTrackingId";
  private static final String TREATY_TRACKING = "treatyTracking";
  private static final String ENTITY_NAME = TREATY_TRACKING;
  private static final int TREATY_IS_ACTIVE = 1;

  private TreatyTrackingRepository treatyTrackingRepository;

  private TreatyTrackingSearchRepository treatyTrackingSearchRepository;

  public TreatyTrackingResource(TreatyTrackingRepository treatyTrackingRepository, TreatyTrackingSearchRepository treatyTrackingSearchRepository) {
    this.treatyTrackingRepository = treatyTrackingRepository;
    this.treatyTrackingSearchRepository = treatyTrackingSearchRepository;
  }

  /**
   * POST  /treaty-trackings : Create a new treatyTracking.
   *
   * @param treatyTracking the treatyTracking to create
   * @return the ResponseEntity with status 201 (Created) and with body the new treatyTracking, or with status 400 (Bad Request) if the treatyTracking has already an ID
   * @throws URISyntaxException if the Location URI syntax is incorrect
   */
  @RequestMapping(value = "/treaty-trackings",
    method = RequestMethod.POST,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<TreatyTracking> createTreatyTracking(@RequestBody TreatyTracking treatyTracking) throws URISyntaxException {
    log.debug(append(TREATY_TRACKING, treatyTracking), "REST request to save TreatyTracking");
    if (treatyTracking.getId() != null) {
      return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new treatyTracking cannot already have an ID")).body(null);
    }
    TreatyTracking result = treatyTrackingRepository.save(treatyTracking);
    treatyTrackingSearchRepository.save(result);
    return ResponseEntity.created(new URI("/api/treaty-trackings/" + result.getId()))
      .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
      .body(result);
  }

  /**
   * PUT  /treaty-trackings : Updates an existing treatyTracking.
   *
   * @param treatyTracking the treatyTracking to update
   * @return the ResponseEntity with status 200 (OK) and with body the updated treatyTracking,
   * or with status 400 (Bad Request) if the treatyTracking is not valid,
   * or with status 500 (Internal Server Error) if the treatyTracking couldnt be updated
   * @throws URISyntaxException if the Location URI syntax is incorrect
   */
  @RequestMapping(value = "/treaty-trackings",
    method = RequestMethod.PUT,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<TreatyTracking> updateTreatyTracking(@RequestBody TreatyTracking treatyTracking) throws URISyntaxException {
    log.debug(append(TREATY_TRACKING, treatyTracking), "REST request to update TreatyTracking");
    if (treatyTracking.getId() == null) {
      return createTreatyTracking(treatyTracking);
    }
    TreatyTracking result = treatyTrackingRepository.save(treatyTracking);
    treatyTrackingSearchRepository.save(result);
    return ResponseEntity.ok()
      .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, treatyTracking.getId().toString()))
      .body(result);
  }

  /**
   * GET  /treaty-trackings : get all the treatyTrackings.
   *
   * @param pageable the pagination information
   * @return the ResponseEntity with status 200 (OK) and the list of treatyTrackings in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/treaty-trackings",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllTreatyTrackings(Pageable pageable) throws URISyntaxException {
    log.debug(append(PAGEABLE, pageable), "REST request to get a page of TreatyTrackings");

    Page<TreatyTracking> page = treatyTrackingRepository.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treaty-trackings");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
  }

  /**
   * GET  /treaty-trackings/:id : get the "id" treatyTracking.
   *
   * @param id the id of the treatyTracking to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the treatyTracking, or with status 404 (Not Found)
   */
  @RequestMapping(value = "/treaty-trackings/{id}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<TreatyTracking> getTreatyTracking(@PathVariable Long id) {
    log.debug(append(TREATY_TRACKING_ID, id), "REST request to get TreatyTracking");

    TreatyTracking treatyTracking = treatyTrackingRepository.findOne(id);
    return Optional.ofNullable(treatyTracking)
      .map(result -> new ResponseEntity<>(
        result,
        HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  /**
   * GET  /treaty-trackings/treaty/{treatyId} -> get all by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "/treaty-trackings/treaty/{treatyId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllByTreatyId(@PathVariable Long treatyId, Pageable pageable) throws URISyntaxException {
    log.debug(append(TREATY_ID, treatyId).and(append(PAGEABLE, pageable)), "REST request to get all TreatyTracking by Treaty Id");

    Page<TreatyTracking> page = treatyTrackingRepository.findAllByTreatyIdAndIsActive(treatyId, pageable, TREATY_IS_ACTIVE);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treaty-trackings/treaty");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
  }


  /**
   * GET  /treaty-trackings/treaty/{treatyId} -> get all by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "/treaty-trackings/Open-Tracking/{treatyId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllByTreatyIdAndTrackingStatus(@PathVariable Long treatyId, Pageable pageable) throws URISyntaxException {
    log.debug(append(TREATY_ID, treatyId).and(append(PAGEABLE, pageable)), "REST request to get all TreatyTracking by Treaty Id and Tracking Status");

    Page<TreatyTracking> page = treatyTrackingRepository.findAllByTreatyIdAndIsActiveAndTrackingStatus(treatyId, pageable, TREATY_IS_ACTIVE, OPEN);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treaty-trackings/Open-Tracking");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
  }


  /**
   * GET  /treaty-trackings/treaty/{treatyId} -> get all by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "/treaty-trackings/{treatyId}/dates/{startYear}/{startMonth}/{startDay}/{endYear}/{endMonth}/{endDay}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllByReportingMonthYearDates(
    @PathVariable Long treatyId,
    @PathVariable Integer startYear,
    @PathVariable Integer startMonth,
    @PathVariable Integer startDay,
    @PathVariable Integer endYear,
    @PathVariable Integer endMonth,
    @PathVariable Integer endDay,
    Pageable pageable)
    throws URISyntaxException {
    log.debug(append(TREATY_ID, treatyId)
      .and(append(START_YEAR, startYear))
      .and(append(START_MONTH, startMonth))
      .and(append(START_DAY, startDay))
      .and(append(END_YEAR, endYear))
      .and(append(END_MONTH, endMonth))
      .and(append(END_DAY, endDay))
      .and(append(PAGEABLE, pageable)), "REST request to get all Reporting Month Year Dates");

    Page<TreatyTracking> page = treatyTrackingRepository.findByTreatyIdAndIsActiveAndReportingMonthYearBetween(
      treatyId, TREATY_IS_ACTIVE,
      LocalDate.of(startYear, startMonth, startDay),
      LocalDate.of(endYear, endMonth, endDay),
      pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treaty-trackings/dates/");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
  }

  /**
   * GET  /treaty-trackings/treaty/{treatyId} -> get all by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "/treaty-trackings/ExpirationDatesAssuming/{endYear}/{endMonth}/{endDay}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllByCalculatedAssumingGraceDate(
    @PathVariable Integer endYear,
    @PathVariable Integer endMonth,
    @PathVariable Integer endDay
  ) throws URISyntaxException {
    log.debug(append(END_YEAR, endYear)
      .and(append(END_MONTH, endMonth))
      .and(append(END_DAY, endDay)), "REST request to get all Treaty Tracking by calculated assuming grace dates");
    List<TreatyTracking> Notification = treatyTrackingRepository.findByIsActiveAndCalculatedAssumingGraceDateBetween(
      TREATY_IS_ACTIVE, LocalDate.now(), LocalDate.of(endYear, endMonth, endDay));
    return new ResponseEntity<>(Notification, HttpStatus.OK);
  }


  /**
   * GET  /treaty-trackings/treaty/{treatyId} -> get all by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "/treaty-trackings/ExpirationDatesReporingDue/{endYear}/{endMonth}/{endDay}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllByCalculatedReportingDueDate(
    @PathVariable Integer endYear,
    @PathVariable Integer endMonth,
    @PathVariable Integer endDay
  ) throws URISyntaxException {
    log.debug(append(END_YEAR, endYear)
      .and(append(END_MONTH, endMonth))
      .and(append(END_DAY, endDay)), "REST request to get all Treaty Tracking by calculated reporting due date");
    List<TreatyTracking> Notification = treatyTrackingRepository.findByIsActiveAndTrackingStatusAndCalculatedReportingDueDateBetweenAndReportingReceivedDateIsNullAndReportNotifFlag(
      TREATY_IS_ACTIVE, OPEN, LocalDate.now(), LocalDate.of(endYear, endMonth, endDay), 0);
    return new ResponseEntity<>(Notification, HttpStatus.OK);
  }

  /**
   * GET  /cleared-treaty-tracking-reporting -> get all by cleared reporting records
   */
  @RequestMapping(value = "/cleared-treaty-tracking-reporting",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllByTreatyTrackingReportingCleared() throws URISyntaxException {
    log.debug("REST request to get all Treaty Tracking where the reporting is cleared");
    List<TreatyTracking> Notification = treatyTrackingRepository.findByIsActiveAndTrackingStatusAndReportNotifFlag(
      TREATY_IS_ACTIVE, OPEN, 1);
    return new ResponseEntity<>(Notification, HttpStatus.OK);
  }

  /**
   * GET  /treaty-trackings/treaty/{treatyId} -> get all by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "/treaty-trackings/SeriatimDueDate",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllBySeriatimDate() throws URISyntaxException {
    log.debug("REST request to get all Treaty Tracking by seriatim date");
    List<TreatyTracking> NotificationSeriatim = treatyTrackingRepository.findByIsActiveAndCalculatedSeriatimDueDateBetween(
      TREATY_IS_ACTIVE, LocalDate.now(), LocalDate.now().plusDays(7));
    return new ResponseEntity<>(NotificationSeriatim, HttpStatus.OK);
  }

  /**
   * GET  /treaty-trackings/treaty/{treatyId} -> get all by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "/treaty-trackings/AssumingDate/AlmostPastDueDate",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllByCalculateAssumingDateBetween() throws URISyntaxException {
    log.debug("REST request to get all Treaty Tracking by calculated assuming date between");

    List<TreatyTracking> NotificationDueAssuming = treatyTrackingRepository.findByIsActiveAndTrackingStatusAndCashDueFlagAndCalculatedAssumingDateBetweenAndCashDueTypeAndCashSentRecDateIsNull(
      TREATY_IS_ACTIVE, OPEN, "Yes", LocalDate.now(), LocalDate.now().plusDays(7), "Assuming");
    return new ResponseEntity<>(NotificationDueAssuming, HttpStatus.OK);
  }

  /**
   * GET  /treaty-trackings/treaty/{treatyId} -> get all by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "/treaty-trackings/AssumingDate/AlmostExpired",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllByCalculateAssumingDateEarly() throws URISyntaxException {
    log.debug("REST request to get all Treaty Tracking by calculated assuming date early");

    List<TreatyTracking> NotificationAlmostExpiredAssuming = treatyTrackingRepository.findByIsActiveAndTrackingStatusAndCashDueFlagAndCalculatedAssumingDateBeforeAndCalculatedAssumingGraceDateAfterAndCashDueTypeAndCashSentRecDateIsNull(
      TREATY_IS_ACTIVE, OPEN, "Yes", LocalDate.now(), LocalDate.now(), "Assuming");
    return new ResponseEntity<>(NotificationAlmostExpiredAssuming, HttpStatus.OK);
  }

  /**
   * GET  /treaty-trackings/treaty/{treatyId} -> get all by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "/treaty-trackings/AssumingDate/Expired",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllByCalculateAssumingDatePastDue() throws URISyntaxException {
    log.debug("REST request to get all Treaty Tracking by calculated assuming date past due");
    List<TreatyTracking> NotificationSettlementAssumingExpired = treatyTrackingRepository.findByIsActiveAndTrackingStatusAndCashDueFlagAndCalculatedAssumingGraceDateBeforeAndCashDueTypeAndCashSentRecDateIsNull(
      TREATY_IS_ACTIVE, OPEN, "Yes", LocalDate.now().plusDays(1), "Assuming");
    return new ResponseEntity<>(NotificationSettlementAssumingExpired, HttpStatus.OK);
  }

  /**
   * GET  /treaty-trackings/treaty/{treatyId} -> get all by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "/treaty-trackings/CedingDate/AlmostPastDueDate",
  method = RequestMethod.GET,
  produces = MediaType.APPLICATION_JSON_VALUE)
@Timed
public ResponseEntity<List<TreatyTracking>> getAllByCalculateCedingDateBetween() throws URISyntaxException {
  log.debug("REST request to get all Treaty Tracking by calculated Ceding date between");

  List<TreatyTracking> NotificationDueCeding = treatyTrackingRepository.findByIsActiveAndTrackingStatusAndCashDueFlagAndCalculatedCedingDateBetweenAndCashDueTypeAndCashSentRecDateIsNull(
    TREATY_IS_ACTIVE, OPEN, "Yes", LocalDate.now(), LocalDate.now().plusDays(7), "Ceding");
  return new ResponseEntity<>(NotificationDueCeding, HttpStatus.OK);
}

/**
 * GET  /treaty-trackings/treaty/{treatyId} -> get all by Treaty Id treaty-trackings.
 */
@RequestMapping(value = "/treaty-trackings/CedingDate/AlmostExpired",
  method = RequestMethod.GET,
  produces = MediaType.APPLICATION_JSON_VALUE)
@Timed
public ResponseEntity<List<TreatyTracking>> getAllByCalculateCedingDateEarly() throws URISyntaxException {
  log.debug("REST request to get all Treaty Tracking by calculated CedingDate early");

  List<TreatyTracking> NotificationAlmostExpiredCeding = treatyTrackingRepository.findByIsActiveAndTrackingStatusAndCashDueFlagAndCalculatedCedingDateBeforeAndCalculatedCedingGraceDateAfterAndCashDueTypeAndCashSentRecDateIsNull(
    TREATY_IS_ACTIVE, OPEN, "Yes", LocalDate.now(), LocalDate.now(), "Ceding");
  return new ResponseEntity<>(NotificationAlmostExpiredCeding, HttpStatus.OK);
}

/**
 * GET  /treaty-trackings/treaty/{treatyId} -> get all by Treaty Id treaty-trackings.
 */
@RequestMapping(value = "/treaty-trackings/CedingDate/Expired",
  method = RequestMethod.GET,
  produces = MediaType.APPLICATION_JSON_VALUE)
@Timed
public ResponseEntity<List<TreatyTracking>> getAllByCalculateCedingDatePastDue() throws URISyntaxException {
  log.debug("REST request to get all Treaty Tracking by calculated Ceding date past due");
  List<TreatyTracking> NotificationSettlementCedingExpired = treatyTrackingRepository.findByIsActiveAndTrackingStatusAndCashDueFlagAndCalculatedCedingGraceDateBeforeAndCashDueTypeAndCashSentRecDateIsNull(
    TREATY_IS_ACTIVE, OPEN, "Yes", LocalDate.now().plusDays(1), "Ceding");
  return new ResponseEntity<>(NotificationSettlementCedingExpired, HttpStatus.OK);
}



  /**
   * GET tracking with reporting due date past and  cash due flag no
   */
  @RequestMapping(value = "/treaty-trackings/Reporting/noCashDue/Expired",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getTrackingPastReportingDueDate() throws URISyntaxException {
    log.debug("REST request to  GET tracking with reporting due date past and  cash due flag no");
    List<TreatyTracking> NotificationReportingDueExpired = treatyTrackingRepository.findByIsActiveAndTrackingStatusAndReportingReceivedDateIsNullAndCalculatedReportingDueDateBefore(
      TREATY_IS_ACTIVE, OPEN, LocalDate.now());
    return new ResponseEntity<>(NotificationReportingDueExpired, HttpStatus.OK);
  }



  /**
   * GET  already expired date
   */
  @RequestMapping(value = "/treaty-trackings/SeriatimDueDateExpired",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllBySeriatimDateExpired() throws URISyntaxException {
    log.debug("REST request to get all Treaty Tracking by seriatim date expired");
    List<TreatyTracking> NotificationSeriatimExpired = treatyTrackingRepository.findByIsActiveAndTrackingStatusAndCalculatedSeriatimDueDateBefore(
      TREATY_IS_ACTIVE, OPEN, LocalDate.now());
    return new ResponseEntity<>(NotificationSeriatimExpired, HttpStatus.OK);
  }

  /**
   * GET  already expired date
   */
  @RequestMapping(value = "/treaty-trackings/SettlementAsummingExpired",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllExpiredAssuming() throws URISyntaxException {
    log.debug("REST request to get all Treaty Tracking by expired assuming");
    List<TreatyTracking> NotificationSettlementAssumingExpired = treatyTrackingRepository.findByIsActiveAndTrackingStatusAndCalculatedAssumingGraceDateBefore(
      TREATY_IS_ACTIVE, OPEN, LocalDate.now());
    return new ResponseEntity<>(NotificationSettlementAssumingExpired, HttpStatus.OK);
  }

  /**
   * GET  already expired date
   */
  @RequestMapping(value = "/treaty-trackings/SettlementCedingExpired",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllExpiredCeding() throws URISyntaxException {
    log.debug("REST request to get all Treaty Tracking by expired ceding");
    List<TreatyTracking> NotificationSettlementCedingExpired = treatyTrackingRepository.findByIsActiveAndTrackingStatusAndCalculatedCedingGraceDateBefore(
      TREATY_IS_ACTIVE, OPEN, LocalDate.now());
    return new ResponseEntity<>(NotificationSettlementCedingExpired, HttpStatus.OK);
  }


  /**
   * GET  already expired date
   */
  @RequestMapping(value = "/treaty-trackings/ReportingExpired",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllExpiredReporting() throws URISyntaxException {
    log.debug("REST request to get all Treaty Tracking by expired reporting");
    List<TreatyTracking> notificationsReportingExpired = treatyTrackingRepository.findByIsActiveAndTrackingStatusAndCalculatedReportingDueDateBeforeAndCalculatedAssumingGraceDateAfterAndReportingReceivedDateIsNullAndReportNotifFlag(
      TREATY_IS_ACTIVE, OPEN, LocalDate.now(), LocalDate.now(), 0);
    return new ResponseEntity<>(notificationsReportingExpired, HttpStatus.OK);
  }

   /**
   * GET  past grace period date
   */
  @RequestMapping(value = "/treaty-trackings/ReportingPastGracePeriod",
  method = RequestMethod.GET,
  produces = MediaType.APPLICATION_JSON_VALUE)
@Timed
public ResponseEntity<List<TreatyTracking>> getAllGraceExpiredReporting() throws URISyntaxException {
  log.debug("REST request to get all Treaty Tracking by expired grace period");
  List<TreatyTracking> notificationsReportingExpired = treatyTrackingRepository.findByIsActiveAndTrackingStatusAndCalculatedAssumingGraceDateBeforeAndReportingReceivedDateIsNullAndReportNotifFlag(
    TREATY_IS_ACTIVE, OPEN, LocalDate.now(), 0);
  return new ResponseEntity<>(notificationsReportingExpired, HttpStatus.OK);
}


  /**
   * GET  /treaty-trackings/treaty/{treatyId} -> get all by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "/treaty-trackings/{treatyId}/TrackingPeriod/{endYear}/{endMonth}/{endDay}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public List<TreatyTracking> getAllByCalculatedAssumingGraceDate(
    @PathVariable Long treatyId,
    @PathVariable Integer endYear,
    @PathVariable Integer endMonth,
    @PathVariable Integer endDay
  ) throws URISyntaxException {
    log.debug(append(TREATY_ID, treatyId)
      .and(append(END_YEAR, endYear))
      .and(append(END_MONTH, endMonth))
      .and(append(END_DAY, endDay)), "REST request to get all Treaty Tracking by calculated assuming grace date");
    List<TreatyTracking> TrackingsPeriod = treatyTrackingRepository.findByTreatyIdAndIsActiveAndReportingMonthYear(
      treatyId, TREATY_IS_ACTIVE,
      LocalDate.of(endYear, endMonth, endDay));
    return TrackingsPeriod;
  }

  /**
   * GET  /treaty-trackings/treaty/{treatyId} -> get all by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "/treaty-trackings/ExpirationDatesCeding/{endYear}/{endMonth}/{endDay}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllByCalculatedCedingGraceDate(
    @PathVariable Integer endYear,
    @PathVariable Integer endMonth,
    @PathVariable Integer endDay
  ) throws URISyntaxException {
    log.debug(append(END_YEAR, endYear)
      .and(append(END_MONTH, endMonth))
      .and(append(END_DAY, endDay)), "REST request to get all Treaty Tracking by calculated ceding grace date");
    List<TreatyTracking> Notification = treatyTrackingRepository.findByIsActiveAndCalculatedCedingGraceDateBetween(
      TREATY_IS_ACTIVE, LocalDate.now(),
      LocalDate.of(endYear, endMonth, endDay));
    return new ResponseEntity<>(Notification, HttpStatus.OK);
  }


  /**
   * GET  /treaty-trackings/treaty/{treatyId} -> get all by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "/treaty-trackings/{treatyId}/LastReportingMonthYear/{endYear}/{endMonth}/{endDay}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> getAllByLastReportingMonthYear(
    @PathVariable Integer endYear,
    @PathVariable Integer endMonth,
    @PathVariable Integer endDay,
    @PathVariable Long treatyId
  )
    throws URISyntaxException {
    log.debug(append(END_YEAR, endYear)
      .and(append(END_MONTH, endMonth))
      .and(append(END_DAY, endDay))
      .and(append(TREATY_ID, treatyId)), "REST request to get all Treaty Tracking by last reporting month");
    List<TreatyTracking> LastReportingMonthYear = treatyTrackingRepository.findByIsActiveAndReportingMonthYearBeforeAndTreatyId(
      TREATY_IS_ACTIVE, LocalDate.of(endYear, endMonth, endDay), treatyId);
    return new ResponseEntity<>(LastReportingMonthYear, HttpStatus.OK);
  }

  /**
   * GET  /treaty-trackings/count/{treatyId} -> get count by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "treaty-trackings/count/{treatyId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Count> getCountByTreaty(@PathVariable Long treatyId) throws URISyntaxException {
    log.debug(append(TREATY_ID, treatyId), "REST request to get all Treaty Tracking by last reporting month");
    Long count = treatyTrackingRepository.countByIsActiveAndTreatyId(1, treatyId);

    return new ResponseEntity<>(new Count(count), HttpStatus.OK);
  }


  /**
   * DELETE  /treaty-trackings/:id : delete the "id" treatyTracking.
   *
   * @param id the id of the treatyTracking to delete
   * @return the ResponseEntity with status 200 (OK)
   */
  @RequestMapping(value = "/treaty-trackings/{id}",
    method = RequestMethod.DELETE,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Void> deleteTreatyTracking(@PathVariable Long id) {
    log.debug(append(TREATY_TRACKING_ID, id), "REST request to delete TreatyTracking");
    treatyTrackingRepository.delete(id);
    treatyTrackingSearchRepository.delete(id);
    return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(TREATY_TRACKING, id.toString())).build();
  }

  /**
   * SEARCH  /_search/treaty-trackings?query=:query : search for the treatyTracking corresponding
   * to the query.
   *
   * @param query the query of the treatyTracking search
   * @return the result of the search
   */
  @RequestMapping(value = "/_search/treaty-trackings",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTracking>> searchTreatyTrackings(@RequestParam String query, Pageable pageable)
    throws URISyntaxException {
    log.debug(append(QUERY, query).and(append(PAGEABLE, pageable)), "REST request to search for a page of TreatyTrackings for query");
    Page<TreatyTracking> page = treatyTrackingSearchRepository.search(queryStringQuery(query), pageable);

    HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/treaty-trackings");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
  }

}
