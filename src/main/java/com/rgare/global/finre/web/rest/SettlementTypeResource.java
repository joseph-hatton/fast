package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.SettlementType;
import com.rgare.global.finre.repository.SettlementTypeRepository;
import com.rgare.global.finre.repository.search.SettlementTypeSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SettlementType.
 */
@RestController
@RequestMapping("/api")
public class SettlementTypeResource {

    private final Logger log = LoggerFactory.getLogger(SettlementTypeResource.class);

    private static final String ENTITY_NAME = "settlementType";

    private SettlementTypeRepository settlementTypeRepository;

    private SettlementTypeSearchRepository settlementTypeSearchRepository;

    public SettlementTypeResource(SettlementTypeRepository settlementTypeRepository, SettlementTypeSearchRepository settlementTypeSearchRepository) {
        this.settlementTypeRepository = settlementTypeRepository;
        this.settlementTypeSearchRepository = settlementTypeSearchRepository;
    }

    /**
     * POST  /settlement-types : Create a new settlementType.
     *
     * @param settlementType the settlementType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new settlementType, or with status 400 (Bad Request) if the settlementType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/settlement-types",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SettlementType> createSettlementType(@RequestBody SettlementType settlementType) throws URISyntaxException {
        log.debug("REST request to save SettlementType : {}", settlementType);
        if (settlementType.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("settlementType", "idexists", "A new settlementType cannot already have an ID")).body(null);
        }
        SettlementType result = settlementTypeRepository.save(settlementType);
        settlementTypeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/settlement-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("settlementType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /settlement-types : Updates an existing settlementType.
     *
     * @param settlementType the settlementType to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated settlementType,
     * or with status 400 (Bad Request) if the settlementType is not valid,
     * or with status 500 (Internal Server Error) if the settlementType couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/settlement-types",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SettlementType> updateSettlementType(@RequestBody SettlementType settlementType) throws URISyntaxException {
        log.debug("REST request to update SettlementType : {}", settlementType);
        if (settlementType.getId() == null) {
            return createSettlementType(settlementType);
        }
        SettlementType result = settlementTypeRepository.save(settlementType);
        settlementTypeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("settlementType", settlementType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /settlement-types : get all the settlementTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of settlementTypes in body
     */
    @RequestMapping(value = "/settlement-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SettlementType> getAllSettlementTypes() {
        log.debug("REST request to get all SettlementTypes");
        List<SettlementType> settlementTypes = settlementTypeRepository.findAll();
        return settlementTypes;
    }

    /**
     * GET  /settlement-types/:id : get the "id" settlementType.
     *
     * @param id the id of the settlementType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the settlementType, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/settlement-types/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SettlementType> getSettlementType(@PathVariable Long id) {
        log.debug("REST request to get SettlementType : {}", id);
        SettlementType settlementType = settlementTypeRepository.findOne(id);
        return Optional.ofNullable(settlementType)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /settlement-types/:id : delete the "id" settlementType.
     *
     * @param id the id of the settlementType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/settlement-types/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSettlementType(@PathVariable Long id) {
        log.debug("REST request to delete SettlementType : {}", id);
        settlementTypeRepository.delete(id);
        settlementTypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("settlementType", id.toString())).build();
    }

    /**
     * SEARCH  /_search/settlement-types?query=:query : search for the settlementType corresponding
     * to the query.
     *
     * @param query the query of the settlementType search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/settlement-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SettlementType> searchSettlementTypes(@RequestParam String query) {
        log.debug("REST request to search SettlementTypes for query {}", query);
        return StreamSupport
            .stream(settlementTypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
