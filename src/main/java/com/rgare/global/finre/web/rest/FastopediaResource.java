package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.Fastopedia;
import com.rgare.global.finre.repository.FastopediaRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;

/**
 * REST controller for managing Fastopedia.
 */
@RestController
@RequestMapping("/api")
public class FastopediaResource {

    private final Logger log = LoggerFactory.getLogger(Fastopedia.class);

    private static final String ENTITY_NAME = "fastopedia";

    private FastopediaRepository fastopediaRepository;

    public FastopediaResource(FastopediaRepository fastopediaRepository) {
        this.fastopediaRepository = fastopediaRepository;
    }

    /**
     * POST  /fastopedia : Create a new fastopedia.
     *
     * @param fastopedia the agreementType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new fastopedia, or with status 400 (Bad Request) if the fastopedia has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/fastopedia",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Fastopedia> createFastopedia(@Valid @RequestBody Fastopedia fastopedia) throws URISyntaxException {
        log.debug("REST request to save Fastopedia : {}", fastopedia);
        if (fastopedia.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new fastopedia cannot already have an ID")).body(null);
        }
        Fastopedia result = fastopediaRepository.save(fastopedia);
        return ResponseEntity.created(new URI("/api/fastopedia/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /fastopedia : Updates an existing fastopedia.
     *
     * @param fastopedia the fastopedia to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated fastopedia,
     * or with status 400 (Bad Request) if the fastopedia is not valid,
     * or with status 500 (Internal Server Error) if the fastopedia couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/fastopedia",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Fastopedia> updateFastopedia(@Valid @RequestBody Fastopedia fastopedia) throws URISyntaxException {
        log.debug("REST request to update Fastopedia : {}", fastopedia);
        Fastopedia result = fastopediaRepository.save(fastopedia);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, fastopedia.getId().toString()))
            .body(result);
    }

    /**
     * GET  /fastopedia : get all the fastopedia.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of fastopedia in body
     */
    @RequestMapping(value = "/fastopedia",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Fastopedia>> getAllFastopedia() {
        log.debug("REST request to get all Fastopedia");
        List<Fastopedia> fastopedia = fastopediaRepository.findAllByIsActive(1);
        return ResponseEntity.ok()
        .headers(null)
        .body(fastopedia);
    }

    /**
     * GET  /fastopedia : get all the fastopedia.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of fastopedia in body
     */
    @RequestMapping(value = "/fastopedia/{searchQuery}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
@Timed
public ResponseEntity<Set<Fastopedia>> searchFastopedia(@PathVariable String searchQuery) {
    log.debug("REST request to search Fastopedia: {0}", searchQuery);
    Set<Fastopedia> fastopedia = fastopediaRepository.findAllByIsActiveAndTitleContainingIgnoreCase(1, searchQuery);
    fastopedia.addAll(fastopediaRepository.findAllByIsActiveAndContentContainingIgnoreCase(1, searchQuery));
    return ResponseEntity.ok()
    .headers(null)
    .body(fastopedia);
}
}