package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.*;
import com.rgare.global.finre.repository.AuditRiskFeeRepository;
import com.rgare.global.finre.repository.AuditTreatyRepository;
import com.rgare.global.finre.repository.RiskFeeRepository;
import com.rgare.global.finre.repository.search.RiskFeeSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RiskFee.
 */
@RestController
@RequestMapping("/api")
public class RiskFeeResource {

    private final Logger log = LoggerFactory.getLogger(RiskFeeResource.class);

    private static final String ENTITY_NAME = "riskFee";

    private RiskFeeRepository riskFeeRepository;

    private AuditRiskFeeRepository auditRiskFeeRepository;

    private RiskFeeSearchRepository riskFeeSearchRepository;

    public RiskFeeResource(RiskFeeRepository riskFeeRepository, RiskFeeSearchRepository riskFeeSearchRepository, AuditRiskFeeRepository auditRiskFeeRepository) {
        this.riskFeeRepository = riskFeeRepository;
        this.riskFeeSearchRepository = riskFeeSearchRepository;
        this.auditRiskFeeRepository = auditRiskFeeRepository;
    }

    /**
     * POST  /risk-fees : Create a new riskFee.
     *
     * @param riskFee the riskFee to create
     * @return the ResponseEntity with status 201 (Created) and with body the new riskFee, or with status 400 (Bad Request) if the riskFee has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/risk-fees",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<RiskFee> createRiskFee(@Valid @RequestBody RiskFee riskFee) throws URISyntaxException {
        log.debug("REST request to save RiskFee : {}", riskFee);
        if (riskFee.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("riskFee", "idexists", "A new riskFee cannot already have an ID")).body(null);
        }
        RiskFee result = riskFeeRepository.save(riskFee);
      riskFeeSearchRepository.save(result);
        //  auditRiskFeeRepository.save(getAuditRiskFee(riskFee,AuditTreatyAction.SAVE.name()));
        return ResponseEntity.created(new URI("/api/risk-fees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("riskFee", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /risk-fees : Updates an existing riskFee.
     *
     * @param riskFee the riskFee to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated riskFee,
     * or with status 400 (Bad Request) if the riskFee is not valid,
     * or with status 500 (Internal Server Error) if the riskFee couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/risk-fees",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<RiskFee> updateRiskFee(@Valid @RequestBody RiskFee riskFee) throws URISyntaxException {
        log.debug("REST request to update RiskFee : {}", riskFee);
        if (riskFee.getId() == null) {
            return createRiskFee(riskFee);
        }
        RiskFee result = riskFeeRepository.save(riskFee);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("riskFee", riskFee.getId().toString()))
            .body(result);
    }

    /**
     * GET  /risk-fees : get all the riskFees.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of riskFees in body
     */
    @RequestMapping(value = "/risk-fees",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<RiskFee> getAllRiskFees() {
        log.debug("REST request to get all RiskFees");
        List<RiskFee> riskFees = riskFeeRepository.findAll();
        return riskFees;
    }

    /**
     * GET  /risk-fees/:id : get the "id" riskFee.
     *
     * @param id the id of the riskFee to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the riskFee, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/risk-fees/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<RiskFee> getRiskFee(@PathVariable Long id) {
        log.debug("REST request to get RiskFee : {}", id);
        RiskFee riskFee = riskFeeRepository.findOne(id);
        return Optional.ofNullable(riskFee)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /risk-fees/:id : delete the "id" riskFee.
     *
     * @param id the id of the riskFee to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/risk-fees/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteRiskFee(@PathVariable Long id) {
        log.debug("REST request to delete RiskFee : {}", id);
        riskFeeRepository.delete(id);
        riskFeeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("riskFee", id.toString())).build();
    }

    /**
     * SEARCH  /_search/risk-fees?query=:query : search for the riskFee corresponding
     * to the query.
     *
     * @param query the query of the riskFee search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/risk-fees",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<RiskFee> searchRiskFees(@RequestParam String query) {
        log.debug("REST request to search RiskFees for query {}", query);
        return StreamSupport
            .stream(riskFeeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }


}
