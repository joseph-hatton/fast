package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.Count;
import com.rgare.global.finre.domain.TreatySubtransactionRelief;
import com.rgare.global.finre.domain.TreatyTransactionRelief;
import com.rgare.global.finre.repository.TreatySubtransactionReliefRepository;
import com.rgare.global.finre.repository.TreatyTransactionReliefRepository;
import com.rgare.global.finre.repository.search.TreatyTransactionReliefSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing TreatyTransactionRelief.
 */
@RestController
@RequestMapping("/api")
public class TreatyTransactionReliefResource {

    private final Logger log = LoggerFactory.getLogger(TreatyTransactionReliefResource.class);

    private static final String ENTITY_NAME = "treatyTransactionRelief";

    private TreatyTransactionReliefRepository treatyTransactionReliefRepository;
    private TreatySubtransactionReliefRepository treatySubtransactionReliefRepository;

    private TreatyTransactionReliefSearchRepository treatyTransactionReliefSearchRepository;

    public TreatyTransactionReliefResource(TreatyTransactionReliefRepository treatyTransactionReliefRepository, TreatyTransactionReliefSearchRepository treatyTransactionReliefSearchRepository, TreatySubtransactionReliefRepository treatySubtransactionReliefRepository) {
        this.treatyTransactionReliefRepository = treatyTransactionReliefRepository;
        this.treatyTransactionReliefSearchRepository = treatyTransactionReliefSearchRepository;
        this.treatySubtransactionReliefRepository = treatySubtransactionReliefRepository;
    }
    /**
     * POST  /treaty-transaction-reliefs : Create a new treatyTransactionRelief.
     *
     * @param treatyTransactionRelief the treatyTransactionRelief to create
     * @return the ResponseEntity with status 201 (Created) and with body the new treatyTransactionRelief, or with status 400 (Bad Request) if the treatyTransactionRelief has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaty-transaction-reliefs",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyTransactionRelief> createTreatyTransactionRelief(@RequestBody TreatyTransactionRelief treatyTransactionRelief) throws URISyntaxException {
        log.debug("REST request to save TreatyTransactionRelief : {}", treatyTransactionRelief);
        if (treatyTransactionRelief.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("treatyTransactionRelief", "idexists", "A new treatyTransactionRelief cannot already have an ID")).body(null);
        }
        TreatyTransactionRelief result = treatyTransactionReliefRepository.save(treatyTransactionRelief);
        treatyTransactionReliefSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/treaty-transaction-reliefs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("treatyTransactionRelief", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /treaty-transaction-reliefs : Updates an existing treatyTransactionRelief.
     *
     * @param treatyTransactionRelief the treatyTransactionRelief to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated treatyTransactionRelief,
     * or with status 400 (Bad Request) if the treatyTransactionRelief is not valid,
     * or with status 500 (Internal Server Error) if the treatyTransactionRelief couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaty-transaction-reliefs",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyTransactionRelief> updateTreatyTransactionRelief(@RequestBody TreatyTransactionRelief treatyTransactionRelief) throws URISyntaxException {
        log.debug("REST request to update TreatyTransactionRelief : {}", treatyTransactionRelief);
        if (treatyTransactionRelief.getId() == null) {
            return createTreatyTransactionRelief(treatyTransactionRelief);
        }
        TreatyTransactionRelief result = treatyTransactionReliefRepository.save(treatyTransactionRelief);
        treatyTransactionReliefSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("treatyTransactionRelief", treatyTransactionRelief.getId().toString()))
            .body(result);
    }

    /**
     * GET  /treaty-transaction-reliefs : get all the treatyTransactionReliefs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of treatyTransactionReliefs in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/treaty-transaction-reliefs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TreatyTransactionRelief>> getAllTreatyTransactionReliefs(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TreatyTransactionReliefs");
        Page<TreatyTransactionRelief> page = treatyTransactionReliefRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treaty-transaction-reliefs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /treaty-transaction-reliefs/:id : get the "id" treatyTransactionRelief.
     *
     * @param id the id of the treatyTransactionRelief to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the treatyTransactionRelief, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/treaty-transaction-reliefs/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyTransactionRelief> getTreatyTransactionRelief(@PathVariable Long id) {
        log.debug("REST request to get TreatyTransactionRelief : {}", id);
        TreatyTransactionRelief treatyTransactionRelief = treatyTransactionReliefRepository.findOne(id);
        return Optional.ofNullable(treatyTransactionRelief)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /treaty-transactions-reliefs/deals/{dealId} -> get all by Deal Id treaty-transactions-reliefs.
     */
    @RequestMapping(value = "/treaty-transaction-reliefs/deals/{dealId}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TreatyTransactionRelief>> getAllByDealId(@PathVariable Long dealId, Pageable pageable)
      throws URISyntaxException {
      log.debug("REST request to get TreatyTransactionRelief : {}", dealId);
      Page<TreatyTransactionRelief> page = treatyTransactionReliefRepository.findAllByDealIdAndIsActive(dealId,1,pageable);
      HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treaty-transaction-reliefs/deals");
      return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

/**
     * GET  /treaty-transactions-reliefs/deals/{dealId} -> get all by Deal Id treaty-transactions-reliefs.
     */
    @RequestMapping(value = "/treaty-transaction-reliefs/ordered-by-latest-subtransaction/deals/{dealId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTransactionRelief>> getAllByDealIdOrderedBySubtrans(@PathVariable Long dealId)
    throws URISyntaxException {
    log.debug("REST request to get TreatyTransactionRelief : {}", dealId);
    List<TreatySubtransactionRelief> subtransactions = treatySubtransactionReliefRepository.findAllByDealIdAndIsActive(dealId, 1, new Sort(Sort.Direction.DESC, "id"));

    List<TreatyTransactionRelief> treatyTransactionRelief = new ArrayList<>();

    for (TreatySubtransactionRelief sub : subtransactions) {
      TreatyTransactionRelief current = sub.getTreatyTransactionRelief();
      if (!treatyTransactionRelief.contains(current))
        treatyTransactionRelief.add(current);
    }

    return new ResponseEntity<>(treatyTransactionRelief, null, HttpStatus.OK);
  }


  /**
   * GET  /treaty-transactions-reliefs/deals/{dealId} -> get all by Deal Id treaty-transactions-reliefs.
   */
  @RequestMapping(value = "/treaty-transaction-reliefs/ordered-by-transactions/deals/{dealId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public List<TreatyTransactionRelief> getAllByDealIdOrderedByTrans(@PathVariable Long dealId)
    throws URISyntaxException {
    log.debug("REST request to get TreatyTransactionRelief : {}", dealId);

    List<TreatyTransactionRelief> treatyTransactionRelief = new ArrayList<>();
    treatyTransactionRelief=  treatyTransactionReliefRepository.findAllByDealIdAndIsActiveOrderByIdDesc(dealId,1);

    return treatyTransactionRelief;
  }




  /**
   * GET  /treaty-transactions-reliefs/deals/{dealId} -> get all by Deal Id treaty-transactions-reliefs.
   */
  @RequestMapping(value = "/treaty-transaction-reliefs-count/deals/{dealId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Count> getCountByDealId(@PathVariable Long dealId)
    throws URISyntaxException {
    log.debug("REST request to get TreatyTransactionRelief : {}", dealId);
    Long count = treatyTransactionReliefRepository.countByDealIdAndIsActive(dealId,1);
    return new ResponseEntity<>(new Count(count), HttpStatus.OK);
  }

  /**
   * GET  /treaty-transaction-reliefs/deals/ -> get all by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "/treaty-transaction-reliefs/deals/{dealId}/dates/{startYear}/{startMonth}/{startDay}/{endYear}/{endMonth}/{endDay}",
  method = RequestMethod.GET,
  produces = MediaType.APPLICATION_JSON_VALUE)
@Timed
public ResponseEntity<List<TreatyTransactionRelief>> getAllByReportingDateDates(
  @PathVariable Long dealId,
  @PathVariable Integer startYear,
  @PathVariable Integer startMonth,
  @PathVariable Integer startDay,
  @PathVariable Integer endYear,
  @PathVariable Integer endMonth,
  @PathVariable Integer endDay,
  Pageable pageable)
 throws URISyntaxException {
  Page<TreatyTransactionRelief> page = treatyTransactionReliefRepository.findAllByDealIdAndIsActiveAndReportingDateBetween(
    dealId,1,
    LocalDate.of(startYear.intValue(),startMonth.intValue(),startDay.intValue()),
    LocalDate.of(endYear.intValue(),endMonth.intValue(),endDay.intValue()),
    pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treaty-transaction-reliefs/deals");
  return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
}

  /**
   * GET  /treaty-transaction-reliefs/deals/ -> get all by Treaty Id treaty-trackings.
   */
    @RequestMapping(value = "/treaty-transaction-reliefs/ordered-by-id-desc/deals/{dealId}/dates/{startYear}/{startMonth}/{startDay}/{endYear}/{endMonth}/{endDay}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TreatyTransactionRelief>> getAllByReportingDateDatesOrderedByIdDesc(
      @PathVariable Long dealId,
      @PathVariable Integer startYear,
      @PathVariable Integer startMonth,
      @PathVariable Integer startDay,
      @PathVariable Integer endYear,
      @PathVariable Integer endMonth,
      @PathVariable Integer endDay,
      Pageable pageable)
     throws URISyntaxException {
      List<TreatySubtransactionRelief> subtransactions = treatySubtransactionReliefRepository.findAllByDealIdAndIsActiveAndReportingDateBetween(
        dealId,1,
        LocalDate.of(startYear.intValue(),startMonth.intValue(),startDay.intValue()),
        LocalDate.of(endYear.intValue(),endMonth.intValue(),endDay.intValue()),
        new Sort(Sort.Direction.DESC, "id"));

        List<TreatyTransactionRelief> treatyTransactionRelief = new ArrayList<>();

            for (TreatySubtransactionRelief sub : subtransactions) {
              TreatyTransactionRelief current = sub.getTreatyTransactionRelief();
              if (!treatyTransactionRelief.contains(current))
                treatyTransactionRelief.add(current);
            }

      return new ResponseEntity<>(treatyTransactionRelief, null, HttpStatus.OK);
   }



  /**
     * DELETE  /treaty-transaction-reliefs/:id : delete the "id" treatyTransactionRelief.
     *
     * @param id the id of the treatyTransactionRelief to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/treaty-transaction-reliefs/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTreatyTransactionRelief(@PathVariable Long id) {
        log.debug("REST request to delete TreatyTransactionRelief : {}", id);
        treatyTransactionReliefRepository.delete(id);
        treatyTransactionReliefSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("treatyTransactionRelief", id.toString())).build();
    }

    /**
     * SEARCH  /_search/treaty-transaction-reliefs?query=:query : search for the treatyTransactionRelief corresponding
     * to the query.
     *
     * @param query the query of the treatyTransactionRelief search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/treaty-transaction-reliefs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TreatyTransactionRelief>> searchTreatyTransactionReliefs(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of TreatyTransactionReliefs for query {}", query);
        Page<TreatyTransactionRelief> page = treatyTransactionReliefSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/treaty-transaction-reliefs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
