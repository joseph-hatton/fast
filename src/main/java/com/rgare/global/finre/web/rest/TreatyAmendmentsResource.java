package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.Treaty;
import com.rgare.global.finre.domain.TreatyAmendments;
import com.rgare.global.finre.repository.TreatyAmendmentsRepository;
import com.rgare.global.finre.repository.search.TreatyAmendmentsSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TreatyAmendments.
 */
@RestController
@RequestMapping("/api")
public class TreatyAmendmentsResource {

    private final Logger log = LoggerFactory.getLogger(TreatyAmendmentsResource.class);

    private static final String ENTITY_NAME = "treatyAmendments";

    private TreatyAmendmentsRepository treatyAmendmentsRepository;

    private TreatyAmendmentsSearchRepository treatyAmendmentsSearchRepository;

    public TreatyAmendmentsResource(TreatyAmendmentsRepository treatyAmendmentsRepository, TreatyAmendmentsSearchRepository treatyAmendmentsSearchRepository) {
        this.treatyAmendmentsRepository = treatyAmendmentsRepository;
        this.treatyAmendmentsSearchRepository = treatyAmendmentsSearchRepository;
    }

    /**
     * POST  /treaty-amendments : Create a new treatyAmendments.
     *
     * @param treatyAmendments the treatyAmendments to create
     * @return the ResponseEntity with status 201 (Created) and with body the new treatyAmendments, or with status 400 (Bad Request) if the treatyAmendments has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaty-amendments",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyAmendments> createTreatyAmendments(@RequestBody TreatyAmendments treatyAmendments) throws URISyntaxException {
        log.debug("REST request to save TreatyAmendments : {}", treatyAmendments);
        if (treatyAmendments.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("treatyAmendments", "idexists", "A new treatyAmendments cannot already have an ID")).body(null);
        }
        TreatyAmendments result = treatyAmendmentsRepository.save(treatyAmendments);
        treatyAmendmentsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/treaty-amendments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("treatyAmendments", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /treaty-amendments : Updates an existing treatyAmendments.
     *
     * @param treatyAmendments the treatyAmendments to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated treatyAmendments,
     * or with status 400 (Bad Request) if the treatyAmendments is not valid,
     * or with status 500 (Internal Server Error) if the treatyAmendments couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaty-amendments",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyAmendments> updateTreatyAmendments(@RequestBody TreatyAmendments treatyAmendments) throws URISyntaxException {
        log.debug("REST request to update TreatyAmendments : {}", treatyAmendments);
        if (treatyAmendments.getId() == null) {
            return createTreatyAmendments(treatyAmendments);
        }
        TreatyAmendments result = treatyAmendmentsRepository.save(treatyAmendments);
        treatyAmendmentsSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("treatyAmendments", treatyAmendments.getId().toString()))
            .body(result);
    }

    /**
     * GET  /treaty-amendments : get all the treatyAmendments.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of treatyAmendments in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/treaty-amendments",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TreatyAmendments>> getAllTreatyAmendments(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TreatyAmendments");
        Page<TreatyAmendments> page = treatyAmendmentsRepository.findAllByIsActive(1,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treaty-amendments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

  /**
   * GET  /treaty-amendments : get all the treatyAmendments.
   *
   * @return the ResponseEntity with status 200 (OK) and the list of treatyAmendments in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/treaty-amendments/nopage",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyAmendments>> getAllAmendmentsNoPages()
    throws URISyntaxException {
    log.debug("REST request to get a All Deals");
    List<TreatyAmendments> page = treatyAmendmentsRepository.findAllByIsActive(1);
    return Optional.ofNullable(page)
      .map(result -> new ResponseEntity<>(
        result,
        HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  /**
   * GET  /treaty-amendments : get all the treatyAmendments.
   *
   * @return the ResponseEntity with status 200 (OK) and the list of treatyAmendments in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/treaty-amendments/nopage/all",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyAmendments>> getAllAmendments()
    throws URISyntaxException {
    log.debug("REST request to get a All");
    List<TreatyAmendments> page = treatyAmendmentsRepository.findAll();
    return Optional.ofNullable(page)
      .map(result -> new ResponseEntity<>(
        result,
        HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }




  /**
     * GET  /treaty-amendments/:id : get the "id" treatyAmendments.
     *
     * @param id the id of the treatyAmendments to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the treatyAmendments, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/treaty-amendments/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyAmendments> getTreatyAmendments(@PathVariable Long id) {
        log.debug("REST request to get TreatyAmendments : {}", id);
        TreatyAmendments treatyAmendments = treatyAmendmentsRepository.findOne(id);
        return Optional.ofNullable(treatyAmendments)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /treaty-amendments/treaties/{treatyId} -> get all by Treaty Id treaty-amendments.
     */
    @RequestMapping(value = "/treaty-amendments/treaties/{treatyId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TreatyAmendments>> getAllByTreatyId(@PathVariable Long treatyId, Sort sort)
        throws URISyntaxException {
        log.debug("REST request to get TreatyAmendments : {}", treatyId);
        List<TreatyAmendments> list = treatyAmendmentsRepository.findAllByTreatyIdAndIsActive(treatyId,1, sort);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    /**
     * DELETE  /treaty-amendments/:id : delete the "id" treatyAmendments.
     *
     * @param id the id of the treatyAmendments to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/treaty-amendments/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTreatyAmendments(@PathVariable Long id) {
        log.debug("REST request to delete TreatyAmendments : {}", id);
        treatyAmendmentsRepository.delete(id);
        treatyAmendmentsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("treatyAmendments", id.toString())).build();
    }

    /**
     * SEARCH  /_search/treaty-amendments?query=:query : search for the treatyAmendments corresponding
     * to the query.
     *
     * @param query the query of the treatyAmendments search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/treaty-amendments",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TreatyAmendments>> searchTreatyAmendments(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of TreatyAmendments for query {}", query);
        Page<TreatyAmendments> page = treatyAmendmentsSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/treaty-amendments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
