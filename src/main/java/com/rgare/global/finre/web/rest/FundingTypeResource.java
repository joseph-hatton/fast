package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.FundingType;
import com.rgare.global.finre.repository.FundingTypeRepository;
import com.rgare.global.finre.repository.search.FundingTypeSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing FundingType.
 */
@RestController
@RequestMapping("/api")
public class FundingTypeResource {

    private final Logger log = LoggerFactory.getLogger(FundingTypeResource.class);

    private static final String ENTITY_NAME = "fundingType";

    private FundingTypeRepository fundingTypeRepository;

    private FundingTypeSearchRepository fundingTypeSearchRepository;

    public FundingTypeResource(FundingTypeRepository fundingTypeRepository, FundingTypeSearchRepository fundingTypeSearchRepository) {
        this.fundingTypeRepository = fundingTypeRepository;
        this.fundingTypeSearchRepository = fundingTypeSearchRepository;
    }
    /**
     * POST  /funding-types : Create a new fundingType.
     *
     * @param fundingType the fundingType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new fundingType, or with status 400 (Bad Request) if the fundingType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/funding-types",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FundingType> createFundingType(@Valid @RequestBody FundingType fundingType) throws URISyntaxException {
        log.debug("REST request to save FundingType : {}", fundingType);
        if (fundingType.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("fundingType", "idexists", "A new fundingType cannot already have an ID")).body(null);
        }
        FundingType result = fundingTypeRepository.save(fundingType);
        fundingTypeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/funding-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("fundingType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /funding-types : Updates an existing fundingType.
     *
     * @param fundingType the fundingType to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated fundingType,
     * or with status 400 (Bad Request) if the fundingType is not valid,
     * or with status 500 (Internal Server Error) if the fundingType couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/funding-types",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FundingType> updateFundingType(@Valid @RequestBody FundingType fundingType) throws URISyntaxException {
        log.debug("REST request to update FundingType : {}", fundingType);
        if (fundingType.getId() == null) {
            return createFundingType(fundingType);
        }
        FundingType result = fundingTypeRepository.save(fundingType);
        fundingTypeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("fundingType", fundingType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /funding-types : get all the fundingTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of fundingTypes in body
     */
    @RequestMapping(value = "/funding-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<FundingType> getAllFundingTypes() {
        log.debug("REST request to get all FundingTypes");
        List<FundingType> fundingTypes = fundingTypeRepository.findAll();
        return fundingTypes;
    }

    /**
     * GET  /funding-types/:id : get the "id" fundingType.
     *
     * @param id the id of the fundingType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the fundingType, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/funding-types/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FundingType> getFundingType(@PathVariable Long id) {
        log.debug("REST request to get FundingType : {}", id);
        FundingType fundingType = fundingTypeRepository.findOne(id);
        return Optional.ofNullable(fundingType)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /funding-types/:id : delete the "id" fundingType.
     *
     * @param id the id of the fundingType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/funding-types/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteFundingType(@PathVariable Long id) {
        log.debug("REST request to delete FundingType : {}", id);
        fundingTypeRepository.delete(id);
        fundingTypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("fundingType", id.toString())).build();
    }

    /**
     * SEARCH  /_search/funding-types?query=:query : search for the fundingType corresponding
     * to the query.
     *
     * @param query the query of the fundingType search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/funding-types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<FundingType> searchFundingTypes(@RequestParam String query) {
        log.debug("REST request to search FundingTypes for query {}", query);
        return StreamSupport
            .stream(fundingTypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
