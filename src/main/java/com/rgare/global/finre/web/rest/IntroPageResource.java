package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.*;
import com.rgare.global.finre.repository.IntroPageRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing Treaty.
 */
@RestController
@RequestMapping("/api")
public class IntroPageResource {

    private final Logger log = LoggerFactory.getLogger(TreatyResource.class);

    private static final String ENTITY_NAME = "intro_page";

    private IntroPageRepository introPageRepository;

    public IntroPageResource(IntroPageRepository introPageRepository) {
        this.introPageRepository = introPageRepository;
    }
    /**
     * POST  /intro : Create a new introPage.
     *
     * @param introPage the introPage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new treaty, or with status 400 (Bad Request) if the treaty has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/intro",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<IntroPage> createIntroPage(@Valid @RequestBody IntroPage introPage) throws URISyntaxException {
        log.debug("REST request to save IntroPage : {}", introPage);
        if (introPage.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("introPage", "idexists", "A new introPage cannot already have an ID")).body(null);
        }
        IntroPage result = filterOutInactiveSteps(introPageRepository.save(introPage));

        return ResponseEntity.created(new URI("/api/treaties/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("introPage", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /intro : Updates an existing treaty.
     *
     * @param treaty the treaty to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated treaty,
     * or with status 400 (Bad Request) if the treaty is not valid,
     * or with status 500 (Internal Server Error) if the treaty couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/intro",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<IntroPage> updateIntroPage(@Valid @RequestBody IntroPage introPage) throws URISyntaxException {
        log.debug("REST request to update IntroPage : {}", introPage);
        if (introPage.getId() == null) {
            return createIntroPage(introPage);
        }

        IntroPage result = filterOutInactiveSteps(introPageRepository.save(introPage));
        
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("introPage", introPage.getId().toString()))
            .body(result);
    }


  /**
   * GET  /intro/nopage : get all the intros. No Pagination
   *
   * @return the ResponseEntity with status 200 (OK) and the list of treaties in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/intro",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<IntroPage>> getAllIntroPages() {
    log.debug("REST request to get all IntroPages");
    log.debug("IntroPageRepo result: {}", introPageRepository);
    List<IntroPage> pages = introPageRepository.findAllByIsActive(1);

    for (int i = 0; i < pages.size(); i++) {
        pages.set(i, filterOutInactiveSteps(pages.get(i)));
    }

    log.debug("IntroPages result: {}", pages);
    return new ResponseEntity<>(pages, null, HttpStatus.OK);
  }

  /**
   * GET  /intro/:id : get the intro for this id
   *
   * @return the ResponseEntity with status 200 (OK) and the list of treaties in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/intro/{id}",
  method = RequestMethod.GET,
  produces = MediaType.APPLICATION_JSON_VALUE)
@Timed
public ResponseEntity<IntroPage> getIntroPage(@PathVariable Long id) {
  log.debug("REST request to get IntroPage by id {}", id);
  
  IntroPage page = filterOutInactiveSteps(introPageRepository.findOneByIsActiveAndId(1, id));

  return new ResponseEntity<>(page, null, HttpStatus.OK);
}

  private IntroPage filterOutInactiveSteps(IntroPage page){
    List<IntroStep> steps = page.getSteps();
    for(int i = steps.size() - 1; i >= 0; i--){
        if(steps.get(i).getIsActive() == 0){
            steps.remove(i);
        }
    }
    page.setSteps(steps);
    
    return page;
  }
}
