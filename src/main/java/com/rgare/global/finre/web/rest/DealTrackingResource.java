package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.DealTracking;
import com.rgare.global.finre.domain.TreatyTransactionRelief;
import com.rgare.global.finre.repository.DealTrackingRepository;
import com.rgare.global.finre.repository.search.DealTrackingSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing DealTracking.
 */
@RestController
@RequestMapping("/api")
public class DealTrackingResource {

    private final Logger log = LoggerFactory.getLogger(DealTrackingResource.class);

    private static final String ENTITY_NAME = "dealTracking";

    private DealTrackingRepository dealTrackingRepository;

    private DealTrackingSearchRepository dealTrackingSearchRepository;

    public DealTrackingResource(DealTrackingRepository dealTrackingRepository, DealTrackingSearchRepository dealTrackingSearchRepository) {
        this.dealTrackingRepository = dealTrackingRepository;
        this.dealTrackingSearchRepository = dealTrackingSearchRepository;
    }

    /**
     * POST  /deal-trackings : Create a new dealTracking.
     *
     * @param dealTracking the dealTracking to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dealTracking, or with status 400 (Bad Request) if the dealTracking has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/deal-trackings",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DealTracking> createDealTracking(@RequestBody DealTracking dealTracking) throws URISyntaxException {
        log.debug("REST request to save DealTracking : {}", dealTracking);
        if (dealTracking.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dealTracking cannot already have an ID")).body(null);
        }
        DealTracking result = dealTrackingRepository.save(dealTracking);
        dealTrackingSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/deal-trackings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /deal-trackings : Updates an existing dealTracking.
     *
     * @param dealTracking the dealTracking to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dealTracking,
     * or with status 400 (Bad Request) if the dealTracking is not valid,
     * or with status 500 (Internal Server Error) if the dealTracking couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/deal-trackings",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DealTracking> updateDealTracking(@RequestBody DealTracking dealTracking) throws URISyntaxException {
        log.debug("REST request to update DealTracking : {}", dealTracking);
        if (dealTracking.getId() == null) {
            return createDealTracking(dealTracking);
        }
        DealTracking result = dealTrackingRepository.save(dealTracking);
        dealTrackingSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dealTracking.getId().toString()))
            .body(result);
    }

    /**
     * GET  /deal-trackings : get all the dealTrackings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dealTrackings in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/deal-trackings",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<DealTracking>> getAllDealTrackings(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of DealTrackings");
        Page<DealTracking> page = dealTrackingRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/deal-trackings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /deal-trackings/deal/{dealId} -> get all by Deal Id deal-trackings.
     */
    @RequestMapping(value = "/deal-trackings/deal/{dealId}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<DealTracking>> getAllByDealId(@PathVariable Long dealId, Pageable pageable)
      throws URISyntaxException {
      log.debug("REST request to get DealTracking : {}", dealId);
      Page<DealTracking> page = dealTrackingRepository.findAllByDealIdAndIsActive(dealId, pageable,1);
      HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/deal-trackings/deal");
      return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

  /**
   * GET  /deal-trackings/all -> get all deal-trackings.
   */
  @RequestMapping(value = "/deal-trackings/all",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<DealTracking>> getAll()
    throws URISyntaxException {
    List<DealTracking> page = dealTrackingRepository.findAllByIsActiveAndDealTrackingStatus(1, "Open");
    return new ResponseEntity<>(page, HttpStatus.OK);
  }

  /**
   * GET  /treaty-transaction-reliefs/deals/ -> get all by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "/deal-trackings/date-range/{dealId}/{startYear}/{startMonth}/{startDay}/{endYear}/{endMonth}/{endDay}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<DealTracking>> getAllByReportingDateDates(
    @PathVariable Long dealId,
    @PathVariable Integer startYear,
    @PathVariable Integer startMonth,
    @PathVariable Integer startDay,
    @PathVariable Integer endYear,
    @PathVariable Integer endMonth,
    @PathVariable Integer endDay,
    Pageable pageable)
    throws URISyntaxException {
    Page<DealTracking> page = dealTrackingRepository.findAllByDealIdAndIsActiveAndReportingPeriodEndDateBetweenOrderByIdDesc(
      dealId,1,
      LocalDate.of(startYear.intValue(),startMonth.intValue(),startDay.intValue()),
      LocalDate.of(endYear.intValue(),endMonth.intValue(),endDay.intValue()),
      pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/deal-trackings/date-range");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
  }




  /**
   * GET  /Deal-Tracking-reliefs/deals/ -> get all by Treaty Id treaty-trackings.
   */
  @RequestMapping(value = "/deal-trackings/tracking-treaty/{dealId}/{startYear}/{startMonth}/{startDay}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<DealTracking>> getAllByReportingDate(
    @PathVariable Long dealId,
    @PathVariable Integer startYear,
    @PathVariable Integer startMonth,
    @PathVariable Integer startDay)
    throws URISyntaxException {
    List<DealTracking> dates = dealTrackingRepository.findAllByDealIdAndReportingPeriodEndDateAndIsActive(
      dealId,
      LocalDate.of(startYear.intValue(),startMonth.intValue(),startDay.intValue()),1);
    return new ResponseEntity<>(dates, HttpStatus.OK);
  }

  /**
     * GET  /deal-trackings/:id : get the "id" dealTracking.
     *
     * @param id the id of the dealTracking to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dealTracking, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/deal-trackings/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DealTracking> getDealTracking(@PathVariable Long id) {
        log.debug("REST request to get DealTracking : {}", id);
        DealTracking dealTracking = dealTrackingRepository.findOne(id);
        return Optional.ofNullable(dealTracking)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /deal-trackings/:id : delete the "id" dealTracking.
     *
     * @param id the id of the dealTracking to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/deal-trackings/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteDealTracking(@PathVariable Long id) {
        log.debug("REST request to delete DealTracking : {}", id);
        dealTrackingRepository.delete(id);
        dealTrackingSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/deal-trackings?query=:query : search for the dealTracking corresponding
     * to the query.
     *
     * @param query the query of the dealTracking search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/deal-trackings",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<DealTracking>> searchDealTrackings(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of DealTrackings for query {}", query);
        Page<DealTracking> page = dealTrackingSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/deal-trackings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
