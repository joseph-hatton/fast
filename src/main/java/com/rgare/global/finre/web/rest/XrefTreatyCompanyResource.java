package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.Treaty;
import com.rgare.global.finre.domain.XrefTreatyCompany;
import com.rgare.global.finre.repository.XrefTreatyCompanyRepository;
import com.rgare.global.finre.repository.search.XrefTreatyCompanySearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing XrefTreatyCompany.
 */
@RestController
@RequestMapping("/api")
public class XrefTreatyCompanyResource {

    private final Logger log = LoggerFactory.getLogger(XrefTreatyCompanyResource.class);

    private static final String ENTITY_NAME = "xrefTreatyCompany";

    private XrefTreatyCompanyRepository xrefTreatyCompanyRepository;

    private XrefTreatyCompanySearchRepository xrefTreatyCompanySearchRepository;

    public XrefTreatyCompanyResource(XrefTreatyCompanyRepository xrefTreatyCompanyRepository, XrefTreatyCompanySearchRepository xrefTreatyCompanySearchRepository) {
        this.xrefTreatyCompanyRepository = xrefTreatyCompanyRepository;
        this.xrefTreatyCompanySearchRepository = xrefTreatyCompanySearchRepository;
    }

    /**
     * POST  /xref-treaty-companies : Create a new xrefTreatyCompany.
     *
     * @param xrefTreatyCompany the xrefTreatyCompany to create
     * @return the ResponseEntity with status 201 (Created) and with body the new xrefTreatyCompany, or with status 400 (Bad Request) if the xrefTreatyCompany has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/xref-treaty-companies",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<XrefTreatyCompany> createXrefTreatyCompany(@Valid @RequestBody XrefTreatyCompany xrefTreatyCompany) throws URISyntaxException {
        log.debug("REST request to save XrefTreatyCompany : {}", xrefTreatyCompany);
        if (xrefTreatyCompany.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("xrefTreatyCompany", "idexists", "A new xrefTreatyCompany cannot already have an ID")).body(null);
        }
        XrefTreatyCompany result = xrefTreatyCompanyRepository.save(xrefTreatyCompany);
        xrefTreatyCompanySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/xref-treaty-companies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("xrefTreatyCompany", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /xref-treaty-companies : Updates an existing xrefTreatyCompany.
     *
     * @param xrefTreatyCompany the xrefTreatyCompany to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated xrefTreatyCompany,
     * or with status 400 (Bad Request) if the xrefTreatyCompany is not valid,
     * or with status 500 (Internal Server Error) if the xrefTreatyCompany couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/xref-treaty-companies",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<XrefTreatyCompany> updateXrefTreatyCompany(@Valid @RequestBody XrefTreatyCompany xrefTreatyCompany) throws URISyntaxException {
        log.debug("REST request to update XrefTreatyCompany : {}", xrefTreatyCompany);
        if (xrefTreatyCompany.getId() == null) {
            return createXrefTreatyCompany(xrefTreatyCompany);
        }
        XrefTreatyCompany result = xrefTreatyCompanyRepository.save(xrefTreatyCompany);
        xrefTreatyCompanySearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("xrefTreatyCompany", xrefTreatyCompany.getId().toString()))
            .body(result);
    }

    /**
     * GET  /xref-treaty-companies : get all the xrefTreatyCompanies.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of xrefTreatyCompanies in body
     */
    @RequestMapping(value = "/xref-treaty-companies",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<XrefTreatyCompany> getAllXrefTreatyCompanies() {
        log.debug("REST request to get all XrefTreatyCompanies");
        List<XrefTreatyCompany> xrefTreatyCompanies = xrefTreatyCompanyRepository.findAll();
        return xrefTreatyCompanies;
    }

    /**
     * GET  /xref-treaty-companies/:id : get the "id" xrefTreatyCompany.
     *
     * @param id the id of the xrefTreatyCompany to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the xrefTreatyCompany, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/xref-treaty-companies/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<XrefTreatyCompany> getXrefTreatyCompany(@PathVariable Long id) {
        log.debug("REST request to get XrefTreatyCompany : {}", id);
        XrefTreatyCompany xrefTreatyCompany = xrefTreatyCompanyRepository.findOne(id);
        return Optional.ofNullable(xrefTreatyCompany)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /xref-treaty-companies/treaties -> get all xref treaty-company by treatyId.
     */
    @RequestMapping(value = "/xref-treaty-companies/treaties/{treatyId}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<XrefTreatyCompany>> getAllTreatiesByTreatyId(@PathVariable Long treatyId) {
      log.debug("REST request to get all XrefTreatyCompany by TreatyId");

      List<XrefTreatyCompany> xrefTreatyCompanies = xrefTreatyCompanyRepository.findAllByTreatyId(treatyId);
      return Optional.ofNullable(xrefTreatyCompanies)
        .map(result -> new ResponseEntity<>(
          result,
          HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /xref-treaty-companies/companies -> get all xref treaty-company by companyId.
     */
    @RequestMapping(value = "/xref-treaty-companies/companies/{companyId}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<XrefTreatyCompany>> getAllByCompanyId(@PathVariable Long companyId) {
      log.debug("REST request to get all XrefTreatyCompany by companyId");

      List<XrefTreatyCompany> xrefTreatyCompanies = xrefTreatyCompanyRepository.findAllByCompanyId(companyId);
      return Optional.ofNullable(xrefTreatyCompanies)
        .map(result -> new ResponseEntity<>(
          result,
          HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /xref-treaty-companies/companies -> get all xref treaty-company by companyId.
     */
    @RequestMapping(value = "/xref-treaty-companies/company-count/{companyId}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Integer getAllByCompanyCount(@PathVariable Long companyId) {
      log.debug("REST request to get all XrefTreatyCompany by companyId");

      List<XrefTreatyCompany> xrefTreatyCompanies = xrefTreatyCompanyRepository.findAllByCompanyId(companyId);
      if (xrefTreatyCompanies == null)
        return 0;
      return xrefTreatyCompanies.size();
    }

    /**
     * DELETE  /xref-treaty-companies/:id : delete the "id" xrefTreatyCompany.
     *
     * @param id the id of the xrefTreatyCompany to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/xref-treaty-companies/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteXrefTreatyCompany(@PathVariable Long id) {
        log.debug("REST request to delete XrefTreatyCompany : {}", id);
        xrefTreatyCompanyRepository.delete(id);
        xrefTreatyCompanySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("xrefTreatyCompany", id.toString())).build();
    }

  /**
     * SEARCH  /_search/xref-treaty-companies?query=:query : search for the xrefTreatyCompany corresponding
     * to the query.
     *
     * @param query the query of the xrefTreatyCompany search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/xref-treaty-companies",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<XrefTreatyCompany> searchXrefTreatyCompanies(@RequestParam String query) {
        log.debug("REST request to search XrefTreatyCompanies for query {}", query);
        return StreamSupport
            .stream(xrefTreatyCompanySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
