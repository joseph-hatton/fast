package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.AuditTransactions;
import com.rgare.global.finre.domain.AuditTreatyAction;
import com.rgare.global.finre.domain.TreatySubtransactionCash;
import com.rgare.global.finre.domain.TreatyTransactionCash;
import com.rgare.global.finre.repository.AuditTransactionsRepository;
import com.rgare.global.finre.repository.TreatyTransactionCashRepository;
import com.rgare.global.finre.repository.TreatySubtransactionCashRepository;
import com.rgare.global.finre.repository.search.TreatyTransactionCashSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing TreatyTransactionCash.
 */
@RestController
@RequestMapping("/api")
public class TreatyTransactionCashResource {

    private final Logger log = LoggerFactory.getLogger(TreatyTransactionCashResource.class);

    private static final String ENTITY_NAME = "treatyTransactionCash";

    private TreatyTransactionCashRepository treatyTransactionCashRepository;

    private TreatyTransactionCashSearchRepository treatyTransactionCashSearchRepository;

    private AuditTransactionsRepository auditTransactionsRepository;

    private TreatySubtransactionCashRepository treatySubtransactionCashRepository;


    public TreatyTransactionCashResource(TreatyTransactionCashRepository treatyTransactionCashRepository,
                                         TreatyTransactionCashSearchRepository treatyTransactionCashSearchRepository,
                                         AuditTransactionsRepository auditTransactionsRepository, TreatySubtransactionCashRepository treatySubtransactionCashRepository) {
        this.treatyTransactionCashRepository = treatyTransactionCashRepository;
        this.treatyTransactionCashSearchRepository = treatyTransactionCashSearchRepository;
        this.auditTransactionsRepository = auditTransactionsRepository;
      this.treatySubtransactionCashRepository = treatySubtransactionCashRepository;
    }

  /**
     * POST  /treaty-transaction-cash : Create a new TreatyTransactionCash.
     *
     * @param treatyTransactionCash the treatyTransactionCash to create
     * @return the ResponseEntity with status 201 (Created) and with body the new treatyTransactionCash, or with status 400 (Bad Request) if the treatyTransactionCash has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaty-transaction-cash",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyTransactionCash> createTreatyTransactionCash(@RequestBody TreatyTransactionCash treatyTransactionCash) throws URISyntaxException {
        log.debug("REST request to save TreatyTransactionCash : {}", treatyTransactionCash);
        if (treatyTransactionCash.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("treatyTransactionCash", "idexists", "A new treatyTransactionCash cannot already have an ID")).body(null);
        }
        TreatyTransactionCash result = treatyTransactionCashRepository.save(treatyTransactionCash);
      auditTransactionsRepository.save(getAuditTransactions(result, AuditTreatyAction.SAVE.name()));
        return ResponseEntity.created(new URI("/api/treaty-transaction-cash/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("treatyTransactionCash", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /treaty-transaction-cash : Updates an existing treatyTransactionCash.
     *
     * @param treatyTransactionCash the treatyTransactionCash to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated treatyTransactionCash,
     * or with status 400 (Bad Request) if the treatyTransactionCash is not valid,
     * or with status 500 (Internal Server Error) if the treatyTransactionCash couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaty-transaction-cash",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyTransactionCash> updatetreatyTransactionCash(@RequestBody TreatyTransactionCash treatyTransactionCash) throws URISyntaxException {
        log.debug("REST request to update TreatyTransactionCash : {}", treatyTransactionCash);
        if (treatyTransactionCash.getId() == null) {
            return createTreatyTransactionCash(treatyTransactionCash);
        }
        TreatyTransactionCash result = treatyTransactionCashRepository.save(treatyTransactionCash);
        auditTransactionsRepository.save(getAuditTransactions(result, AuditTreatyAction.UPDATE.name()));
          return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("treatyTransactionCash", treatyTransactionCash.getId().toString()))
            .body(result);
    }

    /**
     * GET  /treaty-transaction-cash : get all the treatyTransactionCashs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of treatyTransactionCashs in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/treaty-transaction-cash",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TreatyTransactionCash>> getAllTreatyTransactionCashs(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TreatyTransactionCash");
        Page<TreatyTransactionCash> page = treatyTransactionCashRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treaty-transaction-cash");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /treaty-transaction-cash/:id : get the "id" treatyTransactionCash.
     *
     * @param id the id of the treatyTransactionCash to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the treatyTransactionCash, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/treaty-transaction-cash/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatyTransactionCash> getTreatyTransactionCash(@PathVariable Long id) {
        log.debug("REST request to get TreatyTransactionCash : {}", id);
        TreatyTransactionCash treatyTransactionCash = treatyTransactionCashRepository.findOne(id);
        return Optional.ofNullable(treatyTransactionCash)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }




  /**
   * GET  /treaty-transaction-cash/treaties/{treatyId} -> get all by Treaty Id treaty-transactions.
   */
  @RequestMapping(value = "/treaty-transaction/Actual/treaties/{treatyId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public List<TreatyTransactionCash> getAllByTreatyIdAndActual(@PathVariable Long treatyId)
    throws URISyntaxException {
    log.debug("REST request to get TreatyTransactionCash : {}", treatyId);
    List<TreatyTransactionCash> transactionList = treatyTransactionCashRepository.findAllByTreatyIdAndIsActiveAndTransactionStatusId(treatyId,1, (long) 1);
    return transactionList;
  }


  /**
     * GET  /treaty-transaction-cash/treaties/{treatyId} -> get all by Treaty Id treaty-transactions.
     */
    @RequestMapping(value = "/treaty-transaction-cash/treaties/{treatyId}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TreatyTransactionCash> getAllByTreatyId(@PathVariable Long treatyId)
      throws URISyntaxException {
      log.debug("REST request to get TreatyTransactionCash : {}", treatyId);
      List<TreatyTransactionCash> transactionList = treatyTransactionCashRepository.findAllByTreatyIdAndIsActive(treatyId,1);
      return transactionList;
    }


  /**
   * GET  /treaty-transaction-cash/treaties/{treatyId} -> get all by Treaty Id treaty-transactions.
   */
  @RequestMapping(value = "/treaty-transaction-cash/deleted",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public List<TreatyTransactionCash> getAllByDeletedNotification()
    throws URISyntaxException {
    log.debug("REST request to get TreatyTransactionCash : {}");
    List<TreatyTransactionCash> DeletedtransactionList = treatyTransactionCashRepository.findAllByDeleteNotification(1);
    return DeletedtransactionList;
  }





    /**
     * GET  /treaty-subtransaction-cash/company/:treatyId/:currencyCode: find all by the "treatyId" and "currencyCode".
     */
    @RequestMapping(value = "/treaty-transaction-cash/currency/{treatyId}/{currencyCode}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TreatyTransactionCash> getAllByTreatyIdCurrencyCode(@PathVariable Long treatyId, @PathVariable String currencyCode) {
      log.debug("REST request to get all Sub Transaction Cash by treatyId & currencyCode");

      List<TreatyTransactionCash> treatyTransactionCashes = treatyTransactionCashRepository.findAllByTreatyIdAndCurrencyCodeAndIsActive(treatyId,currencyCode,1);
      return treatyTransactionCashes;
    }

  /**
   * GET  /treaty-Transaction-cash/:companyId : find all by the "companyId" by companyId.
   */
  @RequestMapping(value = "/treaty-Transaction-cash/subtransaction-count/{companyId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public Integer getAllByCompanyIdCount(@PathVariable Long companyId) {
    log.debug("REST request to get all Sub Transaction Cash by companyId");

    List<TreatyTransactionCash> treatyCashes = treatyTransactionCashRepository.findAllByCompanyIdAndIsActive(companyId,1);
    if (treatyCashes == null)
      return 0;
    return treatyCashes.size();
  }

  /**
   * GET  /treaty-Transaction-cash/company/:treatyId/:companyId : find all by the "treatyId" and "companyId".
   */
  @RequestMapping(value = "/treaty-transaction-cash/company/{treatyId}/{companyId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public List<TreatyTransactionCash> getAllByTreatyIdCompanyId(@PathVariable Long treatyId, @PathVariable Long companyId) {
    log.debug("REST request to get all Sub Transaction Cash by treatyId & companyId");

    List<TreatyTransactionCash> treatyCashes = treatyTransactionCashRepository.findAllByTreatyIdAndCompanyIdAndIsActive(treatyId,companyId,1);
    return treatyCashes;
  }


  /**
     * GET  /treaty-transaction-cash/mix/{mixId} -> get all by Cash transactions by Mix
     */
    @RequestMapping(value = "/treaty-transaction-cash/mix/{mixTransactions}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TreatyTransactionCash>> getAllByMixId(@PathVariable Integer mixTransactions)
      throws URISyntaxException {
      log.debug("REST request to get TreatyTransactionCash : {}", mixTransactions);
      List<TreatyTransactionCash> transactionCashes = treatyTransactionCashRepository.findAllByMixTransactionsAndIsActive(mixTransactions,1);
      return Optional.ofNullable(transactionCashes)
        .map(result -> new ResponseEntity<>(
          result,
          HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));    }

  /**
   * GET  /treaty-transaction-cash/mix/{mixId} -> get all by Cash transactions by Mix
   */
  @RequestMapping(value = "/treaty-transaction-cash/mixSpecific/{mixTransactions}/{treatyId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatyTransactionCash>> getAllByMixIdAndTreatyId(@PathVariable Integer mixTransactions, @PathVariable Long treatyId)
    throws URISyntaxException {
    log.debug("REST request to get TreatyTransactionCash : {}", mixTransactions, treatyId);

    List<TreatySubtransactionCash> subtransactions = treatySubtransactionCashRepository.findAllByTreatyIdAndIsActiveOrderByIdDesc(treatyId, 1);

    List<TreatyTransactionCash> transactionList = new ArrayList<>();

    for (TreatySubtransactionCash sub : subtransactions) {
      TreatyTransactionCash trans = sub.getTreatyTransactionCash();
      if (!transactionList.contains(trans)
      && trans.getMixTransactions() == mixTransactions
      && trans.getIsActive() == 1)
        transactionList.add(trans);
    }

    return Optional.ofNullable(transactionList)
      .map(result -> new ResponseEntity<>(
        result,
        HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));    }

  /**
     * DELETE  /treaty-transaction-cash/:id : delete the "id" treatyTransactionCash.
     *
     * @param id the id of the treatyTransactionCash to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/treaty-transaction-cash/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTreatyTransactionCash(@PathVariable Long id) {
        log.debug("REST request to delete TreatyTransactionCash : {}", id);
      treatyTransactionCashRepository.delete(id);
      treatyTransactionCashSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("treatyTransactionCash", id.toString())).build();
    }





    /**
     * SEARCH  /_search/treaty-transaction-cash?query=:query : search for the treatyTransactionCash corresponding
     * to the query.
     *
     * @param query the query of the treatyTransactionCash search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/treaty-transaction-cash",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TreatyTransactionCash>> searchTreatyTransactionCashs(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of TreatyTransactionCashs for query {}", query);
        Page<TreatyTransactionCash> page = treatyTransactionCashSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/treaty-transaction-cash");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    private AuditTransactions getAuditTransactions(@RequestBody TreatyTransactionCash treatyTransactionCash, String auditTreatyActionType) {
      AuditTransactions auditTransactions = new AuditTransactions();
      auditTransactions.setAction(auditTreatyActionType);
      auditTransactions.setTransactionId(treatyTransactionCash.getId());
      auditTransactions.setTreatyId(treatyTransactionCash.getTreaty().getId());
      auditTransactions.setCreationUser(treatyTransactionCash.getLastUser());
      auditTransactions.setName("TRANSACTION");
      auditTransactions.setComments(treatyTransactionCash.toString());
      return auditTransactions;
    }
}
