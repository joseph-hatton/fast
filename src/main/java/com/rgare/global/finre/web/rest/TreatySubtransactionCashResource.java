package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.TreatySubtransactionCash;
import com.rgare.global.finre.repository.TreatySubtransactionCashRepository;
import com.rgare.global.finre.repository.search.TreatySubtransactionCashSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TreatySubTransaction.
 */
@RestController
@RequestMapping("/api")
public class TreatySubtransactionCashResource {

    private final Logger log = LoggerFactory.getLogger(TreatySubtransactionCashResource.class);

    private static final String ENTITY_NAME = "treatySubtransactionCash";

    private TreatySubtransactionCashRepository treatySubtransactionCashRepository;

    private TreatySubtransactionCashSearchRepository treatySubtransactionCashSearchRepository;

    public TreatySubtransactionCashResource(TreatySubtransactionCashRepository treatySubtransactionCashRepository, TreatySubtransactionCashSearchRepository treatySubtransactionCashSearchRepository) {
        this.treatySubtransactionCashRepository = treatySubtransactionCashRepository;
        this.treatySubtransactionCashSearchRepository = treatySubtransactionCashSearchRepository;
    }
    /**
     * POST  /treaty-subtransaction-cash : Create a new treatySubTransaction.
     *
     * @param treatySubTransaction the treatySubTransaction to create
     * @return the ResponseEntity with status 201 (Created) and with body the new treatySubTransaction, or with status 400 (Bad Request) if the treatySubTransaction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaty-subtransaction-cash",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatySubtransactionCash> createTreatySubTransaction(@RequestBody TreatySubtransactionCash treatySubTransaction) throws URISyntaxException {
        log.debug("REST request to save TreatySubTransaction : {}", treatySubTransaction);
        if (treatySubTransaction.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("treatySubTransaction", "idexists", "A new treatySubTransaction cannot already have an ID")).body(null);
        }
        TreatySubtransactionCash result = treatySubtransactionCashRepository.save(treatySubTransaction);
      treatySubtransactionCashSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/treaty-subtransaction-cash/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("treatySubTransaction", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /treaty-subtransaction-cash : Updates an existing treatySubTransaction.
     *
     * @param treatySubTransaction the treatySubTransaction to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated treatySubTransaction,
     * or with status 400 (Bad Request) if the treatySubTransaction is not valid,
     * or with status 500 (Internal Server Error) if the treatySubTransaction couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaty-subtransaction-cash",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatySubtransactionCash> updateTreatySubTransaction(@RequestBody TreatySubtransactionCash treatySubTransaction) throws URISyntaxException {
        log.debug("REST request to update TreatySubTransaction : {}", treatySubTransaction);
        if (treatySubTransaction.getId() == null) {
            return createTreatySubTransaction(treatySubTransaction);
        }
        TreatySubtransactionCash result = treatySubtransactionCashRepository.save(treatySubTransaction);
      treatySubtransactionCashSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("treatySubTransaction", treatySubTransaction.getId().toString()))
            .body(result);
    }

    /**
     * GET  /treaty-subtransaction-cash : get all the treatySubTransactions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of treatySubTransactions in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/treaty-subtransaction-cash",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TreatySubtransactionCash>> getAllTreatySubTransactions(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TreatySubTransactions");
        Page<TreatySubtransactionCash> page = treatySubtransactionCashRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treaty-subtransaction-cash");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /treaty-subtransaction-cash/:id : get the "id" treatySubTransaction.
     *
     * @param id the id of the treatySubTransaction to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the treatySubTransaction, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/treaty-subtransaction-cash/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatySubtransactionCash> getTreatySubTransaction(@PathVariable Long id) {
        log.debug("REST request to get TreatySubTransaction : {}", id);
      TreatySubtransactionCash treatySubTransaction = treatySubtransactionCashRepository.findOne(id);
        return Optional.ofNullable(treatySubTransaction)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /treaty-subtransaction-cash/treaty-transactions/{treatyTransactionId} -> get all by TreatyTransactionId treaty-sub-transactions.
     */
    @RequestMapping(value = "/treaty-subtransaction-cash/treaty-transactions/{treatyTransactionId}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TreatySubtransactionCash>> getAllByTreatyTransactionId(@PathVariable Long treatyTransactionCashId, Pageable pageable)
      throws URISyntaxException {
      log.debug("REST request to get TreatySubTransactions : {}", treatyTransactionCashId);
      Page<TreatySubtransactionCash> page = treatySubtransactionCashRepository.findAllByTreatyTransactionCashId(treatyTransactionCashId,pageable);
      HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treaty-subtransaction-cash/treaty-transactions");
      return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

  /**
   * GET  /treaty-subtransaction-cash/treaty-transactions/{treatyTransactionId} -> get all by TreatyTransactionId treaty-sub-transactions.
   */
  @RequestMapping(value = "/treaty-subtransaction-cash/tracking/{trackingId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public List<TreatySubtransactionCash> getAllByTrackingId(@PathVariable Long trackingId)
    throws URISyntaxException {
    log.debug("REST request to get TreatySubTransactions : {}", trackingId);
    List<TreatySubtransactionCash> ListSubTransaction = treatySubtransactionCashRepository.findAllByTrackingIdAndIsActive(trackingId,1);
    return ListSubTransaction;
  }


  /**
   * GET  /treaty-subtransaction-cash/treaty-mdm/{mdm} -> get all by TreatyTransactionId treaty-sub-transactions.
   */
  @RequestMapping(value = "/treaty-subtransaction-cash/mdm/{mdmId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public List<TreatySubtransactionCash> getAllByMdmId(@PathVariable Long mdmId)
    throws URISyntaxException {
    log.debug("REST request to get TreatySubTransactions : {}", mdmId);
    List<TreatySubtransactionCash> ListSubTransaction = treatySubtransactionCashRepository.findAllByMdmIdAndIsActive(mdmId,1);
    return ListSubTransaction;
  }



  /**
     * DELETE  /treaty-subtransaction-cash/:id : delete the "id" treatySubTransaction.
     *
     * @param id the id of the treatySubTransaction to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/treaty-subtransaction-cash/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTreatySubTransaction(@PathVariable Long id) {
        log.debug("REST request to delete TreatySubTransaction : {}", id);
      treatySubtransactionCashRepository.delete(id);
        treatySubtransactionCashSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("treatySubTransaction", id.toString())).build();
    }




    /**
     * SEARCH  /_search/treaty-subtransaction-cash?query=:query : search for the treatySubTransaction corresponding
     * to the query.
     *
     * @param query the query of the treatySubTransaction search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/treaty-subtransaction-cash",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TreatySubtransactionCash>> searchTreatySubTransactions(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of TreatySubTransactions for query {}", query);
        Page<TreatySubtransactionCash> page = treatySubtransactionCashSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/treaty-subtransaction-cash");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
