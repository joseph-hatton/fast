package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.ProductStructure;
import com.rgare.global.finre.repository.ProductStructureRepository;
import com.rgare.global.finre.repository.search.ProductStructureSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ProductStructure.
 */
@RestController
@RequestMapping("/api")
public class ProductStructureResource {

    private final Logger log = LoggerFactory.getLogger(ProductStructureResource.class);

    private static final String ENTITY_NAME = "productStructure";

    private ProductStructureRepository productStructureRepository;

    private ProductStructureSearchRepository productStructureSearchRepository;

    public ProductStructureResource(ProductStructureRepository productStructureRepository, ProductStructureSearchRepository productStructureSearchRepository) {
        this.productStructureRepository = productStructureRepository;
        this.productStructureSearchRepository = productStructureSearchRepository;
    }

    /**
     * POST  /product-structures : Create a new productStructure.
     *
     * @param productStructure the productStructure to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productStructure, or with status 400 (Bad Request) if the productStructure has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/product-structures",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductStructure> createProductStructure(@RequestBody ProductStructure productStructure) throws URISyntaxException {
        log.debug("REST request to save ProductStructure : {}", productStructure);
        if (productStructure.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("productStructure", "idexists", "A new productStructure cannot already have an ID")).body(null);
        }
        ProductStructure result = productStructureRepository.save(productStructure);
        productStructureSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/product-structures/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("productStructure", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /product-structures : Updates an existing productStructure.
     *
     * @param productStructure the productStructure to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated productStructure,
     * or with status 400 (Bad Request) if the productStructure is not valid,
     * or with status 500 (Internal Server Error) if the productStructure couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/product-structures",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductStructure> updateProductStructure(@RequestBody ProductStructure productStructure) throws URISyntaxException {
        log.debug("REST request to update ProductStructure : {}", productStructure);
        if (productStructure.getId() == null) {
            return createProductStructure(productStructure);
        }
        ProductStructure result = productStructureRepository.save(productStructure);
        productStructureSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("productStructure", productStructure.getId().toString()))
            .body(result);
    }

    /**
     * GET  /product-structures : get all the productStructures.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of productStructures in body
     */
    @RequestMapping(value = "/product-structures",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ProductStructure> getAllProductStructures() {
        log.debug("REST request to get all ProductStructures");
        List<ProductStructure> productStructures = productStructureRepository.findAll();
        return productStructures;
    }

    /**
     * GET  /product-structures/:id : get the "id" productStructure.
     *
     * @param id the id of the productStructure to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the productStructure, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/product-structures/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductStructure> getProductStructure(@PathVariable Long id) {
        log.debug("REST request to get ProductStructure : {}", id);
        ProductStructure productStructure = productStructureRepository.findOne(id);
        return Optional.ofNullable(productStructure)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /product-structures/:id : delete the "id" productStructure.
     *
     * @param id the id of the productStructure to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/product-structures/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteProductStructure(@PathVariable Long id) {
        log.debug("REST request to delete ProductStructure : {}", id);
        productStructureRepository.delete(id);
        productStructureSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("productStructure", id.toString())).build();
    }

    /**
     * SEARCH  /_search/product-structures?query=:query : search for the productStructure corresponding
     * to the query.
     *
     * @param query the query of the productStructure search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/product-structures",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ProductStructure> searchProductStructures(@RequestParam String query) {
        log.debug("REST request to search ProductStructures for query {}", query);
        return StreamSupport
            .stream(productStructureSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
