package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.TreatySeriatim;
import com.rgare.global.finre.repository.TreatySeriatimRepository;
import com.rgare.global.finre.repository.search.TreatySeriatimSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TreatySeriatim.
 */
@RestController
@RequestMapping("/api")
public class TreatySeriatimResource {

    private final Logger log = LoggerFactory.getLogger(TreatySeriatimResource.class);

    private static final String ENTITY_NAME = "treatySeriatim";

    private TreatySeriatimRepository treatySeriatimRepository;

    private TreatySeriatimSearchRepository treatySeriatimSearchRepository;

    public TreatySeriatimResource(TreatySeriatimRepository treatySeriatimRepository, TreatySeriatimSearchRepository treatySeriatimSearchRepository) {
        this.treatySeriatimRepository = treatySeriatimRepository;
        this.treatySeriatimSearchRepository = treatySeriatimSearchRepository;
    }

    /**
     * POST  /treaty-seriatims : Create a new treatySeriatim.
     *
     * @param treatySeriatim the treatySeriatim to create
     * @return the ResponseEntity with status 201 (Created) and with body the new treatySeriatim, or with status 400 (Bad Request) if the treatySeriatim has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaty-seriatims",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatySeriatim> createTreatySeriatim(@Valid @RequestBody TreatySeriatim treatySeriatim) throws URISyntaxException {
        log.debug("REST request to save TreatySeriatim : {}", treatySeriatim);
        if (treatySeriatim.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("treatySeriatim", "idexists", "A new treatySeriatim cannot already have an ID")).body(null);
        }
        TreatySeriatim result = treatySeriatimRepository.save(treatySeriatim);
        treatySeriatimSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/treaty-seriatims/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("treatySeriatim", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /treaty-seriatims : Updates an existing treatySeriatim.
     *
     * @param treatySeriatim the treatySeriatim to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated treatySeriatim,
     * or with status 400 (Bad Request) if the treatySeriatim is not valid,
     * or with status 500 (Internal Server Error) if the treatySeriatim couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/treaty-seriatims",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatySeriatim> updateTreatySeriatim(@Valid @RequestBody TreatySeriatim treatySeriatim) throws URISyntaxException {
        log.debug("REST request to update TreatySeriatim : {}", treatySeriatim);
        if (treatySeriatim.getId() == null) {
            return createTreatySeriatim(treatySeriatim);
        }
        TreatySeriatim result = treatySeriatimRepository.save(treatySeriatim);
        treatySeriatimSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("treatySeriatim", treatySeriatim.getId().toString()))
            .body(result);
    }

    /**
     * GET  /treaty-seriatims : get all the treatySeriatims.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of treatySeriatims in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/treaty-seriatims",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TreatySeriatim>> getAllTreatySeriatims(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TreatySeriatims");
        Page<TreatySeriatim> page = treatySeriatimRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treaty-seriatims");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /treaty-seriatims/:id : get the "id" treatySeriatim.
     *
     * @param id the id of the treatySeriatim to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the treatySeriatim, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/treaty-seriatims/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TreatySeriatim> getTreatySeriatim(@PathVariable Long id) {
        log.debug("REST request to get TreatySeriatim : {}", id);
        TreatySeriatim treatySeriatim = treatySeriatimRepository.findOne(id);
        return Optional.ofNullable(treatySeriatim)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

  /**
   * GET  /treaty-seriatims/treaties/{treatyId} -> get all by Treaty Id treaty-seriatims.
   */
  @RequestMapping(value = "/treaty-seriatims/treaties/{treatyId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<TreatySeriatim>> getAllByTreatyId(@PathVariable Long treatyId, Pageable pageable)
    throws URISyntaxException {
    log.debug("REST request to get TreatySeriatim : {}", treatyId);
    Page<TreatySeriatim> page = treatySeriatimRepository.findAllByTreatyId(treatyId,pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treaty-seriatims/treaties");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
  }




  /**
     * DELETE  /treaty-seriatims/:id : delete the "id" treatySeriatim.
     *
     * @param id the id of the treatySeriatim to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/treaty-seriatims/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTreatySeriatim(@PathVariable Long id) {
        log.debug("REST request to delete TreatySeriatim : {}", id);
        treatySeriatimRepository.delete(id);
        treatySeriatimSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("treatySeriatim", id.toString())).build();
    }

    /**
     * SEARCH  /_search/treaty-seriatims?query=:query : search for the treatySeriatim corresponding
     * to the query.
     *
     * @param query the query of the treatySeriatim search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/treaty-seriatims",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TreatySeriatim>> searchTreatySeriatims(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of TreatySeriatims for query {}", query);
        Page<TreatySeriatim> page = treatySeriatimSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/treaty-seriatims");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
