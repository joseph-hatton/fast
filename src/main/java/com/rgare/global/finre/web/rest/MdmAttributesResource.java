package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.MdmAttributes;
import com.rgare.global.finre.domain.Treaty;
import com.rgare.global.finre.repository.MdmAttributesRepository;
import com.rgare.global.finre.repository.search.MdmAttributesSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MdmAttributes.
 */
@RestController
@RequestMapping("/api")
public class MdmAttributesResource {

    private final Logger log = LoggerFactory.getLogger(MdmAttributesResource.class);

    private static final String ENTITY_NAME = "mdmAttributes";

    private MdmAttributesRepository mdmAttributesRepository;

    private MdmAttributesSearchRepository mdmAttributesSearchRepository;

    public MdmAttributesResource(MdmAttributesRepository mdmAttributesRepository, MdmAttributesSearchRepository mdmAttributesSearchRepository) {
        this.mdmAttributesRepository = mdmAttributesRepository;
        this.mdmAttributesSearchRepository = mdmAttributesSearchRepository;
    }

    /**
     * POST  /mdm-attributes : Create a new mdmAttributes.
     *
     * @param mdmAttributes the mdmAttributes to create
     * @return the ResponseEntity with status 201 (Created) and with body the new mdmAttributes, or with status 400 (Bad Request) if the mdmAttributes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/mdm-attributes",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MdmAttributes> createMdmAttributes(@RequestBody MdmAttributes mdmAttributes) throws URISyntaxException {
        log.debug("REST request to save MdmAttributes : {}", mdmAttributes);
        if (mdmAttributes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("mdmAttributes", "idexists", "A new mdmAttributes cannot already have an ID")).body(null);
        }
        MdmAttributes result = mdmAttributesRepository.save(mdmAttributes);
        mdmAttributesSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/mdm-attributes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("mdmAttributes", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /mdm-attributes : Updates an existing mdmAttributes.
     *
     * @param mdmAttributes the mdmAttributes to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated mdmAttributes,
     * or with status 400 (Bad Request) if the mdmAttributes is not valid,
     * or with status 500 (Internal Server Error) if the mdmAttributes couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/mdm-attributes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MdmAttributes> updateMdmAttributes(@RequestBody MdmAttributes mdmAttributes) throws URISyntaxException {
        log.debug("REST request to update MdmAttributes : {}", mdmAttributes);
        if (mdmAttributes.getId() == null) {
            return createMdmAttributes(mdmAttributes);
        }
        MdmAttributes result = mdmAttributesRepository.save(mdmAttributes);
        mdmAttributesSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("mdmAttributes", mdmAttributes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /mdm-attributes : get all the mdmAttributes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of mdmAttributes in body
     */
    @RequestMapping(value = "/mdm-attributes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<MdmAttributes> getAllMdmAttributes() {
        log.debug("REST request to get all MdmAttributes");
        List<MdmAttributes> mdmAttributes = mdmAttributesRepository.findAll();
        return mdmAttributes;
    }

    /**
     * GET  /mdm-attributes/:id : get the "id" mdmAttributes.
     *
     * @param id the id of the mdmAttributes to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the mdmAttributes, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/mdm-attributes/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MdmAttributes> getMdmAttributes(@PathVariable Long id) {
        log.debug("REST request to get MdmAttributes : {}", id);
        MdmAttributes mdmAttributes = mdmAttributesRepository.findOne(id);
        return Optional.ofNullable(mdmAttributes)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }



  /**
   * GET  /MdmAttributes : get all the MdmAttributes.
   *
   * @return the ResponseEntity with status 200 (OK) and the list of MdmAttributes in body
   * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
   */
  @RequestMapping(value = "/mdm-attributes/enterpriseId/{enterpriseId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<MdmAttributes>> getAllMdmAttributes(@PathVariable Long enterpriseId)
    throws URISyntaxException {
    log.debug("REST request to get a list of MdmAttributes: {}", enterpriseId);
    List<MdmAttributes>  ListMdmAttributes= mdmAttributesRepository.findAllByEnterpriseId(enterpriseId);
    return new ResponseEntity<>(ListMdmAttributes, HttpStatus.OK);
  }

  /**
     * DELETE  /mdm-attributes/:id : delete the "id" mdmAttributes.
     *
     * @param id the id of the mdmAttributes to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/mdm-attributes/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteMdmAttributes(@PathVariable Long id) {
        log.debug("REST request to delete MdmAttributes : {}", id);
        mdmAttributesRepository.delete(id);
        mdmAttributesSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("mdmAttributes", id.toString())).build();
    }

    /**
     * SEARCH  /_search/mdm-attributes?query=:query : search for the mdmAttributes corresponding
     * to the query.
     *
     * @param query the query of the mdmAttributes search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/mdm-attributes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<MdmAttributes> searchMdmAttributes(@RequestParam String query) {
        log.debug("REST request to search MdmAttributes for query {}", query);
        return StreamSupport
            .stream(mdmAttributesSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
