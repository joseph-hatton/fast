package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.GfsAssignee;
import com.rgare.global.finre.repository.GfsAssigneeRepository;
import com.rgare.global.finre.repository.search.GfsAssigneeSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing GfsAssignee.
 */
@RestController
@RequestMapping("/api")
public class GfsAssigneeResource {

    private final Logger log = LoggerFactory.getLogger(GfsAssigneeResource.class);

    private static final String ENTITY_NAME = "gfsAssignee";

    private GfsAssigneeRepository gfsAssigneeRepository;

    private GfsAssigneeSearchRepository gfsAssigneeSearchRepository;

    public GfsAssigneeResource(GfsAssigneeRepository gfsAssigneeRepository, GfsAssigneeSearchRepository gfsAssigneeSearchRepository) {
        this.gfsAssigneeRepository = gfsAssigneeRepository;
        this.gfsAssigneeSearchRepository = gfsAssigneeSearchRepository;
    }

    /**
     * POST  /gfs-assignees : Create a new gfsAssignee.
     *
     * @param gfsAssignee the gfsAssignee to create
     * @return the ResponseEntity with status 201 (Created) and with body the new gfsAssignee, or with status 400 (Bad Request) if the gfsAssignee has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/gfs-assignees",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GfsAssignee> createGfsAssignee(@RequestBody GfsAssignee gfsAssignee) throws URISyntaxException {
        log.debug("REST request to save GfsAssignee : {}", gfsAssignee);
        if (gfsAssignee.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("gfsAssignee", "idexists", "A new gfsAssignee cannot already have an ID")).body(null);
        }
        GfsAssignee result = gfsAssigneeRepository.save(gfsAssignee);
        gfsAssigneeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/gfs-assignees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("gfsAssignee", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /gfs-assignees : Updates an existing gfsAssignee.
     *
     * @param gfsAssignee the gfsAssignee to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated gfsAssignee,
     * or with status 400 (Bad Request) if the gfsAssignee is not valid,
     * or with status 500 (Internal Server Error) if the gfsAssignee couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/gfs-assignees",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GfsAssignee> updateGfsAssignee(@RequestBody GfsAssignee gfsAssignee) throws URISyntaxException {
        log.debug("REST request to update GfsAssignee : {}", gfsAssignee);
        if (gfsAssignee.getId() == null) {
            return createGfsAssignee(gfsAssignee);
        }
        GfsAssignee result = gfsAssigneeRepository.save(gfsAssignee);
        gfsAssigneeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("gfsAssignee", gfsAssignee.getId().toString()))
            .body(result);
    }

    /**
     * GET  /gfs-assignees : get all the gfsAssignees.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of gfsAssignees in body
     */
    @RequestMapping(value = "/gfs-assignees",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<GfsAssignee> getAllGfsAssignees() {
        log.debug("REST request to get all GfsAssignees");
        List<GfsAssignee> gfsAssignees = gfsAssigneeRepository.allActiveGfsAssignee();
        return gfsAssignees;
    }

    /**
     * GET  /gfs-assignees/:id : get the "id" gfsAssignee.
     *
     * @param id the id of the gfsAssignee to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the gfsAssignee, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/gfs-assignees/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GfsAssignee> getGfsAssignee(@PathVariable Long id) {
        log.debug("REST request to get GfsAssignee : {}", id);
        GfsAssignee gfsAssignee = gfsAssigneeRepository.findOne(id);
        return Optional.ofNullable(gfsAssignee)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /gfs-assignees/:id : delete the "id" gfsAssignee.
     *
     * @param id the id of the gfsAssignee to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/gfs-assignees/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteGfsAssignee(@PathVariable Long id) {
        log.debug("REST request to delete GfsAssignee : {}", id);
        gfsAssigneeRepository.delete(id);
        gfsAssigneeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("gfsAssignee", id.toString())).build();
    }

    /**
     * SEARCH  /_search/gfs-assignees?query=:query : search for the gfsAssignee corresponding
     * to the query.
     *
     * @param query the query of the gfsAssignee search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/gfs-assignees",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<GfsAssignee> searchGfsAssignees(@RequestParam String query) {
        log.debug("REST request to search GfsAssignees for query {}", query);
        return StreamSupport
            .stream(gfsAssigneeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
