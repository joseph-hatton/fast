package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.TransactionStatus;
import com.rgare.global.finre.repository.TransactionStatusRepository;
import com.rgare.global.finre.repository.search.TransactionStatusSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TransactionStatus.
 */
@RestController
@RequestMapping("/api")
public class TransactionStatusResource {

    private final Logger log = LoggerFactory.getLogger(TransactionStatusResource.class);

    private static final String ENTITY_NAME = "transactionStatus";

    private TransactionStatusRepository transactionStatusRepository;

    private TransactionStatusSearchRepository transactionStatusSearchRepository;

    public TransactionStatusResource(TransactionStatusRepository transactionStatusRepository, TransactionStatusSearchRepository transactionStatusSearchRepository) {
        this.transactionStatusRepository = transactionStatusRepository;
        this.transactionStatusSearchRepository = transactionStatusSearchRepository;
    }

    /**
     * POST  /transaction-statuses : Create a new transactionStatus.
     *
     * @param transactionStatus the transactionStatus to create
     * @return the ResponseEntity with status 201 (Created) and with body the new transactionStatus, or with status 400 (Bad Request) if the transactionStatus has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/transaction-statuses",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TransactionStatus> createTransactionStatus(@Valid @RequestBody TransactionStatus transactionStatus) throws URISyntaxException {
        log.debug("REST request to save TransactionStatus : {}", transactionStatus);
        if (transactionStatus.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("transactionStatus", "idexists", "A new transactionStatus cannot already have an ID")).body(null);
        }
        TransactionStatus result = transactionStatusRepository.save(transactionStatus);
        transactionStatusSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/transaction-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("transactionStatus", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /transaction-statuses : Updates an existing transactionStatus.
     *
     * @param transactionStatus the transactionStatus to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated transactionStatus,
     * or with status 400 (Bad Request) if the transactionStatus is not valid,
     * or with status 500 (Internal Server Error) if the transactionStatus couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/transaction-statuses",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TransactionStatus> updateTransactionStatus(@Valid @RequestBody TransactionStatus transactionStatus) throws URISyntaxException {
        log.debug("REST request to update TransactionStatus : {}", transactionStatus);
        if (transactionStatus.getId() == null) {
            return createTransactionStatus(transactionStatus);
        }
        TransactionStatus result = transactionStatusRepository.save(transactionStatus);
        transactionStatusSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("transactionStatus", transactionStatus.getId().toString()))
            .body(result);
    }

    /**
     * GET  /transaction-statuses : get all the transactionStatuses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of transactionStatuses in body
     */
    @RequestMapping(value = "/transaction-statuses",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TransactionStatus> getAllTransactionStatuses() {
        log.debug("REST request to get all TransactionStatuses");
        List<TransactionStatus> transactionStatuses = transactionStatusRepository.findAll();
        return transactionStatuses;
    }

    /**
     * GET  /transaction-statuses/:id : get the "id" transactionStatus.
     *
     * @param id the id of the transactionStatus to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the transactionStatus, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/transaction-statuses/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TransactionStatus> getTransactionStatus(@PathVariable Long id) {
        log.debug("REST request to get TransactionStatus : {}", id);
        TransactionStatus transactionStatus = transactionStatusRepository.findOne(id);
        return Optional.ofNullable(transactionStatus)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /transaction-statuses/:id : delete the "id" transactionStatus.
     *
     * @param id the id of the transactionStatus to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/transaction-statuses/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTransactionStatus(@PathVariable Long id) {
        log.debug("REST request to delete TransactionStatus : {}", id);
        transactionStatusRepository.delete(id);
        transactionStatusSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("transactionStatus", id.toString())).build();
    }

    /**
     * SEARCH  /_search/transaction-statuses?query=:query : search for the transactionStatus corresponding
     * to the query.
     *
     * @param query the query of the transactionStatus search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/transaction-statuses",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TransactionStatus> searchTransactionStatuses(@RequestParam String query) {
        log.debug("REST request to search TransactionStatuses for query {}", query);
        return StreamSupport
            .stream(transactionStatusSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
