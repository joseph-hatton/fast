package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.service.dto.UploadDTO;
import com.rgare.global.finre.web.rest.vm.ManagedUserVM;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * REST controller for managing EucUse.
 */
@RestController
@RequestMapping("/api")
public class FileUploadResource {

    private final Logger log = LoggerFactory.getLogger(FileUploadResource.class);

    private static final String ENTITY_NAME = "";

    /**
     * POST  /file-upload : Create a new file-upload.
     *
     * @param file the eucUse to create
     * @return the ResponseEntity with status 201 (Created) and with body the new eucUse, or with status 400 (Bad Request) if the eucUse has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping(value = "/file-upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Timed
    public ResponseEntity<?> uploadFile(@RequestParam(value = "file") String file) {
      if (file == null) {
        return new ResponseEntity("please select a file!", HttpStatus.OK);
      }

      try {
        byte[] decodedBytes = Base64.getDecoder().decode(file);
        saveUploadedFiles(decodedBytes);

      } catch (IOException e) {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
      }

      return new ResponseEntity("Successfully uploaded XSLX file ", new HttpHeaders(), HttpStatus.OK);

    }

  private void saveUploadedFiles(byte[] content)  throws IOException  {
    String UPLOADED_FOLDER = "C://temp//";
    Path path = Paths.get(UPLOADED_FOLDER + "test1.png");
    Files.write(path, content);
  }
}
