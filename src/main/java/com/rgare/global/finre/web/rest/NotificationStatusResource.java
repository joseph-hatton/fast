package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.NotificationStatus;
import com.rgare.global.finre.repository.NotificationStatusRepository;
import com.rgare.global.finre.repository.search.NotificationStatusSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NotificationStatus.
 */
@RestController
@RequestMapping("/api")
public class NotificationStatusResource {

    private final Logger log = LoggerFactory.getLogger(NotificationStatusResource.class);

    private static final String ENTITY_NAME = "notificationStatus";

    private NotificationStatusRepository notificationStatusRepository;

    private NotificationStatusSearchRepository notificationStatusSearchRepository;

    public NotificationStatusResource(NotificationStatusRepository notificationStatusRepository, NotificationStatusSearchRepository notificationStatusSearchRepository) {
        this.notificationStatusRepository = notificationStatusRepository;
        this.notificationStatusSearchRepository = notificationStatusSearchRepository;
    }

    /**
     * POST  /notification-statuses : Create a new notificationStatus.
     *
     * @param notificationStatus the notificationStatus to create
     * @return the ResponseEntity with status 201 (Created) and with body the new notificationStatus, or with status 400 (Bad Request) if the notificationStatus has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/notification-statuses",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<NotificationStatus> createNotificationStatus(@RequestBody NotificationStatus notificationStatus) throws URISyntaxException {
        log.debug("REST request to save NotificationStatus : {}", notificationStatus);
        if (notificationStatus.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("notificationStatus", "idexists", "A new notificationStatus cannot already have an ID")).body(null);
        }
        NotificationStatus result = notificationStatusRepository.save(notificationStatus);
        notificationStatusSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/notification-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("notificationStatus", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /notification-statuses : Updates an existing notificationStatus.
     *
     * @param notificationStatus the notificationStatus to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated notificationStatus,
     * or with status 400 (Bad Request) if the notificationStatus is not valid,
     * or with status 500 (Internal Server Error) if the notificationStatus couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/notification-statuses",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<NotificationStatus> updateNotificationStatus(@RequestBody NotificationStatus notificationStatus) throws URISyntaxException {
        log.debug("REST request to update NotificationStatus : {}", notificationStatus);
        if (notificationStatus.getId() == null) {
            return createNotificationStatus(notificationStatus);
        }
        NotificationStatus result = notificationStatusRepository.save(notificationStatus);
        notificationStatusSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("notificationStatus", notificationStatus.getId().toString()))
            .body(result);
    }

    /**
     * GET  /notification-statuses : get all the notificationStatuses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of notificationStatuses in body
     */
    @RequestMapping(value = "/notification-statuses",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<NotificationStatus> getAllNotificationStatuses() {
        log.debug("REST request to get all NotificationStatuses");
        List<NotificationStatus> notificationStatuses = notificationStatusRepository.findAll();
        return notificationStatuses;
    }

    /**
     * GET  /notification-statuses/:id : get the "id" notificationStatus.
     *
     * @param id the id of the notificationStatus to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the notificationStatus, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/notification-statuses/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<NotificationStatus> getNotificationStatus(@PathVariable Long id) {
        log.debug("REST request to get NotificationStatus : {}", id);
        NotificationStatus notificationStatus = notificationStatusRepository.findOne(id);
        return Optional.ofNullable(notificationStatus)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /notification-statuses/:id : delete the "id" notificationStatus.
     *
     * @param id the id of the notificationStatus to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/notification-statuses/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteNotificationStatus(@PathVariable Long id) {
        log.debug("REST request to delete NotificationStatus : {}", id);
        notificationStatusRepository.delete(id);
        notificationStatusSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("notificationStatus", id.toString())).build();
    }

    /**
     * SEARCH  /_search/notification-statuses?query=:query : search for the notificationStatus corresponding
     * to the query.
     *
     * @param query the query of the notificationStatus search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/notification-statuses",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<NotificationStatus> searchNotificationStatuses(@RequestParam String query) {
        log.debug("REST request to search NotificationStatuses for query {}", query);
        return StreamSupport
            .stream(notificationStatusSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
