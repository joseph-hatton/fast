package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.ProductSubtype;
import com.rgare.global.finre.repository.ProductSubtypeRepository;
import com.rgare.global.finre.repository.search.ProductSubtypeSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ProductSubtype.
 */
@RestController
@RequestMapping("/api")
public class ProductSubtypeResource {

    private final Logger log = LoggerFactory.getLogger(ProductSubtypeResource.class);

    private static final String ENTITY_NAME = "productSubtype";

    private ProductSubtypeRepository productSubtypeRepository;

    private ProductSubtypeSearchRepository productSubtypeSearchRepository;

    public ProductSubtypeResource(ProductSubtypeRepository productSubtypeRepository, ProductSubtypeSearchRepository productSubtypeSearchRepository) {
        this.productSubtypeRepository = productSubtypeRepository;
        this.productSubtypeSearchRepository = productSubtypeSearchRepository;
    }

    /**
     * POST  /product-subtypes : Create a new productSubtype.
     *
     * @param productSubtype the productSubtype to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productSubtype, or with status 400 (Bad Request) if the productSubtype has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/product-subtypes",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductSubtype> createProductSubtype(@Valid @RequestBody ProductSubtype productSubtype) throws URISyntaxException {
        log.debug("REST request to save ProductSubtype : {}", productSubtype);
        if (productSubtype.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("productSubtype", "idexists", "A new productSubtype cannot already have an ID")).body(null);
        }
        ProductSubtype result = productSubtypeRepository.save(productSubtype);
        productSubtypeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/product-subtypes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("productSubtype", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /product-subtypes : Updates an existing productSubtype.
     *
     * @param productSubtype the productSubtype to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated productSubtype,
     * or with status 400 (Bad Request) if the productSubtype is not valid,
     * or with status 500 (Internal Server Error) if the productSubtype couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/product-subtypes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductSubtype> updateProductSubtype(@Valid @RequestBody ProductSubtype productSubtype) throws URISyntaxException {
        log.debug("REST request to update ProductSubtype : {}", productSubtype);
        if (productSubtype.getId() == null) {
            return createProductSubtype(productSubtype);
        }
        ProductSubtype result = productSubtypeRepository.save(productSubtype);
        productSubtypeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("productSubtype", productSubtype.getId().toString()))
            .body(result);
    }

    /**
     * GET  /product-subtypes : get all the productSubtypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of productSubtypes in body
     */
    @RequestMapping(value = "/product-subtypes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ProductSubtype> getAllProductSubtypes() {
        log.debug("REST request to get all ProductSubtypes");
        List<ProductSubtype> productSubtypes = productSubtypeRepository.findAll();
        return productSubtypes;
    }

    /**
     * GET  /product-subtypes/:id : get the "id" productSubtype.
     *
     * @param id the id of the productSubtype to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the productSubtype, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/product-subtypes/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductSubtype> getProductSubtype(@PathVariable Long id) {
        log.debug("REST request to get ProductSubtype : {}", id);
        ProductSubtype productSubtype = productSubtypeRepository.findOne(id);
        return Optional.ofNullable(productSubtype)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /product-subtypes/:id : delete the "id" productSubtype.
     *
     * @param id the id of the productSubtype to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/product-subtypes/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteProductSubtype(@PathVariable Long id) {
        log.debug("REST request to delete ProductSubtype : {}", id);
        productSubtypeRepository.delete(id);
        productSubtypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("productSubtype", id.toString())).build();
    }

    /**
     * SEARCH  /_search/product-subtypes?query=:query : search for the productSubtype corresponding
     * to the query.
     *
     * @param query the query of the productSubtype search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/product-subtypes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ProductSubtype> searchProductSubtypes(@RequestParam String query) {
        log.debug("REST request to search ProductSubtypes for query {}", query);
        return StreamSupport
            .stream(productSubtypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
