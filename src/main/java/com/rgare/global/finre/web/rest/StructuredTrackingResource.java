package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.StructuredTracking;
import com.rgare.global.finre.repository.StructuredTrackingRepository;
import com.rgare.global.finre.repository.search.StructuredTrackingSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import com.rgare.global.finre.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing StructuredTracking.
 */
@RestController
@RequestMapping("/api")
public class StructuredTrackingResource {

    private final Logger log = LoggerFactory.getLogger(StructuredTrackingResource.class);

    private static final String ENTITY_NAME = "structuredTracking";

    private StructuredTrackingRepository structuredTrackingRepository;

    private StructuredTrackingSearchRepository structuredTrackingSearchRepository;

    public StructuredTrackingResource(StructuredTrackingRepository structuredTrackingRepository, StructuredTrackingSearchRepository structuredTrackingSearchRepository) {
        this.structuredTrackingRepository = structuredTrackingRepository;
        this.structuredTrackingSearchRepository = structuredTrackingSearchRepository;
    }

    /**
     * POST  /structured-trackings : Create a new structuredTracking.
     *
     * @param structuredTracking the structuredTracking to create
     * @return the ResponseEntity with status 201 (Created) and with body the new structuredTracking, or with status 400 (Bad Request) if the structuredTracking has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/structured-trackings",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<StructuredTracking> createStructuredTracking(@RequestBody StructuredTracking structuredTracking) throws URISyntaxException {
        log.debug("REST request to save StructuredTracking : {}", structuredTracking);
        if (structuredTracking.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("structuredTracking", "idexists", "A new structuredTracking cannot already have an ID")).body(null);
        }
        StructuredTracking result = structuredTrackingRepository.save(structuredTracking);
        structuredTrackingSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/structured-trackings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("structuredTracking", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /structured-trackings : Updates an existing structuredTracking.
     *
     * @param structuredTracking the structuredTracking to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated structuredTracking,
     * or with status 400 (Bad Request) if the structuredTracking is not valid,
     * or with status 500 (Internal Server Error) if the structuredTracking couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/structured-trackings",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<StructuredTracking> updateStructuredTracking(@RequestBody StructuredTracking structuredTracking) throws URISyntaxException {
        log.debug("REST request to update StructuredTracking : {}", structuredTracking);
        if (structuredTracking.getId() == null) {
            return createStructuredTracking(structuredTracking);
        }
        StructuredTracking result = structuredTrackingRepository.save(structuredTracking);
        structuredTrackingSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("structuredTracking", structuredTracking.getId().toString()))
            .body(result);
    }

    /**
     * GET  /structured-trackings : get all the structuredTrackings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of structuredTrackings in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/structured-trackings",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<StructuredTracking>> getAllStructuredTrackings(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of StructuredTrackings");
        Page<StructuredTracking> page = structuredTrackingRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/structured-trackings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
   * GET  /structured-trackings/:id : get the "id" structuredTracking.
   *
   * @param id the id of the structuredTracking to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the structuredTracking, or with status 404 (Not Found)
   */
  @RequestMapping(value = "/structured-trackings/{id}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<StructuredTracking> getStructuredTracking(@PathVariable Long id) {
    log.debug("REST request to get StructuredTracking : {}", id);
    StructuredTracking structuredTracking = structuredTrackingRepository.findOne(id);
    return Optional.ofNullable(structuredTracking)
      .map(result -> new ResponseEntity<>(
        result,
        HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  /**
   * GET  /structured-trackings/:id : get the "id" structuredTracking.
   *
   * @param dealId the id of the structuredTracking to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the structuredTracking, or with status 404 (Not Found)
   */
  @RequestMapping(value = "/structured-tracking-by-deal/{dealId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<StructuredTracking>> getStructuredTrackingByDealId(@PathVariable Long dealId,Pageable pageable)throws URISyntaxException {
      log.debug("REST request to get StructuredTracking : {}", dealId);
      Page<StructuredTracking> page = structuredTrackingRepository.findAllByDealIdAndIsActive(dealId,pageable,1);
      HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/structured-trackings");
      return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);

  }


  /**
   * GET  /structured-trackings/:id : get the "id" structuredTracking.
   *
   * @param financeID the id of the structuredTracking to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the structuredTracking, or with status 404 (Not Found)
   */
  @RequestMapping(value = "/structured-tracking-by-structured-FinanceID/{financeID}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<StructuredTracking>> getStructuredTrackingByDealIdAndFinanceID(@PathVariable Long financeID,Pageable pageable)throws URISyntaxException {
    log.debug("REST request to get StructuredTracking By FinanceID : {}", financeID);
    Page<StructuredTracking> page = structuredTrackingRepository.findAllByStructuredFinanceIdAndIsActive(financeID,pageable,1);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/structured-trackings");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);

  }

  /**
   * GET  /structured-trackings/:id : get the "id" structuredTracking.
   *
   * @param financeID the id of the structuredTracking to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the structuredTracking, or with status 404 (Not Found)
   */
  @RequestMapping(value = "/structured-tracking-by-Dates/{financeID}/dates/{startYear}/{startMonth}/{startDay}/{endYear}/{endMonth}/{endDay}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<StructuredTracking>> getStructuredTrackingByDate(@PathVariable Long financeID,
                                                                              @PathVariable Integer startYear,
                                                                              @PathVariable Integer startMonth,
                                                                              @PathVariable Integer startDay,
                                                                              @PathVariable Integer endYear,
                                                                              @PathVariable Integer endMonth,
                                                                              @PathVariable Integer endDay,
                                                                              Pageable pageable)throws URISyntaxException {
    log.debug("REST request to get StructuredTracking By Finance ID and Dates : {}", financeID);
    Page<StructuredTracking> page = structuredTrackingRepository.findAllByStructuredFinanceIdAndClientReportingDateBetweenAndIsActive(financeID,

      LocalDate.of(startYear.intValue(),startMonth.intValue(),startDay.intValue()),
      LocalDate.of(endYear.intValue(),endMonth.intValue(),endDay.intValue()),
      pageable,1);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/structured-trackings");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);

  }
    /**
     * DELETE  /structured-trackings/:id : delete the "id" structuredTracking.
     *
     * @param id the id of the structuredTracking to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/structured-trackings/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteStructuredTracking(@PathVariable Long id) {
        log.debug("REST request to delete StructuredTracking : {}", id);
        structuredTrackingRepository.delete(id);
        structuredTrackingSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("structuredTracking", id.toString())).build();
    }

    /**
     * SEARCH  /_search/structured-trackings?query=:query : search for the structuredTracking corresponding
     * to the query.
     *
     * @param query the query of the structuredTracking search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/structured-trackings",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<StructuredTracking>> searchStructuredTrackings(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of StructuredTrackings for query {}", query);
        Page<StructuredTracking> page = structuredTrackingSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/structured-trackings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
