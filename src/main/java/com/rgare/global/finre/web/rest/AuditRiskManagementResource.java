package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.AuditRiskManagement;

import com.rgare.global.finre.repository.AuditRiskManagementRepository;
import com.rgare.global.finre.repository.search.AuditRiskManagementSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AuditRiskManagement.
 */
@RestController
@RequestMapping("/api")
public class AuditRiskManagementResource {

    private final Logger log = LoggerFactory.getLogger(AuditRiskManagementResource.class);

    private static final String ENTITY_NAME = "auditRiskManagement";
        
    private final AuditRiskManagementRepository auditRiskManagementRepository;

    private final AuditRiskManagementSearchRepository auditRiskManagementSearchRepository;

    public AuditRiskManagementResource(AuditRiskManagementRepository auditRiskManagementRepository, AuditRiskManagementSearchRepository auditRiskManagementSearchRepository) {
        this.auditRiskManagementRepository = auditRiskManagementRepository;
        this.auditRiskManagementSearchRepository = auditRiskManagementSearchRepository;
    }

    /**
     * POST  /audit-risk-managements : Create a new auditRiskManagement.
     *
     * @param auditRiskManagement the auditRiskManagement to create
     * @return the ResponseEntity with status 201 (Created) and with body the new auditRiskManagement, or with status 400 (Bad Request) if the auditRiskManagement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/audit-risk-managements")
    @Timed
    public ResponseEntity<AuditRiskManagement> createAuditRiskManagement(@RequestBody AuditRiskManagement auditRiskManagement) throws URISyntaxException {
        log.debug("REST request to save AuditRiskManagement : {}", auditRiskManagement);
        if (auditRiskManagement.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new auditRiskManagement cannot already have an ID")).body(null);
        }
        AuditRiskManagement result = auditRiskManagementRepository.save(auditRiskManagement);
        auditRiskManagementSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/audit-risk-managements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /audit-risk-managements : Updates an existing auditRiskManagement.
     *
     * @param auditRiskManagement the auditRiskManagement to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated auditRiskManagement,
     * or with status 400 (Bad Request) if the auditRiskManagement is not valid,
     * or with status 500 (Internal Server Error) if the auditRiskManagement couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/audit-risk-managements")
    @Timed
    public ResponseEntity<AuditRiskManagement> updateAuditRiskManagement(@RequestBody AuditRiskManagement auditRiskManagement) throws URISyntaxException {
        log.debug("REST request to update AuditRiskManagement : {}", auditRiskManagement);
        if (auditRiskManagement.getId() == null) {
            return createAuditRiskManagement(auditRiskManagement);
        }
        AuditRiskManagement result = auditRiskManagementRepository.save(auditRiskManagement);
        auditRiskManagementSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, auditRiskManagement.getId().toString()))
            .body(result);
    }

    /**
     * GET  /audit-risk-managements : get all the auditRiskManagements.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of auditRiskManagements in body
     */
    @GetMapping("/audit-risk-managements")
    @Timed
    public List<AuditRiskManagement> getAllAuditRiskManagements() {
        log.debug("REST request to get all AuditRiskManagements");
        List<AuditRiskManagement> auditRiskManagements = auditRiskManagementRepository.findAll();
        return auditRiskManagements;
    }

    /**
     * GET  /audit-risk-managements/:id : get the "id" auditRiskManagement.
     *
     * @param id the id of the auditRiskManagement to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the auditRiskManagement, or with status 404 (Not Found)
     */
    @GetMapping("/audit-risk-managements/{id}")
    @Timed
    public ResponseEntity<AuditRiskManagement> getAuditRiskManagement(@PathVariable Long id) {
        log.debug("REST request to get AuditRiskManagement : {}", id);
        AuditRiskManagement auditRiskManagement = auditRiskManagementRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(auditRiskManagement));
    }

    /**
     * DELETE  /audit-risk-managements/:id : delete the "id" auditRiskManagement.
     *
     * @param id the id of the auditRiskManagement to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/audit-risk-managements/{id}")
    @Timed
    public ResponseEntity<Void> deleteAuditRiskManagement(@PathVariable Long id) {
        log.debug("REST request to delete AuditRiskManagement : {}", id);
        auditRiskManagementRepository.delete(id);
        auditRiskManagementSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/audit-risk-managements?query=:query : search for the auditRiskManagement corresponding
     * to the query.
     *
     * @param query the query of the auditRiskManagement search 
     * @return the result of the search
     */
    @GetMapping("/_search/audit-risk-managements")
    @Timed
    public List<AuditRiskManagement> searchAuditRiskManagements(@RequestParam String query) {
        log.debug("REST request to search AuditRiskManagements for query {}", query);
        return StreamSupport
            .stream(auditRiskManagementSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }


}
