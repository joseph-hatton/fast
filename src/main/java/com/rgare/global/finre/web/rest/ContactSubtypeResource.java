package com.rgare.global.finre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.rgare.global.finre.domain.ContactSubtype;
import com.rgare.global.finre.repository.ContactSubtypeRepository;
import com.rgare.global.finre.repository.search.ContactSubtypeSearchRepository;
import com.rgare.global.finre.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ContactSubtype.
 */
@RestController
@RequestMapping("/api")
public class ContactSubtypeResource {

    private final Logger log = LoggerFactory.getLogger(ContactSubtypeResource.class);

    private static final String ENTITY_NAME = "contactSubtype";

    private ContactSubtypeRepository contactSubtypeRepository;

    private ContactSubtypeSearchRepository contactSubtypeSearchRepository;

    public ContactSubtypeResource(ContactSubtypeRepository contactSubtypeRepository, ContactSubtypeSearchRepository contactSubtypeSearchRepository) {
        this.contactSubtypeRepository = contactSubtypeRepository;
        this.contactSubtypeSearchRepository = contactSubtypeSearchRepository;
    }

    /**
     * POST  /contact-subtypes : Create a new contactSubtype.
     *
     * @param contactSubtype the contactSubtype to create
     * @return the ResponseEntity with status 201 (Created) and with body the new contactSubtype, or with status 400 (Bad Request) if the contactSubtype has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/contact-subtypes",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ContactSubtype> createContactSubtype(@Valid @RequestBody ContactSubtype contactSubtype) throws URISyntaxException {
        log.debug("REST request to save ContactSubtype : {}", contactSubtype);
        if (contactSubtype.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new contactSubtype cannot already have an ID")).body(null);
        }
        ContactSubtype result = contactSubtypeRepository.save(contactSubtype);
        contactSubtypeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/contact-subtypes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /contact-subtypes : Updates an existing contactSubtype.
     *
     * @param contactSubtype the contactSubtype to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated contactSubtype,
     * or with status 400 (Bad Request) if the contactSubtype is not valid,
     * or with status 500 (Internal Server Error) if the contactSubtype couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/contact-subtypes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ContactSubtype> updateContactSubtype(@Valid @RequestBody ContactSubtype contactSubtype) throws URISyntaxException {
        log.debug("REST request to update ContactSubtype : {}", contactSubtype);
        if (contactSubtype.getId() == null) {
            return createContactSubtype(contactSubtype);
        }
        ContactSubtype result = contactSubtypeRepository.save(contactSubtype);
        contactSubtypeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, contactSubtype.getId().toString()))
            .body(result);
    }

    /**
     * GET  /contact-subtypes : get all the contactSubtypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of contactSubtypes in body
     */
    @RequestMapping(value = "/contact-subtypes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ContactSubtype> getAllContactSubtypes() {
        log.debug("REST request to get all ContactSubtypes");
        List<ContactSubtype> contactSubtypes = contactSubtypeRepository.findAll();
        return contactSubtypes;
    }

    /**
     * GET  /contact-subtypes/:id : get the "id" contactSubtype.
     *
     * @param id the id of the contactSubtype to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the contactSubtype, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/contact-subtypes/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ContactSubtype> getContactSubtype(@PathVariable Long id) {
        log.debug("REST request to get ContactSubtype : {}", id);
        ContactSubtype contactSubtype = contactSubtypeRepository.findOne(id);
        return Optional.ofNullable(contactSubtype)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /contact-subtypes/:id : delete the "id" contactSubtype.
     *
     * @param id the id of the contactSubtype to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/contact-subtypes/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteContactSubtype(@PathVariable Long id) {
        log.debug("REST request to delete ContactSubtype : {}", id);
        contactSubtypeRepository.delete(id);
        contactSubtypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/contact-subtypes?query=:query : search for the contactSubtype corresponding
     * to the query.
     *
     * @param query the query of the contactSubtype search
     * @return the result of the search
     */
    @RequestMapping(value = "/_search/contact-subtypes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ContactSubtype> searchContactSubtypes(@RequestParam String query) {
        log.debug("REST request to search ContactSubtypes for query {}", query);
        return StreamSupport
            .stream(contactSubtypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
