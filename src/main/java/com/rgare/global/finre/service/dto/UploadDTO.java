package com.rgare.global.finre.service.dto;

import com.rgare.global.finre.config.Constants;
import com.rgare.global.finre.domain.Authority;
import com.rgare.global.finre.domain.User;
import org.hibernate.validator.constraints.Email;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A DTO representing a user, with his authorities.
 */
public class UploadDTO {

  private String extraField;

  private MultipartFile file;

  public String getExtraField() {
    return extraField;
  }

  public void setExtraField(String extraField) {
    this.extraField = extraField;
  }

  public MultipartFile getFile() {
    return file;
  }

  public void setFile(MultipartFile file) {
    this.file = file;
  }

  @Override
    public String toString() {
        return "UserDTO{" +
            "extraField='" + extraField + '\'' +
            ", file='" +   file + '\'' +
            "}";
    }
}
