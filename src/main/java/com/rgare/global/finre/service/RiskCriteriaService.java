package com.rgare.global.finre.service;

import com.rgare.global.finre.domain.RiskCriteria;
import java.util.List;

/**
 * Service Interface for managing RiskCriteria.
 */
public interface RiskCriteriaService {

    /**
     * Save a riskCriteria.
     *
     * @param riskCriteria the entity to save
     * @return the persisted entity
     */
    RiskCriteria save(RiskCriteria riskCriteria);

    /**
     *  Get all the riskCriteria.
     *  
     *  @return the list of entities
     */
    List<RiskCriteria> findAll();

    /**
     *  Get the "id" riskCriteria.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    RiskCriteria findOne(Long id);

    /**
     *  Delete the "id" riskCriteria.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the riskCriteria corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @return the list of entities
     */
    List<RiskCriteria> search(String query);
}
