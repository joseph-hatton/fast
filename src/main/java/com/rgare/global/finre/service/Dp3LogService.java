package com.rgare.global.finre.service;

import com.rgare.global.finre.security.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.rgare.net.dp3.DP3LogInterface;
import com.rgare.net.dp3.pojo.InputPojoRequest;
import com.rgare.net.dp3.pojo.Optional;
import com.rgare.net.dp3.pojo.What;
import com.rgare.net.dp3.pojo.When;
import com.rgare.net.dp3.pojo.Where;
import com.rgare.net.dp3.pojo.Who;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by s0044277 on 11/8/2016.
 */
@Service
public class Dp3LogService {
    private final Logger log = LoggerFactory.getLogger(Dp3LogService.class);

    @Value("${server.app.dp3-env}")
    private String dp3Environment;

    @Value("${server.app.dp3-address}")
    private String dp3Address;

    private final SecurityUtils securityUtils;

    public Dp3LogService(SecurityUtils securityUtils) {
      this.securityUtils = securityUtils;
    }

    private static final DP3LogInterface dp3LogInterface = new DP3LogInterface();

    public int postDp3log(String action, String service, String details){

        int statusCode=0;
        InputPojoRequest inputPojoRequest = prepareRequest(action, service, details);
        When when = new When();
        when.setUTCEventDateTime("new Date().toUTCString()");
        inputPojoRequest.setWhen(when);

        dp3LogInterface.setEnv(dp3Environment);

        dp3LogInterface.setInputPojoRequest(inputPojoRequest);

        try {
            statusCode = dp3LogInterface.postDB3Logs();
        } catch (Exception e) {
          e.printStackTrace();
        }
        return statusCode;
    }

    private  InputPojoRequest prepareRequest(String action, String service, String details) {
      InputPojoRequest request = new InputPojoRequest();

      Where where = new Where();
      where.setAppIdentifier("FAST");
      where.setAppAddress(dp3Address);
      where.setService(service);
      request.setWhere(where);

      Who who = new Who();
      who.setUserInteraction("true");
      who.setUserId(getUserId());
      who.setSourceAddress(dp3Address);
      request.setWho(who);

      What what = new What();
      what.setSeverity("0");
      what.setType("0");
      what.setDescription(details);
      what.setCleanDescription(details);
      request.setWhat(what);

      Optional optional = new Optional();
      optional.setExtendedDetails("No additional information");
      optional.setUTCSecondaryDateTime("new Date().toUTCString()");
      request.setOptional(optional);
      return request;
    }

    private String getUserId() {
      return securityUtils.getCurrentUserLogin();
    }
}
