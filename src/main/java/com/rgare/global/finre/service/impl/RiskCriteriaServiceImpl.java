package com.rgare.global.finre.service.impl;

import com.rgare.global.finre.service.RiskCriteriaService;
import com.rgare.global.finre.domain.RiskCriteria;
import com.rgare.global.finre.repository.RiskCriteriaRepository;
import com.rgare.global.finre.repository.search.RiskCriteriaSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing RiskCriteria.
 */
@Service
@Transactional
public class RiskCriteriaServiceImpl implements RiskCriteriaService{

    private final Logger log = LoggerFactory.getLogger(RiskCriteriaServiceImpl.class);
    
    private final RiskCriteriaRepository riskCriteriaRepository;

    private final RiskCriteriaSearchRepository riskCriteriaSearchRepository;

    public RiskCriteriaServiceImpl(RiskCriteriaRepository riskCriteriaRepository, RiskCriteriaSearchRepository riskCriteriaSearchRepository) {
        this.riskCriteriaRepository = riskCriteriaRepository;
        this.riskCriteriaSearchRepository = riskCriteriaSearchRepository;
    }

    /**
     * Save a riskCriteria.
     *
     * @param riskCriteria the entity to save
     * @return the persisted entity
     */
    @Override
    public RiskCriteria save(RiskCriteria riskCriteria) {
        log.debug("Request to save RiskCriteria : {}", riskCriteria);
        RiskCriteria result = riskCriteriaRepository.save(riskCriteria);
        riskCriteriaSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the riskCriteria.
     *  
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<RiskCriteria> findAll() {
        log.debug("Request to get all RiskCriteria");
        List<RiskCriteria> result = riskCriteriaRepository.findAll();

        return result;
    }

    /**
     *  Get one riskCriteria by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public RiskCriteria findOne(Long id) {
        log.debug("Request to get RiskCriteria : {}", id);
        RiskCriteria riskCriteria = riskCriteriaRepository.findOne(id);
        return riskCriteria;
    }

    /**
     *  Delete the  riskCriteria by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RiskCriteria : {}", id);
        riskCriteriaRepository.delete(id);
        riskCriteriaSearchRepository.delete(id);
    }

    /**
     * Search for the riskCriteria corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<RiskCriteria> search(String query) {
        log.debug("Request to search RiskCriteria for query {}", query);
        return StreamSupport
            .stream(riskCriteriaSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
