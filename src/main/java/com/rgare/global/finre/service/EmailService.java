package com.rgare.global.finre.service;

import com.rgare.global.finre.domain.AuditFinanceEmail;
import com.rgare.global.finre.domain.TreatySubtransactionCash;
import com.rgare.global.finre.domain.TreatyTransactionCash;
import com.rgare.global.finre.repository.AuditFinanceEmailRepository;
import io.github.jhipster.config.JHipsterProperties;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;
import java.util.Base64;

import static java.time.ZonedDateTime.now;
import static net.logstash.logback.marker.Markers.append;

/**
 * Service for sending e-mails.
 * <p>
 * We use the @Async annotation to send e-mails asynchronously.
 * </p>
 */
@Service
public class EmailService {

  private final Logger log = LoggerFactory.getLogger(EmailService.class);

  private JHipsterProperties jHipsterProperties;

  private JavaMailSender javaMailSender;

  private MessageSource messageSource;

  private SpringTemplateEngine templateEngine;

  private AuditFinanceEmailRepository auditFinanceEmailRepository;

  private String financefastxls = "/finance-fast.xls";
  private String yyyyMM = "yyyy-MM";

  @Value("${server.app.email-address}")
  private String xlsxLocation;

  public EmailService(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender,
                      MessageSource messageSource, SpringTemplateEngine templateEngine, AuditFinanceEmailRepository auditFinanceEmailRepository) {
    this.jHipsterProperties = jHipsterProperties;
    this.javaMailSender = javaMailSender;
    this.messageSource = messageSource;
    this.templateEngine = templateEngine;
    this.auditFinanceEmailRepository = auditFinanceEmailRepository;
  }

  @Async
  public void sendEmailCashTransaction(TreatyTransactionCash treatyTransactionCash) {
    log.debug("Send e-mail[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
      treatyTransactionCash.toString());

    try {
      if (treatyTransactionCash.getContacts() == null) {
        throw new Exception("No Contacts");
      }

      MimeMessage message = javaMailSender.createMimeMessage();

      // Set From: header field of the header.
      message.setFrom(new InternetAddress(jHipsterProperties.getMail().getFrom()));
      message.addRecipients(Message.RecipientType.TO,
        InternetAddress.parse(treatyTransactionCash.getContacts()));

      if(treatyTransactionCash.getMixTransactions() != 1 ) {
        message.setSubject(treatyTransactionCash.getTreaty().getDeal().getName() + " - " + treatyTransactionCash.getTreaty().getAgreementType().getName());
      }else
      {
        message.setSubject(treatyTransactionCash.getSubject());
      }
      HSSFWorkbook workbook;
      // Create the message part
      BodyPart messageBodyPart = new MimeBodyPart();
      String html = generateHtmlData(treatyTransactionCash);
      messageBodyPart.setContent(html,"text/html; charset=utf-8");
      if(treatyTransactionCash.getCurrency().getCode().equals("USD") ) {
       workbook =  createSpreadsheetUSD(treatyTransactionCash);
      }else{
       workbook =  createSpreadsheet(treatyTransactionCash);
      }
      // Part two is attachment
      Multipart multipart = new MimeMultipart("mixed");
      multipart.addBodyPart(messageBodyPart);
      messageBodyPart = new MimeBodyPart();


      // Write the output to a temporary excel file
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      workbook.write(bos); // write excel data to a byte array
      bos.close();

      // Now use your ByteArrayDataSource as
      DataSource dataSource = new ByteArrayDataSource(bos.toByteArray(), "application/vnd.ms-excel");
      messageBodyPart.setDataHandler(new DataHandler(dataSource));
      messageBodyPart.setFileName("Transaction.xls");
      multipart.addBodyPart(messageBodyPart);

      // ADD THE EXTRA ATTACHMENT
      if (treatyTransactionCash.getFileContent() != null) {
        byte[] decodedBytes = Base64.getDecoder().decode(treatyTransactionCash.getFileContent());
        DataSource dataSource1 = new ByteArrayDataSource(decodedBytes, treatyTransactionCash.getFileType());
        messageBodyPart = new MimeBodyPart();
        messageBodyPart.setDataHandler(new DataHandler(dataSource1));
        messageBodyPart.setFileName(treatyTransactionCash.getFileName());
        multipart.addBodyPart(messageBodyPart);
      }

      message.setContent(multipart);
      // Send message
      javaMailSender.send(message);
      log.debug("Sent e-mail to User '{}'", message);

      for (TreatySubtransactionCash subtransaction :treatyTransactionCash.getSubTransactions()) {
        auditFinanceEmailRepository.save(getAuditFinanceEmail(subtransaction, treatyTransactionCash,"Send Successfully"));
      }

    } catch (Exception e) {
      log.error("E-mail could not be sent to user '{}', exception is: {}", e);
      for (TreatySubtransactionCash subtransaction :treatyTransactionCash.getSubTransactions()) {
        auditFinanceEmailRepository.save(getAuditFinanceEmail(subtransaction, treatyTransactionCash, "Error sending Email"));
      }
    }
  }



  private String generateHtmlData(TreatyTransactionCash treatyTransactionCash) {
    String tdOpen = "<td>";
    String tdOpenAlignRight = "<td align=\"right\">";
    String tdOpenAlignCenter = "<td align=\"center\">";
    String tdClosed = "</td>";

    String cashBound = null;
    DecimalFormat formatCurrencyAmount = new DecimalFormat("###,###,###,###.00");
    if (treatyTransactionCash.getFundingType().getName().contains("In")) {
      cashBound = "Inbound";
    } else if (treatyTransactionCash.getFundingType().getName().contains("Out")) {
      cashBound = "Outbound";
    } else {
      cashBound = treatyTransactionCash.getFundingType().getName();
    }
    String reviewLine = "<p>A " + cashBound + " Cash Transaction has occurred. The Excel file is attached. Please review for posting and approval.<br/><br/>" + treatyTransactionCash.getEmailComment() + "</p><br/>";

    StringBuilder sb = new StringBuilder();
    sb.append(reviewLine);
    sb.append("<table style='border: 4px solid #ddd;padding: 1px;'>")
      .append("<thead style='background-color: lightgrey;'>")
      .append("<tr>")
      .append("<th>Funding Type</th>")
      .append("<th>Settlement Date</th>")
      .append("<th>Header Description</th>")
      .append("<th>Reporting Period</th>")
      .append("<th>Business Unit</th>")
      .append("<th>Client Company</th>")
      .append("<th>Enterprise Key</th>")
      .append("<th>Base Currency</th>")
      .append("<th>Base Amount</th>")
      .append("</tr>")
      .append("</thead>");

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(yyyyMM);
    for (TreatySubtransactionCash subtransaction :treatyTransactionCash.getSubTransactions()) {
      sb.append("<tr>")
        .append(tdOpen).append(treatyTransactionCash.getFundingType().getName()).append(tdClosed)
        .append(tdOpen).append(subtransaction.getSettlementDate().format(DateTimeFormatter.ofPattern("MM-dd-yyyy"))).append(tdClosed)
        .append(tdOpen).append(subtransaction.getTreaty().getDeal().getName()).append(" - ").append(subtransaction.getTreaty().getAgreementType().getName()).append(tdClosed)
        .append(tdOpenAlignCenter).append(subtransaction.getReportingDate().format(formatter)).append(tdClosed)
        .append(tdOpenAlignCenter).append(subtransaction.getMdm().getBusinessUnitId()).append(tdClosed)
        .append(tdOpenAlignCenter).append(treatyTransactionCash.getCompany().getClientCompanyId()).append(tdClosed)
        .append(tdOpenAlignCenter).append(subtransaction.getMdm().getEnterpriseId()).append(tdClosed)
        .append(tdOpenAlignCenter).append(treatyTransactionCash.getCurrency().getCode()).append(tdClosed)
        .append(tdOpenAlignRight).append(formatCurrencyAmount.format(subtransaction.getAmount())).append(tdClosed)
        .append("</tr>");
    }

    sb.append("<tr>")
      .append(tdOpen).append("").append(tdClosed)
      .append(tdOpen).append("").append(tdClosed)
      .append(tdOpen).append("").append(tdClosed)
      .append(tdOpen).append("").append(tdClosed)
      .append(tdOpen).append("").append(tdClosed)
      .append(tdOpen).append("").append(tdClosed)
      .append(tdOpen).append("").append(tdClosed)
      .append(tdOpen).append("TOTAL WIRE AMOUNT").append(tdClosed)
      .append(tdOpenAlignRight).append(formatCurrencyAmount.format(treatyTransactionCash.getNetTotal())).append(tdClosed)
      .append("</tr>");

    sb.append("</table>");

    return sb.toString();
  }


  private HSSFWorkbook createSpreadsheetUSD(TreatyTransactionCash treatyTransactionCash) throws IOException {
    InputStream io = new URL(xlsxLocation+financefastxls).openStream();
    HSSFWorkbook workbook = new HSSFWorkbook(io);
    HSSFSheet sheet = workbook.getSheetAt(0);
    int count=1 ;
    int col = 3;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(yyyyMM);
    for (TreatySubtransactionCash subtransaction :treatyTransactionCash.getSubTransactions()) {
      Row row = sheet.createRow(col++);
      //Business Unit (B)
      Cell cell = row.createCell(1);
      cell.setCellValue(subtransaction.getMdm().getBusinessUnitId());
    // JOURNAL ID
      cell = row.createCell(2);
      cell.setCellValue("FRESUS");

      //Journal Date  (D)
      cell = row.createCell(3);
      cell.setCellValue(subtransaction.getSettlementDate().format(DateTimeFormatter.ofPattern("M/dd/yyyy")));

      //DEAL NAME - PERIOD
      cell = row.createCell(5);
      cell.setCellValue(subtransaction.getTreaty().getDeal().getName() +" - "+subtransaction.getReportingDate().format(formatter));


      //Business Unit (I)
      cell = row.createCell(7);
      cell.setCellValue(count);

      //Business Unit (I)
      cell = row.createCell(8);
      cell.setCellValue(subtransaction.getMdm().getBusinessUnitId());

      //Business Unit (I)
      cell = row.createCell(9);
      cell.setCellValue(10);

      //Client Company (K)
      cell = row.createCell(10);
      cell.setCellValue(treatyTransactionCash.getCompany().getClientCompanyId());

      //Enterprise Key (L)
      cell = row.createCell(11);
        cell.setCellValue("");
      //ACCOUNT ( p)
      cell = row.createCell(15);
      cell.setCellValue(144015);
      CellStyle style = workbook.createCellStyle();
      style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
      style.setFillPattern(CellStyle.SOLID_FOREGROUND);
      cell.setCellStyle(style);

      //ACCOUNT ( p)
      cell = row.createCell(16);
      cell.setCellStyle(style);

      //Line Description (R)
      cell = row.createCell(17);
      if(subtransaction.getTreaty().getDeal().getName().length()>30) {
        cell.setCellValue(subtransaction.getTreaty().getDeal().getName().substring(0, 30) +
          " - " + subtransaction.getReportingDate().format(formatter));
      }else{
        cell.setCellValue(subtransaction.getTreaty().getDeal().getName() +
          " - " + subtransaction.getReportingDate().format(formatter));
      }
      //Base Currency (S)
      cell = row.createCell(18);
      cell.setCellValue(treatyTransactionCash.getCurrency().getCode());

      //Base Amount (T)
      cell = row.createCell(19);
      cell.setCellValue(subtransaction.getAmount().doubleValue());

      //Currency (W)
      cell = row.createCell(22);
      cell.setCellValue(treatyTransactionCash.getCurrency().getCode());

      // Amount (X)
      cell = row.createCell(23);
      cell.setCellValue(subtransaction.getAmount().doubleValue());

      Row row2 = sheet.createRow(col);
      col=col+1;
      //Business Unit (B)
      cell = row2.createCell(1);
      cell.setCellValue(subtransaction.getMdm().getBusinessUnitId());
      // JOURNAL ID
      cell = row2.createCell(2);
      cell.setCellValue("FRESUS");

      //Journal Date  (D)
      cell = row2.createCell(3);
      cell.setCellValue(subtransaction.getSettlementDate().format(DateTimeFormatter.ofPattern("M/dd/yyyy")));

      //DEAL NAME - PERIOD
      cell = row2.createCell(5);
      cell.setCellValue(subtransaction.getTreaty().getDeal().getName() +" - "+subtransaction.getReportingDate().format(formatter));


      //Business Unit (I)
      cell = row2.createCell(7);
      cell.setCellValue(count+1);

      //Business Unit (I)
      cell = row2.createCell(8);
      cell.setCellValue(subtransaction.getMdm().getBusinessUnitId());

      //Business Unit (I)
      cell = row2.createCell(9);
      cell.setCellValue(10);

      //Client Company (K)
      cell = row2.createCell(10);
      cell.setCellValue(treatyTransactionCash.getCompany().getClientCompanyId());

      //Enterprise Key (L)
      cell = row2.createCell(11);
        cell.setCellValue(subtransaction.getMdm().getEnterpriseId());

      //ACCOUNT ( p)

      cell = row2.createCell(15);
      cell.setCellValue(349100);
      cell.setCellStyle(style); //yellow
      //ACCOUNT ( p)
      cell = row2.createCell(16);
      cell.setCellStyle(style);

      //Line Description (R)
      cell = row2.createCell(17);
      if(subtransaction.getTreaty().getDeal().getName().length()>30) {
        cell.setCellValue(subtransaction.getTreaty().getDeal().getName().substring(0, 30) +
          " - " + subtransaction.getReportingDate().format(formatter));
      }else{
        cell.setCellValue(subtransaction.getTreaty().getDeal().getName() +
          " - " + subtransaction.getReportingDate().format(formatter));
      }

      //Base Currency (S)
      cell = row2.createCell(18);
      cell.setCellValue(treatyTransactionCash.getCurrency().getCode());

      //Base Amount (T)
      cell = row2.createCell(19);
      cell.setCellValue(subtransaction.getAmount().doubleValue()*-1);

      //Currency (W)
      cell = row2.createCell(22);
      cell.setCellValue(treatyTransactionCash.getCurrency().getCode());

      // Amount (X)
      cell = row2.createCell(23);
      cell.setCellValue(subtransaction.getAmount().doubleValue()*-1);



      count= count+2;
    }

    return workbook;
  }


  private HSSFWorkbook createSpreadsheet(TreatyTransactionCash treatyTransactionCash) throws IOException {
    String url = xlsxLocation + financefastxls;
    log.debug(append("url", url), "Reading finance fast spreadsheet");
    InputStream io = new URL(xlsxLocation+financefastxls).openStream();
    HSSFWorkbook workbook = new HSSFWorkbook(io);

    log.debug(append("url", url).and(append("sheets", workbook.getNumberOfSheets())), "finance fast spreadsheet loaded");
    HSSFSheet sheet = workbook.getSheetAt(0);
    log.debug(append("url", url), "finance fast spreadsheet sheet 0 opened");

    int col = 3;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(yyyyMM);
    for (TreatySubtransactionCash subtransaction :treatyTransactionCash.getSubTransactions()) {
      Row row = sheet.createRow(col++);
      //Business Unit (B)
      Cell cell = row.createCell(1);
      cell.setCellValue(subtransaction.getMdm().getBusinessUnitId());

      //Journal Date  (D)
      cell = row.createCell(3);
      cell.setCellValue(subtransaction.getSettlementDate().format(DateTimeFormatter.ofPattern("MM-dd-yyyy")));

      //Treaty Name (F)
      cell = row.createCell(5);
      cell.setCellValue(subtransaction.getTreaty().getTreatyName() +" - "+subtransaction.getReportingDate().format(formatter));

      //Business Unit (I)
      cell = row.createCell(8);
      cell.setCellValue(subtransaction.getMdm().getBusinessUnitId());

      //Client Company (K)
      cell = row.createCell(10);
      cell.setCellValue(treatyTransactionCash.getCompany().getClientCompanyId());

      //Enterprise Key (L)
      cell = row.createCell(11);
      cell.setCellValue(subtransaction.getMdm().getEnterpriseId());

      //Line Description (R)
      cell = row.createCell(17);
      cell.setCellValue(subtransaction.getTreaty().getTreatyName() +
        " - "+subtransaction.getTreaty().getAgreementType().getName() +
        " - "+subtransaction.getReportingDate().format(formatter));

      //Base Currency (S)
      cell = row.createCell(18);
      cell.setCellValue(treatyTransactionCash.getCurrency().getCode());

      //Base Amount (T)
      cell = row.createCell(19);
      cell.setCellValue(subtransaction.getAmount().doubleValue());

      //Currency (W)
      cell = row.createCell(22);
      cell.setCellValue(treatyTransactionCash.getCurrency().getCode());

      // Amount (X)
      cell = row.createCell(23);
      cell.setCellValue(subtransaction.getAmount().doubleValue());
    }

    return workbook;
  }

  @Async
  public  AuditFinanceEmail getAuditFinanceEmail(TreatySubtransactionCash subtransaction, TreatyTransactionCash cashTransaction, String status) {
    // this is for a report AUDIT FINANCE - do not change
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(yyyyMM);
    String user= cashTransaction.getSender();
    String emailComment=cashTransaction.getEmailComment();
    String HeaderDescription=cashTransaction.getTreaty().getDeal().getName()+" "+subtransaction.getReportingDate().format(formatter);
    String lineDescription= cashTransaction.getTreaty().getDeal().getName()
      +" "+ cashTransaction.getTreaty().getAgreementType().getName()+" "+subtransaction.getReportingDate().format(formatter);
    AuditFinanceEmail auditFinanceEmail= new AuditFinanceEmail();
    auditFinanceEmail.setCreationUser(user);
    auditFinanceEmail.setCreationTs(now());
    auditFinanceEmail.setComments(" '"+user+"','"+cashTransaction.getSettlementDate()+"','"+subtransaction.getReportingDate().format(formatter)
      +"','"+cashTransaction.getFundingType().getName()+"','"+subtransaction.getMdm().getBusinessUnitId()+"','"+
      cashTransaction.getCompany().getClientCompanyId()+"','"+subtransaction.getMdm().getEnterpriseId()+"','"+
      cashTransaction.getCurrency().getCode()+"','"+subtransaction.getAmount()+"','"+cashTransaction.getTreaty().getId()+"','"+cashTransaction.getId()+"','"+subtransaction.getId()
      +"','"+cashTransaction.getContacts()+"','"+lineDescription+"','"+HeaderDescription+"','"+status+"','"+emailComment+"'");
    return auditFinanceEmail;
  }

  private BodyPart addAttachment(TreatyTransactionCash treatyTransactionCash) throws MessagingException {
    BodyPart messageBodyPart = null;
    try {

        //check to see if existing file attachment exist
        if (treatyTransactionCash.getFileContent() != null ) {
          messageBodyPart = new MimeBodyPart();
          byte[] decodedBytes = Base64.getDecoder().decode(treatyTransactionCash.getFileContent());
          DataSource dataSource = new ByteArrayDataSource(decodedBytes, treatyTransactionCash.getFileType());
          messageBodyPart.setFileName(treatyTransactionCash.getFileName());
          messageBodyPart.setDataHandler(new DataHandler(dataSource));
        }

        return messageBodyPart;

    } catch (Exception e) {
        throw e;
    }
  }


  @Async
  public void sendEmailTransactionWithAttachment(TreatyTransactionCash treatyTransactionCash) {
    log.debug("Send e-mail[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
      treatyTransactionCash.toString());

    try {
      if (treatyTransactionCash.getContacts() == null) {
        throw new Exception("No Contacts");
      }
      // Create a default MimeMessage object.
//            Session session = Session.getInstance(props);
//            Message message = new MimeMessage(session);
      MimeMessage message = javaMailSender.createMimeMessage();

      // Set From: header field of the header.
      message.setFrom(new InternetAddress(jHipsterProperties.getMail().getFrom()));
      message.addRecipients(Message.RecipientType.TO,
        InternetAddress.parse(treatyTransactionCash.getContacts()));
    if(treatyTransactionCash.getMixTransactions() != 1 ) {
      message.setSubject(treatyTransactionCash.getTreaty().getDeal().getName() + " - " + treatyTransactionCash.getTreaty().getAgreementType().getName());
    }else
    {
      message.setSubject(treatyTransactionCash.getSubject());
    }
      // Create the message part
      BodyPart messageBodyPart = new MimeBodyPart();
      String html = generateHtmlData(treatyTransactionCash);
      messageBodyPart.setContent(html,"text/html");
      DataSource dataSource = null;

      // Part two is attachment
      Multipart multipart = new MimeMultipart();
      multipart.addBodyPart(messageBodyPart);
      multipart.addBodyPart(messageBodyPart);
      messageBodyPart = new MimeBodyPart();

      //check to see if existing file attachment exist
      if (treatyTransactionCash.getFileContent() != null ) {
        byte[] decodedBytes = Base64.getDecoder().decode(treatyTransactionCash.getFileContent());
        dataSource = new ByteArrayDataSource(decodedBytes, treatyTransactionCash.getFileType());
        messageBodyPart.setFileName(treatyTransactionCash.getFileName());
        messageBodyPart.setDataHandler(new DataHandler(dataSource));
      }

      multipart.addBodyPart(messageBodyPart);

      // Send the complete message parts
      message.setContent(multipart);

      // Send message
      javaMailSender.send(message);
      log.debug("Sent e-mail to User '{}'");

      for (TreatySubtransactionCash subtransaction :treatyTransactionCash.getSubTransactions()) {
        auditFinanceEmailRepository.save(getAuditFinanceEmail(subtransaction, treatyTransactionCash,"Send Successfully"));
      }

    } catch (Exception e) {
      for (TreatySubtransactionCash subtransaction :treatyTransactionCash.getSubTransactions()) {
        auditFinanceEmailRepository.save(getAuditFinanceEmail(subtransaction, treatyTransactionCash, "Error sending Email"));
      }
      log.error("E-mail could not be sent to user '{}', exception is: {}", e);
    }
  }

}
