package com.rgare.global.finre.service;

import com.rgare.global.finre.domain.*;
import com.rgare.global.finre.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service for sending e-mails.
 * <p>
 * We use the @Async annotation to send e-mails asynchronously.
 * </p>
 */
@Service
public class RiskManagementUploadService {
    private final Logger log = LoggerFactory.getLogger(RiskManagementUploadService.class);
    private DealRepository dealRepository;
    private RiskManagementRepository riskManagementRepository;
    private CurrencyRepository currencyRepository;
    private AuditRiskManagementRepository auditRiskManagementRepository;
    private RiskCriteriaRepository riskCriteriaRepository;
    public RiskManagementUploadService(DealRepository dealRepository,
                                       RiskManagementRepository riskManagementRepository,
                                       CurrencyRepository currencyRepository,
                                       AuditRiskManagementRepository auditRiskManagementRepository,
                                       RiskCriteriaRepository riskCriteriaRepository) {
        this.dealRepository = dealRepository;
        this.riskManagementRepository = riskManagementRepository;
        this.currencyRepository = currencyRepository;
        this.auditRiskManagementRepository= auditRiskManagementRepository;
        this.riskCriteriaRepository=riskCriteriaRepository;
    }

    @Transactional
    public String uploadRiskManagement(RiskManagement riskManagementUpload) throws Exception{
      String data = riskManagementUpload.getUploadText();
      //Initializing
      List<RiskManagement> riskManagements = null;
      List<DealRiskManagementUploadDTO> uploadList = new  ArrayList <>();
      Map<Deal,List<DealRiskManagementUploadDTO>> dealMap = null;
      riskManagements = new ArrayList<>();
      dealMap = new HashMap<>();
      //Parse the data
      List<String[]> dealStrings = parseData(data);
     // System.out.println(Arrays.toString(dealStrings));
      //Loop through all the deals to see if they exist
      for (String[] dealArray: dealStrings) {
        System.out.println("--------DATA----------------------:" + dealArray);

        Deal deal = dealRepository.findByName(dealArray[0]);
        if (deal == null) {
          return "Your upload contains a deal named "+dealArray[0]+", which is an invalid name. Please fix and retry your upload.";
         // throw new Exception("This Deal does NOT exist!!!!"+dealArray[0]);
        }

       Currency currency= currencyRepository.findByCodeAndYear(dealArray[1],riskManagementUpload.getMonthYear().getYear());
        if (currency == null) {
       return "Your upload contains a currency code "+dealArray[1]+", which is an invalid code. Please fix and retry your upload.";
       //   throw new Exception("Currency NOT exist!!!!"+dealArray[1]);
        }
         RiskCriteria riskCriteria= riskCriteriaRepository.findByName(dealArray[2]);
        if (riskCriteria == null) {

          return "Your upload contains a Risk Criteria name "+dealArray[2]+", which is an invalid name. Please fix and retry your upload.";
            //"Risk Criteria Not exist!!!! "+dealArray[2];
         // throw new Exception("Risk Criteria Not exist!!!!"+dealArray[2]);
        }
        DealRiskManagementUploadDTO riskManagementItem = new DealRiskManagementUploadDTO();
        riskManagementItem.setDeal(deal);
        riskManagementItem.setRiskManagement(dealArray);
       riskManagementItem.setCurrency(currency);
        riskManagementItem.setRiskCriteria(riskCriteria);
          uploadList.add(riskManagementItem);
          dealMap.put(deal, uploadList);
      }

      for(DealRiskManagementUploadDTO  item : uploadList) {
        //Create RiskManagement and set the data.
        Double converted;
        Double amount = Double.parseDouble(item.getRiskManagement()[3].replaceAll(",", ""));
        //BigDecimal amount=new BigDecimal(item.getRiskManagement()[3].replaceAll(",", ""));

         //converted=item.getCurrency().getExchangeRate().multiply(amount);
        converted=item.getCurrency().getExchangeRate() * amount;

          RiskManagement riskManagementItem = new RiskManagement();
          riskManagementItem.setDeal(item.getDeal());
          riskManagementItem.setRiskCriteria(item.getRiskCriteria());
          riskManagementItem.setCurrency(item.getCurrency());
          riskManagementItem.setConvertedAmount( new BigDecimal(converted));
          riskManagementItem.setCountry(item.getDeal().getCountry());
          riskManagementItem.setLastUser(riskManagementUpload.getLastUser());
          riskManagementItem.setLastUpdate(riskManagementUpload.getLastUpdate());
          riskManagementItem.setCreationTs(riskManagementUpload.getCreationTs());
          riskManagementItem.setCreationUser(riskManagementUpload.getCreationUser());
          riskManagementItem.setAmount(new BigDecimal(item.getRiskManagement()[3].replaceAll(",", "")));
          riskManagementItem.setMonthYear(riskManagementUpload.getMonthYear());
          riskManagements.add(riskManagementItem);
      }

      //If all deals exist then save it to the database!
      if (!riskManagements.isEmpty()) {
        saveRiskManagement(riskManagements);
      }
      System.out.println("---------------------RETRUN SUCESSFUL -----------------------------");
      return "Successful!";
    }

    private List<String[]> parseData(String unparsdeData) throws Exception{
        //Are there any return carriage lines?
        String[] rows = unparsdeData.split("\\r?\\n"); //or \n
        if (rows.length == 0) {
          throw new Exception("No Return Lines!!!!");
        }
        List<String[]> data = null;
        data = new ArrayList<>();
        //Now separate them from the tabs.
        for (String dealRow: rows) {
          String[] deal = dealRow.split("\\t");
          data.add(deal);
        }
        return data;
    }

    private void saveRiskManagement(List<RiskManagement> dealList) {
        for (RiskManagement riskManagement: dealList) {

          RiskManagement result =riskManagementRepository.save(riskManagement);
          AuditRiskManagement auditRiskManagementObject =new AuditRiskManagement();
          auditRiskManagementObject.setCreationUser(result.getCreationUser());
          auditRiskManagementObject.setLastUpdate(riskManagement.getLastUpdate());
           auditRiskManagementObject.setCreationTs(riskManagement.getCreationTs());
          auditRiskManagementObject.setLastUser(result.getCreationUser());
          auditRiskManagementObject.setRiskManagementId(result.getId());
          auditRiskManagementObject.setDealId(riskManagement.getDeal().getId());
          auditRiskManagementObject.setAction("SAVE-GLOBAL");
          String comma = "' , '";
            auditRiskManagementObject.setComments("'"+riskManagement.getRiskCriteria().getName()+comma+riskManagement.getAmount()+comma+riskManagement.getConvertedAmount()+comma+riskManagement.getMonthYear()+comma+riskManagement.getCurrency().getCode()+"'");
          auditRiskManagementRepository.save(auditRiskManagementObject);
          System.out.println("---------------------Saved Risk MANAGEMENT AUDIT-----------------------------"+ result.getId()+"eee"+riskManagement.getRiskCriteria().getName()+comma+riskManagement.getAmount()+comma+riskManagement.getConvertedAmount()+comma+riskManagement.getMonthYear()+comma+riskManagement.getCurrency().getCode()+"'");
        }
    }


  private class DealRiskManagementUploadDTO {
    private Deal deal;
    private String[] riskManagement;
    private Currency currency;
    private RiskCriteria riskCriteria;

    public RiskCriteria getRiskCriteria() {
      return riskCriteria;
    }

    public void setRiskCriteria(RiskCriteria riskCriteria) {
      this.riskCriteria = riskCriteria;
    }

    public Currency getCurrency() {
      return currency;
    }

    public void setCurrency(Currency currency) {
      this.currency = currency;
    }

    public Deal getDeal() {
      return deal;
    }
    public void setDeal(Deal deal) {
      this.deal = deal;
    }
    public String[] getRiskManagement() {
      return riskManagement;
    }
    public void setRiskManagement( String[] riskManagement) {
      this.riskManagement = riskManagement;
    }
  }

}
