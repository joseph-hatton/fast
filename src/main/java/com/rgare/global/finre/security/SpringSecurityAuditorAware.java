package com.rgare.global.finre.security;

import com.rgare.global.finre.config.Constants;

import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

/**
 * Implementation of AuditorAware based on Spring Security.
 */
@Component
public class SpringSecurityAuditorAware implements AuditorAware<String> {

  private final SecurityUtils securityUtils;

  public SpringSecurityAuditorAware(SecurityUtils securityUtils) {
    this.securityUtils = securityUtils;
  }

  @Override
    public String getCurrentAuditor() {
        String userName = securityUtils.getCurrentUserLogin();
        return userName != null ? userName : Constants.SYSTEM_ACCOUNT;
    }
}
