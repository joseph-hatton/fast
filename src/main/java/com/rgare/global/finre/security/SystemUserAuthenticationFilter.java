package com.rgare.global.finre.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import javax.servlet.http.HttpServletRequest;

public class SystemUserAuthenticationFilter extends AbstractPreAuthenticatedProcessingFilter {

  private final Logger log = LoggerFactory.getLogger(SystemUserAuthenticationFilter.class);

  protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
    String userName = System.getProperty("user.name");
    log.debug("user.name: " + userName);
    return userName;
  }

  protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
    return "N/A";
  }
}
