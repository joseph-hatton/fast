package com.rgare.global.finre.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_APP_FINRE_OPS_ADMIN";

    public static final String USER = "ROLE_APP_FINRE_PROCESSOR";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    AuthoritiesConstants() {
    }
}
