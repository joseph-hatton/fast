package com.rgare.global.finre.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.LdapUserDetails;
import org.springframework.stereotype.Component;

import static net.logstash.logback.marker.Markers.append;


/**
 * Utility class for Spring Security.
 */
@Component
public class SecurityUtils {

    private static final Logger log = LoggerFactory.getLogger(SecurityUtils.class);

    /**
     * Get the login of the current user.
     *
     * @return the login of the current user
     */
    public String getCurrentUserLogin() {
        log.debug( "Getting current user login");
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        String userName = null;
        String userNameText = "username";
        if (authentication != null) {
          log.debug(append("authenticationPrincipalClass", authentication.getPrincipal().getClass()), "The authentication principal class");
          if (authentication.getPrincipal() instanceof UserDetails) {
                UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();
                userName = springSecurityUser.getUsername();
                if(userName !=null){
                  log.debug(append(userNameText, userName), "Found current user as UserDetails");
                }
            } else if (authentication.getPrincipal() instanceof String) {
                userName = (String) authentication.getPrincipal();
                if(userName !=null){
                  log.debug(append(userNameText, userName), "Found current user as String");
                }
            } else if (authentication.getPrincipal() instanceof LdapUserDetails) {
                LdapUserDetails ldapUser = (LdapUserDetails) authentication.getPrincipal();
                if(ldapUser.getUsername() !=null){
                  log.debug(append(userNameText, ldapUser.getUsername()), "Found current user as ldap user");
                }
                return ldapUser.getUsername();
            }
        }
      log.debug(append(userNameText, userName), "returning current user");
      return userName;
    }

    /**
     * Check if a user is authenticated.
     *
     * @return true if the user is authenticated, false otherwise
     */
    public boolean isAuthenticated() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        if (authentication != null) {
            return authentication.getAuthorities().stream()
                .noneMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(AuthoritiesConstants.ANONYMOUS));
        }
        return false;
    }

    /**
     * If the current user has a specific authority (security role).
     *
     * <p>The name of this method comes from the isUserInRole() method in the Servlet API</p>
     *
     * @param authority the authority to check
     * @return true if the current user has the authority, false otherwise
     */
    public boolean isCurrentUserInRole(String authority) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        if (authentication != null) {
            return authentication.getAuthorities().stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(authority));
        }
        return false;
    }
}
