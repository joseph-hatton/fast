package com.rgare.global.finre.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import static net.logstash.logback.marker.Markers.append;
import static net.logstash.logback.marker.Markers.appendArray;

public class DebugFilter extends GenericFilterBean {

  private final Logger log = LoggerFactory.getLogger(DebugFilter.class);

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest httpRequest = (HttpServletRequest) request;
    Enumeration<String> headerNames = httpRequest.getHeaderNames();

    Cookie[] cookies = httpRequest.getCookies();
    if (headerNames != null || cookies != null){
      List<String> headers = new ArrayList<String>();
      List<String> cookiesList = new ArrayList<String>();
      if (headerNames != null) {
        while (headerNames.hasMoreElements()) {
          String headerName = headerNames.nextElement();
          headers.add(headerName + " = " + httpRequest.getHeader(headerName));
        }
      }
      if (cookies != null) {
        for (Cookie cookie : cookies) {
          cookiesList.add( cookie.getName() + " = " + cookie.getValue());
        }
      }
      log.debug(append("uri", httpRequest.getRequestURI()).and(appendArray("headers", headers).and(appendArray("cookies", cookiesList))), "debug filter");
    };

    chain.doFilter(request, response);
  }
}
