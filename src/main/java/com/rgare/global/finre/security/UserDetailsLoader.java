package com.rgare.global.finre.security;

import com.rgare.global.finre.domain.User;
import com.rgare.global.finre.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static net.logstash.logback.marker.Markers.append;

@Component
public class UserDetailsLoader implements AuthenticationUserDetailsService {

  private static final String NAME = "name";
  private static final String AUTHORITIES = "authorities";
  private final Logger log = LoggerFactory.getLogger(UserDetailsLoader.class);

  private UserService userService;

  @Override
  public UserDetails loadUserDetails(Authentication token) throws UsernameNotFoundException {
    log.debug(append(NAME, token.getName()), "Load user details by name");

    List<SimpleGrantedAuthority> grantedAuthorities = null;
    Optional<User> userWithAuthorities = userService.getUserWithAuthoritiesByLogin(token.getPrincipal() + "@RGARE.NET");
    if (userWithAuthorities.isPresent()) {
      log.debug(append(NAME, token.getName()).and(append(AUTHORITIES, userWithAuthorities.get().getAuthorities())), "found user!");
      grantedAuthorities = userWithAuthorities.get().getAuthorities().stream().map((authority) -> new SimpleGrantedAuthority(authority.getName())).collect(Collectors.toList());
    } else {
      log.debug(append(NAME, token.getName()), "unable to find user!");
      grantedAuthorities = new ArrayList<>();
    }
    return new org.springframework.security.core.userdetails.User(token.getName(), "", grantedAuthorities);
  }

  public void setUserService(UserService userService) {
    log.debug("setting user service", userService);
    this.userService = userService;
  }
}
