package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.SettlementType;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the SettlementType entity.
 */
public interface SettlementTypeRepository extends JpaRepository<SettlementType,Long> {

}
