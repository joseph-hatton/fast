package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.NotificationStatus;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the NotificationStatus entity.
 */
public interface NotificationStatusRepository extends JpaRepository<NotificationStatus,Long> {

}
