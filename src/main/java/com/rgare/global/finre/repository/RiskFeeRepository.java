package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.RiskFee;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the RiskFee entity.
 */
public interface RiskFeeRepository extends JpaRepository<RiskFee,Long> {

}
