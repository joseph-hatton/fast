package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.AllocationType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the AllocationType entity.
 */
public interface AllocationTypeSearchRepository extends ElasticsearchRepository<AllocationType, Long> {
}
