package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.RiskManagement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

/**
 * Spring Data JPA repository for the RiskManagement entity.
 */
public interface RiskManagementRepository extends JpaRepository<RiskManagement,Long> {

  List<RiskManagement> findByMonthYear(LocalDate monthYear);

  List<RiskManagement> findAllByDealIdAndMonthYear(Long dealId, LocalDate date);
}
