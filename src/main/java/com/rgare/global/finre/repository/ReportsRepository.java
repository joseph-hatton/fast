package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.Reports;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Reports entity.
 */
@SuppressWarnings("unused")
public interface ReportsRepository extends JpaRepository<Reports,Long> {

}
