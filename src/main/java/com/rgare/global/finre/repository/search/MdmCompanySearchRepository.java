package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.MdmCompany;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the MdmCompany entity.
 */
public interface MdmCompanySearchRepository extends ElasticsearchRepository<MdmCompany, Long> {
}
