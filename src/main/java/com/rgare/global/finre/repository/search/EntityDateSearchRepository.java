package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.EntityDate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the EntityDate entity.
 */
public interface EntityDateSearchRepository extends ElasticsearchRepository<EntityDate, Long> {
}
