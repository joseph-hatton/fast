package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.ProductSubtype;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ProductSubtype entity.
 */
public interface ProductSubtypeRepository extends JpaRepository<ProductSubtype,Long> {

}
