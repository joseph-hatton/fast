package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.ProductStructure;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ProductStructure entity.
 */
public interface ProductStructureSearchRepository extends ElasticsearchRepository<ProductStructure, Long> {
}
