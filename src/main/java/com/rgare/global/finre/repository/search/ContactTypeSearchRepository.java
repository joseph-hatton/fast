package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.ContactType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ContactType entity.
 */
public interface ContactTypeSearchRepository extends ElasticsearchRepository<ContactType, Long> {
}
