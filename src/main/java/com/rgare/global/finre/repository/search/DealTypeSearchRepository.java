package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.DealType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the DealType entity.
 */
public interface DealTypeSearchRepository extends ElasticsearchRepository<DealType, Long> {
}
