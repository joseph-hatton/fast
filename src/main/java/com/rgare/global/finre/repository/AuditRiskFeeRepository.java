package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.AuditRiskFee;
import com.rgare.global.finre.domain.AuditTransactions;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by s0044514 on 1/17/2017.
 */
public interface AuditRiskFeeRepository extends JpaRepository<AuditRiskFee,Long> {
}
