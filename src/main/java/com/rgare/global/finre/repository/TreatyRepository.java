package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.Treaty;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Treaty entity.
 */
public interface TreatyRepository extends JpaRepository<Treaty,Long> {

    Page<Treaty> findAllByIsActive(Integer isActive, Pageable pageable);

    List<Treaty> findAllByIsActive(Integer isActive);

    Page<Treaty> findAllByDealIdAndIsActive(Long dealId, Integer isActive, Pageable pageable);

    Long countByDealIdAndIsActive(Long dealId, Integer isActive);

    List<Treaty> findAllByNotificationStatus(Integer notificationStatus);

    List<Treaty> findAllByTreatyNameIgnoreCaseAndIsActive(String treatyName, Integer isActive);
}
