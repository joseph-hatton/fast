package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.TreatySeriatim;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TreatySeriatim entity.
 */
public interface TreatySeriatimSearchRepository extends ElasticsearchRepository<TreatySeriatim, Long> {
}
