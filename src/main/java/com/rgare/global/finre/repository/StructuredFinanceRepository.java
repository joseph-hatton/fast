package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.StructuredFinance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.rgare.global.finre.domain.StructuredTracking;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the StructuredFinance entity.
 */
public interface StructuredFinanceRepository extends JpaRepository<StructuredFinance,Long> {

  Page<StructuredFinance> findAllByDealIdAndIsActive(Long dealId, Pageable pageable, Integer isActive);

}
