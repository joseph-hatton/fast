package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.Deal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Deal entity.
 */
public interface DealRepository extends JpaRepository<Deal, Long> {

  @Query("SELECT deal FROM Deal deal WHERE LOWER(deal.name) LIKE LOWER(CONCAT('%',:searchString,'%')) AND deal.isActive=1")
  Page<Deal> searchExistingDealQuery(@Param("searchString") String searchString, Pageable pageable);

  List<Deal> findAllByNameIgnoreCaseAndIsActiveAndDealStatus(String searchString,Integer isActive, String DealStatus);

  Page<Deal> findDealsByIsActive(Integer isActive, Pageable pageable);

  List<Deal> findDealsByIsActive(Integer isActive);

  Deal findByName(String name);
}
