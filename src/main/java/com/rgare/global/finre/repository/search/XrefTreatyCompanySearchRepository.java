package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.XrefTreatyCompany;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the XrefTreatyCompany entity.
 */
public interface XrefTreatyCompanySearchRepository extends ElasticsearchRepository<XrefTreatyCompany, Long> {
}
