package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.AgreementType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the AgreementType entity.
 */
public interface AgreementTypeSearchRepository extends ElasticsearchRepository<AgreementType, Long> {
}
