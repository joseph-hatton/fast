package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.RiskCriteria;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the RiskCriteria entity.
 */
@SuppressWarnings("unused")
public interface RiskCriteriaRepository extends JpaRepository<RiskCriteria,Long> {

  RiskCriteria findByName(String name);

}
