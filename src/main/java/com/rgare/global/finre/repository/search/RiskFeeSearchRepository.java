package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.RiskFee;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the RiskFee entity.
 */
public interface RiskFeeSearchRepository extends ElasticsearchRepository<RiskFee, Long> {
}
