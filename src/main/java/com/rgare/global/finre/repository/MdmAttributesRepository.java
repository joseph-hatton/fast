package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.MdmAttributes;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the MdmAttributes entity.
 */
public interface MdmAttributesRepository extends JpaRepository<MdmAttributes,Long> {
  List<MdmAttributes> findAllByEnterpriseId(Long enterpriseId);
}
