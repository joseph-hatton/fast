package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.StructuredTracking;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the StructuredTracking entity.
 */
public interface StructuredTrackingSearchRepository extends ElasticsearchRepository<StructuredTracking, Long> {
}
