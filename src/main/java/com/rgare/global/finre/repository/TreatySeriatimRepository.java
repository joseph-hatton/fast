package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.TreatySeriatim;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the TreatySeriatim entity.
 */
public interface TreatySeriatimRepository extends JpaRepository<TreatySeriatim,Long> {

  Page<TreatySeriatim> findAllByTreatyId(@Param("treatyId")Long treatyId, Pageable pageable);

}
