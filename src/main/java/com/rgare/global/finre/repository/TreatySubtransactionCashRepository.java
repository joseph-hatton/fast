package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.TreatySubtransactionCash;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the TreatySubTransaction entity.
 */
public interface TreatySubtransactionCashRepository extends JpaRepository<TreatySubtransactionCash,Long> {

  Page<TreatySubtransactionCash> findAllByTreatyTransactionCashId(@Param("treatyTransactionCashId")Long treatyTransactionCashId, Pageable pageable);

  List<TreatySubtransactionCash> findAllByTrackingIdAndIsActive(Long trackingId,Integer isActive);
  List<TreatySubtransactionCash> findAllByMdmIdAndIsActive(Long mdmId,Integer isActive);
  List<TreatySubtransactionCash> findAllByTreatyIdAndIsActiveOrderByIdDesc(Long treatyId,Integer isActive);

 // List<TreatySubtransactionCash> findAllByTreatyIdAndCompanyId(Long treatyId,Long companyId);
}
