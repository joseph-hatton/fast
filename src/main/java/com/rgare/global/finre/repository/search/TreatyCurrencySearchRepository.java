package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.TreatyCurrency;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TreatyCurrency entity.
 */
public interface TreatyCurrencySearchRepository extends ElasticsearchRepository<TreatyCurrency, Long> {
}
