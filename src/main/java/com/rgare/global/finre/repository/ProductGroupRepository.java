package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.ProductGroup;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the ProductGroup entity.
 */
public interface ProductGroupRepository extends JpaRepository<ProductGroup,Long> {

}
