package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.MdmAttributes;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the MdmAttributes entity.
 */
public interface MdmAttributesSearchRepository extends ElasticsearchRepository<MdmAttributes, Long> {
}
