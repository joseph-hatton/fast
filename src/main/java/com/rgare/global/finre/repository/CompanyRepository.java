package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.Company;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Company entity.
 */
public interface CompanyRepository extends JpaRepository<Company,Long> {

    @Query("SELECT company FROM Company company WHERE LOWER(company.name) LIKE LOWER(CONCAT('%',:searchString,'%')) OR " +
      "company.clientCompanyId LIKE CONCAT('%',:searchString,'%')")
    Page<Company> searchCompaniesQuery(@Param("searchString") String searchString, Pageable pageable);

  /* findByClientCompanyIdIsNull */
  @Query("SELECT company FROM Company company WHERE (company.clientCompanyId is null or company.clientCompanyId = '')")
  Page<Company> searchAllNonMDMCompaniesQuery(Pageable pageable);

  /* findByClientCompanyIdIsNotNull */
  @Query("SELECT company FROM Company company WHERE  (company.clientCompanyId is not null)")
  Page<Company> searchAllMDMCompaniesQuery(Pageable pageable);

  /* findByClientCompanyIdIsNotNull */
  @Query("SELECT company FROM Company company WHERE  (company.clientCompanyId is not null)")
  List<Company> getAllMDMCompanies();


  /* findByClientCompanyIdIsNull */
  @Query("SELECT company FROM Company company WHERE LOWER(company.name) LIKE LOWER(CONCAT('%',:searchString,'%')) AND " +
    "(company.clientCompanyId is null or company.clientCompanyId = '')")
  Page<Company> searchClientCompanyIdIsNullQuery(@Param("searchString") String searchString,Pageable pageable);

  /* findByClientCompanyIdIsNotNull */
  @Query("SELECT company FROM Company company WHERE  LOWER(company.name) LIKE LOWER(CONCAT('%',:searchString,'%')) AND " +
    "(company.clientCompanyId is not null)")
  Page<Company> searchClientCompanyIdIsNotNullQuery(@Param("searchString") String searchString,Pageable pageable);



  @Query("SELECT company " +
    "FROM Company company  " +
    "WHERE LOWER(company.name) " +
    "LIKE LOWER(CONCAT('%',:searchString,'%')) AND company.clientCompanyId IS NOT NULL OR " +
    "company.clientCompanyId LIKE CONCAT('%',:searchString,'%') " +
    "AND company.clientCompanyId IS NOT NULL "
  )
  Page<Company> searchExistingMdmCompanyQuery(@Param("searchString") String searchString, Pageable pageable);





  List<Company> findByIsRgaCompany(Integer isRgaCompany);

}
