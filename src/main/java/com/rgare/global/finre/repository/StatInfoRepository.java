package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.StatInfo;
import java.util.List;
import org.springframework.data.jpa.repository.*;

/**
 * Spring Data JPA repository for the IntroStep entity.
 */
public interface StatInfoRepository extends JpaRepository<StatInfo,Long> {
    public List<StatInfo> findAllByTitleLikeAndIsActive(String title, Integer isActive);
 }
