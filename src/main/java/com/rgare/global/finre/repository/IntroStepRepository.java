package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.IntroStep;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the IntroStep entity.
 */
public interface IntroStepRepository extends JpaRepository<IntroStep,Long> {

    Page<IntroStep> findIntroStepsByIsActive(Integer isActive, Pageable pageable);

    List<IntroStep> findIntroStepsByIsActive(Integer isActive);

    IntroStep findOneByIsActiveAndId(Integer isActive, Long id);
}
