package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.TreatyTracking;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TreatyTracking entity.
 */
public interface TreatyTrackingSearchRepository extends ElasticsearchRepository<TreatyTracking, Long> {
}
