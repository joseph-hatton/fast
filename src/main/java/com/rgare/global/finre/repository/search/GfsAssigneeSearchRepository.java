package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.GfsAssignee;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the GfsAssignee entity.
 */
public interface GfsAssigneeSearchRepository extends ElasticsearchRepository<GfsAssignee, Long> {
}
