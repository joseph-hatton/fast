package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.Frequency;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Frequency entity.
 */
public interface FrequencySearchRepository extends ElasticsearchRepository<Frequency, Long> {
}
