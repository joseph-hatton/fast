package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.Treaty;
import com.rgare.global.finre.domain.TreatyAmendments;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the TreatyAmendments entity.
 */
public interface TreatyAmendmentsRepository extends JpaRepository<TreatyAmendments,Long> {

    Page<TreatyAmendments> findAllByIsActive(Integer isActive, Pageable pageable);

    Page<TreatyAmendments> findAllByTreatyIdAndIsActive(Long treatyId, Integer isActive, Pageable pageable);
    List<TreatyAmendments> findAllByTreatyIdAndIsActive(Long treatyId, Integer isActive, Sort sort);

    List<TreatyAmendments> findAllByIsActive(Integer isActive);

    List<TreatyAmendments> findAll();
}
