package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.ProductGroup;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ProductGroup entity.
 */
public interface ProductGroupSearchRepository extends ElasticsearchRepository<ProductGroup, Long> {
}
