package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.Frequency;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Frequency entity.
 */
public interface FrequencyRepository extends JpaRepository<Frequency,Long> {

}
