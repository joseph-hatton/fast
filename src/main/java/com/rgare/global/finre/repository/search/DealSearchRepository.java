package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.Deal;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Deal entity.
 */
public interface DealSearchRepository extends ElasticsearchRepository<Deal, Long> {
}
