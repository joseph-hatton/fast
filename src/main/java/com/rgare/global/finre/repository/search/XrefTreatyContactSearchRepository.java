package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.XrefTreatyContact;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the XrefTreatyContact entity.
 */
public interface XrefTreatyContactSearchRepository extends ElasticsearchRepository<XrefTreatyContact, Long> {
}
