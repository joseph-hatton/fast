package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.TreatyTransactionCash;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TreatyTransaction entity.
 */
public interface TreatyTransactionCashSearchRepository extends ElasticsearchRepository<TreatyTransactionCash, Long> {
}
