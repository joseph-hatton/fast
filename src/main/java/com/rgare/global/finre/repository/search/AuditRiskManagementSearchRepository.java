package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.AuditRiskManagement;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AuditRiskManagement entity.
 */
public interface AuditRiskManagementSearchRepository extends ElasticsearchRepository<AuditRiskManagement, Long> {
}
