package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.TransactionStatus;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TransactionStatus entity.
 */
public interface TransactionStatusSearchRepository extends ElasticsearchRepository<TransactionStatus, Long> {
}
