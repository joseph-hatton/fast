package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.Contact;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Contact entity.
 */
public interface ContactRepository extends JpaRepository<Contact,Long> {

  @Query("SELECT contact FROM Contact contact WHERE LOWER(contact.firstName) LIKE LOWER(CONCAT('%',:searchString,'%')) OR " +
    "LOWER(contact.lastName) LIKE LOWER(CONCAT('%',:searchString,'%'))")
  List<Contact> findByFirstNameOrLastName(@Param("searchString") String searchString);


  @Query("SELECT contact FROM Contact contact WHERE LOWER(contact.firstName) LIKE LOWER(CONCAT('%',:searchString,'%')) OR " +
    "LOWER(contact.lastName) LIKE LOWER(CONCAT('%',:searchString,'%'))")
  Page<Contact> findByFirstNameOrLastNamePag(@Param("searchString") String searchString ,Pageable pageable);
}
