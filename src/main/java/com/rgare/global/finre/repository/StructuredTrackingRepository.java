package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.StructuredTracking;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.time.LocalDate;
import java.util.List;

/**
 * Spring Data JPA repository for the StructuredTracking entity.
 */
public interface StructuredTrackingRepository extends JpaRepository<StructuredTracking,Long> {

  Page<StructuredTracking> findAllByDealIdAndIsActive(Long dealId, Pageable pageable, Integer isActive);

  Page<StructuredTracking> findAllByStructuredFinanceIdAndIsActive(Long structuredFinanceId,Pageable pageable, Integer isActive);

  Page<StructuredTracking> findAllByStructuredFinanceIdAndClientReportingDateBetweenAndIsActive(Long structuredFinanceId, LocalDate startDate, LocalDate endDate, Pageable pageable, Integer isActive);
}
