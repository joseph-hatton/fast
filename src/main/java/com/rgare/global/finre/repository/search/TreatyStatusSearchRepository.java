package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.TreatyStatus;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TreatyStatus entity.
 */
public interface TreatyStatusSearchRepository extends ElasticsearchRepository<TreatyStatus, Long> {
}
