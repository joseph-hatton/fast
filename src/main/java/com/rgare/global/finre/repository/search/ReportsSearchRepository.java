package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.Reports;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Reports entity.
 */
public interface ReportsSearchRepository extends ElasticsearchRepository<Reports, Long> {
}
