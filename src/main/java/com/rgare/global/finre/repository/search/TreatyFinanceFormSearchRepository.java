//package com.rgare.global.finre.repository.search;
//
//import com.rgare.global.finre.domain.TreatyFinanceForm;
//import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
//
///**
// * Spring Data ElasticSearch repository for the TreatyFinanceForm entity.
// */
//public interface TreatyFinanceFormSearchRepository extends ElasticsearchRepository<TreatyFinanceForm, Long> {
//}
