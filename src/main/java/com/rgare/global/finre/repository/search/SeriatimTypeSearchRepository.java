package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.SeriatimType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the SeriatimType entity.
 */
public interface SeriatimTypeSearchRepository extends ElasticsearchRepository<SeriatimType, Long> {
}
