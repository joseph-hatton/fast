package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.RiskManagement;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the RiskManagement entity.
 */
public interface RiskManagementSearchRepository extends ElasticsearchRepository<RiskManagement, Long> {
}
