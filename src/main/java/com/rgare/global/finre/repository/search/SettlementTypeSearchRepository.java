package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.SettlementType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the SettlementType entity.
 */
public interface SettlementTypeSearchRepository extends ElasticsearchRepository<SettlementType, Long> {
}
