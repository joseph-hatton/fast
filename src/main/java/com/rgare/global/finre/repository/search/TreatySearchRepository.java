package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.Treaty;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Treaty entity.
 */
public interface TreatySearchRepository extends ElasticsearchRepository<Treaty, Long> {
}
