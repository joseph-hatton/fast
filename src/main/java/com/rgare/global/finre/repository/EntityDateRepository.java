package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.EntityDate;

import com.rgare.global.finre.domain.Treaty;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the EntityDate entity.
 */
public interface EntityDateRepository extends JpaRepository<EntityDate,Long> {

  List<EntityDate> findAllByEntityId(Long entityId);

}
