package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.EnterpriseKey;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the EnterpriseKey entity.
 */
public interface EnterpriseKeyRepository extends JpaRepository<EnterpriseKey,Long> {

  EnterpriseKey findOneByEnterpriseId(Long enterpriseId);


}
