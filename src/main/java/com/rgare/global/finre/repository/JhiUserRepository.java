package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.JhiUser;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the JhiUser entity.
 */
public interface JhiUserRepository extends JpaRepository<JhiUser,Long> {
    JhiUser findOneById(@Param("id")Long id);
    void deleteById(@Param("id")Long id);
}
