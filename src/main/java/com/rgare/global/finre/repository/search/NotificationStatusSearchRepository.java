package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.NotificationStatus;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the NotificationStatus entity.
 */
public interface NotificationStatusSearchRepository extends ElasticsearchRepository<NotificationStatus, Long> {
}
