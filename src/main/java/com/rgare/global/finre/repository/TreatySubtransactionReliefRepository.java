package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.TreatySubtransactionRelief;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

/**
 * Spring Data JPA repository for the TreatyTransactionRelief entity.
 */
public interface TreatySubtransactionReliefRepository extends JpaRepository<TreatySubtransactionRelief,Long> {

  @Query("SELECT sub FROM TreatySubtransactionRelief sub INNER JOIN sub.treatyTransactionRelief tran INNER JOIN tran.deal WHERE tran.deal.id = :dealId AND tran.isActive = :active")
  List<TreatySubtransactionRelief> findAllByDealIdAndIsActive(@Param("dealId")Long dealId, @Param("active")Integer active, Sort sort);

  @Query("SELECT sub FROM TreatySubtransactionRelief sub INNER JOIN sub.treatyTransactionRelief tran INNER JOIN tran.deal WHERE tran.deal.id = :dealId AND tran.isActive = :active AND tran.reportingDate BETWEEN :start AND :end")
  List<TreatySubtransactionRelief> findAllByDealIdAndIsActiveAndReportingDateBetween(@Param("dealId")Long dealId, @Param("active")Integer active, @Param("start")LocalDate start, @Param("end")LocalDate end, Sort sort);
}
