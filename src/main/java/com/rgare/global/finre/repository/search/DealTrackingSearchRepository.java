package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.DealTracking;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the DealTracking entity.
 */
public interface DealTrackingSearchRepository extends ElasticsearchRepository<DealTracking, Long> {
}
