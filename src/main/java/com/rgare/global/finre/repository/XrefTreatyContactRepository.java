package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.XrefTreatyContact;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the XrefTreatyContact entity.
 */
public interface XrefTreatyContactRepository extends JpaRepository<XrefTreatyContact,Long> {

  List<XrefTreatyContact> findAllByTreatyId(Long treatyId);

  List<XrefTreatyContact> findAllByContactId(Long contactId);

}
