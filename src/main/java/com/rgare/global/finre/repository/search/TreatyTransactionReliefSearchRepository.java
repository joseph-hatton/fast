package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.TreatyTransactionRelief;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TreatyTransactionRelief entity.
 */
public interface TreatyTransactionReliefSearchRepository extends ElasticsearchRepository<TreatyTransactionRelief, Long> {
}
