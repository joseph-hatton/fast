package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.AuditFinanceEmail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by s0044514 on 1/17/2017.
 */
public interface AuditFinanceEmailRepository extends JpaRepository<AuditFinanceEmail,Long> {
}
