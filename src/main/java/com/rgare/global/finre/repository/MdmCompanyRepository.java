package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.MdmCompany;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the MdmAttributes entity.
 */
public interface MdmCompanyRepository extends JpaRepository<MdmCompany,Long> {

  @Query("SELECT mdmcompany " +
    "FROM MdmCompany mdmcompany " +
    "WHERE LOWER(mdmcompany.companyName) " +
    "LIKE LOWER(CONCAT('%',:searchString,'%')) AND mdmcompany.clientCompanyId not in (SELECT DISTINCT(clientCompanyId) FROM Company company WHERE company.clientCompanyId IS NOT NULL) OR " +
    "mdmcompany.clientCompanyId LIKE CONCAT('%',:searchString,'%') " +
    "AND mdmcompany.clientCompanyId not in (SELECT DISTINCT(clientCompanyId) FROM Company company WHERE company.clientCompanyId IS NOT NULL)"
  )
  Page<MdmCompany> searchExistingMdmCompanyQuery(@Param("searchString") String searchString, Pageable pageable);

  List<MdmCompany> findAll();
}
