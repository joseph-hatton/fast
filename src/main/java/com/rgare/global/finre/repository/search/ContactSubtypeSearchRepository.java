package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.ContactSubtype;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ContactSubtype entity.
 */
public interface ContactSubtypeSearchRepository extends ElasticsearchRepository<ContactSubtype, Long> {
}
