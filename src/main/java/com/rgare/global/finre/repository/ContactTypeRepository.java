package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.ContactType;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ContactType entity.
 */
public interface ContactTypeRepository extends JpaRepository<ContactType,Long> {

}
