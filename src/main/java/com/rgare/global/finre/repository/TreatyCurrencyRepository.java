package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.TreatyCurrency;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TreatyCurrency entity.
 */
public interface TreatyCurrencyRepository extends JpaRepository<TreatyCurrency,Long> {

  List<TreatyCurrency> findAllByCurrencyId(Long currencyId);


}
