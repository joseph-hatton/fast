package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.TreatyTracking;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

/**
 * Spring Data JPA repository for the TreatyTracking entity.
 */
public interface TreatyTrackingRepository extends JpaRepository<TreatyTracking,Long> {

  TreatyTracking findOneByTreatyId(@Param("treatyId")Long treatyId);

  Page<TreatyTracking> findAllByTreatyIdAndIsActive(Long treatyId, Pageable pageable,Integer isActive);
  Page<TreatyTracking> findAllByTreatyIdAndIsActiveOrderByIdDesc(Long treatyId, Pageable pageable, Integer isActive);

  Page<TreatyTracking> findAllByTreatyIdAndIsActiveAndTrackingStatus(Long treatyId, Pageable pageable,Integer isActive,String trackingStatus);

  Page<TreatyTracking> findByTreatyIdAndIsActiveAndReportingMonthYearBetween(Long treatyId,Integer isActive,LocalDate start, LocalDate end, Pageable pageable);

  List<TreatyTracking> findAll();
  List<TreatyTracking> findByIsActiveAndCalculatedAssumingGraceDateBetween(Integer isActive, LocalDate start, LocalDate end);
  List<TreatyTracking> findByIsActiveAndCalculatedCedingGraceDateBetween(Integer isActive,LocalDate start, LocalDate end);
  List<TreatyTracking> findByIsActiveAndReportingMonthYearBeforeAndTreatyId(Integer isActive,LocalDate end,Long treatyId);
  List<TreatyTracking> findByTreatyIdAndIsActiveAndReportingMonthYear(Long treatyId,Integer isActive,LocalDate start);
  List<TreatyTracking> findByIsActiveAndCalculatedSeriatimDueDateBetween(Integer isActive,LocalDate start, LocalDate end);
  List<TreatyTracking> findByIsActiveAndTrackingStatusAndCalculatedSeriatimDueDateBefore(Integer isActive,String Status,LocalDate start);
  List<TreatyTracking> findByIsActiveAndTrackingStatusAndCalculatedCedingGraceDateBefore(Integer isActive,String Status,LocalDate start);
//  List<TreatyTracking> findByIsActiveAndTrackingStatusAndCalculatedAssumingGraceDateBefore(Integer isActive,String Status,LocalDate start);
  List<TreatyTracking> findByIsActiveAndTrackingStatusAndCalculatedReportingDueDateBefore(Integer isActive,String Status,LocalDate start);
  List<TreatyTracking> findByIsActiveAndCalculatedReportingDueDateBetween(Integer isActive,LocalDate start, LocalDate end);
  List<TreatyTracking> findByIsActiveAndTrackingStatusAndCalculatedAssumingDateBetween(Integer isActive, String trackingStatus, LocalDate start, LocalDate end);
  List<TreatyTracking> findByIsActiveAndTrackingStatusAndCalculatedAssumingDateBeforeAndCalculatedAssumingGraceDateAfter(Integer isActive, String trackingStatus, LocalDate date1, LocalDate date2);
  List<TreatyTracking> findByIsActiveAndTrackingStatusAndCalculatedAssumingGraceDateBefore(Integer isActive, String trackingStatus, LocalDate date);
  Long countByIsActiveAndTreatyId(Integer isActive, Long treatyId);




  List<TreatyTracking> findByIsActiveAndTrackingStatusAndReportNotifFlag(Integer isActive,String trackingStatus, Integer reportNotifFlag);

  List<TreatyTracking> findByIsActiveAndTrackingStatusAndCalculatedReportingDueDateBetweenAndReportingReceivedDateIsNullAndReportNotifFlag(Integer isActive,String trackingStatus,LocalDate start, LocalDate end, Integer reportNotifFlag);
  List<TreatyTracking> findByIsActiveAndTrackingStatusAndCalculatedAssumingGraceDateBeforeAndReportingReceivedDateIsNullAndReportNotifFlag(Integer isActive, String trackingStatus, LocalDate date, Integer reportNotifFlag);
  List<TreatyTracking> findByIsActiveAndTrackingStatusAndCalculatedReportingDueDateBeforeAndCalculatedAssumingGraceDateAfterAndReportingReceivedDateIsNullAndReportNotifFlag(Integer isActive,String Status,LocalDate start, LocalDate end, Integer reportNotifFlag);

  List<TreatyTracking> findByIsActiveAndTrackingStatusAndCashDueFlagAndCalculatedAssumingDateBetweenAndCashDueTypeAndCashSentRecDateIsNull(Integer isActive, String trackingStatus, String cashDueFlag, LocalDate start, LocalDate end, String cashDueType);
  List<TreatyTracking> findByIsActiveAndTrackingStatusAndCashDueFlagAndCalculatedAssumingDateBeforeAndCalculatedAssumingGraceDateAfterAndCashDueTypeAndCashSentRecDateIsNull(Integer isActive, String trackingStatus, String cashDueFlag, LocalDate date1, LocalDate date2, String cashDueType);
  List<TreatyTracking> findByIsActiveAndTrackingStatusAndCashDueFlagAndCalculatedAssumingGraceDateBeforeAndCashDueTypeAndCashSentRecDateIsNull(Integer isActive, String trackingStatus, String cashDueFlag, LocalDate date, String cashDueType);

  List<TreatyTracking> findByIsActiveAndTrackingStatusAndCashDueFlagAndCalculatedCedingDateBetweenAndCashDueTypeAndCashSentRecDateIsNull(Integer isActive, String trackingStatus, String cashDueFlag, LocalDate start, LocalDate end, String cashDueType);
  List<TreatyTracking> findByIsActiveAndTrackingStatusAndCashDueFlagAndCalculatedCedingDateBeforeAndCalculatedCedingGraceDateAfterAndCashDueTypeAndCashSentRecDateIsNull(Integer isActive, String trackingStatus, String cashDueFlag, LocalDate date1, LocalDate date2, String cashDueType);
  List<TreatyTracking> findByIsActiveAndTrackingStatusAndCashDueFlagAndCalculatedCedingGraceDateBeforeAndCashDueTypeAndCashSentRecDateIsNull(Integer isActive, String trackingStatus, String cashDueFlag, LocalDate date, String cashDueType);
  List<TreatyTracking> findByIsActiveAndTrackingStatusAndReportingReceivedDateIsNullAndCalculatedReportingDueDateBefore(Integer isActive, String trackingStatus, LocalDate date);

}

