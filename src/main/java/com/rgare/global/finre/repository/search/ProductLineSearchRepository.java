package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.ProductLine;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ProductLine entity.
 */
public interface ProductLineSearchRepository extends ElasticsearchRepository<ProductLine, Long> {
}
