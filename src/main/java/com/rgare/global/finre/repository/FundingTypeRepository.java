package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.FundingType;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the FundingType entity.
 */
public interface FundingTypeRepository extends JpaRepository<FundingType,Long> {

}
