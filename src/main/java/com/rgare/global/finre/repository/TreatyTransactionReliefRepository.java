package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.TreatyTransactionRelief;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

/**
 * Spring Data JPA repository for the TreatyTransactionRelief entity.
 */
public interface TreatyTransactionReliefRepository extends JpaRepository<TreatyTransactionRelief,Long> {

  Page<TreatyTransactionRelief> findAllByDealId(@Param("dealId")Long dealId, Pageable pageable);

  Page<TreatyTransactionRelief> findAllByDealIdAndIsActive(Long dealId, Integer isActive, Pageable pageable);
  List<TreatyTransactionRelief> findAllByDealIdAndIsActiveOrderByIdDesc(Long dealId, Integer isActive);
  Page<TreatyTransactionRelief> findAllByDealIdAndIsActiveOrderByIdDesc(Long dealId, Integer isActive, Pageable pageable);

  Long countByDealIdAndIsActive(Long dealId, Integer isActive);

  Page<TreatyTransactionRelief> findAllByDealIdAndIsActiveAndReportingDateBetween(Long dealId, Integer isActive,LocalDate start, LocalDate end, Pageable pageable);
}
