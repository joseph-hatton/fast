package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.JhiPersistentAuditEvent;

import org.springframework.data.jpa.repository.*;

/**
 * Spring Data JPA repository for the JhiPersistentAuditEvent entity.
 */
public interface JhiPersistentAuditEventRepository extends JpaRepository<JhiPersistentAuditEvent,Long> {}
