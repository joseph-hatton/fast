package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.SeriatimType;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the SeriatimType entity.
 */
public interface SeriatimTypeRepository extends JpaRepository<SeriatimType,Long> {

}
