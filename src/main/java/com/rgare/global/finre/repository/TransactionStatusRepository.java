package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.TransactionStatus;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TransactionStatus entity.
 */
public interface TransactionStatusRepository extends JpaRepository<TransactionStatus,Long> {

}
