package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.StructuredFinance;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the StructuredFinance entity.
 */
public interface StructuredFinanceSearchRepository extends ElasticsearchRepository<StructuredFinance, Long> {
}
