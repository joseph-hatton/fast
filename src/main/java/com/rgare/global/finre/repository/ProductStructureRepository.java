package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.ProductStructure;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ProductStructure entity.
 */
public interface ProductStructureRepository extends JpaRepository<ProductStructure,Long> {

}
