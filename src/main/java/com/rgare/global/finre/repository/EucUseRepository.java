package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.EucUse;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the EucUse entity.
 */
public interface EucUseRepository extends JpaRepository<EucUse,Long> {

}
