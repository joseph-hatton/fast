package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.TreatyAmendments;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TreatyAmendments entity.
 */
public interface TreatyAmendmentsSearchRepository extends ElasticsearchRepository<TreatyAmendments, Long> {
}
