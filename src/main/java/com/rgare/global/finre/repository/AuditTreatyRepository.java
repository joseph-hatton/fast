package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.AuditTreaty;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by s0044514 on 1/17/2017.
 */
public interface AuditTreatyRepository extends JpaRepository<AuditTreaty,Long> {
}
