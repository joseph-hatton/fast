package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.AgreementType;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the AgreementType entity.
 */
public interface AgreementTypeRepository extends JpaRepository<AgreementType,Long> {

}
