package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.TreatyManagement;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the TreatyManagement entity.
 */
public interface TreatyManagementRepository extends JpaRepository<TreatyManagement,Long> {
  TreatyManagement findOneByTreatyId(Long treatyId);

  /* findByClientCompanyIdIsNotNull */
  @Query("SELECT management FROM TreatyManagement management " +
    "WHERE  (management.treaty in(select treaty from Treaty treaty where treaty.treatyStatus.name='Pending' AND treaty.inputStatus='Complete'))" +
    "AND management.inputStatus='Complete'")
  List<TreatyManagement> completeNotification();

}
