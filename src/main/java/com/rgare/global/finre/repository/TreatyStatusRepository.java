package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.TreatyStatus;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TreatyStatus entity.
 */
public interface TreatyStatusRepository extends JpaRepository<TreatyStatus,Long> {

}
