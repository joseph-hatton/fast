package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.JhiAuthority;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the JhiAuthority entity.
 */
public interface JhiAuthorityRepository extends JpaRepository<JhiAuthority,Long> {
}
