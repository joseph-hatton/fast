package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.XrefTrackingTransaction;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the XrefTrackingTransaction entity.
 */
public interface XrefTrackingTransactionRepository extends JpaRepository<XrefTrackingTransaction,Long> {

}
