package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.AllocationType;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the AllocationType entity.
 */
public interface AllocationTypeRepository extends JpaRepository<AllocationType,Long> {

}
