package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.GfsAssignee;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the GfsAssignee entity.
 */
public interface GfsAssigneeRepository extends JpaRepository<GfsAssignee,Long> {

  @Query("SELECT gfsAssignee FROM GfsAssignee gfsAssignee WHERE  gfsAssignee.id NOT IN (3,1)")
  List<GfsAssignee> allActiveGfsAssignee();

}
