package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.TreatyManagement;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TreatyManagement entity.
 */
public interface TreatyManagementSearchRepository extends ElasticsearchRepository<TreatyManagement, Long> {
}
