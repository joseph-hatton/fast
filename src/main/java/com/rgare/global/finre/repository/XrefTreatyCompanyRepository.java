package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.XrefTreatyCompany;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the XrefTreatyCompany entity.
 */
public interface XrefTreatyCompanyRepository extends JpaRepository<XrefTreatyCompany,Long> {

  List<XrefTreatyCompany> findAllByTreatyId(Long treatyId);

  List<XrefTreatyCompany> findAllByCompanyId(Long companyId);

}
