package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.TreatySubtransactionCash;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TreatySubTransaction entity.
 */
public interface TreatySubtransactionCashSearchRepository extends ElasticsearchRepository<TreatySubtransactionCash, Long> {
}
