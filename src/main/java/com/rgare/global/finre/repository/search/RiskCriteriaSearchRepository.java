package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.RiskCriteria;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RiskCriteria entity.
 */
public interface RiskCriteriaSearchRepository extends ElasticsearchRepository<RiskCriteria, Long> {
}
