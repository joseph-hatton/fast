package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.DealType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DealType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DealTypeRepository extends JpaRepository<DealType, Long> {

}
