package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.DealTracking;

import com.rgare.global.finre.domain.TreatyTransactionRelief;
import io.swagger.models.auth.In;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.time.LocalDate;
import java.util.List;

/**
 * Spring Data JPA repository for the DealTracking entity.
 */
public interface DealTrackingRepository extends JpaRepository<DealTracking,Long> {

  Page<DealTracking> findAllByDealIdAndIsActive(Long dealId, Pageable pageable, Integer isActive);
  List<DealTracking> findAllByIsActiveAndDealTrackingStatus(Integer isActive, String status);
  //Page<DealTracking> findAllByDealIdAndIsActiveAndReportingPeriodEndDateBetween(Long dealId, Integer isActive, LocalDate start, LocalDate end, Pageable pageable);

//  Page<DealTracking> findAllByDealIdAndIsActiveAndReportingPeriodEndDateBetween(Long dealId, Integer isActive, LocalDate start, LocalDate end, Pageable pageable);
  Page<DealTracking> findAllByDealIdAndIsActiveAndReportingPeriodEndDateBetweenOrderByIdDesc(Long dealId, Integer isActive, LocalDate start, LocalDate end, Pageable pageable);

  //List<DealTracking> findAllByDealIdAndReportingPeriodEndDateAndIsActive(Long dealId, LocalDate start,Integer isActive);
  List<DealTracking> findAllByDealIdAndReportingPeriodEndDateAndIsActive(Long dealId, LocalDate start, Integer isActive);
}
