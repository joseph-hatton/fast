package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.State;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the State entity.
 */
public interface StateRepository extends JpaRepository<State,Long> {

}
