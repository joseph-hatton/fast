package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.TreatyTransactionCash;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Spring Data JPA repository for the TreatyTransaction entity.
 */
public interface TreatyTransactionCashRepository extends JpaRepository<TreatyTransactionCash,Long> {


      List<TreatyTransactionCash> findAllByTreatyIdAndIsActive(Long treatyId, Integer isActive);
      List<TreatyTransactionCash> findAllByMixTransactionsAndIsActive(Integer mixTransactions, Integer isActive);
      List<TreatyTransactionCash> findAllByMixTransactionsAndTreatyIdAndIsActive(Integer mixTransactions, Long treatyId, Integer isActive);
      List<TreatyTransactionCash> findAllByTreatyIdAndCurrencyCodeAndIsActive(Long treatyId,String currencyCode,Integer isActive);
      List<TreatyTransactionCash> findAllByCompanyIdAndIsActive(Long companyId,Integer isActive);
      List<TreatyTransactionCash> findAllByTreatyIdAndIsActiveAndTransactionStatusId(Long treatyId, Integer isActive ,Long transactionStatus);
      List<TreatyTransactionCash> findAllByTreatyIdAndCompanyIdAndIsActive(Long treatyId,Long companyId,Integer isActive);
      List<TreatyTransactionCash> findAllByDeleteNotification(Integer deleteNotification);
}

