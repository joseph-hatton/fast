package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.ProductSubtype;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ProductSubtype entity.
 */
public interface ProductSubtypeSearchRepository extends ElasticsearchRepository<ProductSubtype, Long> {
}
