package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.AuditRiskManagement;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the AuditRiskManagement entity.
 */
@SuppressWarnings("unused")
public interface AuditRiskManagementRepository extends JpaRepository<AuditRiskManagement,Long> {

}
