package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.FundingType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the FundingType entity.
 */
public interface FundingTypeSearchRepository extends ElasticsearchRepository<FundingType, Long> {
}
