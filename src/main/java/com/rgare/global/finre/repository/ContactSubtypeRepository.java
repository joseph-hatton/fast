package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.ContactSubtype;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ContactSubtype entity.
 */
public interface ContactSubtypeRepository extends JpaRepository<ContactSubtype,Long> {

}
