package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.EucUse;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the EucUse entity.
 */
public interface EucUseSearchRepository extends ElasticsearchRepository<EucUse, Long> {
}
