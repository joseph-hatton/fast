package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.Fastopedia;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import java.util.Set;

/**
 * Spring Data JPA repository for the AgreementType entity.
 */
public interface FastopediaRepository extends JpaRepository<Fastopedia,Long> {
    List<Fastopedia> findAllByIsActive(Integer isActive);
    Set<Fastopedia> findAllByIsActiveAndTitleContainingIgnoreCase(Integer isActive, String query);
    Set<Fastopedia> findAllByIsActiveAndContentContainingIgnoreCase(Integer isActive, String query);
}
