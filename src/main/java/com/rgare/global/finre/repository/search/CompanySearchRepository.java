package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Company entity.
 */
public interface CompanySearchRepository extends ElasticsearchRepository<Company, Long> {

  Page<Company> findByClientCompanyId(Long clientCompanyId ,Pageable pageable);

  Page<Company> findByClientCompanyIdIsNot(Long clientCompanyId ,Pageable pageable);

}
