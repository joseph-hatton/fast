package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.Currency;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Currency entity.
 */
public interface CurrencyRepository extends JpaRepository<Currency,Long> {

  Currency findByCodeAndYear(String code, Integer year);

  List<Currency> findByCode(String code);


}
