package com.rgare.global.finre.repository;

import com.rgare.global.finre.domain.IntroPage;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the IntroPage entity.
 */
public interface IntroPageRepository extends JpaRepository<IntroPage,Long> {

    Page<IntroPage> findAllByIsActive(Integer isActive, Pageable pageable);

    List<IntroPage> findAllByIsActive(@Param("isActive")Integer isActive);

    IntroPage findOneByIsActiveAndId(@Param("isActive")Integer isActive, @Param("id")Long id);
}
