package com.rgare.global.finre.repository.search;

import com.rgare.global.finre.domain.XrefTrackingTransaction;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the XrefTrackingTransaction entity.
 */
public interface XrefTrackingTransactionSearchRepository extends ElasticsearchRepository<XrefTrackingTransaction, Long> {
}
