
# Docker Hosts

## Where

[Nagios Log Server](https://monitor.rgare.net/nagioslogserver/)


---

## Example Help Desk ticket to create hosts

    Help Desk,
     
      Can you please create a ticket for Dev/Ops Unix Admins to create the “gold Image” for docker with additional 100G /opt/fast filesystem.  SAME NFS Filesystem across all of the boxes.
     
    We have already had them created for dev and test (stldfastdock01-03 and stltfastdock01-03).
     
    If we could have a cluster of two created for UAT and a cluster of two created for PROD. 
     
    Some questions asked when we requested the dev/test hosts:
     
    So who do you want it billed to?
    FAST
    How many systems are you requesting? 
                    2 nodes for UAT
                    3 nodes for PROD
    What is the application that the docker host will be supporting?
    FAST
    Who will need access to the server?
    Joe Hatton and Derek Tandy should get sudo access
    What environment will these systems be?
                    UAT
                    PROD
    Are there any lower environments that already exist for the docker application stack?
    stldfastdock01.rgare.net
    stldfastdock02.rgare.net
    stldfastdock03.rgare.net
    stltfastdock01.rgare.net
    stltfastdock02.rgare.net
    stltfastdock03.rgare.net
    I am assuming that the 100GB /opt/fast will be the nfs mount.
    Yes   

---

## Example Help Desk ticket to have Service Principal Names (SPNs) created for Kerberos

    Help Desk
    
    I need the following SPNs created for service account svc4finreldap
                   
        HTTP/stlsfastdock01
        HTTP/stlsfastdock01.rgare.net
        HTTP/stlsfastdock02
        HTTP/stlsfastdock02.rgare.net
        HTTP/stlpfastdock01
        HTTP/stlpfastdock01.rgare.net
        HTTP/stlpfastdock02
        HTTP/stlpfastdock02.rgare.net
        HTTP/stlpfastdock03
        HTTP/stlpfastdock03.rgare.net
     
    Instructions by Chris Crossen:
                   
        setspn -s HTTP/stlsfastdock01 RGARE\svc4finreldap
        setspn -s HTTP/stlsfastdock01.rgare.net RGARE\svc4finreldap
        setspn -s HTTP/stlsfastdock02 RGARE\svc4finreldap
        setspn -s HTTP/stlsfastdock02.rgare.net RGARE\svc4finreldap

        setspn -s HTTP/stlpfastdock01 RGARE\svc4finreldap
        setspn -s HTTP/stlpfastdock01.rgare.net RGARE\svc4finreldap
        setspn -s HTTP/stlpfastdock02 RGARE\svc4finreldap
        setspn -s HTTP/stlpfastdock02.rgare.net RGARE\svc4finreldap
        setspn -s HTTP/stlpfastdock03 RGARE\svc4finreldap
        setspn -s HTTP/stlpfastdock03.rgare.net RGARE\svc4finreldap
