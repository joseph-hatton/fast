## Fast API calls with csrf

Calling a FAST API takes a bit of work since you have to:
 
* be authenticated
* have a valid csrf token


---

### Curl scripts to call Fast API 

Creates a file in the current directory called fastcookies.txt to be used with `curl -b fastcookies.txt` on all future requests

	read -p "Enter s RGA User ID: " RGA_USER

	read -s -p "Enter RGA Password: " RGA_PASSWORD


	curl -I -c fastcookies.txt http://stldfastdock01.rgare.net/


	XSRF=$(cat fastcookies.txt | grep XSRF-TOKEN | cut -f 7)


	curl -c fastcookies.txt -sD - 'http://stldfastdock01.rgare.net/api/authentication?cacheBuster=1498655214893' -H 'Origin: http://stldfastdock01.rgare.net' -H "Cookie: XSRF-TOKEN=$XSRF; NG_TRANSLATE_LANG_KEY=%22en%22" -H "X-XSRF-TOKEN: $XSRF" -H 'Accept-Language: en-US,en;q=0.8' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Accept: application/json, text/plain, */*' -H 'Referer: http://stldfastdock01.rgare.net/' -H 'Accept-Encoding: gzip, deflate' -H 'Connection: keep-alive' --data "j_username=$RGA_USER&j_password=$RGA_PASSWORD&remember-me=true&submit=Login" --compressed

	unset $RGA_USER

	unset $RGA_PASSWORD

---

Example call

	curl -s -b fastcookies.txt 'http://stldfastdock01.rgare.net/api/treaty-trackings/SettlementCedingExpired?cacheBuster=1498655215089' -H 'Accept-Language: en-US,en;q=0.8' -H 'Accept: application/json, text/plain, */*' -H 'Referer: http://stldfastdock01.rgare.net/' -H 'Accept-Encoding: gzip, deflate, sdch'



	curl -s http://stldfastdock01.rgare.net/api/states' -H 'Accept-Language: en-US,en;q=0.8' -H 'Accept: application/json, text/plain, */*' -H 'Referer: http://stldfastdock01.rgare.net/' -H 'Accept-Encoding: gzip, deflate, sdch'
