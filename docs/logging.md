
# Centralized Logging

## Where

[Nagios Log Server](https://monitor.rgare.net/nagioslogserver/)


---

## Why

Why should we send our logs to centralized logging?

* It's easier to grant non-technical people access to your logs
* They persist longer than your Docker container and separately
* Once you have more than 1 instance for any given app per environment, it would be a pain to have to check each one for logs.  So instead, we send them all to one place.  


---

## Which logs are out there currently?

Only the dockerized Fast deployments are sending their logs to Nagios.

* DEV stldfastdock*
* Test stltfastdock* `not yet`

---

## How do our logs get there?

Docker makes it easy to redirect stdout and stderr logs by just applying a few arguments to the `docker run` command.

The Fast `docker run` is using configured with the following parameters for log redirection: 

  --log-driver=gelf --log-opt gelf-address=udp://10.17.6.132:12201 --log-opt gelf-compression-type=gzip --log-opt tag="{{.ImageName}}/{{.Name}}/{{.ID}}"


---

## How do we see our logs?

Go to the Nagios Log Server link (above) and you can paste any of the following queries into the search bar.  

	host: stldfastdock*

	host: stldfastdock* and message: "%Catalina%"	

	host: stldfastdock* and message: "%ERROR%"

	host: stldfastdock* and doc.message: "Monitoring the datasource"

	host: stldfastdock* and doc.level: ERROR

	host: stldfastdock* and message: "%[Krb5LoginModule] user entered username:%"

---

## Why are the logs in JSON format?

The Nagios Log Server automatically attempts to parse our log message strings as JSON.  
The parsed JSON object fields end up as fields we can query as `doc.field`. 

That will automatically happen for `doc.message` and `doc.level`, but we can also add custom fields in the java code:

[PersistentTokenRememberMeServices.java](../src/main/java/com/rgare/global/finre/security/PersistentTokenRememberMeServices.java)

I added the following Java imports.

    import net.logstash.logback.marker.Markers;
    
    import static net.logstash.logback.marker.Markers.append;

Then, I changed the following like of code:

    log.debug("Refreshing persistent login token for user '{}', series '{}'", login, token.getSeries());
  
to: 

    log.debug(append("login", login).and(append("tokenSeries", token.getSeries())), "Refreshing persistent login token");
         

The thought is when using JSON logging to no longer use String Interpolation to show your variable data in the log.   This makes it difficult to search for a specific log later with a specific value, for instance:


It is not the easiest to quickly find the following log where series is `9879237492374` (using regex or do a partial string match):

    Refreshing persistent login token for user 's0045015@rgare.net', series '9879237492374'

With our logs in JSON and using Markers to send the user and series to the log, we can very easily just use the following query:

    doc.message: "Refreshing persistent login token" and doc.tokenSeries: "9879237492374"
