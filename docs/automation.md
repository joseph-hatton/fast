### Fast Project Automation - My Goal

"Improve the operation efficiency and supportability of the FAST system we are moving forward with modernizing some items on the FAST architecture (Docker, Grafana, Nagios, PagerDuty, Jenkins, Ansible, Vault, SecureAuth etc.)"

---

### Architecture

||||
|-|-|-|-|
|Markdown<!-- .element: style="color: yellow" -->|Docker Hosts<!-- .element: style="color: yellow" -->|Docker<!-- .element: style="color: yellow " -->|Jenkins<!-- .element: style="color: yellow" -->|
|Nagios Logging<!-- .element: style="color: yellow" -->|Testing Coverage<!-- .element: style="color: yellow" -->|High Availability (HA)<!-- .element: style="color: yellow" -->|Docker 101<!-- .element: style="color: yellow" -->|
|Nagios Metrics|Grafana|PagerDuty|Split Deployables|
|SecureAuth|Disaster Recovery(DR)|Ansible|Vault|

----

### Markdown - _in progress_<!-- .element: style="color: yellow" -->	 

* Goals
  * Build version controlled documentation that lives with the project
  * Help transition the team to become used to utilizing it
* Work Completed <!-- .element: style="color: green" -->
  * [Started FAST documentation](http://tfs2015.rgare.net:8080/tfs/UWSolutions/FinRe/_welcome?name=FinRe&repositoryType=1&_a=view&path=%2FREADME.md)
  * [Detailed explanation on centralized logging](http://tfs2015.rgare.net:8080/tfs/UWSolutions/FinRe/_welcome?name=FinRe&path=%2Fdocs%2Flogging.md&repositoryType=1&_a=view)
* Work Required to Complete<!-- .element: style="color: yellow" -->
  * Once the team uses the new documentation as the source of truth and they add to it.<!-- .element: style="color: white" -->

----

### Docker Hosts - _in progress_<!-- .element: style="color: yellow" -->	

* Goals
  * Get and setup new hosts
* Work Completed<!-- .element: style="color: green" -->
  * Get Docker hosts
  * Get Admin account on hosts
  * [Create kerberos configuration on hosts](https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/job/manual%20-%20generate%20keytab/)
  * DEV/TEST Service Principal Names (SPNs) created
* Work Required to Complete<!-- .element: style="color: yellow" -->
  * UAT/PROD Service Principal Names (SPNs) created - in progress
  * Delete 3rd nodes on new dev/test<!-- .element: style="color: orange" -->

----

### Docker - _in progress_<!-- .element: style="color: yellow" -->	 

* Goals
  * Convert the current FAST deployment to a Docker container
* Work Completed <!-- .element: style="color: green" -->
  * [Dockerfile](http://tfs2015.rgare.net:8080/tfs/UWSolutions/_git/FinRe?path=%2Fsrc%2Fmain%2Fdocker%2FDockerfile&version=GBmaster&_a=contents)
  * [DEV](http://tfs2015.rgare.net:8080/tfs/UWSolutions/_git/FinRe/commit/caee625acfbd6ead11da1e4a8b03170e58f6e098)
  * TEST<!-- .element: style="color: white" -->
* Work Required to Complete<!-- .element: style="color: yellow" -->
  * Deploy to UAT - needs deployment/testing
  * Deploy to PROD- needs deployment/testing

----

### Jenkins - _in progress_<!-- .element: style="color: yellow" -->	 

* Goals
  * Automate deployments with continuous integration
* Work Completed<!-- .element: style="color: green" -->
  * [Automated build/test/sonarqube/docker image](https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/) 
    * [Deploy docker container to DEV](https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/job/02%20-%20docker%20deploy%20to%20dev/)
    * [Deploy docker container to TEST](https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/job/02%20-%20docker%20deploy%20to%20test/)
* Work Required to Complete
  * Deploy to UAT - in progress<!-- .element: style="color: yellow" -->
  * Deploy to PROD - not started<!-- .element: style="color: orange" -->
  * [Automate FAST Test Automation - in progress](https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/job/Automation/)

----

### Nagios Logging - _in progress_<!-- .element: style="color: yellow" -->

* Goals
  * Move application logs to centralized logging
* Work Completed<!-- .element: style="color: green" -->
  * Centralized json logging via gelf log driver on docker container
  * Converted deployed logs to JSON
  * [Added logging documentation](http://tfs2015.rgare.net:8080/tfs/UWSolutions/FinRe/_welcome?name=FinRe&path=%2Fdocs%2Flogging.md&repositoryType=1&_a=view)
  * Access request for entire team
* Work Required to Complete<!-- .element: style="color: yellow" -->
  * To truly benefit from JSON logging all log statements would have to be amended

----

### Testing Coverage - _in progress_<!-- .element: style="color: yellow" -->

* Goals
  * Integrate testing coverage with jenkins (Java and JS)
* Work Completed<!-- .element: style="color: green" -->
  * Resolved build errors to be able to run Java tests
  * [Jenkins build to upload both Java and JS tests](https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/job/01%20-%20build%20test%20sonarqube%20and%20docker%20image/)
* Work Required to Complete<!-- .element: style="color: yellow" -->
  * ?

----

### High Availability (HA) - _in progress_<!-- .element: style="color: yellow" -->

* Goals
  * Deploy multiple hosts behind a load balancer for reliability
* Work Completed<!-- .element: style="color: green" -->
  * DEV Deploy more than one host 
  * Test how stateful Spring Security is
  * TEST Deploy more than one host
* Work Required to Complete<!-- .element: style="color: yellow" -->
  * UAT Deploy more than one host
  * PROD Deploy more than one host
  * Configure load balancer (F5) in all environments

----

### Docker 101 - _in progress_<!-- .element: style="color: yellow" -->

* Goals
  * Show team the benefits of Docker and show the current Fast implementation
* Work Completed<!-- .element: style="color: green" -->
  * Slides created
  * Schedule meeting (Thursday July 13 1-1:50pm)
* Work Required to Complete<!-- .element: style="color: yellow" -->
  * Have meeting

----

### Nagios Metrics - _not started_<!-- .element: style="color: orange" -->

* Goals
  * Integrate system metrics to show environment status
* Work Completed<!-- .element: style="color: green" -->
  * Initial conversation with ITAM
* Work Required to Complete<!-- .element: style="color: yellow" -->
  * Integrate Registrator on hosts to update ITAM consul
    * talk to Doug Wagner: `starts first, shuts down last, points to itam consul`

----

### Grafana - _not started_<!-- .element: style="color: orange" -->

* Goals
  * Visualize system/app metrics
* Work Completed<!-- .element: style="color: green" -->
  * Initial conversation with ITAM
* Work Required to Complete<!-- .element: style="color: yellow" -->
  * First integrate metrics

----

### PagerDuty - _not started_<!-- .element: style="color: orange" -->

* Goals
  * Get automated notifications 
* Work Completed<!-- .element: style="color: green" -->
  * Initial conversation with ITAM
* Work Required to Complete<!-- .element: style="color: yellow" -->
  * Determine support group
  * Have ITAM create business service (they need URLs of app)
  * Configure notifications

----

### Split Deployables - _not started_<!-- .element: style="color: orange" -->

* Goals
  * Separate Fast application into micro-service and UI
* Work Completed<!-- .element: style="color: green" -->
  * 
* Work Required to Complete<!-- .element: style="color: yellow" -->
  * ??

----

### SecureAuth - _not started_<!-- .element: style="color: orange" -->

* Goals
  * Convert Fast from authenticating using Kerberos and over to oauth
* Work Completed<!-- .element: style="color: green" -->
  * Discussed with other Ocelots
* Work Required to Complete<!-- .element: style="color: yellow" -->
  * Determine path forward
    * Oauth for UI's requires custom logic on app or in a proxy
    * Ocelot has already built a proxy that does this - requires Swarm currently

----

### Automated Disaster Recovery - _not started_<!-- .element: style="color: orange" -->

* Goals
  * Automatically resolve app health issues
* Work Completed<!-- .element: style="color: green" -->
  * Discussed with ITAM, nothing currently available
* Work Required to Complete<!-- .element: style="color: yellow" -->
  * Determine path forward
    * ITAM has plans to try to get F5 to pull out bad hosts (and re-add)
    * Utilizing Docker Swarm could also solve this

----

### Ansible - _not started_<!-- .element: style="color: orange" -->

* Goals
  * Utilize standard automation tooling
* Work Completed<!-- .element: style="color: green" -->
  * Demo'd current Jenkins stragety with ITAM - Ansible use optional
* Work Required to Complete<!-- .element: style="color: yellow" -->
  * Determine path forward

----

### Vault - _not started_<!-- .element: style="color: orange" -->

* Goals
  * Utilize centralized secret management Vault to remove passwords from vcs
* Work Completed<!-- .element: style="color: green" -->
  * 
* Work Required to Complete<!-- .element: style="color: yellow" -->
  * Investigate
  * Implement

---

### Additional Work

* Add build restrictions to deployments
  * restrict build to not allow projects with less/worse testing to deploy - `started - on hold`
  * restrict uat - ??
  * prod to only allow Aria - `not started`
* Blue/Green deployments
    * Zero downtime deployments
    * Requires configurable DNS to point to different ports (or swarm healthchecks)
* bower is deprecated
* no java unit test coverage
* java IT
* automated database updates


