# Fast (finre)

This application was generated using JHipster 4.2.0, you can find documentation and help at [https://jhipster.github.io/documentation-archive/v4.2.0](https://jhipster.github.io/documentation-archive/v4.2.0).

---

## Deployed URLs

The FAST containers are now deployed on the same hosts as the Stable Value containers.
DEV, TEST and UAT all go to stldstbvldock01-stldstbvldock03
PROD goes to stlpstbvldock01-stlpstbvldock03

### Docker-ized
* [DEV - fast-dev.rgare.net](http://fast-dev.rgare.net)
  * stldstbvldock01, stldstbvldock02, stldstbvldock03
	* [swagger docs](http://fast-dev.rgare.net/v2/api-docs)
	* [swagger ui](http://fast-dev.rgare.net/swagger-ui/index.html)
	* [swarm visualizer](http://stldstbvldock01.rgare.net:8081)
* [TEST - fast-test.rgare.net](http://fast-test.rgare.net)
  * stldstbvldock01, stldstbvldock02, stldstbvldock03
	* [swagger docs](http://fast-test.rgare.net/v2/api-docs)
	* [swagger ui](http://fast-test.rgare.net/swagger-ui/index.html)
	* [swarm visualizer](http://stldstbvldock01.rgare.net:8081)
* [UAT - fast-uat.rgare.net](http://fast-uat.rgare.net)
  * stldstbvldock01, stldstbvldock02, stldstbvldock03
	* [swagger docs](http://fast-uat.rgare.net/v2/api-docs)
	* [swagger ui](http://fast-uat.rgare.net/swagger-ui/index.html)
	* [swarm visualizer](http://stldstbvldock01.rgare.net:8081)
* [PROD - fast.rgare.net](http://fast.rgare.net)
  * stlpstbvldock01, stlpstbvldock02, stlpstbvldock03
	* [swagger docs](http://fast.rgare.net/v2/api-docs)
	* [swagger ui](http://fast.rgare.net/swagger-ui/index.html)
	* [swarm visualizer](http://stlpstbvldock01.rgare.net:8081)

---

## Logs

[Nagios Log Server](https://monitor.rgare.net/nagioslogserver/)

* dev - sample query
  * [container_name: "fast-dev_web*"](https://monitor.rgare.net/nagioslogserver/index.php/dashboard#/dashboard/script/logserver.js?split=,&query=container_name:%20%22fast-dev_web*%22)
* test - sample query
  * [container_name: "fast-test_web*"](https://monitor.rgare.net/nagioslogserver/index.php/dashboard#/dashboard/script/logserver.js?split=,&query=container_name:%20%22fast-test_web*%22)

---

### Detailed explanation on centralized logging

[Fast Centralized Logging - where, how, why](docs/logging.md)

---

## VCS

* [TFS Fast](http://tfs2015.rgare.net:8080/tfs/UWSolutions/FinRe/_git/FinRe)
  * The Fast application
* [TFS Fast Common Deployment](http://tfs2015.rgare.net:8080/tfs/UWSolutions/FinRe/_git/FAST-Common-Deployment)
  * Deployment scripts and the jenkins slack script
* [TFS Fast Test Automation](http://tfs2015.rgare.net:8080/tfs/UWSolutions/FinRe/_git/Fast-Test-Automation)
  * Automated testing using Visual Studio

---

## CI

[Fast Jenkins Builds](https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/)

* [01 - build test sonarqube and docker image](https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/job/01%20-%20build%20test%20sonarqube%20and%20docker%20image/)
  * **When:** Checks every 5 minutes if there has been a commit to master
  * **What:** ./gradlew clean war test jacocoTestReport gulp_test buildDocker
  * **What:** Uploads sonar results
  * **What:** Tags the Fast Docker Image with the GIT COMMIT hash and pushes it to the RGA Docker Registry
  * **What:** The `-P` profile attribute in the jenkins configuration on the `./gradlew war` defines if the resulting war has it's js/css minified
    * `-Pdev=dev` does not minify the JS/CSS
    * `-Pprod=prod` DOES minify the JS/CSS
    * These options are configured under `Switches` in the `Invoke Gradle Script` section of the build
  * **After:** If successful, sends the GIT_COMMIT (as FAST_DOCKER_IMAGE_VERSION) to the `02 - docker deploy to dev`
* [02 - docker deploy to dev](https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/job/02%20-%20docker%20deploy%20to%20dev/)
  * **When:** Fired by each successful `01 - build test sonarqube and docker image`
  * **What:** Deploys Fast Docker Stack
* [02 - docker deploy version from dev into test (only if different)](https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/job/02%20-%20docker%20deploy%20version%20from%20dev%20into%20test%20(only%20if%20different)/)
  * **When:** Runs every two hours
  * **What:** Checks to see if the docker version in Test is the same as what is in Dev
  * **After:** If Dev and Test have different versions, sends the DEV FAST_DOCKER_IMAGE_VERSION to the `03 - docker deploy to test`
* [03 - docker deploy to test](https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/job/03%20-%20docker%20deploy%20to%20test/)
  * **When:** Fired by each `02 - docker deploy version from dev into test (only if different)` that finds different versions
  * **What:** Deploys Fast Docker Stack
  * **After:** If successful, sends the FAST_DOCKER_IMAGE_VERSION to the `04 - smoke test the test env - blocks uat deploy`
* [04 - smoke test the test env - blocks uat deploy](https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/job/04%20-%20smoke%20test%20ethe%20test%20env%20-%20blocks%20uat%20deploy/)
  * **When:** Fired by each successful `03 - docker deploy to test` 
  * **Where:** Jenkins Host: stlpalm19 - set up for Windows Selenium runs as user `Svc4jenkinsautotst@RGARE.NET `
  * **What:** Runs the Automated Smoke Tests against the Test environment
  * **After:** If successful, sends the FAST_DOCKER_IMAGE_VERSION to the `05 - docker deploy to uat`
* [05 - docker deploy to uat](https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/job/05%20-%20docker%20deploy%20to%20uat/)
  * **When:** Fired by each successful `04 - smoke test the test env - blocks uat deploy` 
  * **What:** Deploys Fast Docker Stack
* [15 - docker deploy to prod](https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/job/15%20-%20docker%20deploy%20to%20prod/)
  * **When:** Has to be manually started
  * **Wwho:** Aria and Matt and people in ALM - Actuarial Solutions-FAST_Deployers can deploy to prod
  * **What:** Gets the FAST_DOCKER_IMAGE_VERSION by grabbing the latest version from UAT
  * **What:** Deploys Fast Docker Stack
  
---

## Code Quality

* [SonarQube - FinRe](https://stlpsnrqube01.rgare.net/dashboard?id=com.rgare.global.finre%3Afinre)

---

## Hosts

* **dev**
  * stldfastdock01.rgare.net
  * stldfastdock02.rgare.net
  * stldfastdock03.rgare.net
* **test**
  * stltfastdock01.rgare.net
  * stltfastdock02.rgare.net
  * stltfastdock03.rgare.net
* **uat**
  * stlsfastdock01.rgare.net
  * stlsfastdock02.rgare.net
* **prod**
  * stlpfastdock01.rgare.net
  * stlpfastdock02.rgare.net
  * stlpfastdock03.rgare.net

All of these hosts were created with the following 
/opt/fast - 100 GB nfs mount

### Kerberos

Kerberos keytabs and configs have been set up in `/opt/fast/kerberos`

You can generate new keytabs with this [Jenkins Build](https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/job/99%20-%20create%20and%20deploy%20keytabs%20to%20all%20hosts%20(prod%20not%20fully%20pushed,%20requires%20ticket)/)

----

### Docker host ticket examples

[Help Desk Ticket Examples](docs/docker-hosts.md)

---

## CSRF and APIs


* [API CSRF Example CURLs](docs/api-csrf.md)
* [Jenkins Build](https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/job/zz%20-%20example%20of%20how%20to%20get%20csrf%20and%20test%20finre%20api/)


---

## Grafana (Infrastructure Alerting / Monitoring)

* [Grafana - be sure to log in](http://stldstbvldock01.rgare.net:3001/dashboard/db/docker-swarm-services?refresh=30s&orgId=1)

---

## Ocelot Automation

* [slides](docs/automation.md)



## Development

Before you can build this project, you must install and configure the following dependencies on your machine:

1. [Node.js][]: We use Node to run a development web server and build the project.
   Depending on your system, you can install Node either from source or as a pre-packaged bundle.

After installing Node, you should be able to run the following command to install development tools.
You will only need to run this command when dependencies change in [package.json](package.json).

    npm install

We use [Gulp][] as our build system. Install the Gulp command-line tool globally with:

    npm install -g gulp-cli

Run the following commands in two separate terminals to create a blissful development experience where your browser
auto-refreshes when files change on your hard drive.

    ./gradlew
    gulp

[Bower][] is used to manage CSS and JavaScript dependencies used in this application. You can upgrade dependencies by
specifying a newer version in [bower.json](bower.json). You can also run `bower update` and `bower install` to manage dependencies.
Add the `-h` flag on any command to see how you can use it. For example, `bower update -h`.

For further instructions on how to develop with JHipster, have a look at [Using JHipster in development][].


## Building for production

To optimize the finre application for production, run:

    ./gradlew -Pprod clean bootRepackage

This will concatenate and minify the client CSS and JavaScript files. It will also modify `index.html` so it references these new files.
To ensure everything worked, run:

    java -jar build/libs/*.war

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser.

Refer to [Using JHipster in production][] for more details.

## Testing

To launch your application's tests, run:

    ./gradlew test

### Client tests

Unit tests are run by [Karma][] and written with [Jasmine][]. They're located in [src/test/javascript/](src/test/javascript/) and can be run with:

    gulp test

UI end-to-end tests are powered by [Protractor][], which is built on top of WebDriverJS. They're located in [src/test/javascript/e2e](src/test/javascript/e2e)
and can be run by starting Spring Boot in one terminal (`./gradlew bootRun`) and running the tests (`gulp itest`) in a second one.
### Other tests

Performance tests are run by [Gatling][] and written in Scala. They're located in [src/test/gatling](src/test/gatling) and can be run with:

    ./gradlew gatlingRun

For more information, refer to the [Running tests page][].

